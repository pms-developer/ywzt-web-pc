﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-20 11:49
    /// 描 述：工程合同变更
    /// </summary>
    public class ProjectContractChangeBLL : ProjectContractChangeIBLL
    {
        private ProjectContractChangeService projectContractChangeService = new ProjectContractChangeService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ContractChangeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectContractChangeService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChangeQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractChangeQuantitiesEntity> GetProject_ContractChangeQuantitiesList(string keyValue)
        {
            try
            {
                return projectContractChangeService.GetProject_ContractChangeQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChangeMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractChangeMaterialsEntity> GetProject_ContractChangeMaterialsList(string keyValue)
        {
            try
            {
                return projectContractChangeService.GetProject_ContractChangeMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChange表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractChangeEntity GetProject_ContractChangeEntity(string keyValue)
        {
            try
            {
                return projectContractChangeService.GetProject_ContractChangeEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChangeMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractChangeMaterialsEntity GetProject_ContractChangeMaterialsEntity(string keyValue)
        {
            try
            {
                return projectContractChangeService.GetProject_ContractChangeMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractChangeQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractChangeQuantitiesEntity GetProject_ContractChangeQuantitiesEntity(string keyValue)
        {
            try
            {
                return projectContractChangeService.GetProject_ContractChangeQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectContractChangeService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_ContractChangeEntity entity, List<Project_ContractChangeMaterialsEntity> project_ContractChangeMaterialsList, List<Project_ContractChangeQuantitiesEntity> project_ContractChangeQuantitiesList, string deleteList, string type)
        {
            try
            {
                projectContractChangeService.SaveEntity(keyValue, entity, project_ContractChangeMaterialsList, project_ContractChangeQuantitiesList, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("10004");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 审核通过后事件
        /// </summary>
        /// <param name="keyValue">合同变更主键</param>
        public void Audit(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            try
            {
                var quantitiesList = new List<Project_ContractQuantitiesEntity>();
                var materialsList = new List<Project_ContractMaterialsEntity>();

                var changeInfo = projectContractChangeService.GetProject_ContractChangeEntity(keyValue);
                var contractId = changeInfo.ProjectContractId;

                var contractModel = contractBll.GetProject_ContractEntity(contractId);
                contractModel.TotalAmount= (contractModel.TotalAmount??0)+changeInfo.CurrentChangeAmount;
                contractModel.ChangeAmount= (contractModel.ChangeAmount??0)+changeInfo.CurrentChangeAmount;

                var changeQuantities = projectContractChangeService.GetProject_ContractChangeQuantitiesList(keyValue);
                if (changeQuantities.Any())
                {
                    var quantities = contractBll.GetProject_ContractQuantitiesList(contractId);
                    foreach (var item in changeQuantities)
                    {
                        var info = quantities.FirstOrDefault(i => i.ID == item.ProjectContractQuantitiesId);
                        if (info == null) continue;

                        info.ChangeQuantities = (info.ChangeQuantities ?? 0) + item.Quantities;
                        info.ChangeTotalPrice = (info.ChangeTotalPrice ?? 0) + item.TotalPrice;

                        if (info.ChangeTotalPrice != 0)
                        {
                            info.ChangePrice = info.ChangeTotalPrice / info.ChangeQuantities;
                        }
                        else
                        {
                            info.ChangePrice = 0;
                        }
                     
                        info.SetCurrentInfo();

                        quantitiesList.Add(info);
                    }
                }

                var changeMaterials = projectContractChangeService.GetProject_ContractChangeMaterialsList(keyValue);
                if (changeMaterials.Any())
                {
                    var materials = contractBll.GetProject_ContractMaterialsList(contractId);
                    foreach (var item in changeMaterials)
                    {
                        var info = materials.FirstOrDefault(i => i.ID == item.ProjectContractMaterialsId);
                        if (info == null) continue;
                        info.ChangeQuantity = (info.ChangeQuantity??0)+item.Quantity;
                        info.ChangeTotalPrice = (info.ChangeTotalPrice??0)+item.TotalPrice;

                        if (info.ChangeTotalPrice != 0)
                        {
                            info.ChangePrice = info.ChangeTotalPrice / info.ChangeQuantity;
                        }
                        else
                        {
                            info.ChangePrice = 0;
                        }

                        info.SetCurrentInfo();

                        materialsList.Add(info);
                    }
                }
                contractBll.UpdateContract(contractModel, quantitiesList, materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 反审核事件
        /// </summary>
        /// <param name="keyValue">合同变更主键</param>
        public void UnAudit(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            try
            {
                var quantitiesList = new List<Project_ContractQuantitiesEntity>();
                var materialsList = new List<Project_ContractMaterialsEntity>();

                var changeInfo = projectContractChangeService.GetProject_ContractChangeEntity(keyValue);
                var contractId = changeInfo.ProjectContractId;

                var contractModel = contractBll.GetProject_ContractEntity(contractId);
                contractModel.TotalAmount = (contractModel.TotalAmount ?? 0) - changeInfo.CurrentChangeAmount;
                contractModel.ChangeAmount = (contractModel.ChangeAmount ?? 0) - changeInfo.CurrentChangeAmount;

                var changeQuantities = projectContractChangeService.GetProject_ContractChangeQuantitiesList(keyValue);
                if (changeQuantities.Any())
                {
                    var quantities = contractBll.GetProject_ContractQuantitiesList(contractId);
                    foreach (var item in changeQuantities)
                    {
                        var info = quantities.FirstOrDefault(i => i.ID == item.ProjectContractQuantitiesId);
                        if (info == null) continue;
        
                        info.ChangeQuantities = (info.ChangeQuantities ?? 0) - item.Quantities;
                        info.ChangeTotalPrice = (info.ChangeTotalPrice ?? 0) - item.TotalPrice;

                        if (info.ChangeTotalPrice != 0)
                        {
                            info.ChangePrice = info.ChangeTotalPrice / info.ChangeQuantities;
                        }
                        else
                        {
                            info.ChangePrice = 0;
                        }

                        info.SetCurrentInfo();

                        quantitiesList.Add(info);
                    }
                }

                var changeMaterials = projectContractChangeService.GetProject_ContractChangeMaterialsList(keyValue);
                if (changeMaterials.Any())
                {
                    var materials = contractBll.GetProject_ContractMaterialsList(contractId);
                    foreach (var item in changeMaterials)
                    {
                        var info = materials.FirstOrDefault(i => i.ID == item.ProjectContractMaterialsId);
                        if (info == null) continue;
                        info.ChangeQuantity = (info.ChangeQuantity ?? 0) - item.Quantity;
                        info.ChangeTotalPrice = (info.ChangeTotalPrice ?? 0) - item.TotalPrice;

                        if (info.ChangeTotalPrice != 0)
                        {
                            info.ChangePrice = info.ChangeTotalPrice / info.ChangeQuantity;
                        }
                        else
                        {
                            info.ChangePrice = 0;
                        }

                        info.SetCurrentInfo();

                        materialsList.Add(info);
                    }
                }
                contractBll.UpdateContract(contractModel, quantitiesList, materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
