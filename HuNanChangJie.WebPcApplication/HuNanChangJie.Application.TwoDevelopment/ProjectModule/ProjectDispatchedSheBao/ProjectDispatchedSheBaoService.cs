﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 19:18
    /// 描 述：外派人员社保
    /// </summary>
    public class ProjectDispatchedSheBaoService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_DispatchedSheBaoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Workflow_ID,
                t.AuditStatus,
                t.TotalAmount,
                t.ProjectSubjectId,
                t.CreationDate,
                t.ProjectID,
                t.AccrueDate,
                t.TotalCount,
                t.FinishCount,
                t.TotalFinish,
                t.MonthTotalAmount,
                t.AccrueState
                ");
                strSql.Append("  FROM Project_DispatchedSheBao t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" and t.ProjectId=@ProjectId");
                }
                var list=this.BaseRepository().FindList<Project_DispatchedSheBaoEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedSheBaoDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_DispatchedSheBaoDetailsEntity> GetProject_DispatchedSheBaoDetailsList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.F_RealName as UserName 
                            from Project_DispatchedSheBaoDetails as a left join Base_user as b
                            on a.userId=b.F_UserId
                            where   a.ProjectDispatchedStaffSocialSecurityId=@keyValue ");
                var list= this.BaseRepository().FindList<Project_DispatchedSheBaoDetailsEntity>(sb.ToString(),new { keyValue });
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedSheBao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_DispatchedSheBaoEntity GetProject_DispatchedSheBaoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_DispatchedSheBaoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedSheBaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_DispatchedSheBaoDetailsEntity GetProject_DispatchedSheBaoDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_DispatchedSheBaoDetailsEntity>(t=>t.ProjectDispatchedStaffSocialSecurityId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return this.BaseRepository().FindList<ProjectEntity>(i => i.AuditStatus == "2");
            }
            catch (Exception ex)
            {

                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        internal IEnumerable<Project_DispatchedSheBaoEntity> GetDispatchedSheBaoList(IEnumerable<string> projectIds)
        {
            try
            {
                return this.BaseRepository().FindList<Project_DispatchedSheBaoEntity>(i => projectIds.Contains(i.ProjectID) && i.AuditStatus == "2" && i.AccrueState != true);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_DispatchedSheBaoEntity = GetProject_DispatchedSheBaoEntity(keyValue); 
                db.Delete<Project_DispatchedSheBaoEntity>(t=>t.ID == keyValue);
                db.Delete<Project_DispatchedSheBaoDetailsEntity>(t=>t.ProjectDispatchedStaffSocialSecurityId == project_DispatchedSheBaoEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_DispatchedSheBaoEntity GetFormInfo(string projectId)
        {
            try
            {
                return BaseRepository().FindEntity<Project_DispatchedSheBaoEntity>(i => i.ProjectID == projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
    

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_DispatchedSheBaoEntity entity,List<Project_DispatchedSheBaoDetailsEntity> project_DispatchedSheBaoDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_DispatchedSheBaoEntityTmp = GetProject_DispatchedSheBaoEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_DispatchedSheBaoDetailsUpdateList= project_DispatchedSheBaoDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_DispatchedSheBaoDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_DispatchedSheBaoDetailsInserList= project_DispatchedSheBaoDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_DispatchedSheBaoDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectDispatchedStaffSocialSecurityId = project_DispatchedSheBaoEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_DispatchedSheBaoDetailsEntity item in project_DispatchedSheBaoDetailsList)
                    {
                        item.ProjectDispatchedStaffSocialSecurityId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
