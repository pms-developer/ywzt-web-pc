﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 19:18
    /// 描 述：外派人员社保
    /// </summary>
    public class ProjectDispatchedSheBaoBLL : ProjectDispatchedSheBaoIBLL
    {
        private ProjectDispatchedSheBaoService projectDispatchedSheBaoService = new ProjectDispatchedSheBaoService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_DispatchedSheBaoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectDispatchedSheBaoService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedSheBaoDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_DispatchedSheBaoDetailsEntity> GetProject_DispatchedSheBaoDetailsList(string keyValue)
        {
            try
            {
                return projectDispatchedSheBaoService.GetProject_DispatchedSheBaoDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedSheBao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_DispatchedSheBaoEntity GetProject_DispatchedSheBaoEntity(string keyValue)
        {
            try
            {
                return projectDispatchedSheBaoService.GetProject_DispatchedSheBaoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_DispatchedSheBaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_DispatchedSheBaoDetailsEntity GetProject_DispatchedSheBaoDetailsEntity(string keyValue)
        {
            try
            {
                return projectDispatchedSheBaoService.GetProject_DispatchedSheBaoDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return projectDispatchedSheBaoService.GetAllProjectList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        IEnumerable<Project_DispatchedSheBaoEntity> ProjectDispatchedSheBaoIBLL.GetDispatchedSheBaoList(IEnumerable<string> projectIds)
        {
            try
            {
                return projectDispatchedSheBaoService.GetDispatchedSheBaoList(projectIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectDispatchedSheBaoService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_DispatchedSheBaoEntity entity,List<Project_DispatchedSheBaoDetailsEntity> project_DispatchedSheBaoDetailsList,string deleteList,string type)
        {
            try
            {
                projectDispatchedSheBaoService.SaveEntity(keyValue, entity,project_DispatchedSheBaoDetailsList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_DispatchedSheBaoEntity GetFormInfo(string projectId)
        {
            try
            {
                return projectDispatchedSheBaoService.GetFormInfo(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
