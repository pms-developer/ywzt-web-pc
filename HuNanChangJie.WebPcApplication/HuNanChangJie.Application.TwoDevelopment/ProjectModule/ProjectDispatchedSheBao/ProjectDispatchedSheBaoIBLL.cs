﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 19:18
    /// 描 述：外派人员社保
    /// </summary>
    public interface ProjectDispatchedSheBaoIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_DispatchedSheBaoEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_DispatchedSheBaoDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_DispatchedSheBaoDetailsEntity> GetProject_DispatchedSheBaoDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_DispatchedSheBao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_DispatchedSheBaoEntity GetProject_DispatchedSheBaoEntity(string keyValue);
        /// <summary>
        /// 获取Project_DispatchedSheBaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_DispatchedSheBaoDetailsEntity GetProject_DispatchedSheBaoDetailsEntity(string keyValue);

        /// <summary>
        /// 获取所有已审核的项目信息
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProjectEntity> GetAllProjectList();

        /// <summary>
        /// 根据项目ID集合获取项目外派人员使用社保集合(已审核)
        /// </summary>
        /// <param name="projectIds">项目ID集合</param>
        /// <returns></returns>
        IEnumerable<Project_DispatchedSheBaoEntity> GetDispatchedSheBaoList(IEnumerable<string> projectIds);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_DispatchedSheBaoEntity entity,List<Project_DispatchedSheBaoDetailsEntity> project_DispatchedSheBaoDetailsList,string deleteList,string type);
        Project_DispatchedSheBaoEntity GetFormInfo(string projectId);
        #endregion

    }
}
