﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.IO;
using HuNanChangJie.Cache.Base;
using HuNanChangJie.Cache.Factory;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-08 16:58
    /// 描 述：投标文档
    /// </summary>
    public class ProjectBidDocumentBLL : ProjectBidDocumentIBLL
    {
        private ProjectBidDocumentService projectBidDocumentService = new ProjectBidDocumentService();
        private ICache cache = CacheFactory.CaChe();
        private string cacheKey = "hncjpms_annexes_";

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_BidDocumentEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectBidDocumentService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_BidDocument表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_BidDocumentEntity GetProject_BidDocumentEntity(string keyValue)
        {
            try
            {
                return projectBidDocumentService.GetProject_BidDocumentEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectBidDocumentService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_BidDocumentEntity entity,string deleteList,string type)
        {
            try
            {
                projectBidDocumentService.SaveEntity(keyValue, entity,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 保存附件到文件中
        /// </summary>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="filePath">文件路径</param>
        /// <param name="chunks">总共分片数</param>
        /// <returns>-1:表示保存失败</returns>
        public long SaveAnnexesToFile(string fileGuid, string filePath, int chunks)
        {
            try
            {
                long filesize = 0;
                //创建一个FileInfo对象
                FileInfo file = new FileInfo(filePath);
                //创建文件
                FileStream fs = file.Create();
                for (int i = 0; i < chunks; i++)
                {
                    byte[] bufferByRedis = cache.Read<byte[]>(cacheKey + i + "_" + fileGuid, CacheId.annexes);
                    if (bufferByRedis == null)
                    {
                        return -1;
                    }
                    //写入二进制流
                    fs.Write(bufferByRedis, 0, bufferByRedis.Length);
                    filesize += bufferByRedis.Length;
                    cache.Remove(cacheKey + i + "_" + fileGuid, CacheId.annexes);
                }
                //关闭文件流
                fs.Close();

                return filesize;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 移除文件分片数据
        /// </summary>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="chunks">文件分片数</param>
        public void RemoveChunkAnnexes(string fileGuid, int chunks)
        {
            try
            {
                for (int i = 0; i < chunks; i++)
                {
                    cache.Remove(cacheKey + i + "_" + fileGuid, CacheId.annexes);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存附件（支持大文件分片传输）
        /// </summary>
        /// <param name="projectId">项目ID</param>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="fileName">文件名</param>
        /// <param name="chunks">文件总分片数</param>
        /// <param name="userInfo">用户信息</param>
        /// <returns></returns>
        public bool SaveAnnexes(string projectId, string fileGuid, string fileName, int chunks, UserInfo userInfo)
        {
            try
            {
                //获取文件完整文件名(包含绝对路径)
                //文件存放路径格式：/Resource/ResourceFile/{userId}/{date}/{guid}.{后缀名}
                string filePath = Config.GetValue("AnnexesFile");
                string uploadDate = DateTime.Now.ToString("yyyyMMdd");
                string FileEextension = Path.GetExtension(fileName);
                string virtualPath = string.Format("{0}/{1}/{2}/{3}{4}", filePath, userInfo.userId, uploadDate, fileGuid, FileEextension);
                //创建文件夹
                string path = Path.GetDirectoryName(virtualPath);
                Directory.CreateDirectory(path);
                var fileEntity  = new Project_BidDocumentEntity();
                if (!System.IO.File.Exists(virtualPath))
                {
                    long filesize = SaveAnnexesToFile(fileGuid, virtualPath, chunks);
                    if (filesize == -1)// 表示保存失败
                    {
                        RemoveChunkAnnexes(fileGuid, chunks);
                        return false;
                    }

                    //文件信息写入数据库
                    fileEntity.ID = fileGuid;
                    fileEntity.FileName = fileName;
                    fileEntity.FilePath = virtualPath;
                    fileEntity.FileSize = filesize.ToDecimal();

                    fileEntity.Creation_Id = userInfo.userId;
                    fileEntity.CreationName = userInfo.realName;
                    fileEntity.ProjectID =projectId;
                    fileEntity.CreationDate = DateTime.Now;
                    projectBidDocumentService.SaveEntity(fileEntity);
                }
                return true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
