﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-08 16:58
    /// 描 述：投标文档
    /// </summary>
    public interface ProjectBidDocumentIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_BidDocumentEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_BidDocument表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_BidDocumentEntity GetProject_BidDocumentEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_BidDocumentEntity entity,string deleteList,string type);

        /// <summary>
        /// 合并上传附件的分片数据
        /// </summary>
        /// <param name="projectId">项目ID</param>
        /// <param name="fileGuid">文件主键</param>
        /// <param name="fileName">文件名</param>
        /// <param name="chunks">文件总分片数</param>
        /// <param name="userInfo">用户信息</param>
        /// <returns></returns>
        bool SaveAnnexes(string projectId, string fileGuid, string fileName, int chunks, UserInfo userInfo);
        #endregion

    }
}
