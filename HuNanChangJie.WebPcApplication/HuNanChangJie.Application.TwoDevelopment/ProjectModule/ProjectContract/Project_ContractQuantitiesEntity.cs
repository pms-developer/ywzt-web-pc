﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 14:21
    /// 描 述：工程合同
    /// </summary>
    public class Project_ContractQuantitiesEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }

        /// <summary>
        /// 完工决算ID
        /// </summary>
        public string ProjectFinalId { get; set; }
        /// <summary>
        /// 合同ID
        /// </summary>
        [Column("PROJECTCONTRACTID")]
        public string ProjectContractId { get; set; }
        /// <summary>
        /// 清单编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 清单名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 项目特征
        /// </summary>
        [Column("FEATURE")]
        public string Feature { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 工程量
        /// </summary>
        [Column("QUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantities { get; set; } = 0;
        /// <summary>
        /// 综合单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; } = 0;
        /// <summary>
        /// 合价
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; } = 0;
        /// <summary>
        /// 累计变更工程量
        /// </summary>
        [Column("CHANGEQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangeQuantities { get; set; } = 0;
        /// <summary>
        /// 累计变更综合单价
        /// </summary>
        [Column("CHANGEPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangePrice { get; set; } = 0;
        /// <summary>
        /// 累计变更合价
        /// </summary>
        [Column("CHANGETOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangeTotalPrice { get; set; } = 0;
        /// <summary>
        /// 累计签证工程量
        /// </summary>
        [Column("VISAQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? VisaQuantities { get; set; } = 0;
        /// <summary>
        /// 累计签证综合单价
        /// </summary>
        [Column("VISAPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? VisaPrice { get; set; } = 0;
        /// <summary>
        /// 累计签证合价
        /// </summary>
        [Column("VISATOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? VisaTotalPrice { get; set; } = 0;
        /// <summary>
        /// 累计结算合价
        /// </summary>
        [Column("SETTLEMENTTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementTotalPrice { get; set; } = 0;
        /// <summary>
        /// 累计结算工程量
        /// </summary>
        [Column("SETTLEMENTQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementQuantities { get; set; } = 0;
        /// <summary>
        /// 累计结算综合单价
        /// </summary>
        [Column("SETTLEMENTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementPrice { get; set; } = 0;
        /// <summary>
        /// 当前总工程量
        /// </summary>
        [Column("CURRENTQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? CurrentQuantities { get; set; } = 0;
        /// <summary>
        /// 当前总综合单价
        /// </summary>
        [Column("CURRENTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? CurrentPrice { get; set; } = 0;
        /// <summary>
        /// 当前总合价
        /// </summary>
        [Column("CURRENTTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? CurrentTotalPrice { get; set; } = 0;
        /// <summary>
        /// 完工决算合价
        /// </summary>
        [Column("FINALTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? FinalTotalPrice { get; set; } = 0;
        /// <summary>
        /// 完工决算工程量
        /// </summary>
        [Column("FINALQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? FinalQuantities { get; set; } = 0;
        /// <summary>
        /// 完工决算综合单价
        /// </summary>
        [Column("FINALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? FinalPrice { get; set; } = 0;
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        public int? SortCode { get; set; } = 0;
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            CurrentPrice = Price;
            CurrentQuantities = Quantities;
            CurrentTotalPrice = TotalPrice;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            CurrentPrice = Price;
            CurrentQuantities = Quantities;
            CurrentTotalPrice = TotalPrice;
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            CurrentPrice = Price;
            CurrentQuantities = Quantities;
            CurrentTotalPrice = TotalPrice;
            this.ID = keyValue;
        }

        /// <summary>
        /// 设计当前合同单价、当前合同总价、当前合同数量
        /// </summary>
        public void SetCurrentInfo()
        {
            //当前量多合同初始量+累计变更量+累计签证量
            CurrentQuantities = (Quantities ?? 0) + (ChangeQuantities ?? 0) + (VisaQuantities ?? 0);

            //当前量多合同初始价+累计变更价+累计签证价
            CurrentTotalPrice = (TotalPrice ?? 0) + (ChangeTotalPrice ?? 0) + (VisaTotalPrice ?? 0);

            //当前单价 当前总价/当前量
            CurrentPrice = CurrentTotalPrice / CurrentQuantities;
        }

        #endregion
    }
}

