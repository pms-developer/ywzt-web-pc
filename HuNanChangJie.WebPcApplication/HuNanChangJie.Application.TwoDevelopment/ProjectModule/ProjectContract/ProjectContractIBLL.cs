﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 14:21
    /// 描 述：工程合同
    /// </summary>
    public interface ProjectContractIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_ContractEntity> GetPageList(XqPagination pagination, string queryJson);

        /// <summary>
        /// 获取合同工程量清单
        /// </summary>
        /// <param name="paginationobj"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_ContractQuantitiesEntity> GetQuantitiesList(XqPagination paginationobj, string queryJson);

        /// <summary>
        /// 获取合同材料清单
        /// </summary>
        /// <param name="paginationobj"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_ContractMaterialsEntity> GetMaterialsList(XqPagination paginationobj, string queryJson);

        /// <summary>
        /// 获取Project_ContractAgreement表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_ContractAgreementEntity> GetProject_ContractAgreementList(string keyValue);
        /// <summary>
        /// 获取Project_ContractQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_ContractQuantitiesEntity> GetProject_ContractQuantitiesList(string keyValue);
        /// <summary>
        /// 获取Project_ContractMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_ContractMaterialsEntity> GetProject_ContractMaterialsList(string keyValue);
        /// <summary>
        /// 获取Project_Contract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ContractEntity GetProject_ContractEntity(string keyValue);
        /// <summary>
        /// 获取Project_ContractAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ContractAgreementEntity GetProject_ContractAgreementEntity(string keyValue);
        IEnumerable<Project_ContractEntity> GetWaitFinalList(string projectId);
        string GetCustomerName(string projectId);

        /// <summary>
        /// 获取Project_ContractMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ContractMaterialsEntity GetProject_ContractMaterialsEntity(string keyValue);
        IEnumerable<Project_ContractEntity> GetList(string projectId);

        /// <summary>
        /// 获取Project_ContractQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_ContractQuantitiesEntity GetProject_ContractQuantitiesEntity(string keyValue);

        Project_ContractEntity GetFormData(string keyValue);
        #endregion

        #region  提交数据
        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_ContractEntity entity,List<Project_ContractAgreementEntity> project_ContractAgreementList,List<Project_ContractMaterialsEntity> project_ContractMaterialsList,List<Project_ContractQuantitiesEntity> project_ContractQuantitiesList,string deleteList,string type);
        Project_ContractEntity GetContractInfo(string projectId);

        /// <summary>
        /// 获取指定项目工程合同（审核通过后）的初始签定金额
        /// </summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        decimal GetInitAmountTotals(string projectId);

        #endregion

    }
}
