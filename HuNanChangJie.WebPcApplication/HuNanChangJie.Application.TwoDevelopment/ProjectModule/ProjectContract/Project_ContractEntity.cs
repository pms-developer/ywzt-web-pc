﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 14:21
    /// 描 述：工程合同
    /// </summary>
    public class Project_ContractEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 罚款金额
        /// </summary>
        [Column("FINEEXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? FineExpend { get; set; }

        /// <summary>
        /// 其它收入
        /// </summary>
        [Column("OTHERINCOME")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherIncome { get; set; }
        /// <summary>
        /// 其它支出
        /// </summary>
        [Column("OTHEREXPEND")]
        [DecimalPrecision(18, 4)]
        public decimal? OtherExpend { get; set; }
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 签订日期
        /// </summary>
        [Column("SIGNDATE")]
        public DateTime? SignDate { get; set; }
        /// <summary>
        /// 合同类型
        /// </summary>
        [Column("CONTRACTTYPE")]
        public string ContractType { get; set; }
        /// <summary>
        /// 合同编号
        /// </summary>
        //[Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 外部合同编号
        /// </summary>
        [Column("EXTERNALCODE")]
        public string ExternalCode { get; set; }
        /// <summary>
        /// 合同名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 甲方
        /// </summary>
        [Column("JIA")]
        public string Jia { get; set; }
        /// <summary>
        /// 乙方
        /// </summary>
        [Column("YI")]
        public string Yi { get; set; }
        /// <summary>
        /// 计价方式
        /// </summary>
        [Column("PRICETYPE")]
        public string PriceType { get; set; }
        /// <summary>
        /// 合同签订金额/合同暂估金额
        /// </summary>
        [Column("INITAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InitAmount { get; set; } = 0;
        /// <summary>
        /// 累计变更金额
        /// </summary>
        [Column("CHANGEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? ChangeAmount { get; set; } = 0;
        /// <summary>
        /// 累计签证金额
        /// </summary>
        [Column("VISAAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? VisaAmount { get; set; } = 0;
        /// <summary>
        /// 合同总金额
        /// </summary>
        [Column("TOTALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalAmount { get; set; } = 0;
        /// <summary>
        /// 税额
        /// </summary>
        [Column("TAXAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxAmount { get; set; } = 0;
        /// <summary>
        /// 货币单位
        /// </summary>
        [Column("MONETARYUNIT")]
        public string MonetaryUnit { get; set; }
        /// <summary>
        /// 发票类型
        /// </summary>
        [Column("INVOICETYPE")]
        public string InvoiceType { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        [Column("STARTDATE")]
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        [Column("ENDDATE")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 履约地
        /// </summary>
        [Column("ADDRESS")]
        public string Address { get; set; }
        /// <summary>
        /// 项目模式
        /// </summary>
        [Column("PROJECTMODE")]
        public string ProjectMode { get; set; }
        /// <summary>
        /// 保修期
        /// </summary>
        [Column("WARRANTY")]
        public string Warranty { get; set; } 
        /// <summary>
        /// 履约保证金
        /// </summary>
        [Column("DEPOSIT")]
        [DecimalPrecision(18, 4)]
        public decimal? Deposit { get; set; } = 0;
        /// <summary>
        /// 质保金
        /// </summary>
        [Column("QUALITYDEPOSIT")]
        [DecimalPrecision(18, 4)]
        public decimal? QualityDeposit { get; set; } = 0;
        /// <summary>
        /// 合同让利
        /// </summary>
        [Column("SALE")]
        [DecimalPrecision(18, 4)]
        public decimal? Sale { get; set; } = 0;
        /// <summary>
        /// 销售人员
        /// </summary>
        [Column("SALEUSERID")]
        public string SaleUserId { get; set; }
        /// <summary>
        /// 项目经理
        /// </summary>
        [Column("MANAGEMENTID")]
        public string ManagementId { get; set; }
        /// <summary>
        /// 开票金额
        /// </summary>
        [Column("INVIOCEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? InvioceAmount { get; set; } = 0;
        /// <summary>
        /// 未开票金额
        /// </summary>
        [Column("NOINVIOCEAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? NoInvioceAmount { get; set; } = 0;
        /// <summary>
        /// 结算金额
        /// </summary>
        [Column("SETTLEMENTAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? SettlementAmount { get; set; } = 0;
        /// <summary>
        /// 完工决算金额
        /// </summary> 
        [Column("FINALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? FinalAmount { get; set; } = 0;
        /// <summary>
        /// 已收款金额
        /// </summary>
        [Column("RECEIVEDAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? ReceivedAmount { get; set; } = 0;
        /// <summary>
        /// 合同说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; } = true;
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }


        /// <summary>
        /// 是否签协议
        /// </summary>
        [Column("ISSIGNAGREEMENT")]
        public int? IsSignAgreement { get; set; }


        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }

        /// <summary>
        /// 是否已完工决算 
        /// </summary>
        public bool? IsFinal { get; set; }

        /// <summary>
        /// 约定付款比例
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? YdfkBiLi { get; set; }

        /// <summary>
        /// 签约单位
        /// </summary>
        public string QyDanWei { get; set; }

        /// <summary>
        /// 约定应收账款  约定应收账款=累计结算金额*约定付款比例-累计收款金额
        /// </summary>
        [NotMapped]
        public decimal? YdysKuan
        {
            get
            {
                if (YdfkBiLi == null) return null;
                if (YdfkBiLi >= 0) return null;
                return (SettlementAmount ?? 0) * YdfkBiLi / 100 - (ReceivedAmount ?? 0);
            }
        }


        /// <summary>
        /// 合同工期
        /// </summary>
        [NotMapped]
        public int ContractDuration
        {
            get
            {
                if (StartDate == null) return 0;
                if (EndDate == null) return 0;
                return Time.DiffDays(StartDate.ToDate(), EndDate.ToDate());
            }
        }

        /// <summary>
        /// 总合同工期
        /// </summary>
        [NotMapped]
        public int TotalDuration
        {
            get;set;
        }

        /// <summary>
        /// 客户名称（甲方）
        /// </summary>
        [NotMapped]
        public string CustomerName { get; set; }

        /// <summary>
        /// 项目经理
        /// </summary>
        [NotMapped]
        public string F_RealName { get; set; }

        /// <summary>
        /// 乙方名称
        /// </summary>
        [NotMapped]
        public string YiName { get; set; }

        /// <summary>
        /// 合同类型名称
        /// </summary>
        [NotMapped]
        public string ContractTypeName { get; set; }

        /// <summary>
        /// 发票类型名称
        /// </summary>
        [NotMapped]
        public string InvoiceTypeName { get; set; }


        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            TotalAmount = InitAmount;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            TotalAmount = InitAmount;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            TotalAmount = InitAmount;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

