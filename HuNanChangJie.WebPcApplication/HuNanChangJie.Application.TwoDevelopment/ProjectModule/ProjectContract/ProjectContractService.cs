﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 14:21
    /// 描 述：工程合同
    /// </summary>
    public class ProjectContractService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ContractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT "); //    t.TotalAmount,
                strSql.Append(@"bs.F_RealName,bc.FullName,bc.FullName as CustomerName,
                t.ID,
                t.Code,
                t.Name,
                t.Jia,
                t.Yi,
                t.InitAmount,
                t.SignDate,
                t.ContractType,
                t.ManagementId,
                t.ExternalCode,
                t.InvoiceType,
(t.VisaAmount+t.TotalAmount) as TotalAmount,
                t.ChangeAmount,
                t.VisaAmount,
                t.InvioceAmount,
                t.SettlementAmount,
                t.ReceivedAmount,
                a.FinalAmount,
                t.AuditStatus,
                t.AuditorName,
                t.Workflow_ID,
                t.IsFinal,t.CreationDate,
                t.Auditor_ID,t.ProjectID,t.ProjectName,
                t.YdfkBiLi,t.QyDanWei,
                comp.F_FullName as YiName,
                contractTypeDb.F_ItemName as ContractTypeName,
                t.InvoiceType as InvoiceTypeName
                ");
                strSql.Append("  FROM Project_Contract t  left join  Project_Final as a on a.ProjectContractId = t.ID");
                strSql.Append(" left join Base_User bs on t.ManagementId = bs.F_UserId");

                strSql.Append(" left join Base_Customer bc on t.Jia = bc.ID");
                strSql.Append(" left join Base_Company comp on t.Yi = comp.F_CompanyId");
                strSql.Append(" left join Base_DataItemDetail contractTypeDb on t.ContractType = contractTypeDb.F_ItemValue");
                //strSql.Append(" left join Base_DataItemDetail InvoiceTypeDb on t.InvoiceType = InvoiceTypeDb.F_ItemValue");

                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID=@ProjectId");
                }

                if (!queryParam["ProjectName"].IsEmpty())
                {
                    dp.Add("ProjectName", "%" + queryParam["ProjectName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.ProjectName Like @ProjectName");
                }

                if (!queryParam["F_RealName"].IsEmpty())
                {
                    dp.Add("F_RealName", "%" + queryParam["F_RealName"].ToString() + "%", DbType.String);
                    strSql.Append(" AND bs.F_RealName Like @F_RealName");
                }

                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Name Like @Name ");
                }
                 
                if (!queryParam["Jia"].IsEmpty())
                {
                    dp.Add("Jia", "%" + queryParam["Jia"].ToString() + "%", DbType.String);
                    strSql.Append(" AND bc.FullName Like @Jia ");
                }

                if (!queryParam["Yi"].IsEmpty())
                {
                    dp.Add("Yi", "%" + queryParam["Yi"].ToString() + "%", DbType.String);
                    strSql.Append(" AND comp.F_FullName Like @Yi ");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }
                if (pagination != null && string.IsNullOrEmpty(pagination.sidx))
                {
                    pagination.sidx = "CreationDate";
                    pagination.sord = "DESC";
                }
                var list = this.BaseRepository().FindList<Project_ContractEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractQuantitiesEntity> GetQuantitiesList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * ");
                strSql.Append("  FROM Project_ContractQuantities t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID=@ProjectID");
                }
                if (!queryParam["ContractId"].IsEmpty())
                {
                    dp.Add("ContractId", queryParam["ContractId"].ToString(), DbType.String);
                    strSql.Append(" And t.ProjectContractId=@ContractId");
                }
                if (!queryParam["Name"].IsEmpty())
                {
                    dp.Add("Name", "%" + queryParam["Name"].ToString() + "%", DbType.String);
                    strSql.Append("    AND  name like @Name");
                }
                var list = this.BaseRepository().FindList<Project_ContractQuantitiesEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractMaterialsEntity> GetProject_ContractMaterialsLists(IEnumerable<string> materialsIdList)
        {
            try
            {
                return BaseRepository().FindList<Project_ContractMaterialsEntity>(i => materialsIdList.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractMaterialsEntity> GetMaterialsList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT * ");
                strSql.Append("  FROM Project_ContractMaterials t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID=@ProjectID");
                }
                if (!queryParam["ContractId"].IsEmpty())
                {
                    dp.Add("ContractId", queryParam["ContractId"].ToString(), DbType.String);
                    strSql.Append(" And ProjectContractId=@ContractId");
                }
                var list = this.BaseRepository().FindList<Project_ContractMaterialsEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractQuantitiesEntity> GetProject_ContractQuantitiesLists(IEnumerable<string> quantitiesIdList)
        {
            try
            {
                return BaseRepository().FindList<Project_ContractQuantitiesEntity>(i => quantitiesIdList.Contains(i.ID));
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractAgreement表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractAgreementEntity> GetProject_ContractAgreementList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_ContractAgreementEntity>(t => t.ProjectContractId == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractQuantitiesEntity> GetProject_ContractQuantitiesList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_ContractQuantitiesEntity>(t => t.ProjectContractId == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractMaterialsEntity> GetProject_ContractMaterialsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_ContractMaterialsEntity>(t => t.ProjectContractId == keyValue);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Contract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractEntity GetProject_ContractEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractEntity>(i => i.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractAgreementEntity GetProject_ContractAgreementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractAgreementEntity>(t => t.ProjectContractId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractMaterialsEntity GetProject_ContractMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractMaterialsEntity>(t => t.ProjectContractId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 合同变更审核通过后执行的事件
        /// </summary>
        /// <param name="contractModel">合同主表对象</param>
        /// <param name="quantitiesList">合同子表-工程量清单对象</param>
        /// <param name="materialsList">合同子表-材料清单对象</param>
        public void UpdateContract(Project_ContractEntity contractModel, List<Project_ContractQuantitiesEntity> quantitiesList, List<Project_ContractMaterialsEntity> materialsList)
        {
            var db = BaseRepository().BeginTrans();
            try
            {
                db.Update(contractModel);
                foreach (var quantity in quantitiesList)
                {
                    db.Update(quantity);
                }
                foreach (var material in materialsList)
                {
                    db.Update(material);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractEntity> GetWaitFinalList(string projectId)
        {
            try
            {
                return BaseRepository().FindList<Project_ContractEntity>(i => i.ProjectID == projectId && i.AuditStatus == "2" && i.IsFinal != true);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<Project_ContractEntity> GetList(string projectId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(projectId))
                {
                    return BaseRepository().FindList<Project_ContractEntity>(i => i.AuditStatus == "2");
                }
                else
                {
                    return BaseRepository().FindList<Project_ContractEntity>(i => i.ProjectID == projectId && i.AuditStatus == "2");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal string GetCustomerName(string projectId)
        {
            try
            {
                var sql = "select b.FullName from Project_Contract as a left join Base_Customer as b on a.Jia=b.id where a.ProjectId=@projectId";
                dynamic info = BaseRepository().FindList<dynamic>(sql, new { projectId });
                if (info == null || info.Count == 0) return "";
                return info[0].FullName.ToString();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_ContractEntity GetContractInfo(string projectId)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.FullName as CustomerName 
                            from Project_Contract as a 
                            left join Base_Customer as b on a.Jia=b.ID ");
                sb.Append(" where a.ProjectId=@projectId and a.AuditStatus='2' ");

                var list = BaseRepository().FindList<Project_ContractEntity>(sb.ToString(), new { projectId });
                var info = new Project_ContractEntity();
                if (list.Count() == 0) return info;
                foreach (var item in list)
                {
                    info.InitAmount = (info.InitAmount ?? 0) + (item.InitAmount ?? 0);
                    info.SettlementAmount = (info.SettlementAmount ?? 0) + (item.SettlementAmount ?? 0);
                    info.InvioceAmount = (info.InvioceAmount ?? 0) + (item.InvioceAmount ?? 0);
                    info.ReceivedAmount = (info.ReceivedAmount ?? 0) + (item.ReceivedAmount ?? 0);
                    info.TotalDuration += item.ContractDuration;
                    info.CustomerName = item.CustomerName;
                }
                return info;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取指定项目工程合同（审核通过后）的初始签定金额
        /// </summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        public decimal GetInitAmountTotals(string projectId)
        {

            try
            {
                var list = BaseRepository().FindList<Project_ContractEntity>(i => i.ProjectID == projectId && i.AuditStatus == ((int)AuditStatus.Pass).ToString());
                var totals = list.Select(i => i.InitAmount).Sum();
                return totals ?? 0;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_ContractEntity GetFormData(string keyValue)
        {
            try
            {
                return BaseRepository().FindEntity<Project_ContractEntity>(i => i.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractQuantitiesEntity GetProject_ContractQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractQuantitiesEntity>(t => t.ProjectContractId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Project_ContractEntity project_ContractEntity = db.FindEntity<Project_ContractEntity>(keyValue);
                if (project_ContractEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                project_ContractEntity.AuditStatus = "4";
                db.Update<Project_ContractEntity>(project_ContractEntity);


                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };

            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Project_ContractEntity project_ContractEntity = db.FindEntity<Project_ContractEntity>(keyValue);
                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (project_ContractEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                project_ContractEntity.AuditStatus = "2";
                db.Update<Project_ContractEntity>(project_ContractEntity);

                ProjectEntity projectEntity = db.FindEntity<ProjectEntity>(project_ContractEntity.ProjectID);

                var configInfo = LoadSystemConfig.SystemConfigs.FirstOrDefault(i => i.Name == "fundsync-GenerateAccount");
                if (configInfo != null)
                {
                    var flag = configInfo.Value;

                    if (flag)
                    {
                        Base_FundAccountEntity base_FundAccountEntity = db.FindEntity<Base_FundAccountEntity>(m => m.ProjectId == project_ContractEntity.ProjectID);

                        if (base_FundAccountEntity == null)
                        {
                            base_FundAccountEntity = new Base_FundAccountEntity()
                            {
                                ID = Guid.NewGuid().ToString(),
                                Name = $"可用资金({projectEntity.ProjectName})",
                                CompanyId = project_ContractEntity.Yi,
                                Bank = "系统生成虚拟账户",
                                BankCode = $"可用资金({projectEntity.ProjectName})",
                                Holder = projectEntity.ProjectName,
                                ProjectId = project_ContractEntity.ProjectID,
                                IsVirtual = true
                            };
                            db.Insert(base_FundAccountEntity);
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();
                return new Util.Common.OperateResultEntity() { Success = true };

            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_ContractEntity = GetProject_ContractEntity(keyValue);
                db.Delete<Project_ContractEntity>(t => t.ID == keyValue);
                db.Delete<Project_ContractAgreementEntity>(t => t.ProjectContractId == project_ContractEntity.ID);
                db.Delete<Project_ContractMaterialsEntity>(t => t.ProjectContractId == project_ContractEntity.ID);
                db.Delete<Project_ContractQuantitiesEntity>(t => t.ProjectContractId == project_ContractEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_ContractEntity entity, List<Project_ContractAgreementEntity> project_ContractAgreementList, List<Project_ContractMaterialsEntity> project_ContractMaterialsList, List<Project_ContractQuantitiesEntity> project_ContractQuantitiesList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var project_ContractEntityTmp = GetProject_ContractEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码 
                    var Project_ContractAgreementUpdateList = project_ContractAgreementList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ContractAgreementUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_ContractAgreementInserList = project_ContractAgreementList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_ContractAgreementInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectContractId = project_ContractEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Project_ContractMaterialsUpdateList = project_ContractMaterialsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ContractMaterialsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_ContractMaterialsInserList = project_ContractMaterialsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_ContractMaterialsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectContractId = project_ContractEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Project_ContractQuantitiesUpdateList = project_ContractQuantitiesList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ContractQuantitiesUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_ContractQuantitiesInserList = project_ContractQuantitiesList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_ContractQuantitiesInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectContractId = project_ContractEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_ContractAgreementEntity item in project_ContractAgreementList)
                    {
                        item.Create(item.ID);
                        item.ProjectContractId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_ContractMaterialsEntity item in project_ContractMaterialsList)
                    {
                        item.Create(item.ID);
                        item.ProjectContractId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_ContractQuantitiesEntity item in project_ContractQuantitiesList)
                    {
                        item.Create(item.ID);
                        item.ProjectContractId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
