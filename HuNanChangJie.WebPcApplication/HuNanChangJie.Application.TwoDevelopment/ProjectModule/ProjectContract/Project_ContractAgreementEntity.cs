﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 14:21
    /// 描 述：工程合同
    /// </summary>
    public class Project_ContractAgreementEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 合同主表ID
        /// </summary>
        [Column("PROJECTCONTRACTID")]
        public string ProjectContractId { get; set; }
        /// <summary>
        /// 款项性质
        /// </summary>
        [Column("FUNDTYPE")]
        public string FundType { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        [Column("PAYMENTTYPE")]
        public string PaymentType { get; set; }
        /// <summary>
        /// 收款条件
        /// </summary>
        [Column("PROCEEDSCONDITION")]
        public string ProceedsCondition { get; set; }
        /// <summary>
        /// 支付比例
        /// </summary>
        [Column("PAYMENTPERCENT")]
        [DecimalPrecision(18, 4)]
        public decimal? PaymentPercent { get; set; }
        /// <summary>
        /// 协议收款金额
        /// </summary>
        [Column("AMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? Amount { get; set; }
        /// <summary>
        /// 实际收款金额
        /// </summary>
        [Column("PRACTICALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? PracticalAmount { get; set; }
        /// <summary>
        /// 预计收款时间
        /// </summary>
        [Column("PREDICTDATE")]
        public DateTime? PredictDate { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

