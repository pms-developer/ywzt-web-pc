﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-19 14:21
    /// 描 述：工程合同
    /// </summary>
    public class ProjectContractBLL : ProjectContractIBLL
    {
        private ProjectContractService projectContractService = new ProjectContractService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ContractEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectContractService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取合同工程量清单数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ContractQuantitiesEntity> GetQuantitiesList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectContractService.GetQuantitiesList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取合同工程量清单数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_ContractMaterialsEntity> GetMaterialsList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectContractService.GetMaterialsList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractMaterialsEntity> GetProject_ContractMaterialsLists(IEnumerable<string> materialsIdList)
        {
            try
            {
                return projectContractService.GetProject_ContractMaterialsLists(materialsIdList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractAgreement表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractAgreementEntity> GetProject_ContractAgreementList(string keyValue)
        {
            try
            {
                return projectContractService.GetProject_ContractAgreementList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractQuantitiesEntity> GetProject_ContractQuantitiesList(string keyValue)
        {
            try
            {
                return projectContractService.GetProject_ContractQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractQuantitiesEntity> GetProject_ContractQuantitiesLists(IEnumerable<string> quantitiesIdList)
        {
            try
            {
                return projectContractService.GetProject_ContractQuantitiesLists(quantitiesIdList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_ContractMaterialsEntity> GetProject_ContractMaterialsList(string keyValue)
        {
            try
            {
                return projectContractService.GetProject_ContractMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Contract表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractEntity GetProject_ContractEntity(string keyValue)
        {
            try
            {
                return projectContractService.GetProject_ContractEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractAgreement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractAgreementEntity GetProject_ContractAgreementEntity(string keyValue)
        {
            try
            {
                return projectContractService.GetProject_ContractAgreementEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractMaterialsEntity GetProject_ContractMaterialsEntity(string keyValue)
        {
            try
            {
                return projectContractService.GetProject_ContractMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_ContractQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_ContractQuantitiesEntity GetProject_ContractQuantitiesEntity(string keyValue)
        {
            try
            {
                return projectContractService.GetProject_ContractQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_ContractEntity GetFormData(string keyValue)
        {
            try
            {
                return projectContractService.GetFormData(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectContractService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
               return projectContractService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return projectContractService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_ContractEntity entity,List<Project_ContractAgreementEntity> project_ContractAgreementList,List<Project_ContractMaterialsEntity> project_ContractMaterialsList,List<Project_ContractQuantitiesEntity> project_ContractQuantitiesList,string deleteList,string type)
        {
            try
            {
                projectContractService.SaveEntity(keyValue, entity,project_ContractAgreementList,project_ContractMaterialsList,project_ContractQuantitiesList,deleteList,type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("ContractCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 合同变更审核通过后执行的事件
        /// </summary>
        /// <param name="contractModel">合同主表对象</param>
        /// <param name="quantitiesList">合同子表-工程量清单对象</param>
        /// <param name="materialsList">合同子表-材料清单对象</param>
        public void UpdateContract(Project_ContractEntity contractModel, List<Project_ContractQuantitiesEntity> quantitiesList, List<Project_ContractMaterialsEntity> materialsList)
        {
            try
            {
                projectContractService.UpdateContract(contractModel, quantitiesList, materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取指定项目工程合同（审核通过后）的初始签定金额
        /// </summary>
        /// <param name="projectId">项目ID</param>
        /// <returns></returns>
        public decimal GetInitAmountTotals(string projectId)
        {
            try
            {
                return projectContractService.GetInitAmountTotals(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_ContractEntity GetContractInfo(string projectId)
        {
            try
            {
                return projectContractService.GetContractInfo(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractEntity> GetWaitFinalList(string projectId)
        {
            try
            {
                return projectContractService.GetWaitFinalList(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public string GetCustomerName(string projectId)
        {
            try
            {
                return projectContractService.GetCustomerName(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_ContractEntity> GetList(string projectId)
        {
            try
            {
                return projectContractService.GetList(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
