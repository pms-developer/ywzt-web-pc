﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 10:56
    /// 描 述：工程签证
    /// </summary>
    public class Project_VisaMaterialsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目编码
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 工程合同材料清单合同
        /// </summary>
        [Column("ProjectContractMaterialsId")]
        public string ProjectContractMaterialsId { get; set; }
        /// <summary>
        /// 合同签证ID
        /// </summary>
        [Column("PROJECTVISAID")]
        public string ProjectVisaId { get; set; }
        /// <summary>
        /// 清单编码
        /// </summary>
        [Column("LISTCODE")]
        public string ListCode { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 本次变更数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; } = 0;
        /// <summary>
        /// 本次变更单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; } = 0;
        /// <summary>
        /// 本次变更合计金额
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; } = 0;
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATA")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; } = 0;

        /// <summary>
        /// 材料取值类型
        /// </summary>
        [Column("MaterialsType")]
        public bool? MaterialsType { get; set; }

        /// <summary>
        /// 合同初始单价
        /// </summary>
        [NotMapped]
        public decimal? InitPrice { get; set; } = 0;

        /// <summary>
        /// 合同初始数量
        /// </summary>
        [NotMapped]
        public decimal? InitQuantity { get; set; } = 0;

        /// <summary>
        /// 合同初始合计金额
        /// </summary>
        [NotMapped]
        public decimal? InitTotal { get; set; } = 0;
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            CreationDate = DateTime.Now;
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            CreationDate = DateTime.Now;
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

