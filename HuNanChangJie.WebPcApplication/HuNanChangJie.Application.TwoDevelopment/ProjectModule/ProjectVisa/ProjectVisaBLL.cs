﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 10:56
    /// 描 述：工程签证
    /// </summary>
    public class ProjectVisaBLL : ProjectVisaIBLL
    {
        private ProjectVisaService projectVisaService = new ProjectVisaService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_VisaEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectVisaService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_VisaQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_VisaQuantitiesEntity> GetProject_VisaQuantitiesList(string keyValue)
        {
            try
            {
                return projectVisaService.GetProject_VisaQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_VisaQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_VisaQuantitiesEntity> GetProject_VisaQuantitiesListsql(string keyValue)
        {
            try
            {
                return projectVisaService.GetProject_VisaQuantitiesListsql(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Project_VisaMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_VisaMaterialsEntity> GetProject_VisaMaterialsList(string keyValue)
        {
            try
            {
                return projectVisaService.GetProject_VisaMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Visa表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_VisaEntity GetProject_VisaEntity(string keyValue)
        {
            try
            {
                return projectVisaService.GetProject_VisaEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_VisaMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_VisaMaterialsEntity GetProject_VisaMaterialsEntity(string keyValue)
        {
            try
            {
                return projectVisaService.GetProject_VisaMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_VisaQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_VisaQuantitiesEntity GetProject_VisaQuantitiesEntity(string keyValue)
        {
            try
            {
                return projectVisaService.GetProject_VisaQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectVisaService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_VisaEntity entity,List<Project_VisaMaterialsEntity> project_VisaMaterialsList,List<Project_VisaQuantitiesEntity> project_VisaQuantitiesList,string deleteList,string type)
        {
            try
            {
                projectVisaService.SaveEntity(keyValue, entity,project_VisaMaterialsList,project_VisaQuantitiesList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 审核通过后事件
        /// </summary>
        /// <param name="keyValue"></param>
        public void Audit(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            try
            {
                var quantitiesList = new List<Project_ContractQuantitiesEntity>();
                var materialsList = new List<Project_ContractMaterialsEntity>();

                var visaInfo = projectVisaService.GetProject_VisaEntity(keyValue);
                var contractId = visaInfo.ContractId;

                var contractModel = contractBll.GetProject_ContractEntity(contractId);
                contractModel.VisaAmount = (contractModel.VisaAmount ?? 0) + visaInfo.VisaPrice;

                var visaQuantities = projectVisaService.GetProject_VisaQuantitiesList(keyValue);
                if (visaQuantities.Any())
                {
                    var quantities = contractBll.GetProject_ContractQuantitiesList(contractId);
                    foreach (var item in visaQuantities)
                    {
                        var info = quantities.FirstOrDefault(i => i.ID == item.ProjectContractQuantitiesId);
                        if (info == null) continue;

                        info.VisaQuantities = (info.VisaQuantities ?? 0) + item.Quantities;
                        info.VisaTotalPrice = (info.VisaTotalPrice ?? 0) + item.TotalPrice;

                        if (info.VisaTotalPrice != 0)
                        {
                            info.VisaPrice = info.VisaTotalPrice / info.VisaQuantities;
                        }
                        else
                        {
                            info.VisaPrice = 0;
                        }

                        quantitiesList.Add(info);
                    }
                }

                var visaMaterials = projectVisaService.GetProject_VisaMaterialsList(keyValue);
                if (visaMaterials.Any())
                {
                    var materials = contractBll.GetProject_ContractMaterialsList(contractId);
                    foreach (var item in visaMaterials)
                    {
                        var info = materials.FirstOrDefault(i => i.ID == item.ProjectContractMaterialsId);
                        if (info == null) continue;
                        info.VisaQuantity = (info.VisaQuantity ?? 0) + item.Quantity;
                        info.VisaTotalPrice = (info.VisaTotalPrice ?? 0) + item.TotalPrice;

                        if (info.VisaTotalPrice != 0)
                        {
                            info.VisaPrice = info.VisaTotalPrice / info.VisaQuantity;
                        }
                        else
                        {
                            info.VisaPrice = 0;
                        }

                        materialsList.Add(info);
                    }
                }
                contractBll.UpdateContract(contractModel, quantitiesList, materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 反审核后事件
        /// </summary>
        /// <param name="keyValue"></param>
        public void UnAudit(string keyValue)
        {
            var contractBll = new ProjectContractBLL();
            try
            {
                var quantitiesList = new List<Project_ContractQuantitiesEntity>();
                var materialsList = new List<Project_ContractMaterialsEntity>();

                var visaInfo = projectVisaService.GetProject_VisaEntity(keyValue);
                var contractId = visaInfo.ContractId;

                var contractModel = contractBll.GetProject_ContractEntity(contractId);
                contractModel.VisaAmount = (contractModel.VisaAmount ?? 0) - visaInfo.VisaPrice;

                var visaQuantities = projectVisaService.GetProject_VisaQuantitiesList(keyValue);
                if (visaQuantities.Any())
                {
                    var quantities = contractBll.GetProject_ContractQuantitiesList(contractId);
                    foreach (var item in visaQuantities)
                    {
                        var info = quantities.FirstOrDefault(i => i.ID == item.ProjectContractQuantitiesId);
                        if (info == null) continue;

                        info.VisaQuantities = (info.VisaQuantities ?? 0) - item.Quantities;
                        info.VisaTotalPrice = (info.VisaTotalPrice ?? 0) - item.TotalPrice;

                        if (info.VisaTotalPrice != 0)
                        {
                            info.VisaPrice = info.VisaTotalPrice / info.VisaQuantities;
                        }
                        else
                        {
                            info.VisaPrice = 0;
                        }

                        quantitiesList.Add(info);
                    }
                }

                var visaMaterials = projectVisaService.GetProject_VisaMaterialsList(keyValue);
                if (visaMaterials.Any())
                {
                    var materials = contractBll.GetProject_ContractMaterialsList(contractId);
                    foreach (var item in visaMaterials)
                    {
                        var info = materials.FirstOrDefault(i => i.ID == item.ProjectContractMaterialsId);
                        if (info == null) continue;
                        info.VisaQuantity = (info.VisaQuantity ?? 0) - item.Quantity;
                        info.VisaTotalPrice = (info.VisaTotalPrice ?? 0) - item.TotalPrice;

                        if (info.VisaTotalPrice != 0)
                        {
                            info.VisaPrice = info.VisaTotalPrice / info.VisaQuantity;
                        }
                        else
                        {
                            info.VisaPrice = 0;
                        }

                        materialsList.Add(info);
                    }
                }
                contractBll.UpdateContract(contractModel, quantitiesList, materialsList);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
