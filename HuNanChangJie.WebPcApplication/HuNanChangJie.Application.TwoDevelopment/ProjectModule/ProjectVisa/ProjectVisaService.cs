﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 10:56
    /// 描 述：工程签证
    /// </summary>
    public class ProjectVisaService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_VisaEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.SortCode,
                t.ContractId,
                t.CompanyCode,
                t.VisaType,
                t.VisaPrice,
                t.VisaDate,
                t.OperatorId,
                t.ProjectID,
                t.OperatorDepartmentId,
                t.Abstract,
                t.Remark,
                t.ProjectName,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID
                ");
                strSql.Append("  FROM Project_Visa t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" and ProjectID=@ProjectID ");
                }
                var list=this.BaseRepository().FindList<Project_VisaEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_VisaQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_VisaQuantitiesEntity> GetProject_VisaQuantitiesList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Project_VisaQuantitiesEntity>(t=>t.ProjectVisaId == keyValue );
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_VisaQuantitiesEntity> GetProject_VisaQuantitiesListsql(string keyValue)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.ProjectId,
                t.ProjectContractQuantitiesId,
                t.ProjectVisaId,
                t.Code,
                t.Name,
                t.Feature,
                t.Unit,
                t.Quantities,
                t.Price,
                t.TotalPrice,
                t.CreationDate,
                t.Remark,
                t.SortCode,
                b.Price as InitPrice,
                b.Quantities as InitQuantities,
                b.TotalPrice as InitTotal
                ");
                strSql.Append("  FROM Project_VisaQuantities as t  left join Project_ContractQuantities as b on t.ProjectContractQuantitiesId =b.ID");
                strSql.Append($"  WHERE 1=1 and t.ProjectVisaId='{keyValue}'");
                // 虚拟参数
                
                var list = this.BaseRepository().FindList<Project_VisaQuantitiesEntity>(strSql.ToString());
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }


        /// <summary>
        /// 获取Project_VisaMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_VisaMaterialsEntity> GetProject_VisaMaterialsList(string keyValue)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("select * from ( ");
                strSql.Append(@"
                	select a.* from project_visamaterials a 
                		join Project_ContractMaterials b on a.projectcontractmaterialsid =b.ID 
                	where MaterialsType = 1
                union 
                	select * from project_visamaterials where MaterialsType = 0
                	) v 
                ");
                strSql.Append($" where projectvisaid='{keyValue}'");
                var list= this.BaseRepository().FindList<Project_VisaMaterialsEntity>(strSql.ToString());
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Visa表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_VisaEntity GetProject_VisaEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_VisaEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_VisaMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_VisaMaterialsEntity GetProject_VisaMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_VisaMaterialsEntity>(t=>t.ProjectVisaId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_VisaQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_VisaQuantitiesEntity GetProject_VisaQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_VisaQuantitiesEntity>(t=>t.ProjectVisaId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_VisaEntity = GetProject_VisaEntity(keyValue); 
                db.Delete<Project_VisaEntity>(t=>t.ID == keyValue);
                db.Delete<Project_VisaMaterialsEntity>(t=>t.ProjectVisaId == project_VisaEntity.ID);
                db.Delete<Project_VisaQuantitiesEntity>(t=>t.ProjectVisaId == project_VisaEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_VisaEntity entity,List<Project_VisaMaterialsEntity> project_VisaMaterialsList,List<Project_VisaQuantitiesEntity> project_VisaQuantitiesList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_VisaEntityTmp = GetProject_VisaEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_VisaMaterialsUpdateList= project_VisaMaterialsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_VisaMaterialsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_VisaMaterialsInserList= project_VisaMaterialsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_VisaMaterialsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectVisaId = project_VisaEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                    
                    //没有生成代码 
                    var Project_VisaQuantitiesUpdateList= project_VisaQuantitiesList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_VisaQuantitiesUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_VisaQuantitiesInserList= project_VisaQuantitiesList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_VisaQuantitiesInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectVisaId = project_VisaEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_VisaMaterialsEntity item in project_VisaMaterialsList)
                    {
                        item.ProjectVisaId = entity.ID;
                        item.Create(item.ID);
                        db.Insert(item);
                    }
                    foreach (Project_VisaQuantitiesEntity item in project_VisaQuantitiesList)
                    {
                        item.ProjectVisaId = entity.ID;
                        item.Create(item.ID);
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
