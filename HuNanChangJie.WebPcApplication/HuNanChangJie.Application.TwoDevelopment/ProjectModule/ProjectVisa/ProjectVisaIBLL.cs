﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-24 10:56
    /// 描 述：工程签证
    /// </summary>
    public interface ProjectVisaIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_VisaEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_VisaQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_VisaQuantitiesEntity> GetProject_VisaQuantitiesList(string keyValue);

        /// <summary>
        /// 获取Project_VisaQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_VisaQuantitiesEntity> GetProject_VisaQuantitiesListsql(string keyValue);
        /// <summary>
        /// 获取Project_VisaMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_VisaMaterialsEntity> GetProject_VisaMaterialsList(string keyValue);
        /// <summary>
        /// 获取Project_Visa表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_VisaEntity GetProject_VisaEntity(string keyValue);
        /// <summary>
        /// 获取Project_VisaMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_VisaMaterialsEntity GetProject_VisaMaterialsEntity(string keyValue);
        /// <summary>
        /// 获取Project_VisaQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_VisaQuantitiesEntity GetProject_VisaQuantitiesEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_VisaEntity entity,List<Project_VisaMaterialsEntity> project_VisaMaterialsList,List<Project_VisaQuantitiesEntity> project_VisaQuantitiesList,string deleteList,string type);

        /// <summary>
        /// 审核通过后事件
        /// </summary>
        /// <param name="keyValue"></param>
        void Audit(string keyValue);

        /// <summary>
        /// 反审核后事件
        /// </summary>
        /// <param name="keyValue"></param>
        void UnAudit(string keyValue);
        #endregion

    }
}
