﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-26 17:47
    /// 描 述：分包结算
    /// </summary>
    public class Project_SubcontractSettlementMaterialsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目编码
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 分包结算Id
        /// </summary>
        [Column("PROJECTSUBCONTRACTSETTLEMENTID")]
        public string ProjectSubcontractSettlementId { get; set; }
        /// <summary>
        /// 分包合同材料清单ID
        /// </summary>
        [Column("PROJECTSUBCONTRACTMATERIALSID")]
        public string ProjectSubcontractMaterialsId { get; set; }

        /// <summary>
        /// 结算单价不含税
        /// </summary>
        [Column("AUDITTOTALPRICENOTAX")]
        [DecimalPrecision(18, 4)]
        public decimal? AuditTotalPriceNoTax { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        [Column("TAXRATE")]
        [DecimalPrecision(18, 4)]
        public decimal? TaxRate { get; set; }
        /// <summary>
        /// 结算金额不含税
        /// </summary>
        [Column("TOTALPRICENOTAX")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPriceNoTax { get; set; }
        /// <summary>
        /// 结算金额不含税
        /// </summary>
        [Column("PRICENOTAX")]
        [DecimalPrecision(18, 4)]
        public decimal? PriceNoTax { get; set; }

        /// <summary>
        /// 清单编码
        /// </summary>
        [Column("LISTCODE")]
        public string ListCode { get; set; }
        /// <summary>
        /// 材料编码
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        [Column("BRAND")]
        public string Brand { get; set; }
        /// <summary>
        /// 规格型号
        /// </summary>
        [Column("MODELNUMBER")]
        public string ModelNumber { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Column("QUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantity { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 合计金额
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 形象进度
        /// </summary>
        [Column("VISUALPROGRESS")]
        public string VisualProgress { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 审批数据
        /// </summary>
        [Column("AUDITQUANTITY")]
        [DecimalPrecision(18, 4)]
        public decimal? AuditQuantity { get; set; }
        /// <summary>
        /// 审批数据合计金额
        /// </summary>
        [Column("AUDITTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? AuditTotalPrice { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

