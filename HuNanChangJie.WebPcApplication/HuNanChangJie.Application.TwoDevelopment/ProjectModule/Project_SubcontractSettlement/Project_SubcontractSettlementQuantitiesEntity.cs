﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-26 17:47
    /// 描 述：分包结算
    /// </summary>
    public class Project_SubcontractSettlementQuantitiesEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 分包结算ID
        /// </summary>
        [Column("PROJECTSUBCONTRACTSETTLEMENTID")]
        public string ProjectSubcontractSettlementId { get; set; }
        /// <summary>
        /// 分包合同工程量清单ID
        /// </summary>
        [Column("PROJECTSUBCONTRACTQUANTITIESID")]
        public string ProjectSubcontractQuantitiesId { get; set; }
        /// <summary>
        /// 清单编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 清单名称
        /// </summary>
        [Column("NAME")]
        public string Name { get; set; }
        /// <summary>
        /// 项目特征
        /// </summary>
        [Column("FEATURE")]
        public string Feature { get; set; }
        /// <summary>
        /// 计量单位
        /// </summary>
        [Column("UNIT")]
        public string Unit { get; set; }
        /// <summary>
        /// 综合单价
        /// </summary>
        [Column("PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Price { get; set; }
        /// <summary>
        /// 工程量
        /// </summary>
        [Column("QUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? Quantities { get; set; }
        /// <summary>
        /// 合价
        /// </summary>
        [Column("TOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalPrice { get; set; }
        /// <summary>
        /// 形象进度
        /// </summary>
        [Column("VISUALPROGRESS")]
        public string VisualProgress { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 审批数量
        /// </summary>
        [Column("AUDITQUANTITIES")]
        [DecimalPrecision(18, 4)]
        public decimal? AuditQuantities { get; set; }
        /// <summary>
        /// 审批合价
        /// </summary>
        [Column("AUDITTOTALPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? AuditTotalPrice { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

