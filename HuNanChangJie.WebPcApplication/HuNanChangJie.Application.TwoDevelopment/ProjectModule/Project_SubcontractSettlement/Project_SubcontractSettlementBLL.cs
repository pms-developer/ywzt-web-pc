﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-26 17:47
    /// 描 述：分包结算
    /// </summary>
    public class Project_SubcontractSettlementBLL : Project_SubcontractSettlementIBLL
    {
        private Project_SubcontractSettlementService project_SubcontractSettlementService = new Project_SubcontractSettlementService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractSettlementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return project_SubcontractSettlementService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlementMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractSettlementMaterialsEntity> GetProject_SubcontractSettlementMaterialsList(string keyValue)
        {
            try
            {
                return project_SubcontractSettlementService.GetProject_SubcontractSettlementMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlementQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractSettlementQuantitiesEntity> GetProject_SubcontractSettlementQuantitiesList(string keyValue)
        {
            try
            {
                return project_SubcontractSettlementService.GetProject_SubcontractSettlementQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractSettlementEntity GetProject_SubcontractSettlementEntity(string keyValue)
        {
            try
            {
                return project_SubcontractSettlementService.GetProject_SubcontractSettlementEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlementMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractSettlementMaterialsEntity GetProject_SubcontractSettlementMaterialsEntity(string keyValue)
        {
            try
            {
                return project_SubcontractSettlementService.GetProject_SubcontractSettlementMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlementQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractSettlementQuantitiesEntity GetProject_SubcontractSettlementQuantitiesEntity(string keyValue)
        {
            try
            {
                return project_SubcontractSettlementService.GetProject_SubcontractSettlementQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue)
        {
            try
            {
                return project_SubcontractSettlementService.GetCB_LeaseholdList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            try
            {
                return project_SubcontractSettlementService.UnAudit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            try
            {
                return project_SubcontractSettlementService.Audit(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                project_SubcontractSettlementService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SubcontractSettlementEntity entity,List<Project_SubcontractSettlementMaterialsEntity> project_SubcontractSettlementMaterialsList,List<Project_SubcontractSettlementQuantitiesEntity> project_SubcontractSettlementQuantitiesList, List<CB_Leasehold> CB_LeaseholdList,string deleteList,string type)
        {
            try
            {
                project_SubcontractSettlementService.SaveEntity(keyValue, entity,project_SubcontractSettlementMaterialsList,project_SubcontractSettlementQuantitiesList, CB_LeaseholdList,deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("CJ_BASE_0024");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
