﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Application.TwoDevelopment.SystemModule;
using HuNanChangeJie.Application.Project.ProjectSubject;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-26 17:47
    /// 描 述：分包结算
    /// </summary>
    public class Project_SubcontractSettlementService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractSettlementEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Code,
                t.SettlementDate,
                t.ProjectSubcontractId,
                a.name ,
                a.name ProjectSubcontractName,
                t.ReportedAmount,
                t.SettlementPaymentTotalAmount,
                t.AuditAmount,
                t.SettlementWarrantyAmount,
                t.OperatorId,
                t.CutoffDate,
                t.OperatorDepartmentId,
                t.Abstract,
                t.AuditStatus,
                t.CreationDate,
                t.Workflow_ID,
                us.F_RealName as OperatorName,
                dept.F_FullName as OperatorDepartmentName
                ");
                strSql.Append("  FROM Project_SubcontractSettlement t  left join Project_Subcontract a on t.ProjectSubcontractId = a.id");
                strSql.Append("  left join Base_User as us on t.OperatorId=us.F_UserId");
                strSql.Append("  left join Base_Department as dept on t.OperatorDepartmentId=dept.F_DepartmentId");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["Code"].IsEmpty())
                {
                    dp.Add("Code", "%" + queryParam["Code"].ToString() + "%", DbType.String);
                    strSql.Append(" AND t.Code Like @Code ");
                }

                if (!queryParam["StartTime"].IsEmpty() && !queryParam["EndTime"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["StartTime"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["EndTime"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                if (!queryParam["ProjectID"].IsEmpty())
                {
                    dp.Add("ProjectID", queryParam["ProjectID"].ToString(), DbType.String);
                    strSql.Append(" AND t.ProjectID = @ProjectID ");
                }

                if (!queryParam["Abstract"].IsEmpty())
                {
                    dp.Add("Abstract", "%" + queryParam["Abstract"].ToString() + "%", DbType.String);
                    strSql.Append("AND t.Abstract like @Abstract");
                }

                if (!queryParam["AuditStatus"].IsEmpty())
                {
                    var auditStatus = queryParam["AuditStatus"].ToString();
                    dp.Add("AuditStatus", auditStatus, DbType.String);
                    if (auditStatus == "0")
                    {
                        strSql.Append(" AND (t.AuditStatus in(@AuditStatus) or t.AuditStatus is null) ");
                    }
                    else
                    {
                        strSql.Append(" AND t.AuditStatus in(@AuditStatus) ");
                    }
                }

                if (!queryParam["name"].IsEmpty())
                {
                    dp.Add("name", "%" + queryParam["name"].ToString() + "%", DbType.String);
                    strSql.Append("AND a.name like @name");
                }
                var list=this.BaseRepository().FindList<Project_SubcontractSettlementEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlementMaterials表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractSettlementMaterialsEntity> GetProject_SubcontractSettlementMaterialsList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Project_SubcontractSettlementMaterialsEntity>(t=>t.ProjectSubcontractSettlementId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlementQuantities表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_SubcontractSettlementQuantitiesEntity> GetProject_SubcontractSettlementQuantitiesList(string keyValue)
        {
            try
            {
                var list= this.BaseRepository().FindList<Project_SubcontractSettlementQuantitiesEntity>(t=>t.ProjectSubcontractSettlementId == keyValue );
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractSettlementEntity GetProject_SubcontractSettlementEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractSettlementEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlementMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractSettlementMaterialsEntity GetProject_SubcontractSettlementMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractSettlementMaterialsEntity>(t=>t.ProjectSubcontractSettlementId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_SubcontractSettlementQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_SubcontractSettlementQuantitiesEntity GetProject_SubcontractSettlementQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_SubcontractSettlementQuantitiesEntity>(t=>t.ProjectSubcontractSettlementId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion


        public IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select * from CB_Leasehold where 1=1  ");
                sb.Append("  and  ID=@keyValue");
                var list = this.BaseRepository().FindList<CB_Leasehold>(sb.ToString(), new { keyValue });
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #region  提交数据
        public Util.Common.OperateResultEntity UnAudit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Project_SubcontractSettlementEntity project_SubcontractSettlementEntity = db.FindEntity<Project_SubcontractSettlementEntity>(keyValue);
                if (project_SubcontractSettlementEntity.AuditStatus == "4")
                {
                    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                }
                project_SubcontractSettlementEntity.AuditStatus = "4";
                db.Update<Project_SubcontractSettlementEntity>(project_SubcontractSettlementEntity);

                var project_SubcontractSettlementMaterialsEntityList = db.FindList<Project_SubcontractSettlementMaterialsEntity>(m => m.ProjectSubcontractSettlementId == keyValue);
                foreach (var item in project_SubcontractSettlementMaterialsEntityList)
                {
                    Project_SubcontractMaterialsEntity project_SubcontractMaterialsEntity = db.FindEntity<Project_SubcontractMaterialsEntity>(m => m.ProjectSubcontractId == project_SubcontractSettlementEntity.ProjectSubcontractId && m.ID == item.ProjectSubcontractMaterialsId);
                    if (!project_SubcontractMaterialsEntity.SettlementQuantities.HasValue)
                        project_SubcontractMaterialsEntity.SettlementQuantities = 0;

                    if (!project_SubcontractMaterialsEntity.SettlementPrice.HasValue)
                        project_SubcontractMaterialsEntity.SettlementPrice = 0;

                    if (!project_SubcontractMaterialsEntity.SettlementTotalPrice.HasValue)
                        project_SubcontractMaterialsEntity.SettlementTotalPrice = 0;

                    project_SubcontractMaterialsEntity.SettlementQuantities -= item.AuditQuantity;
                    project_SubcontractMaterialsEntity.SettlementTotalPrice -= item.AuditTotalPrice;

                    project_SubcontractMaterialsEntity.SettlementPrice = project_SubcontractMaterialsEntity.SettlementTotalPrice / project_SubcontractMaterialsEntity.SettlementQuantities;

                    db.Update<Project_SubcontractMaterialsEntity>(project_SubcontractMaterialsEntity);
                }


                var project_SubcontractSettlementQuantitiesEntityList = db.FindList<Project_SubcontractSettlementQuantitiesEntity>(m => m.ProjectSubcontractSettlementId == keyValue);
                foreach (var item in project_SubcontractSettlementQuantitiesEntityList)
                {
                    Project_SubcontractQuantitiesEntity project_SubcontractQuantitiesEntity = db.FindEntity<Project_SubcontractQuantitiesEntity>(m => m.ProjectSubcontractId == project_SubcontractSettlementEntity.ProjectSubcontractId && m.ID == item.ProjectSubcontractQuantitiesId);

                    if (!project_SubcontractQuantitiesEntity.SettlementQuantities.HasValue)
                        project_SubcontractQuantitiesEntity.SettlementQuantities = 0;

                    if (!project_SubcontractQuantitiesEntity.SettlementPrice.HasValue)
                        project_SubcontractQuantitiesEntity.SettlementPrice = 0;

                    if (!project_SubcontractQuantitiesEntity.SettlementTotalPrice.HasValue)
                        project_SubcontractQuantitiesEntity.SettlementTotalPrice = 0;


                    project_SubcontractQuantitiesEntity.SettlementQuantities -= item.AuditQuantities;
                    project_SubcontractQuantitiesEntity.SettlementTotalPrice -= item.AuditTotalPrice;

                    if (project_SubcontractQuantitiesEntity.SettlementQuantities == 0)
                        project_SubcontractQuantitiesEntity.SettlementPrice = 0;
                    else
                        project_SubcontractQuantitiesEntity.SettlementPrice = project_SubcontractQuantitiesEntity.SettlementTotalPrice / project_SubcontractQuantitiesEntity.SettlementQuantities;

                    db.Update<Project_SubcontractQuantitiesEntity>(project_SubcontractQuantitiesEntity);
                }
                Project_SubcontractEntity project_SubcontractEntity = db.FindEntity<Project_SubcontractEntity>(project_SubcontractSettlementEntity.ProjectSubcontractId);
                if (!project_SubcontractEntity.SettlementAmount.HasValue)
                    project_SubcontractEntity.SettlementAmount = 0;

                project_SubcontractEntity.SettlementAmount -= project_SubcontractSettlementEntity.AuditAmount;
                if (!project_SubcontractEntity.FineExpend.HasValue)
                    project_SubcontractEntity.FineExpend = 0;
                if (!project_SubcontractEntity.OtherExpend.HasValue)
                    project_SubcontractEntity.OtherExpend = 0;
                if (!project_SubcontractEntity.OtherIncome.HasValue)
                    project_SubcontractEntity.OtherIncome = 0;

                project_SubcontractEntity.FineExpend -= project_SubcontractSettlementEntity.FineExpend ?? 0;
                project_SubcontractEntity.OtherExpend -= project_SubcontractSettlementEntity.OtherExpend ?? 0;
                project_SubcontractEntity.OtherIncome -= project_SubcontractSettlementEntity.OtherIncome ?? 0;


                db.Update<Project_SubcontractEntity>(project_SubcontractEntity);


                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(m => m.Name == "ProjectSubcontractHandleNode");
                if (config != null && config.Value == 1)
                {

                    Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(m => m.ID == project_SubcontractEntity.SubcontractType);

                    if (base_ContractTypeEntity != null)
                    {
                        var subjectInfo = new ProjectSubjectBll().GetEntity(base_ContractTypeEntity.BaseSubjectId, project_SubcontractSettlementEntity.ProjectID);
                        if (subjectInfo != null)
                        //   resultList.Add(new OperateResultEntity() { Success = false, Message = "未找到对应的科目" });
                        //else
                        {
                            subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) - (project_SubcontractSettlementEntity.AuditAmount ?? 0);

                            //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                            //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                            db.Update<ProjectSubjectEntity>(subjectInfo);
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }

        public Util.Common.OperateResultEntity Audit(string keyValue)
        {
            var db = this.BaseRepository();
            db.BeginTrans();
            try
            {
                List<OperateResultEntity> resultList = new List<OperateResultEntity>();
                List<string> messageList = new List<string>();

                Project_SubcontractSettlementEntity project_SubcontractSettlementEntity = db.FindEntity<Project_SubcontractSettlementEntity>(keyValue);

                //走流程审核会先把审核状态改为通过再执行页面审核方法 目前先注释解决 要大改
                //if (project_SubcontractSettlementEntity.AuditStatus == "2")
                //{
                //    throw new Exception("当前记录状态不能执行此操作,请刷新后重试");
                //}
                project_SubcontractSettlementEntity.AuditStatus = "2";
                db.Update<Project_SubcontractSettlementEntity>(project_SubcontractSettlementEntity);

                var project_SubcontractSettlementMaterialsEntityList = db.FindList<Project_SubcontractSettlementMaterialsEntity>(m=>m.ProjectSubcontractSettlementId == keyValue);
                foreach (var item in project_SubcontractSettlementMaterialsEntityList)
                {
                    Project_SubcontractMaterialsEntity project_SubcontractMaterialsEntity = db.FindEntity<Project_SubcontractMaterialsEntity>(m => m.ProjectSubcontractId == project_SubcontractSettlementEntity.ProjectSubcontractId && m.ID == item.ProjectSubcontractMaterialsId);
                    if (!project_SubcontractMaterialsEntity.SettlementQuantities.HasValue)
                        project_SubcontractMaterialsEntity.SettlementQuantities = 0;

                    if (!project_SubcontractMaterialsEntity.SettlementPrice.HasValue)
                        project_SubcontractMaterialsEntity.SettlementPrice = 0;

                    if (!project_SubcontractMaterialsEntity.SettlementTotalPrice.HasValue)
                        project_SubcontractMaterialsEntity.SettlementTotalPrice = 0;

                    project_SubcontractMaterialsEntity.SettlementQuantities += item.AuditQuantity;
                    project_SubcontractMaterialsEntity.SettlementTotalPrice += item.AuditTotalPrice;

                    project_SubcontractMaterialsEntity.SettlementPrice = project_SubcontractMaterialsEntity.SettlementTotalPrice / project_SubcontractMaterialsEntity.SettlementQuantities;

                    db.Update<Project_SubcontractMaterialsEntity>(project_SubcontractMaterialsEntity);
                }


                var project_SubcontractSettlementQuantitiesEntityList = db.FindList<Project_SubcontractSettlementQuantitiesEntity>(m => m.ProjectSubcontractSettlementId == keyValue);
                foreach (var item in project_SubcontractSettlementQuantitiesEntityList)
                {
                    Project_SubcontractQuantitiesEntity project_SubcontractQuantitiesEntity = db.FindEntity<Project_SubcontractQuantitiesEntity>(m=>m.ProjectSubcontractId== project_SubcontractSettlementEntity.ProjectSubcontractId && m.ID==item.ProjectSubcontractQuantitiesId);

                    if (!project_SubcontractQuantitiesEntity.SettlementQuantities.HasValue)
                        project_SubcontractQuantitiesEntity.SettlementQuantities = 0;

                    if (!project_SubcontractQuantitiesEntity.SettlementPrice.HasValue)
                        project_SubcontractQuantitiesEntity.SettlementPrice = 0;

                    if (!project_SubcontractQuantitiesEntity.SettlementTotalPrice.HasValue)
                        project_SubcontractQuantitiesEntity.SettlementTotalPrice = 0;


                    project_SubcontractQuantitiesEntity.SettlementQuantities += item.AuditQuantities;
                    project_SubcontractQuantitiesEntity.SettlementTotalPrice += item.AuditTotalPrice;

                    if (project_SubcontractQuantitiesEntity.SettlementQuantities == 0)
                        project_SubcontractQuantitiesEntity.SettlementPrice = 0;
                    else
                        project_SubcontractQuantitiesEntity.SettlementPrice = project_SubcontractQuantitiesEntity.SettlementTotalPrice / project_SubcontractQuantitiesEntity.SettlementQuantities;

                    db.Update<Project_SubcontractQuantitiesEntity>(project_SubcontractQuantitiesEntity);
                }
                Project_SubcontractEntity project_SubcontractEntity = db.FindEntity<Project_SubcontractEntity>(project_SubcontractSettlementEntity.ProjectSubcontractId);
                if (!project_SubcontractEntity.SettlementAmount.HasValue)
                    project_SubcontractEntity.SettlementAmount = 0;

                project_SubcontractEntity.SettlementAmount += project_SubcontractSettlementEntity.AuditAmount;
                if (!project_SubcontractEntity.FineExpend.HasValue)
                    project_SubcontractEntity.FineExpend = 0;
                if (!project_SubcontractEntity.OtherExpend.HasValue)
                    project_SubcontractEntity.OtherExpend = 0;
                if (!project_SubcontractEntity.OtherIncome.HasValue)
                    project_SubcontractEntity.OtherIncome = 0;

                project_SubcontractEntity.FineExpend += project_SubcontractSettlementEntity.FineExpend ?? 0;
                project_SubcontractEntity.OtherExpend += project_SubcontractSettlementEntity.OtherExpend ?? 0;
                project_SubcontractEntity.OtherIncome += project_SubcontractSettlementEntity.OtherIncome ?? 0;

                db.Update<Project_SubcontractEntity>(project_SubcontractEntity);

                var config = LoadSystemConfig.SystemConfigs.FirstOrDefault(m => m.Name == "ProjectSubcontractHandleNode");
                if (config != null && config.Value == 1)
                {

                    Base_ContractTypeEntity base_ContractTypeEntity = db.FindEntity<Base_ContractTypeEntity>(m => m.ID == project_SubcontractEntity.SubcontractType);

                    if (base_ContractTypeEntity != null)
                    {
                        var subjectInfo = new ProjectSubjectBll().GetEntity(base_ContractTypeEntity.BaseSubjectId, project_SubcontractSettlementEntity.ProjectID);
                        if (subjectInfo != null)
                        //    resultList.Add(new OperateResultEntity() { Success = false, Message = "未找到对应的科目" });
                        {
                            subjectInfo.PendingCost = (subjectInfo.PendingCost ?? 0) + (project_SubcontractSettlementEntity.AuditAmount ?? 0);

                            //decimal afterTax = item.PayAmount.Value - (item.PayAmount.Value * purchase_ContractEntity.TaxRage.Value / 100);
                            //subjectInfo.AOAfterTax = (subjectInfo.AOAfterTax ?? 0) + afterTax;

                            db.Update<ProjectSubjectEntity>(subjectInfo);
                        }
                    }
                }

                if (messageList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in messageList)
                    {
                        tempMessage += item;
                    }
                    throw new Exception(tempMessage);
                }

                if (resultList.Count() > 0)
                {
                    string tempMessage = "";
                    foreach (var item in resultList)
                    {
                        tempMessage += item.Message;
                    }
                    throw new Exception(tempMessage);
                }

                db.Commit();

                return new Util.Common.OperateResultEntity() { Success = true };
            }
            catch (Exception ex)
            {
                db.Rollback();
                return new Util.Common.OperateResultEntity() { Success = false, Message = ex.Message };
            }
        }
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_SubcontractSettlementEntity = GetProject_SubcontractSettlementEntity(keyValue); 
                db.Delete<Project_SubcontractSettlementEntity>(t=>t.ID == keyValue);
                db.Delete<Project_SubcontractSettlementMaterialsEntity>(t=>t.ProjectSubcontractSettlementId == project_SubcontractSettlementEntity.ID);
                db.Delete<Project_SubcontractSettlementQuantitiesEntity>(t=>t.ProjectSubcontractSettlementId == project_SubcontractSettlementEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_SubcontractSettlementEntity entity,List<Project_SubcontractSettlementMaterialsEntity> project_SubcontractSettlementMaterialsList,List<Project_SubcontractSettlementQuantitiesEntity> project_SubcontractSettlementQuantitiesList, List<CB_Leasehold> CB_LeaseholdList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_SubcontractSettlementEntityTmp = GetProject_SubcontractSettlementEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_SubcontractSettlementMaterialsUpdateList= project_SubcontractSettlementMaterialsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_SubcontractSettlementMaterialsUpdateList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.Unit = base_MaterialsUnitEntity.Name;
                        }

                        db.Update(item);
                    }
                    var Project_SubcontractSettlementMaterialsInserList= project_SubcontractSettlementMaterialsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_SubcontractSettlementMaterialsInserList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.Unit = base_MaterialsUnitEntity.Name;
                        }

                        item.Create(item.ID);
                        item.ProjectSubcontractSettlementId = project_SubcontractSettlementEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码 
                    if (CB_LeaseholdList != null)
                    {
                        var CB_LeaseholdUpdateList = CB_LeaseholdList.FindAll(i => i.EditType == EditType.Update);
                        foreach (var item in CB_LeaseholdUpdateList)
                        {
                            db.Update(item);
                        }
                        var CB_LeaseholdInserList = CB_LeaseholdList.FindAll(i => i.EditType == EditType.Add);
                        foreach (var item in CB_LeaseholdInserList)
                        {
                            item.Create(item.ID);
                            item.ProjectID = project_SubcontractSettlementEntityTmp.ID;
                            db.Insert(item);
                        }
                    }
                    else
                    {
                        entity.Create(keyValue);
                        db.Insert(entity);
                    }

                    //没有生成代码1 

                    //没有生成代码 
                    var Project_SubcontractSettlementQuantitiesUpdateList= project_SubcontractSettlementQuantitiesList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_SubcontractSettlementQuantitiesUpdateList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.Unit = base_MaterialsUnitEntity.Name;
                        }

                        db.Update(item);
                    }
                    var Project_SubcontractSettlementQuantitiesInserList= project_SubcontractSettlementQuantitiesList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_SubcontractSettlementQuantitiesInserList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.Unit = base_MaterialsUnitEntity.Name;
                        }

                        item.Create(item.ID);
                        item.ProjectSubcontractSettlementId = project_SubcontractSettlementEntityTmp.ID;
                        db.Insert(item);
                    }

                    //var Project_SubcontractSettlementQuantitiesUpdateList = project_SubcontractSettlementQuantitiesList.FindAll(i => i.EditType == EditType.Update);
                    //foreach (var item in Project_SubcontractSettlementQuantitiesUpdateList)
                    //{
                    //    db.Update(item);
                    //}
                    //var Project_SubcontractSettlementQuantitiesInserList = project_SubcontractSettlementQuantitiesList.FindAll(i => i.EditType == EditType.Add);
                    //foreach (var item in Project_SubcontractSettlementQuantitiesInserList)
                    //{
                    //    item.Create(item.ID);
                    //    item.ProjectSubcontractSettlementId = project_SubcontractSettlementEntityTmp.ID;
                    //    db.Insert(item);
                    //}

                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_SubcontractSettlementMaterialsEntity item in project_SubcontractSettlementMaterialsList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.Unit = base_MaterialsUnitEntity.Name;
                        }
                        item.ProjectSubcontractSettlementId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (Project_SubcontractSettlementQuantitiesEntity item in project_SubcontractSettlementQuantitiesList)
                    {
                        Base_MaterialsUnitEntity base_MaterialsUnitEntity = db.FindEntity<Base_MaterialsUnitEntity>(item.Unit);
                        if (base_MaterialsUnitEntity != null)
                        {
                            item.Unit = base_MaterialsUnitEntity.Name;
                        }

                        item.ProjectSubcontractSettlementId = entity.ID;
                        db.Insert(item);
                    }
                    foreach (CB_Leasehold item in CB_LeaseholdList)
                    {
                        item.ID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
