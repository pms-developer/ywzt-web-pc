﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.TwoDevelopment.CaiWu;
using HuNanChangJie.Util.Common;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-26 17:47
    /// 描 述：分包结算
    /// </summary>
    public interface Project_SubcontractSettlementIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_SubcontractSettlementEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_SubcontractSettlementMaterials表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SubcontractSettlementMaterialsEntity> GetProject_SubcontractSettlementMaterialsList(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractSettlementQuantities表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_SubcontractSettlementQuantitiesEntity> GetProject_SubcontractSettlementQuantitiesList(string keyValue);

        IEnumerable<CB_Leasehold> GetCB_LeaseholdList(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractSettlement表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractSettlementEntity GetProject_SubcontractSettlementEntity(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractSettlementMaterials表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractSettlementMaterialsEntity GetProject_SubcontractSettlementMaterialsEntity(string keyValue);
        /// <summary>
        /// 获取Project_SubcontractSettlementQuantities表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_SubcontractSettlementQuantitiesEntity GetProject_SubcontractSettlementQuantitiesEntity(string keyValue);
        #endregion

        #region  提交数据

        OperateResultEntity UnAudit(string keyValue);
        OperateResultEntity Audit(string keyValue);
        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_SubcontractSettlementEntity entity,List<Project_SubcontractSettlementMaterialsEntity> project_SubcontractSettlementMaterialsList,List<Project_SubcontractSettlementQuantitiesEntity> project_SubcontractSettlementQuantitiesList, List<CB_Leasehold> CB_LeaseholdList,string deleteList,string type);
        #endregion

    }
}
