﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-07 15:30
    /// 描 述：投标立项
    /// </summary>
    public class ProjectBidService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_BidEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.ProjectName,
                t.CompanyCode,
                t.Code,
                t.OperatorId,
                t.OperatorDepartmentId,
                t.LimitedPrice,
                t.BiddingType,
                t.StartDate,
                t.EndDate,
                t.EvaluationType,
                t.OpeningDate,
                t.Rate,
                t.Linkman,
                t.Telphone,
                t.Phone,
                t.Fax,
                t.BidAddress,
                t.PostCode,
                t.IsAffect,
                t.AgencyName,
                t.CompanyFullName,
                t.AgencyAddress,
                t.AgencyPhone,
                t.AgencyPayment,
                t.Deposit,
                t.DepositType,
                t.Payee,
                t.ReceivedDate,
                t.OpeningBank,
                t.BankAccount,
                t.WorkerRequirement,
                t.CompanyRequirement,
                t.CreationDate,
                t.Workflow_ID,t.AuditStatus
                ");
                strSql.Append("  FROM Project_Bid t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" and t.ProjectId=@ProjectId");
                }
                var list = this.BaseRepository().FindList<Project_BidEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Bid表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_BidEntity GetProject_BidEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_BidEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_BidEntity GetFormInfo(string projectId)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_BidEntity>(i=>i.ProjectID==projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Project_BidEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_BidEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
