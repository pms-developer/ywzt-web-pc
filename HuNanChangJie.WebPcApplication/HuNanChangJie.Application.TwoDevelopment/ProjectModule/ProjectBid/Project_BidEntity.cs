﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-07 15:30
    /// 描 述：投标立项
    /// </summary>
    public class Project_BidEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 招标编号
        /// </summary>
        [Column("CODE")]
        public string Code { get; set; }
        /// <summary>
        /// 经办人
        /// </summary>
        [Column("OPERATORID")]
        public string OperatorId { get; set; }
        /// <summary>
        /// 经办部门
        /// </summary>
        [Column("OPERATORDEPARTMENTID")]
        public string OperatorDepartmentId { get; set; }
        /// <summary>
        /// 投标限价
        /// </summary>
        [Column("LIMITEDPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? LimitedPrice { get; set; }
        /// <summary>
        /// 招标方式
        /// </summary>
        [Column("BIDDINGTYPE")]
        public string BiddingType { get; set; }
        /// <summary>
        /// 报名开始时间
        /// </summary>
        [Column("STARTDATE")]
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 报名结束时间
        /// </summary>
        [Column("ENDDATE")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 评标方式
        /// </summary>
        [Column("EVALUATIONTYPE")]
        public string EvaluationType { get; set; }
        /// <summary>
        /// 开票日期
        /// </summary>
        [Column("OPENINGDATE")]
        public DateTime? OpeningDate { get; set; }
        /// <summary>
        /// 预估毛利率
        /// </summary>
        [Column("RATE")]
        [DecimalPrecision(18, 4)]
        public decimal? Rate { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }
        /// <summary>
        /// 客户联系人
        /// </summary>
        [Column("LINKMAN")]
        public string Linkman { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        [Column("TELPHONE")]
        public string Telphone { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        [Column("PHONE")]
        public string Phone { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        [Column("FAX")]
        public string Fax { get; set; }
        /// <summary>
        /// 投标地址
        /// </summary>
        [Column("BIDADDRESS")]
        public string BidAddress { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        [Column("POSTCODE")]
        public string PostCode { get; set; }
        /// <summary>
        /// 是否影响编制招标文件
        /// </summary>
        [Column("ISAFFECT")]
        public bool? IsAffect { get; set; }
        /// <summary>
        /// 代理机构名称
        /// </summary>
        [Column("AGENCYNAME")]
        public string AgencyName { get; set; }
        /// <summary>
        /// 代理机构联系地址
        /// </summary>
        [Column("AGENCYADDRESS")]
        public string AgencyAddress { get; set; }
        /// <summary>
        /// 代理机构联系电话
        /// </summary>
        [Column("AGENCYPHONE")]
        public string AgencyPhone { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        [Column("AGENCYPAYMENT")]
        public string AgencyPayment { get; set; }
        /// <summary>
        /// 代理机构联系人
        /// </summary>
        [Column("AGENCYLINKMAN")]
        public string AgencyLinkman { get; set; }
        /// <summary>
        /// 投标保证金
        /// </summary>
        [Column("DEPOSIT")]
        [DecimalPrecision(18, 4)]
        public decimal? Deposit { get; set; }
        /// <summary>
        /// 保证金方式
        /// </summary>
        [Column("DEPOSITTYPE")]
        public string DepositType { get; set; }
        /// <summary>
        /// 保证金收款单位
        /// </summary>
        [Column("PAYEE")]
        public string Payee { get; set; }
        /// <summary>
        /// 要求到账日期
        /// </summary>
        [Column("RECEIVEDDATE")]
        public DateTime? ReceivedDate { get; set; }
        /// <summary>
        /// 收款单位开户行
        /// </summary>
        [Column("OPENINGBANK")]
        public string OpeningBank { get; set; }
        /// <summary>
        /// 收款单位银行账号
        /// </summary>
        [Column("BANKACCOUNT")]
        public string BankAccount { get; set; }
        /// <summary>
        /// 人员资质要求
        /// </summary>
        [Column("WORKERREQUIREMENT")]
        public string WorkerRequirement { get; set; }
        /// <summary>
        /// 公司资质要求
        /// </summary>
        [Column("COMPANYREQUIREMENT")]
        public string CompanyRequirement { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 修改者ID
        /// </summary>
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 修改名称
        /// </summary>
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 公司编码
        /// </summary>
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary>
        /// 公司ID
        /// </summary>
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary>
        /// 公司全称
        /// </summary>
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary>
        /// 公司简称
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 拼音码
        /// </summary>
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary>
        /// 拼音码首字母简写
        /// </summary>
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary>
        /// 审核者ID
        /// </summary>
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary>
        /// 审核者名称
        /// </summary>
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary>
        /// 审批流ID
        /// </summary>
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

