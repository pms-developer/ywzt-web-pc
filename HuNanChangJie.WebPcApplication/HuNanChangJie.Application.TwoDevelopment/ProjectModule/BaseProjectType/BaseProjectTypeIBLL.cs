﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-05 15:08
    /// 描 述：项目类型
    /// </summary>
    public interface BaseProjectTypeIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_ProjectTypeEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_ProjectTypeDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Base_ProjectTypeDetailsEntity> GetBase_ProjectTypeDetailsList(string keyValue);
        /// <summary>
        /// 获取Base_ProjectType表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_ProjectTypeEntity GetBase_ProjectTypeEntity(string keyValue);
        bool CheckIsSameName(string value);

        /// <summary>
        /// 获取Base_ProjectTypeDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_ProjectTypeDetailsEntity GetBase_ProjectTypeDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_ProjectTypeEntity entity,List<Base_ProjectTypeDetailsEntity> base_ProjectTypeDetailsList,string deleteList,string type);
        #endregion

    }
}
