﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-05 15:08
    /// 描 述：项目类型
    /// </summary>
    public class BaseProjectTypeService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Base_ProjectTypeEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.Name,
                t.SortCode,
                t.IsEnable
                ");
                strSql.Append("  FROM Base_ProjectType t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                var list=this.BaseRepository().FindList<Base_ProjectTypeEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_ProjectTypeDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Base_ProjectTypeDetailsEntity> GetBase_ProjectTypeDetailsList(string keyValue)
        {
            try
            {
                var sql = @"select a.*,
                            b.Name,
                            c.Name as NextPhaseName,
                            d.Name as FormName,
                            b.SortCode
                            from Base_ProjectTypeDetails as 
                            a left join Base_ProjectPhase as b 
                            on a.PhaseId=b.ID
                            left join Base_ProjectPhase as c 
                            on a.NextPhaseId=c.ID
                            left join 
                            (select 
                            m.f_fullName as Name,
                            m.f_enCode as Code,
                            t.F_ModuleId as ModuleId,
                            t.F_FormId as FormId  
                            FROM Form_Relation t
                            LEFT JOIN Base_Module as m  on t.F_ModuleId = m.F_ModuleId) as d
                            on a.FormId=d.FormId
                            where a.TypeId=@keyValue";
                var list= this.BaseRepository().FindList<Base_ProjectTypeDetailsEntity>(sql,new { keyValue });
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_ProjectType表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_ProjectTypeEntity GetBase_ProjectTypeEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_ProjectTypeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Base_ProjectTypeDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Base_ProjectTypeDetailsEntity GetBase_ProjectTypeDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Base_ProjectTypeDetailsEntity>(t=>t.TypeId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var base_ProjectTypeEntity = GetBase_ProjectTypeEntity(keyValue); 
                db.Delete<Base_ProjectTypeEntity>(t=>t.ID == keyValue);
                db.Delete<Base_ProjectTypeDetailsEntity>(t=>t.TypeId == base_ProjectTypeEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public bool CheckIsSameName(string value)
        {
            try
            {
                var info = BaseRepository().FindEntity<Base_ProjectTypeEntity>(i => i.Name == value);
                return info == null ? false : true;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Base_ProjectTypeEntity entity,List<Base_ProjectTypeDetailsEntity> base_ProjectTypeDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var base_ProjectTypeEntityTmp = GetBase_ProjectTypeEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Base_ProjectTypeDetailsUpdateList= base_ProjectTypeDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Base_ProjectTypeDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Base_ProjectTypeDetailsInserList= base_ProjectTypeDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Base_ProjectTypeDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.TypeId = base_ProjectTypeEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Base_ProjectTypeDetailsEntity item in base_ProjectTypeDetailsList)
                    {
                        item.TypeId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
