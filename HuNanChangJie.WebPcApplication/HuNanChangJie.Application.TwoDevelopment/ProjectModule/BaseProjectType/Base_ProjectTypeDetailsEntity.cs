﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-05 15:08
    /// 描 述：项目类型
    /// </summary>
    public class Base_ProjectTypeDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 项目类型ID
        /// </summary>
        [Column("TYPEID")]
        public string TypeId { get; set; }
        /// <summary>
        /// 表单ID
        /// </summary>
        [Column("FORMID")]
        public string FormId { get; set; }
        /// <summary>
        /// 项目阶段ID
        /// </summary>
        [Column("PHASEID")]
        public string PhaseId { get; set; }
        /// <summary>
        /// 后续阶段ID
        /// </summary>
        [Column("NEXTPHASEID")]
        public string NextPhaseId { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        [Column("ISENABLE")]
        public bool? IsEnable { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region  扩展属性
        [NotMapped]
        public string NextPhaseName { get; set; }
        [NotMapped]
        public string FormName { get; set; }
        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public int SortCode { get; set; }

        #endregion
    }
}

