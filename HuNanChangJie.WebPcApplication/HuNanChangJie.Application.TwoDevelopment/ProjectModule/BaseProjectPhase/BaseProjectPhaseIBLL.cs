﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-04-05 14:45
    /// 描 述：项目阶段
    /// </summary>
    public interface BaseProjectPhaseIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Base_ProjectPhaseEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Base_ProjectPhase表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Base_ProjectPhaseEntity GetBase_ProjectPhaseEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Base_ProjectPhaseEntity entity,string deleteList,string type);
        bool CheckIsSameName(string value);
        IEnumerable<Base_ProjectPhaseEntity> GetList();
        IEnumerable<Base_ProjectPhaseEntity> GetListByCurrentId(string currentId);
        #endregion

    }
}
