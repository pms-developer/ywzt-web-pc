﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 15:57
    /// 描 述：证照社保
    /// </summary>
    public class ProjectLicenseSheBaoBLL : ProjectLicenseSheBaoIBLL
    {
        private ProjectLicenseSheBaoService projectLicenseSheBaoService = new ProjectLicenseSheBaoService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_LicenseSheBaoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectLicenseSheBaoService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseSheBaoDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_LicenseSheBaoDetailsEntity> GetProject_LicenseSheBaoDetailsList(string keyValue)
        {
            try
            {
                return projectLicenseSheBaoService.GetProject_LicenseSheBaoDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseSheBao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_LicenseSheBaoEntity GetProject_LicenseSheBaoEntity(string keyValue)
        {
            try
            {
                return projectLicenseSheBaoService.GetProject_LicenseSheBaoEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseSheBaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_LicenseSheBaoDetailsEntity GetProject_LicenseSheBaoDetailsEntity(string keyValue)
        {
            try
            {
                return projectLicenseSheBaoService.GetProject_LicenseSheBaoDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectLicenseSheBaoService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_LicenseSheBaoEntity entity,List<Project_LicenseSheBaoDetailsEntity> project_LicenseSheBaoDetailsList,string deleteList,string type)
        {
            try
            {
                projectLicenseSheBaoService.SaveEntity(keyValue, entity,project_LicenseSheBaoDetailsList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_LicenseSheBaoEntity GetFormInfo(string projectId)
        {
            try
            {
                return projectLicenseSheBaoService.GetFormInfo(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return projectLicenseSheBaoService.GetAllProjectList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        IEnumerable<Project_LicenseSheBaoEntity> ProjectLicenseSheBaoIBLL.GetLicenseSheBaoList(IEnumerable<string> projectIds)
        {
            try
            {
                return projectLicenseSheBaoService.GetLicenseSheBaoList(projectIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 根取证照使用社保集合(已审核) 计提状态不等于true
        /// </summary>
        /// <returns></returns>
       public IEnumerable<Project_LicenseSheBaoEntity> GetLicenseSheBaoList()
        {
            try
            {
                return projectLicenseSheBaoService.GetLicenseSheBaoList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }
        #endregion

    }
}
