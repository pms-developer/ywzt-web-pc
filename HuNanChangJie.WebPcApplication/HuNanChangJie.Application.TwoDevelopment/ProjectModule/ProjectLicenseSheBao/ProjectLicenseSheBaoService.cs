﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 15:57
    /// 描 述：证照社保
    /// </summary>
    public class ProjectLicenseSheBaoService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_LicenseSheBaoEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.*
                ");
                strSql.Append("  FROM Project_LicenseSheBao t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();

                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" and t.ProjectId=@ProjectId");
                }
                var list = this.BaseRepository().FindList<Project_LicenseSheBaoEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseSheBaoDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_LicenseSheBaoDetailsEntity> GetProject_LicenseSheBaoDetailsList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.zz_xinming,
                            b.zz_bianhao,
                            b.zz_shenfengzheng,
                            c.zzmc_name 
                            from Project_LicenseSheBaoDetails as a left join Base_CJ_ZhengZhao as b
                            on a.ZhengZhaoId=b.ID
                            left join Base_CJ_ZhenZhaoMingCheng as c 
                            on b.fk_zhengzhaomingcheng=c.zzmc_id");
                sb.Append(" where a.ProjectLicenseSheBaoId=@keyValue");
                var list = this.BaseRepository().FindList<Project_LicenseSheBaoDetailsEntity>(sb.ToString(), new { keyValue });
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseSheBao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_LicenseSheBaoEntity GetProject_LicenseSheBaoEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_LicenseSheBaoEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseSheBaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_LicenseSheBaoDetailsEntity GetProject_LicenseSheBaoDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_LicenseSheBaoDetailsEntity>(t=>t.ProjectLicenseSheBaoId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_LicenseSheBaoEntity = GetProject_LicenseSheBaoEntity(keyValue); 
                db.Delete<Project_LicenseSheBaoEntity>(t=>t.ID == keyValue);
                db.Delete<Project_LicenseSheBaoDetailsEntity>(t=>t.ProjectLicenseSheBaoId == project_LicenseSheBaoEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_LicenseSheBaoEntity GetFormInfo(string projectId)
        {
            try
            {
                return BaseRepository().FindEntity<Project_LicenseSheBaoEntity>(i => i.ProjectID == projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_LicenseSheBaoEntity entity,List<Project_LicenseSheBaoDetailsEntity> project_LicenseSheBaoDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_LicenseSheBaoEntityTmp = GetProject_LicenseSheBaoEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_LicenseSheBaoDetailsUpdateList= project_LicenseSheBaoDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_LicenseSheBaoDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_LicenseSheBaoDetailsInserList= project_LicenseSheBaoDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_LicenseSheBaoDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectLicenseSheBaoId = project_LicenseSheBaoEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_LicenseSheBaoDetailsEntity item in project_LicenseSheBaoDetailsList)
                    {
                        item.ProjectLicenseSheBaoId = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 根取证照使用社保集合(已审核) 计提状态不等于true
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Project_LicenseSheBaoEntity> GetLicenseSheBaoList()
        {
            try 
            {
                return this.BaseRepository().FindList<Project_LicenseSheBaoEntity>(i => i.AuditStatus == "2" && i.AccrueState != true);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return this.BaseRepository().FindList<ProjectEntity>(i => i.AuditStatus == "2");
            }
            catch (Exception ex)
            {

                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<Project_LicenseSheBaoEntity> GetLicenseSheBaoList(IEnumerable<string> projectIds)
        {
            try
            {
                return this.BaseRepository().FindList<Project_LicenseSheBaoEntity>(i => projectIds.Contains(i.ProjectID) && i.AuditStatus == "2" && i.AccrueState != true);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }
        #endregion

    }
}
