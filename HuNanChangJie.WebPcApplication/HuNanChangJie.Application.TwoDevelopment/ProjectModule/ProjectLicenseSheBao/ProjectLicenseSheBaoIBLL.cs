﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 15:57
    /// 描 述：证照社保
    /// </summary>
    public interface ProjectLicenseSheBaoIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_LicenseSheBaoEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_LicenseSheBaoDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_LicenseSheBaoDetailsEntity> GetProject_LicenseSheBaoDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_LicenseSheBao表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_LicenseSheBaoEntity GetProject_LicenseSheBaoEntity(string keyValue);
        /// <summary>
        /// 获取Project_LicenseSheBaoDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_LicenseSheBaoDetailsEntity GetProject_LicenseSheBaoDetailsEntity(string keyValue);

        /// <summary>
        /// 获取所有已审核的项目信息
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProjectEntity> GetAllProjectList();

        /// <summary>
        /// 根据项目ID集合获取项目证照使用社保集合(已审核)
        /// </summary>
        /// <param name="projectIds">项目ID集合</param>
        /// <returns></returns>
        IEnumerable<Project_LicenseSheBaoEntity> GetLicenseSheBaoList(IEnumerable<string> projectIds);

        /// <summary>
        /// 根取证照使用社保集合(已审核) 计提状态不等于true
        /// </summary>
        /// <returns></returns>
        IEnumerable<Project_LicenseSheBaoEntity> GetLicenseSheBaoList();
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_LicenseSheBaoEntity entity,List<Project_LicenseSheBaoDetailsEntity> project_LicenseSheBaoDetailsList,string deleteList,string type);
        Project_LicenseSheBaoEntity GetFormInfo(string projectId);

        #endregion

    }
}
