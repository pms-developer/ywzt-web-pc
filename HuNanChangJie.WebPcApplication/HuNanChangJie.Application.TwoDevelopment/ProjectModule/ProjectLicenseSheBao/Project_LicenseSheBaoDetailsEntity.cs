﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-30 15:57
    /// 描 述：证照社保
    /// </summary>
    public class Project_LicenseSheBaoDetailsEntity:BaseEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 证照社保ID
        /// </summary>
        [Column("PROJECTLICENSESHEBAOID")]
        public string ProjectLicenseSheBaoId { get; set; }
        /// <summary>
        /// 项目ID
        /// </summary>
        [Column("PROJECTID")]
        public string ProjectId { get; set; }
        /// <summary>
        /// 证照ID
        /// </summary>
        [Column("ZHENGZHAOID")]
        public string ZhengZhaoId { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        [Column("STRATDATE")]
        public DateTime? StratDate { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        [Column("ENDDATE")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 每月社保
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? SocialSecurity { get; set; }
        /// <summary>
        /// 时间小计
        /// </summary>
        [Column("TIMECOUNT")]
        public string TimeCount { get; set; }
        /// <summary>
        /// 社保小计
        /// </summary>
        [Column("TOTALAMOUNT")]
        [DecimalPrecision(18, 4)]
        public decimal? TotalAmount { get; set; }
        /// <summary>
        /// 排序号
        /// </summary>
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        /// <summary>
        /// 是否已完成
        /// </summary>
        public bool? IsFinished { get; set; }

        /// <summary>
        /// 已发生金额
        /// </summary>
        [DecimalPrecision(18, 4)]
        public decimal? FinishedAmount { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion

        #region 扩展成员

        [NotMapped]
        public string zz_xinming { get; set; }

        [NotMapped]
        public string zz_bianhao { get; set; }

        [NotMapped]
        public string zzmc_name { get; set; }

        [NotMapped]
        public string zz_shenfengzheng { get; set; }

        #endregion
    }
}

