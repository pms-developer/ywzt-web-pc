﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-18 12:19
    /// 描 述：开标登记
    /// </summary>
    public interface BidOpeningIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_BidOpeningEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_BidOpeningDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_BidOpeningDetailsEntity> GetProject_BidOpeningDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_BidOpening表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_BidOpeningEntity GetProject_BidOpeningEntity(string keyValue);
        /// <summary>
        /// 获取Project_BidOpeningDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_BidOpeningDetailsEntity GetProject_BidOpeningDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_BidOpeningEntity entity,List<Project_BidOpeningDetailsEntity> project_BidOpeningDetailsList,string deleteList,string type);
        #endregion

    }
}
