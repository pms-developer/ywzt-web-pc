﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-02-18 12:19
    /// 描 述：开标登记
    /// </summary>
    public class Project_BidOpeningDetailsEntity : BaseEntity
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// 开标登记ID
        /// </summary>
        [Column("BIDOPENINGID")]
        public string BidOpeningId { get; set; }
        /// <summary>
        /// 参与公司
        /// </summary>
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 报价
        /// </summary>
        [Column("OFFER")]
        [DecimalPrecision(18, 4)]
        public decimal? Offer { get; set; }
        /// <summary>
        /// 商务得分
        /// </summary>
        [Column("BUSINESS")]
        [DecimalPrecision(18, 4)]
        public decimal? Business { get; set; }
        /// <summary>
        /// 技术得分
        /// </summary>
        [Column("TECHNOLOGY")]
        [DecimalPrecision(18, 4)]
        public decimal? Technology { get; set; }
        /// <summary>
        /// 清标得分
        /// </summary>
        [Column("QINGBIAO")]
        [DecimalPrecision(18, 4)]
        public decimal? QingBiao { get; set; }
        /// <summary>
        /// 综合得分
        /// </summary>
        [Column("SYNTHESIZE")]
        [DecimalPrecision(18, 4)]
        public decimal? Synthesize { get; set; }
        /// <summary>
        /// 排名
        /// </summary>
        [Column("RANKING")]
        public int? Ranking { get; set; }
        /// <summary>
        /// 投标负责人
        /// </summary>
        [Column("LEADER")]
        public string Leader { get; set; }
        /// <summary>
        /// 是否挂靠
        /// </summary>
        [Column("ISGUAKAO")]
        public bool? IsGuaKao { get; set; }
        /// <summary>
        /// 是否中标
        /// </summary>
        [Column("ISBID")]
        public bool? IsBid { get; set; }
        /// <summary>
        /// 工期(天)
        /// </summary>
        [Column("PERIOD")]
        public int? Period { get; set; }
        /// <summary>
        /// 质保期(天)
        /// </summary>
        [Column("WARRANTY")]
        public int? Warranty { get; set; }
        /// <summary>
        /// 行备注
        /// </summary>
        [Column("REMARK")]
        public string Remark { get; set; }

        public DateTime CreationDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 新增调用ID由前端传入
        /// </summary>
        /// <param name="keyValue"></param>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            this.CreationDate = DateTime.Now;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

