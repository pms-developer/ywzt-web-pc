﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangJie.Application.Base.SystemModule;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2020-03-24 10:58 
    /// 描 述：完成决算 
    /// </summary> 
    public class ProjectFinalBLL : ProjectFinalIBLL
    {
        private ProjectFinalService projectFinalService = new ProjectFinalService();
        private ProjectContractIBLL projectContractBll = new ProjectContractBLL();

        #region  获取数据 

        /// <summary> 
        /// 获取页面显示列表数据 
        /// <summary> 
        /// <param name="queryJson">查询参数</param> 
        /// <returns></returns> 
        public IEnumerable<Project_FinalEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectFinalService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_ContractQuantities表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<Project_ContractQuantitiesEntity> GetProject_ContractQuantitiesList(string keyValue)
        {
            try
            {
                return projectFinalService.GetProject_ContractQuantitiesList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_ContractMaterials表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<Project_ContractMaterialsEntity> GetProject_ContractMaterialsList(string keyValue)
        {
            try
            {
                return projectFinalService.GetProject_ContractMaterialsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_Final表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public Project_FinalEntity GetProject_FinalEntity(string keyValue)
        {
            try
            {
                return projectFinalService.GetProject_FinalEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_ContractQuantities表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public Project_ContractQuantitiesEntity GetProject_ContractQuantitiesEntity(string keyValue)
        {
            try
            {
                return projectFinalService.GetProject_ContractQuantitiesEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_ContractMaterials表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public Project_ContractMaterialsEntity GetProject_ContractMaterialsEntity(string keyValue)
        {
            try
            {
                return projectFinalService.GetProject_ContractMaterialsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectFinalService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void SaveEntity(string keyValue, Project_FinalEntity entity, List<Project_ContractQuantitiesEntity> project_ContractQuantitiesList, List<Project_ContractMaterialsEntity> project_ContractMaterialsList, string deleteList, string type)
        {
            try
            {
                projectFinalService.SaveEntity(keyValue, entity, project_ContractQuantitiesList, project_ContractMaterialsList, deleteList, type);
                if (type == "add")
                {
                    CodeRuleIBLL codeRuleIBLL = new CodeRuleBLL();
                    codeRuleIBLL.UseRuleSeed("ProjectFinalCode");
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void Audit(string keyValue)
        {
            try
            {
                var info = projectFinalService.GetProject_FinalEntity(keyValue);
                info.AuditStatus = "2";
                info.Auditor_ID = LoginUserInfo.UserInfo.userId;
                info.AuditorName = LoginUserInfo.UserInfo.realName;

                var contractId = info.ProjectContractId;
                var contractInfo = projectContractBll.GetProject_ContractEntity(contractId);               
                contractInfo.IsFinal = true;               

                projectFinalService.ProjectFinal(contractInfo, info);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public void UnAudit(string keyValue)
        {
            try
            {
                var info = projectFinalService.GetProject_FinalEntity(keyValue);
                info.AuditStatus = "0";
                info.Auditor_ID = null;
                info.AuditorName = null;

                var contractId = info.ProjectContractId;
                var contractInfo = projectContractBll.GetProject_ContractEntity(contractId);
                contractInfo.IsFinal = false;

                projectFinalService.ProjectFinal(contractInfo,info);

            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

    }
}
