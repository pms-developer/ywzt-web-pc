﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2020-03-24 10:58 
    /// 描 述：完成决算 
    /// </summary> 
    public class ProjectFinalService : RepositoryFactory
    {
        private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据 

        /// <summary> 
        /// 获取页面显示列表数据 
        /// <summary> 
        /// <param name="queryJson">查询参数</param> 
        /// <returns></returns> 
        public IEnumerable<Project_FinalEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                var sql = @"select a.*,b.InitAmount,b.TotalAmount,b.SettlementAmount,
                          (b.Name+'('+b.Code+')')as ContractFullName from 
                          Project_Final as a left join Project_Contract as b 
                          on a.ProjectContractId=b.ID ";
                sql += " where 1=1 ";
                strSql.Append(sql);
                var queryParam = queryJson.ToJObject();
                // 虚拟参数 
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" and a.ProjectId=@ProjectId");
                }
                var list = this.BaseRepository().FindList<Project_FinalEntity>(strSql.ToString(), dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_ContractQuantities表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<Project_ContractQuantitiesEntity> GetProject_ContractQuantitiesList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_ContractQuantitiesEntity>(t => t.ProjectFinalId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_ContractMaterials表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<Project_ContractMaterialsEntity> GetProject_ContractMaterialsList(string keyValue)
        {
            try
            {
                var list = this.BaseRepository().FindList<Project_ContractMaterialsEntity>(t => t.ProjectFinalId == keyValue);
                var query = from item in list orderby item.CreationDate ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_Final表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public Project_FinalEntity GetProject_FinalEntity(string keyValue)
        {
            try
            {
                var sql = @"select a.*,b.InitAmount,b.TotalAmount,b.SettlementAmount,
                          (b.Name+'('+b.Code+')')as ContractFullName from 
                          Project_Final as a left join Project_Contract as b 
                          on a.ProjectContractId=b.ID ";
                sql += " where a.Id=@keyValue";
                return this.BaseRepository().FindEntity<Project_FinalEntity>(sql,new { keyValue});
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_ContractQuantities表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public Project_ContractQuantitiesEntity GetProject_ContractQuantitiesEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractQuantitiesEntity>(t => t.ProjectFinalId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 获取Project_ContractMaterials表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public Project_ContractMaterialsEntity GetProject_ContractMaterialsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_ContractMaterialsEntity>(t => t.ProjectFinalId == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_FinalEntity = GetProject_FinalEntity(keyValue);
                db.Delete<Project_FinalEntity>(t => t.ID == keyValue);
                db.Delete<Project_ContractQuantitiesEntity>(t => t.ProjectFinalId == project_FinalEntity.ID);
                db.Delete<Project_ContractMaterialsEntity>(t => t.ProjectFinalId == project_FinalEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void ProjectFinal(Project_ContractEntity contractInfo, Project_FinalEntity info)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                db.Update(info);
                db.Update(contractInfo);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void SaveEntity(string keyValue, Project_FinalEntity entity, List<Project_ContractQuantitiesEntity> project_ContractQuantitiesList, List<Project_ContractMaterialsEntity> project_ContractMaterialsList, string deleteList, string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type == "edit")
                {
                    var project_FinalEntityTmp = GetProject_FinalEntity(keyValue);
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson = deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }

                    //没有生成代码  
                    var Project_ContractQuantitiesUpdateList = project_ContractQuantitiesList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ContractQuantitiesUpdateList)
                    {
                        item.ProjectFinalId = project_FinalEntityTmp.ID;
                        db.Update(item);
                    }
                    var Project_ContractQuantitiesInserList = project_ContractQuantitiesList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_ContractQuantitiesInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectFinalId = project_FinalEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1  

                    //没有生成代码  
                    var Project_ContractMaterialsUpdateList = project_ContractMaterialsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ContractMaterialsUpdateList)
                    {
                        item.ProjectFinalId = project_FinalEntityTmp.ID;
                        db.Update(item);
                    }
                    var Project_ContractMaterialsInserList = project_ContractMaterialsList.FindAll(i => i.EditType == EditType.Add);
                    foreach (var item in Project_ContractMaterialsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectFinalId = project_FinalEntityTmp.ID;
                        db.Insert(item);
                    }

                    //没有生成代码1  
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);

                    var Project_ContractQuantitiesUpdateList = project_ContractQuantitiesList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ContractQuantitiesUpdateList)
                    {
                        item.ProjectFinalId = entity.ID;
                        db.Update(item);
                    }

                    var Project_ContractMaterialsUpdateList = project_ContractMaterialsList.FindAll(i => i.EditType == EditType.Update);
                    foreach (var item in Project_ContractMaterialsUpdateList)
                    {
                        item.ProjectFinalId = entity.ID;
                        db.Update(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
