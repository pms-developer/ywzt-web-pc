﻿using HuNanChangJie.Util;
using System;
using HuNanChangJie.SystemCommon;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2020-03-24 10:58 
    /// 描 述：完成决算 
    /// </summary> 
    public class Project_FinalEntity : BaseEntity
    {
        #region  实体成员 
        /// <summary> 
        /// 主键 
        /// </summary> 
        [Column("ID")]
        public string ID { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 决算金额
        /// </summary>
         [DecimalPrecision(18, 4)]
        public decimal? FinalAmount { get; set; }

        /// <summary> 
        /// 合同ID 
        /// </summary> 
        [Column("PROJECTCONTRACTID")]
        public string ProjectContractId { get; set; }
        /// <summary> 
        /// 决算时间 
        /// </summary> 
        [Column("FINALDATE")]
        public DateTime? FinalDate { get; set; }
        /// <summary> 
        /// 经办人 
        /// </summary> 
        [Column("OPERATORID")]
        public string OperatorId { get; set; }
        /// <summary> 
        /// 经办部门 
        /// </summary> 
        [Column("OPERATORDEPARTMENTID")]
        public string OperatorDepartmentId { get; set; }
        /// <summary> 
        /// 摘要 
        /// </summary> 
        [Column("ABSTRACT")]
        public string Abstract { get; set; }
        /// <summary> 
        /// 是否启用 
        /// </summary> 
        [Column("ENABLED")]
        public bool? Enabled { get; set; }
        /// <summary> 
        /// 创建时间 
        /// </summary> 
        [Column("CREATIONDATE")]
        public DateTime? CreationDate { get; set; }
        /// <summary> 
        /// 创建者ID 
        /// </summary> 
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        /// <summary> 
        /// 创建者名称 
        /// </summary> 
        [Column("CREATIONNAME")]
        public string CreationName { get; set; }
        /// <summary> 
        /// 修改时间 
        /// </summary> 
        [Column("MODIFICATIONDATE")]
        public DateTime? ModificationDate { get; set; }
        /// <summary> 
        /// 修改者ID 
        /// </summary> 
        [Column("MODIFICATION_ID")]
        public string Modification_Id { get; set; }
        /// <summary> 
        /// 修改名称 
        /// </summary> 
        [Column("MODIFICATIONNAME")]
        public string ModificationName { get; set; }
        /// <summary> 
        /// 排序号 
        /// </summary> 
        [Column("SORTCODE")]
        public int? SortCode { get; set; }
        /// <summary> 
        /// 公司编码 
        /// </summary> 
        [Column("COMPANYCODE")]
        public string CompanyCode { get; set; }
        /// <summary> 
        /// 公司ID 
        /// </summary> 
        [Column("COMPANY_ID")]
        public string Company_ID { get; set; }
        /// <summary> 
        /// 公司全称 
        /// </summary> 
        [Column("COMPANYFULLNAME")]
        public string CompanyFullName { get; set; }
        /// <summary> 
        /// 公司简称 
        /// </summary> 
        [Column("COMPANYNAME")]
        public string CompanyName { get; set; }
        /// <summary> 
        /// 拼音码 
        /// </summary> 
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary> 
        /// 拼音码首字母简写 
        /// </summary> 
        [Column("PINYINSHORT")]
        public string pinyinShort { get; set; }
        /// <summary> 
        /// 审核者ID 
        /// </summary> 
        [Column("AUDITOR_ID")]
        public string Auditor_ID { get; set; }
        /// <summary> 
        /// 审核者名称 
        /// </summary> 
        [Column("AUDITORNAME")]
        public string AuditorName { get; set; }
        /// <summary> 
        /// 审核状态 
        /// </summary> 
        [Column("AUDITSTATUS")]
        public string AuditStatus { get; set; }
        /// <summary> 
        /// 项目ID 
        /// </summary> 
        [Column("PROJECTID")]
        public string ProjectID { get; set; }
        /// <summary> 
        /// 项目名称 
        /// </summary> 
        [Column("PROJECTNAME")]
        public string ProjectName { get; set; }
        /// <summary> 
        /// 审批流ID 
        /// </summary> 
        [Column("WORKFLOW_ID")]
        public string Workflow_ID { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }

        /// <summary> 
        /// 新增调用ID由前端传入 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.CreationDate = DateTime.Now;
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
            this.Company_ID = userInfo.companyId;
            this.CompanyCode = userInfo.CompanyCode;
            this.CompanyName = userInfo.CompanyName;
            this.CompanyFullName = userInfo.CompanyFullName;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段 

        /// <summary>
        /// InitAmount
        /// </summary>
        [NotMapped]
        public decimal? InitAmount { get; set; }

        /// <summary>
        /// 当前合同金额
        /// </summary>
        [NotMapped]
        public decimal? TotalAmount { get; set; }

        /// <summary>
        /// 产值结算金额
        /// </summary>
        [NotMapped]
        public decimal? SettlementAmount { get; set; }

        /// <summary>
        /// 合同全名(name+code)
        /// </summary>
        [NotMapped]
        public string ContractFullName { get; set; }

        #endregion
    }
}
