﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary> 
    ///  
    ///   
    /// 创 建：超级管理员 
    /// 日 期：2020-03-24 10:58 
    /// 描 述：完成决算 
    /// </summary> 
    public interface ProjectFinalIBLL
    {
        #region  获取数据 

        /// <summary> 
        /// 获取页面显示列表数据 
        /// <summary> 
        /// <param name="queryJson">查询参数</param> 
        /// <returns></returns> 
        IEnumerable<Project_FinalEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary> 
        /// 获取Project_ContractQuantities表数据 
        /// <summary> 
        /// <returns></returns> 
        IEnumerable<Project_ContractQuantitiesEntity> GetProject_ContractQuantitiesList(string keyValue);
        /// <summary> 
        /// 获取Project_ContractMaterials表数据 
        /// <summary> 
        /// <returns></returns> 
        IEnumerable<Project_ContractMaterialsEntity> GetProject_ContractMaterialsList(string keyValue);
        /// <summary> 
        /// 获取Project_Final表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        Project_FinalEntity GetProject_FinalEntity(string keyValue);
        /// <summary> 
        /// 获取Project_ContractQuantities表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        Project_ContractQuantitiesEntity GetProject_ContractQuantitiesEntity(string keyValue);
        /// <summary> 
        /// 获取Project_ContractMaterials表实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        Project_ContractMaterialsEntity GetProject_ContractMaterialsEntity(string keyValue);
        #endregion

        #region  提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        void DeleteEntity(string keyValue);
        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        void SaveEntity(string keyValue, Project_FinalEntity entity, List<Project_ContractQuantitiesEntity> project_ContractQuantitiesList, List<Project_ContractMaterialsEntity> project_ContractMaterialsList, string deleteList, string type);
        void Audit(string keyValue);
        void UnAudit(string keyValue);
        #endregion

    }
}
