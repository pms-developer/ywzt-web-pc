﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangeJie.Application.Project.ProjectSubject;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-29 17:02
    /// 描 述：证照使用工资
    /// </summary>
    public class ProjectLicenseWageService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_LicenseWageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.ID,
                t.AccrueDate,
                t.TotalAmount,
                t.MonthTotalAmount,
                t.AccrueState,
                t.ModificationName,
                t.CreationDate,
                t.CompanyCode,
                t.Company_ID,
                t.Enabled,
                t.Workflow_ID,
                t.AuditStatus
                ");
                strSql.Append("  FROM Project_LicenseWage t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" and t.ProjectId=@ProjectId");
                }
                var list=this.BaseRepository().FindList<Project_LicenseWageEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseWageDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_LicenseWageDetailsEntity> GetProject_LicenseWageDetailsList(string keyValue)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.zz_xinming,
                            b.zz_bianhao,
                            b.zz_shenfengzheng,
                            c.zzmc_name 
                            from Project_LicenseWageDetails as a left join Base_CJ_ZhengZhao as b
                            on a.ZhengZhaoId=b.ID
                            left join Base_CJ_ZhenZhaoMingCheng as c 
                            on b.fk_zhengzhaomingcheng=c.zzmc_id");
                sb.Append(" where a.ProjectLicenseWageID=@keyValue");
                var list= this.BaseRepository().FindList<Project_LicenseWageDetailsEntity>(sb.ToString(),new { keyValue} );
                var query = from item in list orderby item.SortCode ascending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseWage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_LicenseWageEntity GetProject_LicenseWageEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_LicenseWageEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseWageDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_LicenseWageDetailsEntity GetProject_LicenseWageDetailsEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_LicenseWageDetailsEntity>(t=>t.ProjectLicenseWageID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                var project_LicenseWageEntity = GetProject_LicenseWageEntity(keyValue); 
                db.Delete<Project_LicenseWageEntity>(t=>t.ID == keyValue);
                db.Delete<Project_LicenseWageDetailsEntity>(t=>t.ProjectLicenseWageID == project_LicenseWageEntity.ID);
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<ProjectEntity> GetAllProjectList()
        {
            
            try
            {
                return this.BaseRepository().FindList<ProjectEntity>(i => i.AuditStatus == "2");
            }
            catch (Exception ex)
            {
                
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<Project_LicenseWageEntity> GetLicenseWageList(IEnumerable<string> projectIds)
        {
            try
            {
                return this.BaseRepository().FindList<Project_LicenseWageEntity>(i => projectIds.Contains(i.ProjectID) && i.AuditStatus=="2" &&i.AccrueState!=true);
            }
            catch (Exception ex)
            {

                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<ProjectSubjectEntity> GetProjectSubjectList(IEnumerable<string> projectSubjectIds)
        {
            try
            {
                return this.BaseRepository().FindList<ProjectSubjectEntity>(i => projectSubjectIds.Contains(i.ID));
            }
            catch (Exception ex)
            {

                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal IEnumerable<Project_LicenseWageDetailsEntity> GetLicenseWageDetailsList(IEnumerable<string> licenseWageIds)
        {
            try
            {
                return this.BaseRepository().FindList<Project_LicenseWageDetailsEntity>(i => licenseWageIds.Contains(i.ProjectLicenseWageID));
            }
            catch (Exception ex)
            {

                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public IEnumerable<Project_LicenseWageDetailsEntity> GetDetails(string projectId)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(@"select a.*,
                            b.zz_xinming,
                            b.zz_bianhao,
                            b.zz_shenfengzheng,
                            c.zzmc_name 
							from Project_LicenseWageDetails as a
							left join Base_CJ_ZhengZhao as b
                            on a.ZhengZhaoId=b.ID
                            left join Base_CJ_ZhenZhaoMingCheng as c 
                            on b.fk_zhengzhaomingcheng=c.zzmc_id
							left join Project_LicenseWage as d
                            on a.ProjectLicenseWageId=d.id
                            where d.AuditStatus='2' ");
                sb.Append(" and a.projectId=@projectId");
                return BaseRepository().FindList<Project_LicenseWageDetailsEntity>(sb.ToString(), new { projectId });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public Project_LicenseWageEntity GetFormInfo(string projectId)
        {
            try
            {
                return BaseRepository().FindEntity<Project_LicenseWageEntity>(i => i.ProjectID == projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_LicenseWageEntity entity,List<Project_LicenseWageDetailsEntity> project_LicenseWageDetailsList,string deleteList,string type)
        {
            var db = this.BaseRepository().BeginTrans();
            try
            {
                if (type=="edit")
                {
                    var project_LicenseWageEntityTmp = GetProject_LicenseWageEntity(keyValue); 
                    entity.Modify(keyValue);
                    db.Update(entity);
                    var delJson=deleteList.ToObject<List<JObject>>();
                    foreach (var del in delJson)
                    {
                        var idList = del["idList"].ToString();
                        var sql = "";
                        if (idList.Contains(","))
                        {
                            var ids = idList.Split(',');
                            foreach (var id in ids)
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{id}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(idList))
                            {
                                sql = $" DELETE FROM { del["TableName"].ToString()} WHERE id ='{idList}' ";
                                databaseLinkIBLL.ExecuteBySqlTrans(sql, new { }, db);
                            }
                        }
                    }
                    
                    //没有生成代码 
                    var Project_LicenseWageDetailsUpdateList= project_LicenseWageDetailsList.FindAll(i=>i.EditType==EditType.Update);
                    foreach (var item in Project_LicenseWageDetailsUpdateList)
                    {
                        db.Update(item);
                    }
                    var Project_LicenseWageDetailsInserList= project_LicenseWageDetailsList.FindAll(i=>i.EditType==EditType.Add);
                    foreach (var item in Project_LicenseWageDetailsInserList)
                    {
                        item.Create(item.ID);
                        item.ProjectLicenseWageID = project_LicenseWageEntityTmp.ID;
                        db.Insert(item);
                    }
                    
                    //没有生成代码1 
                }
                else
                {
                    entity.Create(keyValue);
                    db.Insert(entity);
                    foreach (Project_LicenseWageDetailsEntity item in project_LicenseWageDetailsList)
                    {
                        item.ProjectLicenseWageID = entity.ID;
                        db.Insert(item);
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
