﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangeJie.Application.Project.ProjectSubject;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-29 17:02
    /// 描 述：证照使用工资
    /// </summary>
    public interface ProjectLicenseWageIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<Project_LicenseWageEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取Project_LicenseWageDetails表数据
        /// <summary>
        /// <returns></returns>
        IEnumerable<Project_LicenseWageDetailsEntity> GetProject_LicenseWageDetailsList(string keyValue);
        /// <summary>
        /// 获取Project_LicenseWage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_LicenseWageEntity GetProject_LicenseWageEntity(string keyValue);
        /// <summary>
        /// 获取Project_LicenseWageDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        Project_LicenseWageDetailsEntity GetProject_LicenseWageDetailsEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, Project_LicenseWageEntity entity,List<Project_LicenseWageDetailsEntity> project_LicenseWageDetailsList,string deleteList,string type);
        Project_LicenseWageEntity GetFormInfo(string projectId);
        IEnumerable<Project_LicenseWageDetailsEntity> GetDetails(string projectId);

        /// <summary>
        /// 获取所有已审核的项目信息
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProjectEntity> GetAllProjectList();

        /// <summary>
        /// 根据项目ID集合获取项目证照使用工资集合(已审核)
        /// </summary>
        /// <param name="projectIds">项目ID集合</param>
        /// <returns></returns>
        IEnumerable<Project_LicenseWageEntity> GetLicenseWageList(IEnumerable<string> projectIds);

        /// <summary>
        /// 获取证照使用工资对应的科目信息
        /// </summary>
        /// <param name="projectSubjectIds">证照工资ID集合</param>
        /// <returns></returns>
        IEnumerable<ProjectSubjectEntity> GetProjectSubjectList(IEnumerable<string> projectSubjectIds);

        /// <summary>
        /// 根据证照工资ID集合获取证照使用工资详情集合
        /// </summary>
        /// <param name="licenseWageIds">证照工资ID集合</param>
        /// <returns></returns>
        IEnumerable<Project_LicenseWageDetailsEntity> GetLicenseWageDetailsList(IEnumerable<string> licenseWageIds);

        #endregion

    }
}
