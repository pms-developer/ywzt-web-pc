﻿using HuNanChangJie.Util;
using System;
using System.Data;
using System.Collections.Generic;
using HuNanChangeJie.Application.Project.Model;
using HuNanChangeJie.Application.Project.ProjectSubject;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-29 17:02
    /// 描 述：证照使用工资
    /// </summary>
    public class ProjectLicenseWageBLL : ProjectLicenseWageIBLL
    {
        private ProjectLicenseWageService projectLicenseWageService = new ProjectLicenseWageService();

        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_LicenseWageEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                return projectLicenseWageService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseWageDetails表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<Project_LicenseWageDetailsEntity> GetProject_LicenseWageDetailsList(string keyValue)
        {
            try
            {
                return projectLicenseWageService.GetProject_LicenseWageDetailsList(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseWage表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_LicenseWageEntity GetProject_LicenseWageEntity(string keyValue)
        {
            try
            {
                return projectLicenseWageService.GetProject_LicenseWageEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_LicenseWageDetails表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_LicenseWageDetailsEntity GetProject_LicenseWageDetailsEntity(string keyValue)
        {
            try
            {
                return projectLicenseWageService.GetProject_LicenseWageDetailsEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                projectLicenseWageService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_LicenseWageEntity entity,List<Project_LicenseWageDetailsEntity> project_LicenseWageDetailsList,string deleteList,string type)
        {
            try
            {
                projectLicenseWageService.SaveEntity(keyValue, entity,project_LicenseWageDetailsList,deleteList,type);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public Project_LicenseWageEntity GetFormInfo(string projectId)
        {
            try
            {
                return projectLicenseWageService.GetFormInfo(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_LicenseWageDetailsEntity> GetDetails(string projectId)
        {
            try
            {
                return projectLicenseWageService.GetDetails(projectId);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<ProjectEntity> GetAllProjectList()
        {
            try
            {
                return projectLicenseWageService.GetAllProjectList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_LicenseWageEntity> GetLicenseWageList(IEnumerable<string> projectIds)
        {
            try
            {
                return projectLicenseWageService.GetLicenseWageList(projectIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<ProjectSubjectEntity> GetProjectSubjectList(IEnumerable<string> projectSubjectIds)
        {
            try
            {
                return projectLicenseWageService.GetProjectSubjectList(projectSubjectIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }

        public IEnumerable<Project_LicenseWageDetailsEntity> GetLicenseWageDetailsList(IEnumerable<string> licenseWageIds)
        {
            try
            {
                return projectLicenseWageService.GetLicenseWageDetailsList(licenseWageIds);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
        }



        #endregion

    }
}
