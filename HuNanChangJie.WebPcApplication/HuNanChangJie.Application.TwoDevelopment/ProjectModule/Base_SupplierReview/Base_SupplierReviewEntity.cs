﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule

{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-13 10:52
    /// 描 述：供应商评审
    /// </summary>
    public class Base_SupplierReviewEntity 
    {
        #region  实体成员
        /// <summary>
        /// ID
        /// </summary>
        /// <returns></returns>
        [Column("ID")]
        public string ID { get; set; }
        /// <summary>
        /// FK_SupplierId
        /// </summary>
        /// <returns></returns>
        [Column("FK_SUPPLIERID")]
        public string FK_SupplierId { get; set; }
        /// <summary>
        /// CreateDate
        /// </summary>
        /// <returns></returns>
        [Column("CREATEDATE")]
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// 合作状态
        /// </summary>
        /// <returns></returns>
        [Column("STATUS")]
        public string Status { get; set; }
        /// <summary>
        /// 等级
        /// </summary>
        /// <returns></returns>
        [Column("GRADE")]
        public string Grade { get; set; }
        /// <summary>
        /// 产品质量
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCTQUALITY")]
        public string ProductQuality { get; set; }
        /// <summary>
        /// 售后服务
        /// </summary>
        /// <returns></returns>
        [Column("AFTERSERVICE")]
        public string AfterService { get; set; }
        /// <summary>
        /// 技术支持
        /// </summary>
        /// <returns></returns>
        [Column("TECHNICALSUPPORT")]
        public string TechnicalSupport { get; set; }
        /// <summary>
        /// 诚信度
        /// </summary>
        /// <returns></returns>
        [Column("INTEGRITY")]
        public string Integrity { get; set; }
        /// <summary>
        /// 评审意见
        /// </summary>
        /// <returns></returns>
        [Column("REVIEWOPINION")]
        public string ReviewOpinion { get; set; }
        /// <summary>
        /// Creation_Id
        /// </summary>
        /// <returns></returns>
        [Column("CREATION_ID")]
        public string Creation_Id { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
        }
        
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create(string keyValue)
        {
            this.ID = keyValue;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
        }
        
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}

