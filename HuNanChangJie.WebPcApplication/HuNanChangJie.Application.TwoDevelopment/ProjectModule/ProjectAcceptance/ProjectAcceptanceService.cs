﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Application.Base.SystemModule;
using System.Text;

using System.Linq;
using HuNanChangeJie.Application.Project.Model;

namespace HuNanChangJie.Application.TwoDevelopment.ProjectModule
{


    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2020-03-24 09:48
    /// 描 述：项目验收
    /// </summary>
    public class ProjectAcceptanceService : RepositoryFactory
    {
         private DatabaseLinkIBLL databaseLinkIBLL = new DatabaseLinkBLL();
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<Project_AcceptanceEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                                a.*,
                                b.*,
                                c.FullName as CustomerName 
                                from Project_Acceptance as a  
                                left join ");
                strSql.Append(@"
                                (select projectId as pid,jia,
                                sum(InitAmount)as InitAmount,
                                sum(SettlementAmount) as SettlementAmount,
                                sum(InvioceAmount) as InvioceAmount,
                                sum(ReceivedAmount) as ReceivedAmount
                                from Project_Contract 
                                where Auditstatus='2'  
                                group by projectId,jia ) as b");
                strSql.Append(@"
                                on a.ProjectID=b.pid
                                left join Base_Customer as c on b.Jia=c.ID");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
                var dp = new DynamicParameters(new { });
                if (!queryParam["ProjectId"].IsEmpty())
                {
                    dp.Add("ProjectId", queryParam["ProjectId"].ToString(), DbType.String);
                    strSql.Append(" and a.ProjectId=@ProjectId");
                }
                var list=this.BaseRepository().FindList<Project_AcceptanceEntity>(strSql.ToString(),dp, pagination);
                var query = from item in list orderby item.CreationDate descending select item;
                return query;
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取Project_Acceptance表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public Project_AcceptanceEntity GetProject_AcceptanceEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<Project_AcceptanceEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        public void ProjectAcceptance(ProjectEntity projectInfo)
        {
            try
            {
                BaseRepository().Update(projectInfo);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);

                }
            }
        }

                #endregion

                #region  提交数据

                /// <summary>
                /// 删除实体数据
                /// <param name="keyValue">主键</param>
                /// <summary>
                /// <returns></returns>
                public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<Project_AcceptanceEntity>(t=>t.ID == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, Project_AcceptanceEntity entity,string deleteList,string type)
        {
            try
            {
                if (type=="edit")
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create(keyValue);
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
