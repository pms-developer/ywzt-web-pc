﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PortalSite
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-29 15:22
    /// 描 述：文章
    /// </summary>
    public interface ArticleIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<PS_ArticleEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取PS_Article表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        PS_ArticleEntity GetPS_ArticleEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, PS_ArticleEntity entity);
        List<PS_ArticleEntity> GetList(string queryJson);
        #endregion

    }
}
