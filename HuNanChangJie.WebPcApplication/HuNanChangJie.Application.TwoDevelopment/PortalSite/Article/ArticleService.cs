﻿using Dapper;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HuNanChangJie.Application.TwoDevelopment.PortalSite
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-29 15:22
    /// 描 述：文章
    /// </summary>
    public class ArticleService : RepositoryFactory
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        public IEnumerable<PS_ArticleEntity> GetPageList(XqPagination pagination, string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_Id,
                t.F_Title,
                t.F_ImgName,
                t.F_Img,
                t.F_Category,
                t.F_PushDate,
                t.F_Content
                ");
                strSql.Append("  FROM PS_Article t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数
              
                var dp = new DynamicParameters(new { });
                if (!queryParam["dateBegin"].IsEmpty() && !queryParam["dateEnd"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["dateBegin"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["dateEnd"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                if (!queryParam["F_Title"].IsEmpty())
                {
                    dp.Add("title","%"+ queryParam["F_Title"]+"%", DbType.String);
                    strSql.Append(" AND ( t.F_Title  like  @title) ");
                }
                //
                if (!queryParam["F_Category"].IsEmpty())
                {
                    dp.Add("category",  queryParam["F_Category"].ToString()  , DbType.String);
                    strSql.Append(" AND ( t.F_Category  =  @category) ");
                }
                return this.BaseRepository().FindList<PS_ArticleEntity>(strSql.ToString(),dp, pagination);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 获取PS_Article表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public PS_ArticleEntity GetPS_ArticleEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<PS_ArticleEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        internal List<PS_ArticleEntity> GetList(string queryJson)
        {
            try
            {
                var strSql = new StringBuilder();
                strSql.Append("SELECT ");
                strSql.Append(@"
                t.F_Id,
                t.F_Title,
                t.F_ImgName,
                t.F_Img,
                t.F_Category,
                t.F_PushDate,
                t.F_Content
                ");
                strSql.Append("  FROM PS_Article t ");
                strSql.Append("  WHERE 1=1 ");
                var queryParam = queryJson.ToJObject();
                // 虚拟参数

                var dp = new DynamicParameters(new { });
                if (!queryParam["dateBegin"].IsEmpty() && !queryParam["dateEnd"].IsEmpty())
                {
                    dp.Add("startTime", queryParam["dateBegin"].ToDate(), DbType.DateTime);
                    dp.Add("endTime", queryParam["dateEnd"].ToDate(), DbType.DateTime);
                    strSql.Append(" AND ( t.CreationDate >= @startTime AND t.CreationDate <= @endTime ) ");
                }

                if (!queryParam["F_Title"].IsEmpty())
                {
                    dp.Add("title", "%" + queryParam["F_Title"] + "%", DbType.String);
                    strSql.Append(" AND ( t.F_Title  like  @title) ");
                }
                //
                if (!queryParam["category"].IsEmpty())
                {
                    dp.Add("category", queryParam["category"].ToString(), DbType.String);
                    strSql.Append(" AND ( t.F_Category  =  @category) ");
                }
                return this.BaseRepository().FindList<PS_ArticleEntity>(strSql.ToString(), dp).ToList();
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete<PS_ArticleEntity>(t=>t.F_Id == keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, PS_ArticleEntity entity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    entity.Modify(keyValue);
                    this.BaseRepository().Update(entity);
                }
                else
                {
                    entity.Create();
                    this.BaseRepository().Insert(entity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }
        }

        #endregion

    }
}
