﻿using HuNanChangJie.Util;
using System.Data;
using System.Collections.Generic;

namespace HuNanChangJie.Application.TwoDevelopment.PortalSite
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-29 15:12
    /// 描 述：首页配置
    /// </summary>
    public interface HomeConfigIBLL
    {
        #region  获取数据

        /// <summary>
        /// 获取页面显示列表数据
        /// <summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns></returns>
        IEnumerable<PS_HomeConfigEntity> GetPageList(XqPagination pagination, string queryJson);
        /// <summary>
        /// 获取PS_HomeConfig表实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        PS_HomeConfigEntity GetPS_HomeConfigEntity(string keyValue);
        #endregion

        #region  提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void DeleteEntity(string keyValue);
        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        void SaveEntity(string keyValue, PS_HomeConfigEntity entity);
        /// <summary>
        /// 根据类型获取配置
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        List<PS_HomeConfigEntity> GetList(int type);
        List<PS_HomeConfigEntity> GetAllList();
        #endregion

    }
}
