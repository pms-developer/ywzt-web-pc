﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuNanChangJie.Application.TwoDevelopment.PortalSite
{
    /// <summary>
    /// 
    ///  
    /// 创 建：超级管理员
    /// 日 期：2019-01-29 15:23
    /// 描 述：子页面管理
    /// </summary>
    public class PS_PageEntity 
    {
        #region  实体成员
        /// <summary>
        /// 主键
        /// </summary>
        [Column("F_ID")]
        public string F_Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [Column("F_TITLE")]
        public string F_Title { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        [Column("F_IMG")]
        public string F_Img { get; set; }
        /// <summary>
        /// 类型1.列表2图形列表3详细信息
        /// </summary>
        [Column("F_TYPE")]
        public string F_Type { get; set; }
        /// <summary>
        /// 页面配置模板
        /// </summary>
        [Column("F_SCHEME")]
        public string F_Scheme { get; set; }
        /// <summary>
        /// 创建人ID
        /// </summary>
        [Column("Creation_Id")]
        public string Creation_Id { get; set; }
        /// <summary>
        /// 编辑人
        /// </summary>
        [Column("ModificationName")]
        public string ModificationName { get; set; }
        /// <summary>
        /// 编辑人ID
        /// </summary>
        [Column("Modification_Id")]
        public string Modification_Id { get; set; }
        /// <summary>
        /// 编辑日期
        /// </summary>
        [Column("ModificationDate")]
        public DateTime? ModificationDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        [Column("CreationName")]
        public string CreationName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Column("CreationDate")]
        public DateTime? CreationDate { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.F_Id = Guid.NewGuid().ToString();
            this.CreationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Creation_Id = userInfo.userId;
            this.CreationName = userInfo.realName;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.F_Id = keyValue;
            this.ModificationDate = DateTime.Now;
            UserInfo userInfo = LoginUserInfo.Get();
            this.Modification_Id = userInfo.userId;
            this.ModificationName = userInfo.realName;
        }
        #endregion
        #region  扩展字段
        #endregion
    }
}

