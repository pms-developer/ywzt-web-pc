﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;

using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.Site_Order

{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-28 11:12
    /// 描 述：产品库
    /// </summary>
    public class tProductEntity 
    {
        #region  实体成员
        /// <summary>
        /// Product_Id
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCT_ID")]
        public Guid Product_Id { get; set; }
        /// <summary>
        /// Category_Id
        /// </summary>
        /// <returns></returns>
        [Column("CATEGORY_ID")]
        public int? Category_Id { get; set; }
        /// <summary>
        /// Product_Name
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCT_NAME")]
        public string Product_Name { get; set; }
        /// <summary>
        /// Product_Price
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCT_PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Product_Price { get; set; }
        /// <summary>
        /// Product_No
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCT_NO")]
        public int? Product_No { get; set; }
        /// <summary>
        /// IsView
        /// </summary>
        /// <returns></returns>
        [Column("ISVIEW")]
        public int? IsView { get; set; }
        /// <summary>
        /// Product_Type
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCT_TYPE")]
        public string Product_Type { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Product_Id = Guid.NewGuid();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Product_Id = Guid.Parse(keyValue);
                ;
        }
        #endregion
    }
}

