﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.Site_Order

{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-28 11:11
    /// 描 述：老订单库
    /// </summary>
    public class tOrderEntity 
    {
        #region  实体成员
        /// <summary>
        /// Order_Id
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_ID")]
        public Guid Order_Id { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_NO")]
        public string Order_No { get; set; }
        /// <summary>
        /// 订单提交时间
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_TIME")]
        public DateTime? Order_Time { get; set; }
        /// <summary>
        /// Order_UnitRegionId
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_UNITREGIONID")]
        public Guid Order_UnitRegionId { get; set; }
        /// <summary>
        /// Order_UnitRegionName
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_UNITREGIONNAME")]
        public string Order_UnitRegionName { get; set; }
        /// <summary>
        /// Order_UnitName
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_UNITNAME")]
        public string Order_UnitName { get; set; }
        /// <summary>
        /// Order_UnitAddress
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_UNITADDRESS")]
        public string Order_UnitAddress { get; set; }
        /// <summary>
        /// 邮政编码
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_POSTALCODE")]
        public string Order_PostalCode { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_CONTACT")]
        public string Order_Contact { get; set; }
        /// <summary>
        /// 固定电话
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_PHONE")]
        public string Order_Phone { get; set; }
        /// <summary>
        /// 移动电话
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_MOBILE")]
        public string Order_Mobile { get; set; }
        /// <summary>
        /// 传真号码
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_FAX")]
        public string Order_Fax { get; set; }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_EMAIL")]
        public string Order_Email { get; set; }
        /// <summary>
        /// 发票开具单位
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_INVOICETITLE")]
        public string Order_InvoiceTitle { get; set; }
        /// <summary>
        /// 订单备注
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_MEMO")]
        public string Order_Memo { get; set; }
        /// <summary>
        /// 订单价格
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_PRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? Order_Price { get; set; }
        /// <summary>
        /// 订单状态 0：未处理1：已处理
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_STATUS")]
        public int? Order_Status { get; set; }
        /// <summary>
        /// 支付状态0：待付款 1：已付款
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_PAYMENTSTATUS")]
        public int? Order_PaymentStatus { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_PAYMENTTIME")]
        public DateTime? Order_PaymentTime { get; set; }
        /// <summary>
        /// 发货状态
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_SENDSTATUS")]
        public int? Order_SendStatus { get; set; }
        /// <summary>
        /// 发货时间
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_SENDTIME")]
        public DateTime? Order_SendTime { get; set; }
        /// <summary>
        /// 发货备注
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_SENDMEMO")]
        public string Order_SendMemo { get; set; }
        /// <summary>
        /// Order_PrintTimes
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_PRINTTIMES")]
        public int? Order_PrintTimes { get; set; }
        /// <summary>
        /// Order_PrintTime
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_PRINTTIME")]
        public DateTime? Order_PrintTime { get; set; }
        /// <summary>
        /// 0：未作废1：已作废
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_CANCELLED")]
        public int? Order_Cancelled { get; set; }
        /// <summary>
        /// 0：未删除1：已删除
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_DELETED")]
        public int? Order_Deleted { get; set; }
        /// <summary>
        /// Order_DeleteTime
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_DELETETIME")]
        public DateTime? Order_DeleteTime { get; set; }
        /// <summary>
        /// 订单创建时间
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_CREATETIME")]
        public DateTime? Order_CreateTime { get; set; }
        /// <summary>
        /// Order_ModifyTime
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_MODIFYTIME")]
        public DateTime? Order_ModifyTime { get; set; }
        /// <summary>
        /// Order_CreatorId
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_CREATORID")]
        public Guid Order_CreatorId { get; set; }
        /// <summary>
        /// Order_ModifierId
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_MODIFIERID")]
        public Guid Order_ModifierId { get; set; }
        /// <summary>
        /// Order_DeleterId
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_DELETERID")]
        public Guid Order_DeleterId { get; set; }
        /// <summary>
        /// Order_ExpressCompany
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_EXPRESSCOMPANY")]
        public string Order_ExpressCompany { get; set; }
        /// <summary>
        /// Order_ExpressNo
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_EXPRESSNO")]
        public string Order_ExpressNo { get; set; }
        /// <summary>
        /// Order_ExpressSmsStatus
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_EXPRESSSMSSTATUS")]
        public int? Order_ExpressSmsStatus { get; set; }
        /// <summary>
        /// Order_ExpressSmsTime
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_EXPRESSSMSTIME")]
        public DateTime? Order_ExpressSmsTime { get; set; }
        /// <summary>
        /// Order_EmailBinded
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_EMAILBINDED")]
        public string Order_EmailBinded { get; set; }
        /// <summary>
        /// Order_MobileBinded
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_MOBILEBINDED")]
        public string Order_MobileBinded { get; set; }
        /// <summary>
        /// Order_NeedInvoice
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_NEEDINVOICE")]
        public string Order_NeedInvoice { get; set; }
        /// <summary>
        /// Order_Appendix
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_APPENDIX")]
        public string Order_Appendix { get; set; }
        /// <summary>
        /// 税号
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_INVOICETAXPAYERID")]
        public string Order_InvoiceTaxpayerId { get; set; }
        /// <summary>
        /// 单位地址
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_INVOICETAXADDRESS")]
        public string Order_InvoiceTaxAddress { get; set; }
        /// <summary>
        /// 单位电话
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_INVOICETAXTELNO")]
        public string Order_InvoiceTaxTelNo { get; set; }
        /// <summary>
        /// 开户行
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_INVOICETAXBANK")]
        public string Order_InvoiceTaxBank { get; set; }
        /// <summary>
        /// 银行账号
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_INVOICETAXACCOUNT")]
        public string Order_InvoiceTaxAccount { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.Order_Id = Guid.NewGuid();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.Order_Id = Guid.Parse(keyValue) ;
        }
        #endregion
    }
}

