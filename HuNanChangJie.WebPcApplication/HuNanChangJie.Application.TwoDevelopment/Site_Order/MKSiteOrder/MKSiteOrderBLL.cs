﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangJie.Application.TwoDevelopment.MKSiteOrder;
using HuNanChangJie.Util;

namespace HuNanChangJie.Application.TwoDevelopment.Site_Order
{
    public class MKSiteOrderBLL : MKSiteOrderIBLL
    {
        MKSiteOrderService mkSiteOrder = new MKSiteOrderService();
        public MKSiteOrderBLL()
        {
        }

        public void SaveEntity(string keyValue, tOrderEntity order, tOrderDetailsEntity orderdetail, tProductEntity product)
        {
            try
            {
                mkSiteOrder.SaveEntity(keyValue, order, orderdetail, product);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowBusinessException(ex);
                }
            }
           
        }
    }
}
