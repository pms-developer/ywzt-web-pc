﻿using HuNanChangJie.Application.TwoDevelopment.Site_Order;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.Util;
using System;

namespace HuNanChangJie.Application.TwoDevelopment.MKSiteOrder
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-02-27 16:27
    /// 描 述：消息策略
    /// </summary>
    public class MKSiteOrderService : RepositoryFactory
    {
        /// <summary>
        /// 向老库中添加数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="order"></param>
        /// <param name="orderdetail"></param>
        /// <param name="product"></param>
        public   void SaveEntity(string keyValue, tOrderEntity order, tOrderDetailsEntity orderdetail, tProductEntity product)
        {
            var db = this.BaseRepository("changjiesiteorder");
            try
            {
                var orderentity = db.FindEntity<tOrderEntity>(p => p.Order_No == order.Order_No);
                var tproduct = db.FindEntity<tProductEntity>(p => p.Product_Name == product.Product_Name);

               
                if (orderentity == null && tproduct != null)
                {
                    //插入新的订单信息
                    order.Create();
                    db.Insert(order);
                    //插入订单详细信息
                    orderdetail.Create();
                    orderdetail.Order_Id = order.Order_Id;
                    db.Insert(orderdetail);
                    db.Commit();
                }
                else
                {
                    //更新的订单信息

                    if (orderentity != null)
                        order.Order_Id = orderentity.Order_Id;
                    db.Update(order);

                    var orderdetails = db.FindList<tOrderDetailsEntity>(p => p.Order_Id == orderentity.Order_Id);
                    //更新订单详情
                    foreach (var od in orderdetails)
                    {
                        db.Delete(od);
                    }
                    orderdetail.Create();
                    orderdetail.Order_Id = order.Order_Id;
                    db.Insert(orderdetail);
                    db.Commit();
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                if (ex is ExceptionEx)
                {
                    throw;
                }
                else
                {
                    throw ExceptionEx.ThrowServiceException(ex);
                }
            }


        }
    }
}



