﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Application.TwoDevelopment.Site_Order
{
   public  interface MKSiteOrderIBLL
   {
       /// <summary>
       /// 向老库中添加数据 通过定时任务自动添加
       /// </summary>
       /// <param name="keyValue"></param>
       /// <param name="order"></param>
       /// <param name="orderdetail"></param>
       /// <param name="product"></param>
         void SaveEntity(string keyValue, tOrderEntity order, tOrderDetailsEntity orderdetail, tProductEntity product);  
   }
}
