﻿using HuNanChangJie.Util;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using HuNanChangJie.Util.Attributes;

namespace HuNanChangJie.Application.TwoDevelopment.Site_Order


{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-04-28 11:12
    /// 描 述：订单详情库
    /// </summary>
    public class tOrderDetailsEntity 
    {
        #region  实体成员
        /// <summary>
        /// OrderDetail_Id
        /// </summary>
        /// <returns></returns>
        [Column("ORDERDETAIL_ID")]
        public Guid OrderDetail_Id { get; set; }
        /// <summary>
        /// Order_Id
        /// </summary>
        /// <returns></returns>
        [Column("ORDER_ID")]
        public Guid Order_Id { get; set; }
        /// <summary>
        /// Product_Id
        /// </summary>
        /// <returns></returns>
        [Column("PRODUCT_ID")]
        public Guid Product_Id { get; set; }
        /// <summary>
        /// OrderDetail_UnitName
        /// </summary>
        /// <returns></returns>
        [Column("ORDERDETAIL_UNITNAME")]
        public string OrderDetail_UnitName { get; set; }
        /// <summary>
        /// OrderDetail_UnitRegionId
        /// </summary>
        /// <returns></returns>
        [Column("ORDERDETAIL_UNITREGIONID")]
        public Guid OrderDetail_UnitRegionId { get; set; }
        /// <summary>
        /// OrderDetail_UnitRegionName
        /// </summary>
        /// <returns></returns>
        [Column("ORDERDETAIL_UNITREGIONNAME")]
        public string OrderDetail_UnitRegionName { get; set; }
        /// <summary>
        /// OrderDetail_ProductNum
        /// </summary>
        /// <returns></returns>
        [Column("ORDERDETAIL_PRODUCTNUM")]
        public int? OrderDetail_ProductNum { get; set; }
        /// <summary>
        /// OrderDetail_ProductPrice
        /// </summary>
        /// <returns></returns>
        [Column("ORDERDETAIL_PRODUCTPRICE")]
        [DecimalPrecision(18, 4)]
        public decimal? OrderDetail_ProductPrice { get; set; }
        /// <summary>
        /// OrderDetail_CreateTime
        /// </summary>
        /// <returns></returns>
        [Column("ORDERDETAIL_CREATETIME")]
        public DateTime? OrderDetail_CreateTime { get; set; }
        #endregion

        #region  扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public void Create()
        {
            this.OrderDetail_Id = Guid.NewGuid();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public void Modify(string keyValue)
        {
            this.OrderDetail_Id = Guid.Parse(keyValue) ;
        }
        #endregion
    }
}

