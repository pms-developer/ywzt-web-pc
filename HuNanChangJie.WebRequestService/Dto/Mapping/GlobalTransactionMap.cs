﻿using System.Data.Entity.ModelConfiguration;

namespace HuNanChangJie.WebRequestService
{
    public class GlobalTransactionMap : EntityTypeConfiguration<GlobalTransactionEntity>
    {
        public GlobalTransactionMap() {
            #region  表、主键
            //表
            this.ToTable("GLOBAL_TRANSACTION");
            //主键
            this.HasKey(t => t.t_id);
            #endregion

            #region  配置关系
            #endregion
        }
    }
}
