﻿using HuNanChangJie.SystemCommon;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class GlobalTransactionEntity : BaseEntity
    {
        [Column("T_ID")]
        public string t_id { get; set; }
        [Column("APP_NAME")]
        public string app_name { get; set; }
        [Column("CALL_INFO")]
        public string call_info { get; set; }
        [Column("API_STATE")]
        public int api_state { get; set; }
        [Column("MAIN_STATE")]
        public int main_state{get;set;}
        [Column("CREATE_TIME")]
        public DateTime create_time { get; set; }
        [Column("MODIFY_TIME")]
        public DateTime modify_time { get; set; }
        [Column("RETRY_COUNT")]
        public int retry_count { get; set; }
    }
}
