﻿using HuNanChangJie.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class ApiCallEntity
    {
        public string mainAPP { get; set; }
        public string callMethod { get; set; }
        public List<string> paras { get; set; }
        public List<Api> apis { get; set; }
    }

    public class Api 
    {
        public string appName { get; set; }
        public string url { get; set; }
        public Dictionary<string, string> headers { get; set; }
        public int runState { get; set; }
        public object paras { get; set; }
    }
}
