﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class YunExpressBillDetailsResponse
    {
        public string t { get; set; }
        public string msg { get; set; }
        public string code { get; set; }
        public bool? success { get; set; }
        public ResultBillDetails result { get; set; }
    }

    public class ResultBillDetails
    {
        public List<Receipt> receipt { get; set; }
        public List<ExpenditureRecords> expenditure_records { get; set; }
        public List<AdditionalSurcharge> additional_surcharge { get; set; }
        public List<CorrectRecord> correct_record { get; set; }
        public List<ReturnRecord> return_record { get; set; }
        public List<VatRecords> vat_records { get; set; }
        public List<TransferTracking> transfer_tracking { get; set; }
        public List<ReturnOrder> return_order { get; set; }
        public List<ClearanceRecord> clearance_record { get; set; }
        public List<TransferRecord> transfer_record { get; set; }
    }

    /// <summary>
    /// 充值记录
    /// </summary>
    public class Receipt
    {
        public string customer_code { get; set; }
        public string custmer_name { get; set; }
        public string receipt_number { get; set; }
        public decimal? amount { get; set; }
        public string currency { get; set; }
        public string exchange_rate { get; set; }
        public string receiver { get; set; }
        public string beneficial_account_number { get; set; }
        public string payer { get; set; }
        public string paied_account_number { get; set; }
        public string received_date { get; set; }
        public string remark { get; set; }
    }
    /// <summary>
    /// 末端支出记录
    /// </summary>
    public class ExpenditureRecords
    {
        public string waybill_number { get; set; }
        public string customer_order_number { get; set; }
        public string service_provider_number { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string clerk { get; set; }
        public string arrival_time { get; set; }
        public string partition { get; set; }
        public string destination_country { get; set; }
        public string receiver_name { get; set; }
        public string receiver_address { get; set; }
        public string whether_to_prepay { get; set; }
        public string sales_products { get; set; }
        public string receiving_outlets { get; set; }
        public string chargeable_weight { get; set; }
        public string volume_weight { get; set; }
        public string _long { get; set; }
        public string width { get; set; }
        public string high { get; set; }
        public string net_weight { get; set; }
        public decimal? declared_value { get; set; }
        public string reporting_currency { get; set; }
        public decimal? service_charge { get; set; }
        public decimal? registration_fee { get; set; }
        public decimal? processing_fee { get; set; }
        public decimal? fuel_surcharge { get; set; }
        public decimal? handling_fee { get; set; }
        public decimal? redispatch_fee { get; set; }
        public decimal? remote_surcharge { get; set; }
        public decimal? return_fee { get; set; }
        public decimal? special_products_fee { get; set; }
        public decimal? liquidated_damages { get; set; }
        public decimal? other_fee { get; set; }
        public decimal? total_fee { get; set; }
        public string remark { get; set; }
        public decimal? discount_fee { get; set; }
    }
    /// <summary>
    /// 加收支出记录
    /// </summary>
    public class AdditionalSurcharge
    {
        public string waybill_number { get; set; }
        public string customer_order_number { get; set; }
        public string service_provider_number { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string clerk { get; set; }
        public string arrival_time { get; set; }
        public string partition { get; set; }
        public string destination_country { get; set; }
        public string receiver_name { get; set; }
        public string receiver_address { get; set; }
        public string whether_to_prepay { get; set; }
        public string sales_products { get; set; }
        public string receiving_outlets { get; set; }
        public string chargeable_weight { get; set; }
        public string volume_weight { get; set; }
        public string _long { get; set; }
        public string width { get; set; }
        public string high { get; set; }
        public string net_weight { get; set; }
        public decimal? declared_value { get; set; }
        public string reporting_currency { get; set; }
        public decimal? service_charge { get; set; }
        public decimal? registration_fee { get; set; }
        public decimal? processing_fee { get; set; }
        public decimal? fuel_surcharge { get; set; }
        public decimal? handling_fee { get; set; }
        public decimal? redispatch_fee { get; set; }
        public decimal? remote_surcharge { get; set; }
        public decimal? return_fee { get; set; }
        public decimal? special_products_fee { get; set; }
        public decimal? liquidated_damages { get; set; }
        public decimal? other_fee { get; set; }
        public decimal? total_fee { get; set; }
        public string remark { get; set; }
        public decimal? discount_fee { get; set; }
    }

    /// <summary>
    /// 勘误支出记录
    /// </summary>
    public class CorrectRecord
    {
        public string waybill_number { get; set; }
        public string customer_order_number { get; set; }
        public string service_provider_number { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string clerk { get; set; }
        public string arrival_time { get; set; }
        public string partition { get; set; }
        public string destination_country { get; set; }
        public string receiver_name { get; set; }
        public string receiver_address { get; set; }
        public string whether_to_prepay { get; set; }
        public string sales_products { get; set; }
        public string receiving_outlets { get; set; }
        public string chargeable_weight { get; set; }
        public string volume_weight { get; set; }
        public string _long { get; set; }
        public string width { get; set; }
        public string high { get; set; }
        public string net_weight { get; set; }
        public decimal? declared_value { get; set; }
        public string reporting_currency { get; set; }
        public decimal? service_charge { get; set; }
        public decimal? registration_fee { get; set; }
        public decimal? processing_fee { get; set; }
        public decimal? fuel_surcharge { get; set; }
        public decimal? handling_fee { get; set; }
        public decimal? redispatch_fee { get; set; }
        public decimal? remote_surcharge { get; set; }
        public decimal? return_fee { get; set; }
        public decimal? special_products_fee { get; set; }
        public decimal? liquidated_damages { get; set; }
        public decimal? other_fee { get; set; }
        public decimal? total_fee { get; set; }
        public string remark { get; set; }
        public decimal? discount_fee { get; set; }
    }

    /// <summary>
    /// 退件支出记录
    /// </summary>
    public class ReturnRecord
    {
        public string waybill_number { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string clerk { get; set; }
        public string customer_order_number { get; set; }
        public string service_provider_number { get; set; }
        public string sales_products { get; set; }
        public string destination_country { get; set; }
        public string receiver_name { get; set; }
        public string receiver_address { get; set; }
        public string arrival_time { get; set; }
        public string receiving_outlets { get; set; }
        public decimal? chargeable_weight { get; set; }
        public decimal? volume_weight { get; set; }
        public string net_weight { get; set; }
        public string return_date { get; set; }
        public decimal? refund_amount { get; set; }
        public string currency { get; set; }
        public string reason_for_return { get; set; }
        public string return_type { get; set; }
    }
    /// <summary>
    /// VAT支出记录
    /// </summary>
    public class VatRecords
    {
        public string waybill_number { get; set; }
        public string customer_order_number { get; set; }
        public string service_provider_number { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string clerk { get; set; }
        public string arrival_time { get; set; }
        public string partition { get; set; }
        public string destination_country { get; set; }
        public string receiver_name { get; set; }
        public string receiver_address { get; set; }
        public string whether_to_prepay { get; set; }
        public string sales_products { get; set; }
        public string receiving_outlets { get; set; }
        public string chargeable_weight { get; set; }
        public string volume_weight { get; set; }
        public string net_weight { get; set; }
        public decimal? declared_value { get; set; }
        public string reporting_currency { get; set; }
        public decimal? duty_surcharge { get; set; }
        public decimal? deposit { get; set; }
        public string whether_to_prepay_vat { get; set; }
        public decimal? duty { get; set; }
        public decimal? vat { get; set; }
        public decimal? total_fee { get; set; }
        public string remark { get; set; }
    }
    /// <summary>
    /// 转运订单支出记录
    /// </summary>
    public class TransferTracking
    {
        public string transfer_tracking_no { get; set; }
        public string type { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string saler { get; set; }
        public string product_name { get; set; }
        public string order_creation_time { get; set; }
        public string weight { get; set; }
        public string unit_for_weight { get; set; }
        public string return_warehouse_country { get; set; }
        public string destination_country { get; set; }
        public string billing_weight { get; set; }
        public string billing_weight_unit { get; set; }
        public string post_code { get; set; }
        public string region { get; set; }
        public decimal? transport_fee { get; set; }
        public decimal? other_fees { get; set; }
        public decimal? total_fees { get; set; }
        public string remark { get; set; }
    }

    /// <summary>
    /// 退件订单支出记录
    /// </summary>
    public class ReturnOrder
    {
        public string return_order_no { get; set; }
        public string transfer_tracking_no { get; set; }
        public string pick_up_ref_no { get; set; }
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string saler { get; set; }
        public string product_name { get; set; }
        public string check_in_time { get; set; }
        public string parcel_weight { get; set; }
        public string _long { get; set; }
        public string width { get; set; }
        public string high { get; set; }
        public string shipping_method { get; set; }
        public string pieces { get; set; }
        public string stock_keeping_unit { get; set; }
        public string extended_storage_days { get; set; }
        public string departure_country { get; set; }
        public string return_warehouse_country { get; set; }
        public decimal? billing_weight { get; set; }
        public string billing_weight_unit { get; set; }
        public decimal? return_basic_service_fee { get; set; }
        public decimal? inventory_fee { get; set; }
        public decimal? disposal_fee { get; set; }
        public decimal? extended_storage_fee { get; set; }
        public decimal? door_to_door_charge { get; set; }
        public decimal? site_delivery_fee { get; set; }
        public decimal? parcel_locker_delivery_fee { get; set; }
        public decimal? photo_fee { get; set; }
        public decimal? quality_inspection_fee { get; set; }
        public decimal? fuel_surcharge { get; set; }
        public decimal? other_fees { get; set; }
        public decimal? total_fees { get; set; }
        public string remark { get; set; }
    }
    /// <summary>
    /// 清关支出记录
    /// </summary>
    public class ClearanceRecord
    {
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string batch_no { get; set; }
        public string mawb_no { get; set; }
        public string custom_clearance_time { get; set; }
        public string customs_clearance_on_weekends { get; set; }
        public string landing_port { get; set; }
        public string pieces { get; set; }
        public string actual_weight { get; set; }
        public string billing_weight { get; set; }
        public string the_quantity_of_low_value_goods { get; set; }
        public string total_value_for_low_value_goods { get; set; }
        public string the_quantity_of_medium_value_goods { get; set; }
        public string total_value_for_medium_value_goods { get; set; }
        public string the_quantity_of_high_value_goods { get; set; }
        public string total_value_for_high_value_goods { get; set; }
        public string hs_code { get; set; }
        public string product_name { get; set; }
        public decimal? clearance_fee { get; set; }
        public decimal? document_fees { get; set; }
        public decimal? transfer_fee { get; set; }
        public decimal? processing_fee { get; set; }
        public decimal? airport_charge { get; set; }
        public decimal? air_handling_fee { get; set; }
        public decimal? pick_up_charge { get; set; }
        public decimal? transit_charge { get; set; }
        public decimal? warehouse_handling_fee { get; set; }
        public decimal? special_fee_for_covid_19 { get; set; }
        public decimal? customs_clearance_fee { get; set; }
        public decimal? other_fee { get; set; }
        public decimal? total_fee { get; set; }
        public string currency { get; set; }
    }
    /// <summary>
    /// 中转支出记录
    /// </summary>
    public class TransferRecord
    {
        public string customer_code { get; set; }
        public string customer_name { get; set; }
        public string batch_no { get; set; }
        public string awb_number { get; set; }
        public string departure_time { get; set; }
        public string pieces { get; set; }
        public string landing_port { get; set; }
        public string destination_warehouse { get; set; }
        public string destination_warehouse_code { get; set; }
        public string destination_country { get; set; }
        public decimal? billing_weight { get; set; }
        public string reference_car_number { get; set; }
        public string reference_number { get; set; }
        public string product_name_for_transfer { get; set; }
        public string product_name_for_clearance { get; set; }
        public decimal? transfer_fee { get; set; }
        public decimal? other_fee { get; set; }
        public decimal? total_fee { get; set; }
        public string currency { get; set; }
    }
}
