﻿using HuNanChangJie.WebRequestService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class YunExpressOrderInfoResponse
    {
        public string t { get; set; }
        public string msg { get; set; }
        public string code { get; set; }
        public bool? success { get; set; }
        public ResultOrderInfo result { get; set; }
    }

    public class ResultOrderInfo
    {
        public string waybill_number { get; set; }
        public string customer_order_number { get; set; }
        public string product_code { get; set; }
        public string tracking_number { get; set; }
        public string platform_account_code { get; set; }
        public int? pieces { get; set; }
        public string weight_unit { get; set; }
        public string size_unit { get; set; }
        public string status { get; set; }
        public string sensitive_type { get; set; }
        public string source_code { get; set; }
        public decimal chargeWeight { get; set; }
    }
}
