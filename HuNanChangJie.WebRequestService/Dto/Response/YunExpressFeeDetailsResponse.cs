﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class YunExpressFeeDetailsResponse
    {
        public string t { get; set; }
        public string msg { get; set; }
        public string code { get; set; }
        public bool? success { get; set; }
        public Result result { get; set; }
    }

    public class Result
    {
        public string waybill_number { get; set; }
        public string customer_order_number { get; set; }
        public string tracking_number { get; set; }
        public string currency { get; set; }
        public string fee_name { get; set; }
        public string country_code { get; set; }
        public string prepay { get; set; }
        public string transaction_type { get; set; }
        public decimal? total_amount { get; set; }
        public string transaction_date { get; set; }
        public decimal? charge_weight { get; set; }
        public decimal? actual_weight { get; set; }
        public decimal? volume_weight { get; set; }
        public string charge_unit { get; set; }
        public string product_code { get; set; }
        public string product_name { get; set; }
        public decimal? handling_fee { get; set; }
        public List<FeeDetails> fee_details { get; set; }
    }

    public class FeeDetails
    {
        public string fee_name { get; set; }
        public decimal? amount { get; set; }
        public string currency { get; set; }
        public decimal? rate { get; set; }
        public string deduction_currency { get; set; }
        public decimal? deduction_amount { get; set; }
    }
}
