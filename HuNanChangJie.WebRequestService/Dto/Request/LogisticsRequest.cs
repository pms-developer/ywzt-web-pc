﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class LogisticsRequest
    {
        public string code { get; set; }
        public string name { get; set; }
        public int status { get; set; }
        public string appId { get; set; }
        public string appSecret { get; set; }
    }

}
