﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class WmsLogisticsRequest
    {
        public string ID { get; set; }
        /// <summary>
        /// 承运商编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 承运商名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal? Price { get; set; }
        /// <summary>
        /// 生效时间
        /// </summary>
        public DateTime? EffectiveTime { get; set; }
        /// <summary>
        /// 月结账号
        /// </summary>
        public string MonthlyAccount { get; set; }
        /// <summary>
        /// 接口账号
        /// </summary>
        public string InterfaceAccount { get; set; }
        /// <summary>
        /// 接口密钥
        /// </summary>
        public string InterfaceKey { get; set; }
        /// <summary>
        /// 接口服务地址
        /// </summary>
        public string InterfaceServiceAddress { get; set; }
        /// <summary>
        /// 接口用户信息
        /// </summary>
        public string InterfaceUserInfo { get; set; }
        /// <summary>
        /// EDI服务地址
        /// </summary>
        public string EDIServiceAddress { get; set; }
        /// <summary>
        /// 面单模板
        /// </summary>
        public string SheetTemplate { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool? Enabled { get; set; } 
    }
}
