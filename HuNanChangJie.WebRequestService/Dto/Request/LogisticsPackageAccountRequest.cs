﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class LogisticsPackageAccountRequest
    {
        public string billDate { get; set; }
        public string shipperId { get; set; }
        public string relationNo { get; set; }
        public decimal? totalFee { get; set; }
        public int? surchargeQty { get; set; }
        public decimal? surchargeSum { get; set; }
        public string surchargeFeeDetail { get; set; }
        public int? orderQty { get; set; }
        public List<LogisticsPackageAccountDetailRequest> dtls { get; set; }
    }

    public class LogisticsPackageAccountDetailRequest
    {
        public string relationNo { get; set; }
        public string orderType { get; set; }
        public string warehouseId { get; set; }
        public string carrier { get; set; }
        public string trackingNumber { get; set; }
        public string deliveryDate { get; set; }
        public decimal? subtotal { get; set; }
    }
}
