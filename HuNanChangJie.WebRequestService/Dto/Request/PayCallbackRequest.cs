﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class PayCallbackRequest
    {
        public string settlementNo { get; set; }
        public string remark { get; set; }
        public bool? success { get; set; }
    }
}
