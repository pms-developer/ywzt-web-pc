﻿using HuNanChangJie.WebRequestService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class DeliveryAccountRequest
    {
        public string billDate { get; set; }
        public string warehouseId { get; set; }
        public string shipperId { get; set; }
        public string relationNo { get; set; }
        public int? stockType { get; set; }
        public string orderType { get; set; }
        public decimal? totalFee { get; set; }
        public int? surchargeQty { get; set; }
        public decimal? surchargeSum { get; set; }
        public string surchargeFeeDetail { get; set; }
        public List<DeliveryAccountDetailRequest> dtls { get; set; }
    }

    public class DeliveryAccountDetailRequest
    {
        public string materialid { get; set; }
        public string materialName { get; set; }
        public string qualityState { get; set; }
        public string qty { get; set; }
        public string feeScale { get; set; }
        public decimal? subtotal { get; set; }
    }
}
