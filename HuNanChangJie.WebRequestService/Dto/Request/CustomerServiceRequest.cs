﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class CustomerServiceRequest
    {
        public string number { get; set; }
        public List<CustomerServiceDetailRequest> dtls { get; set; }
    }

    public class CustomerServiceDetailRequest
    {
        public string code { get; set; }
        public string serviceidId { get; set; } = "888888";
        public string serviceName { get; set; }
        public decimal? price { get; set; }
        public string remark { get; set; }
    }
}
