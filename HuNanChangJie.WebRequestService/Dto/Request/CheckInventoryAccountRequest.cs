﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class CheckInventoryAccountRequest
    {
        public string billDate { get; set; }
        public string warehouseId { get; set; }
        public string shipperId { get; set; }
        public string relationNo { get; set; }
        public decimal? totalFee { get; set; }
        public int? stockCount { get; set; }
        public int? surchargeQty { get; set; }
        public decimal? surchargeSum { get; set; }
        public string surchargeFeeDetail { get; set; }
        public List<CheckInventoryAccountDetailRequest> dtls { get; set; }
    }

    public class CheckInventoryAccountDetailRequest
    {
        public string sku { get; set; }
        public string materialName { get; set; }
        public int? qty { get; set; }
        public decimal? feeScale { get; set; }
        public decimal? subtotal { get; set; }
    }
}
