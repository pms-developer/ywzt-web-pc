﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class WarehouseRentAccountRequest
    {
        public string billDate { get; set; }
        public string warehouseId { get; set; }
        public string warehouseName { get; set; }
        public string shipperId { get; set; }
        public string shipperName { get; set; }
        public string relationNo { get; set; }
        public decimal? totalFee { get; set; }
        public int? surchargeQty { get; set; }
        public decimal? surchargeSum {  get; set; }
        public string surchargeFeeDetail { get; set; }
        public List<WarehouseRentAccountDetailRequest> dtls { get; set; }
    }

    public class WarehouseRentAccountDetailRequest
    { 
        public string batchNumber { get; set; }
        public string materialid { get; set; }
        public string materialName { get; set; }
        public string sku { get; set; }
        public string categoryName { get; set; }
        public string productType { get; set; }
        public string stockInDate { get; set; }
        public string stockOutDate { get; set; }
        public string volume { get; set; }
        public string weight { get; set; }
        public int? inStockDays { get; set; }
        public int? qty { get; set;}
        public string totalVolume { get; set; }
        public string feeScale { get; set; }
        public decimal? storageFee { get; set; }
    }
}
