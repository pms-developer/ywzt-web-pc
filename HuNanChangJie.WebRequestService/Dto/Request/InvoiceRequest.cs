﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class InvoiceRequest
    {
        public string settlementNo { get; set; }
        public string fileUrl { get; set; }
    }
}
