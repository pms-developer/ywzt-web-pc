﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public class ReceiptRequest
    {
        public ReceiptModelRequest model { get; set; }
        public List<ReceiptDetailRequest> detail { get; set; }
        public string WarehouseID { get; set; }
    }

    public class ReceiptModelRequest
    {
        public string Code { get; set; }
        public string ID { get; set; }
        public string ShipperID { get; set; }
        public string endState { get; set; }
        public string logisticsCode { get; set; }
        public string logisticsTitle { get; set; }
        public string startState { get; set; }
    }

    public class ReceiptDetailRequest
    {
        public string code { get; set; }
        public int? collectedNum { get; set; }
        public string name { get; set; }
        public int? purchaseNum { get; set; }
        public int? receivedNum { get; set;}
        public int? sendNum { get; set; }
        public string PurchaseCode { get; set; }
        public decimal? Lenght { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public decimal? Volume { get; set; }
        public decimal? Weight { get; set; }
        public decimal? PurchasePrice { get; set; }
    }
}
