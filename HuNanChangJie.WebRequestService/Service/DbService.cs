﻿using HuNanChangJie.Application.Base.SystemModule;
using HuNanChangJie.DataBase;
using HuNanChangJie.DataBase.Repository;
using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public  class DbService : RepositoryFactory
    {
        private  readonly string connectionString = ConfigurationManager.ConnectionStrings["apiSql"].ConnectionString;
        public void AddApiCall(GlobalTransactionEntity apiCall)
        {
            string sql = $@"INSERT INTO global_transaction(t_id,app_name,call_info,api_state,main_state,create_time,modify_time,retry_count)
                          VALUES('{apiCall.t_id}','{apiCall.app_name}','{apiCall.call_info}',{apiCall.api_state},{apiCall.main_state},'{apiCall.create_time}','{apiCall.modify_time}',{apiCall.retry_count})";

            this.BaseRepository(connectionString, DatabaseType.MySql).ExecuteBySql(sql);
        }

        public void UpdateApiCallRunState(string tid,int runState)
        {
            var entity = this.BaseRepository(connectionString, DatabaseType.MySql).FindList<GlobalTransactionEntity>($"select * from global_transaction where t_id='{tid}'").FirstOrDefault();//db.FindEntity<GlobalTransactionEntity>($"select * from global_transaction where t_id='{tid}'");
            if (entity != null)
            {
                var apiCallEntity=JsonConvert.DeserializeObject<ApiCallEntity>(entity.call_info);
                if (apiCallEntity.apis != null)
                {
                    if (apiCallEntity.apis.Count > 0)
                    {
                        foreach (var item in apiCallEntity.apis)
                        {
                            if (item.appName.IndexOf("中台") > -1)
                            {
                                item.runState = runState;
                                entity.call_info = JsonConvert.SerializeObject(apiCallEntity);
                                this.BaseRepository(connectionString, DatabaseType.MySql).ExecuteBySql($"update global_transaction set call_info='{entity.call_info}' where t_id='{tid}'");
                            }
                        }
                    }
                }
            }
        }
    }
}
