﻿using HuNanChangJie.Util;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace HuNanChangJie.WebRequestService
{
    public static class HolidayService
    {
        private readonly static string strUrl = "https://timor.tech/api/holiday/year/" + DateTime.Now.Year;

        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static List<HolidayResponse> GetHoliday()
        {
            List<HolidayResponse> list=new List<HolidayResponse>();
            try
            {
                string filePath = AppDomain.CurrentDomain.BaseDirectory + "/Content/json/"+DateTime.Now.Year+"holiday.json";
                string content = "";
                if (File.Exists(filePath))
                {
                    using (var fileStream = File.OpenRead(filePath))
                    {
                        // 创建一个字节数组来存储文件内容
                        byte[] bytes = new byte[fileStream.Length];

                        // 读取数据
                        int bytesRead = fileStream.Read(bytes, 0, (int)fileStream.Length);

                        // 将字节转换为字符串，假设文件内容是文本
                        content = System.Text.Encoding.UTF8.GetString(bytes, 0, bytesRead);


                    }
                }
                else
                {
                    using (var client = new RestClient(strUrl))
                    {
                        var request = new RestRequest(strUrl, Method.Get);
                        var response = client.Execute(request);
                        if (response.IsSuccessful)
                        {
                            log.Info($"\r\n{DateTime.Now}----response: {response.Content}");

                            content = response.Content;
                            using (var fileStream = File.Create(filePath))
                            {
                                byte[] info = new UTF8Encoding(true).GetBytes(content);
                                fileStream.Write(info, 0, info.Length);
                            }
                        }
                        else
                        {
                            log.Error($"\r\n{DateTime.Now}----response: {response.Content}");
                            return null;
                        }

                    }
                }

                dynamic holidayResponse = JsonConvert.DeserializeObject(content);
                var holidays = holidayResponse.holiday;
                foreach (var item in holidays)
                {
                    HolidayResponse holiday = new HolidayResponse
                    {
                        date = item.Value.date,
                        wage = item.Value.wage,
                    };
                    list.Add(holiday);
                }
                return list;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                //Console.WriteLine(ex.Message);
                throw ex;
            }

        }
    }
}
