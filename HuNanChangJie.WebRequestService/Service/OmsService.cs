﻿using HuNanChangJie.Util;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace HuNanChangJie.WebRequestService
{
    public static class OmsService
    {
        private readonly static string strUrl = Config.GetValue("OmsUrl");

        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static bool SendObj<T>(T m, string path, string tenant = "")
        {
            try
            {
                StackTrace stackTrace = new StackTrace(true);
                MethodBase methodBase = stackTrace.GetFrame(1).GetMethod();

                var tid = Guid.NewGuid().ToString();

                ApiCallEntity apiCall = new ApiCallEntity
                {
                    mainAPP="业务中台",
                    callMethod= methodBase.Name,
                    paras=new List<string>(),
                    apis=new List<Api>(),
                };

                Api api = new Api
                {
                    appName = "OMS",
                    url = path,
                    headers=new Dictionary<string, string> (),
                    runState=1,
                    paras=m
                };
                api.headers.Add("tid", tid);
                if (!string.IsNullOrEmpty(tenant))
                {
                    api.headers.Add("tenant", tenant);
                }
                apiCall.apis.Add(api);

                GlobalTransactionEntity globalTransactionEntity = new GlobalTransactionEntity
                {
                    t_id = tid,
                    app_name = "业务中台",
                    call_info=JsonConvert.SerializeObject(apiCall),
                    api_state=1,
                    main_state=2,
                    create_time=DateTime.Now,
                    modify_time=DateTime.Now,
                    retry_count=0,
                };
                DbService dbService = new DbService();
                dbService.AddApiCall(globalTransactionEntity);

                using (var client = new RestClient(path))
                {
                    var request = new RestRequest(path, Method.Post);
                    request.AddHeader("tid", tid);
                    request.AddHeader("Content-Type", "application/json");
                    if (!string.IsNullOrEmpty(tenant))
                    {
                        request.AddHeader("tenant", tenant);
                    }
                    var body = JsonConvert.SerializeObject(m);
                    log.Info($"\r\n{DateTime.Now}----request:{body}");
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    var response = client.Execute(request);
                    //Console.WriteLine(response.Content);
                    if (response.IsSuccessful)
                    {
                        log.Info($"\r\n{DateTime.Now}----response: {response.Content}");
                    }
                    else
                    {
                        log.Error($"\r\n{DateTime.Now}----response: {response.Content}");
                    }
                    return response.IsSuccessful;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                //Console.WriteLine(ex.Message);
                throw ex;
            }

        }
        /// <summary>
        /// 推送仓租账单至OMS
        /// </summary>
        /// <param name="request"></param>
        public static bool PushWarehouseRentAccount(WarehouseRentAccountRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/erp/api/v1/fin/warehouse-rent-bill/saveData",tenant);
        }

        /// <summary>
        /// 推送入库账单至OMS
        /// </summary>
        /// <param name="request"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public static bool PushInStorageAccount(InStorageAccountRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/erp/api/v1/fin/stock-in-out-bill/saveData", tenant);
        }
        /// <summary>
        /// 推送出库账单至OMS
        /// </summary>
        /// <param name="request"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public static bool PushDeliveryAccount(DeliveryAccountRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/erp/api/v1/fin/stock-in-out-bill/saveData", tenant);
        }
        /// <summary>
        /// 推送客户服务至OMS
        /// </summary>
        /// <param name="request"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public static bool PushCustomerService(CustomerServiceRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/erp/api/v1/assembly/middleServerCallbackAction", tenant);
        }
        /// <summary>
        /// 推送盘点账单至OMS
        /// </summary>
        /// <param name="request"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public static bool PushCheckInventoryAccount(CheckInventoryAccountRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/erp/api/v1/fin/stock-taking-bill/saveData", tenant);
        }
        /// <summary>
        /// 发票信息回传OMS
        /// </summary>
        /// <param name="request"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public static bool PushInvoice(InvoiceRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/erp/api/v1/fin/bill-invoice/bill_invoice_callback", tenant);
        }
        /// <summary>
        /// 推送承运商信息至WMS
        /// </summary>
        /// <param name="request"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public static bool PushLogistics(LogisticsRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/admin/api/logistics/saveAction", tenant);
        }
        /// <summary>
        /// 推送快递账单至OMS
        /// </summary>
        /// <param name="request"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public static bool PushLogisticsPackageAccount(LogisticsPackageAccountRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/erp/api/v1/fin/express-parcel-bill/saveData", tenant);
        }
        /// <summary>
        /// 余额扣款后回调
        /// </summary>
        /// <param name="request"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        public static bool PayCallback(PayCallbackRequest request, string tenant = "")
        {
            return SendObj(request, strUrl + "/erp/api/v1/fin/cloud-warehouse-bill/bill-pay-callback", tenant);
        }
    }
}
