﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using HuNanChangJie.Util;
using log4net;
using System.Reflection;
using RestSharp;

namespace HuNanChangJie.WebRequestService
{
    /// <summary>
    /// 云途物流接口服务
    /// </summary>
    public static class YunExpressService
    {
        public static string strUrl { get; set; }
        public static string appId { get; set; }
        public static string appSecret { get; set; }
        public static string sourceKey { get; set; }

        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        //获取签名
        public static string getSign(string secret, string data)
        {
            var keyBytes = Encoding.UTF8.GetBytes(secret);
            var messageBytes = Encoding.UTF8.GetBytes(data);
            var sign = "";
            using (var hmac = new HMACSHA256(keyBytes))
            {
                var hash = hmac.ComputeHash(messageBytes);
                sign = Convert.ToBase64String(hash);
            }
            Console.WriteLine("\r\n sign：" + sign);
            return sign;
        }

        //获取token
        public static string getToken()
        {

            // 设置请求的URL  
            var url = strUrl + "/openapi/oauth2/token";

            // 创建要发送的JSON数据对象  
            var data = new
            {
                grantType = "client_credentials",
                appId = appId,
                appSecret = appSecret,
                sourceKey = sourceKey
            };
            string jsonData = JsonConvert.SerializeObject(data);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";


            byte[] byteArray = Encoding.UTF8.GetBytes(jsonData);
            request.ContentLength = byteArray.Length;

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            var responseBody = "";
            // 获取响应  
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // 读取响应内容  
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            responseBody = reader.ReadToEnd();
                            Console.WriteLine("\r\n token：" + responseBody);
                        }
                    }
                }
                else
                {
                    log.Error("\r\nError: " + response.StatusCode);
                }
            }
            JObject jsonObject = JObject.Parse(responseBody);
            return jsonObject["accessToken"].ToString();
        }

        /// <summary>
        /// 获取运单扣费详情
        /// </summary>
        /// <param name="waybillNumber">运单号</param>
        /// <returns></returns>
        public static YunExpressFeeDetailsResponse getFeeDetails(string waybillNumber)
        {
            string url = strUrl + "/v1/order/fee-details/get?waybill_number=" + waybillNumber;
            string timestampMilliseconds = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
            var data = "date=" + timestampMilliseconds + "&method=GET&uri=/v1/order/fee-details/get?waybill_number=" + waybillNumber;
            Console.WriteLine("data：" + data);
            var secret = appSecret;

            #region 官网Demo
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            ////request.Method = "GET";
            ////request.ContentType = "application/json";
            //// 设置请求的header  
            //request.Headers.Add("token", getToken());
            //request.Headers.Add("sign", getSign(secret, data));
            //request.Headers.Set("date", timestampMilliseconds);


            //CallPrivateMethod(request, "SetSpecialHeaders", "date", timestampMilliseconds);

            //var responseBody = "";
            //// 获取响应  
            //using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            //{
            //    if (response.StatusCode == HttpStatusCode.OK)
            //    {
            //        // 读取响应内容  
            //        using (Stream responseStream = response.GetResponseStream())
            //        {
            //            using (StreamReader reader = new StreamReader(responseStream))
            //            {
            //                responseBody = reader.ReadToEnd();
            //            }
            //        }
            //    }
            //    else
            //    {
            //        log.Error("\r\nError: " + response.StatusCode);
            //    }
            //}

            //// 输出响应内容  
            //Console.WriteLine("\r\n response：" + responseBody);
            #endregion

            var responseBody = "";
            using (var client = new RestClient(url))
            {
                var request = new RestRequest(url, Method.Get);
                request.AddHeader("token", getToken());
                request.AddHeader("sign", getSign(secret, data));
                request.AddHeader("date", timestampMilliseconds);

                var response = client.Execute(request);
                //Console.WriteLine(response.Content);
                responseBody = response.Content;
                log.Info($"\r\n{DateTime.Now}  运单号：{waybillNumber}----response: {response.Content}");
            }

            return JsonConvert.DeserializeObject<YunExpressFeeDetailsResponse>(responseBody);
        }

        public static void CallPrivateMethod(object instance, string name, params object[] param)
        {
            BindingFlags flag = BindingFlags.Instance | BindingFlags.NonPublic;
            Type type = instance.GetType();
            MethodInfo method = type.GetMethod(name, flag);
            method.Invoke(instance, param);
        }

        /// <summary>
        /// 获取账单明细列表
        /// </summary>
        /// <param name="bill_code">账单编号</param>
        /// <param name="bill_type">账单类型，I-充值记录,Q-清关支出记录,T-中转支出记录,N-末端支出记录,K-勘误支出记录,C-加收支出记录,R-退件支出记录,V-增值支出记录,TJ-退件订单支出记录,TT-转运订单支出记录</param>
        /// <param name="page_no">查询页面的页码，默认值为 1，表示第一页【1,100000】</param>
        /// <param name="page_size">每页数据行数，默认值 10 数值类精确表示法[1,100]</param>
        /// <returns></returns>
        public static YunExpressBillDetailsResponse getBillDetails(string bill_code,string bill_type,int page_no,int page_size)
        {
            string url = strUrl + "/v1/bill/details/list?bill_code=" + bill_code+ "&bill_type="+bill_type+ "&page_no="+page_no+ "&page_size="+page_size;
            string timestampMilliseconds = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
            var data = "date=" + timestampMilliseconds + "&method=GET&uri=/v1/bill/details/list?bill_code=" + bill_code + "&bill_type=" + bill_type + "&page_no=" + page_no + "&page_size=" + page_size;
            Console.WriteLine("data：" + data);
            var secret = appSecret;

            var responseBody = "";
            using (var client = new RestClient(url))
            {
                var request = new RestRequest(url, Method.Get);
                request.AddHeader("token", getToken());
                request.AddHeader("sign", getSign(secret, data));
                request.AddHeader("date", timestampMilliseconds);

                var response = client.Execute(request);
                responseBody = response.Content;
                log.Info($"\r\n{DateTime.Now}  账单编号：{bill_code}----response: {response.Content}");
            }

            return JsonConvert.DeserializeObject<YunExpressBillDetailsResponse>(responseBody);
        }

        /// <summary>
        /// 查询运单详情
        /// </summary>
        /// <param name="order_number">单号（支持运单号、客户单号、跟踪号）</param>
        /// <returns></returns>
        public static YunExpressOrderInfoResponse getOrderInfo(string order_number)
        {
            string url = strUrl + "/v1/order/info/get?order_number=" + order_number;
            string timestampMilliseconds = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
            var data = "date=" + timestampMilliseconds + "&method=GET&uri=/v1/order/info/get?order_number=" + order_number;
            Console.WriteLine("data：" + data);
            var secret = appSecret;

            var responseBody = "";
            using (var client = new RestClient(url))
            {
                var request = new RestRequest(url, Method.Get);
                request.AddHeader("token", getToken());
                request.AddHeader("sign", getSign(secret, data));
                request.AddHeader("date", timestampMilliseconds);

                var response = client.Execute(request);
                responseBody = response.Content;
                log.Info($"\r\n{DateTime.Now}  查询运单详情-单号：{order_number}----response: {response.Content}");
            }

            return JsonConvert.DeserializeObject<YunExpressOrderInfoResponse>(responseBody);
        }
    }
}
