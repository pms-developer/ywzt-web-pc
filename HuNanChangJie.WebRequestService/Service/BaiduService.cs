﻿using HuNanChangJie.Util;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace HuNanChangJie.WebRequestService
{
    public static class BaiduService
    {
        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string apiKey=Config.GetValue("BaiduAIApiKey");
        private static readonly string secretKey = Config.GetValue("BaiduAISecretKey");
        private readonly static string strUrl = $"https://aip.baidubce.com/oauth/2.0/token?client_id={apiKey}&client_secret={secretKey}&grant_type=client_credentials";

        /// <summary>
        /// 获取百度AI  Token
        /// </summary>
        /// <returns></returns>
        public static string GetToken()
        {
            using (var client = new RestClient(strUrl))
            {
                var request = new RestRequest(strUrl,Method.Post);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                var body = @"";
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = client.Execute(request);
                Console.WriteLine(response.Content);
                log.Info(response.Content);
                dynamic tokenResponse = JsonConvert.DeserializeObject(response.Content);
                if (tokenResponse.error == null)
                    return tokenResponse.access_token;
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// 增值税发票识别
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <returns></returns>
        public static string vatInvoice(string imageUrl)
        {
            //string token = "[调用鉴权接口获取的token]";
            string token = GetToken();
            string host = "https://aip.baidubce.com/rest/2.0/ocr/v1/vat_invoice?access_token=" + token;
            Encoding encoding = Encoding.Default;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(host);
            request.Method = "post";
            request.KeepAlive = true;
            // 图片的base64编码
            //string base64 = getFileBase64("[本地图片文件]");
            string base64 = getFileBase64(imageUrl);
            String str = "image=" + HttpUtility.UrlEncode(base64);
            byte[] buffer = encoding.GetBytes(str);
            request.ContentLength = buffer.Length;
            request.GetRequestStream().Write(buffer, 0, buffer.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.Default);
            string result = reader.ReadToEnd();
            Console.WriteLine("增值税发票识别:");
            Console.WriteLine(result);
            log.Info($"\r\n{DateTime.Now}增值税发票识别----response: {result}");
            return result;
        }

        public static String getFileBase64(String fileName)
        {
            FileStream filestream = new FileStream(fileName, FileMode.Open);
            byte[] arr = new byte[filestream.Length];
            filestream.Read(arr, 0, (int)filestream.Length);
            string baser64 = Convert.ToBase64String(arr);
            filestream.Close();
            return baser64;
        }
    }
}
