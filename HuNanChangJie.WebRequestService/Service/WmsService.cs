﻿using HuNanChangJie.SystemCommon;
using HuNanChangJie.Util;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.WebRequestService
{
    public static class WmsService
    {
        private readonly static string strUrl = Config.GetValue("WmsUrl");

        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static async void SendObj<T>(T m, string path)
        {
            try
            {
                StackTrace stackTrace = new StackTrace(true);
                MethodBase methodBase = stackTrace.GetFrame(1).GetMethod();

                var tid = Guid.NewGuid().ToString();

                ApiCallEntity apiCall = new ApiCallEntity
                {
                    mainAPP = "业务中台",
                    callMethod = methodBase.Name,
                    paras = new List<string>(),
                    apis = new List<Api>(),
                };

                Api api = new Api
                {
                    appName = "WMS",
                    url = path,
                    headers = new Dictionary<string, string>(),
                    runState = 1,
                    paras = m
                };
                api.headers.Add("tid", tid);
                apiCall.apis.Add(api);

                GlobalTransactionEntity globalTransactionEntity = new GlobalTransactionEntity
                {
                    t_id = tid,
                    app_name = "业务中台",
                    call_info = JsonConvert.SerializeObject(apiCall),
                    api_state = 1,
                    main_state = 2,
                    create_time = DateTime.Now,
                    modify_time = DateTime.Now,
                    retry_count = 0,
                };

                DbService dbService = new DbService();
                dbService.AddApiCall(globalTransactionEntity);

                using (var client = new RestClient(path))
                {
                    var request = new RestRequest(path, Method.Post);
                    request.AddHeader("tid", tid);
                    request.AddHeader("Content-Type", "application/json");
                    var body = JsonConvert.SerializeObject(m);
                    log.Info($"\r\n{DateTime.Now}----request:{body}");
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                    var response = await client.ExecuteAsync(request);
                    //Console.WriteLine(response.Content);
                    if (response.IsSuccessful)
                    {
                        log.Info($"\r\n{DateTime.Now}----response: {response.Content}");
                    }
                    else
                    {
                        log.Error($"\r\n{DateTime.Now}----response: {response.ErrorMessage}");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                //Console.WriteLine(ex.Message);
                throw ex;
            }

        }
        /// <summary>
        /// 推送发货单至WMS
        /// </summary>
        /// <param name="request"></param>
        public static void PushReceipt(ReceiptRequest request)
        {
            SendObj(request, strUrl + "/meio/OrderEntry/PushReceipt");
        }
        /// <summary>
        /// 推送承运商至WMS
        /// </summary>
        /// <param name="request"></param>
        public static void PushLogistics(WmsLogisticsRequest request)
        {
            SendObj(request, strUrl + "/meio/BaseData/PushCarrier");
        }
            
    }
}
