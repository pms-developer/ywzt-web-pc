﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Domain.Unit
{
    public static class SqlHelper
    {
        public static StringBuilder GenerateWdtOrderSql(this Object obj, string tableName,int batchNo)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"INSERT INTO {tableName}");
            var propertyinfos = obj.GetType().GetProperties().Where(x => Nullable.GetUnderlyingType(x.PropertyType) != null || (!x.PropertyType.IsGenericType && !typeof(IList<>).IsAssignableFrom(x.PropertyType))).ToList();
            System.Text.StringBuilder columnNames = new StringBuilder();
            System.Text.StringBuilder columnValues = new StringBuilder();
            columnNames.Append("(");
            columnValues.Append("(");

            foreach (var item in propertyinfos)
            {
                columnNames.Append(item.Name);
                columnNames.Append(",");
                columnValues.Append("'");
                columnValues.Append(item.GetValue(obj, null)?.ToString().Trim().Replace("'", ""));
                columnValues.Append("',");
            }

            if(tableName.Contains("details"))
            {
                columnNames.Remove(columnNames.Length - 1, 1);
                columnNames.Append(")");
                columnValues.Remove(columnValues.Length - 1, 1);
                columnValues.Append(")");
            }
            else
            {
                columnNames.Append("BatchNo");
                columnNames.Append(")");
                columnValues.Append(batchNo);
                columnValues.Append(")");
            }
           

            sb.Append(columnNames);
            sb.Append("values");
            sb.Append(columnValues.ToString());
            sb.Append(";");
            return sb;
        }


        public static string GenerateInsertSql<T>(List<T> dataList, string tableName)
        {
            StringBuilder orderSql = new StringBuilder();
            foreach (var data in dataList)
            {
                orderSql.Append(data.GenerateSql(tableName));
            }
            string orderCmd = orderSql.ToString();

            return orderCmd;
        }


        public static StringBuilder GenerateSql(this Object obj, string tableName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"INSERT INTO {tableName}");
            var propertyinfos = obj.GetType().GetProperties().Where(x => Nullable.GetUnderlyingType(x.PropertyType) != null || (!x.PropertyType.IsGenericType && !typeof(IList<>).IsAssignableFrom(x.PropertyType))).ToList();
            System.Text.StringBuilder columnNames = new StringBuilder();
            System.Text.StringBuilder columnValues = new StringBuilder();
            columnNames.Append("(");
            columnValues.Append("(");

            foreach (var item in propertyinfos)
            {
                columnNames.Append(item.Name);
                columnNames.Append(",");
                columnValues.Append("'");
                columnValues.Append(item.GetValue(obj, null)?.ToString().Trim().Replace("'", ""));
                columnValues.Append("',");
            }           

            columnNames.Remove(columnNames.Length - 1, 1);
            columnNames.Append(")");
            columnValues.Remove(columnValues.Length - 1, 1);
            columnValues.Append(")");

            sb.Append(columnNames);
            sb.Append("values");
            sb.Append(columnValues.ToString());
            sb.Append(";");
            return sb;
        }

        public static StringBuilder GenerateExcelOrder(this DataTable dataTable)
        {
            StringBuilder sb = new StringBuilder();

            foreach (DataRow row in dataTable.Rows)
            {
                sb.Append($"INSERT INTO xizi_excel_order(OrderId,ReceiverName,ReceiverAddress,ReceiverPhone)values(");
                foreach (var item in row.ItemArray)
                {
                    sb.Append("'");
                    sb.Append(item.ToString().Trim());
                    sb.Append("',");
                }

                sb.Remove(sb.Length - 1, 1);
                sb.Append(");");
            }

            return sb;

        }

        public static StringBuilder GenerateExcelQnjs(this DataTable dataTable)
        {
            StringBuilder sb = new StringBuilder();

            foreach (DataRow row in dataTable.Rows)
            {
                sb.Append($"INSERT INTO qnjs_plan(answers,week,name,actionId)values(");
                foreach (var item in row.ItemArray)
                {
                    sb.Append("'");
                    sb.Append(item.ToString().Trim());
                    sb.Append("',");
                }

                sb.Remove(sb.Length - 1, 1);
                sb.Append(");");
            }

            return sb;

        }
       
    }
}
