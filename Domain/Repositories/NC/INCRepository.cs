﻿
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public interface INCRepository
    {
        Task ExecuteSql(string sql);
 
        Task<IEnumerable<NcMaterial>> GetMaterialList();    

        Task<IEnumerable<NcSpplier>> GetSpplierlList();
        Task<IEnumerable<NcWarehouse>> GetWarehouselList();
        Task<IEnumerable<NcShop>> GetShopList();
    }
}
