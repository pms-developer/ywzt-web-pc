﻿using DbContext;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public class NCRepository : INCRepository
    {
        private readonly IOralceDbCollection _oraclecCtx;
        private readonly IDefaultDbCollection _mysqlctx;
        public NCRepository(IOralceDbCollection oraclecCtx, IDefaultDbCollection mysqlctx)
        {
            _oraclecCtx = oraclecCtx;
            _mysqlctx = mysqlctx;
        }
        public async Task ExecuteSql(string sql)
        {
            await _mysqlctx.ExecuteSql(sql);
        }

        public async Task<IEnumerable<NcMaterial>> GetMaterialList()
        {        
            return await _oraclecCtx.QueryAsync<NcMaterial>("NC.GetMaterialList");
        }

        public async Task<IEnumerable<NcSpplier>> GetSpplierlList()
        {
            return await _oraclecCtx.QueryAsync<NcSpplier>("NC.GetSpplierlList");
        }

        public async Task<IEnumerable<NcWarehouse>> GetWarehouselList()
        {
            return await _oraclecCtx.QueryAsync<NcWarehouse>("NC.GetWarehouselList");
        }

        public async Task<IEnumerable<NcShop>> GetShopList()
        {
            return await _oraclecCtx.QueryAsync<NcShop>("NC.GetShopList");
        }
    }
}
