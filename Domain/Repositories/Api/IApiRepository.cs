﻿
using Domain.Models;
using Domain.Models.DTO;
using System;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public interface IApiRepository
    {
        Task  UpdateApi(ApiDto api);
        Task<int> CreateApiLog(ApiLogDto apiLog);
        Task<DateTime> GetApiLastStartTime(int apiId);
        Task UpdateApiLog(ApiLogDto apiLog);
        Task<WdtClientConfig> GetApiConfig(int apiId);
        
    }
}
