﻿
using Domain.Models;
using Domain.Models.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public interface IShopInfoRepository
    {
        Task<IEnumerable<BaseShopInfoDTO>> GetWdtShopCode(string[] shopIds);
    }
}
