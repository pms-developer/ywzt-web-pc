﻿using DbContext;
using Domain.Models;
using Domain.Models.DTO;
using System;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public class ApiRepository : IApiRepository
    {
        private readonly IDefaultDbCollection _ctx;
        public ApiRepository(IDefaultDbCollection ctx)
        {
            _ctx = ctx;
        }
        public async Task UpdateApi(ApiDto api)
        {
            await _ctx.ExecuteScalarAsync<long>("Api.UpdateApi", api);
        }
        public async Task<int> CreateApiLog(ApiLogDto apiLog)
        {
           return await _ctx.ExecuteScalarAsync<int>("Api.CreateApiLog", apiLog);
        }
        public async Task UpdateApiLog(ApiLogDto apiLog)
        {
            await _ctx.ExecuteScalarAsync<int>("Api.UpdateApiLog", apiLog);
        }

        public async Task<DateTime> GetApiLastStartTime(int apiId)
        {
           return await _ctx.QueryFirstOrDefaultAsync<DateTime>("Api.GetApiLastStartTime", new { ApiId = apiId });
        }

        public async Task<WdtClientConfig> GetApiConfig(int apiId)
        {
            return await _ctx.QueryFirstOrDefaultAsync<WdtClientConfig>("Api.GetApiConfig", new { ApiId = apiId });
        }  
    }
}
