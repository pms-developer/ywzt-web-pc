﻿using DbContext;
using Domain.Models;
using Domain.Models.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public class ShopInfoRepository : IShopInfoRepository
    {
        private readonly IDefaultDbCollection _ctx;
        public ShopInfoRepository(IDefaultDbCollection ctx)
        {
            _ctx = ctx;
        }        

        public async Task<IEnumerable<BaseShopInfoDTO>> GetWdtShopCode(string[] shopIds)
        {
            var result= await _ctx.QueryAsync<BaseShopInfoDTO>("ShopInfo.GetWdtShopCode", new { ShopIds = shopIds });
            return result;
        }  
    }
}
