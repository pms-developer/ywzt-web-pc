﻿
using Domain.Models;
using Domain.Models.DTO;
using System;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public interface IBaseRepository
    {
        Task ExecuteSql(string sql);
    }
}
