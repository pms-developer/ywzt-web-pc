﻿using DbContext;
using Domain.Models;
using Domain.Models.DTO;
using System;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public class BaseRepository : IBaseRepository
    {
        private readonly IDefaultDbCollection _ctx;
        public BaseRepository(IDefaultDbCollection ctx)
        {
            _ctx = ctx;
        }
        public async Task ExecuteSql(string sql)
        {
            await _ctx.ExecuteSql(sql);
        }



    }
}
