﻿
using Domain.Models.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public interface IOrderRepository
    {
        Task<WdtOrder> GetOrderById(string orderId);
        Task SaveOrder(string sql);
        Task DeleteExistOrder(int[] tradeIds);
        Task SyncOrder();
        Task SyncOrderDetails();
        Task<IEnumerable<int>> GetAllOrderIds();

        Task<List<QnjsPlan>> GetAllPlan();
    }
}
