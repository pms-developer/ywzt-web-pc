﻿using DbContext;
using Domain.Models.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doamin.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IDefaultDbCollection _ctx;
        public OrderRepository(IDefaultDbCollection ctx)
        {
            _ctx = ctx;
        }
        public async Task<WdtOrder> GetOrderById(string orderId)
        {
            return await _ctx.QueryFirstOrDefaultAsync<WdtOrder>("Order.GetOrderById", new { Id = orderId });            
        }

        public async Task SaveOrder(string sql)
        {
             await _ctx.ExecuteSql(sql);
        }



        public async Task DeleteExistOrder(int[] tradeIds)
        {
            await _ctx.ExecuteScalarAsync<long>("Order.DeleteExistOrder", new { TradeIds = tradeIds });
        }

        public async Task SyncOrder()
        {
            await _ctx.ExecuteScalarAsync<long>("Order.SyncOrder");
        }
        public async Task SyncOrderDetails()
        {
            await _ctx.ExecuteScalarAsync<long>("Order.SyncOrderDetails");
        } 

        public async Task<IEnumerable<int>> GetAllOrderIds()
        {
           return await _ctx.QueryAsync<int>("Order.SyncOrderDetails");
        }

        public async Task<List<QnjsPlan>> GetAllPlan()
        {
            return (await _ctx.QueryAsync<QnjsPlan>("Order.GetAllPlan")).ToList();
        }
     
    }
}
