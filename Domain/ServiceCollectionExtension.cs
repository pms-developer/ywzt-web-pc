﻿using Autofac;
using Doamin.Repositories;
using sdmap.ext;
using System.Linq;
using System.Reflection;

namespace Domain
{
  
    public static partial class ServiceCollectionExtension
    {
        public static void ConfigureDomain(this ContainerBuilder builder)
        {
            var assembly = Assembly.Load("Domain");

            builder.RegisterInstance(new SdmapContext(EmbeddedResourceSqlEmiter.CreateFrom(assembly)));
            
            builder.RegisterTypes(assembly.GetTypes().Where(type=>type.Name.EndsWith("Service") || type.Name.EndsWith("Repository")).ToArray()).AsImplementedInterfaces().OwnedByLifetimeScope();

        }
    }
}
