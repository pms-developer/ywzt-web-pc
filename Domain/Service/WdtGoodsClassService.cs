﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Service
{
    public partial class WdtService : IWdtService
    {

        public async Task<List<WdtGoodsClass>> GetGoodsClass(WdtClientConfig config)
        {
            WdtClient client = new WdtClient()
            {
                sid = config.Sid,
                appkey = config.AppKey,
                appsecret = config.Appsecret,
                gatewayUrl = config.GatewayUrl ,
                page_no=config.Page_no,
                page_size=config.Page_size
            };

            var wdtGoodsClass = new List<WdtGoodsClass>();

            int totalcount = 0;

            GetWdtPagedGoodsClass(client, wdtGoodsClass, ref totalcount);


            return wdtGoodsClass;
        }

        private void GetWdtPagedGoodsClass(WdtClient client, List<WdtGoodsClass> result, ref int totalCount)
        {
            var warehouses = client.wdtOpenapi();
            var wdtGoodsClass = JsonConvert.DeserializeObject<WDTGoodsClassDto>(warehouses);
            if (client.page_no == 0)
            {
                totalCount = int.Parse(wdtGoodsClass.Total_Count);
            }

            result.AddRange(wdtGoodsClass.Goods_Class);

            if (client.page_no < totalCount / client.page_size)
            {
                client.page_no++;
                GetWdtPagedGoodsClass(client, result, ref totalCount);
            }
        }



        public async Task SaveGoodsClass(List<WdtGoodsClass> WdtGoodsClass, string tableName)
        {
            var sql = GenerateGoodsClassSql(WdtGoodsClass,tableName);
            await _baseRepository.ExecuteSql(sql); 
        }

        private string GenerateGoodsClassSql(List<WdtGoodsClass> WdtGoodsClass, string tableName)
        {
            StringBuilder orderSql = new StringBuilder();
            foreach (var goodClass in WdtGoodsClass)
            {
                orderSql.Append(goodClass.GenerateSql(tableName));
            }
            string orderCmd = orderSql.ToString();

            return orderCmd;
        }

    }
}
