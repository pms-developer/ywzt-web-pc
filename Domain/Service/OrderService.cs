﻿using Aspose.Cells;
using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using Polly;
using QimenCloud.Api;
using QimenCloud.Api.scenehu3cgwt0tc.Request;
using QimenCloud.Api.scenehu3cgwt0tc.Response;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Top.Api;

namespace Domain.Service
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IApiRepository _apiRepository;
        private readonly IDYService _dYService;


        public OrderService(IOrderRepository orderRepository, IApiRepository apiRepository, IDYService dYService)
        {
            _orderRepository = orderRepository;
            _apiRepository = apiRepository;
            _dYService = dYService;
        }

        public async Task<WdtOrder> GetOrderById(string orderId)
        {
            return await _orderRepository.GetOrderById(orderId);
        }

        public async Task SaveWdtOdrer(WdtClientConfig config)
        {

            string wdtOrderCmd = GetInsertSqlForWdtOrder(config, 0);

            if (!string.IsNullOrEmpty(wdtOrderCmd))
            {
                await _orderRepository.SaveOrder(wdtOrderCmd);
                //await _apiRepository.UpdateApi(new ApiDto { Id = 1, LastEndTime = config.End_time, CreateTime = DateTime.Now });
                //await _apiRepository.CreateApiLog(new ApiLogDto { ApiName = "wdtOrder", ApiId = 1, Create_Time = DateTime.Now, QueryParam = config.ToJsonString() });
            }
        }

        public async Task SyncOrder()
        {
            await _orderRepository.SyncOrder();
            await _orderRepository.SyncOrderDetails();
        }

        public void SyncDYOrder()
        {
            _dYService.SyncDYGetOrder(); 
        }

        public async Task SaveExcelOrder(string foldPath)
        {
            string[] files = Directory.GetFiles(foldPath);
            foreach (string file in files.Where(x => x.EndsWith("xlsx")))
            {
                Workbook workbook = new Workbook(file);
                Cells cells = workbook.Worksheets[0].Cells;

                System.Data.DataTable dataTable = cells.ExportDataTable(1, 0, cells.MaxDataRow, cells.MaxColumn + 1);//没有标题
                string insertSql = dataTable.GenerateExcelOrder().ToString();

                await _orderRepository.SaveOrder(insertSql);
            }
        }

        public async Task SaveQimenOdrer(WdtQimenConfig config)
        {
            int batchNo = await _apiRepository.CreateApiLog(new ApiLogDto { ApiName = "wdtQimenOrder", ApiId = 2, Create_Time = DateTime.Now, QueryParam = config.ToJsonString(), Status = 0, Start_Time = config.Start_time, End_time = config.End_time });
            int totalNum = 0;
            Policy.Handle<Exception>()
                   .WaitAndRetry(new[]
                              {
                                TimeSpan.FromSeconds(61),
                                TimeSpan.FromSeconds(90),
                                TimeSpan.FromSeconds(150)
                              })
                  .Execute(() => { totalNum = SaveOrder(config, batchNo).ConfigureAwait(false).GetAwaiter().GetResult(); });

            await _apiRepository.UpdateApiLog(new ApiLogDto { Id = batchNo, TotalCount = totalNum });

            await _apiRepository.UpdateApi(new ApiDto { Id = 2, LastEndTime = config.End_time, CreateTime = DateTime.Now });

        }

        public async Task SaveQimenHistoryOdrer(WdtQimenConfig config)
        {

            Policy.Handle<ArgumentNullException>()
                .Or<Exception>()
                   .WaitAndRetry(new[]
                              {
                                TimeSpan.FromSeconds(61),
                                TimeSpan.FromSeconds(90),
                                TimeSpan.FromSeconds(120)
                              })
                  .Execute(() => SaveQimenHistoryOrder(config).ConfigureAwait(false).GetAwaiter());


            //await SaveQimenHistoryOrder(config);

            await _apiRepository.UpdateApi(new ApiDto { Id = 3, LastEndTime = config.End_time, CreateTime = DateTime.Now });
            await _apiRepository.CreateApiLog(new ApiLogDto { ApiName = "wdtQimenHistoryOrder", ApiId = 3, Create_Time = DateTime.Now, QueryParam = config.ToJsonString() });

        }

        public async Task<int> SaveOrder(WdtQimenConfig config, int batchNo)
        {
            var wdtQimenOrders = GetQimenAllPagedOrder(config);

            if (wdtQimenOrders != null && wdtQimenOrders.Count > 0)
            {
                var tradeIds = wdtQimenOrders.Select(x => x.trade_id).ToArray();

                await _orderRepository.DeleteExistOrder(tradeIds);

                string orderSql = GenerateOrderSql(wdtQimenOrders, batchNo);

                if (!string.IsNullOrEmpty(orderSql))
                {
                    await _orderRepository.SaveOrder(orderSql);
                }

                string orderDetailSql = GenerateOrderDetailsSql(wdtQimenOrders, batchNo);

                if (!string.IsNullOrEmpty(orderDetailSql))
                {
                    await _orderRepository.SaveOrder(orderDetailSql);
                }

                return wdtQimenOrders.Count();
            }
            return 0;
        }

        public async Task SaveQimenHistoryOrder(WdtQimenConfig config)
        {
            var wdtQimenOrders = GetQimenAllPagedHistoryOrder(config);


            if (wdtQimenOrders != null)
            {
                string orderSql = GenerateOrderSql(wdtQimenOrders, wdtQimenOrders.Count());

                if (!string.IsNullOrEmpty(orderSql))
                {
                    await _orderRepository.SaveOrder(orderSql);
                }

                string orderDetailSql = GenerateOrderDetailsSql(wdtQimenOrders, 0);

                if (!string.IsNullOrEmpty(orderDetailSql))
                {
                    await _orderRepository.SaveOrder(orderDetailSql);
                }
            }
        }

        private string GetInsertSqlForWdtOrder(WdtClientConfig config, int batchno)
        {
            WdtClient client = new WdtClient()
            {
                sid = config.Sid,
                appkey = config.AppKey,
                appsecret = config.Appsecret,
                gatewayUrl = config.GatewayUrl,
                start_time = config.Start_time,
                end_time = config.LastEndTime,
                page_size = config.Page_size,
                page_no = config.Page_no
            };

            var wdtOrders = new List<WdtOrder>();

            int totalcount = 0;

            GetWdtPagedOrder(client, wdtOrders, ref totalcount);

            return GenerateSql(wdtOrders, 0);
        }

        //private string GetInsertSqlForQimenOrder(WdtQimenConfig config)
        //{ 
        //    var wdtQimenOrders = GetQimenAllPagedOrder(config);

        //    if(wdtQimenOrders==null)
        //    {
        //        return string.Empty;
        //    }

        //    return GenerateSql(wdtQimenOrders);
        //}

        private string GenerateSql(List<WdtOrder> wdtOrders, int batchno)
        {
            StringBuilder orderSql = new StringBuilder();
            StringBuilder orderDetailSql = new StringBuilder();

            foreach (var order in wdtOrders)
            {
                orderSql.Append(order.GenerateWdtOrderSql("wdt_order", batchno));
            }

            var orderDetails = wdtOrders.SelectMany(x => x.goods_list);

            foreach (var orderDetail in orderDetails)
            {
                orderDetailSql.Append(orderDetail.GenerateWdtOrderSql("wdt_orderdetails", batchno));
            }

            string orderCmd = orderSql.ToString();

            string orderDetailCmd = orderDetailSql.ToString();

            return orderCmd + orderDetailCmd;
        }

        private string GenerateOrderSql(List<WdtOrder> wdtOrders, int batchno)
        {
            StringBuilder orderSql = new StringBuilder();
            foreach (var order in wdtOrders)
            {
                orderSql.Append(order.GenerateWdtOrderSql("wdt_order", batchno));
            }
            string orderCmd = orderSql.ToString();

            return orderCmd;
        }

        private string GenerateOrderDetailsSql(List<WdtOrder> wdtOrders, int batchno)
        {
            StringBuilder orderDetailSql = new StringBuilder();
            var orderDetails = wdtOrders.SelectMany(x => x.goods_list);
            foreach (var orderDetail in orderDetails)
            {
                orderDetailSql.Append(orderDetail.GenerateWdtOrderSql("wdt_orderdetails", batchno));
            }

            string orderDetailCmd = orderDetailSql.ToString();
            return orderDetailCmd;
        }

        private void GetWdtPagedOrder(WdtClient client, List<WdtOrder> result, ref int totalCount)
        {

            var wdtOrders = JsonConvert.DeserializeObject<WDTOrderDto>(client.wdtOpenapi());
            if (client.page_no == 0)
            {
                totalCount = int.Parse(wdtOrders.Total_Count);
            }

            result.AddRange(wdtOrders.trades);

            if (client.page_no < totalCount / client.page_size)
            {
                client.page_no++;
                GetWdtPagedOrder(client, result, ref totalCount);
            }
        }

        public List<WdtOrder> GetQimenAllPagedOrder(WdtQimenConfig config)
        {

            DefaultQimenCloudClient qmclient = new DefaultQimenCloudClient(config.ServerUrl, config.AppKey, config.Appsecret, "json");
            WdtTradeQueryRequest req = new WdtTradeQueryRequest();
            req.SetTargetAppKey(config.TargetAppKey);
            req.Sid = config.Sid;
            req.StartTime = config.Start_time;
            req.EndTime = config.End_time;
            req.PageSize = config.Page_size;
            req.PageNo = config.Page_no;
            if (!string.IsNullOrEmpty(config.ShopNo))
            {
                req.ShopNo = config.ShopNo;
            }

            if (!string.IsNullOrEmpty(config.TradeNo))
            {
                req.TradeNo = config.TradeNo;
            }
            if (!string.IsNullOrEmpty(config.SrcTid))
            {
               req.SrcTid = config.SrcTid;
            }


            WdtTradeQueryResponse res = qmclient.Execute(req);
            if (res.Errorcode != 0)
            {
                throw new Exception();
            }
            var orders = JsonConvert.DeserializeObject<QimenOrderDto>(res.Body);

            var result = orders.Response.Trades;

            while (req.PageNo < res.TotalCount / req.PageSize)
            {
                req.PageNo++;
                var pageRespnse = qmclient.Execute(req);
                result.AddRange(JsonConvert.DeserializeObject<QimenOrderDto>(pageRespnse.Body).Response.Trades);
            }

            return result;
        }

        public List<WdtOrder> GetQimenAllPagedHistoryOrder(WdtQimenConfig config)
        {

            DefaultQimenCloudClient qmclient = new DefaultQimenCloudClient(config.ServerUrl, config.AppKey, config.Appsecret, "json");
            WdtHistoryTradeQueryRequest req = new WdtHistoryTradeQueryRequest();
            req.SetTargetAppKey(config.TargetAppKey);
            req.Sid = config.Sid;
            req.StartTime = config.Start_time;
            req.EndTime = config.End_time;
            req.PageSize = config.Page_size;
            req.PageNo = config.Page_no;
            req.SrcTid= config.SrcTid;
            if(!string.IsNullOrEmpty(config.ShopNo))
            { 
                req.ShopNo = config.ShopNo; 
            }
      

            WdtTradeQueryResponse res = qmclient.Execute(req);

            if (res.Errorcode != 0)
            {
                throw new Exception();
            }

            var orders = JsonConvert.DeserializeObject<QimenOrderDto>(res.Body);

            var result = orders.Response.Trades;

            while (req.PageNo < res.TotalCount / req.PageSize)
            {
                req.PageNo++;
                var pageRespnse = qmclient.Execute(req);
                result.AddRange(JsonConvert.DeserializeObject<QimenOrderDto>(pageRespnse.Body).Response.Trades);
            }

            return result;
        }


        public async Task<DateTime> GetApiLastStartTime(int apiId)
        {
            return await _apiRepository.GetApiLastStartTime(apiId);
        }

        public async Task<WdtClientConfig> GetApiConfig(int apiId)
        {
            return await _apiRepository.GetApiConfig(apiId);
        }


        public async Task SaveQnjsExcel(string filePath)
        {

            Workbook workbook = new Workbook(filePath);
            Cells cells = workbook.Worksheets[0].Cells;

            System.Data.DataTable dataTable = cells.ExportDataTable(1, 0, cells.MaxDataRow, cells.MaxColumn + 1);//没有标题
            string insertSql = dataTable.GenerateExcelQnjs().ToString();

            await _orderRepository.SaveOrder(insertSql);
        }

        public async Task GetAllPlan()
        {
            var plans = await _orderRepository.GetAllPlan();


            var answers = plans.Select(x => x.Answers).Distinct();
             

            var trainPlans=new List<qnjs_trainplan>();
            foreach (var answer in answers)
            {
                qnjs_trainplan trainplan = new qnjs_trainplan();
                trainplan.Answers=answer;
                trainplan.MondayCourses = string.Join(",",plans.Where(x => x.Answers == answer && x.Week == "mondayCourses").Select(x => x.ActionId));
                trainplan.TuesdayCourses = string.Join(",", plans.Where(x => x.Answers == answer && x.Week == "tuesdayCourses").Select(x => x.ActionId));
                trainplan.WednesdayCourses = string.Join(",", plans.Where(x => x.Answers == answer && x.Week == "wednesdayCourses").Select(x => x.ActionId));
                trainplan.ThursdayCourses = string.Join(",", plans.Where(x => x.Answers == answer && x.Week == "thursdayCourses").Select(x => x.ActionId));
                trainplan.FridayCourses = string.Join(",", plans.Where(x => x.Answers == answer && x.Week == "fridayCourses").Select(x => x.ActionId));
                trainPlans.Add(trainplan);
            }

            var sql = SqlHelper.GenerateInsertSql(trainPlans, "qnjs_trainplan");
            await _orderRepository.SaveOrder(sql);

        }


    }
}
