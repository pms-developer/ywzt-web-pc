﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Service
{
    public partial class WdtService : IWdtService
    {

        public async Task<List<WdtPurchaseProvider>> GetPurchaseProvider(WdtClientConfig config)
        {
            WdtClient client = new WdtClient()
            {
                sid = config.Sid,
                appkey = config.AppKey,
                appsecret = config.Appsecret,
                gatewayUrl = config.GatewayUrl,
                page_no = config.Page_no,
                page_size = config.Page_size
            };

            var wdtProviders = new List<WdtPurchaseProvider>();

            int totalcount = 0;

            GetWdtPagedProvider(client, wdtProviders, ref totalcount);

            return wdtProviders;
        }

        private void GetWdtPagedProvider(WdtClient client, List<WdtPurchaseProvider> result, ref int totalCount)
        {
            var proviers = client.wdtOpenapi();

            var wdtShops = JsonConvert.DeserializeObject<WDTProviderDto>(proviers);
            if (client.page_no == 0)
            {
                totalCount = int.Parse(wdtShops.Total_Count);
            }

            result.AddRange(wdtShops.provider_list);

            if (client.page_no < totalCount / client.page_size)
            {
                client.page_no++;
                GetWdtPagedProvider(client, result, ref totalCount);
            }
        }

        public async Task SavePurchaseProvider(List<WdtPurchaseProvider> WdtProvider, string tableName)
        {
            var sql = SqlHelper.GenerateInsertSql(WdtProvider, tableName);
            await _baseRepository.ExecuteSql(sql);
        }
    }
}


