﻿using Aspose.Cells;
using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using Polly;
using QimenCloud.Api;
using QimenCloud.Api.scenehu3cgwt0tc.Request;
using QimenCloud.Api.scenehu3cgwt0tc.Response;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Top.Api;

namespace Domain.Service
{
    public class BillingService : IBillingService
    {

        public BillingService()
        {
          
        }

       public List<WdtBilling> GetQimenAllPagedBilling(WdtQimenConfig config)
        {

            DefaultQimenCloudClient qmclient = new DefaultQimenCloudClient(config.ServerUrl, config.AppKey, config.Appsecret, "json");
            WdtFaApiAccountDetailQueryRequest req = new WdtFaApiAccountDetailQueryRequest();
            req.SetTargetAppKey(config.TargetAppKey);
            req.Sid = config.Sid;
            req.StartTime = config.Start_time.ToLongTimeString();
            req.EndTime = config.End_time.ToLongTimeString();
            req.PageSize = config.Page_size.ToString();
            req.PageNo = config.Page_no.ToString();
            if (!string.IsNullOrEmpty(config.ShopNo))
            {
                req.ShopNo = config.ShopNo;
            }


            WdtFaApiAccountDetailQueryResponse res = qmclient.Execute(req);
            if (res.Errorcode != "0")
            {
                throw new Exception();
            }
            var orders = JsonConvert.DeserializeObject<QimenBillingDto>(res.Body);

            var result = orders.Response.Account_list;
            int pageNo = 0;
            int totalCount =int.Parse(res.TotalCount);
            int pageSize = int.Parse(req.PageSize);

            while (pageNo < totalCount / pageSize)
            {
                pageNo++;
                req.PageNo= pageNo.ToString();
                var pageRespnse = qmclient.Execute(req);
                result.AddRange(JsonConvert.DeserializeObject<QimenBillingDto>(pageRespnse.Body).Response.Account_list);
            }

            return result;
        }
    }
}
