﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Service
{
    public partial class WdtService : IWdtService
    {

        public List<WdtGoods> GetGoods(WdtClientConfig config)
        {
            WdtClient client = new WdtClient()
            {
                sid = config.Sid,
                appkey = config.AppKey,
                appsecret = config.Appsecret,
                gatewayUrl = config.GatewayUrl,
                page_no = config.Page_no,
                page_size = config.Page_size,
                start_time = config.Start_time,
                end_time = config.LastEndTime
            };

            var wdtProviders = new List<WdtGoods>();

            int totalcount = 0;

            GetWdtPagedResult(client, wdtProviders, ref totalcount);

            return wdtProviders;
        }

        private void GetWdtPagedResult(WdtClient client, List<WdtGoods> result, ref int totalCount)
        {
            var proviers = client.wdtOpenapi();

            var results = JsonConvert.DeserializeObject<WDTGoodsDto>(proviers);
            
            if (client?.page_no == 0 && results.Total_Count != null)
            {
                totalCount = int.Parse(results.Total_Count);
            }

            if (results.Goods_list != null)
            {
                result.AddRange(results.Goods_list);
            }

         

            if (client.page_no < totalCount / client.page_size)
            {
                client.page_no++;
                GetWdtPagedResult(client, result, ref totalCount);
            }
        }

        public async Task SaveGoods(List<WdtGoods> dataList, string tableName)
        {
            var sql = SqlHelper.GenerateInsertSql(dataList, tableName);
            await _baseRepository.ExecuteSql(sql);
        }
    }
}


