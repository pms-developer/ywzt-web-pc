﻿using Domain.Service.Interface;
using Dop.Api.OrderSearchList;
using Dop.Core;
using System;
using Newtonsoft.Json;
using System.IO;
using Dop.Json;
using MongoDB.Driver;
using MongoDB.Bson;
using DbContext.Default;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Polly;
using Domain.Models.DTO;
using Doamin.Repositories;
using Dop.Api.OrderSearchList.Param;

namespace Domain.Service
{
    public class DYService : IDYService
    {
        private AccessToken accessToken;
        private readonly IApiRepository _apiRepository;
        public DYService(IApiRepository apiRepository)
        {
            _apiRepository = apiRepository;
            GlobalConfig.GetGlobalConfig().AppKey = System.Configuration.ConfigurationManager.AppSettings["dyAppKey"];
            GlobalConfig.GetGlobalConfig().AppSecret = System.Configuration.ConfigurationManager.AppSettings["dyAppSecret"];
            //accessToken = AccessTokenBuilder.Build(3545060);//入参为shopId 
        }  

        private static ICache tokenCache = new TokenCache();
        /// <summary>
        /// 抖音-订单列表查询接口
        /// </summary>
        /// <returns></returns>
        public async Task SyncDYGetOrder()
        {
            //await Task.Delay(1);  
            //string[] shopIDs = new string[] { "3545060", "9101048" };//为店铺的shopid，可在抖店开放平台查看，中台店铺主表中也加了字段，暂时手动sql维护
            long[] shopIDs = new long[] { 3545060 };
            foreach (var shopID in shopIDs)
            {
                #region 获取token
                var accessTokenCache = tokenCache.GetOrDefault(shopID.ToString());
                if (accessTokenCache != null)
                {
                    accessToken = AccessTokenBuilder.Parse(accessTokenCache.ToString());
                    long? expiresIn = accessToken.GetExpireIn();//过期时间戳（断点查看这个值好像不太对，显示为1970年的时间戳去了）
                }
                else
                {
                    //获取店铺token
                    accessToken = AccessTokenBuilder.Build(shopID);//入参为shopId  
                    tokenCache.Set(shopID.ToString(), accessToken.GetAccessToken(), null, TimeSpan.FromSeconds(604800));//设置为7天有效期
                }
                #endregion
                int page=0;
                var request = GetOrderSearchParam(page);
                var response = request.Execute<OrderSearchListResponse>(accessToken);
                if (response.Code == 10000)
                {
                    //入库
                    InsertMongodb(response);

                    //await _apiRepository.UpdateApiLog(new ApiLogDto { Id = batchNo, TotalCount = (int)response.Data.Total });
                    int pageNum = (int)Math.Ceiling((decimal)(response.Data.Total / 100));
                    while (pageNum > page)
                    {
                        page += 1;
                        Thread.Sleep(1000 * 10);

                        //Policy框架异常重试策略
                        Policy.Handle<Exception>().Or<System.Net.Http.HttpRequestException>().Or<System.Net.WebException>()
                        .WaitAndRetry(new[]
                        { 
                               TimeSpan.FromMinutes(2),
                               TimeSpan.FromMinutes(5),
                               TimeSpan.FromMinutes(10)
                        })
                        .Execute(() => { 
                            var totalNum = OrderSearchListApi(page).ConfigureAwait(false).GetAwaiter().GetResult();
                        }); 
                    }
                }
                else
                { 
                    await _apiRepository.CreateApiLog(new ApiLogDto { ApiName = "SyncDYGetOrder", ApiId = 4, Create_Time = DateTime.Now, QueryParam = request.ToJsonString(), Status = 0, Start_Time = GetDateTime(request.BuildParam().CreateTimeStart), End_time = GetDateTime(request.BuildParam().CreateTimeEnd) });
                }
            }

        }
        /// <summary>
        /// 初始化接口入参
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public OrderSearchListRequest GetOrderSearchParam(int page)
        {
            var request = new OrderSearchListRequest();
            int size = 100;
            var param = request.BuildParam();
            //param.Product = "3473196049974326153";//商品名称或ID
            //param.BType = 2L;//来源，2：抖音
            param.AfterSaleStatusDesc = "all";
            //param.PresellType = 1L;
            //param.OrderType = 1L;
            DateTime dateTime = DateTime.Now.AddDays(-1);
            param.CreateTimeStart = GetTimeStamp(dateTime);//下单时间-开始
            //param.CreateTimeEnd = 1617355413L;//下单时间-结束
            //param.UpdateTimeStart = 1617355413L;
            //param.UpdateTimeEnd = 1617355413L;
            param.Size = size;
            param.Page = page;
            param.OrderBy = "create_time";
            param.OrderAsc = false;
            return request;
        }
        /// <summary>
        /// 递归调用接口
        /// </summary>
        /// <param name="page"></param>
        public async Task<int> OrderSearchListApi(int page)
        { 
            var id = Thread.CurrentThread.ManagedThreadId;
            var name = Thread.CurrentThread.Name;
            var state= Thread.CurrentThread.ThreadState;

            var request = GetOrderSearchParam(page);

            //记录日志
            //int batchNo = await _apiRepository.CreateApiLog(new ApiLogDto { ApiName = "SyncDYGetOrder", ApiId = 4, Create_Time = DateTime.Now, QueryParam = param.ToJsonString(), Status = 0, Start_Time = DateTime.Now.AddDays(-1), End_time = DateTime.Now });
            var response = request.Execute<OrderSearchListResponse>(accessToken);

            if (response.Code == 10000)
            {
                //入库
                InsertMongodb(response);
            }
            else
            {
                //写入错误日志
                //await _apiRepository.CreateApiLog(new ApiLogDto { ApiName = "SyncDYGetOrder", ApiId = 4, Create_Time = DateTime.Now, QueryParam = response.ToJsonString(), Status = -1, Start_Time = DateTime.Now.AddDays(-1), End_time = DateTime.Now });    
                await _apiRepository.CreateApiLog(new ApiLogDto { ApiName = "SyncDYGetOrder", ApiId = 4, Create_Time = DateTime.Now, QueryParam = request.ToJsonString(), Status = 0, Start_Time = GetDateTime(request.BuildParam().CreateTimeStart), End_time = GetDateTime(request.BuildParam().CreateTimeEnd) });
            }
            return page;
        }
        public void InsertMongodb(object response)
        {
            var jsonStr = JsonConvert.SerializeObject(response);
            BsonDocument bson = BsonDocument.Parse(jsonStr); 
            MongoHelper<BsonDocument> dyOrdersMongoHelper = new MongoHelper<BsonDocument>("dyTable");
            dyOrdersMongoHelper.Insert(bson);
        }
        public void toMongoDB(object json)
        {
            string con = @"mongodb://127.0.0.1:27017";
            var client = new MongoClient(con);
            //数据库
            var hl = client.GetDatabase("douyin");
            //表
            IMongoCollection<BsonDocument> arti = hl.GetCollection<BsonDocument>("dy");

            BsonDocument bson = BsonDocument.Parse(JsonConvert.SerializeObject(json));
            //var bson = new BsonDocument { { "name", "cs" }, { "title", "aaaa" } };
            arti.InsertOne(bson);
        }

        /// <summary>
        /// 时间转换为时间搓
        /// </summary>
        /// <returns></returns>
        public int GetTimeStamp(DateTime dt)
        {
            DateTime dateStrat = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            int timeStamp = Convert.ToInt32((dt - dateStrat).TotalSeconds);
            return timeStamp;
        }
        /// <summary>
        /// 时间搓转换为时间
        /// </summary>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public DateTime GetDateTime(long? timestamp)
        { 
            if(timestamp==null)
            {
                return DateTime.MinValue;
            }
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));//当地时区
            return startTime.AddSeconds((long)timestamp);
        }

     
        /// <summary>
        /// 生成json文件
        /// </summary>
        /// <param name="response"></param>
        public void CreateResponseJson(Dop.Api.OrderSearchList.OrderSearchListResponse response)
        {
            #region 生成json文件，或者保存至MongDB
            string saveFile = string.Format(@"E:\RPA_Download\source\抖音订单\{0}", response.Data.ShopOrderList[0].ShopName);
            //获取文件保存路径
            if (!File.Exists(saveFile))
            {
                System.IO.Directory.CreateDirectory(saveFile);//不存在就创建文件夹 
            }
            string path = saveFile + string.Format(@"\orderInfo_{0}.json", "0");
            //将Json生成为文件
            File.WriteAllText(path, JsonConvert.SerializeObject(response.ToJson(), Formatting.Indented));
            #endregion
        }

    }
}
