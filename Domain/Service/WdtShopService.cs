﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Service
{
    public partial class WdtService : IWdtService
    {
        public IApiRepository _apiRepository;
        public IBaseRepository _baseRepository;

        public WdtService(IApiRepository apiRepository, IBaseRepository baseRepository)
        {
            _apiRepository = apiRepository;
            _baseRepository = baseRepository;
        }

        public async Task<List<WdtShop>> GetShop(WdtClientConfig config)
        {
            WdtClient client = new WdtClient()
            {
                sid = config.Sid,
                appkey = config.AppKey,
                appsecret = config.Appsecret,
                gatewayUrl = config.GatewayUrl ,
                page_no=config.Page_no,
                page_size=config.Page_size
            };

            var wdtShops = new List<WdtShop>();

            int totalcount = 0;

            GetWdtPagedShop(client, wdtShops, ref totalcount);


            return wdtShops;
        }

        private void GetWdtPagedShop(WdtClient client, List<WdtShop> result, ref int totalCount)
        {

            var wdtShops = JsonConvert.DeserializeObject<WDTShopDto>(client.wdtOpenapi());
            if (client.page_no == 0)
            {
                totalCount = int.Parse(wdtShops.Total_Count);
            }

            result.AddRange(wdtShops.Shoplist);

            if (client.page_no < totalCount / client.page_size)
            {
                client.page_no++;
                GetWdtPagedShop(client, result, ref totalCount);
            }
        }



        public async Task SaveShop(List<WdtShop> WdtShops, string tableName)
        {
            var sql = GenerateOrderSql(WdtShops,tableName);
            await _baseRepository.ExecuteSql(sql); 
        }

        private string GenerateOrderSql(List<WdtShop> WdtShops, string tableName)
        {
            StringBuilder orderSql = new StringBuilder();
            foreach (var shop in WdtShops)
            {
                orderSql.Append(shop.GenerateSql(tableName));
            }
            string orderCmd = orderSql.ToString();

            return orderCmd;
        }

    }
}
