﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Service.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Service
{
    public class ShopInfoService : IShopInfoService
    {
        public IShopInfoRepository _shopInfoRepository;
        public ShopInfoService(IShopInfoRepository shopInfoRepository)
        {
            _shopInfoRepository=shopInfoRepository;
        }
       public async Task<IEnumerable<BaseShopInfoDTO>> GetWdtShopCode(string shopId)
       {
            var shopIds = new string[] { };
            if (!string.IsNullOrEmpty(shopId))
            {
                 shopIds = shopId.Split(',');
            }
           
            var result= await _shopInfoRepository.GetWdtShopCode(shopIds);
            return result;
        }
    }
}
