﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Service
{
    public partial class WdtService : IWdtService
    {

        public List<WdtGoodsBrand> GetGoodsGrand(WdtClientConfig config)
        {
            WdtClient client = new WdtClient()
            {
                sid = config.Sid,
                appkey = config.AppKey,
                appsecret = config.Appsecret,
                gatewayUrl = config.GatewayUrl,
                page_no = config.Page_no,
                page_size = config.Page_size
            };

            var wdtProviders = new List<WdtGoodsBrand>();

            int totalcount = 0;

            GetWdtPagedResult(client, wdtProviders, ref totalcount);

            return wdtProviders;
        }

        private void GetWdtPagedResult(WdtClient client, List<WdtGoodsBrand> result, ref int totalCount)
        {
            var proviers = client.wdtOpenapi();

            var results = JsonConvert.DeserializeObject<WDTGoodsBrandDto>(proviers);
            if (client.page_no == 0)
            {
                totalCount = int.Parse(results.Total_Count);
            }

            result.AddRange(results.Brand_lists);

            if (client.page_no < totalCount / client.page_size)
            {
                client.page_no++;
                GetWdtPagedResult(client, result, ref totalCount);
            }
        }

        public async Task SaveGoodsGrand(List<WdtGoodsBrand> dataList, string tableName)
        {
            var sql = SqlHelper.GenerateInsertSql(dataList, tableName);
            await _baseRepository.ExecuteSql(sql);
        }
    }
}


