﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Domain.Service
{
    public class NCService : INCService
    {
        public INCRepository _repository;

        public NCService(INCRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<NcMaterial>> GetMaterialList()
        {
            var result = await _repository.GetMaterialList();
            return result.ToList();
        }

        public async Task SaveMaterialList(List<NcMaterial> dataList, string tableName)
        {
            var deleteExistedSql = deleteExistedMaterial(dataList);
            await _repository.ExecuteSql(deleteExistedSql);

            var sql = SqlHelper.GenerateInsertSql(dataList, tableName);
            await _repository.ExecuteSql(sql);
        }

        private string deleteExistedMaterial(List<NcMaterial> dataList)
        {
            return string.Concat($"delete from nc_MATERIAL where pk_material in ({string.Join(",", dataList.Select(x => string.Concat("'", x.PK_Material, "'")))})");
        }

        public async Task<List<NcSpplier>> GetSpplierlList()
        {
            var result = await _repository.GetSpplierlList();
            return result.ToList();
        }

        public async Task SaveSpplierList(List<NcSpplier> dataList, string tableName)
        {
            var sql = SqlHelper.GenerateInsertSql(dataList, tableName);
            await _repository.ExecuteSql(sql);
        }

        public async Task<List<NcWarehouse>> GetWarehouseList()
        {
            var result = await _repository.GetWarehouselList();
            return result.ToList();
        }

        public async Task SaveWarehouseList(List<NcWarehouse> dataList, string tableName)
        {
            var sql = SqlHelper.GenerateInsertSql(dataList, tableName);
            await _repository.ExecuteSql(sql);
        }

        public async Task<List<NcShop>> GetShopList()
        {
            var result = await _repository.GetShopList();
            return result.ToList();
        }

        public async Task SaveShopList(List<NcShop> dataList, string tableName)
        {
            var sql = SqlHelper.GenerateInsertSql(dataList, tableName);
            await _repository.ExecuteSql(sql);
        }

        public async Task<string> AddSaleInvoice(string requestUrl, NcSaleInvoice saleInvoices)
        {

            string jsonString = GenerateSaleInvoiceXml(saleInvoices);
            string result = "";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(requestUrl);
            req.Method = "POST";
            // req.ContentType = "application/x-www-form-urlencoded";

            #region 添加Post 参数
            byte[] data = Encoding.UTF8.GetBytes(jsonString);
            req.ContentLength = data.Length;
            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(data, 0, data.Length);
                reqStream.Close();
            }
            #endregion

            HttpWebResponse resp = (HttpWebResponse)(await req.GetResponseAsync());
            Stream stream = resp.GetResponseStream();
            //获取响应内容
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                result = reader.ReadToEnd();
            }
            return result;
        }

        private string GenerateSaleInvoiceXml(NcSaleInvoice saleInvoices)
        {
            XmlDocument doc = new XmlDocument();
            XmlDeclaration xmlDec;
            xmlDec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.InsertBefore(xmlDec, doc.DocumentElement);
            XmlElement root = doc.CreateElement("ufinterface");
            root.SetAttribute("account", "03");
            root.SetAttribute("billtype", "32");
            root.SetAttribute("filename", "saleInvoice");
            root.SetAttribute("groupcode", "00");
            root.SetAttribute("isexchange", "Y");
            root.SetAttribute("replace", "Y");
            root.SetAttribute("roottag", "Y");
            root.SetAttribute("sender", "SD_DZPT_01");
            doc.AppendChild(root);

            XmlElement bill = doc.CreateElement("bill");
            bill.SetAttribute("id", "");
            root.AppendChild(bill);

            XmlElement billhead = doc.CreateElement("billhead");
            bill.AppendChild(billhead);

            PopulateInvoiceHeader(billhead, saleInvoices.BillHead);

            XmlElement csaleinvoicebid = doc.CreateElement("csaleinvoicebid");
            billhead.AppendChild(csaleinvoicebid);

            PopulateInvoiceBody(csaleinvoicebid, saleInvoices.CsaleInvoicebId);

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            doc.WriteTo(xw);
            return sw.ToString();         
        }

        private void PopulateInvoiceHeader(XmlElement nodeinvoiceHead, NCInvoiceHeader invoiceHead)
        {
            foreach (PropertyInfo propertyInfo in typeof(NCInvoiceHeader).GetProperties())
            {
                var propertyValue = propertyInfo.GetValue(invoiceHead, null);
                if (null == propertyValue)
                {
                    continue;
                }

                XmlElement element = nodeinvoiceHead.OwnerDocument.CreateElement(propertyInfo.Name);
                element.InnerText = propertyValue.ToString();
                nodeinvoiceHead.AppendChild(element);
            }
        }

        private void PopulateInvoiceBody(XmlElement nodeinvoiceBody, List<NCInvoiceDetail> invoiceBodys)
        {
            foreach(var invoiceBody in invoiceBodys)
            {
                XmlElement item = nodeinvoiceBody.OwnerDocument.CreateElement("item");
                nodeinvoiceBody.AppendChild(item);

                foreach (PropertyInfo propertyInfo in typeof(NCInvoiceDetail).GetProperties())
                {
                    var propertyValue = propertyInfo.GetValue(invoiceBody, null);
                    if (null == propertyValue)
                    {
                        continue;
                    }

                    XmlElement element = item.OwnerDocument.CreateElement(propertyInfo.Name);
                    element.InnerText = propertyValue.ToString();
                    item.AppendChild(element);
                }
            }            
        }

    }
}
