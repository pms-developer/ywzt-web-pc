﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using Domain.Service.Interface;
using Domain.Unit;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Service
{
    public partial class WdtService : IWdtService
    {

        public async Task<List<WdtWarehouse>> GetWareHouse(WdtClientConfig config)
        {
            WdtClient client = new WdtClient()
            {
                sid = config.Sid,
                appkey = config.AppKey,
                appsecret = config.Appsecret,
                gatewayUrl = config.GatewayUrl ,
                page_no=config.Page_no,
                page_size=config.Page_size
            };

            var wdtShops = new List<WdtWarehouse>();

            int totalcount = 0;

            GetWdtPagedWareHouse(client, wdtShops, ref totalcount);


            return wdtShops;
        }

        private void GetWdtPagedWareHouse(WdtClient client, List<WdtWarehouse> result, ref int totalCount)
        {
            var warehouses = client.wdtOpenapi();
            var wdtShops = JsonConvert.DeserializeObject<WDTWareHouseDto>(warehouses);
            if (client.page_no == 0)
            {
                totalCount = int.Parse(wdtShops.Total_Count);
            }

            result.AddRange(wdtShops.warehouses);

            if (client.page_no < totalCount / client.page_size)
            {
                client.page_no++;
                GetWdtPagedWareHouse(client, result, ref totalCount);
            }
        }



        public async Task SaveWareHouse(List<WdtWarehouse> WdtShops, string tableName)
        {
            var sql = GenerateWareHouseSql(WdtShops,tableName);
            await _baseRepository.ExecuteSql(sql); 
        }

        private string GenerateWareHouseSql(List<WdtWarehouse> WdtShops, string tableName)
        {
            StringBuilder orderSql = new StringBuilder();
            foreach (var shop in WdtShops)
            {
                orderSql.Append(shop.GenerateSql(tableName));
            }
            string orderCmd = orderSql.ToString();

            return orderCmd;
        }

    }
}
