﻿using Doamin.Repositories;
using Domain.Models;
using Domain.Models.DTO;
using Domain.Service.Interface;
using System.Threading.Tasks;

namespace Domain.Service
{
    public class ApiService: IApiService
    {
        public IApiRepository _apiRepository;
        public ApiService(IApiRepository apiRepository)
        {
            _apiRepository = apiRepository;
        }
       public async Task UpdateApi(ApiDto apiDto)
        {
           await _apiRepository.UpdateApi(apiDto);
        }

       public async Task<int>  CreateApiLog(ApiLogDto apiLogDto)
        {
           return await _apiRepository.CreateApiLog(apiLogDto);
        }

        public async Task<WdtClientConfig> GetApiConfig(int apiId)
        {
            return await _apiRepository.GetApiConfig(apiId);
        }
    }
}
