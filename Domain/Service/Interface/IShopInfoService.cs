﻿using Domain.Models;
using Domain.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface IShopInfoService
    {
       Task<IEnumerable<BaseShopInfoDTO>> GetWdtShopCode(string shopId);
    }
}
