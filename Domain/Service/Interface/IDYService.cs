﻿using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface IDYService
    {
        Task SyncDYGetOrder();
    }
}
