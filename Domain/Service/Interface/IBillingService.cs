﻿using Domain.Models;
using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface IBillingService
    {     
        List<WdtBilling> GetQimenAllPagedBilling(WdtQimenConfig config);
      
    }
}
