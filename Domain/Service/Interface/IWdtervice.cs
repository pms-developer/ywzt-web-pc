﻿using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface IWdtService
    {
        Task<List<WdtShop>> GetShop(WdtClientConfig config);

        Task SaveShop(List<WdtShop> WdtShops, string tableName);

        Task<List<WdtPurchaseProvider>> GetPurchaseProvider(WdtClientConfig config);

        Task SavePurchaseProvider(List<WdtPurchaseProvider> WdtProvider, string tableName);

        Task<List<WdtWarehouse>> GetWareHouse(WdtClientConfig config);

        Task SaveWareHouse(List<WdtWarehouse> WdtShops, string tableName);

        Task<List<WdtGoodsClass>> GetGoodsClass(WdtClientConfig config);

        Task SaveGoodsClass(List<WdtGoodsClass> WdtGoodsClass, string tableName);

        List<WdtGoodsBrand> GetGoodsGrand(WdtClientConfig config);

        Task SaveGoodsGrand(List<WdtGoodsBrand> WdtGoodsBrand, string tableName);

        List<WdtGoods> GetGoods(WdtClientConfig config);
        
        Task SaveGoods(List<WdtGoods> WdtGoods, string tableName);

        List<WdtPlatformGoods> GetPlatformGoods(WdtClientConfig config);

        Task SavePlatformGoods(List<WdtPlatformGoods> WdtGoodsBrand, string tableName);


    }
}
