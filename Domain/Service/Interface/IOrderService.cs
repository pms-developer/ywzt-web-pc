﻿using Domain.Models;
using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface IOrderService
    {
        Task<WdtOrder> GetOrderById(string orderId);

        Task SaveWdtOdrer(WdtClientConfig config);

        Task SaveQimenOdrer(WdtQimenConfig config);

        Task SaveQimenHistoryOdrer(WdtQimenConfig config);

        Task<DateTime> GetApiLastStartTime(int apiId);

        Task SaveExcelOrder(string foldPath);
        Task SyncOrder();

        List<WdtOrder> GetQimenAllPagedOrder(WdtQimenConfig config);

        List<WdtOrder> GetQimenAllPagedHistoryOrder(WdtQimenConfig config);

        Task SaveQnjsExcel(string foldPath);
        Task GetAllPlan();
    }
}
