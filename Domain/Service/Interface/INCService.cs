﻿using Domain.Models;
using Domain.Models.DTO;
using Domain.Models.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface INCService
    {
        Task<List<NcMaterial>> GetMaterialList();
        Task SaveMaterialList(List<NcMaterial> dataList, string tableName);     

        Task<List<NcSpplier>> GetSpplierlList();
        Task SaveSpplierList(List<NcSpplier> dataList, string tableName);

        Task<List<NcWarehouse>> GetWarehouseList();
        Task SaveWarehouseList(List<NcWarehouse> dataList, string tableName);

        Task<List<NcShop>> GetShopList();

        Task SaveShopList(List<NcShop> dataList, string tableName);

        Task<string> AddSaleInvoice(string url, NcSaleInvoice saleInvoices);
    }
}
