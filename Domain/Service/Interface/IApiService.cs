﻿using Domain.Models;
using Domain.Models.DTO;
using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface IApiService
    {
        Task UpdateApi(ApiDto config);

        Task<int> CreateApiLog(ApiLogDto config);
        Task<WdtClientConfig> GetApiConfig(int apiId);
    }
}
