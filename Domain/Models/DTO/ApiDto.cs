﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class ApiDto
    {
        public int Id { get; set; }
        public string ApiName { get; set; }
        public DateTime LastEndTime { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
