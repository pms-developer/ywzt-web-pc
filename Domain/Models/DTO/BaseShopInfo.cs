﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class BaseShopInfoDTO
    {
        public string ShopId { get; set; }
        public string ShopName { get; set; }
        public string ShopCode { get; set; }
    }
}
