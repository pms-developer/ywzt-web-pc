﻿using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class WDTGoodsDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Total_Count { get; set; }

        public List<WdtGoods> Goods_list { get; set; } 
    }
}
