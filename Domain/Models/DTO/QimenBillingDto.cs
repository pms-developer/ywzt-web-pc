﻿using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class QimenBillingDto
    {
       public QimenBillingBodyResponse Response { get; set; }

    }

    public class QimenBillingBodyResponse
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public int Total_Count { get; set; }

        public List<WdtBilling> Account_list { get; set; }

    }

}
