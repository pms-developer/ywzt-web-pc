﻿using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class WDTGoodsBrandDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Total_Count { get; set; }

        public List<WdtGoodsBrand> Brand_lists { get; set; } 
    }
}
