﻿using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class WDTGoodsClassDto
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Total_Count { get; set; }

        public List<WdtGoodsClass> Goods_Class { get; set; } 
    }
}
