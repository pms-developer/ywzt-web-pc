﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class ApiLogDto
    {
        public int Id { get; set; }
        public int ApiId { get; set; }
        public string ApiName { get; set; }
        public DateTime Create_Time { get; set; }
        public DateTime Start_Time { get; set; }
        public DateTime End_time { get; set; }
        public int Status { get; set; }
        public string Comment { get; set; }
        public string QueryParam { get; set; }
        public int TotalCount { get; set; }
    }
}
