﻿using Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.DTO
{
    public class QimenOrderDto
    {
       public QimenBodyResponse Response { get; set; }

    }

    public class QimenBodyResponse
    {
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public int Total_Count { get; set; }

        public List<WdtOrder> Trades { get; set; }

    }

}
