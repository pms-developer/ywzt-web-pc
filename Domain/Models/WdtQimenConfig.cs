﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class WdtQimenConfig
    {
        public string ServerUrl { get; set; }
        public string Sid { get; set; }
        public string AppKey { get; set; }
        public string Appsecret { get; set; }
        public string TargetAppKey { get; set; }
        public DateTime Start_time { get; set; }
        public DateTime End_time { get; set; }
        public int Page_size { get; set; }
        public int Page_no { get; set; } 
        public string ShopNo { get; set; }
        public string TradeNo { get; set; }
        public string SrcTid { get; set; }
    }
}
