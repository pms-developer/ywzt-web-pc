﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class WdtClientConfig
    {
        public int ApiId { get; set; }
        public string ApiName { get; set; }
        public string Sid { get; set; }
        public string AppKey { get; set; }
        public string Appsecret { get; set; }
        public string GatewayUrl { get; set; }
        public DateTime? Start_time { get; set; }
        public DateTime? LastEndTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public int Page_size { get; set; } = 100;
        public int Page_no { get; set; } = 0;
        public int TimeQueryRang { get; set; }
    }
}
