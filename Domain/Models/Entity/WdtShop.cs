﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtShop
    {
        #region  实体成员 
        /// <summary> 
        /// 平台ID详情介绍单击这里datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PLATFORM_ID")]
        public byte? platform_id { get; set; }
        /// <summary> 
        /// 子平台IDdatetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("SUB_PLATFORM_ID")]
        public byte? sub_platform_id { get; set; }
        /// <summary> 
        /// 店铺列表主键datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_ID")]
        public int? shop_id { get; set; }
        /// <summary> 
        /// 代表店铺所有属性的唯一编码，用于店铺区分，ERP内支持自定义（ERP店铺界面设置）datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_NO")]
        public string shop_no { get; set; }
        /// <summary> 
        /// ERP店铺界面的店铺名称datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_NAME")]
        public string shop_name { get; set; }
        /// <summary> 
        /// 平台授权账号IDdatetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("ACCOUNT_ID")]
        public string account_id { get; set; }
        /// <summary> 
        /// 平台授权账号昵称datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("ACCOUNT_NICK")]
        public string account_nick { get; set; }
        /// <summary> 
        /// ERP店铺设置中的省份IDdatetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROVINCE")]
        public int? province { get; set; }
        /// <summary> 
        /// ERP店铺设置中的城市IDdatetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CITY")]
        public int? city { get; set; }
        /// <summary> 
        /// ERP店铺设置中的区县IDdatetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISTRICT")]
        public int? district { get; set; }
        /// <summary> 
        /// ERP店铺设置中的省份名称信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROVINCE_NAME")]
        public string province_name { get; set; }
        /// <summary> 
        /// ERP店铺设置中的城市名称信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CITY_NAME")]
        public string city_name { get; set; }
        /// <summary> 
        /// ERP店铺设置中的区县名称信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISTRICT_NAME")]
        public string district_name { get; set; }
        /// <summary> 
        /// ERP店铺设置中的地址信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("ADDRESS")]
        public string address { get; set; }
        /// <summary> 
        /// ERP店铺设置中的联系人信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CONTACT")]
        public string contact { get; set; }
        /// <summary> 
        /// ERP店铺设置中的邮编信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("ZIP")]
        public string zip { get; set; }
        /// <summary> 
        /// ERP店铺设置中的手机信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("MOBILE")]
        public string mobile { get; set; }
        /// <summary> 
        /// ERP店铺设置中的电话信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("TELNO")]
        public string telno { get; set; }
        /// <summary> 
        /// ERP店铺设置中的备注信息datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK")]
        public string remark { get; set; }
        /// <summary> 
        /// 账款账号，如支付宝，财付通datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAY_ACCOUNT_ID")]
        public string pay_account_id { get; set; }
        /// <summary> 
        /// 授权状态，0未授权 1已授权 2授权失效  3授权停用datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUTH_STATE")]
        public string auth_state { get; set; }
        /// <summary> 
        /// 是否启用推送 0不推送 其它值对应RDS服务器datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PUSH_RDS_ID")]
        public string push_rds_id { get; set; }
        /// <summary> 
        /// 支付宝授权状态 0未授权 1已授权 2授权失效  3授权停用datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAY_AUTH_STATE")]
        public string pay_auth_state { get; set; }
        
        
      
        /// <summary> 
        /// 冻结授权到此时间为止(unix时间戳)datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("FREEZE_AUTH_TO")]
        public string freeze_auth_to { get; set; }
        /// <summary> 
        /// 检查是否是物流宝订单datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("WMS_CHECK")]
        public string wms_check { get; set; }
        /// <summary> 
        /// 使用物流公司0表示按物流策略来计算datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("LOGISTICS_ID")]
        public string logistics_id { get; set; }
        /// <summary> 
        /// 货到付款物流datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("COD_LOGISTICS_ID")]
        public string cod_logistics_id { get; set; }
        /// <summary> 
        /// 国家datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("COUNTRY")]
        public string country { get; set; }
        /// <summary> 
        /// 地址库iddatetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("ADDRESS_ID")]
        public string address_id { get; set; }
        /// <summary> 
        /// 邮件datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("EMAIL")]
        public string email { get; set; }
        /// <summary> 
        /// 店铺网址datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("WEBSITE")]
        public string website { get; set; }
        /// <summary> 
        /// 自定义属性1datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP1")]
        public string prop1 { get; set; }
        /// <summary> 
        /// 自定义属性2datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP2")]
        public string prop2 { get; set; }
        /// <summary> 
        /// 是否禁止自动抓取订单datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_UNDOWNLOAD_TRADE")]
        public string is_undownload_trade { get; set; }
        /// <summary> 
        /// 服务商id，电子发票专用datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICE_PROVIDER_ID")]
        public string invoice_provider_id { get; set; }
        /// <summary> 
        /// 分组id,初始等于0datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("GROUP_ID")]
        public string group_id { get; set; }
        /// <summary> 
        /// 米氏抢单开关datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_HOLD_ENABLED")]
        public string is_hold_enabled { get; set; }
        /// <summary> 
        /// 递交中禁止店铺自动合并datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_NOMERGE")]
        public string is_nomerge { get; set; }
        /// <summary> 
        /// 递交中禁止店铺自动拆分datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_NOSPLIT")]
        public string is_nosplit { get; set; }
        /// <summary> 
        /// 递交中是否禁用货品指定仓库策略datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_FORBIDDEN_SETWAREBYGOODS")]
        public string is_forbidden_setwarebygoods { get; set; }
        /// <summary> 
        /// 货品指定方式 0无 1指定仓库 2指定仓库地址datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_SETWAREBYGOODS")]
        public string is_setwarebygoods { get; set; }
        /// <summary> 
        /// 自定义字段datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("CUST_DATA")]
        public string cust_data { get; set; }
        /// <summary> 
        /// 是否转入预订单，默认0datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_TOPREORDER")]
        public string is_topreorder { get; set; }
        /// <summary> 
        /// 是否停用datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_DISABLED")]
        public string is_disabled { get; set; }
        /// <summary> 
        /// 非淘宝店铺关联淘宝店铺,电子发票专用datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAOBAO_SHOP_ID")]
        public string taobao_shop_id { get; set; }
        /// <summary> 
        /// 收款方id,电子发票专用datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICE_PAYEE_ID")]
        public string invoice_payee_id { get; set; }
        /// <summary> 
        /// 纸质服务商iddatetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAPER_INVOICE_PROVIDER_ID")]
        public string paper_invoice_provider_id { get; set; }
        /// <summary> 
        /// 物流同步延迟小时数datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("LOGISTICS_SYNC_DELAY")]
        public string logistics_sync_delay { get; set; }
        /// <summary> 
        /// 店铺禁用物流列表datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("FORBIDDEN_LOGISTICS_LIST")]
        public string forbidden_logistics_list { get; set; }
        /// <summary> 
        /// 店铺展示顺序datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_PRIORITY")]
        public string shop_priority { get; set; }
        /// <summary> 
        /// 大单的单品数量最低限制，为0代表不使用datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("GREAT_DEAL_LIMIT")]
        public string great_deal_limit { get; set; }
        /// <summary> 
        /// 大单指定仓库datetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("GREAT_DEAL_WAREHOUSE")]
        public string great_deal_warehouse { get; set; }
        /// <summary> 
        /// 最后修改时间    格式：yyyy-MM-dd HH:mm:ssdatetime 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFIED")]
        public DateTime? modified { get; set; }
        /// <summary> 
        /// 创建时间    格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATED")]
        public DateTime? created { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(byte? keyValue)
        {
            this.platform_id = keyValue;
        }
        #endregion
    }
}
