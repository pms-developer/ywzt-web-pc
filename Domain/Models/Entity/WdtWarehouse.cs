﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtWarehouse
    {
        #region  实体成员 
        /// <summary> 
        /// 代表仓库所有属性的唯一编码，用于仓库区分，ERP内支持自定义（ERP仓库界面设置），用于获取指定仓库数据信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("WAREHOUSE_NO")]
        public string warehouse_no { get; set; }
        /// <summary> 
        /// ERP仓库界面的仓库名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("NAME")]
        public string name { get; set; }
        /// <summary> 
        /// （0不限 1普通仓库 2自动流传外部 3京东仓储 4科捷 5百世物流 6 SKU360 7通天晓 8中联网仓 9顺丰仓储 10网仓2号 11奇门仓储 12旺店通仓储 13心怡仓储 14力威仓储 20外部链路型 127其它） 
        /// </summary> 
        /// <returns></returns> 
        [Column("WAREHOUSE_TYPE")]
        public byte? warehouse_type { get; set; }
        /// <summary> 
        /// 外部仓库编号 
        /// </summary> 
        /// <returns></returns> 
        [Column("EXT_WAREHOUSE_NO")]
        public string ext_warehouse_no { get; set; }
        /// <summary> 
        /// 仓库设置的省份信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROVINCE")]
        public string province { get; set; }
        /// <summary> 
        /// 仓库设置的城市信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("CITY")]
        public string city { get; set; }
        /// <summary> 
        /// 仓库设置的区县信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISTRICT")]
        public string district { get; set; }
        /// <summary> 
        /// 仓库地址 
        /// </summary> 
        /// <returns></returns> 
        [Column("ADDRESS")]
        public string address { get; set; }
        /// <summary> 
        /// 仓库设置中的联系人 
        /// </summary> 
        /// <returns></returns> 
        [Column("CONTACT")]
        public string contact { get; set; }
        /// <summary> 
        /// 仓库设置中的固话信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("TELNO")]
        public string telno { get; set; }
        /// <summary> 
        /// 邮编 
        /// </summary> 
        /// <returns></returns> 
        [Column("ZIP")]
        public string zip { get; set; }
        /// <summary> 
        /// 仓库设置中的手机信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("MOBILE")]
        public string mobile { get; set; }
        /// <summary> 
        /// 是否是残次仓（0不是，1是） 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_DEFECT")]
        public byte? is_defect { get; set; }
        /// <summary> 
        /// 仓库设置中的备注信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK")]
        public string remark { get; set; }
        /// <summary> 
        /// 自定义1(ERP内不显示该字段) 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP1")]
        public string prop1 { get; set; }
        /// <summary> 
        /// 自定义2(ERP内不显示该字段) 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP2")]
        public string prop2 { get; set; }
        /// <summary> 
        /// 仓库id 
        /// </summary> 
        /// <returns></returns> 
        [Column("WAREHOUSE_ID")]
        public byte? warehouse_id { get; set; }
        /// <summary> 
        /// 仓库类型 
        /// </summary> 
        /// <returns></returns> 
        [Column("TYPE")]
        public byte? type { get; set; }
        /// <summary> 
        /// 仓库子类型 
        /// </summary> 
        /// <returns></returns> 
        [Column("SUB_TYPE")]
        public byte? sub_type { get; set; }
        /// <summary> 
        /// API中商家的唯一标识 
        /// </summary> 
        /// <returns></returns> 
        [Column("API_KEY")]
        public string api_key { get; set; }
        /// <summary> 
        /// 和ext_warehouse_no一起保证唯一 
        /// </summary> 
        /// <returns></returns> 
        [Column("API_OBJECT_ID")]
        public string api_object_id { get; set; }
        /// <summary> 
        /// 委外仓禁止使用 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAG")]
        public string tag { get; set; }
        /// <summary> 
        /// 区编号,云栈热敏用 
        /// </summary> 
        /// <returns></returns> 
        [Column("DIVISION_ID")]
        public string division_id { get; set; }
        /// <summary> 
        /// 用于下载标记 
        /// </summary> 
        /// <returns></returns> 
        [Column("FLAG")]
        public string flag { get; set; }
        /// <summary> 
        /// 实体店仓库店铺id,米氏抢单用 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_ID")]
        public int? shop_id { get; set; }
        /// <summary> 
        /// 使用外部库存 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_OUTER_STOCK")]
        public string is_outer_stock { get; set; }
        /// <summary> 
        /// 代销仓库,货品没有所有权 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_OUTER_GOODS")]
        public string is_outer_goods { get; set; }
        /// <summary> 
        /// 仓库拣货员数量--智选批次使用 
        /// </summary> 
        /// <returns></returns> 
        [Column("PICKER_NUM")]
        public int? picker_num { get; set; }
        /// <summary> 
        /// 地理坐标_x 
        /// </summary> 
        /// <returns></returns> 
        [Column("COORDINATES_X")]
        public decimal? coordinates_x { get; set; }
        /// <summary> 
        /// 地理坐标_y 
        /// </summary> 
        /// <returns></returns> 
        [Column("COORDINATES_Y")]
        public decimal? coordinates_y { get; set; }
        /// <summary> 
        /// 对应的残次品/正品仓id 
        /// </summary> 
        /// <returns></returns> 
        [Column("MATCH_WAREHOUSE_ID")]
        public byte? match_warehouse_id { get; set; }
        /// <summary> 
        /// 货到付款物流公司 
        /// </summary> 
        /// <returns></returns> 
        [Column("COD_LOGISTICS_ID")]
        public string cod_logistics_id { get; set; }
        /// <summary> 
        /// 是否停用 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_DISABLED")]
        public string is_disabled { get; set; }
        /// <summary> 
        /// 仓库的优先级 
        /// </summary> 
        /// <returns></returns> 
        [Column("PRIORITY")]
        public string priority { get; set; }
        /// <summary> 
        /// 创建时间， 格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATED")]
        public DateTime? created { get; set; }
        /// <summary> 
        /// 最后修改时间， 格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFIED")]
        public DateTime? modified { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.warehouse_no = Guid.NewGuid().ToString();
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
            this.warehouse_no = keyValue;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.warehouse_no = keyValue;
        }
        #endregion
    }
}
