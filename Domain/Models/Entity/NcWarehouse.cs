﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Entity
{
    public class NcWarehouse
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string PK_stordoc { get; set; }
        public string Gubflag { get; set; }
        public string Csflag { get; set; }
        public string Isdirectstore { get; set; }
        public string Iscommissionout { get; set; }

    }
}
