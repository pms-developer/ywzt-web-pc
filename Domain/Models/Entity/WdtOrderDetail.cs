﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtOrderDetail
    {
        #region  实体成员 
        /// <summary> 
        /// erp子订单主键 
        /// </summary> 
        /// <returns></returns> 
        
        [Column("REC_ID")]
        public int rec_id { get; set; }
        /// <summary> 
        /// erp订单主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("TRADE_ID")]
        public int? trade_id { get; set; }
        /// <summary> 
        /// erp内商品主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPEC_ID")]
        public int? spec_id { get; set; }
        /// <summary> 
        /// 平台ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("PLATFORM_ID")]
        public byte? platform_id { get; set; }
        /// <summary> 
        /// 原始子订单号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SRC_OID")]
        public string src_oid { get; set; }
        /// <summary> 
        /// 平台货品ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("PLATFORM_GOODS_ID")]
        public string platform_goods_id { get; set; }
        /// <summary> 
        /// 平台商品ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("PLATFORM_SPEC_ID")]
        public string platform_spec_id { get; set; }
        /// <summary> 
        /// 如果货品是由组合装拆分的，这里组合装ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("SUITE_ID")]
        public int? suite_id { get; set; }
        /// <summary> 
        /// 子单拆分时，单品明细都一致，用来区分子单的唯一性 
        /// </summary> 
        /// <returns></returns> 
        [Column("FLAG")]
        public int? flag { get; set; }
        /// <summary> 
        /// 原始订单号 
        /// </summary> 
        /// <returns></returns> 
        [Column("SRC_TID")]
        public string src_tid { get; set; }
        /// <summary> 
        /// 是否是赠品 0非赠品 1自动赠送 2手工赠送 
        /// </summary> 
        /// <returns></returns> 
        [Column("GIFT_TYPE")]
        public byte? gift_type { get; set; }
        /// <summary> 
        /// 退款状态 0无退款,1取消退款,2已申请退款,3等待退货,4等待收货,5退款成功 6已关闭 
        /// </summary> 
        /// <returns></returns> 
        [Column("REFUND_STATUS")]
        public byte? refund_status { get; set; }
        /// <summary> 
        /// 1担保 2非担保 3在线非担保 
        /// </summary> 
        /// <returns></returns> 
        [Column("GUARANTEE_MODE")]
        public byte? guarantee_mode { get; set; }
        /// <summary> 
        /// 1款到发货 2货到付款(包含部分货到付款) 3分期付款 
        /// </summary> 
        /// <returns></returns> 
        [Column("DELIVERY_TERM")]
        public byte? delivery_term { get; set; }
        /// <summary> 
        /// 关联发货 
        /// </summary> 
        /// <returns></returns> 
        [Column("BIND_OID")]
        public string bind_oid { get; set; }
        /// <summary> 
        /// 货品数量 
        /// </summary> 
        /// <returns></returns> 
        [Column("NUM")]
        public decimal? num { get; set; }
        /// <summary> 
        /// 销售单价，手工新建时使用货品属性中的“零售价” 
        /// </summary> 
        /// <returns></returns> 
        [Column("PRICE")]
        public decimal? price { get; set; }
        /// <summary> 
        /// 实发数量,此数量为发货数量,删除操作等于将此值设置为0 
        /// </summary> 
        /// <returns></returns> 
        [Column("ACTUAL_NUM")]
        public decimal? actual_num { get; set; }
        /// <summary> 
        /// 售后退款数量 
        /// </summary> 
        /// <returns></returns> 
        [Column("REFUND_NUM")]
        public decimal? refund_num { get; set; }
        /// <summary> 
        /// 成交价,原始单折扣及分摊之后的价格 
        /// </summary> 
        /// <returns></returns> 
        [Column("ORDER_PRICE")]
        public decimal? order_price { get; set; }
        /// <summary> 
        /// 进入ERP后再次调整的价格，默认值与order_price一致 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHARE_PRICE")]
        public decimal? share_price { get; set; }
        /// <summary> 
        /// 手工调整价,正数为加价,负数为减价 
        /// </summary> 
        /// <returns></returns> 
        [Column("ADJUST")]
        public decimal? adjust { get; set; }
        /// <summary> 
        /// 总折扣金额 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISCOUNT")]
        public decimal? discount { get; set; }
        /// <summary> 
        /// 分摊后合计应收=share_price * num , share_price 是根据share_amount反推的,因此share_price可能有精度损失 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHARE_AMOUNT")]
        public decimal? share_amount { get; set; }
        /// <summary> 
        /// 分摊邮费 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHARE_POST")]
        public decimal? share_post { get; set; }
        /// <summary> 
        /// 已支付金额 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAID")]
        public decimal? paid { get; set; }
        /// <summary> 
        /// 货品名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_NAME")]
        public string goods_name { get; set; }
        /// <summary> 
        /// 分类名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("CLASS_NAME")]
        public string class_name { get; set; }
        /// <summary> 
        /// 自定义属性2 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP2")]
        public string prop2 { get; set; }
        /// <summary> 
        /// 分销商id 
        /// </summary> 
        /// <returns></returns> 
        [Column("TC_ORDER_ID")]
        public string tc_order_id { get; set; }
        /// <summary> 
        /// 主条码 
        /// </summary> 
        /// <returns></returns> 
        [Column("BARCODE")]
        public string barcode { get; set; }
        /// <summary> 
        /// 货品id 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_ID")]
        public int? goods_id { get; set; }
        /// <summary> 
        /// 代表货品(spu)所有属性的唯一编号，用于系统货品区分，，SPU概念介绍单击这里 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_NO")]
        public string goods_no { get; set; }
        /// <summary> 
        /// 规格名 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPEC_NAME")]
        public string spec_name { get; set; }
        /// <summary> 
        /// 代表单品(sku)所有属性的唯一编码，用于系统单品区分，SKU概念介绍,单击这里 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPEC_NO")]
        public string spec_no { get; set; }
        /// <summary> 
        /// 规格码 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPEC_CODE")]
        public string spec_code { get; set; }
        /// <summary> 
        /// 代表组合装商品所有属性的唯一编码，用于系统组合装商品的区分 
        /// </summary> 
        /// <returns></returns> 
        [Column("SUITE_NO")]
        public string suite_no { get; set; }
        /// <summary> 
        /// 如果是组合装拆分的，此为组合装名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("SUITE_NAME")]
        public string suite_name { get; set; }
        /// <summary> 
        /// 组合装数量 
        /// </summary> 
        /// <returns></returns> 
        [Column("SUITE_NUM")]
        public decimal? suite_num { get; set; }
        /// <summary> 
        /// 组合装分摊后总价 
        /// </summary> 
        /// <returns></returns> 
        [Column("SUITE_AMOUNT")]
        public decimal? suite_amount { get; set; }
        /// <summary> 
        /// 组合装优惠 
        /// </summary> 
        /// <returns></returns> 
        [Column("SUITE_DISCOUNT")]
        public decimal? suite_discount { get; set; }
        /// <summary> 
        /// share_amount备份值,退款恢复使用,!可回收 目前会存放分销订单发货回传的分销价 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHARE_AMOUNT2 ")]
        public decimal? share_amount2 { get; set; }
        /// <summary> 
        /// 0:组合装明细/1:组合装以及明细/2:组合装 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_PRINT_SUITE")]
        public byte? is_print_suite { get; set; }
        /// <summary> 
        /// 是否允许0成本 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_ZERO_COST ")]
        public byte? is_zero_cost { get; set; }
        /// <summary> 
        /// 库存保留情况 0未保留（取消的订单或完成）1无库存记录 2未付款 3已保留待审核 4待发货 5预订单库存 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_RESERVED ")]
        public byte? stock_reserved { get; set; }
        /// <summary> 
        /// 平台已发货 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_CONSIGNED")]
        public byte? is_consigned { get; set; }
        /// <summary> 
        /// 线上订单，标记是否打款 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_RECEIVED")]
        public byte? is_received { get; set; }
        /// <summary> 
        /// 是否主子订单,为发货算法使用 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_MASTER  ")]
        public byte? is_master { get; set; }
        /// <summary> 
        /// 平台货品名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("API_GOODS_NAME")]
        public string api_goods_name { get; set; }
        /// <summary> 
        /// 平台规格名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("API_SPEC_NAME")]
        public string api_spec_name { get; set; }
        /// <summary> 
        /// 预估单个货品重量 
        /// </summary> 
        /// <returns></returns> 
        [Column("WEIGHT")]
        public decimal? weight { get; set; }
        /// <summary> 
        /// 佣金 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMMISSION")]
        public decimal? commission { get; set; }
        /// <summary> 
        /// 1销售商品 2原材料 3包装 4周转材料5虚拟商品 0其它 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_TYPE")]
        public byte? goods_type { get; set; }
        /// <summary> 
        /// 大件类型 0非大件 1普通大件 2独立大件 
        /// </summary> 
        /// <returns></returns> 
        [Column("LARGE_TYPE")]
        public byte? large_type { get; set; }
        /// <summary> 
        /// 发票类别，0 不需要，1普通发票，2增值税发票 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICE_TYPE")]
        public byte? invoice_type { get; set; }
        /// <summary> 
        /// 发票内容 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICE_CONTENT")]
        public string invoice_content { get; set; }
        /// <summary> 
        /// 订单内部来源1手机，2聚划算，4服务子订单，8家装，16二次付款，32开具电子发票，128指定批次，2048当日达，4096次日达，8192预计时效，262144天猫直送/唯品仓中仓，524288‘3PL时效/jitx’，2097152区域零售，4194304预售单，8388608周期购 
        /// </summary> 
        /// <returns></returns> 
        [Column("FROM_MASK")]
        public int? from_mask { get; set; }
        /// <summary> 
        /// 类目id 
        /// </summary> 
        /// <returns></returns> 
        [Column("CID")]
        public int? cid { get; set; }
        /// <summary> 
        /// 货品明细备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK")]
        public string remark { get; set; }
        /// <summary> 
        /// 最后修改时间   时间格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFIED")]
        public DateTime? modified { get; set; }
        /// <summary> 
        /// 创建时间   时间格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATED")]
        public DateTime? created { get; set; }
        /// <summary> 
        /// 税率（根据条件使用订单中或单品中税率） 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAX_RATE")]
        public decimal? tax_rate { get; set; }
        /// <summary> 
        /// 基本单位ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("BASE_UNIT_ID")]
        public int? base_unit_id { get; set; }
        /// <summary> 
        /// 基本单位名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("UNIT_NAME")]
        public string unit_name { get; set; }
        /// <summary> 
        /// 交易流水单号 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAY_ID")]
        public string pay_id { get; set; }
        /// <summary> 
        /// 0(未付款),1(部分付款),2(已付款)如需响应该字段，请将ERP升级到V2.3.8.3及以上。 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAY_STATUS")]
        public byte? pay_status { get; set; }
        /// <summary> 
        /// 时间格式：yyyy-MM-dd HH:mm:ss，如需响应该字段，请将ERP升级到V2.3.8.3及以上。 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAY_TIME")]
        public string pay_time { get; set; }
        #endregion

    }
}
