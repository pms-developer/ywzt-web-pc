﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtBilling
    {
        public string shop_name { get; set; }
        public string shop_no { get; set; }
        public int rec_id { get; set; }

        public string platform_id { get; set; }
        public string shop_id { get; set; }
        public string pay_order_no { get; set; }
        public string row_order_no { get; set; }
        public string order_no { get; set; }
        public string item_id { get; set; }
        public string sub_item_id { get; set; }

        public string item_name { get; set; }
        public string in_amount { get; set; }
        public string out_amount { get; set; }
        public string balance { get; set; }
        public string create_time { get; set; }
        public string created { get; set; }
        public string remark { get; set; }

        public string voucher_id { get; set; }
        public string post_status { get; set; }      
    }
}
