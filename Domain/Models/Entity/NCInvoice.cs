﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Entity
{
    public class NcSaleInvoice
    { 
        public NCInvoiceHeader BillHead { get; set; }

        public List<NCInvoiceDetail> CsaleInvoicebId { get; set; }
    }

    public class NCInvoiceHeader
    {
        public string pk_group { get; set; }
        public string pk_org { get; set; }
        public string pk_org_v { get; set; }
       
        public string vbillcode { get; set; }
        public string cbiztypeid { get; set; }
        public string ctrantypeid { get; set; }
        public string vtrantypecode { get; set; }
        public string dbilldate { get; set; }
        public string cinvoicecustid { get; set; }
        public string vprintcustname { get; set; }
        public string ccustbankid { get; set; }
        public string ccustbankaccid { get; set; }
        public string cpaytermid { get; set; }
        public string vcreditnum { get; set; }
        public string vgoldtaxcode { get; set; }
        public string btogoldtaxflag { get; set; }
        public string tgoldtaxtime { get; set; }
        public string corigcurrencyid { get; set; }
        public string nexchangerate { get; set; }
        public string ccurrencyid { get; set; }
        public string ngroupexchgrate { get; set; }
        public string nglobalexchgrate { get; set; }
        public string nhvoicedisrate { get; set; }
        public string ntotalastnum { get; set; }
        public string ntotalorigsubmny { get; set; }
        public string ntotalorigmny { get; set; }
        public string csendcountryid { get; set; }
        public string crececountryid { get; set; }
        public string ctaxcountryid { get; set; }
        public string fbuysellflag { get; set; }
        public string btriatradeflag { get; set; }
        public string vvatcode { get; set; }
        public string vcustvatcode { get; set; }
        public string ctradewordid { get; set; }
        public string bsubunitflag { get; set; }
        public string fopposeflag { get; set; }
        public string vopposesrccode { get; set; }
        public string vnote { get; set; }
        public string fstatusflag { get; set; }
        public string creator { get; set; }
        public string billmaker { get; set; }
        public string dmakedate { get; set; }
        public string creationtime { get; set; }
        public string modifier { get; set; }
        public string modifiedtime { get; set; }
        public string approver { get; set; }
        public string taudittime { get; set; }

    }

    public class NCInvoiceDetail
    {
        public string pk_group { get; set; }
        public string pk_org { get; set; }
        public string dbilldate { get; set; }
        public string crowno { get; set; }
        public string castunitid { get; set; }
        public string cmaterialid { get; set; }
        public string cmaterialvid { get; set; }
        public string cvendorid { get; set; }
        public string cprojectid { get; set; }
        public string cproductorid { get; set; }
        public string nastnum { get; set; }
        public string cunitid { get; set; }
        public string nnum { get; set; }
        public string vchangerate { get; set; }
        public string cqtunitid { get; set; }
        public string vqtunitrate { get; set; }
        public string nqtunitnum { get; set; }

        public string bdiscountflag { get; set; }
        public string blaborflag { get; set; }
        public string blargessflag { get; set; }
        public string pk_batchcode { get; set; }
        public string vbatchcode { get; set; }
        public string ctaxcodeid { get; set; }
        public string ntaxrate { get; set; }
        public string ftaxtypeflag { get; set; }
        public string ndiscountrate { get; set; }
        public string nitemdiscountrate { get; set; }
        public string ninvoicedisrate { get; set; }
        public string norigtaxprice { get; set; }
        public string norigprice { get; set; }
        public string norigtaxnetprice { get; set; }
        public string norignetprice { get; set; }
        public string nqtorigtaxprice { get; set; }
        public string nqtorigprice { get; set; }
        public string nqtorigtaxnetprc { get; set; }
        public string nqtorignetprice { get; set; }
        public string ntax { get; set; }
        public string ncaltaxmny { get; set; }
        public string norigmny { get; set; }
        public string norigtaxmny { get; set; }
        public string norigdiscount { get; set; }
        public string ntaxprice { get; set; }
        public string nprice { get; set; }
        public string ntaxnetprice { get; set; }
        public string nnetprice { get; set; }
        public string nqttaxprice { get; set; }
        public string nqtprice { get; set; }
        public string nqttaxnetprice { get; set; }
        public string nqtnetprice { get; set; }
        public string nmny { get; set; }
        public string ntaxmny { get; set; }

        public string ndiscount { get; set; }
        public string norigsubmny { get; set; }
        public string ngroupmny { get; set; }
        public string ngrouptaxmny { get; set; }
        public string nglobalmny { get; set; }

        public string nglobaltaxmny { get; set; }
        public string vfirsttype { get; set; }
        public string vfirstcode { get; set; }
        public string vfirsttrantype { get; set; }
        public string vfirstrowno { get; set; }

        public string cfirstid { get; set; }
        public string cfirstbid { get; set; }
        public string vsrctype { get; set; }
        public string vsrccode { get; set; }
        public string vsrctrantype { get; set; }

        public string vsrcrowno { get; set; }
        public string csrcid { get; set; }
        public string csrcbid { get; set; }
        public string copposesrcbid { get; set; }
        public string csaleorgid { get; set; }

        public string csaleorgvid { get; set; }
        public string cprofitcenterid { get; set; }
        public string cprofitcentervid { get; set; }
        public string carorgid { get; set; }
        public string carorgvid { get; set; }
        public string cordercustid { get; set; }

        public string bfreecustflag { get; set; }
        public string cfreecustid { get; set; }
        public string cdeptid { get; set; }
        public string cdeptvid { get; set; }
        public string cemployeeid { get; set; }
        public string cchanneltypeid { get; set; }

        public string creceivecustid { get; set; }
        public string creceiveaddrid { get; set; }
        public string ctransporttypeid { get; set; }
        public string csendstockorgid { get; set; }
        public string csendstockorgvid { get; set; }

        public string csendstordocid { get; set; }
        public string cprodlineid { get; set; }

        public string ccostsubjid { get; set; }
        public string cctmanageid { get; set; }
        public string cvmivenderid { get; set; }
        public string vsumcode { get; set; }
        public string csumid { get; set; }

        public string nshouldoutnum { get; set; }
        public string ntotaloutnum { get; set; }
        public string ntotalincomenum { get; set; }
        public string ntotalincomemny { get; set; }
        public string ntotalcostnum { get; set; }
        public string ntotalpaymny { get; set; }
        public string vrownote { get; set; }
    }
}
