﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtPlatformGoods
    {
        #region  实体成员 
        
        [Column("REC_ID")]
        public int? rec_id { get; set; }
        /// <summary> 
        /// 代表店铺所有属性的唯一编码，用于店铺区分，ERP内支持自定义 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_NO")]
        public string shop_no { get; set; }
        /// <summary> 
        /// ERP内支持自定义的店铺名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_NAME")]
        public string shop_name { get; set; }
        /// <summary> 
        /// 响应值为代表平台的iD数值，数值对应的平台名称单击这里 
        /// </summary> 
        /// <returns></returns> 
        [Column("PLATFORM_ID")]
        public byte? platform_id { get; set; }
        /// <summary> 
        /// 平台货品id 
        /// </summary> 
        /// <returns></returns> 
        [Column("API_GOODS_ID")]
        public string api_goods_id { get; set; }
        /// <summary> 
        /// 平台规格id 
        /// </summary> 
        /// <returns></returns> 
        [Column("API_SPEC_ID")]
        public string api_spec_id { get; set; }
        /// <summary> 
        /// 平台货品名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("API_GOODS_NAME")]
        public string api_goods_name { get; set; }
        /// <summary> 
        /// 平台规格名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("API_SPEC_NAME")]
        public string api_spec_name { get; set; }
        /// <summary> 
        /// 表主键对应数据最后更新时间，时间格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFIED")]
        public DateTime? modified { get; set; }
        /// <summary> 
        /// 平台货品编码，举例：淘宝商家后台一级商家编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("OUTER_ID")]
        public string outer_id { get; set; }
        /// <summary> 
        /// 平台规格编码，举例：淘宝商家后台二级商家编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPEC_OUTER_ID")]
        public string spec_outer_id { get; set; }
        /// <summary> 
        /// 平台单品（sku）库存 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_NUM")]
        public decimal? stock_num { get; set; }
        /// <summary> 
        /// 平台零售价 
        /// </summary> 
        /// <returns></returns> 
        [Column("PRICE")]
        public decimal? price { get; set; }
        /// <summary> 
        /// 平台类目 
        /// </summary> 
        /// <returns></returns> 
        [Column("CID")]
        public string cid { get; set; }
        /// <summary> 
        /// 图片链接 
        /// </summary> 
        /// <returns></returns> 
        [Column("PIC_URL")]
        public string pic_url { get; set; }
        /// <summary> 
        /// 店铺列表主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_ID")]
        public int? shop_id { get; set; }
        /// <summary> 
        /// 平台规格编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPEC_CODE")]
        public string spec_code { get; set; }
        /// <summary> 
        /// 平台sku属性串,如 1627207:3232483;1630696:3284570 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPEC_SKU_PROPERTIES")]
        public string spec_sku_properties { get; set; }
        /// <summary> 
        /// 平台上的条码,多个条码用逗号分隔 
        /// </summary> 
        /// <returns></returns> 
        [Column("BARCODE")]
        public string barcode { get; set; }
        /// <summary> 
        /// 如果是根据商家编码自动匹配的，那么这个字段记录了商家编码，可以是货品的编码+规格的编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("MATCH_CODE")]
        public string match_code { get; set; }
        /// <summary> 
        /// 品牌ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("BRAND_ID")]
        public string brand_id { get; set; }
        /// <summary> 
        /// outder_id,outer_spec_id计算出来的 
        /// </summary> 
        /// <returns></returns> 
        [Column("OUTER_CODE ")]
        public string outer_code { get; set; }
        /// <summary> 
        /// 创建时间   时间格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATED")]
        public DateTime? created { get; set; }
        /// <summary> 
        /// 0  自动匹配 1 手动匹配 2 绑定到菜鸟联盟 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_MANUAL_MATCH")]
        public byte? is_manual_match { get; set; }
        /// <summary> 
        /// 分类 
        /// </summary> 
        /// <returns></returns> 
        [Column("CLASS_ID_PATH")]
        public string class_id_path { get; set; }
        /// <summary> 
        /// 在此映射记录上起作用的同步规则策略id,0表示手工设置的个性化策略,-1表示没有任何匹配上任何库存同步规则 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_SYN_RULE_ID")]
        public string stock_syn_rule_id { get; set; }
        /// <summary> 
        /// 在此映射记录上起作用的同步规则策略编号 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_SYN_RULE_NO")]
        public string stock_syn_rule_no { get; set; }
        /// <summary> 
        /// 库存计算方法的掩码 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_SYN_MASK")]
        public string stock_syn_mask { get; set; }
        /// <summary> 
        /// 同步百分比 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_SYN_PERCENT")]
        public string stock_syn_percent { get; set; }
        /// <summary> 
        /// 百分比附加值 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_SYN_PLUS")]
        public decimal? stock_syn_plus { get; set; }
        /// <summary> 
        /// 最小同步库存量 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_SYN_MIN")]
        public decimal? stock_syn_min { get; set; }
        /// <summary> 
        /// 停止库存同步 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_DISABLE_SYN")]
        public byte? is_disable_syn { get; set; }
        /// <summary> 
        /// 后台信息同步库存，直接到达这个时间(unix timestamp) 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISABLE_SYN_UNTIL")]
        public string disable_syn_until { get; set; }
        /// <summary> 
        /// 停止同步原因 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISABLED_REASON")]
        public string disabled_reason { get; set; }
        /// <summary> 
        /// 最后同步库存量 
        /// </summary> 
        /// <returns></returns> 
        [Column("LAST_SYN_NUM")]
        public decimal? last_syn_num { get; set; }
        /// <summary> 
        /// 最后一次库存同步后，库存有没发生变化 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_STOCK_CHANGED")]
        public byte? is_stock_changed { get; set; }
        /// <summary> 
        /// 库存变化时自增 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_CHANGE_COUNT")]
        public string stock_change_count { get; set; }
        /// <summary> 
        /// 是否刚刚更新过此记录 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_UPDATE")]
        public byte? is_update { get; set; }
        /// <summary> 
        /// 修改标记,商家编码变化，值为1 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFY_FLAG")]
        public string modify_flag { get; set; }
        /// <summary> 
        /// 手动修改的标记 
        /// </summary> 
        /// <returns></returns> 
        [Column("FLAG_ID")]
        public string flag_id { get; set; }
        /// <summary> 
        /// 掩码,1临时延时 2组合装同步多仓库总库存计算的虚拟值 4多仓库存同步 
        /// </summary> 
        /// <returns></returns> 
        [Column("MASK ")]
        public string mask { get; set; }
        /// <summary> 
        /// 最大同步库存 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCK_SYN_MAX")]
        public string stock_syn_max { get; set; }
        /// <summary> 
        /// 阿里巴巴区别1688|lst 
        /// </summary> 
        /// <returns></returns> 
        [Column("RESERVE_S ")]
        public string reserve_s { get; set; }
        /// <summary> 
        /// 是否已经删除,status的一个备份 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_DELETED")]
        public byte? is_deleted { get; set; }
        /// <summary> 
        /// 按照平台扣减库存方式，占用的库存量 
        /// </summary> 
        /// <returns></returns> 
        [Column("HOLD_STOCK")]
        public decimal? hold_stock { get; set; }
        /// <summary> 
        /// 平台扣减库存方式：1拍下减库存 ，2付款减库存 
        /// </summary> 
        /// <returns></returns> 
        [Column("HOLD_STOCK_TYPE")]
        public byte? hold_stock_type { get; set; }
        /// <summary> 
        /// 是否自动上架，返回值0否，1是 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_AUTO_LISTING")]
        public byte? is_auto_listing { get; set; }
        /// <summary> 
        /// 是否自动上架，返回值0否，1是 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_AUTO_DELISTING")]
        public byte? is_auto_delisting { get; set; }
        /// <summary> 
        /// 平台货品状态：0删除 1在架 2下架, 
        /// </summary> 
        /// <returns></returns> 
        [Column("STATUS")]
        public byte? status { get; set; }
        /// <summary> 
        /// 返回值：0未匹配， 1匹配单品商家编码，2匹配组合装商家编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("MATCH_TARGET_TYPE")]
        public byte? match_target_type { get; set; }
        /// <summary> 
        /// match_target_type=1时返回的为单品商家编码、match_target_type=2时返回的为组合装商家编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("MERCHANT_NO")]
        public string merchant_no { get; set; }
        /// <summary> 
        /// match_target_type=1时返回的为单品商家编码id、match_target_type=2时返回的为组合装商家编码id 
        /// </summary> 
        /// <returns></returns> 
        [Column("MATCH_TARGET_ID")]
        public string match_target_id { get; set; }
        /// <summary> 
        /// match_target_type=1时返回的为单品货品名称、match_target_type=2时返回的为组合装货品名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("MERCHANT_NAME")]
        public string merchant_name { get; set; }
        /// <summary> 
        /// match_target_type=1时返回的为单品规格名称、match_target_type=2时返回为空 
        /// </summary> 
        /// <returns></returns> 
        [Column("MERCHANT_CODE")]
        public string merchant_code { get; set; }
        /// <summary> 
        /// 最后同步时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("LAST_SYN_TIME")]
        public string last_syn_time { get; set; }
      
        /// <summary> 
        /// 定时下架时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("DELIST_TIME")]
        public string delist_time { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(int? keyValue)
        {
            this.rec_id = keyValue;
        }
        #endregion
    }
}
