﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtPurchaseProvider
    {
        #region  实体成员 
        /// <summary> 
        /// 主键 
        /// </summary> 
        /// <returns></returns> 
      
        [Column("PROVIDER_ID")]
        public int? provider_id { get; set; }
        /// <summary> 
        /// 代表供应商所有属性的唯一编码，用于供应商区分，ERP内支持自定义（ERP供应商界面设置），用于获取指定供应商数据信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROVIDER_NO")]
        public string provider_no { get; set; }
        /// <summary> 
        /// 供应商名称（ERP内自定义的名称） 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROVIDER_NAME")]
        public string provider_name { get; set; }
        /// <summary> 
        /// ERP中编辑的联系人 
        /// </summary> 
        /// <returns></returns> 
        [Column("CONTACT")]
        public string contact { get; set; }
        /// <summary> 
        /// 供应商设置中座机信息 
        /// </summary> 
        /// <returns></returns> 
        [Column("TELNO")]
        public string telno { get; set; }
        /// <summary> 
        /// 供应商设置中移动电话 
        /// </summary> 
        /// <returns></returns> 
        [Column("MOBILE")]
        public string mobile { get; set; }
        /// <summary> 
        /// 供应商设置中传真 
        /// </summary> 
        /// <returns></returns> 
        [Column("FAX")]
        public string fax { get; set; }
        /// <summary> 
        /// 供应商设置中邮编 
        /// </summary> 
        /// <returns></returns> 
        [Column("ZIP")]
        public string zip { get; set; }
        /// <summary> 
        /// 供应商设置中邮件 
        /// </summary> 
        /// <returns></returns> 
        [Column("EMAIL")]
        public string email { get; set; }
        /// <summary> 
        /// 供应商设置中QQ  
        /// </summary> 
        /// <returns></returns> 
        [Column("QQ")]
        public string qq { get; set; }
        /// <summary> 
        /// 供应商设置中旺旺账号 
        /// </summary> 
        /// <returns></returns> 
        [Column("WANGWANG")]
        public string wangwang { get; set; }
        /// <summary> 
        /// 收款银行账户 
        /// </summary> 
        /// <returns></returns> 
        [Column("ACCOUNT_BANK_NO ")]
        public string account_bank_no { get; set; }
        /// <summary> 
        /// 收款银行 
        /// </summary> 
        /// <returns></returns> 
        [Column("ACCOUNT_BANK")]
        public string account_bank { get; set; }
        /// <summary> 
        /// 收款人 
        /// </summary> 
        /// <returns></returns> 
        [Column("COLLECT_NAME")]
        public string collect_name { get; set; }
        /// <summary> 
        /// 国家编码，默认为0 
        /// </summary> 
        /// <returns></returns> 
        [Column("COUNTRY")]
        public int? country { get; set; }
        /// <summary> 
        /// 省份编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROVINCE")]
        public int? province { get; set; }
        /// <summary> 
        /// 城市编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("CITY")]
        public int? city { get; set; }
        /// <summary> 
        /// 区县编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISTRICT")]
        public int? district { get; set; }
        /// <summary> 
        /// 供应商设置中地址 
        /// </summary> 
        /// <returns></returns> 
        [Column("ADDRESS")]
        public string address { get; set; }
        /// <summary> 
        /// 最小采购量 
        /// </summary> 
        /// <returns></returns> 
        [Column("MIN_PURCHASE_NUM")]
        public decimal? min_purchase_num { get; set; }
        /// <summary> 
        /// 供应商设置中网址 
        /// </summary> 
        /// <returns></returns> 
        [Column("WEBSITE")]
        public string website { get; set; }
        /// <summary> 
        /// 采购周期 
        /// </summary> 
        /// <returns></returns> 
        [Column("PURCHASE_CYCLE_DAYS")]
        public int? purchase_cycle_days { get; set; }
        /// <summary> 
        /// 分组 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROVIDER_GROUP")]
        public string provider_group { get; set; }
        /// <summary> 
        /// 到货周期 
        /// </summary> 
        /// <returns></returns> 
        [Column("ARRIVE_CYCLE_DAYS")]
        public int? arrive_cycle_days { get; set; }
        /// <summary> 
        /// 结算周期 
        /// </summary> 
        /// <returns></returns> 
        [Column("CHARGE_CYCLE_DAYS")]
        public int? charge_cycle_days { get; set; }
        
        /// <summary> 
        /// 供应商设置中备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK")]
        public string remark { get; set; }
        /// <summary> 
        /// null否禁用:0.否;1.null 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_DISABLED")]
        public byte? is_disabled { get; set; }
        /// <summary> 
        /// 1970年至删除时间的时长 
        /// </summary> 
        /// <returns></returns> 
        [Column("DELETED")]
        public int? deleted { get; set; }
        /// <summary> 
        /// 最后修改时间格式 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFIED")]
        public DateTime? modified { get; set; }
        /// <summary> 
        /// created 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATED")]
        public DateTime? created { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(int? keyValue)
        {
            this.provider_id = keyValue;
        }
        #endregion
    }
}
