﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Entity
{
    public class NcSpplier
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string Pk_Supplierclass { get; set; }
        public string Pk_Supplier { get; set; }
    }
}
