﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtOrder
    {
        #region  实体成员 
        /// <summary> 
        /// erp订单表的主键 
        /// </summary> 
        /// <returns></returns> 
      
        [Column("TRADE_ID")]
        public int trade_id { get; set; }
        /// <summary> 
        /// 系统订单编号，默认单号为JY开头，ERP内支持自定义订单编号生成规则（设置路径：设置——编码设置） 
        /// </summary> 
        /// <returns></returns> 
        [Column("TRADE_NO")]
        public string trade_no { get; set; }
        /// <summary> 
        /// 响应值为代表平台的ID数字，ID对应的平台名称单击这里 
        /// </summary> 
        /// <returns></returns> 
        [Column("PLATFORM_ID")]
        public byte? platform_id { get; set; }
        /// <summary> 
        /// 响应值为代表平台的ID数字，ID对应的平台名称单击这里 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_PLATFORM_ID")]
        public byte? shop_platform_id { get; set; }
        /// <summary> 
        /// 代表店铺所有属性的唯一编码，用于店铺区分，ERP内支持自定义（ERP店铺界面设置） 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_NO")]
        public string shop_no { get; set; }
        /// <summary> 
        /// 店铺名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_NAME")]
        public string shop_name { get; set; }
        /// <summary> 
        /// 店铺备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_REMARK")]
        public string shop_remark { get; set; }
        /// <summary> 
        /// 0不限 1普通仓库 2自动流转外部 3京东仓储 4科捷 5百世物流 6 SKU360 7通天晓 8中联网仓 9顺丰仓储 10网仓2号 11奇门仓储 12旺店通仓储 13心怡仓储 14力威仓储 15京东沧海 16云集仓储  20外部链路型仓库 127其他 
        /// </summary> 
        /// <returns></returns> 
        [Column("WAREHOUSE_TYPE")]
        public byte? warehouse_type { get; set; }
        /// <summary> 
        /// 代表仓库所有属性的唯一编码，用于仓库区分，ERP内支持自定义（ERP仓库界面设置）（根据编号可查询仓库名称） 
        /// </summary> 
        /// <returns></returns> 
        [Column("WAREHOUSE_NO")]
        public string warehouse_no { get; set; }
        /// <summary> 
        /// 商城或电商平台的订单编号,合并订单的多个订单编号有逗号隔开 
        /// </summary> 
        /// <returns></returns> 
        [Column("SRC_TIDS")]
        public string src_tids { get; set; }
        /// <summary> 
        /// 订单状态 5已取消 10待付款 12待尾款 13待选仓 15等未付16延时审核 19预订单前处理 20前处理(赠品，合并，拆分)21委外前处理22抢单前处理 25预订单 27待抢单 30待客审 35待财审 40待递交仓库 45递交仓库中 50已递交仓库 53未确认 55已确认（已审核） 95已发货 105部分打款 110已完成 113异常发货 
        /// </summary> 
        /// <returns></returns> 
        [Column("TRADE_STATUS")]
        public byte? trade_status { get; set; }
        /// <summary> 
        /// 出库状态：0表示无出库状态，1验货2称重4出库8物流同步16分拣（注意：如果是3，则表示完成了验货和称重“1+2”，如果是15，则表示四个过程都完成了“1+2+4+8”，其他数字以此类推） 
        /// </summary> 
        /// <returns></returns> 
        [Column("CONSIGN_STATUS")]
        public int? consign_status { get; set; }
        /// <summary> 
        /// 1网店销售 2线下零售 3售后换货 4批发业务 5保修换新 6保修完成 7订单补发 .. 101自定义类型1 102自定义类型2 103自定义类型3 104自定义类型4 105自定义类型5 106自定义类型6 107自定义属性7 108自定义属性8 
        /// </summary> 
        /// <returns></returns> 
        [Column("TRADE_TYPE")]
        public byte? trade_type { get; set; }
        /// <summary> 
        /// 发货条件 1款到发货 2货到付款(包含部分货到付款)，4挂账 
        /// </summary> 
        /// <returns></returns> 
        [Column("DELIVERY_TERM")]
        public byte? delivery_term { get; set; }
        /// <summary> 
        /// 冻结原因 
        /// </summary> 
        /// <returns></returns> 
        [Column("FREEZE_REASON")]
        public int? freeze_reason { get; set; }
        /// <summary> 
        /// 退款状态 0无退款 1申请退款 2部分退款 3全部退款 4未付款关闭或手工关闭  
        /// </summary> 
        /// <returns></returns> 
        [Column("REFUND_STATUS")]
        public byte? refund_status { get; set; }
        /// <summary> 
        /// 0非分销订单 1转供销 2代销 3经销 
        /// </summary> 
        /// <returns></returns> 
        [Column("FENXIAO_TYPE")]
        public byte? fenxiao_type { get; set; }
        /// <summary> 
        /// 分销商名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("FENXIAO_NICK")]
        public string fenxiao_nick { get; set; }
        /// <summary> 
        /// 下单时间  时间格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("TRADE_TIME")]
        public string trade_time { get; set; }
        /// <summary> 
        /// 付款时间  时间格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAY_TIME")]
        public string pay_time { get; set; }
        /// <summary> 
        /// 客户名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("CUSTOMER_NAME")]
        public string customer_name { get; set; }
        /// <summary> 
        /// 客户编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("CUSTOMER_NO")]
        public string customer_no { get; set; }
        /// <summary> 
        /// 买家付款账号 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAY_ACCOUNT")]
        public string pay_account { get; set; }
        /// <summary> 
        /// 客户网名 
        /// </summary> 
        /// <returns></returns> 
        [Column("BUYER_NICK")]
        public string buyer_nick { get; set; }
        /// <summary> 
        /// 收件人 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_NAME")]
        public string receiver_name { get; set; }
        /// <summary> 
        /// 收件人的省份 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_PROVINCE")]
        public int? receiver_province { get; set; }
        /// <summary> 
        /// 收件人的城市 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_CITY")]
        public int? receiver_city { get; set; }
        /// <summary> 
        /// 收件人的地区 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_DISTRICT")]
        public int? receiver_district { get; set; }
        /// <summary> 
        /// 地址 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_ADDRESS")]
        public string receiver_address { get; set; }
        /// <summary> 
        /// 手机 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_MOBILE")]
        public string receiver_mobile { get; set; }
        /// <summary> 
        /// 电话 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_TELNO")]
        public string receiver_telno { get; set; }
        /// <summary> 
        /// 收件人邮编 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_ZIP")]
        public string receiver_zip { get; set; }
        /// <summary> 
        /// 省市县空格分隔 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_AREA")]
        public string receiver_area { get; set; }
        /// <summary> 
        /// 收件人区域 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_RING")]
        public string receiver_ring { get; set; }
        /// <summary> 
        /// 大头笔 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_DTB")]
        public string receiver_dtb { get; set; }
        
        /// <summary> 
        /// 异常订单(bit位)，1无库存记录 2地址发生变化 4发票变化 8仓库变化 16备注变化 32平台更换货品 64退款 
        /// </summary> 
        /// <returns></returns> 
        [Column("BAD_REASON")]
        public int? bad_reason { get; set; }
        /// <summary> 
        /// 物流公司ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("LOGISTICS_ID")]
        public int? logistics_id { get; set; }
        /// <summary> 
        /// 物流公司名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("LOGISTICS_NAME")]
        public string logistics_name { get; set; }
        /// <summary> 
        /// 代表物流所有属性的唯一编码，用于物流区分，ERP内支持自定义（ERP物流界面设置） 
        /// </summary> 
        /// <returns></returns> 
        [Column("LOGISTICS_CODE")]
        public string logistics_code { get; set; }
        /// <summary> 
        /// 响应值为代表物流方式的数字，数字对应的物流方式名称单击这里 
        /// </summary> 
        /// <returns></returns> 
        [Column("LOGISTICS_TYPE")]
        public int? logistics_type { get; set; }
        /// <summary> 
        /// 物流单号 
        /// </summary> 
        /// <returns></returns> 
        [Column("LOGISTICS_NO")]
        public string logistics_no { get; set; }
        /// <summary> 
        /// 店铺列表主键 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHOP_ID")]
        public int? shop_id { get; set; }
        /// <summary> 
        /// 仓库ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("WAREHOUSE_ID")]
        public int? warehouse_id { get; set; }
        /// <summary> 
        /// 用于多级审核,特殊值：-100根据预售策略自动转入特殊单，-101人工转入 
        /// </summary> 
        /// <returns></returns> 
        [Column("CHECK_STEP")]
        public int? check_step { get; set; }
        /// <summary> 
        /// 未合并标记,1有未付款订单,2有同名未合并订单 
        /// </summary> 
        /// <returns></returns> 
        [Column("UNMERGE_MASK")]
        public byte? unmerge_mask { get; set; }
        /// <summary> 
        /// 延时此进一步处理，等未付或延时审核 激活时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("DELAY_TO_TIME")]
        public string delay_to_time { get; set; }
        /// <summary> 
        /// 0普通客户1经销商 
        /// </summary> 
        /// <returns></returns> 
        [Column("CUSTOMER_TYPE ")]
        public byte? customer_type { get; set; }
        /// <summary> 
        /// 买家ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("CUSTOMER_ID ")]
        public string customer_id { get; set; }
        /// <summary> 
        /// 收件人国家 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVER_COUNTRY ")]
        public int? receiver_country { get; set; }
        /// <summary> 
        /// 配送中心,未使用 
        /// </summary> 
        /// <returns></returns> 
        [Column("PRE_CHARGE_TIME ")]
        public string pre_charge_time { get; set; }
        /// <summary> 
        /// 是否京配(为1时,只能发京邦达 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_PREV_NOTIFY")]
        public byte? is_prev_notify { get; set; }
        /// <summary> 
        /// 便签条数 
        /// </summary> 
        /// <returns></returns> 
        [Column("NOTE_COUNT")]
        public int? note_count { get; set; }
        /// <summary> 
        /// 买家留言条数 
        /// </summary> 
        /// <returns></returns> 
        [Column("BUYER_MESSAGE_COUNT")]
        public byte? buyer_message_count { get; set; }
        /// <summary> 
        /// 客服备注条数 
        /// </summary> 
        /// <returns></returns> 
        [Column("CS_REMARK_COUNT")]
        public byte? cs_remark_count { get; set; }
        /// <summary> 
        /// 客服备注变化  0,未变化1平台变化,2手工修改,4发票手工修改 
        /// </summary> 
        /// <returns></returns> 
        [Column("CS_REMARK_CHANGE_COUNT")]
        public int? cs_remark_change_count { get; set; }
        /// <summary> 
        /// 优惠变化金额,更新货品和数量 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISCOUNT_CHANGE")]
        public decimal? discount_change { get; set; }
        /// <summary> 
        /// 客户使用的预存款 
        /// </summary> 
        /// <returns></returns> 
        [Column("TRADE_PREPAY ")]
        public decimal? trade_prepay { get; set; }
        /// <summary> 
        /// 分期付款金额 
        /// </summary> 
        /// <returns></returns> 
        [Column("PI_AMOUNT")]
        public decimal? pi_amount { get; set; }
        /// <summary> 
        /// 其它成本(目前用作记录货到付款单据的物流佣金 
        /// </summary> 
        /// <returns></returns> 
        [Column("OTHER_COST")]
        public decimal? other_cost { get; set; }
        /// <summary> 
        /// 体积 
        /// </summary> 
        /// <returns></returns> 
        [Column("VOLUME")]
        public decimal? volume { get; set; }
        /// <summary> 
        /// 销售积分,未使用 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALES_SCORE")]
        public string sales_score { get; set; }
        /// <summary> 
        /// 背景色标记 
        /// </summary> 
        /// <returns></returns> 
        [Column("FLAG_ID")]
        public int? flag_id { get; set; }
        /// <summary> 
        /// 不可合并拆分 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_SEALED ")]
        public byte? is_sealed { get; set; }
        /// <summary> 
        /// 赠品标记1已处理过赠品,但没有匹配任何策略2自动赠送4手工赠送6即有自动也有手工 
        /// </summary> 
        /// <returns></returns> 
        [Column("GIFT_MASK ")]
        public byte? gift_mask { get; set; }
        /// <summary> 
        /// 拆分订单，原单ID，用于避免自动合并,大件拆分为（原订单的id值），自动拆分为负值（原订单的-id值） 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPLIT_FROM_TRADE_ID")]
        public string split_from_trade_id { get; set; }
        /// <summary> 
        /// 未使用 
        /// </summary> 
        /// <returns></returns> 
        [Column("LOGISTICS_TEMPLATE_ID ")]
        public string logistics_template_id { get; set; }
        /// <summary> 
        /// 未使用 
        /// </summary> 
        /// <returns></returns> 
        [Column("SENDBILL_TEMPLATE_ID")]
        public string sendbill_template_id { get; set; }
        /// <summary> 
        /// 驳回原因 
        /// </summary> 
        /// <returns></returns> 
        [Column("REVERT_REASON ")]
        public int? revert_reason { get; set; }
        /// <summary> 
        /// 取消原因 
        /// </summary> 
        /// <returns></returns> 
        [Column("CANCEL_REASON")]
        public int? cancel_reason { get; set; }
        /// <summary> 
        /// 催未付款订单消息发送标记 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_UNPAYMENT_SMS ")]
        public byte? is_unpayment_sms { get; set; }
        /// <summary> 
        /// 包装id 
        /// </summary> 
        /// <returns></returns> 
        [Column("PACKAGE_ID")]
        public string package_id { get; set; }
        /// <summary> 
        /// 订单标记位 1使用智选物流 2 航空禁运 4 预订单自动转审核失败 8 预占用待发货库存 16 订单货品指定批次 32 自动流转仓库 64 部分发货 128 全部发货 256 已发过签收消息 512 大单锁定仓库 1024 人工转入预订单 2048因配置先占用待发货库存 4096 顺丰前置发货 8192订单批量合并后标记  33554432前N有礼订单  67108864预售下沉 
        /// </summary> 
        /// <returns></returns> 
        [Column("TRADE_MASK")]
        public string trade_mask { get; set; }
        /// <summary> 
        /// 保留 
        /// </summary> 
        /// <returns></returns> 
        [Column("RESERVE ")]
        public string reserve { get; set; }
        /// <summary> 
        /// 包含大件类型，1普通套件2独立套件3分组单发,未使用-1非单发件 取子单中的最大值 
        /// </summary> 
        /// <returns></returns> 
        [Column("LARGE_TYPE")]
        public byte? large_type { get; set; }
        /// <summary> 
        /// 买家留言 
        /// </summary> 
        /// <returns></returns> 
        [Column("BUYER_MESSAGE")]
        public string buyer_message { get; set; }
        /// <summary> 
        /// 客服备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("CS_REMARK")]
        public string cs_remark { get; set; }
        /// <summary> 
        /// 标旗 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK_FLAG")]
        public byte? remark_flag { get; set; }
        /// <summary> 
        /// 打印备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("PRINT_REMARK")]
        public string print_remark { get; set; }
        /// <summary> 
        /// 货品种类数 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_TYPE_COUNT")]
        public int? goods_type_count { get; set; }
        /// <summary> 
        /// 货品总数 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_COUNT")]
        public decimal? goods_count { get; set; }
        /// <summary> 
        /// 货品总额（未扣除优惠），sum（share_amount+discount）所得 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_AMOUNT")]
        public decimal? goods_amount { get; set; }
        /// <summary> 
        /// 邮费 
        /// </summary> 
        /// <returns></returns> 
        [Column("POST_AMOUNT")]
        public decimal? post_amount { get; set; }
        /// <summary> 
        /// 其它从买家的收费（非订单支付金额以及服务费），从原始订单列表继承 
        /// </summary> 
        /// <returns></returns> 
        [Column("OTHER_AMOUNT")]
        public decimal? other_amount { get; set; }
        /// <summary> 
        /// 订单优惠，系统子订单“优惠”求合所得 
        /// </summary> 
        /// <returns></returns> 
        [Column("DISCOUNT")]
        public decimal? discount { get; set; }
        /// <summary> 
        /// 应收金额，系统订单的（“货品总额”+“邮资”-“折扣”）所得 
        /// </summary> 
        /// <returns></returns> 
        [Column("RECEIVABLE")]
        public decimal? receivable { get; set; }
        /// <summary> 
        /// 款到发货金额,paid>=dap_amount才可发货 
        /// </summary> 
        /// <returns></returns> 
        [Column("DAP_AMOUNT")]
        public decimal? dap_amount { get; set; }
        /// <summary> 
        /// 货到付款订单金额，系统子订单的（“分摊后总价”+“分摊邮费”-“已付”）再求和 
        /// </summary> 
        /// <returns></returns> 
        [Column("COD_AMOUNT")]
        public decimal? cod_amount { get; set; }
        /// <summary> 
        /// 货到付款非订单金额，从原始订单继承 
        /// </summary> 
        /// <returns></returns> 
        [Column("EXT_COD_FEE")]
        public decimal? ext_cod_fee { get; set; }
        /// <summary> 
        /// 货款预估成本 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_COST")]
        public decimal? goods_cost { get; set; }
        /// <summary> 
        /// 预估邮费成本 
        /// </summary> 
        /// <returns></returns> 
        [Column("POST_COST")]
        public decimal? post_cost { get; set; }
        /// <summary> 
        /// 已付金额，系统子订单“已付”求合所得 
        /// </summary> 
        /// <returns></returns> 
        [Column("PAID")]
        public decimal? paid { get; set; }
        /// <summary> 
        /// 预估重量 
        /// </summary> 
        /// <returns></returns> 
        [Column("WEIGHT")]
        public decimal? weight { get; set; }
        /// <summary> 
        /// 预估毛利 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROFIT")]
        public decimal? profit { get; set; }
        /// <summary> 
        /// 税额 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAX")]
        public decimal? tax { get; set; }
        /// <summary> 
        /// 税率 
        /// </summary> 
        /// <returns></returns> 
        [Column("TAX_RATE")]
        public decimal? tax_rate { get; set; }
        /// <summary> 
        /// 佣金 
        /// </summary> 
        /// <returns></returns> 
        [Column("COMMISSION")]
        public decimal? commission { get; set; }
        /// <summary> 
        /// 发票类别 0 不需要，1普通发票，2增值普通税发票，3增值专用税发票 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICE_TYPE")]
        public byte? invoice_type { get; set; }
        /// <summary> 
        /// 发票抬头 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICE_TITLE")]
        public string invoice_title { get; set; }
        /// <summary> 
        /// 发票内容 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICE_CONTENT")]
        public string invoice_content { get; set; }
        /// <summary> 
        /// 业务员ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALESMAN_ID")]
        public int? salesman_id { get; set; }
        /// <summary> 
        /// 审核员工ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("CHECKER_ID")]
        public int? checker_id { get; set; }
        /// <summary> 
        /// 业务员姓名 
        /// </summary> 
        /// <returns></returns> 
        [Column("FULLNAME")]
        public string fullname { get; set; }
        /// <summary> 
        /// 审核员工姓名 
        /// </summary> 
        /// <returns></returns> 
        [Column("CHECKER_NAME")]
        public string checker_name { get; set; }
        /// <summary> 
        /// 财审操作员ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("FCHECKER_ID")]
        public int? fchecker_id { get; set; }
        /// <summary> 
        /// 签出员工id 
        /// </summary> 
        /// <returns></returns> 
        [Column("CHECKOUTER_ID")]
        public int? checkouter_id { get; set; }
        /// <summary> 
        /// 出库单号,内部或外部仓库的订单号 
        /// </summary> 
        /// <returns></returns> 
        [Column("STOCKOUT_NO")]
        public string stockout_no { get; set; }
        /// <summary> 
        /// 背景色标记名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("FLAG_NAME")]
        public string flag_name { get; set; }
        /// <summary> 
        /// 订单来源 1API抓单，2手工建单 3excel导入 4现款销售 
        /// </summary> 
        /// <returns></returns> 
        [Column("TRADE_FROM")]
        public byte? trade_from { get; set; }
        /// <summary> 
        /// 货品商家编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("SINGLE_SPEC_NO")]
        public string single_spec_no { get; set; }
        /// <summary> 
        /// 原始货品数量 
        /// </summary> 
        /// <returns></returns> 
        [Column("RAW_GOODS_COUNT")]
        public decimal? raw_goods_count { get; set; }
        /// <summary> 
        /// 原始货品种类数 
        /// </summary> 
        /// <returns></returns> 
        [Column("RAW_GOODS_TYPE_COUNT")]
        public int? raw_goods_type_count { get; set; }
        /// <summary> 
        /// 币种 
        /// </summary> 
        /// <returns></returns> 
        [Column("CURRENCY")]
        public string currency { get; set; }
        /// <summary> 
        /// 已拆分包裹数 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPLIT_PACKAGE_NUM")]
        public int? split_package_num { get; set; }
        /// <summary> 
        /// 0表示未开发票，＞0表示已开发票 
        /// </summary> 
        /// <returns></returns> 
        [Column("INVOICE_ID")]
        public int? invoice_id { get; set; }
        /// <summary> 
        /// 订单每修改一次，版本号做一次变更 
        /// </summary> 
        /// <returns></returns> 
        [Column("VERSION_ID")]
        public int? version_id { get; set; }
        /// <summary> 
        /// 最后修改时间   时间格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFIED")]
        public DateTime? modified { get; set; }
        /// <summary> 
        /// 系统单生成时间   时间格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATED")]
        public DateTime? created { get; set; }
        /// <summary> 
        /// 证件类别 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID_CARD_TYPE")]
        public byte? id_card_type { get; set; }
        /// <summary> 
        /// 淘系平台不返回，其他平台正常返回 
        /// </summary> 
        /// <returns></returns> 
        [Column("ID_CARD")]
        public string id_card { get; set; }
        /// <summary> 
        /// 财审人名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("FCHECKER_NAME")]
        public string fchecker_name { get; set; }
        /// <summary> 
        /// 签出人名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("CHECKOUTER_NAME")]
        public string checkouter_name { get; set; }
        /// <summary> 
        /// 冻结原因名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("FREEZE_REASON_INFO")]
        public string freeze_reason_info { get; set; }


        public List<WdtOrderDetail> goods_list { get; set; }
        #endregion

    }
}
