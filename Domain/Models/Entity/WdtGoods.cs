﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtGoods
    {
        #region  实体成员 
        /// <summary> 
        /// 货品表主键 
        /// </summary> 
        /// <returns></returns> 
       
        [Column("GOODS_ID")]
        public int? goods_id { get; set; }
        /// <summary> 
        /// 代表SPU所有属性的唯一编号，用于系统货品的区分，SPU概念介绍，单击这里 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_NO")]
        public string goods_no { get; set; }
        /// <summary> 
        /// 货品名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_NAME")]
        public string goods_name { get; set; }
        /// <summary> 
        /// 简称 
        /// </summary> 
        /// <returns></returns> 
        [Column("SHORT_NAME")]
        public string short_name { get; set; }
        /// <summary> 
        /// 货品别名 
        /// </summary> 
        /// <returns></returns> 
        [Column("ALIAS")]
        public string alias { get; set; }
        /// <summary> 
        /// 货品类别  0:其它, 1:销售货品, 2:原材料, 3:包装物, 4:周转材料, 5:虚拟商品, 6:固定资产,7:保修配件 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_TYPE")]
        public byte? goods_type { get; set; }
        /// <summary> 
        /// 规格数 
        /// </summary> 
        /// <returns></returns> 
        [Column("SPEC_COUNT")]
        public int? spec_count { get; set; }
        /// <summary> 
        /// 拼音 
        /// </summary> 
        /// <returns></returns> 
        [Column("PINYIN")]
        public string pinyin { get; set; }
        /// <summary> 
        /// 品牌编号 
        /// </summary> 
        /// <returns></returns> 
        [Column("BRAND_NO")]
        public string brand_no { get; set; }
        /// <summary> 
        /// 品牌名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("BRAND_NAME")]
        public string brand_name { get; set; }
        /// <summary> 
        /// 备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK")]
        public string remark { get; set; }
        /// <summary> 
        /// 自定义属性1 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP1")]
        public string prop1 { get; set; }
        /// <summary> 
        /// 自定义属性2 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP2")]
        public string prop2 { get; set; }
        /// <summary> 
        /// 自定义属性3 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP3")]
        public string prop3 { get; set; }
        /// <summary> 
        /// 自定义属性4 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP4")]
        public string prop4 { get; set; }
        /// <summary> 
        /// 自定义属性5 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP5")]
        public string prop5 { get; set; }
        /// <summary> 
        /// 自定义属性6 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROP6")]
        public string prop6 { get; set; }
        /// <summary> 
        /// 产地 
        /// </summary> 
        /// <returns></returns> 
        [Column("ORIGIN")]
        public string origin { get; set; }
        /// <summary> 
        /// 分类id 
        /// </summary> 
        /// <returns></returns> 
        [Column("CLASS_ID")]
        public string class_id { get; set; }
        /// <summary> 
        /// 分类名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("CLASS_NAME")]
        public string class_name { get; set; }
        /// <summary> 
        /// 品牌ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("BRAND_ID")]
        public string brand_id { get; set; }
        /// <summary> 
        /// 基本单位id 
        /// </summary> 
        /// <returns></returns> 
        [Column("UNIT")]
        public string unit { get; set; }
        /// <summary> 
        /// 辅助单位id 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUX_UNIT")]
        public string aux_unit { get; set; }
        /// <summary> 
        /// 标记 
        /// </summary> 
        /// <returns></returns> 
        [Column("FLAG_ID")]
        public string flag_id { get; set; }
        /// <summary> 
        /// 属性 
        /// </summary> 
        /// <returns></returns> 
        [Column("PROPERTIES")]
        public string properties { get; set; }
        /// <summary> 
        /// 版本号，用来检查同时修改的 
        /// </summary> 
        /// <returns></returns> 
        [Column("VERSION_ID")]
        public string version_id { get; set; }
        /// <summary> 
        /// 最后修改时间  格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFIED")]
        public DateTime? modified { get; set; }
        /// <summary> 
        /// 创建时间  格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATED")]
        public DateTime? created { get; set; }
        /// <summary> 
        /// 基本单位名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("UNIT_NAME")]
        public string unit_name { get; set; }
        /// <summary> 
        /// 辅助单位名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("AUX_UNIT_NAME")]
        public string aux_unit_name { get; set; }
        /// <summary> 
        /// 标记名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("FLAG_NAME")]
        public string flag_name { get; set; }
        /// <summary> 
        /// 格式：yyyy-MM-dd HH:mm:ss。ERP客户端需升级至V2.3.8.6及以上版本可获取此字段 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_CREATED")]
        public DateTime? goods_created { get; set; }
        /// <summary> 
        /// 格式：yyyy-MM-dd HH:mm:ss 
        /// </summary> 
        /// <returns></returns> 
        [Column("GOODS_MODIFIED")]
        public DateTime? goods_modified { get; set; }
        /// <summary> 
        /// null否已删除： 0：未删除  >0代表已删除 
        /// </summary> 
        /// <returns></returns> 
        [Column("DELETED")]
        public int? deleted { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(int? keyValue)
        {
            this.goods_id = keyValue;
        }
        #endregion
    }
}
