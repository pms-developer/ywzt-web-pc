﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Entity
{
    public class NcMaterial
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string PK_Material { get; set; }
        public string PK_marbasclass { get; set; }
        public string PK_measdoc { get; set; }
        public string PK_mattaxes { get; set; }
        public string PK_brand { get; set; }

        public string Materialspec { get; set; }
        public string Materialtype { get; set; }

        public string Memo { get; set; }

        public string Materialbarcode { get; set; }

        public string PurchsePrice { get; set; }

        public string PK_CurrentType { get; set; }

    }
}
