﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtGoodsClass
    {
        #region  实体成员 
        /// <summary> 
        /// 分类主键id 
        /// </summary> 
        /// <returns></returns> 
       
        [Column("CLASS_ID")]
        public int? class_id { get; set; }
        /// <summary> 
                /// 货品分组id, ERP内自定义设置（路径:ERP→货品→货品分类）,返回货品分类的上层分组id 
                /// </summary> 
                /// <returns></returns> 
        [Column("PARENT_ID")]
        public string parent_id { get; set; }
        /// <summary> 
                /// 是分类 
                /// </summary> 
                /// <returns></returns> 
        [Column("IS_LEAF")]
        public string is_leaf { get; set; }
        /// <summary> 
                /// 分类名称 
                /// </summary> 
                /// <returns></returns> 
        [Column("CLASS_NAME")]
        public string class_name { get; set; }
        /// <summary> 
                /// 路径 
                /// </summary> 
                /// <returns></returns> 
        [Column("PATH")]
        public string path { get; set; }
        /// <summary> 
                /// 最后修改时间 
                /// </summary> 
                /// <returns></returns> 
        [Column("MODIFIED")]
        public DateTime? modified { get; set; }
        /// <summary> 
                /// 创建时间 
                /// </summary> 
                /// <returns></returns> 
        [Column("CREATED")]
        public DateTime? created { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
                /// 新增调用 
                /// </summary> 
        public void Create()
        {
        }

        /// <summary> 
                /// 新增调用 
                /// </summary> 
        public void Create(string keyValue)
        {
        }

        /// <summary> 
                /// 编辑调用 
                /// </summary> 
                /// <param name="keyValue"></param> 
        public void Modify(int? keyValue)
        {
            this.class_id = keyValue;
        }
        #endregion
    }
}
