﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Entity
{
    public class NcShop
    {
        public string Name { get; set; }

        public string Code { get; set; }

        public string PK_defdoc { get; set; }

        public string PID { get; set; }
        public string PK_org { get; set; }
        
    }
}
