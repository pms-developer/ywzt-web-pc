﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models.Entity
{
    public class WdtGoodsBrand
    {
        #region  实体成员 
        /// <summary> 
        /// 品牌ID 
        /// </summary> 
        /// <returns></returns> 
        [Column("BRAND_ID")]
        public string brand_id { get; set; }
        /// <summary> 
        /// 品牌编号 
        /// </summary> 
        /// <returns></returns> 
        [Column("BRAND_NO")]
        public string brand_no { get; set; }
        /// <summary> 
        /// 品牌名称 
        /// </summary> 
        /// <returns></returns> 
        [Column("BRAND_NAME")]
        public string brand_name { get; set; }
        /// <summary> 
        /// 备注 
        /// </summary> 
        /// <returns></returns> 
        [Column("REMARK")]
        public string remark { get; set; }
        /// <summary> 
        /// 销售增长率类型 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALES_RATE_TYPE")]
        public string sales_rate_type { get; set; }
        /// <summary> 
        /// 动态销售增长率计算周期 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALES_RATE_CYCLE")]
        public int? sales_rate_cycle { get; set; }
        /// <summary> 
        /// 动态销售增长率计算周期（前A日） 
        /// </summary> 
        /// <returns></returns> 
        [Column("PURCHASE_COMPUTING_CYCLE")]
        public int? purchase_computing_cycle { get; set; }
        /// <summary> 
        /// 动态销售增长率计算周期（前B日） 
        /// </summary> 
        /// <returns></returns> 
        [Column("PURCHASE_COMPUTING_CYCLE1")]
        public int? purchase_computing_cycle1 { get; set; }
        /// <summary> 
        /// 固定销售增长率 
        /// </summary> 
        /// <returns></returns> 
        [Column("SALES_RATE")]
        public string sales_rate { get; set; }
        /// <summary> 
        /// 警戒库存类型 
        /// </summary> 
        /// <returns></returns> 
        [Column("ALARM_TYPE")]
        public string alarm_type { get; set; }
        /// <summary> 
        /// 警戒库存天数 
        /// </summary> 
        /// <returns></returns> 
        [Column("ALARM_DAYS")]
        public string alarm_days { get; set; }
        /// <summary> 
        /// 最大警戒天数 
        /// </summary> 
        /// <returns></returns> 
        [Column("ALARM_DAYS1")]
        public string alarm_days1 { get; set; }
        /// <summary> 
        /// null否停用，0表示正常，1表示停用 
        /// </summary> 
        /// <returns></returns> 
        [Column("IS_DISABLED")]
        public string is_disabled { get; set; }
        /// <summary> 
        /// 最后修改时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("MODIFIED")]
        public string modified { get; set; }
        /// <summary> 
        /// 创建时间 
        /// </summary> 
        /// <returns></returns> 
        [Column("CREATED")]
        public string created { get; set; }
        #endregion

        #region  扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
            this.brand_id = Guid.NewGuid().ToString();
        }

        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create(string keyValue)
        {
            this.brand_id = keyValue;
        }

        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.brand_id = keyValue;
        }
        #endregion
    }
}
