﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models.Entity
{
    public class QnjsPlan
    {
        public string Answers { get; set; }
        public string Week { get; set; }
        public string Name { get; set; }
        public string ActionId { get; set; }
    }

    public class qnjs_trainplan
    {
        public string Answers { get; set; }
        public string MondayCourses { get; set; }
        public string TuesdayCourses { get; set; }
        public string WednesdayCourses { get; set; }
        public string ThursdayCourses { get; set; }
        public string FridayCourses { get; set; }
        public string SaturdayCourses { get; set; }
        public string SundayCourses { get; set; }

    }

}
