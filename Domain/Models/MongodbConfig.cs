﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class MongodbConfig
    {
        public string MongodbService { get; set; }
        public string Uid { get; set; }
        public string Password { get; set; }
        public string AppKey { get; set; }
        public string AppSecret { get; set; }
      
    }
}
