﻿using HuNanChangJie.Application.TwoDevelopment.BaseInfo;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ComputeLogisticsPackageAccountTaskJob
{
    internal class Program
    {
        private static MeioErpLogisticsPackageAccountIBLL logisticsPackageAccountIBLL = new MeioErpLogisticsPackageAccountBLL();
        private static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            try
            {
                log.Info($"\r\n{DateTime.Now}：开始计算快递账单");
                //计算快递账单
                logisticsPackageAccountIBLL.ComputeLogisticsPackageAccount(null,1);
                log.Info($"\r\n{DateTime.Now}：结束计算快递账单");
            }
            catch (Exception ex)
            {
                log.Error($"{DateTime.Now}：{ex.Message}");
            }
        }
    }
}
