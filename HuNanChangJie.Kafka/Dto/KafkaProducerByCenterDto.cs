﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HuNanChangJie.Kafka.Dto
{
    public class KafkaProducerByCenterDto
    {
        public string system { get; set; }
        public string action { get; set; }
        public string table { get; set; }
        public object data { get; set; }
    }
}
