﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuNanChangJie.Kafka
{
    public enum Action
    {
        /// <summary>
        /// 添加
        /// </summary>
        [Description("添加")]
        add,
        /// <summary>
        /// 修改
        /// </summary>
        [Description("修改")]
        update
    }
}
