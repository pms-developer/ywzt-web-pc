﻿using KafkaNet.Model;
using KafkaNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HuNanChangJie.Kafka.Dto;
//using Dop.Json;
using HuNanChangJie.Util;
using log4net.Util;
using System.Runtime.CompilerServices;
using log4net;
using System.Reflection;
using KafkaNet.Protocol;

namespace HuNanChangJie.Kafka.Service
{
    public static class KafkaService
    {
        private readonly static string strUri= Config.GetValue("KafkaUri");

        private static ILog log=LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static void Producer(string topic, string payload)
        {
            try
            {
                KafkaNet.Protocol.Message msg = new KafkaNet.Protocol.Message(payload);
                Uri uri = new Uri(strUri);
                var options = new KafkaOptions(uri);
                var router = new BrokerRouter(options);
                var client = new Producer(router);
                client.SendMessageAsync(topic, new List<KafkaNet.Protocol.Message> { msg }).Wait(1000);
                log.Info($"\r\nkafka生产时间：{DateTime.Now}-------内容：{payload}");
            }
            catch (Exception e)
            {
                log.Error($"\r\nkafka生产异常，时间：{DateTime.Now}-----异常:"+e.Message);
            }
        }

        private static string Consumer(string topic)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                Uri uri = new Uri(strUri);
                var options = new KafkaOptions(uri);
                var router = new BrokerRouter(options);
                var consumer = new Consumer(new ConsumerOptions(topic, router));
                foreach (var message in consumer.Consume())
                    result.Append(Encoding.UTF8.GetString(message.Value));
                log.Info($"\r\nkafka消费时间：{DateTime.Now}-------内容：{result.ToString()}");
            }
            catch (Exception e)   
            {
                log.Error($"\r\nkafka消费异常，时间：{DateTime.Now}-----异常:" + e.Message);
            }
            return result.ToString();
        }

        public static void SendMessage(Action action, string tableName,object obj)
        {
            if (obj != null)
            {
                KafkaProducerByCenterDto centerDto = new KafkaProducerByCenterDto
                {
                    system = "center",
                    action = action.ToString(),
                    table= tableName,
                    data =obj
                };
                Producer("center", centerDto.ToJson().Replace("\\",""));
            }
        }

    }
}
