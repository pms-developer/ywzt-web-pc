﻿using System;
namespace Dop.Core
{
    public abstract class DoudianOpApiRequest<R>
    {

        public DoudianOpApiRequest()
        {
            Config = GlobalConfig.GetGlobalConfig();
            Client = DoudianOpApiClient.GetInstance();
        }

        public T Execute<T>(AccessToken accessToken)
        {
            return Client.Request<T, R>(this, accessToken);
        }

        public abstract string GetUrlPath();

        public abstract Type GetResponseType();

        public R Param { get; set; }

        public DoudianOpApiClient Client { get; set; }

        public DoudianOpConfig Config { get; set; }
    }
}
