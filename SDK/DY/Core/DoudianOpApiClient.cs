﻿using System;
using Dop.Core;
using Dop.Core.Http;
using Dop.Exception;
using Dop.Utils;
using Dop.Json;

namespace Dop.Core
{
    public class DoudianOpApiClient
    {

        private static DoudianOpApiClient INSTANCE = new DoudianOpApiClient();

        public static DoudianOpApiClient GetInstance()
        {
            return INSTANCE;
        }

        private DoudianOpHttpClient httpClient;

        public DoudianOpApiClient()
        {
            httpClient = DoudianOpHttpClient.GetInstance();
        }

        public T Request<T, R>(DoudianOpApiRequest<R> request, AccessToken accessToken)
        {
            //必要参数校验
            if (request.Config.AppKey == null)
            {
                throw DoudianOpException.Build(DoudianOpExcepptionCode.AppKeyEmptyException);
            }
            if (request.Config.AppSecret == null)
            {
                throw DoudianOpException.Build(DoudianOpExcepptionCode.AppSecretEmptyException);
            }

            var param = request.Param;
            var paramJson = "{}";
            if (param != null)
            {
                paramJson = param.ToJson();
            }
            var timestamp = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
            var urlPath = GetRequestUrlPath(request.GetUrlPath());
            var appKey = request.Config.AppKey;
            var appSecret = request.Config.AppSecret;
            var method = urlPath.Replace("/", ".");

            //计算签名
            var sign = SignUtil.Sign(appKey, appSecret, method, timestamp, paramJson);

            string accessTokenStr = "";
            if (accessToken != null)
            {
                accessTokenStr = accessToken.GetAccessToken();
            }

            //组装实际发起请求的url
            var requestUrl = ConstructRequestUrl(request.Config.OpenRequestUrl, urlPath, appKey, method, sign, timestamp, accessTokenStr);
            var httpRequest = new HttpRequest
            {
                Url = requestUrl,
                Body = paramJson
            };

            //标志请求来源
            httpRequest.Headers.Add("from", "sdk");
            httpRequest.Headers.Add("sdk-type", "dotnet");
            httpRequest.Headers.Add("sdk-version", Constant.DoudianSdkVersion);
            httpRequest.Headers.Add("x-open-no-old-err-code", "1");

            //发起http请求
            var httpResponse = httpClient.Post(httpRequest);

            if (httpResponse.StatusCode != 200)
            {
                throw DoudianOpException.Build(DoudianOpExcepptionCode.HttpResponseCodeNot200, string.Format("status code -> %d", httpResponse.StatusCode));
            }

            var bodyJson = httpResponse.Body;
            if (bodyJson == null || bodyJson.Length == 0)
            {
                bodyJson = "{}";
            }

            //将服务端返回的json序列化成对象模型
            try
            {
                return (T)JSONParser.FromJson(bodyJson, request.GetResponseType());
            }
            catch (System.Exception e)
            {
                throw DoudianOpException.Build(DoudianOpExcepptionCode.JsonParseError, e);
            }

        }

        private string GetRequestUrlPath(String originUrl)
        {
            if (originUrl == null || originUrl.Length == 0)
            {
                return originUrl;
            }
            if (originUrl.StartsWith("/"))
            {
                return originUrl.Substring(1);
            }
            return originUrl;
        }

        private String ConstructRequestUrl(string url, string path, string appKey, string method, string sign, string timestamp, string accessToken)
        {
            return url + "/" + path + "?" + "app_key=" + appKey + "&method=" + method + "&v=2" + "&sign=" + sign + "&timestamp=" + timestamp + "&access_token=" + accessToken + "&sign_method=hmac-sha256";
        }
    }

}
