﻿using System;
namespace Dop.Core
{
    public class GlobalConfig : DoudianOpConfig
    {
        private static GlobalConfig INSTANCE = new GlobalConfig();

        public static GlobalConfig GetGlobalConfig()
        {
            return INSTANCE;
        }

    }
}
