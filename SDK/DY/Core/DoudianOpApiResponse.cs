﻿using System;
using System.Runtime.Serialization;

namespace Dop.Core
{
    public class DoudianOpApiResponse<T>
    {
        [DataMemberAttribute(Name = "data")]
        public T Data { get; set; }

        [DataMemberAttribute(Name = "log_id")]
        public string LogId { get; set; }

        [DataMemberAttribute(Name = "code")]
        public long Code { get; set; }

        [DataMemberAttribute(Name = "msg")]
        public string Msg { get; set; }

        [DataMemberAttribute(Name = "sub_code")]
        public string SubCode { get; set; }

        [DataMemberAttribute(Name = "sub_msg")]
        public string SubMsg { get; set; }

        public bool IsSuccess()
        {
            return Code == 10000;
        }
    }
}
