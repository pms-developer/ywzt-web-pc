﻿using System;
namespace Dop.Core
{
    public class DoudianOpConfig
    {
        public DoudianOpConfig()
        {
            OpenRequestUrl = "https://openapi-fxg.jinritemai.com";
        }
        public string AppSecret { get; set; }
        public string AppKey { get; set; }
        public int Timeout { get; set; }
        public int ReadWriteTimeout { get; set; }
        public string OpenRequestUrl { get; set; }
    }
}
