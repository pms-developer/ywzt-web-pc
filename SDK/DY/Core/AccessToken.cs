﻿using System;
using Dop.Api.Token;

namespace Dop.Core
{
    public class AccessToken
    {
        private AccessTokenResponse accessTokenResponse;
        public AccessTokenResponse GetAccessTokenResponse()
        {
            return accessTokenResponse;
        }

        public AccessToken(AccessTokenResponse accessTokenResponse)
        {
            this.accessTokenResponse = accessTokenResponse;
        }


        public bool IsSuccess()
        {
            return accessTokenResponse != null && accessTokenResponse.IsSuccess();
        }

        public string GetAccessToken() {
            if (accessTokenResponse != null && accessTokenResponse.Data != null)
            {
                return accessTokenResponse.Data.AccessToken;
            }
            return default;
        }

        public long? GetExpireIn()
        {
            if (accessTokenResponse != null && accessTokenResponse.Data != null)
            {
                return accessTokenResponse.Data.ExpiresIn;
            }
            return default;
        }
        public string GetScope()
        {
            if (accessTokenResponse != null && accessTokenResponse.Data != null)
            {
                return accessTokenResponse.Data.Scope;
            }
            return default;
        }
        public string GetShopId()
        {
            if (accessTokenResponse != null && accessTokenResponse.Data != null)
            {
                return accessTokenResponse.Data.ShopId;
            }
            return default;
        }
        public string GetShopName()
        {
            if (accessTokenResponse != null && accessTokenResponse.Data != null)
            {
                return accessTokenResponse.Data.ShopName;
            }
            return default;
        }
        public string GetRefreshToken()
        {
            if (accessTokenResponse != null && accessTokenResponse.Data != null)
            {
                return accessTokenResponse.Data.RefreshToken;
            }
            return default;
        }
        public string GetAuthorityId()
        {
            if (accessTokenResponse != null && accessTokenResponse.Data != null)
            {
                return accessTokenResponse.Data.AuthorityId;
            }
            return default;
        }

    }
}
