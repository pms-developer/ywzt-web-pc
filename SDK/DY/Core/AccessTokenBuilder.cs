﻿using System;
using Dop.Api.Token;
using Dop.Api.Token.Data;
using Dop.Api.Token.Param;

namespace Dop.Core
{
    /// <summary>
    /// 该类用于创建AccessToken
    /// 使用Build方法创建AccessToken后，开发者需要将AccessToken缓存起来
    /// 在AccessToken过期之前调用Refresh方法进行刷新，剩余过期时间通过AccessToken.GetExpireIn()方法获取
    /// </summary>
    public class AccessTokenBuilder
    {
        /// <summary>
        /// 自用型应用获取AccessToken
        /// </summary>
        /// <param name="shopId">店铺ID</param>
        /// <returns></returns>
        public static AccessToken Build(long shopId)
        {
            return Build(shopId, GlobalConfig.GetGlobalConfig());
        }

        /// <summary>
        /// 工具型应用获取AccessToken
        /// </summary>
        /// <param name="code">授权code</param>
        /// <returns></returns>
        public static AccessToken Build(string code)
        {
            return Build(code, GlobalConfig.GetGlobalConfig());
        }

        /// <summary>
        /// 自用型应用获取AccessToken
        /// </summary>
        /// <param name="shopId">店铺ID</param>
        /// <param name="config">自定义配置</param>
        /// <returns></returns>
        public static AccessToken Build(long shopId, DoudianOpConfig config)
        {
            return Build(shopId, null, config);
        }

        /// <summary>
        /// 工具型应用获取AccessToken
        /// </summary>
        /// <param name="code">授权code</param>
        /// <param name="config">自定义配置</param>
        /// <returns></returns>
        public static AccessToken Build(string code, DoudianOpConfig config)
        {
            return Build(0, code, config);
        }

        /// <summary>
        /// 将原始的access_token string转换成AccessToken对象
        /// 一般适用于开发者自己管理token生命周期的场景使用
        /// </summary>
        /// <param name="accessTokenStr"></param>
        /// <returns></returns>
        public static AccessToken Parse(string accessTokenStr)
        {
            AccessTokenResponse accessTokenResponse = new AccessTokenResponse();
            accessTokenResponse.Code = 10000;
            accessTokenResponse.Data = new AccessTokenData();
            accessTokenResponse.Data.AccessToken = accessTokenStr;
            return new AccessToken(accessTokenResponse);
        }

        /// <summary>
        /// 刷新AccessToken
        /// 刷新操作只有在AccessToken过期前一个小时调用才会生效，否则返回原始的AccessToken
        /// </summary>
        /// <param name="refreshTokenStr"></param>
        /// <returns>刷新后的新AccessToken</returns>
        public static AccessToken Refresh(string refreshTokenStr)
        {
            RefreshTokenRequest request = new RefreshTokenRequest();
            RefreshTokenParam param = request.BuildParam();
            param.GrantType = "refresh_token";
            param.RefreshToken = refreshTokenStr;

            var response = request.Execute<AccessTokenResponse>(null);
            AccessToken accessToken = new AccessToken(response);
            return accessToken;
        }

        /// <summary>
        /// 刷新AccessToken
        /// 刷新操作只有在AccessToken过期前一个小时调用才会生效，否则返回原始的AccessToken
        /// </summary>
        /// <param name="refreshTokenStr"></param>
        /// <param name="config"></param>
        /// <returns>刷新后的新AccessToken</returns>
        public static AccessToken Refresh(string refreshTokenStr, DoudianOpConfig config)
        {
            RefreshTokenRequest request = new RefreshTokenRequest();
            request.Config = config;
            RefreshTokenParam param = request.BuildParam();
            param.GrantType = "refresh_token";
            param.RefreshToken = refreshTokenStr;

            var response = request.Execute<AccessTokenResponse>(null);
            AccessToken accessToken = new AccessToken(response);
            return accessToken;
        }


        private static AccessToken Build(long shopId, string code, DoudianOpConfig config)
        {
            AccessTokenRequest request = new AccessTokenRequest();
            request.Config = config;
            AccessTokenParam param = request.BuildParam();

            if (string.IsNullOrEmpty(code))
            {
                param.ShopId = shopId;
                param.GrantType = "authorization_self";
                param.Code = "";
            } else
            {
                param.GrantType = "authorization_code";
                param.Code = code;
            }
          

            var response = request.Execute<AccessTokenResponse>(null);
            AccessToken accessToken = new AccessToken(response);
            return accessToken;
        }
      
    }
}
