﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using Dop.Exception;

namespace Dop.Core.Http
{
    public class DoudianOpHttpClient
    {

        private static DoudianOpHttpClient INSTANCE = new DoudianOpHttpClient();

        public static DoudianOpHttpClient GetInstance()
        {
            return INSTANCE;
        }


        private HttpClient httpClient = null;

        public DoudianOpHttpClient()
        {
            httpClient = new HttpClient();
        }

        public HttpResponse Post(HttpRequest request)
        {
            Stream requestStream = null;
            HttpWebResponse webResp = null;
            System.Net.ServicePointManager.DefaultConnectionLimit = 50;
            try
            {
                HttpWebRequest webRequest = BuildWebRequest(request.Url, "POST", request.Headers, request.Timeout, request.ReadWriteTimeout);
                webRequest.ContentType = "application/json;charset=utf-8";
                byte[] postData = Encoding.UTF8.GetBytes(request.Body);
                requestStream = webRequest.GetRequestStream();
                requestStream.Write(postData, 0, postData.Length);
                requestStream.Close();
                System.GC.Collect();
                webResp = (HttpWebResponse)webRequest.GetResponse();
                string respBody = ReadWebResponse(webResp);
                HttpResponse ret = new HttpResponse();
                ret.Body = respBody;
                ret.StatusCode = (int)webResp.StatusCode;
                return ret;
            } finally
            {
                if (requestStream != null) requestStream.Close();
                if (webResp != null) requestStream.Close();
            }
        }


        public string ReadWebResponse(HttpWebResponse resp)
        {
            Stream stream = null;
            StreamReader reader = null;
            try
            {
                stream = resp.GetResponseStream();
                reader = new StreamReader(stream);
                return reader.ReadToEnd();
            }
            finally
            {
                if (reader != null) reader.Close();
                if (stream != null) stream.Close();
            }
        }

        private HttpWebRequest BuildWebRequest(string url, string method, Dictionary<string, string> headers, int timeout, int readWriteTimeout)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            if (headers != null && headers.Count > 0)
            {
                foreach (string key in headers.Keys)
                {
                    req.Headers.Add(key, headers[key]);
                }
            }

            req.ServicePoint.Expect100Continue = false;
            req.Method = method;
            req.KeepAlive = true;
            req.Timeout = timeout;
            req.ReadWriteTimeout = readWriteTimeout;
            return req;
        }
    }

}
