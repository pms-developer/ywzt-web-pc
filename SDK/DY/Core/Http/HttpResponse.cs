﻿using System;
namespace Dop.Core.Http
{
    public class HttpResponse
    {
        public string Body { get; set; }
        public int StatusCode { get; set; }
    }
}
