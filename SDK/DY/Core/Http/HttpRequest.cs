﻿using System.Collections.Generic;

namespace Dop.Core.Http
{
    public class HttpRequest
    {
        public HttpRequest()
        {
            this.Timeout = 1000;
            this.ReadWriteTimeout = 12000;
            this.Headers = new Dictionary<string, string>();
        }

        public string Body { get; set; }
        public string Url { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public int Timeout { get; set; }
        public int ReadWriteTimeout { get; set; }
    }
}
