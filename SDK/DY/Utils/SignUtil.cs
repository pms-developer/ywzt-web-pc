﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Dop.Utils
{
    public class SignUtil
    {
        public static string Sign(string appKey, string appSecret, string method, string timestamp, string paramJson)
        {
            // 按给定规则拼接参数
            var paramPattern = "app_key" + appKey + "method" + method + "param_json" + paramJson + "timestamp" + timestamp + "v2";
            var signPattern = appSecret + paramPattern + appSecret;
            return Hmac(signPattern, appSecret);
        }


        // 计算hmac
        public static string Hmac(string plainText, string appSecret)
        {
            var h = new HMACSHA256(Encoding.UTF8.GetBytes(appSecret));
            var sum = h.ComputeHash(Encoding.UTF8.GetBytes(plainText));

            var sb = new StringBuilder();
            foreach (byte b in sum)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }
    }

}
