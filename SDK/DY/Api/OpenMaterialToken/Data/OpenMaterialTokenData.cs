using System.Runtime.Serialization;

namespace Dop.Api.OpenMaterialToken.Data
{
	public class OpenMaterialTokenData
	{
		[DataMemberAttribute(Name = "auth_query")]
		public string AuthQuery { get; set; }

	}
}
