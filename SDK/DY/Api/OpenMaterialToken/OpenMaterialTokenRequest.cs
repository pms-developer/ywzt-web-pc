using System;
using Dop.Core;
using Dop.Api.OpenMaterialToken.Param;

namespace Dop.Api.OpenMaterialToken
{
	public class OpenMaterialTokenRequest : DoudianOpApiRequest<OpenMaterialTokenParam>
	{
		public override string GetUrlPath()
		{
			return "/open/materialToken";
		}

		public override Type GetResponseType()
		{
			return typeof(OpenMaterialTokenResponse);
		}

		public  OpenMaterialTokenParam BuildParam()
		{
			return Param ?? (Param = new OpenMaterialTokenParam());
		}

	}
}
