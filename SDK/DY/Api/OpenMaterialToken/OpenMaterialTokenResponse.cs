using Dop.Core;
using Dop.Api.OpenMaterialToken.Data;

namespace Dop.Api.OpenMaterialToken
{
	public class OpenMaterialTokenResponse : DoudianOpApiResponse<OpenMaterialTokenData>
	{
	}
}
