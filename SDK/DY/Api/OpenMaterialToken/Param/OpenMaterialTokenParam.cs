using System.Runtime.Serialization;

namespace Dop.Api.OpenMaterialToken.Param
{
	public class OpenMaterialTokenParam
	{
		[DataMemberAttribute(Name = "upload_num")]
		public long? UploadNum { get; set; }

		[DataMemberAttribute(Name = "file_extension")]
		public string FileExtension { get; set; }

	}
}
