using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Param
{
	public class AfterSaleDetailParam
	{
		[DataMemberAttribute(Name = "after_sale_id")]
		public string AfterSaleId { get; set; }

		[DataMemberAttribute(Name = "is_searchable")]
		public bool IsSearchable { get; set; }

	}
}
