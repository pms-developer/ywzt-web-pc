using System;
using Dop.Core;
using Dop.Api.AfterSaleDetail.Param;

namespace Dop.Api.AfterSaleDetail
{
	public class AfterSaleDetailRequest : DoudianOpApiRequest<AfterSaleDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/Detail";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleDetailResponse);
		}

		public  AfterSaleDetailParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleDetailParam());
		}

	}
}
