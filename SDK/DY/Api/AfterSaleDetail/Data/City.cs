using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class City
	{
		[DataMemberAttribute(Name = "id")]
		public string Id { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

	}
}
