using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class ArbitrateInfo
	{
		[DataMemberAttribute(Name = "arbitrate_id")]
		public string ArbitrateId { get; set; }

		[DataMemberAttribute(Name = "arbitrate_status")]
		public long? ArbitrateStatus { get; set; }

		[DataMemberAttribute(Name = "is_required_evidence")]
		public bool? IsRequiredEvidence { get; set; }

		[DataMemberAttribute(Name = "arbitrate_status_deadline")]
		public string ArbitrateStatusDeadline { get; set; }

		[DataMemberAttribute(Name = "arbitrate_opinion")]
		public string ArbitrateOpinion { get; set; }

		[DataMemberAttribute(Name = "arbitrate_conclusion")]
		public long? ArbitrateConclusion { get; set; }

		[DataMemberAttribute(Name = "arbitrate_create_time")]
		public long? ArbitrateCreateTime { get; set; }

		[DataMemberAttribute(Name = "arbitrate_update_time")]
		public long? ArbitrateUpdateTime { get; set; }

		[DataMemberAttribute(Name = "arbitrate_evidence_tmpl")]
		public ArbitrateEvidenceTmpl ArbitrateEvidenceTmpl { get; set; }

		[DataMemberAttribute(Name = "arbitrate_evidence")]
		public ArbitrateEvidence ArbitrateEvidence { get; set; }

		[DataMemberAttribute(Name = "arbitrate_blame")]
		public long? ArbitrateBlame { get; set; }

	}
}
