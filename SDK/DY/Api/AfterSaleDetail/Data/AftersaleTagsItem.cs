using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class AftersaleTagsItem
	{
		[DataMemberAttribute(Name = "tag_detail")]
		public string TagDetail { get; set; }

		[DataMemberAttribute(Name = "tag_detail_en")]
		public string TagDetailEn { get; set; }

		[DataMemberAttribute(Name = "tag_detail_text")]
		public string TagDetailText { get; set; }

		[DataMemberAttribute(Name = "tag_link_url")]
		public string TagLinkUrl { get; set; }

		[DataMemberAttribute(Name = "placeholder")]
		public Dictionary<string,PlaceholderItem> Placeholder { get; set; }

	}
}
