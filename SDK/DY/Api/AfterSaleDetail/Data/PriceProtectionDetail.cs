using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class PriceProtectionDetail
	{
		[DataMemberAttribute(Name = "title")]
		public string Title { get; set; }

		[DataMemberAttribute(Name = "price_protection_formulas")]
		public string PriceProtectionFormulas { get; set; }

		[DataMemberAttribute(Name = "standard_price")]
		public StandardPrice StandardPrice { get; set; }

		[DataMemberAttribute(Name = "check_price")]
		public CheckPrice CheckPrice { get; set; }

		[DataMemberAttribute(Name = "refund_detail")]
		public RefundDetail RefundDetail { get; set; }

		[DataMemberAttribute(Name = "refund_bearer_list")]
		public List<RefundBearerListItem> RefundBearerList { get; set; }

		[DataMemberAttribute(Name = "platform_to_merchant_refund_status")]
		public long? PlatformToMerchantRefundStatus { get; set; }

		[DataMemberAttribute(Name = "platform_recycle_amount")]
		public long? PlatformRecycleAmount { get; set; }

	}
}
