using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class ArbitrateEvidence
	{
		[DataMemberAttribute(Name = "images")]
		public List<string> Images { get; set; }

		[DataMemberAttribute(Name = "describe")]
		public string Describe { get; set; }

	}
}
