using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class Resend
	{
		[DataMemberAttribute(Name = "tracking_no")]
		public string TrackingNo { get; set; }

		[DataMemberAttribute(Name = "company_name")]
		public string CompanyName { get; set; }

		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "logistics_time")]
		public long? LogisticsTime { get; set; }

	}
}
