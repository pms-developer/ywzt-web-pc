using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class RefundDetail
	{
		[DataMemberAttribute(Name = "actual_amount")]
		public ActualAmount ActualAmount { get; set; }

		[DataMemberAttribute(Name = "origin_amount")]
		public OriginAmount OriginAmount { get; set; }

		[DataMemberAttribute(Name = "deduction_price_detail")]
		public List<DeductionPriceDetailItem> DeductionPriceDetail { get; set; }

	}
}
