using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class SkuSpecItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "value")]
		public string Value { get; set; }

	}
}
