using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class AfterSaleDetailData
	{
		[DataMemberAttribute(Name = "process_info")]
		public ProcessInfo ProcessInfo { get; set; }

		[DataMemberAttribute(Name = "order_info")]
		public OrderInfo OrderInfo { get; set; }

	}
}
