using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class AfterSaleInfo
	{
		[DataMemberAttribute(Name = "after_sale_id")]
		public long? AfterSaleId { get; set; }

		[DataMemberAttribute(Name = "after_sale_status")]
		public long? AfterSaleStatus { get; set; }

		[DataMemberAttribute(Name = "after_sale_status_desc")]
		public string AfterSaleStatusDesc { get; set; }

		[DataMemberAttribute(Name = "refund_type")]
		public long? RefundType { get; set; }

		[DataMemberAttribute(Name = "refund_type_text")]
		public string RefundTypeText { get; set; }

		[DataMemberAttribute(Name = "refund_status")]
		public long? RefundStatus { get; set; }

		[DataMemberAttribute(Name = "refund_total_amount")]
		public long? RefundTotalAmount { get; set; }

		[DataMemberAttribute(Name = "refund_post_amount")]
		public long? RefundPostAmount { get; set; }

		[DataMemberAttribute(Name = "refund_promotion_amount")]
		public long? RefundPromotionAmount { get; set; }

		[DataMemberAttribute(Name = "apply_time")]
		public long? ApplyTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public long? UpdateTime { get; set; }

		[DataMemberAttribute(Name = "reason")]
		public string Reason { get; set; }

		[DataMemberAttribute(Name = "reason_code")]
		public long? ReasonCode { get; set; }

		[DataMemberAttribute(Name = "after_sale_type")]
		public long? AfterSaleType { get; set; }

		[DataMemberAttribute(Name = "after_sale_type_text")]
		public string AfterSaleTypeText { get; set; }

		[DataMemberAttribute(Name = "reason_remark")]
		public string ReasonRemark { get; set; }

		[DataMemberAttribute(Name = "evidence")]
		public List<string> Evidence { get; set; }

		[DataMemberAttribute(Name = "after_sale_apply_count")]
		public long? AfterSaleApplyCount { get; set; }

		[DataMemberAttribute(Name = "need_return_count")]
		public long? NeedReturnCount { get; set; }

		[DataMemberAttribute(Name = "deduction_amount")]
		public long? DeductionAmount { get; set; }

		[DataMemberAttribute(Name = "disable_coupon_id")]
		public string DisableCouponId { get; set; }

		[DataMemberAttribute(Name = "platform_discount_return_amount")]
		public long? PlatformDiscountReturnAmount { get; set; }

		[DataMemberAttribute(Name = "platform_discount_return_status")]
		public long? PlatformDiscountReturnStatus { get; set; }

		[DataMemberAttribute(Name = "kol_discount_return_amount")]
		public long? KolDiscountReturnAmount { get; set; }

		[DataMemberAttribute(Name = "kol_discount_return_status")]
		public long? KolDiscountReturnStatus { get; set; }

		[DataMemberAttribute(Name = "post_receiver")]
		public string PostReceiver { get; set; }

		[DataMemberAttribute(Name = "encrypt_post_receiver")]
		public string EncryptPostReceiver { get; set; }

		[DataMemberAttribute(Name = "post_tel_sec")]
		public string PostTelSec { get; set; }

		[DataMemberAttribute(Name = "encrypt_post_tel_sec")]
		public string EncryptPostTelSec { get; set; }

		[DataMemberAttribute(Name = "post_address")]
		public PostAddress PostAddress { get; set; }

		[DataMemberAttribute(Name = "risk_decsison_code")]
		public long? RiskDecsisonCode { get; set; }

		[DataMemberAttribute(Name = "risk_decsison_reason")]
		public string RiskDecsisonReason { get; set; }

		[DataMemberAttribute(Name = "risk_decsison_description")]
		public string RiskDecsisonDescription { get; set; }

		[DataMemberAttribute(Name = "return_address")]
		public ReturnAddress ReturnAddress { get; set; }

		[DataMemberAttribute(Name = "real_refund_amount")]
		public long? RealRefundAmount { get; set; }

		[DataMemberAttribute(Name = "got_pkg")]
		public long? GotPkg { get; set; }

		[DataMemberAttribute(Name = "status_deadline")]
		public long? StatusDeadline { get; set; }

		[DataMemberAttribute(Name = "return_address_id")]
		public long? ReturnAddressId { get; set; }

		[DataMemberAttribute(Name = "exchange_sku_info")]
		public ExchangeSkuInfo ExchangeSkuInfo { get; set; }

		[DataMemberAttribute(Name = "post_discount_return_amount")]
		public long? PostDiscountReturnAmount { get; set; }

		[DataMemberAttribute(Name = "post_discount_return_status")]
		public long? PostDiscountReturnStatus { get; set; }

		[DataMemberAttribute(Name = "part_type")]
		public long? PartType { get; set; }

		[DataMemberAttribute(Name = "reason_second_labels")]
		public List<ReasonSecondLabelsItem> ReasonSecondLabels { get; set; }

		[DataMemberAttribute(Name = "refund_voucher_num")]
		public long? RefundVoucherNum { get; set; }

		[DataMemberAttribute(Name = "refund_voucher_times")]
		public long? RefundVoucherTimes { get; set; }

		[DataMemberAttribute(Name = "gold_coin_return_amount")]
		public long? GoldCoinReturnAmount { get; set; }

		[DataMemberAttribute(Name = "refund_fail_reason")]
		public string RefundFailReason { get; set; }

		[DataMemberAttribute(Name = "aftersale_tags")]
		public List<AftersaleTagsItem> AftersaleTags { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public string StoreId { get; set; }

		[DataMemberAttribute(Name = "store_name")]
		public string StoreName { get; set; }

	}
}
