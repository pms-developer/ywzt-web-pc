using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class LogisticsInfo
	{
		[DataMemberAttribute(Name = "return")]
		public Return Return { get; set; }

		[DataMemberAttribute(Name = "exchange")]
		public Exchange Exchange { get; set; }

		[DataMemberAttribute(Name = "order")]
		public List<OrderItem> Order { get; set; }

		[DataMemberAttribute(Name = "resend")]
		public Resend Resend { get; set; }

	}
}
