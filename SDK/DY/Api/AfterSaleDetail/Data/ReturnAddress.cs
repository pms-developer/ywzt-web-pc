using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class ReturnAddress
	{
		[DataMemberAttribute(Name = "province")]
		public Province Province { get; set; }

		[DataMemberAttribute(Name = "city")]
		public City City { get; set; }

		[DataMemberAttribute(Name = "town")]
		public Town Town { get; set; }

		[DataMemberAttribute(Name = "street")]
		public Street Street { get; set; }

		[DataMemberAttribute(Name = "landmark")]
		public string Landmark { get; set; }

		[DataMemberAttribute(Name = "detail")]
		public string Detail { get; set; }

	}
}
