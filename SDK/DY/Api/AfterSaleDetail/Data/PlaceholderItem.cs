using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class PlaceholderItem
	{
		[DataMemberAttribute(Name = "text")]
		public string Text { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
