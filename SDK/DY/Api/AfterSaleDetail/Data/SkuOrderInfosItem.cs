using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class SkuOrderInfosItem
	{
		[DataMemberAttribute(Name = "sku_order_id")]
		public long? SkuOrderId { get; set; }

		[DataMemberAttribute(Name = "order_status")]
		public long? OrderStatus { get; set; }

		[DataMemberAttribute(Name = "pay_amount")]
		public long? PayAmount { get; set; }

		[DataMemberAttribute(Name = "post_amount")]
		public long? PostAmount { get; set; }

		[DataMemberAttribute(Name = "item_quantity")]
		public long? ItemQuantity { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "tax_amount")]
		public long? TaxAmount { get; set; }

		[DataMemberAttribute(Name = "is_oversea_order")]
		public long? IsOverseaOrder { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "product_image")]
		public string ProductImage { get; set; }

		[DataMemberAttribute(Name = "tags")]
		public List<TagsItem> Tags { get; set; }

		[DataMemberAttribute(Name = "sku_spec")]
		public List<SkuSpecItem> SkuSpec { get; set; }

		[DataMemberAttribute(Name = "shop_sku_code")]
		public string ShopSkuCode { get; set; }

		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "item_sum_amount")]
		public long? ItemSumAmount { get; set; }

		[DataMemberAttribute(Name = "sku_pay_amount")]
		public long? SkuPayAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_amount")]
		public long? PromotionAmount { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public long? PayType { get; set; }

		[DataMemberAttribute(Name = "insurance_tags")]
		public List<InsuranceTagsItem> InsuranceTags { get; set; }

		[DataMemberAttribute(Name = "after_sale_item_count")]
		public long? AfterSaleItemCount { get; set; }

		[DataMemberAttribute(Name = "given_sku_details")]
		public List<GivenSkuDetailsItem> GivenSkuDetails { get; set; }

	}
}
