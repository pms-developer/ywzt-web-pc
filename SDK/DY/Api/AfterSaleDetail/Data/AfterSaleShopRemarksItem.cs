using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class AfterSaleShopRemarksItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public long? OrderId { get; set; }

		[DataMemberAttribute(Name = "after_sale_id")]
		public long? AfterSaleId { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "operator")]
		public string Operator { get; set; }

	}
}
