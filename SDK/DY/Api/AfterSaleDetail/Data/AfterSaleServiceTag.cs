using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class AfterSaleServiceTag
	{
		[DataMemberAttribute(Name = "after_sale_service_tag")]
		public List<AfterSaleServiceTagItem> AfterSaleServiceTag_ { get; set; }

	}
}
