using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class ValueAddedServicesItem
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

	}
}
