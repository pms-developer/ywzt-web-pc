using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class GivenSkuDetailsItem
	{
		[DataMemberAttribute(Name = "sku_order_id")]
		public string SkuOrderId { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

	}
}
