using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class ExchangeSkuInfo
	{
		[DataMemberAttribute(Name = "sku_id")]
		public string SkuId { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "num")]
		public long? Num { get; set; }

		[DataMemberAttribute(Name = "out_sku_id")]
		public string OutSkuId { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "supplier_id")]
		public string SupplierId { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "price")]
		public string Price { get; set; }

		[DataMemberAttribute(Name = "spec_desc")]
		public string SpecDesc { get; set; }

	}
}
