using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class RefundBearerListItem
	{
		[DataMemberAttribute(Name = "price_text")]
		public string PriceText { get; set; }

		[DataMemberAttribute(Name = "amount")]
		public long? Amount { get; set; }

	}
}
