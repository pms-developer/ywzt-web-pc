using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class ReasonSecondLabelsItem
	{
		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

	}
}
