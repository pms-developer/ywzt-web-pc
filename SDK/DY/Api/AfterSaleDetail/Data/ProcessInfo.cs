using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class ProcessInfo
	{
		[DataMemberAttribute(Name = "after_sale_info")]
		public AfterSaleInfo AfterSaleInfo { get; set; }

		[DataMemberAttribute(Name = "arbitrate_info")]
		public ArbitrateInfo ArbitrateInfo { get; set; }

		[DataMemberAttribute(Name = "after_sale_service_tag")]
		public AfterSaleServiceTag AfterSaleServiceTag { get; set; }

		[DataMemberAttribute(Name = "logistics_info")]
		public LogisticsInfo LogisticsInfo { get; set; }

		[DataMemberAttribute(Name = "after_sale_shop_remarks")]
		public List<AfterSaleShopRemarksItem> AfterSaleShopRemarks { get; set; }

		[DataMemberAttribute(Name = "price_protection_detail")]
		public PriceProtectionDetail PriceProtectionDetail { get; set; }

	}
}
