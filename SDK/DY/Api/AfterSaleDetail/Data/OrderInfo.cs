using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class OrderInfo
	{
		[DataMemberAttribute(Name = "shop_order_id")]
		public long? ShopOrderId { get; set; }

		[DataMemberAttribute(Name = "sku_order_infos")]
		public List<SkuOrderInfosItem> SkuOrderInfos { get; set; }

	}
}
