using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class OrderItem
	{
		[DataMemberAttribute(Name = "tracking_no")]
		public string TrackingNo { get; set; }

		[DataMemberAttribute(Name = "company_name")]
		public string CompanyName { get; set; }

		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "logistics_time")]
		public long? LogisticsTime { get; set; }

		[DataMemberAttribute(Name = "logistics_state")]
		public long? LogisticsState { get; set; }

		[DataMemberAttribute(Name = "value_added_services")]
		public List<ValueAddedServicesItem> ValueAddedServices { get; set; }

	}
}
