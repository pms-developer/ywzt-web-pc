using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleDetail.Data
{
	public class Town
	{
		[DataMemberAttribute(Name = "id")]
		public string Id { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

	}
}
