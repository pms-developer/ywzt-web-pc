using System;
using Dop.Core;
using Dop.Api.SmsSignDelete.Param;

namespace Dop.Api.SmsSignDelete
{
	public class SmsSignDeleteRequest : DoudianOpApiRequest<SmsSignDeleteParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/sign/delete";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsSignDeleteResponse);
		}

		public  SmsSignDeleteParam BuildParam()
		{
			return Param ?? (Param = new SmsSignDeleteParam());
		}

	}
}
