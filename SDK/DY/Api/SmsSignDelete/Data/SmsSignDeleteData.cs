using System.Runtime.Serialization;

namespace Dop.Api.SmsSignDelete.Data
{
	public class SmsSignDeleteData
	{
		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

	}
}
