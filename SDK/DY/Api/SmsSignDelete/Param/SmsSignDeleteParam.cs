using System.Runtime.Serialization;

namespace Dop.Api.SmsSignDelete.Param
{
	public class SmsSignDeleteParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "sign")]
		public string Sign { get; set; }

	}
}
