using System.Runtime.Serialization;

namespace Dop.Api.ProductCategoryDimList.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "level")]
		public int? Level { get; set; }

		[DataMemberAttribute(Name = "parent_id")]
		public long? ParentId { get; set; }

		[DataMemberAttribute(Name = "is_leaf")]
		public int? IsLeaf { get; set; }

		[DataMemberAttribute(Name = "vertical_category_new")]
		public string VerticalCategoryNew { get; set; }

		[DataMemberAttribute(Name = "vertical_category_code_new")]
		public string VerticalCategoryCodeNew { get; set; }

	}
}
