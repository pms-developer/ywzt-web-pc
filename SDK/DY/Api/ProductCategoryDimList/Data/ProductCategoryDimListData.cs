using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductCategoryDimList.Data
{
	public class ProductCategoryDimListData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
