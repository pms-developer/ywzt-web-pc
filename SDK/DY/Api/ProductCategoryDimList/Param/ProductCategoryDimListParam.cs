using System.Runtime.Serialization;

namespace Dop.Api.ProductCategoryDimList.Param
{
	public class ProductCategoryDimListParam
	{
		[DataMemberAttribute(Name = "level")]
		public int? Level { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "parent_id")]
		public long? ParentId { get; set; }

	}
}
