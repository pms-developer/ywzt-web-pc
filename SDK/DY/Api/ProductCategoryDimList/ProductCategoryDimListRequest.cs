using System;
using Dop.Core;
using Dop.Api.ProductCategoryDimList.Param;

namespace Dop.Api.ProductCategoryDimList
{
	public class ProductCategoryDimListRequest : DoudianOpApiRequest<ProductCategoryDimListParam>
	{
		public override string GetUrlPath()
		{
			return "/product/CategoryDimList";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductCategoryDimListResponse);
		}

		public  ProductCategoryDimListParam BuildParam()
		{
			return Param ?? (Param = new ProductCategoryDimListParam());
		}

	}
}
