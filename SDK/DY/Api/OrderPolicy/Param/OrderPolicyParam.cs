using System.Runtime.Serialization;

namespace Dop.Api.OrderPolicy.Param
{
	public class OrderPolicyParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "ins_product_id")]
		public string InsProductId { get; set; }

	}
}
