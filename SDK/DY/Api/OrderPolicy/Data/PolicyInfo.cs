using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderPolicy.Data
{
	public class PolicyInfo
	{
		[DataMemberAttribute(Name = "ins_policy_no")]
		public string InsPolicyNo { get; set; }

		[DataMemberAttribute(Name = "amount")]
		public long? Amount { get; set; }

		[DataMemberAttribute(Name = "premium")]
		public long? Premium { get; set; }

		[DataMemberAttribute(Name = "user_premium")]
		public long? UserPremium { get; set; }

		[DataMemberAttribute(Name = "merchant_premium")]
		public long? MerchantPremium { get; set; }

		[DataMemberAttribute(Name = "platform_premium")]
		public long? PlatformPremium { get; set; }

		[DataMemberAttribute(Name = "status")]
		public int? Status { get; set; }

		[DataMemberAttribute(Name = "claim_status")]
		public int? ClaimStatus { get; set; }

		[DataMemberAttribute(Name = "appeal_status")]
		public int? AppealStatus { get; set; }

		[DataMemberAttribute(Name = "goods_info_list")]
		public List<GoodsInfoListItem> GoodsInfoList { get; set; }

		[DataMemberAttribute(Name = "ins_ensured_time_begin")]
		public long? InsEnsuredTimeBegin { get; set; }

		[DataMemberAttribute(Name = "ins_ensured_time_end")]
		public long? InsEnsuredTimeEnd { get; set; }

		[DataMemberAttribute(Name = "is_allow_appeal")]
		public bool? IsAllowAppeal { get; set; }

		[DataMemberAttribute(Name = "refused_msg")]
		public string RefusedMsg { get; set; }

		[DataMemberAttribute(Name = "ins_hotline")]
		public string InsHotline { get; set; }

		[DataMemberAttribute(Name = "payer_type")]
		public int? PayerType { get; set; }

	}
}
