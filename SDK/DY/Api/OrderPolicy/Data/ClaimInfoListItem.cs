using System.Runtime.Serialization;

namespace Dop.Api.OrderPolicy.Data
{
	public class ClaimInfoListItem
	{
		[DataMemberAttribute(Name = "status")]
		public int? Status { get; set; }

		[DataMemberAttribute(Name = "amount")]
		public long? Amount { get; set; }

		[DataMemberAttribute(Name = "premium")]
		public long? Premium { get; set; }

		[DataMemberAttribute(Name = "claim_time")]
		public long? ClaimTime { get; set; }

		[DataMemberAttribute(Name = "ins_claim_no")]
		public string InsClaimNo { get; set; }

		[DataMemberAttribute(Name = "claim_msg")]
		public string ClaimMsg { get; set; }

		[DataMemberAttribute(Name = "refused_msg")]
		public string RefusedMsg { get; set; }

		[DataMemberAttribute(Name = "agg_claim_status")]
		public long? AggClaimStatus { get; set; }

		[DataMemberAttribute(Name = "claim_applied_times")]
		public int? ClaimAppliedTimes { get; set; }

	}
}
