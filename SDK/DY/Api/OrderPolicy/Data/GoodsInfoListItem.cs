using System.Runtime.Serialization;

namespace Dop.Api.OrderPolicy.Data
{
	public class GoodsInfoListItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public string CategoryId { get; set; }

		[DataMemberAttribute(Name = "show_page_url")]
		public string ShowPageUrl { get; set; }

		[DataMemberAttribute(Name = "count")]
		public int? Count { get; set; }

	}
}
