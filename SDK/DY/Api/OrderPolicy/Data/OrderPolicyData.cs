using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderPolicy.Data
{
	public class OrderPolicyData
	{
		[DataMemberAttribute(Name = "policy_info")]
		public PolicyInfo PolicyInfo { get; set; }

		[DataMemberAttribute(Name = "claim_info_list")]
		public List<ClaimInfoListItem> ClaimInfoList { get; set; }

	}
}
