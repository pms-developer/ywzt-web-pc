using System;
using Dop.Core;
using Dop.Api.OrderPolicy.Param;

namespace Dop.Api.OrderPolicy
{
	public class OrderPolicyRequest : DoudianOpApiRequest<OrderPolicyParam>
	{
		public override string GetUrlPath()
		{
			return "/order/policy";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderPolicyResponse);
		}

		public  OrderPolicyParam BuildParam()
		{
			return Param ?? (Param = new OrderPolicyParam());
		}

	}
}
