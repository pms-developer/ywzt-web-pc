using Dop.Core;
using Dop.Api.LogisticsGetOutRange.Data;

namespace Dop.Api.LogisticsGetOutRange
{
	public class LogisticsGetOutRangeResponse : DoudianOpApiResponse<LogisticsGetOutRangeData>
	{
	}
}
