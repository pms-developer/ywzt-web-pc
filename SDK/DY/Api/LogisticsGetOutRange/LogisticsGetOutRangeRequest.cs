using System;
using Dop.Core;
using Dop.Api.LogisticsGetOutRange.Param;

namespace Dop.Api.LogisticsGetOutRange
{
	public class LogisticsGetOutRangeRequest : DoudianOpApiRequest<LogisticsGetOutRangeParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/getOutRange";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsGetOutRangeResponse);
		}

		public  LogisticsGetOutRangeParam BuildParam()
		{
			return Param ?? (Param = new LogisticsGetOutRangeParam());
		}

	}
}
