using System.Runtime.Serialization;

namespace Dop.Api.LogisticsGetOutRange.Param
{
	public class ServiceListItem
	{
		[DataMemberAttribute(Name = "service_code")]
		public string ServiceCode { get; set; }

		[DataMemberAttribute(Name = "service_value")]
		public string ServiceValue { get; set; }

	}
}
