using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsGetOutRange.Param
{
	public class LogisticsGetOutRangeParam
	{
		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "sender_address")]
		public SenderAddress SenderAddress { get; set; }

		[DataMemberAttribute(Name = "receiver_address")]
		public ReceiverAddress ReceiverAddress { get; set; }

		[DataMemberAttribute(Name = "type")]
		public int? Type { get; set; }

		[DataMemberAttribute(Name = "service_list")]
		public List<ServiceListItem> ServiceList { get; set; }

		[DataMemberAttribute(Name = "product_type")]
		public string ProductType { get; set; }

		[DataMemberAttribute(Name = "delivery_req")]
		public DeliveryReq DeliveryReq { get; set; }

	}
}
