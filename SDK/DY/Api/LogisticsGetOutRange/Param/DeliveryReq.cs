using System.Runtime.Serialization;

namespace Dop.Api.LogisticsGetOutRange.Param
{
	public class DeliveryReq
	{
		[DataMemberAttribute(Name = "is_center_delivery")]
		public bool? IsCenterDelivery { get; set; }

		[DataMemberAttribute(Name = "is_pickup_self")]
		public bool? IsPickupSelf { get; set; }

	}
}
