using System.Runtime.Serialization;

namespace Dop.Api.LogisticsGetOutRange.Data
{
	public class LogisticsGetOutRangeData
	{
		[DataMemberAttribute(Name = "is_out_range")]
		public bool? IsOutRange { get; set; }

		[DataMemberAttribute(Name = "out_range_reason")]
		public string OutRangeReason { get; set; }

	}
}
