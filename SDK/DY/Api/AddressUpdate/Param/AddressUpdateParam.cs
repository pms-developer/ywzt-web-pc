using System.Runtime.Serialization;

namespace Dop.Api.AddressUpdate.Param
{
	public class AddressUpdateParam
	{
		[DataMemberAttribute(Name = "address")]
		public Address Address { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

	}
}
