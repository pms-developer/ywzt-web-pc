using System.Runtime.Serialization;

namespace Dop.Api.AddressUpdate.Param
{
	public class Address
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "user_name")]
		public string UserName { get; set; }

		[DataMemberAttribute(Name = "mobile")]
		public string Mobile { get; set; }

		[DataMemberAttribute(Name = "province_id")]
		public long? ProvinceId { get; set; }

		[DataMemberAttribute(Name = "city_id")]
		public long? CityId { get; set; }

		[DataMemberAttribute(Name = "town_id")]
		public long? TownId { get; set; }

		[DataMemberAttribute(Name = "detail")]
		public string Detail { get; set; }

		[DataMemberAttribute(Name = "street_id")]
		public long? StreetId { get; set; }

		[DataMemberAttribute(Name = "link_type")]
		public int? LinkType { get; set; }

		[DataMemberAttribute(Name = "fixed_phone")]
		public string FixedPhone { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

	}
}
