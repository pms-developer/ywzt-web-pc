using System;
using Dop.Core;
using Dop.Api.AddressUpdate.Param;

namespace Dop.Api.AddressUpdate
{
	public class AddressUpdateRequest : DoudianOpApiRequest<AddressUpdateParam>
	{
		public override string GetUrlPath()
		{
			return "/address/update";
		}

		public override Type GetResponseType()
		{
			return typeof(AddressUpdateResponse);
		}

		public  AddressUpdateParam BuildParam()
		{
			return Param ?? (Param = new AddressUpdateParam());
		}

	}
}
