using Dop.Core;
using Dop.Api.AddressUpdate.Data;

namespace Dop.Api.AddressUpdate
{
	public class AddressUpdateResponse : DoudianOpApiResponse<AddressUpdateData>
	{
	}
}
