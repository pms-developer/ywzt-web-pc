using System.Runtime.Serialization;

namespace Dop.Api.OrderGetShopAccountItem.Param
{
	public class OrderGetShopAccountItemParam
	{
		[DataMemberAttribute(Name = "start_time")]
		public string StartTime { get; set; }

		[DataMemberAttribute(Name = "end_time")]
		public string EndTime { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "size")]
		public int? Size { get; set; }

		[DataMemberAttribute(Name = "account_type")]
		public int? AccountType { get; set; }

		[DataMemberAttribute(Name = "biz_type")]
		public int? BizType { get; set; }

		[DataMemberAttribute(Name = "time_type")]
		public int? TimeType { get; set; }

		[DataMemberAttribute(Name = "start_index")]
		public string StartIndex { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public string ProductId { get; set; }

	}
}
