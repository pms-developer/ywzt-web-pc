using System;
using Dop.Core;
using Dop.Api.OrderGetShopAccountItem.Param;

namespace Dop.Api.OrderGetShopAccountItem
{
	public class OrderGetShopAccountItemRequest : DoudianOpApiRequest<OrderGetShopAccountItemParam>
	{
		public override string GetUrlPath()
		{
			return "/order/getShopAccountItem";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderGetShopAccountItemResponse);
		}

		public  OrderGetShopAccountItemParam BuildParam()
		{
			return Param ?? (Param = new OrderGetShopAccountItemParam());
		}

	}
}
