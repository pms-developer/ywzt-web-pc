using Dop.Core;
using Dop.Api.OrderGetShopAccountItem.Data;

namespace Dop.Api.OrderGetShopAccountItem
{
	public class OrderGetShopAccountItemResponse : DoudianOpApiResponse<OrderGetShopAccountItemData>
	{
	}
}
