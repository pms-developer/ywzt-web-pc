using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderGetShopAccountItem.Data
{
	public class OrderGetShopAccountItemData
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "code_msg")]
		public string CodeMsg { get; set; }

		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

		[DataMemberAttribute(Name = "next_start_index")]
		public string NextStartIndex { get; set; }

		[DataMemberAttribute(Name = "next_start_time")]
		public string NextStartTime { get; set; }

		[DataMemberAttribute(Name = "size")]
		public int? Size { get; set; }

		[DataMemberAttribute(Name = "data_size")]
		public int? DataSize { get; set; }

		[DataMemberAttribute(Name = "is_end")]
		public int? IsEnd { get; set; }

	}
}
