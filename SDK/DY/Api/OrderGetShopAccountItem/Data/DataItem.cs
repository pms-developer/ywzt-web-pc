using System.Runtime.Serialization;

namespace Dop.Api.OrderGetShopAccountItem.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "bill_time")]
		public string BillTime { get; set; }

		[DataMemberAttribute(Name = "fund_flow")]
		public int? FundFlow { get; set; }

		[DataMemberAttribute(Name = "fund_flow_desc")]
		public string FundFlowDesc { get; set; }

		[DataMemberAttribute(Name = "account_amount")]
		public long? AccountAmount { get; set; }

		[DataMemberAttribute(Name = "account_bill_desc")]
		public string AccountBillDesc { get; set; }

		[DataMemberAttribute(Name = "account_bill_desc_tag")]
		public int? AccountBillDescTag { get; set; }

		[DataMemberAttribute(Name = "biz_type")]
		public int? BizType { get; set; }

		[DataMemberAttribute(Name = "biz_type_desc")]
		public string BizTypeDesc { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "shop_order_id")]
		public string ShopOrderId { get; set; }

		[DataMemberAttribute(Name = "after_sale_service_no")]
		public string AfterSaleServiceNo { get; set; }

		[DataMemberAttribute(Name = "business_order_create_time")]
		public string BusinessOrderCreateTime { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public string ProductId { get; set; }

		[DataMemberAttribute(Name = "pay_amount")]
		public long? PayAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_amount")]
		public long? PromotionAmount { get; set; }

		[DataMemberAttribute(Name = "refund_amount")]
		public long? RefundAmount { get; set; }

		[DataMemberAttribute(Name = "platform_service_fee")]
		public long? PlatformServiceFee { get; set; }

		[DataMemberAttribute(Name = "commission")]
		public long? Commission { get; set; }

		[DataMemberAttribute(Name = "channel_fee")]
		public long? ChannelFee { get; set; }

		[DataMemberAttribute(Name = "colonel_service_fee")]
		public long? ColonelServiceFee { get; set; }

		[DataMemberAttribute(Name = "account_type")]
		public int? AccountType { get; set; }

		[DataMemberAttribute(Name = "account_type_desc")]
		public string AccountTypeDesc { get; set; }

		[DataMemberAttribute(Name = "author_coupon_subsidy")]
		public long? AuthorCouponSubsidy { get; set; }

		[DataMemberAttribute(Name = "post_amount")]
		public long? PostAmount { get; set; }

		[DataMemberAttribute(Name = "account_trade_no")]
		public string AccountTradeNo { get; set; }

		[DataMemberAttribute(Name = "order_type")]
		public int? OrderType { get; set; }

		[DataMemberAttribute(Name = "order_type_desc")]
		public string OrderTypeDesc { get; set; }

		[DataMemberAttribute(Name = "actual_zt_pay_promotion")]
		public long? ActualZtPayPromotion { get; set; }

		[DataMemberAttribute(Name = "actual_zr_pay_promotion")]
		public long? ActualZrPayPromotion { get; set; }

		[DataMemberAttribute(Name = "channel_promotion_fee")]
		public long? ChannelPromotionFee { get; set; }

		[DataMemberAttribute(Name = "other_sharing_amount")]
		public long? OtherSharingAmount { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "shop_id")]
		public string ShopId { get; set; }

	}
}
