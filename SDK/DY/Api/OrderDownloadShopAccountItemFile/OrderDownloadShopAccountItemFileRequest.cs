using System;
using Dop.Core;
using Dop.Api.OrderDownloadShopAccountItemFile.Param;

namespace Dop.Api.OrderDownloadShopAccountItemFile
{
	public class OrderDownloadShopAccountItemFileRequest : DoudianOpApiRequest<OrderDownloadShopAccountItemFileParam>
	{
		public override string GetUrlPath()
		{
			return "/order/downloadShopAccountItemFile";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderDownloadShopAccountItemFileResponse);
		}

		public  OrderDownloadShopAccountItemFileParam BuildParam()
		{
			return Param ?? (Param = new OrderDownloadShopAccountItemFileParam());
		}

	}
}
