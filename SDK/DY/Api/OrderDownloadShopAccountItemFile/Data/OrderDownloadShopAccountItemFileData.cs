using System.Runtime.Serialization;

namespace Dop.Api.OrderDownloadShopAccountItemFile.Data
{
	public class OrderDownloadShopAccountItemFileData
	{
		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "code_msg")]
		public string CodeMsg { get; set; }

	}
}
