using System.Runtime.Serialization;

namespace Dop.Api.OrderDownloadShopAccountItemFile.Param
{
	public class OrderDownloadShopAccountItemFileParam
	{
		[DataMemberAttribute(Name = "download_id")]
		public string DownloadId { get; set; }

	}
}
