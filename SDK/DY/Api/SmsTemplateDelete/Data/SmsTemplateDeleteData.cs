using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateDelete.Data
{
	public class SmsTemplateDeleteData
	{
		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

	}
}
