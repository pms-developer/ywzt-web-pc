using Dop.Core;
using Dop.Api.SmsTemplateDelete.Data;

namespace Dop.Api.SmsTemplateDelete
{
	public class SmsTemplateDeleteResponse : DoudianOpApiResponse<SmsTemplateDeleteData>
	{
	}
}
