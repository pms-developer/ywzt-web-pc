using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateDelete.Param
{
	public class SmsTemplateDeleteParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "template_id")]
		public string TemplateId { get; set; }

	}
}
