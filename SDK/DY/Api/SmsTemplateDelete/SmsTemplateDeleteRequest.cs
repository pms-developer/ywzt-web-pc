using System;
using Dop.Core;
using Dop.Api.SmsTemplateDelete.Param;

namespace Dop.Api.SmsTemplateDelete
{
	public class SmsTemplateDeleteRequest : DoudianOpApiRequest<SmsTemplateDeleteParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/template/delete";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsTemplateDeleteResponse);
		}

		public  SmsTemplateDeleteParam BuildParam()
		{
			return Param ?? (Param = new SmsTemplateDeleteParam());
		}

	}
}
