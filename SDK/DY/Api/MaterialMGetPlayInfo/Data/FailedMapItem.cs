using System.Runtime.Serialization;

namespace Dop.Api.MaterialMGetPlayInfo.Data
{
	public class FailedMapItem
	{
		[DataMemberAttribute(Name = "err_code")]
		public int? ErrCode { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

	}
}
