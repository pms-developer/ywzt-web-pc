using System.Runtime.Serialization;

namespace Dop.Api.MaterialMGetPlayInfo.Data
{
	public class SuccessMapItem
	{
		[DataMemberAttribute(Name = "vid")]
		public string Vid { get; set; }

		[DataMemberAttribute(Name = "height")]
		public int? Height { get; set; }

		[DataMemberAttribute(Name = "width")]
		public int? Width { get; set; }

		[DataMemberAttribute(Name = "format")]
		public string Format { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "uri")]
		public string Uri { get; set; }

		[DataMemberAttribute(Name = "duration")]
		public double? Duration { get; set; }

		[DataMemberAttribute(Name = "video_cover_url")]
		public string VideoCoverUrl { get; set; }

		[DataMemberAttribute(Name = "main_url")]
		public string MainUrl { get; set; }

		[DataMemberAttribute(Name = "main_url_expire")]
		public long? MainUrlExpire { get; set; }

	}
}
