using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialMGetPlayInfo.Data
{
	public class MaterialMGetPlayInfoData
	{
		[DataMemberAttribute(Name = "success_map")]
		public Dictionary<string,SuccessMapItem> SuccessMap { get; set; }

		[DataMemberAttribute(Name = "failed_map")]
		public Dictionary<string,FailedMapItem> FailedMap { get; set; }

	}
}
