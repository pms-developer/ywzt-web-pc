using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialMGetPlayInfo.Param
{
	public class MaterialMGetPlayInfoParam
	{
		[DataMemberAttribute(Name = "vid_list")]
		public List<string> VidList { get; set; }

	}
}
