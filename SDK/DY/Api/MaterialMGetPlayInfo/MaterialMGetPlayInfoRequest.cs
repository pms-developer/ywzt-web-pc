using System;
using Dop.Core;
using Dop.Api.MaterialMGetPlayInfo.Param;

namespace Dop.Api.MaterialMGetPlayInfo
{
	public class MaterialMGetPlayInfoRequest : DoudianOpApiRequest<MaterialMGetPlayInfoParam>
	{
		public override string GetUrlPath()
		{
			return "/material/mGetPlayInfo";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialMGetPlayInfoResponse);
		}

		public  MaterialMGetPlayInfoParam BuildParam()
		{
			return Param ?? (Param = new MaterialMGetPlayInfoParam());
		}

	}
}
