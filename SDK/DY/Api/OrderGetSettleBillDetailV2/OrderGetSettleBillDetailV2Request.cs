using System;
using Dop.Core;
using Dop.Api.OrderGetSettleBillDetailV2.Param;

namespace Dop.Api.OrderGetSettleBillDetailV2
{
	public class OrderGetSettleBillDetailV2Request : DoudianOpApiRequest<OrderGetSettleBillDetailV2Param>
	{
		public override string GetUrlPath()
		{
			return "/order/getSettleBillDetailV2";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderGetSettleBillDetailV2Response);
		}

		public  OrderGetSettleBillDetailV2Param BuildParam()
		{
			return Param ?? (Param = new OrderGetSettleBillDetailV2Param());
		}

	}
}
