using Dop.Core;
using Dop.Api.OrderGetSettleBillDetailV2.Data;

namespace Dop.Api.OrderGetSettleBillDetailV2
{
	public class OrderGetSettleBillDetailV2Response : DoudianOpApiResponse<OrderGetSettleBillDetailV2Data>
	{
	}
}
