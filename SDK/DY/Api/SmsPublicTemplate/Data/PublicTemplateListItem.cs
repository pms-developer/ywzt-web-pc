using System.Runtime.Serialization;

namespace Dop.Api.SmsPublicTemplate.Data
{
	public class PublicTemplateListItem
	{
		[DataMemberAttribute(Name = "template_id")]
		public string TemplateId { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "template_content")]
		public string TemplateContent { get; set; }

		[DataMemberAttribute(Name = "channel_type")]
		public string ChannelType { get; set; }

	}
}
