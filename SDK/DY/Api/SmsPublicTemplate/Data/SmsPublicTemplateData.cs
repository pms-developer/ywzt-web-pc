using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SmsPublicTemplate.Data
{
	public class SmsPublicTemplateData
	{
		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "public_template_list")]
		public List<PublicTemplateListItem> PublicTemplateList { get; set; }

	}
}
