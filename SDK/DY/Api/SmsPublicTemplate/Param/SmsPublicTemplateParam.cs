using System.Runtime.Serialization;

namespace Dop.Api.SmsPublicTemplate.Param
{
	public class SmsPublicTemplateParam
	{
		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "template_id")]
		public string TemplateId { get; set; }

	}
}
