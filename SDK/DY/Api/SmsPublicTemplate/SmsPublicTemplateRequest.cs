using System;
using Dop.Core;
using Dop.Api.SmsPublicTemplate.Param;

namespace Dop.Api.SmsPublicTemplate
{
	public class SmsPublicTemplateRequest : DoudianOpApiRequest<SmsPublicTemplateParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/public/template";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsPublicTemplateResponse);
		}

		public  SmsPublicTemplateParam BuildParam()
		{
			return Param ?? (Param = new SmsPublicTemplateParam());
		}

	}
}
