using Dop.Core;
using Dop.Api.OrderGetSettleBillDetail.Data;

namespace Dop.Api.OrderGetSettleBillDetail
{
	public class OrderGetSettleBillDetailResponse : DoudianOpApiResponse<OrderGetSettleBillDetailData>
	{
	}
}
