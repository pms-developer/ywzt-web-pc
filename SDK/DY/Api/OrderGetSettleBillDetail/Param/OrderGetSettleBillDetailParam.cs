using System.Runtime.Serialization;

namespace Dop.Api.OrderGetSettleBillDetail.Param
{
	public class OrderGetSettleBillDetailParam
	{
		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "start_time")]
		public string StartTime { get; set; }

		[DataMemberAttribute(Name = "end_time")]
		public string EndTime { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public string ProductId { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public string PayType { get; set; }

		[DataMemberAttribute(Name = "flow_type")]
		public string FlowType { get; set; }

		[DataMemberAttribute(Name = "time_type")]
		public string TimeType { get; set; }

	}
}
