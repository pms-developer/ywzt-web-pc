using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderGetSettleBillDetail.Data
{
	public class OrderGetSettleBillDetailData
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "code_msg")]
		public string CodeMsg { get; set; }

		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

		[DataMemberAttribute(Name = "total_cnt")]
		public long? TotalCnt { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

	}
}
