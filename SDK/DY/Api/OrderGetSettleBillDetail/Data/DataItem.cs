using System.Runtime.Serialization;

namespace Dop.Api.OrderGetSettleBillDetail.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "shop_order_id")]
		public string ShopOrderId { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "order_time")]
		public string OrderTime { get; set; }

		[DataMemberAttribute(Name = "shop_id")]
		public long? ShopId { get; set; }

		[DataMemberAttribute(Name = "settle_time")]
		public string SettleTime { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public string ProductId { get; set; }

		[DataMemberAttribute(Name = "goods_count")]
		public int? GoodsCount { get; set; }

		[DataMemberAttribute(Name = "trade_type_desc")]
		public string TradeTypeDesc { get; set; }

		[DataMemberAttribute(Name = "pay_type_desc")]
		public string PayTypeDesc { get; set; }

		[DataMemberAttribute(Name = "request_no")]
		public string RequestNo { get; set; }

		[DataMemberAttribute(Name = "flow_type_desc")]
		public string FlowTypeDesc { get; set; }

		[DataMemberAttribute(Name = "phase_order_no")]
		public string PhaseOrderNo { get; set; }

		[DataMemberAttribute(Name = "phase_cnt")]
		public int? PhaseCnt { get; set; }

		[DataMemberAttribute(Name = "phase_id")]
		public int? PhaseId { get; set; }

		[DataMemberAttribute(Name = "total_income")]
		public long? TotalIncome { get; set; }

		[DataMemberAttribute(Name = "total_outcome")]
		public long? TotalOutcome { get; set; }

		[DataMemberAttribute(Name = "profit")]
		public long? Profit { get; set; }

		[DataMemberAttribute(Name = "settle_amount")]
		public long? SettleAmount { get; set; }

		[DataMemberAttribute(Name = "actual_subsidy_amount")]
		public long? ActualSubsidyAmount { get; set; }

		[DataMemberAttribute(Name = "total_amount")]
		public long? TotalAmount { get; set; }

		[DataMemberAttribute(Name = "total_goods_amount")]
		public long? TotalGoodsAmount { get; set; }

		[DataMemberAttribute(Name = "post_amount")]
		public long? PostAmount { get; set; }

		[DataMemberAttribute(Name = "real_pay_amount")]
		public long? RealPayAmount { get; set; }

		[DataMemberAttribute(Name = "settled_pay_amount")]
		public long? SettledPayAmount { get; set; }

		[DataMemberAttribute(Name = "platform_coupon")]
		public long? PlatformCoupon { get; set; }

		[DataMemberAttribute(Name = "author_coupon")]
		public long? AuthorCoupon { get; set; }

		[DataMemberAttribute(Name = "pay_promotion")]
		public long? PayPromotion { get; set; }

		[DataMemberAttribute(Name = "actual_platform_coupon")]
		public long? ActualPlatformCoupon { get; set; }

		[DataMemberAttribute(Name = "actual_author_coupon")]
		public long? ActualAuthorCoupon { get; set; }

		[DataMemberAttribute(Name = "actual_pay_promotion")]
		public long? ActualPayPromotion { get; set; }

		[DataMemberAttribute(Name = "shop_coupon")]
		public long? ShopCoupon { get; set; }

		[DataMemberAttribute(Name = "platform_service_fee")]
		public long? PlatformServiceFee { get; set; }

		[DataMemberAttribute(Name = "refund")]
		public long? Refund { get; set; }

		[DataMemberAttribute(Name = "commission")]
		public long? Commission { get; set; }

		[DataMemberAttribute(Name = "good_learn_channel_fee")]
		public long? GoodLearnChannelFee { get; set; }

		[DataMemberAttribute(Name = "colonel_service_fee")]
		public long? ColonelServiceFee { get; set; }

		[DataMemberAttribute(Name = "shop_refund_loss")]
		public long? ShopRefundLoss { get; set; }

		[DataMemberAttribute(Name = "trade_type")]
		public string TradeType { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public string PayType { get; set; }

		[DataMemberAttribute(Name = "flow_type")]
		public string FlowType { get; set; }

	}
}
