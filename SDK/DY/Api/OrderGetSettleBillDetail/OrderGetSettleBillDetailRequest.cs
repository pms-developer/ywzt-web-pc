using System;
using Dop.Core;
using Dop.Api.OrderGetSettleBillDetail.Param;

namespace Dop.Api.OrderGetSettleBillDetail
{
	public class OrderGetSettleBillDetailRequest : DoudianOpApiRequest<OrderGetSettleBillDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/order/getSettleBillDetail";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderGetSettleBillDetailResponse);
		}

		public  OrderGetSettleBillDetailParam BuildParam()
		{
			return Param ?? (Param = new OrderGetSettleBillDetailParam());
		}

	}
}
