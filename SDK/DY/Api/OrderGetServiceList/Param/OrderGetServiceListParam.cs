using System.Runtime.Serialization;

namespace Dop.Api.OrderGetServiceList.Param
{
	public class OrderGetServiceListParam
	{
		[DataMemberAttribute(Name = "start_time")]
		public long? StartTime { get; set; }

		[DataMemberAttribute(Name = "end_time")]
		public long? EndTime { get; set; }

		[DataMemberAttribute(Name = "status")]
		public int? Status { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public long? OrderId { get; set; }

		[DataMemberAttribute(Name = "service_id")]
		public long? ServiceId { get; set; }

		[DataMemberAttribute(Name = "order_by")]
		public string OrderBy { get; set; }

		[DataMemberAttribute(Name = "page")]
		public int? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public int? Size { get; set; }

		[DataMemberAttribute(Name = "order")]
		public string Order { get; set; }

	}
}
