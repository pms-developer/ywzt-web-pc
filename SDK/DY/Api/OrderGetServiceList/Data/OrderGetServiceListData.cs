using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderGetServiceList.Data
{
	public class OrderGetServiceListData
	{
		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "list")]
		public List<ListItem> List { get; set; }

	}
}
