using System.Runtime.Serialization;

namespace Dop.Api.OrderGetServiceList.Data
{
	public class ListItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public long? OrderId { get; set; }

		[DataMemberAttribute(Name = "operate_status")]
		public int? OperateStatus { get; set; }

		[DataMemberAttribute(Name = "detail")]
		public string Detail { get; set; }

		[DataMemberAttribute(Name = "reply")]
		public string Reply { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "service_type")]
		public long? ServiceType { get; set; }

		[DataMemberAttribute(Name = "reply_time")]
		public string ReplyTime { get; set; }

		[DataMemberAttribute(Name = "operate_status_desc")]
		public string OperateStatusDesc { get; set; }

		[DataMemberAttribute(Name = "shop_id")]
		public long? ShopId { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public string UpdateTime { get; set; }

	}
}
