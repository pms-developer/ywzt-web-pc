using System;
using Dop.Core;
using Dop.Api.OrderGetServiceList.Param;

namespace Dop.Api.OrderGetServiceList
{
	public class OrderGetServiceListRequest : DoudianOpApiRequest<OrderGetServiceListParam>
	{
		public override string GetUrlPath()
		{
			return "/order/getServiceList";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderGetServiceListResponse);
		}

		public  OrderGetServiceListParam BuildParam()
		{
			return Param ?? (Param = new OrderGetServiceListParam());
		}

	}
}
