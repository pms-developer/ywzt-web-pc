using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialMoveFolderToRecycleBin.Data
{
	public class MaterialMoveFolderToRecycleBinData
	{
		[DataMemberAttribute(Name = "success_ids")]
		public List<string> SuccessIds { get; set; }

		[DataMemberAttribute(Name = "failed_map")]
		public Dictionary<long,FailedMapItem> FailedMap { get; set; }

	}
}
