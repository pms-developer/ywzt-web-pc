using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialMoveFolderToRecycleBin.Param
{
	public class MaterialMoveFolderToRecycleBinParam
	{
		[DataMemberAttribute(Name = "folder_ids")]
		public List<string> FolderIds { get; set; }

	}
}
