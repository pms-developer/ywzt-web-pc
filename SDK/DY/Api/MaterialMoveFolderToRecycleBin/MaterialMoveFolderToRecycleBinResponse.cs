using Dop.Core;
using Dop.Api.MaterialMoveFolderToRecycleBin.Data;

namespace Dop.Api.MaterialMoveFolderToRecycleBin
{
	public class MaterialMoveFolderToRecycleBinResponse : DoudianOpApiResponse<MaterialMoveFolderToRecycleBinData>
	{
	}
}
