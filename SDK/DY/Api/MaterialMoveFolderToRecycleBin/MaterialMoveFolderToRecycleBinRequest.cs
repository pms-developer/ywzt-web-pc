using System;
using Dop.Core;
using Dop.Api.MaterialMoveFolderToRecycleBin.Param;

namespace Dop.Api.MaterialMoveFolderToRecycleBin
{
	public class MaterialMoveFolderToRecycleBinRequest : DoudianOpApiRequest<MaterialMoveFolderToRecycleBinParam>
	{
		public override string GetUrlPath()
		{
			return "/material/moveFolderToRecycleBin";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialMoveFolderToRecycleBinResponse);
		}

		public  MaterialMoveFolderToRecycleBinParam BuildParam()
		{
			return Param ?? (Param = new MaterialMoveFolderToRecycleBinParam());
		}

	}
}
