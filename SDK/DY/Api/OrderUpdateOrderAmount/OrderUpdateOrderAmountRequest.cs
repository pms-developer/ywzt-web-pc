using System;
using Dop.Core;
using Dop.Api.OrderUpdateOrderAmount.Param;

namespace Dop.Api.OrderUpdateOrderAmount
{
	public class OrderUpdateOrderAmountRequest : DoudianOpApiRequest<OrderUpdateOrderAmountParam>
	{
		public override string GetUrlPath()
		{
			return "/order/updateOrderAmount";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderUpdateOrderAmountResponse);
		}

		public  OrderUpdateOrderAmountParam BuildParam()
		{
			return Param ?? (Param = new OrderUpdateOrderAmountParam());
		}

	}
}
