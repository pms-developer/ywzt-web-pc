using Dop.Core;
using Dop.Api.OrderUpdateOrderAmount.Data;

namespace Dop.Api.OrderUpdateOrderAmount
{
	public class OrderUpdateOrderAmountResponse : DoudianOpApiResponse<OrderUpdateOrderAmountData>
	{
	}
}
