using System.Runtime.Serialization;

namespace Dop.Api.OrderUpdateOrderAmount.Param
{
	public class UpdateDetailItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "discount_amount")]
		public long? DiscountAmount { get; set; }

	}
}
