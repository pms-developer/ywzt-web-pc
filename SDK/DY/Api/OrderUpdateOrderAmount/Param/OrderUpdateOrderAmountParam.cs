using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderUpdateOrderAmount.Param
{
	public class OrderUpdateOrderAmountParam
	{
		[DataMemberAttribute(Name = "pid")]
		public string Pid { get; set; }

		[DataMemberAttribute(Name = "update_detail")]
		public List<UpdateDetailItem> UpdateDetail { get; set; }

	}
}
