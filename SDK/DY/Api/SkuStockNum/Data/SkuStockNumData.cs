using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SkuStockNum.Data
{
	public class SkuStockNumData
	{
		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "out_sku_id")]
		public long? OutSkuId { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "sku_type")]
		public long? SkuType { get; set; }

		[DataMemberAttribute(Name = "stock_num")]
		public long? StockNum { get; set; }

		[DataMemberAttribute(Name = "prehold_stock_num")]
		public long? PreholdStockNum { get; set; }

		[DataMemberAttribute(Name = "prom_stock_num")]
		public long? PromStockNum { get; set; }

		[DataMemberAttribute(Name = "step_stock_num")]
		public long? StepStockNum { get; set; }

		[DataMemberAttribute(Name = "prehold_step_stock_num")]
		public long? PreholdStepStockNum { get; set; }

		[DataMemberAttribute(Name = "prom_step_stock_num")]
		public long? PromStepStockNum { get; set; }

		[DataMemberAttribute(Name = "stock_num_map")]
		public Dictionary<string,long> StockNumMap { get; set; }

		[DataMemberAttribute(Name = "prehold_stock_map")]
		public Dictionary<string,long> PreholdStockMap { get; set; }

		[DataMemberAttribute(Name = "ship_rule_map")]
		public Dictionary<string,ShipRuleMapItem> ShipRuleMap { get; set; }

		[DataMemberAttribute(Name = "normal_stock_num")]
		public long? NormalStockNum { get; set; }

		[DataMemberAttribute(Name = "channel_stock_num")]
		public long? ChannelStockNum { get; set; }

	}
}
