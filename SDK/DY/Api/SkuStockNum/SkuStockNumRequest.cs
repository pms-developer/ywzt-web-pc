using System;
using Dop.Core;
using Dop.Api.SkuStockNum.Param;

namespace Dop.Api.SkuStockNum
{
	public class SkuStockNumRequest : DoudianOpApiRequest<SkuStockNumParam>
	{
		public override string GetUrlPath()
		{
			return "/sku/stockNum";
		}

		public override Type GetResponseType()
		{
			return typeof(SkuStockNumResponse);
		}

		public  SkuStockNumParam BuildParam()
		{
			return Param ?? (Param = new SkuStockNumParam());
		}

	}
}
