using System.Runtime.Serialization;

namespace Dop.Api.SkuStockNum.Param
{
	public class SkuStockNumParam
	{
		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

	}
}
