using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuInfoBySpuId.Param
{
	public class SpuGetSpuInfoBySpuIdParam
	{
		[DataMemberAttribute(Name = "spu_id")]
		public long? SpuId { get; set; }

	}
}
