using System;
using Dop.Core;
using Dop.Api.SpuGetSpuInfoBySpuId.Param;

namespace Dop.Api.SpuGetSpuInfoBySpuId
{
	public class SpuGetSpuInfoBySpuIdRequest : DoudianOpApiRequest<SpuGetSpuInfoBySpuIdParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/getSpuInfoBySpuId";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuGetSpuInfoBySpuIdResponse);
		}

		public  SpuGetSpuInfoBySpuIdParam BuildParam()
		{
			return Param ?? (Param = new SpuGetSpuInfoBySpuIdParam());
		}

	}
}
