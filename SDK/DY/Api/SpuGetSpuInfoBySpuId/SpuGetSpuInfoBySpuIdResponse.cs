using Dop.Core;
using Dop.Api.SpuGetSpuInfoBySpuId.Data;

namespace Dop.Api.SpuGetSpuInfoBySpuId
{
	public class SpuGetSpuInfoBySpuIdResponse : DoudianOpApiResponse<SpuGetSpuInfoBySpuIdData>
	{
	}
}
