using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuInfoBySpuId.Data
{
	public class SpuGetSpuInfoBySpuIdData
	{
		[DataMemberAttribute(Name = "spu_property_infos")]
		public List<SpuPropertyInfosItem> SpuPropertyInfos { get; set; }

		[DataMemberAttribute(Name = "spu_info")]
		public SpuInfo SpuInfo { get; set; }

	}
}
