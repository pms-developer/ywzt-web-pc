using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuInfoBySpuId.Data
{
	public class SpuInfo
	{
		[DataMemberAttribute(Name = "spu_name")]
		public string SpuName { get; set; }

		[DataMemberAttribute(Name = "upc_code")]
		public string UpcCode { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

		[DataMemberAttribute(Name = "brand_id")]
		public long? BrandId { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public string UpdateTime { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "spu_id")]
		public string SpuId { get; set; }

		[DataMemberAttribute(Name = "op_status")]
		public long? OpStatus { get; set; }

		[DataMemberAttribute(Name = "audit_time")]
		public string AuditTime { get; set; }

	}
}
