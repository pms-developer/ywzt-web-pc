using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuInfoBySpuId.Data
{
	public class SpuPropertyValueInfoItem
	{
		[DataMemberAttribute(Name = "value_id")]
		public long? ValueId { get; set; }

		[DataMemberAttribute(Name = "value_name")]
		public string ValueName { get; set; }

		[DataMemberAttribute(Name = "value_alias")]
		public string ValueAlias { get; set; }

	}
}
