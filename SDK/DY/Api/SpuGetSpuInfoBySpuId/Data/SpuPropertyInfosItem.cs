using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuGetSpuInfoBySpuId.Data
{
	public class SpuPropertyInfosItem
	{
		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "property_name")]
		public string PropertyName { get; set; }

		[DataMemberAttribute(Name = "type")]
		public long? Type { get; set; }

		[DataMemberAttribute(Name = "property_alias")]
		public string PropertyAlias { get; set; }

		[DataMemberAttribute(Name = "spu_property_value_info")]
		public List<SpuPropertyValueInfoItem> SpuPropertyValueInfo { get; set; }

	}
}
