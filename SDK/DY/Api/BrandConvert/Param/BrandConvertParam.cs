using System.Runtime.Serialization;

namespace Dop.Api.BrandConvert.Param
{
	public class BrandConvertParam
	{
		[DataMemberAttribute(Name = "related_id")]
		public long? RelatedId { get; set; }

	}
}
