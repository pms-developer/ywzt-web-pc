using System.Runtime.Serialization;

namespace Dop.Api.BrandConvert.Data
{
	public class BrandConvertData
	{
		[DataMemberAttribute(Name = "brand_id")]
		public long? BrandId { get; set; }

	}
}
