using System;
using Dop.Core;
using Dop.Api.BrandConvert.Param;

namespace Dop.Api.BrandConvert
{
	public class BrandConvertRequest : DoudianOpApiRequest<BrandConvertParam>
	{
		public override string GetUrlPath()
		{
			return "/brand/convert";
		}

		public override Type GetResponseType()
		{
			return typeof(BrandConvertResponse);
		}

		public  BrandConvertParam BuildParam()
		{
			return Param ?? (Param = new BrandConvertParam());
		}

	}
}
