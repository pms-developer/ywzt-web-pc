using Dop.Core;
using Dop.Api.ProductEditBuyerLimit.Data;

namespace Dop.Api.ProductEditBuyerLimit
{
	public class ProductEditBuyerLimitResponse : DoudianOpApiResponse<ProductEditBuyerLimitData>
	{
	}
}
