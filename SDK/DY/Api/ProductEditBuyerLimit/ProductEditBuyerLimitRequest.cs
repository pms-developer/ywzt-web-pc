using System;
using Dop.Core;
using Dop.Api.ProductEditBuyerLimit.Param;

namespace Dop.Api.ProductEditBuyerLimit
{
	public class ProductEditBuyerLimitRequest : DoudianOpApiRequest<ProductEditBuyerLimitParam>
	{
		public override string GetUrlPath()
		{
			return "/product/editBuyerLimit";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductEditBuyerLimitResponse);
		}

		public  ProductEditBuyerLimitParam BuildParam()
		{
			return Param ?? (Param = new ProductEditBuyerLimitParam());
		}

	}
}
