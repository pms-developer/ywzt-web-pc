using System.Runtime.Serialization;

namespace Dop.Api.ProductEditBuyerLimit.Param
{
	public class ProductEditBuyerLimitParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "maximum_per_order")]
		public long? MaximumPerOrder { get; set; }

		[DataMemberAttribute(Name = "limit_per_buyer")]
		public long? LimitPerBuyer { get; set; }

		[DataMemberAttribute(Name = "minimum_per_order")]
		public long? MinimumPerOrder { get; set; }

	}
}
