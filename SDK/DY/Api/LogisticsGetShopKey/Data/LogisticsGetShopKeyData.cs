using System.Runtime.Serialization;

namespace Dop.Api.LogisticsGetShopKey.Data
{
	public class LogisticsGetShopKeyData
	{
		[DataMemberAttribute(Name = "key")]
		public string Key { get; set; }

	}
}
