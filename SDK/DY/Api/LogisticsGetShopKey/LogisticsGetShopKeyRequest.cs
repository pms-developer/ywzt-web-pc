using System;
using Dop.Core;
using Dop.Api.LogisticsGetShopKey.Param;

namespace Dop.Api.LogisticsGetShopKey
{
	public class LogisticsGetShopKeyRequest : DoudianOpApiRequest<LogisticsGetShopKeyParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/getShopKey";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsGetShopKeyResponse);
		}

		public  LogisticsGetShopKeyParam BuildParam()
		{
			return Param ?? (Param = new LogisticsGetShopKeyParam());
		}

	}
}
