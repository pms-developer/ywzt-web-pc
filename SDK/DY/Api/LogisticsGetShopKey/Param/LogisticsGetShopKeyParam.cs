using System.Runtime.Serialization;

namespace Dop.Api.LogisticsGetShopKey.Param
{
	public class LogisticsGetShopKeyParam
	{
		[DataMemberAttribute(Name = "cipher_text")]
		public string CipherText { get; set; }

		[DataMemberAttribute(Name = "deviceInfo")]
		public string DeviceInfo { get; set; }

	}
}
