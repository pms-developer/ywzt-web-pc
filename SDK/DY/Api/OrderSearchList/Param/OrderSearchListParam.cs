using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderSearchList.Param
{
	public class OrderSearchListParam
	{
		[DataMemberAttribute(Name = "product")]
		public string Product { get; set; }

		[DataMemberAttribute(Name = "b_type")]
		public long? BType { get; set; }

		[DataMemberAttribute(Name = "after_sale_status_desc")]
		public string AfterSaleStatusDesc { get; set; }

		[DataMemberAttribute(Name = "tracking_no")]
		public string TrackingNo { get; set; }

		[DataMemberAttribute(Name = "presell_type")]
		public long? PresellType { get; set; }

		[DataMemberAttribute(Name = "order_type")]
		public long? OrderType { get; set; }

		[DataMemberAttribute(Name = "create_time_start")]
		public long? CreateTimeStart { get; set; }

		[DataMemberAttribute(Name = "create_time_end")]
		public long? CreateTimeEnd { get; set; }

		[DataMemberAttribute(Name = "abnormal_order")]
		public long? AbnormalOrder { get; set; }

		[DataMemberAttribute(Name = "trade_type")]
		public long? TradeType { get; set; }

		[DataMemberAttribute(Name = "combine_status")]
		public List<CombineStatusItem> CombineStatus { get; set; }

		[DataMemberAttribute(Name = "update_time_start")]
		public long? UpdateTimeStart { get; set; }

		[DataMemberAttribute(Name = "update_time_end")]
		public long? UpdateTimeEnd { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "order_by")]
		public string OrderBy { get; set; }

		[DataMemberAttribute(Name = "order_asc")]
		public bool? OrderAsc { get; set; }

		[DataMemberAttribute(Name = "is_searchable")]
		public bool IsSearchable { get; set; }

	}
}
