using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Param
{
	public class CombineStatusItem
	{
		[DataMemberAttribute(Name = "order_status")]
		public string OrderStatus { get; set; }

		[DataMemberAttribute(Name = "main_status")]
		public string MainStatus { get; set; }

	}
}
