using System;
using Dop.Core;
using Dop.Api.OrderSearchList.Param;

namespace Dop.Api.OrderSearchList
{
	public class OrderSearchListRequest : DoudianOpApiRequest<OrderSearchListParam>
	{
		public override string GetUrlPath()
		{
			return "/order/searchList";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderSearchListResponse);
		}

		public  OrderSearchListParam BuildParam()
		{
			return Param ?? (Param = new OrderSearchListParam());
		}

	}
}
