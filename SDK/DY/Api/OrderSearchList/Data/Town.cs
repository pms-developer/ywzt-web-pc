using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Data
{
	public class Town
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "id")]
		public string Id { get; set; }

	}
}
