using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderSearchList.Data
{
	public class SkuOrderListItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "parent_order_id")]
		public string ParentOrderId { get; set; }

		[DataMemberAttribute(Name = "order_level")]
		public long? OrderLevel { get; set; }

		[DataMemberAttribute(Name = "biz")]
		public long? Biz { get; set; }

		[DataMemberAttribute(Name = "biz_desc")]
		public string BizDesc { get; set; }

		[DataMemberAttribute(Name = "order_type")]
		public long? OrderType { get; set; }

		[DataMemberAttribute(Name = "order_type_desc")]
		public string OrderTypeDesc { get; set; }

		[DataMemberAttribute(Name = "trade_type")]
		public long? TradeType { get; set; }

		[DataMemberAttribute(Name = "trade_type_desc")]
		public string TradeTypeDesc { get; set; }

		[DataMemberAttribute(Name = "order_status")]
		public long? OrderStatus { get; set; }

		[DataMemberAttribute(Name = "order_status_desc")]
		public string OrderStatusDesc { get; set; }

		[DataMemberAttribute(Name = "main_status")]
		public long? MainStatus { get; set; }

		[DataMemberAttribute(Name = "main_status_desc")]
		public string MainStatusDesc { get; set; }

		[DataMemberAttribute(Name = "pay_time")]
		public long? PayTime { get; set; }

		[DataMemberAttribute(Name = "order_expire_time")]
		public long? OrderExpireTime { get; set; }

		[DataMemberAttribute(Name = "finish_time")]
		public long? FinishTime { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public long? UpdateTime { get; set; }

		[DataMemberAttribute(Name = "cancel_reason")]
		public string CancelReason { get; set; }

		[DataMemberAttribute(Name = "b_type")]
		public long? BType { get; set; }

		[DataMemberAttribute(Name = "b_type_desc")]
		public string BTypeDesc { get; set; }

		[DataMemberAttribute(Name = "sub_b_type")]
		public long? SubBType { get; set; }

		[DataMemberAttribute(Name = "sub_b_type_desc")]
		public string SubBTypeDesc { get; set; }

		[DataMemberAttribute(Name = "send_pay")]
		public long? SendPay { get; set; }

		[DataMemberAttribute(Name = "send_pay_desc")]
		public string SendPayDesc { get; set; }

		[DataMemberAttribute(Name = "author_id")]
		public long? AuthorId { get; set; }

		[DataMemberAttribute(Name = "author_name")]
		public string AuthorName { get; set; }

		[DataMemberAttribute(Name = "theme_type")]
		public string ThemeType { get; set; }

		[DataMemberAttribute(Name = "theme_type_desc")]
		public string ThemeTypeDesc { get; set; }

		[DataMemberAttribute(Name = "app_id")]
		public long? AppId { get; set; }

		[DataMemberAttribute(Name = "room_id")]
		public long? RoomId { get; set; }

		[DataMemberAttribute(Name = "content_id")]
		public string ContentId { get; set; }

		[DataMemberAttribute(Name = "video_id")]
		public string VideoId { get; set; }

		[DataMemberAttribute(Name = "origin_id")]
		public string OriginId { get; set; }

		[DataMemberAttribute(Name = "cid")]
		public long? Cid { get; set; }

		[DataMemberAttribute(Name = "c_biz")]
		public long? CBiz { get; set; }

		[DataMemberAttribute(Name = "c_biz_desc")]
		public string CBizDesc { get; set; }

		[DataMemberAttribute(Name = "page_id")]
		public long? PageId { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public long? PayType { get; set; }

		[DataMemberAttribute(Name = "channel_payment_no")]
		public string ChannelPaymentNo { get; set; }

		[DataMemberAttribute(Name = "order_amount")]
		public long? OrderAmount { get; set; }

		[DataMemberAttribute(Name = "pay_amount")]
		public long? PayAmount { get; set; }

		[DataMemberAttribute(Name = "post_insurance_amount")]
		public long? PostInsuranceAmount { get; set; }

		[DataMemberAttribute(Name = "modify_amount")]
		public long? ModifyAmount { get; set; }

		[DataMemberAttribute(Name = "modify_post_amount")]
		public long? ModifyPostAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_amount")]
		public long? PromotionAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_shop_amount")]
		public long? PromotionShopAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_platform_amount")]
		public long? PromotionPlatformAmount { get; set; }

		[DataMemberAttribute(Name = "shop_cost_amount")]
		public long? ShopCostAmount { get; set; }

		[DataMemberAttribute(Name = "platform_cost_amount")]
		public long? PlatformCostAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_talent_amount")]
		public long? PromotionTalentAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_pay_amount")]
		public long? PromotionPayAmount { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "post_tel")]
		public string PostTel { get; set; }

		[DataMemberAttribute(Name = "encrypt_post_tel")]
		public string EncryptPostTel { get; set; }

		[DataMemberAttribute(Name = "post_receiver")]
		public string PostReceiver { get; set; }

		[DataMemberAttribute(Name = "encrypt_post_receiver")]
		public string EncryptPostReceiver { get; set; }

		[DataMemberAttribute(Name = "post_addr")]
		public PostAddr PostAddr { get; set; }

		[DataMemberAttribute(Name = "exp_ship_time")]
		public long? ExpShipTime { get; set; }

		[DataMemberAttribute(Name = "ship_time")]
		public long? ShipTime { get; set; }

		[DataMemberAttribute(Name = "logistics_receipt_time")]
		public long? LogisticsReceiptTime { get; set; }

		[DataMemberAttribute(Name = "confirm_receipt_time")]
		public long? ConfirmReceiptTime { get; set; }

		[DataMemberAttribute(Name = "goods_type")]
		public long? GoodsType { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "spec")]
		public List<SpecItem> Spec { get; set; }

		[DataMemberAttribute(Name = "first_cid")]
		public long? FirstCid { get; set; }

		[DataMemberAttribute(Name = "second_cid")]
		public long? SecondCid { get; set; }

		[DataMemberAttribute(Name = "third_cid")]
		public long? ThirdCid { get; set; }

		[DataMemberAttribute(Name = "fourth_cid")]
		public long? FourthCid { get; set; }

		[DataMemberAttribute(Name = "out_sku_id")]
		public string OutSkuId { get; set; }

		[DataMemberAttribute(Name = "supplier_id")]
		public string SupplierId { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public string OutProductId { get; set; }

		[DataMemberAttribute(Name = "warehouse_ids")]
		public List<string> WarehouseIds { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_ids")]
		public List<string> OutWarehouseIds { get; set; }

		[DataMemberAttribute(Name = "inventory_type")]
		public string InventoryType { get; set; }

		[DataMemberAttribute(Name = "inventory_type_desc")]
		public string InventoryTypeDesc { get; set; }

		[DataMemberAttribute(Name = "reduce_stock_type")]
		public long? ReduceStockType { get; set; }

		[DataMemberAttribute(Name = "reduce_stock_type_desc")]
		public string ReduceStockTypeDesc { get; set; }

		[DataMemberAttribute(Name = "origin_amount")]
		public long? OriginAmount { get; set; }

		[DataMemberAttribute(Name = "has_tax")]
		public bool? HasTax { get; set; }

		[DataMemberAttribute(Name = "item_num")]
		public long? ItemNum { get; set; }

		[DataMemberAttribute(Name = "sum_amount")]
		public long? SumAmount { get; set; }

		[DataMemberAttribute(Name = "source_platform")]
		public string SourcePlatform { get; set; }

		[DataMemberAttribute(Name = "product_pic")]
		public string ProductPic { get; set; }

		[DataMemberAttribute(Name = "is_comment")]
		public long? IsComment { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "inventory_list")]
		public List<InventoryListItem> InventoryList { get; set; }

		[DataMemberAttribute(Name = "post_amount")]
		public long? PostAmount { get; set; }

		[DataMemberAttribute(Name = "pre_sale_type")]
		public long? PreSaleType { get; set; }

		[DataMemberAttribute(Name = "after_sale_info")]
		public AfterSaleInfo AfterSaleInfo { get; set; }

		[DataMemberAttribute(Name = "promotion_redpack_amount")]
		public long? PromotionRedpackAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_redpack_platform_amount")]
		public long? PromotionRedpackPlatformAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_redpack_talent_amount")]
		public long? PromotionRedpackTalentAmount { get; set; }

		[DataMemberAttribute(Name = "receive_type")]
		public long? ReceiveType { get; set; }

		[DataMemberAttribute(Name = "need_serial_number")]
		public bool? NeedSerialNumber { get; set; }

		[DataMemberAttribute(Name = "ad_env_type")]
		public string AdEnvType { get; set; }

		[DataMemberAttribute(Name = "sku_order_tag_ui")]
		public List<SkuOrderTagUiItem> SkuOrderTagUi { get; set; }

		[DataMemberAttribute(Name = "product_id_str")]
		public string ProductIdStr { get; set; }

		[DataMemberAttribute(Name = "appointment_ship_time")]
		public long? AppointmentShipTime { get; set; }

		[DataMemberAttribute(Name = "room_id_str")]
		public string RoomIdStr { get; set; }

		[DataMemberAttribute(Name = "given_product_type")]
		public string GivenProductType { get; set; }

		[DataMemberAttribute(Name = "master_sku_order_id")]
		public string MasterSkuOrderId { get; set; }

		[DataMemberAttribute(Name = "card_voucher")]
		public CardVoucher CardVoucher { get; set; }

		[DataMemberAttribute(Name = "bundle_sku_info")]
		public List<BundleSkuInfoItem> BundleSkuInfo { get; set; }

		[DataMemberAttribute(Name = "account_list")]
		public AccountList AccountList { get; set; }

		[DataMemberAttribute(Name = "sku_customization_info")]
		public List<SkuCustomizationInfoItem> SkuCustomizationInfo { get; set; }

		[DataMemberAttribute(Name = "author_cost_amount")]
		public long? AuthorCostAmount { get; set; }

		[DataMemberAttribute(Name = "only_platform_cost_amount")]
		public long? OnlyPlatformCostAmount { get; set; }

		[DataMemberAttribute(Name = "promise_info")]
		public string PromiseInfo { get; set; }

		[DataMemberAttribute(Name = "store_info")]
		public StoreInfo StoreInfo { get; set; }

		[DataMemberAttribute(Name = "mask_post_receiver")]
		public string MaskPostReceiver { get; set; }

		[DataMemberAttribute(Name = "mask_post_tel")]
		public string MaskPostTel { get; set; }

		[DataMemberAttribute(Name = "mask_post_addr")]
		public MaskPostAddr MaskPostAddr { get; set; }

	}
}
