using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Data
{
	public class SpecItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "value")]
		public string Value { get; set; }

	}
}
