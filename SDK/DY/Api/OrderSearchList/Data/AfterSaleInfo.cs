using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Data
{
	public class AfterSaleInfo
	{
		[DataMemberAttribute(Name = "after_sale_status")]
		public long? AfterSaleStatus { get; set; }

		[DataMemberAttribute(Name = "after_sale_type")]
		public long? AfterSaleType { get; set; }

		[DataMemberAttribute(Name = "refund_status")]
		public long? RefundStatus { get; set; }

	}
}
