using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Data
{
	public class UserCoordinate
	{
		[DataMemberAttribute(Name = "user_coordinate_longitude")]
		public string UserCoordinateLongitude { get; set; }

		[DataMemberAttribute(Name = "user_coordinate_latitude")]
		public string UserCoordinateLatitude { get; set; }

	}
}
