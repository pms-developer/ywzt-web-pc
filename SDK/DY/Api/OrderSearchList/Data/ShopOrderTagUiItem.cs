using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Data
{
	public class ShopOrderTagUiItem
	{
		[DataMemberAttribute(Name = "key")]
		public string Key { get; set; }

		[DataMemberAttribute(Name = "text")]
		public string Text { get; set; }

		[DataMemberAttribute(Name = "help_doc")]
		public string HelpDoc { get; set; }

	}
}
