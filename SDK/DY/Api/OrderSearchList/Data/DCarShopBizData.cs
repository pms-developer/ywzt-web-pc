using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderSearchList.Data
{
	public class DCarShopBizData
	{
		[DataMemberAttribute(Name = "poi_id")]
		public string PoiId { get; set; }

		[DataMemberAttribute(Name = "poi_name")]
		public string PoiName { get; set; }

		[DataMemberAttribute(Name = "poi_addr")]
		public string PoiAddr { get; set; }

		[DataMemberAttribute(Name = "poi_tel")]
		public string PoiTel { get; set; }

		[DataMemberAttribute(Name = "coupon_right")]
		public List<CouponRightItem> CouponRight { get; set; }

		[DataMemberAttribute(Name = "poi_pname")]
		public string PoiPname { get; set; }

		[DataMemberAttribute(Name = "poi_city_name")]
		public string PoiCityName { get; set; }

		[DataMemberAttribute(Name = "poi_adname")]
		public string PoiAdname { get; set; }

	}
}
