using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Data
{
	public class AccountList
	{
		[DataMemberAttribute(Name = "account_info")]
		public List<AccountInfoItem> AccountInfo { get; set; }

	}
}
