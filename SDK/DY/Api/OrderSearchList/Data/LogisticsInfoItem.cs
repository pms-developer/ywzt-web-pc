using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderSearchList.Data
{
	public class LogisticsInfoItem
	{
		[DataMemberAttribute(Name = "tracking_no")]
		public string TrackingNo { get; set; }

		[DataMemberAttribute(Name = "company")]
		public string Company { get; set; }

		[DataMemberAttribute(Name = "ship_time")]
		public long? ShipTime { get; set; }

		[DataMemberAttribute(Name = "delivery_id")]
		public string DeliveryId { get; set; }

		[DataMemberAttribute(Name = "company_name")]
		public string CompanyName { get; set; }

		[DataMemberAttribute(Name = "product_info")]
		public List<ProductInfoItem> ProductInfo { get; set; }

	}
}
