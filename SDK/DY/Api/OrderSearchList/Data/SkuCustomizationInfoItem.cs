using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Data
{
	public class SkuCustomizationInfoItem
	{
		[DataMemberAttribute(Name = "detail")]
		public Detail Detail { get; set; }

	}
}
