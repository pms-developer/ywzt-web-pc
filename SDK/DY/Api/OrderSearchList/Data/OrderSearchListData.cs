using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderSearchList.Data
{
	public class OrderSearchListData
	{
		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "shop_order_list")]
		public List<ShopOrderListItem> ShopOrderList { get; set; }

	}
}
