using System.Runtime.Serialization;

namespace Dop.Api.OrderSearchList.Data
{
	public class PicItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
