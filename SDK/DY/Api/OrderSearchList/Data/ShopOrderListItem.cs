using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderSearchList.Data
{
	public class ShopOrderListItem
	{
		[DataMemberAttribute(Name = "shop_id")]
		public long? ShopId { get; set; }

		[DataMemberAttribute(Name = "shop_name")]
		public string ShopName { get; set; }

		[DataMemberAttribute(Name = "open_id")]
		public string OpenId { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "order_level")]
		public long? OrderLevel { get; set; }

		[DataMemberAttribute(Name = "biz")]
		public long? Biz { get; set; }

		[DataMemberAttribute(Name = "biz_desc")]
		public string BizDesc { get; set; }

		[DataMemberAttribute(Name = "order_type")]
		public long? OrderType { get; set; }

		[DataMemberAttribute(Name = "order_type_desc")]
		public string OrderTypeDesc { get; set; }

		[DataMemberAttribute(Name = "trade_type")]
		public long? TradeType { get; set; }

		[DataMemberAttribute(Name = "trade_type_desc")]
		public string TradeTypeDesc { get; set; }

		[DataMemberAttribute(Name = "order_status")]
		public long? OrderStatus { get; set; }

		[DataMemberAttribute(Name = "order_status_desc")]
		public string OrderStatusDesc { get; set; }

		[DataMemberAttribute(Name = "main_status")]
		public long? MainStatus { get; set; }

		[DataMemberAttribute(Name = "main_status_desc")]
		public string MainStatusDesc { get; set; }

		[DataMemberAttribute(Name = "pay_time")]
		public long? PayTime { get; set; }

		[DataMemberAttribute(Name = "order_expire_time")]
		public long? OrderExpireTime { get; set; }

		[DataMemberAttribute(Name = "finish_time")]
		public long? FinishTime { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public long? UpdateTime { get; set; }

		[DataMemberAttribute(Name = "cancel_reason")]
		public string CancelReason { get; set; }

		[DataMemberAttribute(Name = "buyer_words")]
		public string BuyerWords { get; set; }

		[DataMemberAttribute(Name = "seller_words")]
		public string SellerWords { get; set; }

		[DataMemberAttribute(Name = "b_type")]
		public long? BType { get; set; }

		[DataMemberAttribute(Name = "b_type_desc")]
		public string BTypeDesc { get; set; }

		[DataMemberAttribute(Name = "sub_b_type")]
		public long? SubBType { get; set; }

		[DataMemberAttribute(Name = "sub_b_type_desc")]
		public string SubBTypeDesc { get; set; }

		[DataMemberAttribute(Name = "app_id")]
		public long? AppId { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public long? PayType { get; set; }

		[DataMemberAttribute(Name = "channel_payment_no")]
		public string ChannelPaymentNo { get; set; }

		[DataMemberAttribute(Name = "order_amount")]
		public long? OrderAmount { get; set; }

		[DataMemberAttribute(Name = "pay_amount")]
		public long? PayAmount { get; set; }

		[DataMemberAttribute(Name = "post_amount")]
		public long? PostAmount { get; set; }

		[DataMemberAttribute(Name = "post_insurance_amount")]
		public long? PostInsuranceAmount { get; set; }

		[DataMemberAttribute(Name = "modify_amount")]
		public long? ModifyAmount { get; set; }

		[DataMemberAttribute(Name = "modify_post_amount")]
		public long? ModifyPostAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_amount")]
		public long? PromotionAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_shop_amount")]
		public long? PromotionShopAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_platform_amount")]
		public long? PromotionPlatformAmount { get; set; }

		[DataMemberAttribute(Name = "shop_cost_amount")]
		public long? ShopCostAmount { get; set; }

		[DataMemberAttribute(Name = "platform_cost_amount")]
		public long? PlatformCostAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_talent_amount")]
		public long? PromotionTalentAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_pay_amount")]
		public long? PromotionPayAmount { get; set; }

		[DataMemberAttribute(Name = "post_tel")]
		public string PostTel { get; set; }

		[DataMemberAttribute(Name = "encrypt_post_tel")]
		public string EncryptPostTel { get; set; }

		[DataMemberAttribute(Name = "post_receiver")]
		public string PostReceiver { get; set; }

		[DataMemberAttribute(Name = "encrypt_post_receiver")]
		public string EncryptPostReceiver { get; set; }

		[DataMemberAttribute(Name = "post_addr")]
		public PostAddr PostAddr { get; set; }

		[DataMemberAttribute(Name = "exp_ship_time")]
		public long? ExpShipTime { get; set; }

		[DataMemberAttribute(Name = "ship_time")]
		public long? ShipTime { get; set; }

		[DataMemberAttribute(Name = "logistics_info")]
		public List<LogisticsInfoItem> LogisticsInfo { get; set; }

		[DataMemberAttribute(Name = "sku_order_list")]
		public List<SkuOrderListItem> SkuOrderList { get; set; }

		[DataMemberAttribute(Name = "seller_remark_stars")]
		public long? SellerRemarkStars { get; set; }

		[DataMemberAttribute(Name = "order_phase_list")]
		public List<OrderPhaseListItem> OrderPhaseList { get; set; }

		[DataMemberAttribute(Name = "doudian_open_id")]
		public string DoudianOpenId { get; set; }

		[DataMemberAttribute(Name = "serial_number_list")]
		public List<string> SerialNumberList { get; set; }

		[DataMemberAttribute(Name = "promotion_redpack_amount")]
		public long? PromotionRedpackAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_redpack_platform_amount")]
		public long? PromotionRedpackPlatformAmount { get; set; }

		[DataMemberAttribute(Name = "promotion_redpack_talent_amount")]
		public long? PromotionRedpackTalentAmount { get; set; }

		[DataMemberAttribute(Name = "user_id_info")]
		public UserIdInfo UserIdInfo { get; set; }

		[DataMemberAttribute(Name = "appointment_ship_time")]
		public long? AppointmentShipTime { get; set; }

		[DataMemberAttribute(Name = "d_car_shop_biz_data")]
		public DCarShopBizData DCarShopBizData { get; set; }

		[DataMemberAttribute(Name = "shop_order_tag_ui")]
		public List<ShopOrderTagUiItem> ShopOrderTagUi { get; set; }

		[DataMemberAttribute(Name = "total_promotion_amount")]
		public long? TotalPromotionAmount { get; set; }

		[DataMemberAttribute(Name = "post_origin_amount")]
		public long? PostOriginAmount { get; set; }

		[DataMemberAttribute(Name = "post_promotion_amount")]
		public long? PostPromotionAmount { get; set; }

		[DataMemberAttribute(Name = "user_tag_ui")]
		public List<UserTagUiItem> UserTagUi { get; set; }

		[DataMemberAttribute(Name = "author_cost_amount")]
		public long? AuthorCostAmount { get; set; }

		[DataMemberAttribute(Name = "only_platform_cost_amount")]
		public long? OnlyPlatformCostAmount { get; set; }

		[DataMemberAttribute(Name = "promise_info")]
		public string PromiseInfo { get; set; }

		[DataMemberAttribute(Name = "mask_post_receiver")]
		public string MaskPostReceiver { get; set; }

		[DataMemberAttribute(Name = "mask_post_tel")]
		public string MaskPostTel { get; set; }

		[DataMemberAttribute(Name = "mask_post_addr")]
		public MaskPostAddr MaskPostAddr { get; set; }

		[DataMemberAttribute(Name = "user_coordinate")]
		public UserCoordinate UserCoordinate { get; set; }

		[DataMemberAttribute(Name = "earliest_receipt_time")]
		public long? EarliestReceiptTime { get; set; }

		[DataMemberAttribute(Name = "latest_receipt_time")]
		public long? LatestReceiptTime { get; set; }

		[DataMemberAttribute(Name = "early_arrival")]
		public bool? EarlyArrival { get; set; }

		[DataMemberAttribute(Name = "target_arrival_time")]
		public long? TargetArrivalTime { get; set; }

	}
}
