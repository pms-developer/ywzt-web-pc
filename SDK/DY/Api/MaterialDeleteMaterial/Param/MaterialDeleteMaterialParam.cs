using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialDeleteMaterial.Param
{
	public class MaterialDeleteMaterialParam
	{
		[DataMemberAttribute(Name = "material_ids")]
		public List<string> MaterialIds { get; set; }

	}
}
