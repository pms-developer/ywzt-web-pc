using System;
using Dop.Core;
using Dop.Api.MaterialDeleteMaterial.Param;

namespace Dop.Api.MaterialDeleteMaterial
{
	public class MaterialDeleteMaterialRequest : DoudianOpApiRequest<MaterialDeleteMaterialParam>
	{
		public override string GetUrlPath()
		{
			return "/material/deleteMaterial";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialDeleteMaterialResponse);
		}

		public  MaterialDeleteMaterialParam BuildParam()
		{
			return Param ?? (Param = new MaterialDeleteMaterialParam());
		}

	}
}
