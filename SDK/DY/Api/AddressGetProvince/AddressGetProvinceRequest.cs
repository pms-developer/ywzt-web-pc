using System;
using Dop.Core;
using Dop.Api.AddressGetProvince.Param;

namespace Dop.Api.AddressGetProvince
{
	public class AddressGetProvinceRequest : DoudianOpApiRequest<AddressGetProvinceParam>
	{
		public override string GetUrlPath()
		{
			return "/address/getProvince";
		}

		public override Type GetResponseType()
		{
			return typeof(AddressGetProvinceResponse);
		}

		public  AddressGetProvinceParam BuildParam()
		{
			return Param ?? (Param = new AddressGetProvinceParam());
		}

	}
}
