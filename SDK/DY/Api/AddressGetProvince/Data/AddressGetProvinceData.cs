using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AddressGetProvince.Data
{
	public class AddressGetProvinceData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
