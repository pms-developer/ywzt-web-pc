using System.Runtime.Serialization;

namespace Dop.Api.LogisticsWaybillApply.Data
{
	public class WaybillInfosItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "print_data")]
		public string PrintData { get; set; }

		[DataMemberAttribute(Name = "sign")]
		public string Sign { get; set; }

	}
}
