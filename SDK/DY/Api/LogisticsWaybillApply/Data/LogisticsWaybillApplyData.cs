using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsWaybillApply.Data
{
	public class LogisticsWaybillApplyData
	{
		[DataMemberAttribute(Name = "waybill_infos")]
		public List<WaybillInfosItem> WaybillInfos { get; set; }

		[DataMemberAttribute(Name = "err_infos")]
		public List<ErrInfosItem> ErrInfos { get; set; }

	}
}
