using System.Runtime.Serialization;

namespace Dop.Api.LogisticsWaybillApply.Data
{
	public class ErrInfosItem
	{
		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "err_code")]
		public int? ErrCode { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

	}
}
