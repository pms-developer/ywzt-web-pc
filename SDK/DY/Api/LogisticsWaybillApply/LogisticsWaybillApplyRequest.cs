using System;
using Dop.Core;
using Dop.Api.LogisticsWaybillApply.Param;

namespace Dop.Api.LogisticsWaybillApply
{
	public class LogisticsWaybillApplyRequest : DoudianOpApiRequest<LogisticsWaybillApplyParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/waybillApply";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsWaybillApplyResponse);
		}

		public  LogisticsWaybillApplyParam BuildParam()
		{
			return Param ?? (Param = new LogisticsWaybillApplyParam());
		}

	}
}
