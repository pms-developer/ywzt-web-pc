using System.Runtime.Serialization;

namespace Dop.Api.LogisticsWaybillApply.Param
{
	public class WaybillAppliesItem
	{
		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

	}
}
