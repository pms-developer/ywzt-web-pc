using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsWaybillApply.Param
{
	public class LogisticsWaybillApplyParam
	{
		[DataMemberAttribute(Name = "waybill_applies")]
		public List<WaybillAppliesItem> WaybillApplies { get; set; }

	}
}
