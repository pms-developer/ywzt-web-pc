using Dop.Core;
using Dop.Api.AfterSaleFillLogistics.Data;

namespace Dop.Api.AfterSaleFillLogistics
{
	public class AfterSaleFillLogisticsResponse : DoudianOpApiResponse<AfterSaleFillLogisticsData>
	{
	}
}
