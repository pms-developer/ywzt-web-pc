using System;
using Dop.Core;
using Dop.Api.AfterSaleFillLogistics.Param;

namespace Dop.Api.AfterSaleFillLogistics
{
	public class AfterSaleFillLogisticsRequest : DoudianOpApiRequest<AfterSaleFillLogisticsParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/fillLogistics";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleFillLogisticsResponse);
		}

		public  AfterSaleFillLogisticsParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleFillLogisticsParam());
		}

	}
}
