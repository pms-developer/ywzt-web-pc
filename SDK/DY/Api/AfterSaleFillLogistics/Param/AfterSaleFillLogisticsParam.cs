using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleFillLogistics.Param
{
	public class AfterSaleFillLogisticsParam
	{
		[DataMemberAttribute(Name = "aftersale_id")]
		public long? AftersaleId { get; set; }

		[DataMemberAttribute(Name = "send_type")]
		public int? SendType { get; set; }

		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "tracking_no")]
		public string TrackingNo { get; set; }

	}
}
