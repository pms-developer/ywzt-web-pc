using System;
using Dop.Core;
using Dop.Api.ProductDel.Param;

namespace Dop.Api.ProductDel
{
	public class ProductDelRequest : DoudianOpApiRequest<ProductDelParam>
	{
		public override string GetUrlPath()
		{
			return "/product/del";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductDelResponse);
		}

		public  ProductDelParam BuildParam()
		{
			return Param ?? (Param = new ProductDelParam());
		}

	}
}
