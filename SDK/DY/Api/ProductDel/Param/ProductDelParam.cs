using System.Runtime.Serialization;

namespace Dop.Api.ProductDel.Param
{
	public class ProductDelParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public long? OutProductId { get; set; }

		[DataMemberAttribute(Name = "delete_forever")]
		public bool? DeleteForever { get; set; }

	}
}
