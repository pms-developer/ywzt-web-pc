using Dop.Core;
using Dop.Api.AfterSaleRejectReasonCodeList.Data;

namespace Dop.Api.AfterSaleRejectReasonCodeList
{
	public class AfterSaleRejectReasonCodeListResponse : DoudianOpApiResponse<AfterSaleRejectReasonCodeListData>
	{
	}
}
