using System;
using Dop.Core;
using Dop.Api.AfterSaleRejectReasonCodeList.Param;

namespace Dop.Api.AfterSaleRejectReasonCodeList
{
	public class AfterSaleRejectReasonCodeListRequest : DoudianOpApiRequest<AfterSaleRejectReasonCodeListParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/rejectReasonCodeList";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleRejectReasonCodeListResponse);
		}

		public  AfterSaleRejectReasonCodeListParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleRejectReasonCodeListParam());
		}

	}
}
