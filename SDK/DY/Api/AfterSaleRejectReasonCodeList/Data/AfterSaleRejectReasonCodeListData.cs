using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleRejectReasonCodeList.Data
{
	public class AfterSaleRejectReasonCodeListData
	{
		[DataMemberAttribute(Name = "items")]
		public List<ItemsItem> Items { get; set; }

	}
}
