using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleRejectReasonCodeList.Data
{
	public class ItemsItem
	{
		[DataMemberAttribute(Name = "reject_reason_code")]
		public long? RejectReasonCode { get; set; }

		[DataMemberAttribute(Name = "reason")]
		public string Reason { get; set; }

		[DataMemberAttribute(Name = "evidence_description")]
		public string EvidenceDescription { get; set; }

		[DataMemberAttribute(Name = "evidence_need")]
		public string EvidenceNeed { get; set; }

		[DataMemberAttribute(Name = "image")]
		public string Image { get; set; }

		[DataMemberAttribute(Name = "order_type")]
		public long? OrderType { get; set; }

		[DataMemberAttribute(Name = "pkg")]
		public long? Pkg { get; set; }

	}
}
