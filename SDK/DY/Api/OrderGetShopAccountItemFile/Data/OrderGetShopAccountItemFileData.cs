using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderGetShopAccountItemFile.Data
{
	public class OrderGetShopAccountItemFileData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "code_msg")]
		public string CodeMsg { get; set; }

	}
}
