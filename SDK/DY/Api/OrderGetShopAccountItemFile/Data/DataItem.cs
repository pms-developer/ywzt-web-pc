using System.Runtime.Serialization;

namespace Dop.Api.OrderGetShopAccountItemFile.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "shop_id")]
		public long? ShopId { get; set; }

		[DataMemberAttribute(Name = "bill_date")]
		public string BillDate { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
