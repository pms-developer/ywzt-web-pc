using System;
using Dop.Core;
using Dop.Api.OrderGetShopAccountItemFile.Param;

namespace Dop.Api.OrderGetShopAccountItemFile
{
	public class OrderGetShopAccountItemFileRequest : DoudianOpApiRequest<OrderGetShopAccountItemFileParam>
	{
		public override string GetUrlPath()
		{
			return "/order/getShopAccountItemFile";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderGetShopAccountItemFileResponse);
		}

		public  OrderGetShopAccountItemFileParam BuildParam()
		{
			return Param ?? (Param = new OrderGetShopAccountItemFileParam());
		}

	}
}
