using System.Runtime.Serialization;

namespace Dop.Api.OrderGetShopAccountItemFile.Param
{
	public class OrderGetShopAccountItemFileParam
	{
		[DataMemberAttribute(Name = "start_date")]
		public string StartDate { get; set; }

		[DataMemberAttribute(Name = "end_date")]
		public string EndDate { get; set; }

	}
}
