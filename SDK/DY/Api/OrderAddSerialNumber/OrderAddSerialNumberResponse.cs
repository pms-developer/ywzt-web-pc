using Dop.Core;
using Dop.Api.OrderAddSerialNumber.Data;

namespace Dop.Api.OrderAddSerialNumber
{
	public class OrderAddSerialNumberResponse : DoudianOpApiResponse<OrderAddSerialNumberData>
	{
	}
}
