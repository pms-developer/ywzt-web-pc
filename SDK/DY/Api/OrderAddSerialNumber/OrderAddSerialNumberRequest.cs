using System;
using Dop.Core;
using Dop.Api.OrderAddSerialNumber.Param;

namespace Dop.Api.OrderAddSerialNumber
{
	public class OrderAddSerialNumberRequest : DoudianOpApiRequest<OrderAddSerialNumberParam>
	{
		public override string GetUrlPath()
		{
			return "/order/addSerialNumber";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderAddSerialNumberResponse);
		}

		public  OrderAddSerialNumberParam BuildParam()
		{
			return Param ?? (Param = new OrderAddSerialNumberParam());
		}

	}
}
