using System.Runtime.Serialization;

namespace Dop.Api.AddressList.Param
{
	public class AddressListParam
	{
		[DataMemberAttribute(Name = "shop_id")]
		public long? ShopId { get; set; }

		[DataMemberAttribute(Name = "page_size")]
		public long? PageSize { get; set; }

		[DataMemberAttribute(Name = "page_no")]
		public long? PageNo { get; set; }

		[DataMemberAttribute(Name = "order_by")]
		public string OrderBy { get; set; }

		[DataMemberAttribute(Name = "order_field")]
		public string OrderField { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

	}
}
