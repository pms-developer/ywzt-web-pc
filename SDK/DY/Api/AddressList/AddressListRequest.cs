using System;
using Dop.Core;
using Dop.Api.AddressList.Param;

namespace Dop.Api.AddressList
{
	public class AddressListRequest : DoudianOpApiRequest<AddressListParam>
	{
		public override string GetUrlPath()
		{
			return "/address/list";
		}

		public override Type GetResponseType()
		{
			return typeof(AddressListResponse);
		}

		public  AddressListParam BuildParam()
		{
			return Param ?? (Param = new AddressListParam());
		}

	}
}
