using System.Runtime.Serialization;

namespace Dop.Api.AddressList.Data
{
	public class AddressListItem
	{
		[DataMemberAttribute(Name = "address_id")]
		public long? AddressId { get; set; }

		[DataMemberAttribute(Name = "reciever_name")]
		public string RecieverName { get; set; }

		[DataMemberAttribute(Name = "is_default")]
		public long? IsDefault { get; set; }

		[DataMemberAttribute(Name = "is_send_default")]
		public long? IsSendDefault { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public string UpdateTime { get; set; }

		[DataMemberAttribute(Name = "receiver_provinc")]
		public string ReceiverProvinc { get; set; }

		[DataMemberAttribute(Name = "receiver_city")]
		public string ReceiverCity { get; set; }

		[DataMemberAttribute(Name = "receiver_district")]
		public string ReceiverDistrict { get; set; }

		[DataMemberAttribute(Name = "receiver_detail")]
		public string ReceiverDetail { get; set; }

		[DataMemberAttribute(Name = "receiver_street")]
		public string ReceiverStreet { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

	}
}
