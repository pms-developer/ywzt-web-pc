using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AddressList.Data
{
	public class AddressListData
	{
		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "page_size")]
		public long? PageSize { get; set; }

		[DataMemberAttribute(Name = "page_no")]
		public long? PageNo { get; set; }

		[DataMemberAttribute(Name = "address_list")]
		public List<AddressListItem> AddressList { get; set; }

	}
}
