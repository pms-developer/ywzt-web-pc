using System;
using Dop.Core;
using Dop.Api.AfterSaleReturnGoodsToWareHouseSuccess.Param;

namespace Dop.Api.AfterSaleReturnGoodsToWareHouseSuccess
{
	public class AfterSaleReturnGoodsToWareHouseSuccessRequest : DoudianOpApiRequest<AfterSaleReturnGoodsToWareHouseSuccessParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/returnGoodsToWareHouseSuccess";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleReturnGoodsToWareHouseSuccessResponse);
		}

		public  AfterSaleReturnGoodsToWareHouseSuccessParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleReturnGoodsToWareHouseSuccessParam());
		}

	}
}
