using Dop.Core;
using Dop.Api.AfterSaleReturnGoodsToWareHouseSuccess.Data;

namespace Dop.Api.AfterSaleReturnGoodsToWareHouseSuccess
{
	public class AfterSaleReturnGoodsToWareHouseSuccessResponse : DoudianOpApiResponse<AfterSaleReturnGoodsToWareHouseSuccessData>
	{
	}
}
