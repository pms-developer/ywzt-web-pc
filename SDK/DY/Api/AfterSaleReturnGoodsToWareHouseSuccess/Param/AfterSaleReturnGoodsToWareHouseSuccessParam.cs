using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleReturnGoodsToWareHouseSuccess.Param
{
	public class AfterSaleReturnGoodsToWareHouseSuccessParam
	{
		[DataMemberAttribute(Name = "aftersale_id")]
		public string AftersaleId { get; set; }

		[DataMemberAttribute(Name = "op_time")]
		public long? OpTime { get; set; }

		[DataMemberAttribute(Name = "tracking_no")]
		public string TrackingNo { get; set; }

		[DataMemberAttribute(Name = "logistics_company_code")]
		public string LogisticsCompanyCode { get; set; }

	}
}
