using Dop.Core;
using Dop.Api.ProductGetProductUpdateRule.Data;

namespace Dop.Api.ProductGetProductUpdateRule
{
	public class ProductGetProductUpdateRuleResponse : DoudianOpApiResponse<ProductGetProductUpdateRuleData>
	{
	}
}
