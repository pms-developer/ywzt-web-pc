using System;
using Dop.Core;
using Dop.Api.ProductGetProductUpdateRule.Param;

namespace Dop.Api.ProductGetProductUpdateRule
{
	public class ProductGetProductUpdateRuleRequest : DoudianOpApiRequest<ProductGetProductUpdateRuleParam>
	{
		public override string GetUrlPath()
		{
			return "/product/getProductUpdateRule";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductGetProductUpdateRuleResponse);
		}

		public  ProductGetProductUpdateRuleParam BuildParam()
		{
			return Param ?? (Param = new ProductGetProductUpdateRuleParam());
		}

	}
}
