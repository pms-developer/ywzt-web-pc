using System.Runtime.Serialization;

namespace Dop.Api.ProductGetProductUpdateRule.Data
{
	public class ProductGetProductUpdateRuleData
	{
		[DataMemberAttribute(Name = "fulfillment_rule")]
		public FulfillmentRule FulfillmentRule { get; set; }

	}
}
