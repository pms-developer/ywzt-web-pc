using System.Runtime.Serialization;

namespace Dop.Api.ProductGetProductUpdateRule.Data
{
	public class TimeSkuSpecDetialItem
	{
		[DataMemberAttribute(Name = "spec_value")]
		public string SpecValue { get; set; }

		[DataMemberAttribute(Name = "is_presell_spec")]
		public bool? IsPresellSpec { get; set; }

		[DataMemberAttribute(Name = "spec_code")]
		public string SpecCode { get; set; }

	}
}
