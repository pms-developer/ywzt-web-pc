using System.Runtime.Serialization;

namespace Dop.Api.ProductGetProductUpdateRule.Data
{
	public class DeliveryAfterPresellConfig
	{
		[DataMemberAttribute(Name = "min_delivery_days")]
		public long? MinDeliveryDays { get; set; }

		[DataMemberAttribute(Name = "max_delivery_days")]
		public long? MaxDeliveryDays { get; set; }

		[DataMemberAttribute(Name = "max_presell_end_days")]
		public long? MaxPresellEndDays { get; set; }

		[DataMemberAttribute(Name = "need_audit")]
		public bool? NeedAudit { get; set; }

	}
}
