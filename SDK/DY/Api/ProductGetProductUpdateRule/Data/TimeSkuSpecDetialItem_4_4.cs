using System.Runtime.Serialization;

namespace Dop.Api.ProductGetProductUpdateRule.Data
{
	public class TimeSkuSpecDetialItem_4_4
	{
		[DataMemberAttribute(Name = "spec_value")]
		public string SpecValue { get; set; }

		[DataMemberAttribute(Name = "is_presell_spec")]
		public bool? IsPresellSpec { get; set; }

	}
}
