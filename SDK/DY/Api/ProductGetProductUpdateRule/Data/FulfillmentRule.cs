using System.Runtime.Serialization;

namespace Dop.Api.ProductGetProductUpdateRule.Data
{
	public class FulfillmentRule
	{
		[DataMemberAttribute(Name = "normal_rule")]
		public NormalRule NormalRule { get; set; }

		[DataMemberAttribute(Name = "step_rule")]
		public StepRule StepRule { get; set; }

		[DataMemberAttribute(Name = "product_presell_rule")]
		public ProductPresellRule ProductPresellRule { get; set; }

		[DataMemberAttribute(Name = "sku_presell_rule")]
		public SkuPresellRule SkuPresellRule { get; set; }

		[DataMemberAttribute(Name = "time_sku_presell_with_normal_rule")]
		public TimeSkuPresellWithNormalRule TimeSkuPresellWithNormalRule { get; set; }

		[DataMemberAttribute(Name = "time_sku_pure_presell_rule")]
		public TimeSkuPurePresellRule TimeSkuPurePresellRule { get; set; }

	}
}
