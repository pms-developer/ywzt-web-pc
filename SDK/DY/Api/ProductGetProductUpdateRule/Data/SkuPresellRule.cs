using System.Runtime.Serialization;

namespace Dop.Api.ProductGetProductUpdateRule.Data
{
	public class SkuPresellRule
	{
		[DataMemberAttribute(Name = "support")]
		public bool? Support { get; set; }

		[DataMemberAttribute(Name = "min_presell_price")]
		public long? MinPresellPrice { get; set; }

		[DataMemberAttribute(Name = "support_delivery_after_pay")]
		public bool? SupportDeliveryAfterPay { get; set; }

		[DataMemberAttribute(Name = "support_delivery_after_presell")]
		public bool? SupportDeliveryAfterPresell { get; set; }

		[DataMemberAttribute(Name = "delivery_after_pay_config")]
		public DeliveryAfterPayConfig DeliveryAfterPayConfig { get; set; }

		[DataMemberAttribute(Name = "delivery_after_presell_config")]
		public DeliveryAfterPresellConfig DeliveryAfterPresellConfig { get; set; }

	}
}
