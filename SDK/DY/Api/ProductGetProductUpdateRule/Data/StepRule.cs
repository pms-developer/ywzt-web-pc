using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductGetProductUpdateRule.Data
{
	public class StepRule
	{
		[DataMemberAttribute(Name = "support")]
		public bool? Support { get; set; }

		[DataMemberAttribute(Name = "delay_options")]
		public List<long> DelayOptions { get; set; }

		[DataMemberAttribute(Name = "is_special_delay_option")]
		public bool? IsSpecialDelayOption { get; set; }

		[DataMemberAttribute(Name = "step_min_delay")]
		public long? StepMinDelay { get; set; }

		[DataMemberAttribute(Name = "step_max_delay")]
		public long? StepMaxDelay { get; set; }

	}
}
