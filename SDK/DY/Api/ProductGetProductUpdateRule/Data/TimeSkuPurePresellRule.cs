using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductGetProductUpdateRule.Data
{
	public class TimeSkuPurePresellRule
	{
		[DataMemberAttribute(Name = "support")]
		public bool? Support { get; set; }

		[DataMemberAttribute(Name = "time_sku_spec_name")]
		public string TimeSkuSpecName { get; set; }

		[DataMemberAttribute(Name = "time_sku_spec_detial")]
		public List<TimeSkuSpecDetialItem_4_4> TimeSkuSpecDetial { get; set; }

		[DataMemberAttribute(Name = "min_presell_price")]
		public long? MinPresellPrice { get; set; }

	}
}
