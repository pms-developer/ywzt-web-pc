using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductGetProductUpdateRule.Param
{
	public class ProductGetProductUpdateRuleParam
	{
		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

		[DataMemberAttribute(Name = "senses")]
		public List<int> Senses { get; set; }

	}
}
