using System;
using Dop.Core;
using Dop.Api.OrderLogisticsAddMultiPack.Param;

namespace Dop.Api.OrderLogisticsAddMultiPack
{
	public class OrderLogisticsAddMultiPackRequest : DoudianOpApiRequest<OrderLogisticsAddMultiPackParam>
	{
		public override string GetUrlPath()
		{
			return "/order/logisticsAddMultiPack";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderLogisticsAddMultiPackResponse);
		}

		public  OrderLogisticsAddMultiPackParam BuildParam()
		{
			return Param ?? (Param = new OrderLogisticsAddMultiPackParam());
		}

	}
}
