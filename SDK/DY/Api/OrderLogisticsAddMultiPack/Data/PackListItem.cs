using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsAddMultiPack.Data
{
	public class PackListItem
	{
		[DataMemberAttribute(Name = "shipped_order_info")]
		public List<ShippedOrderInfoItem> ShippedOrderInfo { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "pack_id")]
		public string PackId { get; set; }

	}
}
