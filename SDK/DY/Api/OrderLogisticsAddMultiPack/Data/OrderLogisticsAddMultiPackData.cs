using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsAddMultiPack.Data
{
	public class OrderLogisticsAddMultiPackData
	{
		[DataMemberAttribute(Name = "pack_list")]
		public List<PackListItem> PackList { get; set; }

	}
}
