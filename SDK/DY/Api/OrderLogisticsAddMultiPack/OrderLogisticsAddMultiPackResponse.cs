using Dop.Core;
using Dop.Api.OrderLogisticsAddMultiPack.Data;

namespace Dop.Api.OrderLogisticsAddMultiPack
{
	public class OrderLogisticsAddMultiPackResponse : DoudianOpApiResponse<OrderLogisticsAddMultiPackData>
	{
	}
}
