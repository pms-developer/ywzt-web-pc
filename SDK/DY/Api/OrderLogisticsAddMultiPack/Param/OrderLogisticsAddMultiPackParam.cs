using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderLogisticsAddMultiPack.Param
{
	public class OrderLogisticsAddMultiPackParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "pack_list")]
		public List<PackListItem> PackList { get; set; }

		[DataMemberAttribute(Name = "is_reject_refund")]
		public bool? IsRejectRefund { get; set; }

		[DataMemberAttribute(Name = "request_id")]
		public string RequestId { get; set; }

		[DataMemberAttribute(Name = "address_id")]
		public string AddressId { get; set; }

		[DataMemberAttribute(Name = "serial_number_list")]
		public List<string> SerialNumberList { get; set; }

		[DataMemberAttribute(Name = "is_refund_reject")]
		public bool? IsRefundReject { get; set; }

	}
}
