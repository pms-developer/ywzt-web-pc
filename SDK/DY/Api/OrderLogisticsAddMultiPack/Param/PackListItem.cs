using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsAddMultiPack.Param
{
	public class PackListItem
	{
		[DataMemberAttribute(Name = "shipped_order_info")]
		public List<ShippedOrderInfoItem> ShippedOrderInfo { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "company")]
		public string Company { get; set; }

		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "logistics_id")]
		public string LogisticsId { get; set; }

	}
}
