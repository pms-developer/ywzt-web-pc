using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderLogisticsAddMultiPack.Param
{
	public class ShippedOrderInfoItem
	{
		[DataMemberAttribute(Name = "shipped_order_id")]
		public string ShippedOrderId { get; set; }

		[DataMemberAttribute(Name = "shipped_num")]
		public long? ShippedNum { get; set; }

		[DataMemberAttribute(Name = "shipped_item_ids")]
		public List<string> ShippedItemIds { get; set; }

		[DataMemberAttribute(Name = "bundle_list")]
		public List<BundleListItem> BundleList { get; set; }

	}
}
