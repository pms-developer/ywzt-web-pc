using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsAddMultiPack.Param
{
	public class BundleListItem
	{
		[DataMemberAttribute(Name = "sub_product_id")]
		public string SubProductId { get; set; }

		[DataMemberAttribute(Name = "sub_sku_id")]
		public string SubSkuId { get; set; }

		[DataMemberAttribute(Name = "combo_num")]
		public long? ComboNum { get; set; }

	}
}
