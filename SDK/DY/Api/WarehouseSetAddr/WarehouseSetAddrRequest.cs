using System;
using Dop.Core;
using Dop.Api.WarehouseSetAddr.Param;

namespace Dop.Api.WarehouseSetAddr
{
	public class WarehouseSetAddrRequest : DoudianOpApiRequest<WarehouseSetAddrParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/setAddr";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseSetAddrResponse);
		}

		public  WarehouseSetAddrParam BuildParam()
		{
			return Param ?? (Param = new WarehouseSetAddrParam());
		}

	}
}
