using System.Runtime.Serialization;

namespace Dop.Api.WarehouseSetAddr.Param
{
	public class WarehouseSetAddrParam
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "addr")]
		public Addr Addr { get; set; }

	}
}
