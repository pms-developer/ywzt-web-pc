using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.WarehouseList.Param
{
	public class WarehouseListParam
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "warehouse_name")]
		public string WarehouseName { get; set; }

		[DataMemberAttribute(Name = "addr")]
		public Addr Addr { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_ids")]
		public List<string> OutWarehouseIds { get; set; }

		[DataMemberAttribute(Name = "order_by")]
		public string OrderBy { get; set; }

		[DataMemberAttribute(Name = "rank")]
		public string Rank { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

	}
}
