using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.WarehouseList.Data
{
	public class WarehouseListData
	{
		[DataMemberAttribute(Name = "warehouses")]
		public List<WarehousesItem> Warehouses { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
