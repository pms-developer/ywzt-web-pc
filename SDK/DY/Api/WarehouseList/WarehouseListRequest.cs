using System;
using Dop.Core;
using Dop.Api.WarehouseList.Param;

namespace Dop.Api.WarehouseList
{
	public class WarehouseListRequest : DoudianOpApiRequest<WarehouseListParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/list";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseListResponse);
		}

		public  WarehouseListParam BuildParam()
		{
			return Param ?? (Param = new WarehouseListParam());
		}

	}
}
