using System.Runtime.Serialization;

namespace Dop.Api.OrderServiceDetail.Param
{
	public class OrderServiceDetailParam
	{
		[DataMemberAttribute(Name = "service_id")]
		public long? ServiceId { get; set; }

	}
}
