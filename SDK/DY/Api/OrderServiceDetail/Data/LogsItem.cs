using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderServiceDetail.Data
{
	public class LogsItem
	{
		[DataMemberAttribute(Name = "content")]
		public string Content { get; set; }

		[DataMemberAttribute(Name = "img_list")]
		public List<string> ImgList { get; set; }

		[DataMemberAttribute(Name = "service_log_type")]
		public int? ServiceLogType { get; set; }

		[DataMemberAttribute(Name = "operate_name")]
		public string OperateName { get; set; }

		[DataMemberAttribute(Name = "service_detail")]
		public string ServiceDetail { get; set; }

		[DataMemberAttribute(Name = "reply_detail")]
		public string ReplyDetail { get; set; }

		[DataMemberAttribute(Name = "close_detail")]
		public string CloseDetail { get; set; }

		[DataMemberAttribute(Name = "reject_detail")]
		public string RejectDetail { get; set; }

		[DataMemberAttribute(Name = "deal_time")]
		public string DealTime { get; set; }

	}
}
