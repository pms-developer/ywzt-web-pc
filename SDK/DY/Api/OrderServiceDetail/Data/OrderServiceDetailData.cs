using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderServiceDetail.Data
{
	public class OrderServiceDetailData
	{
		[DataMemberAttribute(Name = "detail")]
		public Detail Detail { get; set; }

		[DataMemberAttribute(Name = "logs")]
		public List<LogsItem> Logs { get; set; }

	}
}
