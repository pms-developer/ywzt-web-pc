using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderServiceDetail.Data
{
	public class Detail
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public long? OrderId { get; set; }

		[DataMemberAttribute(Name = "operate_status")]
		public int? OperateStatus { get; set; }

		[DataMemberAttribute(Name = "detail")]
		public string Detail_ { get; set; }

		[DataMemberAttribute(Name = "reply")]
		public string Reply { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "service_type")]
		public long? ServiceType { get; set; }

		[DataMemberAttribute(Name = "reply_time")]
		public string ReplyTime { get; set; }

		[DataMemberAttribute(Name = "operate_status_desc")]
		public string OperateStatusDesc { get; set; }

		[DataMemberAttribute(Name = "shop_id")]
		public long? ShopId { get; set; }

		[DataMemberAttribute(Name = "is_reject")]
		public long? IsReject { get; set; }

		[DataMemberAttribute(Name = "reject_detail")]
		public string RejectDetail { get; set; }

		[DataMemberAttribute(Name = "reject_time")]
		public string RejectTime { get; set; }

		[DataMemberAttribute(Name = "proof_demo")]
		public string ProofDemo { get; set; }

		[DataMemberAttribute(Name = "evidence_required")]
		public long? EvidenceRequired { get; set; }

		[DataMemberAttribute(Name = "img_list")]
		public List<string> ImgList { get; set; }

		[DataMemberAttribute(Name = "expiration_time")]
		public string ExpirationTime { get; set; }

		[DataMemberAttribute(Name = "close_time")]
		public string CloseTime { get; set; }

		[DataMemberAttribute(Name = "close_detail")]
		public string CloseDetail { get; set; }

		[DataMemberAttribute(Name = "first_reply_time")]
		public string FirstReplyTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public string UpdateTime { get; set; }

	}
}
