using System;
using Dop.Core;
using Dop.Api.OrderServiceDetail.Param;

namespace Dop.Api.OrderServiceDetail
{
	public class OrderServiceDetailRequest : DoudianOpApiRequest<OrderServiceDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/order/serviceDetail";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderServiceDetailResponse);
		}

		public  OrderServiceDetailParam BuildParam()
		{
			return Param ?? (Param = new OrderServiceDetailParam());
		}

	}
}
