using Dop.Core;
using Dop.Api.LogisticsCustomTemplateList.Data;

namespace Dop.Api.LogisticsCustomTemplateList
{
	public class LogisticsCustomTemplateListResponse : DoudianOpApiResponse<LogisticsCustomTemplateListData>
	{
	}
}
