using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsCustomTemplateList.Data
{
	public class CustomTemplateDataItem
	{
		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "custom_template_infos")]
		public List<CustomTemplateInfosItem> CustomTemplateInfos { get; set; }

	}
}
