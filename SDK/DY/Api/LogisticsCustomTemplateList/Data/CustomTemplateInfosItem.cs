using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCustomTemplateList.Data
{
	public class CustomTemplateInfosItem
	{
		[DataMemberAttribute(Name = "custom_template_code")]
		public string CustomTemplateCode { get; set; }

		[DataMemberAttribute(Name = "custom_template_url")]
		public string CustomTemplateUrl { get; set; }

		[DataMemberAttribute(Name = "custom_template_name")]
		public string CustomTemplateName { get; set; }

		[DataMemberAttribute(Name = "parent_template_code")]
		public string ParentTemplateCode { get; set; }

	}
}
