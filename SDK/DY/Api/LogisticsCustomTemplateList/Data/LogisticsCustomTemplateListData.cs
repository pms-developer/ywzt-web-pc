using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCustomTemplateList.Data
{
	public class LogisticsCustomTemplateListData
	{
		[DataMemberAttribute(Name = "custom_template_data")]
		public List<CustomTemplateDataItem> CustomTemplateData { get; set; }

	}
}
