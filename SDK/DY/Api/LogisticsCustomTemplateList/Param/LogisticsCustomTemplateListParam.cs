using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCustomTemplateList.Param
{
	public class LogisticsCustomTemplateListParam
	{
		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

	}
}
