using System;
using Dop.Core;
using Dop.Api.LogisticsCustomTemplateList.Param;

namespace Dop.Api.LogisticsCustomTemplateList
{
	public class LogisticsCustomTemplateListRequest : DoudianOpApiRequest<LogisticsCustomTemplateListParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/customTemplateList";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsCustomTemplateListResponse);
		}

		public  LogisticsCustomTemplateListParam BuildParam()
		{
			return Param ?? (Param = new LogisticsCustomTemplateListParam());
		}

	}
}
