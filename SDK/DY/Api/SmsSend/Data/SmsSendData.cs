using System.Runtime.Serialization;

namespace Dop.Api.SmsSend.Data
{
	public class SmsSendData
	{
		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

		[DataMemberAttribute(Name = "message_id")]
		public string MessageId { get; set; }

	}
}
