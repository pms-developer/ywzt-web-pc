using System.Runtime.Serialization;

namespace Dop.Api.SmsSend.Param
{
	public class SmsSendParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "sign")]
		public string Sign { get; set; }

		[DataMemberAttribute(Name = "template_id")]
		public string TemplateId { get; set; }

		[DataMemberAttribute(Name = "template_param")]
		public string TemplateParam { get; set; }

		[DataMemberAttribute(Name = "tag")]
		public string Tag { get; set; }

		[DataMemberAttribute(Name = "post_tel")]
		public string PostTel { get; set; }

		[DataMemberAttribute(Name = "user_ext_code")]
		public string UserExtCode { get; set; }

	}
}
