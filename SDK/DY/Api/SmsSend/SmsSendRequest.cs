using System;
using Dop.Core;
using Dop.Api.SmsSend.Param;

namespace Dop.Api.SmsSend
{
	public class SmsSendRequest : DoudianOpApiRequest<SmsSendParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/send";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsSendResponse);
		}

		public  SmsSendParam BuildParam()
		{
			return Param ?? (Param = new SmsSendParam());
		}

	}
}
