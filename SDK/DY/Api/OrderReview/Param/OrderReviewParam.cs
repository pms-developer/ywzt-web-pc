using System.Runtime.Serialization;

namespace Dop.Api.OrderReview.Param
{
	public class OrderReviewParam
	{
		[DataMemberAttribute(Name = "task_type")]
		public long? TaskType { get; set; }

		[DataMemberAttribute(Name = "reject_code")]
		public long? RejectCode { get; set; }

		[DataMemberAttribute(Name = "object_id")]
		public string ObjectId { get; set; }

	}
}
