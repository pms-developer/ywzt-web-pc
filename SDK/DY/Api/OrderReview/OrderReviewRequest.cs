using System;
using Dop.Core;
using Dop.Api.OrderReview.Param;

namespace Dop.Api.OrderReview
{
	public class OrderReviewRequest : DoudianOpApiRequest<OrderReviewParam>
	{
		public override string GetUrlPath()
		{
			return "/order/review";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderReviewResponse);
		}

		public  OrderReviewParam BuildParam()
		{
			return Param ?? (Param = new OrderReviewParam());
		}

	}
}
