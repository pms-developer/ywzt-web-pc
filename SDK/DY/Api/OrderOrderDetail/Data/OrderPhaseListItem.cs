using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class OrderPhaseListItem
	{
		[DataMemberAttribute(Name = "phase_order_id")]
		public string PhaseOrderId { get; set; }

		[DataMemberAttribute(Name = "total_phase")]
		public long? TotalPhase { get; set; }

		[DataMemberAttribute(Name = "current_phase")]
		public long? CurrentPhase { get; set; }

		[DataMemberAttribute(Name = "sku_price")]
		public long? SkuPrice { get; set; }

		[DataMemberAttribute(Name = "pay_success")]
		public bool? PaySuccess { get; set; }

		[DataMemberAttribute(Name = "sku_order_id")]
		public string SkuOrderId { get; set; }

		[DataMemberAttribute(Name = "campaign_id")]
		public string CampaignId { get; set; }

		[DataMemberAttribute(Name = "phase_payable_price")]
		public long? PhasePayablePrice { get; set; }

		[DataMemberAttribute(Name = "phase_pay_type")]
		public long? PhasePayType { get; set; }

		[DataMemberAttribute(Name = "phase_open_time")]
		public long? PhaseOpenTime { get; set; }

		[DataMemberAttribute(Name = "phase_pay_time")]
		public long? PhasePayTime { get; set; }

		[DataMemberAttribute(Name = "phase_close_time")]
		public long? PhaseCloseTime { get; set; }

		[DataMemberAttribute(Name = "channel_payment_no")]
		public string ChannelPaymentNo { get; set; }

		[DataMemberAttribute(Name = "phase_order_amount")]
		public long? PhaseOrderAmount { get; set; }

		[DataMemberAttribute(Name = "phase_sum_amount")]
		public long? PhaseSumAmount { get; set; }

		[DataMemberAttribute(Name = "phase_post_amount")]
		public long? PhasePostAmount { get; set; }

		[DataMemberAttribute(Name = "phase_pay_amount")]
		public long? PhasePayAmount { get; set; }

		[DataMemberAttribute(Name = "phase_promotion_amount")]
		public long? PhasePromotionAmount { get; set; }

		[DataMemberAttribute(Name = "current_phase_status_desc")]
		public string CurrentPhaseStatusDesc { get; set; }

	}
}
