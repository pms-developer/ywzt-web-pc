using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class RelationOrder
	{
		[DataMemberAttribute(Name = "write_off_no")]
		public string WriteOffNo { get; set; }

		[DataMemberAttribute(Name = "relation_order_id")]
		public string RelationOrderId { get; set; }

	}
}
