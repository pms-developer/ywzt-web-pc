using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class InventoryListItem
	{
		[DataMemberAttribute(Name = "warehouse_id")]
		public string WarehouseId { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "inventory_type")]
		public long? InventoryType { get; set; }

		[DataMemberAttribute(Name = "inventory_type_desc")]
		public string InventoryTypeDesc { get; set; }

		[DataMemberAttribute(Name = "count")]
		public long? Count { get; set; }

		[DataMemberAttribute(Name = "warehouse_type")]
		public long? WarehouseType { get; set; }

	}
}
