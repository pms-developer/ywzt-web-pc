using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class TextItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "key")]
		public string Key { get; set; }

		[DataMemberAttribute(Name = "content")]
		public string Content { get; set; }

	}
}
