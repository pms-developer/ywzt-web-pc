using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class PlatformDiscountDetail
	{
		[DataMemberAttribute(Name = "total_amount")]
		public long? TotalAmount { get; set; }

		[DataMemberAttribute(Name = "coupon_amount")]
		public long? CouponAmount { get; set; }

		[DataMemberAttribute(Name = "full_discount_amount")]
		public long? FullDiscountAmount { get; set; }

		[DataMemberAttribute(Name = "coupon_info")]
		public List<CouponInfoItem> CouponInfo { get; set; }

		[DataMemberAttribute(Name = "full_discount_info")]
		public List<FullDiscountInfoItem> FullDiscountInfo { get; set; }

		[DataMemberAttribute(Name = "redpack_amount")]
		public long? RedpackAmount { get; set; }

		[DataMemberAttribute(Name = "redpack_info")]
		public List<RedpackInfoItem> RedpackInfo { get; set; }

	}
}
