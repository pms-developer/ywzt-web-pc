using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class SkuSpecsItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "value")]
		public string Value { get; set; }

	}
}
