using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class AddedServicesItem
	{
		[DataMemberAttribute(Name = "key")]
		public string Key { get; set; }

		[DataMemberAttribute(Name = "text")]
		public string Text { get; set; }

		[DataMemberAttribute(Name = "help_doc")]
		public string HelpDoc { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
