using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class ProductInfoItem
	{
		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "price")]
		public long? Price { get; set; }

		[DataMemberAttribute(Name = "outer_sku_id")]
		public string OuterSkuId { get; set; }

		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "sku_specs")]
		public List<SkuSpecsItem> SkuSpecs { get; set; }

		[DataMemberAttribute(Name = "product_count")]
		public long? ProductCount { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "sku_order_id")]
		public string SkuOrderId { get; set; }

		[DataMemberAttribute(Name = "product_id_str")]
		public string ProductIdStr { get; set; }

	}
}
