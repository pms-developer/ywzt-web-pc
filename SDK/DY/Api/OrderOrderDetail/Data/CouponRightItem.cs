using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class CouponRightItem
	{
		[DataMemberAttribute(Name = "right_type")]
		public long? RightType { get; set; }

		[DataMemberAttribute(Name = "right_name")]
		public string RightName { get; set; }

		[DataMemberAttribute(Name = "quota")]
		public long? Quota { get; set; }

	}
}
