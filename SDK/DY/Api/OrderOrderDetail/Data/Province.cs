using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class Province
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "id")]
		public string Id { get; set; }

	}
}
