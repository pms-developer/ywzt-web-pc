using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class FullDiscountInfoItem
	{
		[DataMemberAttribute(Name = "campaign_id")]
		public long? CampaignId { get; set; }

		[DataMemberAttribute(Name = "campaign_type")]
		public long? CampaignType { get; set; }

		[DataMemberAttribute(Name = "share_discount_cost")]
		public ShareDiscountCost ShareDiscountCost { get; set; }

		[DataMemberAttribute(Name = "campaign_name")]
		public string CampaignName { get; set; }

		[DataMemberAttribute(Name = "campaign_amount")]
		public long? CampaignAmount { get; set; }

		[DataMemberAttribute(Name = "campaign_sub_type")]
		public long? CampaignSubType { get; set; }

	}
}
