using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class SkuCustomizationInfoItem
	{
		[DataMemberAttribute(Name = "detail")]
		public Detail Detail { get; set; }

	}
}
