using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class AccountInfoItem
	{
		[DataMemberAttribute(Name = "account_name")]
		public string AccountName { get; set; }

		[DataMemberAttribute(Name = "account_type")]
		public string AccountType { get; set; }

		[DataMemberAttribute(Name = "account_id")]
		public string AccountId { get; set; }

		[DataMemberAttribute(Name = "encrypt_account_id")]
		public string EncryptAccountId { get; set; }

	}
}
