using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class CardVoucher
	{
		[DataMemberAttribute(Name = "valid_days")]
		public long? ValidDays { get; set; }

		[DataMemberAttribute(Name = "valid_start")]
		public long? ValidStart { get; set; }

		[DataMemberAttribute(Name = "valid_end")]
		public long? ValidEnd { get; set; }

	}
}
