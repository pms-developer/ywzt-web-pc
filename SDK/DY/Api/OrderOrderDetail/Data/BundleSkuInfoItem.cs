using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class BundleSkuInfoItem
	{
		[DataMemberAttribute(Name = "picture_url")]
		public string PictureUrl { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public string ProductId { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "sku_id")]
		public string SkuId { get; set; }

		[DataMemberAttribute(Name = "item_num")]
		public long? ItemNum { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

	}
}
