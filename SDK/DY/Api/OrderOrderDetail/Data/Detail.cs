using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class Detail
	{
		[DataMemberAttribute(Name = "pic")]
		public List<PicItem> Pic { get; set; }

		[DataMemberAttribute(Name = "text")]
		public List<TextItem> Text { get; set; }

		[DataMemberAttribute(Name = "extra")]
		public string Extra { get; set; }

	}
}
