using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class WriteoffInfoItem
	{
		[DataMemberAttribute(Name = "writeoff_no")]
		public string WriteoffNo { get; set; }

		[DataMemberAttribute(Name = "writeoff_start_time")]
		public long? WriteoffStartTime { get; set; }

		[DataMemberAttribute(Name = "writeoff_expire_time")]
		public long? WriteoffExpireTime { get; set; }

		[DataMemberAttribute(Name = "writeoff_status")]
		public long? WriteoffStatus { get; set; }

		[DataMemberAttribute(Name = "writeoff_status_desc")]
		public string WriteoffStatusDesc { get; set; }

		[DataMemberAttribute(Name = "verify_order_id")]
		public string VerifyOrderId { get; set; }

		[DataMemberAttribute(Name = "writeoff_no_mask")]
		public string WriteoffNoMask { get; set; }

		[DataMemberAttribute(Name = "writtenoff_count")]
		public long? WrittenoffCount { get; set; }

		[DataMemberAttribute(Name = "writeoff_total_count")]
		public long? WriteoffTotalCount { get; set; }

	}
}
