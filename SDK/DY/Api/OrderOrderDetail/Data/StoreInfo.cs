using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class StoreInfo
	{
		[DataMemberAttribute(Name = "store_id")]
		public string StoreId { get; set; }

		[DataMemberAttribute(Name = "store_name")]
		public string StoreName { get; set; }

		[DataMemberAttribute(Name = "store_tel")]
		public string StoreTel { get; set; }

		[DataMemberAttribute(Name = "store_address")]
		public StoreAddress StoreAddress { get; set; }

		[DataMemberAttribute(Name = "extra")]
		public string Extra { get; set; }

	}
}
