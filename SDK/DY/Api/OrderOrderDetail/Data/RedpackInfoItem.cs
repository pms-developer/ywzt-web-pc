using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class RedpackInfoItem
	{
		[DataMemberAttribute(Name = "redpack_trans_id")]
		public string RedpackTransId { get; set; }

		[DataMemberAttribute(Name = "redpack_amount")]
		public long? RedpackAmount { get; set; }

		[DataMemberAttribute(Name = "share_discount_cost")]
		public ShareDiscountCost ShareDiscountCost { get; set; }

	}
}
