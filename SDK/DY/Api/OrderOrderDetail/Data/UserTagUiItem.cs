using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class UserTagUiItem
	{
		[DataMemberAttribute(Name = "key")]
		public string Key { get; set; }

		[DataMemberAttribute(Name = "text")]
		public string Text { get; set; }

	}
}
