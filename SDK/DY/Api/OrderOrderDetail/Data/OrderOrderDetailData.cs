using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class OrderOrderDetailData
	{
		[DataMemberAttribute(Name = "shop_order_detail")]
		public ShopOrderDetail ShopOrderDetail { get; set; }

	}
}
