using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class SkuOrderTagUiItem
	{
		[DataMemberAttribute(Name = "key")]
		public string Key { get; set; }

		[DataMemberAttribute(Name = "text")]
		public string Text { get; set; }

		[DataMemberAttribute(Name = "hover_text")]
		public string HoverText { get; set; }

		[DataMemberAttribute(Name = "tag_type")]
		public string TagType { get; set; }

		[DataMemberAttribute(Name = "help_doc")]
		public string HelpDoc { get; set; }

		[DataMemberAttribute(Name = "sort")]
		public long? Sort { get; set; }

		[DataMemberAttribute(Name = "extra")]
		public string Extra { get; set; }

	}
}
