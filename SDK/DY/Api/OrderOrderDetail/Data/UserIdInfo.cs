using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class UserIdInfo
	{
		[DataMemberAttribute(Name = "id_card_no")]
		public string IdCardNo { get; set; }

		[DataMemberAttribute(Name = "encrypt_id_card_no")]
		public string EncryptIdCardNo { get; set; }

		[DataMemberAttribute(Name = "id_card_name")]
		public string IdCardName { get; set; }

		[DataMemberAttribute(Name = "encrypt_id_card_name")]
		public string EncryptIdCardName { get; set; }

	}
}
