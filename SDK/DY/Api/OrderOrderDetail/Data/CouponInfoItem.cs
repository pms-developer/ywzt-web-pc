using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class CouponInfoItem
	{
		[DataMemberAttribute(Name = "coupon_id")]
		public long? CouponId { get; set; }

		[DataMemberAttribute(Name = "coupon_type")]
		public long? CouponType { get; set; }

		[DataMemberAttribute(Name = "coupon_meta_id")]
		public string CouponMetaId { get; set; }

		[DataMemberAttribute(Name = "coupon_amount")]
		public long? CouponAmount { get; set; }

		[DataMemberAttribute(Name = "coupon_name")]
		public string CouponName { get; set; }

		[DataMemberAttribute(Name = "share_discount_cost")]
		public ShareDiscountCost ShareDiscountCost { get; set; }

	}
}
