using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class ShareDiscountCost
	{
		[DataMemberAttribute(Name = "platform_cost")]
		public long? PlatformCost { get; set; }

		[DataMemberAttribute(Name = "shop_cost")]
		public long? ShopCost { get; set; }

		[DataMemberAttribute(Name = "author_cost")]
		public long? AuthorCost { get; set; }

	}
}
