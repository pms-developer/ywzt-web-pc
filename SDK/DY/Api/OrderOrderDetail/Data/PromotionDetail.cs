using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class PromotionDetail
	{
		[DataMemberAttribute(Name = "shop_discount_detail")]
		public ShopDiscountDetail ShopDiscountDetail { get; set; }

		[DataMemberAttribute(Name = "platform_discount_detail")]
		public PlatformDiscountDetail PlatformDiscountDetail { get; set; }

		[DataMemberAttribute(Name = "kol_discount_detail")]
		public KolDiscountDetail KolDiscountDetail { get; set; }

	}
}
