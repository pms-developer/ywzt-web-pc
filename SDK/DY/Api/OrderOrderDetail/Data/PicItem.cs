using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Data
{
	public class PicItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
