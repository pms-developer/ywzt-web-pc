using System.Runtime.Serialization;

namespace Dop.Api.OrderOrderDetail.Param
{
	public class OrderOrderDetailParam
	{
		[DataMemberAttribute(Name = "shop_order_id")]
		public string ShopOrderId { get; set; }

		[DataMemberAttribute(Name = "is_searchable")]
		public bool IsSearchable { get; set; }

	}
}
