using System;
using Dop.Core;
using Dop.Api.OrderOrderDetail.Param;

namespace Dop.Api.OrderOrderDetail
{
	public class OrderOrderDetailRequest : DoudianOpApiRequest<OrderOrderDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/order/orderDetail";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderOrderDetailResponse);
		}

		public  OrderOrderDetailParam BuildParam()
		{
			return Param ?? (Param = new OrderOrderDetailParam());
		}

	}
}
