using System;
using Dop.Core;
using Dop.Api.ProductAddCbProduct.Param;

namespace Dop.Api.ProductAddCbProduct
{
	public class ProductAddCbProductRequest : DoudianOpApiRequest<ProductAddCbProductParam>
	{
		public override string GetUrlPath()
		{
			return "/product/addCbProduct";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductAddCbProductResponse);
		}

		public  ProductAddCbProductParam BuildParam()
		{
			return Param ?? (Param = new ProductAddCbProductParam());
		}

	}
}
