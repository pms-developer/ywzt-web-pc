using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductAddCbProduct.Param
{
	public class ProductAddCbProductParam
	{
		[DataMemberAttribute(Name = "outer_product_id")]
		public string OuterProductId { get; set; }

		[DataMemberAttribute(Name = "product_type")]
		public long? ProductType { get; set; }

		[DataMemberAttribute(Name = "category_leaf_id")]
		public long? CategoryLeafId { get; set; }

		[DataMemberAttribute(Name = "product_format")]
		public string ProductFormat { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "recommend_remark")]
		public string RecommendRemark { get; set; }

		[DataMemberAttribute(Name = "pic")]
		public string Pic { get; set; }

		[DataMemberAttribute(Name = "description")]
		public string Description { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public long? PayType { get; set; }

		[DataMemberAttribute(Name = "delivery_method")]
		public int? DeliveryMethod { get; set; }

		[DataMemberAttribute(Name = "cdf_category")]
		public string CdfCategory { get; set; }

		[DataMemberAttribute(Name = "reduce_type")]
		public long? ReduceType { get; set; }

		[DataMemberAttribute(Name = "assoc_ids")]
		public string AssocIds { get; set; }

		[DataMemberAttribute(Name = "freight_id")]
		public long? FreightId { get; set; }

		[DataMemberAttribute(Name = "weight")]
		public double? Weight { get; set; }

		[DataMemberAttribute(Name = "weight_unit")]
		public long? WeightUnit { get; set; }

		[DataMemberAttribute(Name = "delivery_delay_day")]
		public long? DeliveryDelayDay { get; set; }

		[DataMemberAttribute(Name = "presell_type")]
		public long? PresellType { get; set; }

		[DataMemberAttribute(Name = "presell_delay")]
		public long? PresellDelay { get; set; }

		[DataMemberAttribute(Name = "presell_end_time")]
		public string PresellEndTime { get; set; }

		[DataMemberAttribute(Name = "supply_7day_return")]
		public long? Supply7dayReturn { get; set; }

		[DataMemberAttribute(Name = "mobile")]
		public string Mobile { get; set; }

		[DataMemberAttribute(Name = "commit")]
		public bool? Commit { get; set; }

		[DataMemberAttribute(Name = "brand_id")]
		public long? BrandId { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public long? OutProductId { get; set; }

		[DataMemberAttribute(Name = "quality_list")]
		public List<QualityListItem> QualityList { get; set; }

		[DataMemberAttribute(Name = "spec_name")]
		public string SpecName { get; set; }

		[DataMemberAttribute(Name = "specs")]
		public string Specs { get; set; }

		[DataMemberAttribute(Name = "spec_prices")]
		public string SpecPrices { get; set; }

		[DataMemberAttribute(Name = "spec_pic")]
		public string SpecPic { get; set; }

		[DataMemberAttribute(Name = "maximum_per_order")]
		public long? MaximumPerOrder { get; set; }

		[DataMemberAttribute(Name = "limit_per_buyer")]
		public long? LimitPerBuyer { get; set; }

		[DataMemberAttribute(Name = "minimum_per_order")]
		public long? MinimumPerOrder { get; set; }

		[DataMemberAttribute(Name = "product_format_new")]
		public string ProductFormatNew { get; set; }

		[DataMemberAttribute(Name = "spu_id")]
		public long? SpuId { get; set; }

		[DataMemberAttribute(Name = "appoint_delivery_day")]
		public long? AppointDeliveryDay { get; set; }

		[DataMemberAttribute(Name = "third_url")]
		public string ThirdUrl { get; set; }

		[DataMemberAttribute(Name = "extra")]
		public string Extra { get; set; }

		[DataMemberAttribute(Name = "src")]
		public string Src { get; set; }

		[DataMemberAttribute(Name = "need_check_out")]
		public bool? NeedCheckOut { get; set; }

		[DataMemberAttribute(Name = "poi_resource")]
		public PoiResource PoiResource { get; set; }

		[DataMemberAttribute(Name = "car_vin_code")]
		public string CarVinCode { get; set; }

		[DataMemberAttribute(Name = "need_recharge_mode")]
		public bool? NeedRechargeMode { get; set; }

		[DataMemberAttribute(Name = "presell_config_level")]
		public long? PresellConfigLevel { get; set; }

		[DataMemberAttribute(Name = "account_template_id")]
		public string AccountTemplateId { get; set; }

		[DataMemberAttribute(Name = "presell_delivery_type")]
		public long? PresellDeliveryType { get; set; }

		[DataMemberAttribute(Name = "white_back_ground_pic_url")]
		public string WhiteBackGroundPicUrl { get; set; }

		[DataMemberAttribute(Name = "long_pic_url")]
		public string LongPicUrl { get; set; }

		[DataMemberAttribute(Name = "after_sale_service")]
		public Dictionary<string,string> AfterSaleService { get; set; }

		[DataMemberAttribute(Name = "sell_channel")]
		public List<long> SellChannel { get; set; }

		[DataMemberAttribute(Name = "start_sale_type")]
		public long? StartSaleType { get; set; }

		[DataMemberAttribute(Name = "logistics_info")]
		public string LogisticsInfo { get; set; }

		[DataMemberAttribute(Name = "price_has_tax")]
		public string PriceHasTax { get; set; }

		[DataMemberAttribute(Name = "biz_kind")]
		public long? BizKind { get; set; }

		[DataMemberAttribute(Name = "standard_brand_id")]
		public long? StandardBrandId { get; set; }

		[DataMemberAttribute(Name = "discount_price")]
		public long? DiscountPrice { get; set; }

		[DataMemberAttribute(Name = "market_price")]
		public long? MarketPrice { get; set; }

	}
}
