using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductListV2.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "check_status")]
		public long? CheckStatus { get; set; }

		[DataMemberAttribute(Name = "market_price")]
		public long? MarketPrice { get; set; }

		[DataMemberAttribute(Name = "discount_price")]
		public long? DiscountPrice { get; set; }

		[DataMemberAttribute(Name = "img")]
		public string Img { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public long? PayType { get; set; }

		[DataMemberAttribute(Name = "product_type")]
		public long? ProductType { get; set; }

		[DataMemberAttribute(Name = "spec_id")]
		public long? SpecId { get; set; }

		[DataMemberAttribute(Name = "cos_ratio")]
		public long? CosRatio { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public long? UpdateTime { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public long? OutProductId { get; set; }

		[DataMemberAttribute(Name = "description")]
		public string Description { get; set; }

		[DataMemberAttribute(Name = "mobile")]
		public string Mobile { get; set; }

		[DataMemberAttribute(Name = "extra")]
		public string Extra { get; set; }

		[DataMemberAttribute(Name = "recommend_remark")]
		public string RecommendRemark { get; set; }

		[DataMemberAttribute(Name = "category_detail")]
		public CategoryDetail CategoryDetail { get; set; }

		[DataMemberAttribute(Name = "outer_product_id")]
		public string OuterProductId { get; set; }

		[DataMemberAttribute(Name = "is_package_product")]
		public bool? IsPackageProduct { get; set; }

		[DataMemberAttribute(Name = "package_product_list")]
		public List<long> PackageProductList { get; set; }

		[DataMemberAttribute(Name = "sub_product_list")]
		public List<long> SubProductList { get; set; }

	}
}
