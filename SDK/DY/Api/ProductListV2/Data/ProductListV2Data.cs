using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductListV2.Data
{
	public class ProductListV2Data
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

	}
}
