using System.Runtime.Serialization;

namespace Dop.Api.ProductListV2.Param
{
	public class ProductListV2Param
	{
		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "check_status")]
		public long? CheckStatus { get; set; }

		[DataMemberAttribute(Name = "product_type")]
		public long? ProductType { get; set; }

		[DataMemberAttribute(Name = "start_time")]
		public long? StartTime { get; set; }

		[DataMemberAttribute(Name = "end_time")]
		public long? EndTime { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "update_start_time")]
		public long? UpdateStartTime { get; set; }

		[DataMemberAttribute(Name = "update_end_time")]
		public long? UpdateEndTime { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

	}
}
