using System;
using Dop.Core;
using Dop.Api.ProductListV2.Param;

namespace Dop.Api.ProductListV2
{
	public class ProductListV2Request : DoudianOpApiRequest<ProductListV2Param>
	{
		public override string GetUrlPath()
		{
			return "/product/listV2";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductListV2Response);
		}

		public  ProductListV2Param BuildParam()
		{
			return Param ?? (Param = new ProductListV2Param());
		}

	}
}
