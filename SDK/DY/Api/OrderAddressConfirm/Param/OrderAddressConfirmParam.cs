using System.Runtime.Serialization;

namespace Dop.Api.OrderAddressConfirm.Param
{
	public class OrderAddressConfirmParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "is_approved")]
		public long? IsApproved { get; set; }

	}
}
