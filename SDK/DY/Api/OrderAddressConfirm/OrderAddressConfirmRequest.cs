using System;
using Dop.Core;
using Dop.Api.OrderAddressConfirm.Param;

namespace Dop.Api.OrderAddressConfirm
{
	public class OrderAddressConfirmRequest : DoudianOpApiRequest<OrderAddressConfirmParam>
	{
		public override string GetUrlPath()
		{
			return "/order/addressConfirm";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderAddressConfirmResponse);
		}

		public  OrderAddressConfirmParam BuildParam()
		{
			return Param ?? (Param = new OrderAddressConfirmParam());
		}

	}
}
