using Dop.Core;
using Dop.Api.OrderAddressConfirm.Data;

namespace Dop.Api.OrderAddressConfirm
{
	public class OrderAddressConfirmResponse : DoudianOpApiResponse<OrderAddressConfirmData>
	{
	}
}
