using System;
using Dop.Core;
using Dop.Api.SpuBatchUploadImg.Param;

namespace Dop.Api.SpuBatchUploadImg
{
	public class SpuBatchUploadImgRequest : DoudianOpApiRequest<SpuBatchUploadImgParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/batchUploadImg";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuBatchUploadImgResponse);
		}

		public  SpuBatchUploadImgParam BuildParam()
		{
			return Param ?? (Param = new SpuBatchUploadImgParam());
		}

	}
}
