using System.Runtime.Serialization;

namespace Dop.Api.SpuBatchUploadImg.Param
{
	public class ImageUrlListItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
