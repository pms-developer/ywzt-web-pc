using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SpuBatchUploadImg.Param
{
	public class SpuBatchUploadImgParam
	{
		[DataMemberAttribute(Name = "image_url_list")]
		public List<ImageUrlListItem> ImageUrlList { get; set; }

	}
}
