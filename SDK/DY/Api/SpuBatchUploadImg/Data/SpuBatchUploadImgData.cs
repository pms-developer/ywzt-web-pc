using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SpuBatchUploadImg.Data
{
	public class SpuBatchUploadImgData
	{
		[DataMemberAttribute(Name = "success_list")]
		public List<SuccessListItem> SuccessList { get; set; }

		[DataMemberAttribute(Name = "failed_list")]
		public List<FailedListItem> FailedList { get; set; }

	}
}
