using System.Runtime.Serialization;

namespace Dop.Api.SpuBatchUploadImg.Data
{
	public class FailedListItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "origin_url")]
		public string OriginUrl { get; set; }

		[DataMemberAttribute(Name = "err_code")]
		public int? ErrCode { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

	}
}
