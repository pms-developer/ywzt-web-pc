using System.Runtime.Serialization;

namespace Dop.Api.SpuBatchUploadImg.Data
{
	public class SuccessListItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "origin_url")]
		public string OriginUrl { get; set; }

		[DataMemberAttribute(Name = "spu_url")]
		public string SpuUrl { get; set; }

	}
}
