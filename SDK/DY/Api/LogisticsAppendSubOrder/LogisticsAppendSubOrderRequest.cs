using System;
using Dop.Core;
using Dop.Api.LogisticsAppendSubOrder.Param;

namespace Dop.Api.LogisticsAppendSubOrder
{
	public class LogisticsAppendSubOrderRequest : DoudianOpApiRequest<LogisticsAppendSubOrderParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/appendSubOrder";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsAppendSubOrderResponse);
		}

		public  LogisticsAppendSubOrderParam BuildParam()
		{
			return Param ?? (Param = new LogisticsAppendSubOrderParam());
		}

	}
}
