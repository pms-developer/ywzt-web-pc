using System.Runtime.Serialization;

namespace Dop.Api.LogisticsAppendSubOrder.Param
{
	public class LogisticsAppendSubOrderParam
	{
		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "pack_add_quantity")]
		public int? PackAddQuantity { get; set; }

		[DataMemberAttribute(Name = "is_return_full_sub_codes")]
		public bool? IsReturnFullSubCodes { get; set; }

	}
}
