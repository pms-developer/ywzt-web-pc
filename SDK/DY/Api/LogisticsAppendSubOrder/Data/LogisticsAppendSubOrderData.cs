using System.Runtime.Serialization;

namespace Dop.Api.LogisticsAppendSubOrder.Data
{
	public class LogisticsAppendSubOrderData
	{
		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "pack_quantity")]
		public long? PackQuantity { get; set; }

		[DataMemberAttribute(Name = "sub_waybill_codes")]
		public string SubWaybillCodes { get; set; }

	}
}
