using System;
using Dop.Core;
using Dop.Api.OrderDownloadShopAccountItem.Param;

namespace Dop.Api.OrderDownloadShopAccountItem
{
	public class OrderDownloadShopAccountItemRequest : DoudianOpApiRequest<OrderDownloadShopAccountItemParam>
	{
		public override string GetUrlPath()
		{
			return "/order/downloadShopAccountItem";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderDownloadShopAccountItemResponse);
		}

		public  OrderDownloadShopAccountItemParam BuildParam()
		{
			return Param ?? (Param = new OrderDownloadShopAccountItemParam());
		}

	}
}
