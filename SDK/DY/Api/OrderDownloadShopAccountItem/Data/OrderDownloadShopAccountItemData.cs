using System.Runtime.Serialization;

namespace Dop.Api.OrderDownloadShopAccountItem.Data
{
	public class OrderDownloadShopAccountItemData
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "code_msg")]
		public string CodeMsg { get; set; }

		[DataMemberAttribute(Name = "download_id")]
		public string DownloadId { get; set; }

	}
}
