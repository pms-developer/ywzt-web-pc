using System;
using Dop.Core;
using Dop.Api.ProductAddV2.Param;

namespace Dop.Api.ProductAddV2
{
	public class ProductAddV2Request : DoudianOpApiRequest<ProductAddV2Param>
	{
		public override string GetUrlPath()
		{
			return "/product/addV2";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductAddV2Response);
		}

		public  ProductAddV2Param BuildParam()
		{
			return Param ?? (Param = new ProductAddV2Param());
		}

	}
}
