using System.Runtime.Serialization;

namespace Dop.Api.ProductAddV2.Data
{
	public class SkuItem
	{
		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "out_sku_id")]
		public long? OutSkuId { get; set; }

		[DataMemberAttribute(Name = "outer_sku_id")]
		public string OuterSkuId { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id1")]
		public long? SpecDetailId1 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id2")]
		public long? SpecDetailId2 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id3")]
		public long? SpecDetailId3 { get; set; }

	}
}
