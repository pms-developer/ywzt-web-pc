using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductAddV2.Data
{
	public class ProductAddV2Data
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public long? OutProductId { get; set; }

		[DataMemberAttribute(Name = "outer_product_id")]
		public string OuterProductId { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "sku")]
		public List<SkuItem> Sku { get; set; }

	}
}
