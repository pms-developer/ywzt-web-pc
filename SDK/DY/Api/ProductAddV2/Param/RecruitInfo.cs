using System.Runtime.Serialization;

namespace Dop.Api.ProductAddV2.Param
{
	public class RecruitInfo
	{
		[DataMemberAttribute(Name = "recruit_follow_id")]
		public string RecruitFollowId { get; set; }

		[DataMemberAttribute(Name = "recruit_type")]
		public string RecruitType { get; set; }

	}
}
