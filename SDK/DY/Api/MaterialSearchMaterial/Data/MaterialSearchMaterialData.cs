using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialSearchMaterial.Data
{
	public class MaterialSearchMaterialData
	{
		[DataMemberAttribute(Name = "material_info_list")]
		public List<MaterialInfoListItem> MaterialInfoList { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
