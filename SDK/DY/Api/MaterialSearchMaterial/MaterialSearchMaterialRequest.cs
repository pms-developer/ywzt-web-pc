using System;
using Dop.Core;
using Dop.Api.MaterialSearchMaterial.Param;

namespace Dop.Api.MaterialSearchMaterial
{
	public class MaterialSearchMaterialRequest : DoudianOpApiRequest<MaterialSearchMaterialParam>
	{
		public override string GetUrlPath()
		{
			return "/material/searchMaterial";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialSearchMaterialResponse);
		}

		public  MaterialSearchMaterialParam BuildParam()
		{
			return Param ?? (Param = new MaterialSearchMaterialParam());
		}

	}
}
