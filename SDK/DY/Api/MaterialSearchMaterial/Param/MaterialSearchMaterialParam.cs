using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.MaterialSearchMaterial.Param
{
	public class MaterialSearchMaterialParam
	{
		[DataMemberAttribute(Name = "material_id")]
		public string MaterialId { get; set; }

		[DataMemberAttribute(Name = "material_name")]
		public string MaterialName { get; set; }

		[DataMemberAttribute(Name = "material_type")]
		public List<string> MaterialType { get; set; }

		[DataMemberAttribute(Name = "audit_status")]
		public List<int> AuditStatus { get; set; }

		[DataMemberAttribute(Name = "create_time_start")]
		public string CreateTimeStart { get; set; }

		[DataMemberAttribute(Name = "create_time_end")]
		public string CreateTimeEnd { get; set; }

		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "page_num")]
		public int? PageNum { get; set; }

		[DataMemberAttribute(Name = "page_size")]
		public int? PageSize { get; set; }

		[DataMemberAttribute(Name = "order_type")]
		public int? OrderType { get; set; }

		[DataMemberAttribute(Name = "operate_status")]
		public List<int> OperateStatus { get; set; }

	}
}
