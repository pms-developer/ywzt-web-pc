using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateUpdate.Param
{
	public class ColumnsItem
	{
		[DataMemberAttribute(Name = "first_weight")]
		public double? FirstWeight { get; set; }

		[DataMemberAttribute(Name = "first_weight_price")]
		public double? FirstWeightPrice { get; set; }

		[DataMemberAttribute(Name = "first_num")]
		public long? FirstNum { get; set; }

		[DataMemberAttribute(Name = "first_num_price")]
		public double? FirstNumPrice { get; set; }

		[DataMemberAttribute(Name = "add_weight")]
		public double? AddWeight { get; set; }

		[DataMemberAttribute(Name = "add_weight_price")]
		public double? AddWeightPrice { get; set; }

		[DataMemberAttribute(Name = "add_num")]
		public long? AddNum { get; set; }

		[DataMemberAttribute(Name = "add_num_price")]
		public double? AddNumPrice { get; set; }

		[DataMemberAttribute(Name = "is_default")]
		public long? IsDefault { get; set; }

		[DataMemberAttribute(Name = "is_limited")]
		public bool? IsLimited { get; set; }

		[DataMemberAttribute(Name = "rule_address")]
		public string RuleAddress { get; set; }

		[DataMemberAttribute(Name = "is_over_free")]
		public bool? IsOverFree { get; set; }

		[DataMemberAttribute(Name = "over_weight")]
		public double? OverWeight { get; set; }

		[DataMemberAttribute(Name = "over_amount")]
		public long? OverAmount { get; set; }

		[DataMemberAttribute(Name = "over_num")]
		public long? OverNum { get; set; }

	}
}
