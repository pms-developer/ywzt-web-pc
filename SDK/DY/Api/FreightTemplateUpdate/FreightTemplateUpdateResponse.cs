using Dop.Core;
using Dop.Api.FreightTemplateUpdate.Data;

namespace Dop.Api.FreightTemplateUpdate
{
	public class FreightTemplateUpdateResponse : DoudianOpApiResponse<FreightTemplateUpdateData>
	{
	}
}
