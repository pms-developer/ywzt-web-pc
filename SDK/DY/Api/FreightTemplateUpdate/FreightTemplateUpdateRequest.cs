using System;
using Dop.Core;
using Dop.Api.FreightTemplateUpdate.Param;

namespace Dop.Api.FreightTemplateUpdate
{
	public class FreightTemplateUpdateRequest : DoudianOpApiRequest<FreightTemplateUpdateParam>
	{
		public override string GetUrlPath()
		{
			return "/freightTemplate/update";
		}

		public override Type GetResponseType()
		{
			return typeof(FreightTemplateUpdateResponse);
		}

		public  FreightTemplateUpdateParam BuildParam()
		{
			return Param ?? (Param = new FreightTemplateUpdateParam());
		}

	}
}
