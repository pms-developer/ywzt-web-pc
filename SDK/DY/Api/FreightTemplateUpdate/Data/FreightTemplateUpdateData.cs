using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateUpdate.Data
{
	public class FreightTemplateUpdateData
	{
		[DataMemberAttribute(Name = "template_id")]
		public long? TemplateId { get; set; }

	}
}
