using System;
using Dop.Core;
using Dop.Api.TokenCreate.Param;

namespace Dop.Api.TokenCreate
{
	public class TokenCreateRequest : DoudianOpApiRequest<TokenCreateParam>
	{
		public override string GetUrlPath()
		{
			return "/token/create";
		}

		public override Type GetResponseType()
		{
			return typeof(TokenCreateResponse);
		}

		public  TokenCreateParam BuildParam()
		{
			return Param ?? (Param = new TokenCreateParam());
		}

	}
}
