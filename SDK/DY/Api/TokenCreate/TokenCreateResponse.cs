using Dop.Core;
using Dop.Api.TokenCreate.Data;

namespace Dop.Api.TokenCreate
{
	public class TokenCreateResponse : DoudianOpApiResponse<TokenCreateData>
	{
	}
}
