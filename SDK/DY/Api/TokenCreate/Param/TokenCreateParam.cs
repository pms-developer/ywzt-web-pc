using System.Runtime.Serialization;

namespace Dop.Api.TokenCreate.Param
{
	public class TokenCreateParam
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "grant_type")]
		public string GrantType { get; set; }

		[DataMemberAttribute(Name = "test_shop")]
		public string TestShop { get; set; }

		[DataMemberAttribute(Name = "shop_id")]
		public string ShopId { get; set; }

		[DataMemberAttribute(Name = "auth_id")]
		public string AuthId { get; set; }

		[DataMemberAttribute(Name = "auth_subject_type")]
		public string AuthSubjectType { get; set; }

	}
}
