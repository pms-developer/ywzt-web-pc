using System;
using Dop.Core;
using Dop.Api.AntispamUserLogin.Param;

namespace Dop.Api.AntispamUserLogin
{
	public class AntispamUserLoginRequest : DoudianOpApiRequest<AntispamUserLoginParam>
	{
		public override string GetUrlPath()
		{
			return "/antispam/userLogin";
		}

		public override Type GetResponseType()
		{
			return typeof(AntispamUserLoginResponse);
		}

		public  AntispamUserLoginParam BuildParam()
		{
			return Param ?? (Param = new AntispamUserLoginParam());
		}

	}
}
