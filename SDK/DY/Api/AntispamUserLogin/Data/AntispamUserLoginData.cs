using System.Runtime.Serialization;

namespace Dop.Api.AntispamUserLogin.Data
{
	public class AntispamUserLoginData
	{
		[DataMemberAttribute(Name = "decision")]
		public Decision Decision { get; set; }

	}
}
