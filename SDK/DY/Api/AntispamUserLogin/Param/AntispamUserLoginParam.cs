using System.Runtime.Serialization;

namespace Dop.Api.AntispamUserLogin.Param
{
	public class AntispamUserLoginParam
	{
		[DataMemberAttribute(Name = "params")]
		public string Params { get; set; }

		[DataMemberAttribute(Name = "event_time")]
		public long? EventTime { get; set; }

		[DataMemberAttribute(Name = "user")]
		public User User { get; set; }

	}
}
