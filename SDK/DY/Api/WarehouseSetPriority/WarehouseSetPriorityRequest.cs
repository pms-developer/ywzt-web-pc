using System;
using Dop.Core;
using Dop.Api.WarehouseSetPriority.Param;

namespace Dop.Api.WarehouseSetPriority
{
	public class WarehouseSetPriorityRequest : DoudianOpApiRequest<WarehouseSetPriorityParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/setPriority";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseSetPriorityResponse);
		}

		public  WarehouseSetPriorityParam BuildParam()
		{
			return Param ?? (Param = new WarehouseSetPriorityParam());
		}

	}
}
