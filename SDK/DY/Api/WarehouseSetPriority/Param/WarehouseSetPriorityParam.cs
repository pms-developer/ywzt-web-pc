using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.WarehouseSetPriority.Param
{
	public class WarehouseSetPriorityParam
	{
		[DataMemberAttribute(Name = "addr")]
		public Addr Addr { get; set; }

		[DataMemberAttribute(Name = "priorities")]
		public Dictionary<string,long> Priorities { get; set; }

	}
}
