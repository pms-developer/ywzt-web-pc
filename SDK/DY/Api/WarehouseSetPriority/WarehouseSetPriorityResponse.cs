using Dop.Core;
using Dop.Api.WarehouseSetPriority.Data;

namespace Dop.Api.WarehouseSetPriority
{
	public class WarehouseSetPriorityResponse : DoudianOpApiResponse<WarehouseSetPriorityData>
	{
	}
}
