using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCreateSFOrder.Data
{
	public class RouteLabelData
	{
		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "source_transfer_code")]
		public string SourceTransferCode { get; set; }

		[DataMemberAttribute(Name = "source_city_code")]
		public string SourceCityCode { get; set; }

		[DataMemberAttribute(Name = "source_dept_code")]
		public string SourceDeptCode { get; set; }

		[DataMemberAttribute(Name = "sourceTeamCode")]
		public string SourceTeamCode { get; set; }

		[DataMemberAttribute(Name = "destCityCode")]
		public string DestCityCode { get; set; }

		[DataMemberAttribute(Name = "destDeptCode")]
		public string DestDeptCode { get; set; }

		[DataMemberAttribute(Name = "dest_dept_code_mapping")]
		public string DestDeptCodeMapping { get; set; }

		[DataMemberAttribute(Name = "dest_team_code")]
		public string DestTeamCode { get; set; }

		[DataMemberAttribute(Name = "dest_team_code_mapping")]
		public string DestTeamCodeMapping { get; set; }

		[DataMemberAttribute(Name = "dest_transfer_code")]
		public string DestTransferCode { get; set; }

		[DataMemberAttribute(Name = "dest_route_label")]
		public string DestRouteLabel { get; set; }

		[DataMemberAttribute(Name = "pro_name")]
		public string ProName { get; set; }

		[DataMemberAttribute(Name = "cargo_type_code")]
		public string CargoTypeCode { get; set; }

		[DataMemberAttribute(Name = "limit_type_code")]
		public string LimitTypeCode { get; set; }

		[DataMemberAttribute(Name = "express_type_code")]
		public string ExpressTypeCode { get; set; }

		[DataMemberAttribute(Name = "coding_mapping")]
		public string CodingMapping { get; set; }

		[DataMemberAttribute(Name = "coding_mapping_out")]
		public string CodingMappingOut { get; set; }

		[DataMemberAttribute(Name = "xb_flag")]
		public string XbFlag { get; set; }

		[DataMemberAttribute(Name = "print_flag")]
		public string PrintFlag { get; set; }

		[DataMemberAttribute(Name = "two_dimension_code")]
		public string TwoDimensionCode { get; set; }

		[DataMemberAttribute(Name = "pro_code")]
		public string ProCode { get; set; }

		[DataMemberAttribute(Name = "print_icon")]
		public string PrintIcon { get; set; }

		[DataMemberAttribute(Name = "ab_flag")]
		public string AbFlag { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

	}
}
