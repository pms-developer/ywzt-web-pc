using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCreateSFOrder.Data
{
	public class WaybillNoInfoListItem
	{
		[DataMemberAttribute(Name = "waybill_type")]
		public int? WaybillType { get; set; }

		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

	}
}
