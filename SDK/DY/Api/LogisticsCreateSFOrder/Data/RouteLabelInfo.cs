using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCreateSFOrder.Data
{
	public class RouteLabelInfo
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "route_label_data")]
		public RouteLabelData RouteLabelData { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

	}
}
