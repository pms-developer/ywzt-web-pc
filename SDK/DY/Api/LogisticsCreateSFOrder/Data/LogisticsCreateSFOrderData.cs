using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsCreateSFOrder.Data
{
	public class LogisticsCreateSFOrderData
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "pack_id")]
		public string PackId { get; set; }

		[DataMemberAttribute(Name = "origin_code")]
		public string OriginCode { get; set; }

		[DataMemberAttribute(Name = "dest_code")]
		public string DestCode { get; set; }

		[DataMemberAttribute(Name = "filter_result")]
		public int? FilterResult { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "waybill_no_info_list")]
		public List<WaybillNoInfoListItem> WaybillNoInfoList { get; set; }

		[DataMemberAttribute(Name = "route_label_info")]
		public RouteLabelInfo RouteLabelInfo { get; set; }

		[DataMemberAttribute(Name = "order_channel")]
		public string OrderChannel { get; set; }

	}
}
