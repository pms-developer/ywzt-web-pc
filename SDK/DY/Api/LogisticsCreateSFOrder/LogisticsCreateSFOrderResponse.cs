using Dop.Core;
using Dop.Api.LogisticsCreateSFOrder.Data;

namespace Dop.Api.LogisticsCreateSFOrder
{
	public class LogisticsCreateSFOrderResponse : DoudianOpApiResponse<LogisticsCreateSFOrderData>
	{
	}
}
