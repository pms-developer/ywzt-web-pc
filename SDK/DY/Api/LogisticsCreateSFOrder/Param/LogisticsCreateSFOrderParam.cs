using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsCreateSFOrder.Param
{
	public class LogisticsCreateSFOrderParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "pack_id")]
		public string PackId { get; set; }

		[DataMemberAttribute(Name = "cargo_details")]
		public List<CargoDetailsItem> CargoDetails { get; set; }

		[DataMemberAttribute(Name = "service_list")]
		public List<ServiceListItem> ServiceList { get; set; }

		[DataMemberAttribute(Name = "contact_info_list")]
		public List<ContactInfoListItem> ContactInfoList { get; set; }

		[DataMemberAttribute(Name = "pay_method")]
		public int? PayMethod { get; set; }

		[DataMemberAttribute(Name = "express_type_id")]
		public int? ExpressTypeId { get; set; }

		[DataMemberAttribute(Name = "parcel_qty")]
		public int? ParcelQty { get; set; }

		[DataMemberAttribute(Name = "total_weight")]
		public string TotalWeight { get; set; }

		[DataMemberAttribute(Name = "is_sign_back")]
		public int? IsSignBack { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "total_length")]
		public string TotalLength { get; set; }

		[DataMemberAttribute(Name = "total_width")]
		public string TotalWidth { get; set; }

		[DataMemberAttribute(Name = "total_height")]
		public string TotalHeight { get; set; }

		[DataMemberAttribute(Name = "volume")]
		public string Volume { get; set; }

		[DataMemberAttribute(Name = "user_id")]
		public long? UserId { get; set; }

		[DataMemberAttribute(Name = "order_channel")]
		public string OrderChannel { get; set; }

	}
}
