using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCreateSFOrder.Param
{
	public class ServiceListItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "value")]
		public string Value { get; set; }

		[DataMemberAttribute(Name = "value1")]
		public string Value1 { get; set; }

		[DataMemberAttribute(Name = "value2")]
		public string Value2 { get; set; }

		[DataMemberAttribute(Name = "value3")]
		public string Value3 { get; set; }

		[DataMemberAttribute(Name = "value4")]
		public string Value4 { get; set; }

	}
}
