using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCreateSFOrder.Param
{
	public class ContactInfoListItem
	{
		[DataMemberAttribute(Name = "contact_type")]
		public int? ContactType { get; set; }

		[DataMemberAttribute(Name = "biz_company")]
		public string BizCompany { get; set; }

		[DataMemberAttribute(Name = "contact")]
		public string Contact { get; set; }

		[DataMemberAttribute(Name = "tel")]
		public string Tel { get; set; }

		[DataMemberAttribute(Name = "mobile")]
		public string Mobile { get; set; }

		[DataMemberAttribute(Name = "country")]
		public string Country { get; set; }

		[DataMemberAttribute(Name = "province")]
		public string Province { get; set; }

		[DataMemberAttribute(Name = "city")]
		public string City { get; set; }

		[DataMemberAttribute(Name = "county")]
		public string County { get; set; }

		[DataMemberAttribute(Name = "address")]
		public string Address { get; set; }

	}
}
