using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCreateSFOrder.Param
{
	public class CargoDetailsItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "currency")]
		public string Currency { get; set; }

		[DataMemberAttribute(Name = "count")]
		public long? Count { get; set; }

		[DataMemberAttribute(Name = "unit")]
		public string Unit { get; set; }

		[DataMemberAttribute(Name = "amount")]
		public string Amount { get; set; }

		[DataMemberAttribute(Name = "weight")]
		public string Weight { get; set; }

		[DataMemberAttribute(Name = "source_area")]
		public string SourceArea { get; set; }

	}
}
