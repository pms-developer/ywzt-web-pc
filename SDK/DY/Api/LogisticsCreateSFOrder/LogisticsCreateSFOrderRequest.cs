using System;
using Dop.Core;
using Dop.Api.LogisticsCreateSFOrder.Param;

namespace Dop.Api.LogisticsCreateSFOrder
{
	public class LogisticsCreateSFOrderRequest : DoudianOpApiRequest<LogisticsCreateSFOrderParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/createSFOrder";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsCreateSFOrderResponse);
		}

		public  LogisticsCreateSFOrderParam BuildParam()
		{
			return Param ?? (Param = new LogisticsCreateSFOrderParam());
		}

	}
}
