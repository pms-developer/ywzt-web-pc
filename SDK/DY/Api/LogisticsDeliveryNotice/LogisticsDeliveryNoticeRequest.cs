using System;
using Dop.Core;
using Dop.Api.LogisticsDeliveryNotice.Param;

namespace Dop.Api.LogisticsDeliveryNotice
{
	public class LogisticsDeliveryNoticeRequest : DoudianOpApiRequest<LogisticsDeliveryNoticeParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/deliveryNotice";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsDeliveryNoticeResponse);
		}

		public  LogisticsDeliveryNoticeParam BuildParam()
		{
			return Param ?? (Param = new LogisticsDeliveryNoticeParam());
		}

	}
}
