using System.Runtime.Serialization;

namespace Dop.Api.LogisticsDeliveryNotice.Param
{
	public class LogisticsDeliveryNoticeParam
	{
		[DataMemberAttribute(Name = "waybill_no")]
		public string WaybillNo { get; set; }

		[DataMemberAttribute(Name = "notice_type")]
		public string NoticeType { get; set; }

	}
}
