using System.Runtime.Serialization;

namespace Dop.Api.LogisticsDeliveryNotice.Data
{
	public class LogisticsDeliveryNoticeData
	{
		[DataMemberAttribute(Name = "result")]
		public bool? Result { get; set; }

	}
}
