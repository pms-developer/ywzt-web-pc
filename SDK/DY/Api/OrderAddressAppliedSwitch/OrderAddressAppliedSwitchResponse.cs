using Dop.Core;
using Dop.Api.OrderAddressAppliedSwitch.Data;

namespace Dop.Api.OrderAddressAppliedSwitch
{
	public class OrderAddressAppliedSwitchResponse : DoudianOpApiResponse<OrderAddressAppliedSwitchData>
	{
	}
}
