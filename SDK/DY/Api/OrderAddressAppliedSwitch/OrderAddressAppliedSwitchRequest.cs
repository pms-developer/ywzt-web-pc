using System;
using Dop.Core;
using Dop.Api.OrderAddressAppliedSwitch.Param;

namespace Dop.Api.OrderAddressAppliedSwitch
{
	public class OrderAddressAppliedSwitchRequest : DoudianOpApiRequest<OrderAddressAppliedSwitchParam>
	{
		public override string GetUrlPath()
		{
			return "/order/AddressAppliedSwitch";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderAddressAppliedSwitchResponse);
		}

		public  OrderAddressAppliedSwitchParam BuildParam()
		{
			return Param ?? (Param = new OrderAddressAppliedSwitchParam());
		}

	}
}
