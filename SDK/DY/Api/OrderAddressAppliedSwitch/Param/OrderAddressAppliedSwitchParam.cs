using System.Runtime.Serialization;

namespace Dop.Api.OrderAddressAppliedSwitch.Param
{
	public class OrderAddressAppliedSwitchParam
	{
		[DataMemberAttribute(Name = "is_allowed")]
		public long? IsAllowed { get; set; }

		[DataMemberAttribute(Name = "review_type")]
		public long? ReviewType { get; set; }

	}
}
