using System;
using Dop.Core;
using Dop.Api.WarehouseInfo.Param;

namespace Dop.Api.WarehouseInfo
{
	public class WarehouseInfoRequest : DoudianOpApiRequest<WarehouseInfoParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/info";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseInfoResponse);
		}

		public  WarehouseInfoParam BuildParam()
		{
			return Param ?? (Param = new WarehouseInfoParam());
		}

	}
}
