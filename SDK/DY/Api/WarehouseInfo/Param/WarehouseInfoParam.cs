using System.Runtime.Serialization;

namespace Dop.Api.WarehouseInfo.Param
{
	public class WarehouseInfoParam
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

	}
}
