using System.Runtime.Serialization;

namespace Dop.Api.WarehouseInfo.Data
{
	public class WarehouseInfoData
	{
		[DataMemberAttribute(Name = "data")]
		public Data Data { get; set; }

	}
}
