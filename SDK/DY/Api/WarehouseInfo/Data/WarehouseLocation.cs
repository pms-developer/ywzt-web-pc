using System.Runtime.Serialization;

namespace Dop.Api.WarehouseInfo.Data
{
	public class WarehouseLocation
	{
		[DataMemberAttribute(Name = "address_id1")]
		public long? AddressId1 { get; set; }

		[DataMemberAttribute(Name = "address_id2")]
		public long? AddressId2 { get; set; }

		[DataMemberAttribute(Name = "address_id3")]
		public long? AddressId3 { get; set; }

		[DataMemberAttribute(Name = "address_id4")]
		public long? AddressId4 { get; set; }

		[DataMemberAttribute(Name = "address_name1")]
		public string AddressName1 { get; set; }

		[DataMemberAttribute(Name = "address_name2")]
		public string AddressName2 { get; set; }

		[DataMemberAttribute(Name = "address_name3")]
		public string AddressName3 { get; set; }

		[DataMemberAttribute(Name = "address_name4")]
		public string AddressName4 { get; set; }

	}
}
