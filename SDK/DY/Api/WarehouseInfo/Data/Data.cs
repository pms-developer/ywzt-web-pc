using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.WarehouseInfo.Data
{
	public class Data
	{
		[DataMemberAttribute(Name = "warehouse_id")]
		public long? WarehouseId { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "intro")]
		public string Intro { get; set; }

		[DataMemberAttribute(Name = "addr")]
		public List<AddrItem> Addr { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public long? UpdateTime { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "shop_id")]
		public long? ShopId { get; set; }

		[DataMemberAttribute(Name = "warehouse_location")]
		public WarehouseLocation WarehouseLocation { get; set; }

		[DataMemberAttribute(Name = "address_detail")]
		public string AddressDetail { get; set; }

		[DataMemberAttribute(Name = "out_fence_ids")]
		public List<string> OutFenceIds { get; set; }

	}
}
