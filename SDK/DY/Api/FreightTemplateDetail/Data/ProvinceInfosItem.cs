using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.FreightTemplateDetail.Data
{
	public class ProvinceInfosItem
	{
		[DataMemberAttribute(Name = "id")]
		public string Id { get; set; }

		[DataMemberAttribute(Name = "children")]
		public List<ChildrenItem_5> Children { get; set; }

	}
}
