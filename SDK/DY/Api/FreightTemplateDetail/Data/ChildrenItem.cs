using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateDetail.Data
{
	public class ChildrenItem
	{
		[DataMemberAttribute(Name = "id")]
		public string Id { get; set; }

	}
}
