using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.FreightTemplateDetail.Data
{
	public class ChildrenItem_6
	{
		[DataMemberAttribute(Name = "id")]
		public string Id { get; set; }

		[DataMemberAttribute(Name = "children")]
		public List<ChildrenItem> Children { get; set; }

	}
}
