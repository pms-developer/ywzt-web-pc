using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateDetail.Data
{
	public class FreightTemplateDetailData
	{
		[DataMemberAttribute(Name = "data")]
		public Data Data { get; set; }

	}
}
