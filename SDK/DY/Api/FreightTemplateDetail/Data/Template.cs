using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateDetail.Data
{
	public class Template
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "product_province")]
		public string ProductProvince { get; set; }

		[DataMemberAttribute(Name = "product_city")]
		public string ProductCity { get; set; }

		[DataMemberAttribute(Name = "calculate_type")]
		public long? CalculateType { get; set; }

		[DataMemberAttribute(Name = "transfer_type")]
		public long? TransferType { get; set; }

		[DataMemberAttribute(Name = "rule_type")]
		public long? RuleType { get; set; }

		[DataMemberAttribute(Name = "fixed_amount")]
		public long? FixedAmount { get; set; }

	}
}
