using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateDetail.Param
{
	public class FreightTemplateDetailParam
	{
		[DataMemberAttribute(Name = "freight_id")]
		public long? FreightId { get; set; }

	}
}
