using System;
using Dop.Core;
using Dop.Api.FreightTemplateDetail.Param;

namespace Dop.Api.FreightTemplateDetail
{
	public class FreightTemplateDetailRequest : DoudianOpApiRequest<FreightTemplateDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/freightTemplate/detail";
		}

		public override Type GetResponseType()
		{
			return typeof(FreightTemplateDetailResponse);
		}

		public  FreightTemplateDetailParam BuildParam()
		{
			return Param ?? (Param = new FreightTemplateDetailParam());
		}

	}
}
