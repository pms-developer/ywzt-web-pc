using System;
using Dop.Core;
using Dop.Api.SkuList.Param;

namespace Dop.Api.SkuList
{
	public class SkuListRequest : DoudianOpApiRequest<SkuListParam>
	{
		public override string GetUrlPath()
		{
			return "/sku/list";
		}

		public override Type GetResponseType()
		{
			return typeof(SkuListResponse);
		}

		public  SkuListParam BuildParam()
		{
			return Param ?? (Param = new SkuListParam());
		}

	}
}
