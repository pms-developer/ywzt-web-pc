using System.Runtime.Serialization;

namespace Dop.Api.SkuList.Data
{
	public class CustomsReportInfo
	{
		[DataMemberAttribute(Name = "hs_code")]
		public string HsCode { get; set; }

		[DataMemberAttribute(Name = "first_measure_qty")]
		public double? FirstMeasureQty { get; set; }

		[DataMemberAttribute(Name = "second_measure_qty")]
		public double? SecondMeasureQty { get; set; }

		[DataMemberAttribute(Name = "first_measure_unit")]
		public string FirstMeasureUnit { get; set; }

		[DataMemberAttribute(Name = "second_measure_unit")]
		public string SecondMeasureUnit { get; set; }

		[DataMemberAttribute(Name = "unit")]
		public string Unit { get; set; }

		[DataMemberAttribute(Name = "report_name")]
		public string ReportName { get; set; }

		[DataMemberAttribute(Name = "report_brand_name")]
		public string ReportBrandName { get; set; }

		[DataMemberAttribute(Name = "usage")]
		public string Usage { get; set; }

		[DataMemberAttribute(Name = "g_model")]
		public string GModel { get; set; }

		[DataMemberAttribute(Name = "bar_code")]
		public string BarCode { get; set; }

	}
}
