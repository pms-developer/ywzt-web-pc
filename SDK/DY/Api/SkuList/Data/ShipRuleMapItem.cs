using System.Runtime.Serialization;

namespace Dop.Api.SkuList.Data
{
	public class ShipRuleMapItem
	{
		[DataMemberAttribute(Name = "presell_type")]
		public long? PresellType { get; set; }

		[DataMemberAttribute(Name = "delay_day")]
		public long? DelayDay { get; set; }

		[DataMemberAttribute(Name = "presell_end_time")]
		public long? PresellEndTime { get; set; }

	}
}
