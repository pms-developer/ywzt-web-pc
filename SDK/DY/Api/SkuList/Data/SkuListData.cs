using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SkuList.Data
{
	public class SkuListData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
