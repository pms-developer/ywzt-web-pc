using System.Runtime.Serialization;

namespace Dop.Api.SkuList.Param
{
	public class SkuListParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public long? OutProductId { get; set; }

	}
}
