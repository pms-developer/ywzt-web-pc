using System;
using Dop.Core;
using Dop.Api.WarehouseCreateBatch.Param;

namespace Dop.Api.WarehouseCreateBatch
{
	public class WarehouseCreateBatchRequest : DoudianOpApiRequest<WarehouseCreateBatchParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/createBatch";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseCreateBatchResponse);
		}

		public  WarehouseCreateBatchParam BuildParam()
		{
			return Param ?? (Param = new WarehouseCreateBatchParam());
		}

	}
}
