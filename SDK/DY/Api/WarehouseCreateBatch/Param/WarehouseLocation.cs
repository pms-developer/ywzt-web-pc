using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreateBatch.Param
{
	public class WarehouseLocation
	{
		[DataMemberAttribute(Name = "address_id1")]
		public long? AddressId1 { get; set; }

		[DataMemberAttribute(Name = "address_id2")]
		public long? AddressId2 { get; set; }

		[DataMemberAttribute(Name = "address_id3")]
		public long? AddressId3 { get; set; }

		[DataMemberAttribute(Name = "address_id4")]
		public long? AddressId4 { get; set; }

	}
}
