using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreateBatch.Param
{
	public class WarehouseCreateBatchParam
	{
		[DataMemberAttribute(Name = "out_warehouse_list")]
		public List<OutWarehouseListItem> OutWarehouseList { get; set; }

	}
}
