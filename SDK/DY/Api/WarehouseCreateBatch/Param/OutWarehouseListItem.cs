using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreateBatch.Param
{
	public class OutWarehouseListItem
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "intro")]
		public string Intro { get; set; }

		[DataMemberAttribute(Name = "address_detail")]
		public string AddressDetail { get; set; }

		[DataMemberAttribute(Name = "warehouse_location")]
		public WarehouseLocation WarehouseLocation { get; set; }

	}
}
