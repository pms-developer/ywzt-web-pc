using Dop.Core;
using Dop.Api.WarehouseCreateBatch.Data;

namespace Dop.Api.WarehouseCreateBatch
{
	public class WarehouseCreateBatchResponse : DoudianOpApiResponse<WarehouseCreateBatchData>
	{
	}
}
