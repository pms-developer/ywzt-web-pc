using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreateBatch.Data
{
	public class WarehouseCreateBatchData
	{
		[DataMemberAttribute(Name = "data")]
		public Dictionary<string,bool> Data { get; set; }

	}
}
