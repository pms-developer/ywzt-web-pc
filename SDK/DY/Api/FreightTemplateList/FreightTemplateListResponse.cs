using Dop.Core;
using Dop.Api.FreightTemplateList.Data;

namespace Dop.Api.FreightTemplateList
{
	public class FreightTemplateListResponse : DoudianOpApiResponse<FreightTemplateListData>
	{
	}
}
