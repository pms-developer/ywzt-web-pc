using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateList.Data
{
	public class ListItem
	{
		[DataMemberAttribute(Name = "template")]
		public Template Template { get; set; }

	}
}
