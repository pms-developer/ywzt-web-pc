using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateList.Data
{
	public class FreightTemplateListData
	{
		[DataMemberAttribute(Name = "List")]
		public List<ListItem> List { get; set; }

		[DataMemberAttribute(Name = "Count")]
		public long? Count { get; set; }

	}
}
