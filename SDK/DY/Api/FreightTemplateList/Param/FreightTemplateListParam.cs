using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateList.Param
{
	public class FreightTemplateListParam
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "page")]
		public string Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public string Size { get; set; }

	}
}
