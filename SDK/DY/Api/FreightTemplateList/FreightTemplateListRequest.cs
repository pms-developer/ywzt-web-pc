using System;
using Dop.Core;
using Dop.Api.FreightTemplateList.Param;

namespace Dop.Api.FreightTemplateList
{
	public class FreightTemplateListRequest : DoudianOpApiRequest<FreightTemplateListParam>
	{
		public override string GetUrlPath()
		{
			return "/freightTemplate/list";
		}

		public override Type GetResponseType()
		{
			return typeof(FreightTemplateListResponse);
		}

		public  FreightTemplateListParam BuildParam()
		{
			return Param ?? (Param = new FreightTemplateListParam());
		}

	}
}
