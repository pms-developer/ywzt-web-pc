using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuGetSpu.Data
{
	public class CspusItem
	{
		[DataMemberAttribute(Name = "cspu_id")]
		public long? CspuId { get; set; }

		[DataMemberAttribute(Name = "spu_id")]
		public long? SpuId { get; set; }

		[DataMemberAttribute(Name = "sale_properties")]
		public List<SalePropertiesItem> SaleProperties { get; set; }

	}
}
