using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuGetSpu.Data
{
	public class PropertyInfosItem
	{
		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "property_values")]
		public List<PropertyValuesItem> PropertyValues { get; set; }

		[DataMemberAttribute(Name = "property_name")]
		public string PropertyName { get; set; }

		[DataMemberAttribute(Name = "property_type")]
		public long? PropertyType { get; set; }

	}
}
