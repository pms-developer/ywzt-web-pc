using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpu.Data
{
	public class PropertyValuesItem
	{
		[DataMemberAttribute(Name = "value_id")]
		public long? ValueId { get; set; }

		[DataMemberAttribute(Name = "value_name")]
		public string ValueName { get; set; }

	}
}
