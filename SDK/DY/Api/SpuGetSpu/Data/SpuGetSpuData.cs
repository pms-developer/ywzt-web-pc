using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuGetSpu.Data
{
	public class SpuGetSpuData
	{
		[DataMemberAttribute(Name = "spu_id")]
		public long? SpuId { get; set; }

		[DataMemberAttribute(Name = "spu_name")]
		public string SpuName { get; set; }

		[DataMemberAttribute(Name = "category_leaf_id")]
		public long? CategoryLeafId { get; set; }

		[DataMemberAttribute(Name = "property_infos")]
		public List<PropertyInfosItem> PropertyInfos { get; set; }

		[DataMemberAttribute(Name = "cspus")]
		public List<CspusItem> Cspus { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

	}
}
