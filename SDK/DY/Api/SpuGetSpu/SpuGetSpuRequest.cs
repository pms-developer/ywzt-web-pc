using System;
using Dop.Core;
using Dop.Api.SpuGetSpu.Param;

namespace Dop.Api.SpuGetSpu
{
	public class SpuGetSpuRequest : DoudianOpApiRequest<SpuGetSpuParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/getSpu";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuGetSpuResponse);
		}

		public  SpuGetSpuParam BuildParam()
		{
			return Param ?? (Param = new SpuGetSpuParam());
		}

	}
}
