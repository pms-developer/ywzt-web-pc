using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuGetSpu.Param
{
	public class SpuGetSpuParam
	{
		[DataMemberAttribute(Name = "category_leaf_id")]
		public long? CategoryLeafId { get; set; }

		[DataMemberAttribute(Name = "key_properties")]
		public List<KeyPropertiesItem> KeyProperties { get; set; }

		[DataMemberAttribute(Name = "spu_id")]
		public long? SpuId { get; set; }

	}
}
