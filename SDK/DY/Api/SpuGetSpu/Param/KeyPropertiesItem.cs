using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuGetSpu.Param
{
	public class KeyPropertiesItem
	{
		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "property_values")]
		public List<PropertyValuesItem> PropertyValues { get; set; }

	}
}
