using System;
using Dop.Core;
using Dop.Api.OrderLogisticsAddSinglePack.Param;

namespace Dop.Api.OrderLogisticsAddSinglePack
{
	public class OrderLogisticsAddSinglePackRequest : DoudianOpApiRequest<OrderLogisticsAddSinglePackParam>
	{
		public override string GetUrlPath()
		{
			return "/order/logisticsAddSinglePack";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderLogisticsAddSinglePackResponse);
		}

		public  OrderLogisticsAddSinglePackParam BuildParam()
		{
			return Param ?? (Param = new OrderLogisticsAddSinglePackParam());
		}

	}
}
