using Dop.Core;
using Dop.Api.OrderLogisticsAddSinglePack.Data;

namespace Dop.Api.OrderLogisticsAddSinglePack
{
	public class OrderLogisticsAddSinglePackResponse : DoudianOpApiResponse<OrderLogisticsAddSinglePackData>
	{
	}
}
