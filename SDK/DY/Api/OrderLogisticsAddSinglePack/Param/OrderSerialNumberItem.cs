using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderLogisticsAddSinglePack.Param
{
	public class OrderSerialNumberItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "serial_number_list")]
		public List<string> SerialNumberList { get; set; }

	}
}
