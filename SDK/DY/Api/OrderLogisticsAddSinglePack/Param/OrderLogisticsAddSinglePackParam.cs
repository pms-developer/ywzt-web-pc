using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsAddSinglePack.Param
{
	public class OrderLogisticsAddSinglePackParam
	{
		[DataMemberAttribute(Name = "order_id_list")]
		public List<string> OrderIdList { get; set; }

		[DataMemberAttribute(Name = "shipped_order_info")]
		public List<ShippedOrderInfoItem> ShippedOrderInfo { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "company")]
		public string Company { get; set; }

		[DataMemberAttribute(Name = "request_id")]
		public string RequestId { get; set; }

		[DataMemberAttribute(Name = "is_reject_refund")]
		public bool? IsRejectRefund { get; set; }

		[DataMemberAttribute(Name = "logistics_id")]
		public string LogisticsId { get; set; }

		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "address_id")]
		public string AddressId { get; set; }

		[DataMemberAttribute(Name = "is_refund_reject")]
		public bool? IsRefundReject { get; set; }

		[DataMemberAttribute(Name = "order_serial_number")]
		public List<OrderSerialNumberItem> OrderSerialNumber { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

	}
}
