using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsAddSinglePack.Data
{
	public class OrderLogisticsAddSinglePackData
	{
		[DataMemberAttribute(Name = "pack_id")]
		public string PackId { get; set; }

	}
}
