using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateCreate.Param
{
	public class Template
	{
		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "product_province")]
		public long? ProductProvince { get; set; }

		[DataMemberAttribute(Name = "product_city")]
		public long? ProductCity { get; set; }

		[DataMemberAttribute(Name = "calculate_type")]
		public long? CalculateType { get; set; }

		[DataMemberAttribute(Name = "transfer_type")]
		public long? TransferType { get; set; }

		[DataMemberAttribute(Name = "rule_type")]
		public long? RuleType { get; set; }

		[DataMemberAttribute(Name = "fixed_amount")]
		public long? FixedAmount { get; set; }

	}
}
