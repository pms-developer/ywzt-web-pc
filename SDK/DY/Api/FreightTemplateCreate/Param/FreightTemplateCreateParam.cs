using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.FreightTemplateCreate.Param
{
	public class FreightTemplateCreateParam
	{
		[DataMemberAttribute(Name = "template")]
		public Template Template { get; set; }

		[DataMemberAttribute(Name = "columns")]
		public List<ColumnsItem> Columns { get; set; }

	}
}
