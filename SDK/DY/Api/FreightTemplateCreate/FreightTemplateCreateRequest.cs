using System;
using Dop.Core;
using Dop.Api.FreightTemplateCreate.Param;

namespace Dop.Api.FreightTemplateCreate
{
	public class FreightTemplateCreateRequest : DoudianOpApiRequest<FreightTemplateCreateParam>
	{
		public override string GetUrlPath()
		{
			return "/freightTemplate/create";
		}

		public override Type GetResponseType()
		{
			return typeof(FreightTemplateCreateResponse);
		}

		public  FreightTemplateCreateParam BuildParam()
		{
			return Param ?? (Param = new FreightTemplateCreateParam());
		}

	}
}
