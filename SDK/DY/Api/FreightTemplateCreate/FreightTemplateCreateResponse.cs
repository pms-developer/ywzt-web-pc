using Dop.Core;
using Dop.Api.FreightTemplateCreate.Data;

namespace Dop.Api.FreightTemplateCreate
{
	public class FreightTemplateCreateResponse : DoudianOpApiResponse<FreightTemplateCreateData>
	{
	}
}
