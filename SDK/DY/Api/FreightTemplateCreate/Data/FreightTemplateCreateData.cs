using System.Runtime.Serialization;

namespace Dop.Api.FreightTemplateCreate.Data
{
	public class FreightTemplateCreateData
	{
		[DataMemberAttribute(Name = "template_id")]
		public long? TemplateId { get; set; }

	}
}
