using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Param
{
	public class ProductDetailParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public string ProductId { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public string OutProductId { get; set; }

		[DataMemberAttribute(Name = "show_draft")]
		public string ShowDraft { get; set; }

	}
}
