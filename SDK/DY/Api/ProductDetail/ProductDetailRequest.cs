using System;
using Dop.Core;
using Dop.Api.ProductDetail.Param;

namespace Dop.Api.ProductDetail
{
	public class ProductDetailRequest : DoudianOpApiRequest<ProductDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/product/detail";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductDetailResponse);
		}

		public  ProductDetailParam BuildParam()
		{
			return Param ?? (Param = new ProductDetailParam());
		}

	}
}
