using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Data
{
	public class QualityAttachmentsItem
	{
		[DataMemberAttribute(Name = "media_type")]
		public long? MediaType { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
