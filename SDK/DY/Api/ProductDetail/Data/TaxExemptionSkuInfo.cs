using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Data
{
	public class TaxExemptionSkuInfo
	{
		[DataMemberAttribute(Name = "is_suit")]
		public int? IsSuit { get; set; }

		[DataMemberAttribute(Name = "suit_num")]
		public long? SuitNum { get; set; }

		[DataMemberAttribute(Name = "volume")]
		public long? Volume { get; set; }

	}
}
