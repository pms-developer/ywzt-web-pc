using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductDetail.Data
{
	public class QualityListItem
	{
		[DataMemberAttribute(Name = "quality_key")]
		public string QualityKey { get; set; }

		[DataMemberAttribute(Name = "quality_name")]
		public string QualityName { get; set; }

		[DataMemberAttribute(Name = "quality_attachments")]
		public List<QualityAttachmentsItem> QualityAttachments { get; set; }

	}
}
