using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductDetail.Data
{
	public class SpecsItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "spec_id")]
		public long? SpecId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "pid")]
		public long? Pid { get; set; }

		[DataMemberAttribute(Name = "is_leaf")]
		public int? IsLeaf { get; set; }

		[DataMemberAttribute(Name = "values")]
		public List<ValuesItem> Values { get; set; }

	}
}
