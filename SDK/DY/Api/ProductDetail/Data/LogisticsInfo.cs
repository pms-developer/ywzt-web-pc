using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Data
{
	public class LogisticsInfo
	{
		[DataMemberAttribute(Name = "customs_clear_type")]
		public long? CustomsClearType { get; set; }

		[DataMemberAttribute(Name = "origin_country_id")]
		public long? OriginCountryId { get; set; }

		[DataMemberAttribute(Name = "source_country_id")]
		public long? SourceCountryId { get; set; }

		[DataMemberAttribute(Name = "brand_country_id")]
		public long? BrandCountryId { get; set; }

		[DataMemberAttribute(Name = "tax_payer")]
		public long? TaxPayer { get; set; }

		[DataMemberAttribute(Name = "net_weight_qty")]
		public double? NetWeightQty { get; set; }

	}
}
