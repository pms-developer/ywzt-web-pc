using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Data
{
	public class PoiResource
	{
		[DataMemberAttribute(Name = "coupon_return_methods")]
		public List<long> CouponReturnMethods { get; set; }

	}
}
