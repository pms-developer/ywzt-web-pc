using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductDetail.Data
{
	public class ProductDetailData
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "product_id_str")]
		public string ProductIdStr { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public long? OutProductId { get; set; }

		[DataMemberAttribute(Name = "outer_product_id")]
		public string OuterProductId { get; set; }

		[DataMemberAttribute(Name = "open_user_id")]
		public long? OpenUserId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "description")]
		public string Description { get; set; }

		[DataMemberAttribute(Name = "delivery_method")]
		public int? DeliveryMethod { get; set; }

		[DataMemberAttribute(Name = "cdf_category")]
		public string CdfCategory { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "spec_id")]
		public long? SpecId { get; set; }

		[DataMemberAttribute(Name = "check_status")]
		public long? CheckStatus { get; set; }

		[DataMemberAttribute(Name = "mobile")]
		public string Mobile { get; set; }

		[DataMemberAttribute(Name = "brand_id")]
		public long? BrandId { get; set; }

		[DataMemberAttribute(Name = "is_sub_product")]
		public bool? IsSubProduct { get; set; }

		[DataMemberAttribute(Name = "draft_status")]
		public long? DraftStatus { get; set; }

		[DataMemberAttribute(Name = "category_detail")]
		public CategoryDetail CategoryDetail { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public long? PayType { get; set; }

		[DataMemberAttribute(Name = "recommend_remark")]
		public string RecommendRemark { get; set; }

		[DataMemberAttribute(Name = "extra")]
		public string Extra { get; set; }

		[DataMemberAttribute(Name = "is_create")]
		public long? IsCreate { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public string UpdateTime { get; set; }

		[DataMemberAttribute(Name = "pic")]
		public List<string> Pic { get; set; }

		[DataMemberAttribute(Name = "product_format")]
		public string ProductFormat { get; set; }

		[DataMemberAttribute(Name = "spec_pics")]
		public List<SpecPicsItem> SpecPics { get; set; }

		[DataMemberAttribute(Name = "spec_prices")]
		public List<SpecPricesItem> SpecPrices { get; set; }

		[DataMemberAttribute(Name = "specs")]
		public List<SpecsItem> Specs { get; set; }

		[DataMemberAttribute(Name = "img")]
		public string Img { get; set; }

		[DataMemberAttribute(Name = "presell_type")]
		public long? PresellType { get; set; }

		[DataMemberAttribute(Name = "maximum_per_order")]
		public long? MaximumPerOrder { get; set; }

		[DataMemberAttribute(Name = "limit_per_buyer")]
		public long? LimitPerBuyer { get; set; }

		[DataMemberAttribute(Name = "minimum_per_order")]
		public long? MinimumPerOrder { get; set; }

		[DataMemberAttribute(Name = "quality_list")]
		public List<QualityListItem> QualityList { get; set; }

		[DataMemberAttribute(Name = "logistics_info")]
		public LogisticsInfo LogisticsInfo { get; set; }

		[DataMemberAttribute(Name = "after_sale_service")]
		public string AfterSaleService { get; set; }

		[DataMemberAttribute(Name = "price_has_tax")]
		public long? PriceHasTax { get; set; }

		[DataMemberAttribute(Name = "appoint_delivery_day")]
		public long? AppointDeliveryDay { get; set; }

		[DataMemberAttribute(Name = "product_format_new")]
		public string ProductFormatNew { get; set; }

		[DataMemberAttribute(Name = "standard_brand_id")]
		public long? StandardBrandId { get; set; }

		[DataMemberAttribute(Name = "market_price")]
		public long? MarketPrice { get; set; }

		[DataMemberAttribute(Name = "discount_price")]
		public long? DiscountPrice { get; set; }

		[DataMemberAttribute(Name = "car_vin_code")]
		public string CarVinCode { get; set; }

		[DataMemberAttribute(Name = "need_recharge_mode")]
		public bool? NeedRechargeMode { get; set; }

		[DataMemberAttribute(Name = "account_template_id")]
		public string AccountTemplateId { get; set; }

		[DataMemberAttribute(Name = "presell_config_level")]
		public long? PresellConfigLevel { get; set; }

		[DataMemberAttribute(Name = "delivery_delay_day")]
		public long? DeliveryDelayDay { get; set; }

		[DataMemberAttribute(Name = "presell_delay")]
		public long? PresellDelay { get; set; }

		[DataMemberAttribute(Name = "poi_resource")]
		public PoiResource PoiResource { get; set; }

		[DataMemberAttribute(Name = "delay_rule")]
		public DelayRule DelayRule { get; set; }

		[DataMemberAttribute(Name = "long_pic_url")]
		public string LongPicUrl { get; set; }

		[DataMemberAttribute(Name = "sell_channel")]
		public List<long> SellChannel { get; set; }

		[DataMemberAttribute(Name = "freight_id")]
		public long? FreightId { get; set; }

		[DataMemberAttribute(Name = "material_video_id")]
		public string MaterialVideoId { get; set; }

		[DataMemberAttribute(Name = "pickup_method")]
		public string PickupMethod { get; set; }

		[DataMemberAttribute(Name = "size_info_template_id")]
		public long? SizeInfoTemplateId { get; set; }

		[DataMemberAttribute(Name = "white_back_ground_pic_url")]
		public string WhiteBackGroundPicUrl { get; set; }

		[DataMemberAttribute(Name = "sale_channel_type")]
		public string SaleChannelType { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

		[DataMemberAttribute(Name = "main_product_id")]
		public long? MainProductId { get; set; }

		[DataMemberAttribute(Name = "sale_limit_id")]
		public long? SaleLimitId { get; set; }

	}
}
