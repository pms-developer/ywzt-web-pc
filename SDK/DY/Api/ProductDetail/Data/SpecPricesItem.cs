using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductDetail.Data
{
	public class SpecPricesItem
	{
		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "out_sku_id")]
		public long? OutSkuId { get; set; }

		[DataMemberAttribute(Name = "outer_sku_id")]
		public string OuterSkuId { get; set; }

		[DataMemberAttribute(Name = "spec_detail_ids")]
		public List<long> SpecDetailIds { get; set; }

		[DataMemberAttribute(Name = "stock_num")]
		public long? StockNum { get; set; }

		[DataMemberAttribute(Name = "price")]
		public long? Price { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "step_stock_num")]
		public long? StepStockNum { get; set; }

		[DataMemberAttribute(Name = "prom_stock_num")]
		public long? PromStockNum { get; set; }

		[DataMemberAttribute(Name = "prom_step_stock_num")]
		public long? PromStepStockNum { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id1")]
		public long? SpecDetailId1 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id2")]
		public long? SpecDetailId2 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id3")]
		public long? SpecDetailId3 { get; set; }

		[DataMemberAttribute(Name = "sku_type")]
		public long? SkuType { get; set; }

		[DataMemberAttribute(Name = "supplier_id")]
		public string SupplierId { get; set; }

		[DataMemberAttribute(Name = "promotion_stock_num")]
		public long? PromotionStockNum { get; set; }

		[DataMemberAttribute(Name = "promotion_step_stock_num")]
		public long? PromotionStepStockNum { get; set; }

		[DataMemberAttribute(Name = "customs_report_info")]
		public CustomsReportInfo CustomsReportInfo { get; set; }

		[DataMemberAttribute(Name = "lock_stock_num")]
		public long? LockStockNum { get; set; }

		[DataMemberAttribute(Name = "lock_step_stock_num")]
		public long? LockStepStockNum { get; set; }

		[DataMemberAttribute(Name = "stock_num_map")]
		public Dictionary<string,long> StockNumMap { get; set; }

		[DataMemberAttribute(Name = "tax_exemption_sku_info")]
		public TaxExemptionSkuInfo TaxExemptionSkuInfo { get; set; }

		[DataMemberAttribute(Name = "presell_delay")]
		public long? PresellDelay { get; set; }

		[DataMemberAttribute(Name = "delivery_infos")]
		public List<DeliveryInfosItem> DeliveryInfos { get; set; }

	}
}
