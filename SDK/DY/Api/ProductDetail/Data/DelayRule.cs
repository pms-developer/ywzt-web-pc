using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Data
{
	public class DelayRule
	{
		[DataMemberAttribute(Name = "enable")]
		public bool? Enable { get; set; }

		[DataMemberAttribute(Name = "config_type")]
		public int? ConfigType { get; set; }

		[DataMemberAttribute(Name = "config_value")]
		public long? ConfigValue { get; set; }

		[DataMemberAttribute(Name = "start_time")]
		public long? StartTime { get; set; }

		[DataMemberAttribute(Name = "end_time")]
		public long? EndTime { get; set; }

	}
}
