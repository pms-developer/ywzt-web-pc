using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Data
{
	public class ValuesItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "spec_id")]
		public long? SpecId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "pid")]
		public long? Pid { get; set; }

		[DataMemberAttribute(Name = "is_leaf")]
		public int? IsLeaf { get; set; }

		[DataMemberAttribute(Name = "status")]
		public int? Status { get; set; }

	}
}
