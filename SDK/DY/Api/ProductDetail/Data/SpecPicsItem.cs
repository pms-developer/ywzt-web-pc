using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Data
{
	public class SpecPicsItem
	{
		[DataMemberAttribute(Name = "spec_detail_id")]
		public long? SpecDetailId { get; set; }

		[DataMemberAttribute(Name = "pic")]
		public string Pic { get; set; }

	}
}
