using System.Runtime.Serialization;

namespace Dop.Api.ProductDetail.Data
{
	public class DeliveryInfosItem
	{
		[DataMemberAttribute(Name = "info_type")]
		public string InfoType { get; set; }

		[DataMemberAttribute(Name = "info_value")]
		public string InfoValue { get; set; }

		[DataMemberAttribute(Name = "info_unit")]
		public string InfoUnit { get; set; }

	}
}
