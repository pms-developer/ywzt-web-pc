using Dop.Core;
using Dop.Api.AfterSaleSubmitEvidence.Data;

namespace Dop.Api.AfterSaleSubmitEvidence
{
	public class AfterSaleSubmitEvidenceResponse : DoudianOpApiResponse<AfterSaleSubmitEvidenceData>
	{
	}
}
