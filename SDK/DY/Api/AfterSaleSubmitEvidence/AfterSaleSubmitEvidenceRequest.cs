using System;
using Dop.Core;
using Dop.Api.AfterSaleSubmitEvidence.Param;

namespace Dop.Api.AfterSaleSubmitEvidence
{
	public class AfterSaleSubmitEvidenceRequest : DoudianOpApiRequest<AfterSaleSubmitEvidenceParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/submitEvidence";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleSubmitEvidenceResponse);
		}

		public  AfterSaleSubmitEvidenceParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleSubmitEvidenceParam());
		}

	}
}
