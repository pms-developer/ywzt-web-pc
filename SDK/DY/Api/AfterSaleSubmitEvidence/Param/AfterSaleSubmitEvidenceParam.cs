using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleSubmitEvidence.Param
{
	public class AfterSaleSubmitEvidenceParam
	{
		[DataMemberAttribute(Name = "aftersale_id")]
		public long? AftersaleId { get; set; }

		[DataMemberAttribute(Name = "comment")]
		public string Comment { get; set; }

		[DataMemberAttribute(Name = "evidence")]
		public List<string> Evidence { get; set; }

	}
}
