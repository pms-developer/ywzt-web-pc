using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchSearchIndex.Param
{
	public class OrderBatchSearchIndexParam
	{
		[DataMemberAttribute(Name = "plain_text_list")]
		public List<PlainTextListItem> PlainTextList { get; set; }

	}
}
