using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchSearchIndex.Param
{
	public class PlainTextListItem
	{
		[DataMemberAttribute(Name = "plain_text")]
		public string PlainText { get; set; }

		[DataMemberAttribute(Name = "encrypt_type")]
		public int? EncryptType { get; set; }

	}
}
