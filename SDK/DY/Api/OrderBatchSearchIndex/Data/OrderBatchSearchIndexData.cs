using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchSearchIndex.Data
{
	public class OrderBatchSearchIndexData
	{
		[DataMemberAttribute(Name = "plain_to_encrypt_index_list")]
		public List<PlainToEncryptIndexListItem> PlainToEncryptIndexList { get; set; }

		[DataMemberAttribute(Name = "custom_err")]
		public CustomErr CustomErr { get; set; }

	}
}
