using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchSearchIndex.Data
{
	public class PlainToEncryptIndexListItem
	{
		[DataMemberAttribute(Name = "plain")]
		public string Plain { get; set; }

		[DataMemberAttribute(Name = "search_index")]
		public string SearchIndex { get; set; }

	}
}
