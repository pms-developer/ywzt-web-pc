using System;
using Dop.Core;
using Dop.Api.OrderBatchSearchIndex.Param;

namespace Dop.Api.OrderBatchSearchIndex
{
	public class OrderBatchSearchIndexRequest : DoudianOpApiRequest<OrderBatchSearchIndexParam>
	{
		public override string GetUrlPath()
		{
			return "/order/BatchSearchIndex";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderBatchSearchIndexResponse);
		}

		public  OrderBatchSearchIndexParam BuildParam()
		{
			return Param ?? (Param = new OrderBatchSearchIndexParam());
		}

	}
}
