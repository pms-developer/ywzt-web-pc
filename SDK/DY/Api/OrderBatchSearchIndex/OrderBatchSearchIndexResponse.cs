using Dop.Core;
using Dop.Api.OrderBatchSearchIndex.Data;

namespace Dop.Api.OrderBatchSearchIndex
{
	public class OrderBatchSearchIndexResponse : DoudianOpApiResponse<OrderBatchSearchIndexData>
	{
	}
}
