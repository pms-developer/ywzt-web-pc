using System.Runtime.Serialization;

namespace Dop.Api.ProductQualityTask.Param
{
	public class ProductQualityTaskParam
	{
		[DataMemberAttribute(Name = "brief_only")]
		public bool? BriefOnly { get; set; }

	}
}
