using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductQualityTask.Data
{
	public class ProductQualityTaskData
	{
		[DataMemberAttribute(Name = "product_num_total")]
		public long? ProductNumTotal { get; set; }

		[DataMemberAttribute(Name = "product_num_finished")]
		public long? ProductNumFinished { get; set; }

		[DataMemberAttribute(Name = "task_status")]
		public long? TaskStatus { get; set; }

		[DataMemberAttribute(Name = "product_num_to_improve_total")]
		public long? ProductNumToImproveTotal { get; set; }

		[DataMemberAttribute(Name = "problem_num_total")]
		public long? ProblemNumTotal { get; set; }

		[DataMemberAttribute(Name = "problem_num_to_improve")]
		public long? ProblemNumToImprove { get; set; }

		[DataMemberAttribute(Name = "task_finish_time")]
		public string TaskFinishTime { get; set; }

		[DataMemberAttribute(Name = "problem_type_distribution")]
		public List<ProblemTypeDistributionItem> ProblemTypeDistribution { get; set; }

		[DataMemberAttribute(Name = "task_id")]
		public long? TaskId { get; set; }

		[DataMemberAttribute(Name = "standard_rate")]
		public double? StandardRate { get; set; }

		[DataMemberAttribute(Name = "is_standard")]
		public bool? IsStandard { get; set; }

		[DataMemberAttribute(Name = "meet_standard_num")]
		public long? MeetStandardNum { get; set; }

	}
}
