using System.Runtime.Serialization;

namespace Dop.Api.ProductQualityTask.Data
{
	public class ProblemTypeDistributionItem
	{
		[DataMemberAttribute(Name = "type_key")]
		public long? TypeKey { get; set; }

		[DataMemberAttribute(Name = "type_name")]
		public string TypeName { get; set; }

		[DataMemberAttribute(Name = "num")]
		public long? Num { get; set; }

	}
}
