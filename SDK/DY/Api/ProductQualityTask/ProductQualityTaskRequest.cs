using System;
using Dop.Core;
using Dop.Api.ProductQualityTask.Param;

namespace Dop.Api.ProductQualityTask
{
	public class ProductQualityTaskRequest : DoudianOpApiRequest<ProductQualityTaskParam>
	{
		public override string GetUrlPath()
		{
			return "/product/qualityTask";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductQualityTaskResponse);
		}

		public  ProductQualityTaskParam BuildParam()
		{
			return Param ?? (Param = new ProductQualityTaskParam());
		}

	}
}
