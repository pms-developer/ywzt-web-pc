using System.Runtime.Serialization;

namespace Dop.Api.MaterialEditMaterial.Param
{
	public class MaterialEditMaterialParam
	{
		[DataMemberAttribute(Name = "material_id")]
		public string MaterialId { get; set; }

		[DataMemberAttribute(Name = "material_name")]
		public string MaterialName { get; set; }

		[DataMemberAttribute(Name = "to_folder_id")]
		public string ToFolderId { get; set; }

	}
}
