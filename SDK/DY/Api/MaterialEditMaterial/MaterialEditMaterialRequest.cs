using System;
using Dop.Core;
using Dop.Api.MaterialEditMaterial.Param;

namespace Dop.Api.MaterialEditMaterial
{
	public class MaterialEditMaterialRequest : DoudianOpApiRequest<MaterialEditMaterialParam>
	{
		public override string GetUrlPath()
		{
			return "/material/editMaterial";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialEditMaterialResponse);
		}

		public  MaterialEditMaterialParam BuildParam()
		{
			return Param ?? (Param = new MaterialEditMaterialParam());
		}

	}
}
