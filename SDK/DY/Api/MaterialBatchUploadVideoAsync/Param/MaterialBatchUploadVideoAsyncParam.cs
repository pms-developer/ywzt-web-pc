using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialBatchUploadVideoAsync.Param
{
	public class MaterialBatchUploadVideoAsyncParam
	{
		[DataMemberAttribute(Name = "materials")]
		public List<MaterialsItem> Materials { get; set; }

	}
}
