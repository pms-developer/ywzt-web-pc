using System;
using Dop.Core;
using Dop.Api.MaterialBatchUploadVideoAsync.Param;

namespace Dop.Api.MaterialBatchUploadVideoAsync
{
	public class MaterialBatchUploadVideoAsyncRequest : DoudianOpApiRequest<MaterialBatchUploadVideoAsyncParam>
	{
		public override string GetUrlPath()
		{
			return "/material/batchUploadVideoAsync";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialBatchUploadVideoAsyncResponse);
		}

		public  MaterialBatchUploadVideoAsyncParam BuildParam()
		{
			return Param ?? (Param = new MaterialBatchUploadVideoAsyncParam());
		}

	}
}
