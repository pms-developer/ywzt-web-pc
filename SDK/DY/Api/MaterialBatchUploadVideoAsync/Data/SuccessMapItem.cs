using System.Runtime.Serialization;

namespace Dop.Api.MaterialBatchUploadVideoAsync.Data
{
	public class SuccessMapItem
	{
		[DataMemberAttribute(Name = "material_id")]
		public string MaterialId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "origin_url")]
		public string OriginUrl { get; set; }

		[DataMemberAttribute(Name = "audit_status")]
		public int? AuditStatus { get; set; }

		[DataMemberAttribute(Name = "is_new")]
		public bool? IsNew { get; set; }

	}
}
