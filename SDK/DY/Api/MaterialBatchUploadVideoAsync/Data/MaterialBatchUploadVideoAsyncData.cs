using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialBatchUploadVideoAsync.Data
{
	public class MaterialBatchUploadVideoAsyncData
	{
		[DataMemberAttribute(Name = "success_map")]
		public Dictionary<string,SuccessMapItem> SuccessMap { get; set; }

		[DataMemberAttribute(Name = "failed_map")]
		public Dictionary<string,FailedMapItem> FailedMap { get; set; }

	}
}
