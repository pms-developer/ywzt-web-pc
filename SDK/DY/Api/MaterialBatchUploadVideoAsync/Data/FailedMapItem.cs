using System.Runtime.Serialization;

namespace Dop.Api.MaterialBatchUploadVideoAsync.Data
{
	public class FailedMapItem
	{
		[DataMemberAttribute(Name = "err_code")]
		public int? ErrCode { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

	}
}
