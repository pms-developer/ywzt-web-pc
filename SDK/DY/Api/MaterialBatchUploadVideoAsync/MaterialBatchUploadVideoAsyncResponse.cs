using Dop.Core;
using Dop.Api.MaterialBatchUploadVideoAsync.Data;

namespace Dop.Api.MaterialBatchUploadVideoAsync
{
	public class MaterialBatchUploadVideoAsyncResponse : DoudianOpApiResponse<MaterialBatchUploadVideoAsyncData>
	{
	}
}
