using System;
using Dop.Core;
using Dop.Api.SmsTemplateApplyList.Param;

namespace Dop.Api.SmsTemplateApplyList
{
	public class SmsTemplateApplyListRequest : DoudianOpApiRequest<SmsTemplateApplyListParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/template/apply/list";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsTemplateApplyListResponse);
		}

		public  SmsTemplateApplyListParam BuildParam()
		{
			return Param ?? (Param = new SmsTemplateApplyListParam());
		}

	}
}
