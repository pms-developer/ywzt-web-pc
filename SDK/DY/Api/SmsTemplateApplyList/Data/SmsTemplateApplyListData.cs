using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateApplyList.Data
{
	public class SmsTemplateApplyListData
	{
		[DataMemberAttribute(Name = "template_apply_list")]
		public List<TemplateApplyListItem> TemplateApplyList { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
