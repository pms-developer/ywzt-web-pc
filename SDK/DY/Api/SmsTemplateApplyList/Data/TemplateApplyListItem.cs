using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateApplyList.Data
{
	public class TemplateApplyListItem
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "sms_template_apply_id")]
		public string SmsTemplateApplyId { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "template_content")]
		public string TemplateContent { get; set; }

		[DataMemberAttribute(Name = "channel_type")]
		public string ChannelType { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "status_remark")]
		public string StatusRemark { get; set; }

	}
}
