using System;
using Dop.Core;
using Dop.Api.OrderAddresSwitchConfig.Param;

namespace Dop.Api.OrderAddresSwitchConfig
{
	public class OrderAddresSwitchConfigRequest : DoudianOpApiRequest<OrderAddresSwitchConfigParam>
	{
		public override string GetUrlPath()
		{
			return "/order/addresSwitchConfig";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderAddresSwitchConfigResponse);
		}

		public  OrderAddresSwitchConfigParam BuildParam()
		{
			return Param ?? (Param = new OrderAddresSwitchConfigParam());
		}

	}
}
