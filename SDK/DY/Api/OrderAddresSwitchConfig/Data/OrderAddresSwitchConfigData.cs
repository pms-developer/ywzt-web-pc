using System.Runtime.Serialization;

namespace Dop.Api.OrderAddresSwitchConfig.Data
{
	public class OrderAddresSwitchConfigData
	{
		[DataMemberAttribute(Name = "authorized_status")]
		public long? AuthorizedStatus { get; set; }

	}
}
