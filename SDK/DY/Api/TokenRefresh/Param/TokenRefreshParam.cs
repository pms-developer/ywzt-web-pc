using System.Runtime.Serialization;

namespace Dop.Api.TokenRefresh.Param
{
	public class TokenRefreshParam
	{
		[DataMemberAttribute(Name = "refresh_token")]
		public string RefreshToken { get; set; }

		[DataMemberAttribute(Name = "grant_type")]
		public string GrantType { get; set; }

	}
}
