using System;
using Dop.Core;
using Dop.Api.TokenRefresh.Param;

namespace Dop.Api.TokenRefresh
{
	public class TokenRefreshRequest : DoudianOpApiRequest<TokenRefreshParam>
	{
		public override string GetUrlPath()
		{
			return "/token/refresh";
		}

		public override Type GetResponseType()
		{
			return typeof(TokenRefreshResponse);
		}

		public  TokenRefreshParam BuildParam()
		{
			return Param ?? (Param = new TokenRefreshParam());
		}

	}
}
