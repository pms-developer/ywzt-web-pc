using System.Runtime.Serialization;

namespace Dop.Api.TokenRefresh.Data
{
	public class TokenRefreshData
	{
		[DataMemberAttribute(Name = "access_token")]
		public string AccessToken { get; set; }

		[DataMemberAttribute(Name = "expires_in")]
		public long? ExpiresIn { get; set; }

		[DataMemberAttribute(Name = "refresh_token")]
		public string RefreshToken { get; set; }

		[DataMemberAttribute(Name = "scope")]
		public string Scope { get; set; }

		[DataMemberAttribute(Name = "shop_id")]
		public long? ShopId { get; set; }

		[DataMemberAttribute(Name = "shop_name")]
		public string ShopName { get; set; }

		[DataMemberAttribute(Name = "authority_id")]
		public string AuthorityId { get; set; }

	}
}
