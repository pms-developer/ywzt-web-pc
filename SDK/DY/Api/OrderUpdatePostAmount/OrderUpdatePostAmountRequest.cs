using System;
using Dop.Core;
using Dop.Api.OrderUpdatePostAmount.Param;

namespace Dop.Api.OrderUpdatePostAmount
{
	public class OrderUpdatePostAmountRequest : DoudianOpApiRequest<OrderUpdatePostAmountParam>
	{
		public override string GetUrlPath()
		{
			return "/order/updatePostAmount";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderUpdatePostAmountResponse);
		}

		public  OrderUpdatePostAmountParam BuildParam()
		{
			return Param ?? (Param = new OrderUpdatePostAmountParam());
		}

	}
}
