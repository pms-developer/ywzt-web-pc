using Dop.Core;
using Dop.Api.OrderUpdatePostAmount.Data;

namespace Dop.Api.OrderUpdatePostAmount
{
	public class OrderUpdatePostAmountResponse : DoudianOpApiResponse<OrderUpdatePostAmountData>
	{
	}
}
