using System.Runtime.Serialization;

namespace Dop.Api.OrderUpdatePostAmount.Param
{
	public class OrderUpdatePostAmountParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "post_amount")]
		public long? PostAmount { get; set; }

	}
}
