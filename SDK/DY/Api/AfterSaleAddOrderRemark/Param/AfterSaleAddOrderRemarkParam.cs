using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleAddOrderRemark.Param
{
	public class AfterSaleAddOrderRemarkParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "after_sale_id")]
		public string AfterSaleId { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

	}
}
