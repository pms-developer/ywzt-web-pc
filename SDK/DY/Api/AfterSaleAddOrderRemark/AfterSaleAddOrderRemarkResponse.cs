using Dop.Core;
using Dop.Api.AfterSaleAddOrderRemark.Data;

namespace Dop.Api.AfterSaleAddOrderRemark
{
	public class AfterSaleAddOrderRemarkResponse : DoudianOpApiResponse<AfterSaleAddOrderRemarkData>
	{
	}
}
