using System;
using Dop.Core;
using Dop.Api.AfterSaleAddOrderRemark.Param;

namespace Dop.Api.AfterSaleAddOrderRemark
{
	public class AfterSaleAddOrderRemarkRequest : DoudianOpApiRequest<AfterSaleAddOrderRemarkParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/addOrderRemark";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleAddOrderRemarkResponse);
		}

		public  AfterSaleAddOrderRemarkParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleAddOrderRemarkParam());
		}

	}
}
