using System;
using Dop.Core;
using Dop.Api.LogisticsQueryPackageRoute.Param;

namespace Dop.Api.LogisticsQueryPackageRoute
{
	public class LogisticsQueryPackageRouteRequest : DoudianOpApiRequest<LogisticsQueryPackageRouteParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/queryPackageRoute";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsQueryPackageRouteResponse);
		}

		public  LogisticsQueryPackageRouteParam BuildParam()
		{
			return Param ?? (Param = new LogisticsQueryPackageRouteParam());
		}

	}
}
