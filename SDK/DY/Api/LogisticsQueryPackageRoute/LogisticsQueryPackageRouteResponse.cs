using Dop.Core;
using Dop.Api.LogisticsQueryPackageRoute.Data;

namespace Dop.Api.LogisticsQueryPackageRoute
{
	public class LogisticsQueryPackageRouteResponse : DoudianOpApiResponse<LogisticsQueryPackageRouteData>
	{
	}
}
