using System.Runtime.Serialization;

namespace Dop.Api.LogisticsQueryPackageRoute.Param
{
	public class LogisticsQueryPackageRouteParam
	{
		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "express")]
		public string Express { get; set; }

		[DataMemberAttribute(Name = "receiver")]
		public Receiver Receiver { get; set; }

		[DataMemberAttribute(Name = "sender")]
		public Sender Sender { get; set; }

	}
}
