using System.Runtime.Serialization;

namespace Dop.Api.LogisticsQueryPackageRoute.Param
{
	public class Receiver
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "phone")]
		public string Phone { get; set; }

		[DataMemberAttribute(Name = "mobile")]
		public string Mobile { get; set; }

		[DataMemberAttribute(Name = "email")]
		public string Email { get; set; }

		[DataMemberAttribute(Name = "virtual_mobile")]
		public string VirtualMobile { get; set; }

	}
}
