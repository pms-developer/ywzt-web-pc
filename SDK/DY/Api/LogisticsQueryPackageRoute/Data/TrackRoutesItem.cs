using System.Runtime.Serialization;

namespace Dop.Api.LogisticsQueryPackageRoute.Data
{
	public class TrackRoutesItem
	{
		[DataMemberAttribute(Name = "content")]
		public string Content { get; set; }

		[DataMemberAttribute(Name = "state")]
		public string State { get; set; }

		[DataMemberAttribute(Name = "state_time")]
		public long? StateTime { get; set; }

		[DataMemberAttribute(Name = "opcode")]
		public string Opcode { get; set; }

	}
}
