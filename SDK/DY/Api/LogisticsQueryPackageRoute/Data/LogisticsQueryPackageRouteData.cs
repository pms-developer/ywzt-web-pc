using System.Runtime.Serialization;

namespace Dop.Api.LogisticsQueryPackageRoute.Data
{
	public class LogisticsQueryPackageRouteData
	{
		[DataMemberAttribute(Name = "data")]
		public Data Data { get; set; }

	}
}
