using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsQueryPackageRoute.Data
{
	public class Data
	{
		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "express")]
		public string Express { get; set; }

		[DataMemberAttribute(Name = "track_routes")]
		public List<TrackRoutesItem> TrackRoutes { get; set; }

	}
}
