using System;
using Dop.Core;
using Dop.Api.RightsInfo.Param;

namespace Dop.Api.RightsInfo
{
	public class RightsInfoRequest : DoudianOpApiRequest<RightsInfoParam>
	{
		public override string GetUrlPath()
		{
			return "/rights/info";
		}

		public override Type GetResponseType()
		{
			return typeof(RightsInfoResponse);
		}

		public  RightsInfoParam BuildParam()
		{
			return Param ?? (Param = new RightsInfoParam());
		}

	}
}
