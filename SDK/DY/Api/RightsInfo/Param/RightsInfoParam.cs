using System.Runtime.Serialization;

namespace Dop.Api.RightsInfo.Param
{
	public class RightsInfoParam
	{
		[DataMemberAttribute(Name = "biz_type")]
		public int? BizType { get; set; }

	}
}
