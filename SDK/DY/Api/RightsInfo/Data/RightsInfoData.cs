using System.Runtime.Serialization;

namespace Dop.Api.RightsInfo.Data
{
	public class RightsInfoData
	{
		[DataMemberAttribute(Name = "expire_time")]
		public string ExpireTime { get; set; }

		[DataMemberAttribute(Name = "rights_type")]
		public int? RightsType { get; set; }

		[DataMemberAttribute(Name = "spec_type")]
		public int? SpecType { get; set; }

		[DataMemberAttribute(Name = "spec_val")]
		public string SpecVal { get; set; }

	}
}
