using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.PromiseSetSkuShipTime.Param
{
	public class PromiseSetSkuShipTimeParam
	{
		[DataMemberAttribute(Name = "rules")]
		public List<RulesItem> Rules { get; set; }

	}
}
