using System.Runtime.Serialization;

namespace Dop.Api.PromiseSetSkuShipTime.Param
{
	public class RulesItem
	{
		[DataMemberAttribute(Name = "sku_id")]
		public string SkuId { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "pre_sell_type")]
		public int? PreSellType { get; set; }

		[DataMemberAttribute(Name = "delay_day")]
		public int? DelayDay { get; set; }

		[DataMemberAttribute(Name = "pre_sell_end_time")]
		public long? PreSellEndTime { get; set; }

	}
}
