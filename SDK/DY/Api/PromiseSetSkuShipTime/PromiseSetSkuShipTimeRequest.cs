using System;
using Dop.Core;
using Dop.Api.PromiseSetSkuShipTime.Param;

namespace Dop.Api.PromiseSetSkuShipTime
{
	public class PromiseSetSkuShipTimeRequest : DoudianOpApiRequest<PromiseSetSkuShipTimeParam>
	{
		public override string GetUrlPath()
		{
			return "/promise/setSkuShipTime";
		}

		public override Type GetResponseType()
		{
			return typeof(PromiseSetSkuShipTimeResponse);
		}

		public  PromiseSetSkuShipTimeParam BuildParam()
		{
			return Param ?? (Param = new PromiseSetSkuShipTimeParam());
		}

	}
}
