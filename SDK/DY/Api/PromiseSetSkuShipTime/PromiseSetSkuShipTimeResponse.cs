using Dop.Core;
using Dop.Api.PromiseSetSkuShipTime.Data;

namespace Dop.Api.PromiseSetSkuShipTime
{
	public class PromiseSetSkuShipTimeResponse : DoudianOpApiResponse<PromiseSetSkuShipTimeData>
	{
	}
}
