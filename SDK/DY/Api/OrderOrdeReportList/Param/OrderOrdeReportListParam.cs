using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderOrdeReportList.Param
{
	public class OrderOrdeReportListParam
	{
		[DataMemberAttribute(Name = "add_real_mobile_whites")]
		public List<AddRealMobileWhitesItem> AddRealMobileWhites { get; set; }

	}
}
