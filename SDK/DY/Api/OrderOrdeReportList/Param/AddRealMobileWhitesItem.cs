using System.Runtime.Serialization;

namespace Dop.Api.OrderOrdeReportList.Param
{
	public class AddRealMobileWhitesItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "after_sale_id")]
		public string AfterSaleId { get; set; }

		[DataMemberAttribute(Name = "reason_no")]
		public long? ReasonNo { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

	}
}
