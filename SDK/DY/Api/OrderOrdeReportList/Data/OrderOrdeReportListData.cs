using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderOrdeReportList.Data
{
	public class OrderOrdeReportListData
	{
		[DataMemberAttribute(Name = "add_real_mobile_whites")]
		public List<AddRealMobileWhitesItem> AddRealMobileWhites { get; set; }

	}
}
