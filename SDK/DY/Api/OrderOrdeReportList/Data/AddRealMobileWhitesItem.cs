using System.Runtime.Serialization;

namespace Dop.Api.OrderOrdeReportList.Data
{
	public class AddRealMobileWhitesItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "after_sale_id")]
		public string AfterSaleId { get; set; }

		[DataMemberAttribute(Name = "msg")]
		public string Msg { get; set; }

	}
}
