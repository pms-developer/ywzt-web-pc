using System;
using Dop.Core;
using Dop.Api.OrderOrdeReportList.Param;

namespace Dop.Api.OrderOrdeReportList
{
	public class OrderOrdeReportListRequest : DoudianOpApiRequest<OrderOrdeReportListParam>
	{
		public override string GetUrlPath()
		{
			return "/order/ordeReportList";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderOrdeReportListResponse);
		}

		public  OrderOrdeReportListParam BuildParam()
		{
			return Param ?? (Param = new OrderOrdeReportListParam());
		}

	}
}
