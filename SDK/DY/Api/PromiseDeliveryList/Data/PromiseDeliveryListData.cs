using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.PromiseDeliveryList.Data
{
	public class PromiseDeliveryListData
	{
		[DataMemberAttribute(Name = "products")]
		public List<ProductsItem> Products { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
