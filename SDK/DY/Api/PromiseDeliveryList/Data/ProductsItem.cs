using System.Runtime.Serialization;

namespace Dop.Api.PromiseDeliveryList.Data
{
	public class ProductsItem
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "ship_mode")]
		public int? ShipMode { get; set; }

	}
}
