using System;
using Dop.Core;
using Dop.Api.PromiseDeliveryList.Param;

namespace Dop.Api.PromiseDeliveryList
{
	public class PromiseDeliveryListRequest : DoudianOpApiRequest<PromiseDeliveryListParam>
	{
		public override string GetUrlPath()
		{
			return "/promise/deliveryList";
		}

		public override Type GetResponseType()
		{
			return typeof(PromiseDeliveryListResponse);
		}

		public  PromiseDeliveryListParam BuildParam()
		{
			return Param ?? (Param = new PromiseDeliveryListParam());
		}

	}
}
