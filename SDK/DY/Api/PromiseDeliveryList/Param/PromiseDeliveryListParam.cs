using System.Runtime.Serialization;

namespace Dop.Api.PromiseDeliveryList.Param
{
	public class PromiseDeliveryListParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

	}
}
