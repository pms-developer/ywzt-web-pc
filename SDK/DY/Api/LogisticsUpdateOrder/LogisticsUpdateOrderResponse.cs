using Dop.Core;
using Dop.Api.LogisticsUpdateOrder.Data;

namespace Dop.Api.LogisticsUpdateOrder
{
	public class LogisticsUpdateOrderResponse : DoudianOpApiResponse<LogisticsUpdateOrderData>
	{
	}
}
