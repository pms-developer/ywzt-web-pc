using System.Runtime.Serialization;

namespace Dop.Api.LogisticsUpdateOrder.Param
{
	public class Warehouse
	{
		[DataMemberAttribute(Name = "is_sum_up")]
		public bool? IsSumUp { get; set; }

		[DataMemberAttribute(Name = "wh_order_no")]
		public string WhOrderNo { get; set; }

	}
}
