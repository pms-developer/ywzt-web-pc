using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsUpdateOrder.Param
{
	public class LogisticsUpdateOrderParam
	{
		[DataMemberAttribute(Name = "sender_info")]
		public SenderInfo SenderInfo { get; set; }

		[DataMemberAttribute(Name = "receiver_info")]
		public ReceiverInfo ReceiverInfo { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "items")]
		public List<ItemsItem> Items { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "extra")]
		public string Extra { get; set; }

		[DataMemberAttribute(Name = "user_id")]
		public long? UserId { get; set; }

		[DataMemberAttribute(Name = "volume")]
		public string Volume { get; set; }

		[DataMemberAttribute(Name = "weight")]
		public long? Weight { get; set; }

		[DataMemberAttribute(Name = "warehouse")]
		public Warehouse Warehouse { get; set; }

	}
}
