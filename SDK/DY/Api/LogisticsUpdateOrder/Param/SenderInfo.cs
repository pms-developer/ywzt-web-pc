using System.Runtime.Serialization;

namespace Dop.Api.LogisticsUpdateOrder.Param
{
	public class SenderInfo
	{
		[DataMemberAttribute(Name = "contact")]
		public Contact Contact { get; set; }

	}
}
