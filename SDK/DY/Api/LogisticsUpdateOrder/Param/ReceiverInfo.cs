using System.Runtime.Serialization;

namespace Dop.Api.LogisticsUpdateOrder.Param
{
	public class ReceiverInfo
	{
		[DataMemberAttribute(Name = "address")]
		public Address Address { get; set; }

		[DataMemberAttribute(Name = "contact")]
		public Contact Contact { get; set; }

	}
}
