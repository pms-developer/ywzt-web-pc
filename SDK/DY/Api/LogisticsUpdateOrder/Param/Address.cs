using System.Runtime.Serialization;

namespace Dop.Api.LogisticsUpdateOrder.Param
{
	public class Address
	{
		[DataMemberAttribute(Name = "country_code")]
		public string CountryCode { get; set; }

		[DataMemberAttribute(Name = "province_name")]
		public string ProvinceName { get; set; }

		[DataMemberAttribute(Name = "city_name")]
		public string CityName { get; set; }

		[DataMemberAttribute(Name = "district_name")]
		public string DistrictName { get; set; }

		[DataMemberAttribute(Name = "street_name")]
		public string StreetName { get; set; }

		[DataMemberAttribute(Name = "detail_address")]
		public string DetailAddress { get; set; }

		[DataMemberAttribute(Name = "province_code")]
		public string ProvinceCode { get; set; }

		[DataMemberAttribute(Name = "city_code")]
		public string CityCode { get; set; }

		[DataMemberAttribute(Name = "district_code")]
		public string DistrictCode { get; set; }

		[DataMemberAttribute(Name = "street_code")]
		public string StreetCode { get; set; }

	}
}
