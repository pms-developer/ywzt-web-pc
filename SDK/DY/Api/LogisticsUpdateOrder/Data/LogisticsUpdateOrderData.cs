using System.Runtime.Serialization;

namespace Dop.Api.LogisticsUpdateOrder.Data
{
	public class LogisticsUpdateOrderData
	{
		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "sort_code")]
		public string SortCode { get; set; }

		[DataMemberAttribute(Name = "package_center_code")]
		public string PackageCenterCode { get; set; }

		[DataMemberAttribute(Name = "package_center_name")]
		public string PackageCenterName { get; set; }

		[DataMemberAttribute(Name = "short_address_code")]
		public string ShortAddressCode { get; set; }

		[DataMemberAttribute(Name = "short_address_name")]
		public string ShortAddressName { get; set; }

		[DataMemberAttribute(Name = "extra_resp")]
		public string ExtraResp { get; set; }

	}
}
