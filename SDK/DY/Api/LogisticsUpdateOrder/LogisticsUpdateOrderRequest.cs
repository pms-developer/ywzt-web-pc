using System;
using Dop.Core;
using Dop.Api.LogisticsUpdateOrder.Param;

namespace Dop.Api.LogisticsUpdateOrder
{
	public class LogisticsUpdateOrderRequest : DoudianOpApiRequest<LogisticsUpdateOrderParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/updateOrder";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsUpdateOrderResponse);
		}

		public  LogisticsUpdateOrderParam BuildParam()
		{
			return Param ?? (Param = new LogisticsUpdateOrderParam());
		}

	}
}
