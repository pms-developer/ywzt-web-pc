using System.Runtime.Serialization;

namespace Dop.Api.ProductGetComponentTemplate.Param
{
	public class ProductGetComponentTemplateParam
	{
		[DataMemberAttribute(Name = "template_type")]
		public string TemplateType { get; set; }

		[DataMemberAttribute(Name = "template_sub_type")]
		public string TemplateSubType { get; set; }

		[DataMemberAttribute(Name = "template_id")]
		public long? TemplateId { get; set; }

		[DataMemberAttribute(Name = "shareable")]
		public bool? Shareable { get; set; }

		[DataMemberAttribute(Name = "page_num")]
		public long? PageNum { get; set; }

		[DataMemberAttribute(Name = "page_size")]
		public long? PageSize { get; set; }

	}
}
