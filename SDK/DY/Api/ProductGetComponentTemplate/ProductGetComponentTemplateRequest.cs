using System;
using Dop.Core;
using Dop.Api.ProductGetComponentTemplate.Param;

namespace Dop.Api.ProductGetComponentTemplate
{
	public class ProductGetComponentTemplateRequest : DoudianOpApiRequest<ProductGetComponentTemplateParam>
	{
		public override string GetUrlPath()
		{
			return "/product/getComponentTemplate";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductGetComponentTemplateResponse);
		}

		public  ProductGetComponentTemplateParam BuildParam()
		{
			return Param ?? (Param = new ProductGetComponentTemplateParam());
		}

	}
}
