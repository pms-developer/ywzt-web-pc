using Dop.Core;
using Dop.Api.ProductGetComponentTemplate.Data;

namespace Dop.Api.ProductGetComponentTemplate
{
	public class ProductGetComponentTemplateResponse : DoudianOpApiResponse<ProductGetComponentTemplateData>
	{
	}
}
