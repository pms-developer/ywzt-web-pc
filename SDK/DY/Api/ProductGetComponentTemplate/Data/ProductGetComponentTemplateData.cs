using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductGetComponentTemplate.Data
{
	public class ProductGetComponentTemplateData
	{
		[DataMemberAttribute(Name = "component_template_info_list")]
		public List<ComponentTemplateInfoListItem> ComponentTemplateInfoList { get; set; }

		[DataMemberAttribute(Name = "total_num")]
		public long? TotalNum { get; set; }

	}
}
