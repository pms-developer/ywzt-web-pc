using System.Runtime.Serialization;

namespace Dop.Api.ProductGetComponentTemplate.Data
{
	public class Image
	{
		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

		[DataMemberAttribute(Name = "width")]
		public int? Width { get; set; }

		[DataMemberAttribute(Name = "height")]
		public int? Height { get; set; }

	}
}
