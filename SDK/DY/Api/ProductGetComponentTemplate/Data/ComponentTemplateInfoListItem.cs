using System.Runtime.Serialization;

namespace Dop.Api.ProductGetComponentTemplate.Data
{
	public class ComponentTemplateInfoListItem
	{
		[DataMemberAttribute(Name = "template_id")]
		public long? TemplateId { get; set; }

		[DataMemberAttribute(Name = "template_type")]
		public string TemplateType { get; set; }

		[DataMemberAttribute(Name = "template_sub_type")]
		public string TemplateSubType { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "component_data")]
		public string ComponentData { get; set; }

		[DataMemberAttribute(Name = "image")]
		public Image Image { get; set; }

		[DataMemberAttribute(Name = "shareable")]
		public bool? Shareable { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public long? UpdateTime { get; set; }

		[DataMemberAttribute(Name = "component_front_data")]
		public string ComponentFrontData { get; set; }

	}
}
