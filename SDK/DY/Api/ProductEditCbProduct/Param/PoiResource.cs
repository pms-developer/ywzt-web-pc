using System.Runtime.Serialization;

namespace Dop.Api.ProductEditCbProduct.Param
{
	public class PoiResource
	{
		[DataMemberAttribute(Name = "condition")]
		public string Condition { get; set; }

		[DataMemberAttribute(Name = "valid_days")]
		public long? ValidDays { get; set; }

		[DataMemberAttribute(Name = "valid_start")]
		public long? ValidStart { get; set; }

		[DataMemberAttribute(Name = "valid_end")]
		public long? ValidEnd { get; set; }

		[DataMemberAttribute(Name = "service_num")]
		public string ServiceNum { get; set; }

		[DataMemberAttribute(Name = "notification")]
		public string Notification { get; set; }

		[DataMemberAttribute(Name = "code_type")]
		public long? CodeType { get; set; }

		[DataMemberAttribute(Name = "total_can_use_count")]
		public int? TotalCanUseCount { get; set; }

		[DataMemberAttribute(Name = "coupon_second_exchange")]
		public long? CouponSecondExchange { get; set; }

		[DataMemberAttribute(Name = "link")]
		public string Link { get; set; }

		[DataMemberAttribute(Name = "count")]
		public long? Count { get; set; }

	}
}
