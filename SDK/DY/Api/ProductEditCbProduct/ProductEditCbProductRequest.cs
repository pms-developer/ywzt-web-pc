using System;
using Dop.Core;
using Dop.Api.ProductEditCbProduct.Param;

namespace Dop.Api.ProductEditCbProduct
{
	public class ProductEditCbProductRequest : DoudianOpApiRequest<ProductEditCbProductParam>
	{
		public override string GetUrlPath()
		{
			return "/product/editCbProduct";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductEditCbProductResponse);
		}

		public  ProductEditCbProductParam BuildParam()
		{
			return Param ?? (Param = new ProductEditCbProductParam());
		}

	}
}
