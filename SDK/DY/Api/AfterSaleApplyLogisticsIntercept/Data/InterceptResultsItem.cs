using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleApplyLogisticsIntercept.Data
{
	public class InterceptResultsItem
	{
		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "company_name")]
		public string CompanyName { get; set; }

		[DataMemberAttribute(Name = "tracking_no")]
		public string TrackingNo { get; set; }

		[DataMemberAttribute(Name = "value_amount")]
		public long? ValueAmount { get; set; }

		[DataMemberAttribute(Name = "can_intercept")]
		public bool? CanIntercept { get; set; }

		[DataMemberAttribute(Name = "is_success")]
		public bool? IsSuccess { get; set; }

		[DataMemberAttribute(Name = "unavailable_reason_code")]
		public long? UnavailableReasonCode { get; set; }

		[DataMemberAttribute(Name = "unavailable_reason")]
		public string UnavailableReason { get; set; }

		[DataMemberAttribute(Name = "intercept_cost")]
		public long? InterceptCost { get; set; }

		[DataMemberAttribute(Name = "cur_product")]
		public CurProduct CurProduct { get; set; }

		[DataMemberAttribute(Name = "other_products")]
		public List<OtherProductsItem> OtherProducts { get; set; }

		[DataMemberAttribute(Name = "other_product_amount")]
		public long? OtherProductAmount { get; set; }

	}
}
