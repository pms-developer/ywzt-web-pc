using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleApplyLogisticsIntercept.Data
{
	public class CurProduct
	{
		[DataMemberAttribute(Name = "order_id")]
		public long? OrderId { get; set; }

		[DataMemberAttribute(Name = "product_image")]
		public string ProductImage { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "product_spec")]
		public string ProductSpec { get; set; }

		[DataMemberAttribute(Name = "tags")]
		public List<string> Tags { get; set; }

		[DataMemberAttribute(Name = "price")]
		public long? Price { get; set; }

		[DataMemberAttribute(Name = "amount")]
		public long? Amount { get; set; }

	}
}
