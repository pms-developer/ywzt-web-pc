using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleApplyLogisticsIntercept.Data
{
	public class AfterSaleApplyLogisticsInterceptData
	{
		[DataMemberAttribute(Name = "intercept_results")]
		public List<InterceptResultsItem> InterceptResults { get; set; }

		[DataMemberAttribute(Name = "success_count")]
		public long? SuccessCount { get; set; }

		[DataMemberAttribute(Name = "failed_count")]
		public long? FailedCount { get; set; }

		[DataMemberAttribute(Name = "unavailable_reason_code")]
		public long? UnavailableReasonCode { get; set; }

		[DataMemberAttribute(Name = "unavailable_reason")]
		public string UnavailableReason { get; set; }

		[DataMemberAttribute(Name = "refund_amount")]
		public long? RefundAmount { get; set; }

	}
}
