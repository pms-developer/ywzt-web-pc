using System;
using Dop.Core;
using Dop.Api.AfterSaleApplyLogisticsIntercept.Param;

namespace Dop.Api.AfterSaleApplyLogisticsIntercept
{
	public class AfterSaleApplyLogisticsInterceptRequest : DoudianOpApiRequest<AfterSaleApplyLogisticsInterceptParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/applyLogisticsIntercept";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleApplyLogisticsInterceptResponse);
		}

		public  AfterSaleApplyLogisticsInterceptParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleApplyLogisticsInterceptParam());
		}

	}
}
