using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleApplyLogisticsIntercept.Param
{
	public class AfterSaleApplyLogisticsInterceptParam
	{
		[DataMemberAttribute(Name = "after_sale_id")]
		public long? AfterSaleId { get; set; }

		[DataMemberAttribute(Name = "op_from")]
		public int? OpFrom { get; set; }

		[DataMemberAttribute(Name = "intercept_targets")]
		public List<InterceptTargetsItem> InterceptTargets { get; set; }

	}
}
