using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleApplyLogisticsIntercept.Param
{
	public class InterceptTargetsItem
	{
		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "tracking_no")]
		public string TrackingNo { get; set; }

	}
}
