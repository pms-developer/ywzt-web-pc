using Dop.Core;
using Dop.Api.AfterSaleApplyLogisticsIntercept.Data;

namespace Dop.Api.AfterSaleApplyLogisticsIntercept
{
	public class AfterSaleApplyLogisticsInterceptResponse : DoudianOpApiResponse<AfterSaleApplyLogisticsInterceptData>
	{
	}
}
