using Dop.Core;
using Dop.Api.SkuSyncStock.Data;

namespace Dop.Api.SkuSyncStock
{
	public class SkuSyncStockResponse : DoudianOpApiResponse<SkuSyncStockData>
	{
	}
}
