using System;
using Dop.Core;
using Dop.Api.SkuSyncStock.Param;

namespace Dop.Api.SkuSyncStock
{
	public class SkuSyncStockRequest : DoudianOpApiRequest<SkuSyncStockParam>
	{
		public override string GetUrlPath()
		{
			return "/sku/syncStock";
		}

		public override Type GetResponseType()
		{
			return typeof(SkuSyncStockResponse);
		}

		public  SkuSyncStockParam BuildParam()
		{
			return Param ?? (Param = new SkuSyncStockParam());
		}

	}
}
