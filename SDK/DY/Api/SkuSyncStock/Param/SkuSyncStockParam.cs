using System.Runtime.Serialization;

namespace Dop.Api.SkuSyncStock.Param
{
	public class SkuSyncStockParam
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "out_sku_id")]
		public long? OutSkuId { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public long? OutProductId { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "supplier_id")]
		public string SupplierId { get; set; }

		[DataMemberAttribute(Name = "incremental")]
		public bool? Incremental { get; set; }

		[DataMemberAttribute(Name = "idempotent_id")]
		public string IdempotentId { get; set; }

		[DataMemberAttribute(Name = "stock_num")]
		public long? StockNum { get; set; }

		[DataMemberAttribute(Name = "step_stock_num")]
		public long? StepStockNum { get; set; }

	}
}
