using System;
using Dop.Core;
using Dop.Api.SpuGetSpuRule.Param;

namespace Dop.Api.SpuGetSpuRule
{
	public class SpuGetSpuRuleRequest : DoudianOpApiRequest<SpuGetSpuRuleParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/getSpuRule";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuGetSpuRuleResponse);
		}

		public  SpuGetSpuRuleParam BuildParam()
		{
			return Param ?? (Param = new SpuGetSpuRuleParam());
		}

	}
}
