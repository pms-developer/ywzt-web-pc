using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuRule.Data
{
	public class ValuesItem
	{
		[DataMemberAttribute(Name = "value_id")]
		public long? ValueId { get; set; }

		[DataMemberAttribute(Name = "value_name")]
		public string ValueName { get; set; }

	}
}
