using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuGetSpuRule.Data
{
	public class SpuPropertyRuleItem
	{
		[DataMemberAttribute(Name = "property_name")]
		public string PropertyName { get; set; }

		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "is_required")]
		public long? IsRequired { get; set; }

		[DataMemberAttribute(Name = "value_type")]
		public string ValueType { get; set; }

		[DataMemberAttribute(Name = "property_type")]
		public long? PropertyType { get; set; }

		[DataMemberAttribute(Name = "diy_type")]
		public long? DiyType { get; set; }

		[DataMemberAttribute(Name = "max_select_num")]
		public long? MaxSelectNum { get; set; }

		[DataMemberAttribute(Name = "values")]
		public List<ValuesItem> Values { get; set; }

	}
}
