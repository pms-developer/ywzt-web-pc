using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuRule.Data
{
	public class SpuGetSpuRuleData
	{
		[DataMemberAttribute(Name = "spu_property_rule")]
		public List<SpuPropertyRuleItem> SpuPropertyRule { get; set; }

		[DataMemberAttribute(Name = "spu_images_rule")]
		public SpuImagesRule SpuImagesRule { get; set; }

		[DataMemberAttribute(Name = "spu_actual_images_rule")]
		public SpuActualImagesRule SpuActualImagesRule { get; set; }

		[DataMemberAttribute(Name = "control_type")]
		public long? ControlType { get; set; }

		[DataMemberAttribute(Name = "support_spu_product")]
		public bool? SupportSpuProduct { get; set; }

		[DataMemberAttribute(Name = "support_create_spu")]
		public bool? SupportCreateSpu { get; set; }

	}
}
