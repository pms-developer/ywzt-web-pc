using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuRule.Data
{
	public class SpuImagesRule
	{
		[DataMemberAttribute(Name = "is_required")]
		public long? IsRequired { get; set; }

		[DataMemberAttribute(Name = "max_num")]
		public long? MaxNum { get; set; }

		[DataMemberAttribute(Name = "min_num")]
		public long? MinNum { get; set; }

	}
}
