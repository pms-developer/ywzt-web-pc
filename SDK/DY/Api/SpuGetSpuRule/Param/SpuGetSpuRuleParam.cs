using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuRule.Param
{
	public class SpuGetSpuRuleParam
	{
		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

	}
}
