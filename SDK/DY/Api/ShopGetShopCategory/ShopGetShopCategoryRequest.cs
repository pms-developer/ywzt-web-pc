using System;
using Dop.Core;
using Dop.Api.ShopGetShopCategory.Param;

namespace Dop.Api.ShopGetShopCategory
{
	public class ShopGetShopCategoryRequest : DoudianOpApiRequest<ShopGetShopCategoryParam>
	{
		public override string GetUrlPath()
		{
			return "/shop/getShopCategory";
		}

		public override Type GetResponseType()
		{
			return typeof(ShopGetShopCategoryResponse);
		}

		public  ShopGetShopCategoryParam BuildParam()
		{
			return Param ?? (Param = new ShopGetShopCategoryParam());
		}

	}
}
