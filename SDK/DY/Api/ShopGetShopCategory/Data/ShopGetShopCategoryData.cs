using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ShopGetShopCategory.Data
{
	public class ShopGetShopCategoryData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
