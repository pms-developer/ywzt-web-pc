using System.Runtime.Serialization;

namespace Dop.Api.ShopGetShopCategory.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "level")]
		public long? Level { get; set; }

		[DataMemberAttribute(Name = "parent_id")]
		public long? ParentId { get; set; }

		[DataMemberAttribute(Name = "is_leaf")]
		public bool? IsLeaf { get; set; }

		[DataMemberAttribute(Name = "enable")]
		public bool? Enable { get; set; }

	}
}
