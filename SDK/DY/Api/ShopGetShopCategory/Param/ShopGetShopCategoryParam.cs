using System.Runtime.Serialization;

namespace Dop.Api.ShopGetShopCategory.Param
{
	public class ShopGetShopCategoryParam
	{
		[DataMemberAttribute(Name = "cid")]
		public long? Cid { get; set; }

	}
}
