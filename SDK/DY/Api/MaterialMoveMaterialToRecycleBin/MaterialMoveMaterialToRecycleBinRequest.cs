using System;
using Dop.Core;
using Dop.Api.MaterialMoveMaterialToRecycleBin.Param;

namespace Dop.Api.MaterialMoveMaterialToRecycleBin
{
	public class MaterialMoveMaterialToRecycleBinRequest : DoudianOpApiRequest<MaterialMoveMaterialToRecycleBinParam>
	{
		public override string GetUrlPath()
		{
			return "/material/moveMaterialToRecycleBin";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialMoveMaterialToRecycleBinResponse);
		}

		public  MaterialMoveMaterialToRecycleBinParam BuildParam()
		{
			return Param ?? (Param = new MaterialMoveMaterialToRecycleBinParam());
		}

	}
}
