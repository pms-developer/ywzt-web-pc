using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialMoveMaterialToRecycleBin.Param
{
	public class MaterialMoveMaterialToRecycleBinParam
	{
		[DataMemberAttribute(Name = "material_ids")]
		public List<string> MaterialIds { get; set; }

	}
}
