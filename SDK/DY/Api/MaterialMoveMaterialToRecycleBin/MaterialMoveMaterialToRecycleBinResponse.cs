using Dop.Core;
using Dop.Api.MaterialMoveMaterialToRecycleBin.Data;

namespace Dop.Api.MaterialMoveMaterialToRecycleBin
{
	public class MaterialMoveMaterialToRecycleBinResponse : DoudianOpApiResponse<MaterialMoveMaterialToRecycleBinData>
	{
	}
}
