using System;
using Dop.Core;
using Dop.Api.MaterialGetCapInfo.Param;

namespace Dop.Api.MaterialGetCapInfo
{
	public class MaterialGetCapInfoRequest : DoudianOpApiRequest<MaterialGetCapInfoParam>
	{
		public override string GetUrlPath()
		{
			return "/material/get_cap_info";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialGetCapInfoResponse);
		}

		public  MaterialGetCapInfoParam BuildParam()
		{
			return Param ?? (Param = new MaterialGetCapInfoParam());
		}

	}
}
