using System.Runtime.Serialization;

namespace Dop.Api.MaterialGetCapInfo.Data
{
	public class EquityInfoItem
	{
		[DataMemberAttribute(Name = "equity_type")]
		public string EquityType { get; set; }

		[DataMemberAttribute(Name = "equity_type_desc")]
		public string EquityTypeDesc { get; set; }

		[DataMemberAttribute(Name = "begin_time")]
		public long? BeginTime { get; set; }

		[DataMemberAttribute(Name = "end_time")]
		public long? EndTime { get; set; }

		[DataMemberAttribute(Name = "total_capacity")]
		public long? TotalCapacity { get; set; }

		[DataMemberAttribute(Name = "photo_capacity")]
		public long? PhotoCapacity { get; set; }

		[DataMemberAttribute(Name = "video_capacity")]
		public long? VideoCapacity { get; set; }

	}
}
