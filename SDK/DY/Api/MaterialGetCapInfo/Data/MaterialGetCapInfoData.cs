using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.MaterialGetCapInfo.Data
{
	public class MaterialGetCapInfoData
	{
		[DataMemberAttribute(Name = "total_capacity")]
		public long? TotalCapacity { get; set; }

		[DataMemberAttribute(Name = "total_capacity_used")]
		public long? TotalCapacityUsed { get; set; }

		[DataMemberAttribute(Name = "photo_capacity")]
		public long? PhotoCapacity { get; set; }

		[DataMemberAttribute(Name = "photo_capacity_used")]
		public long? PhotoCapacityUsed { get; set; }

		[DataMemberAttribute(Name = "video_capacity")]
		public long? VideoCapacity { get; set; }

		[DataMemberAttribute(Name = "video_capacity_used")]
		public long? VideoCapacityUsed { get; set; }

		[DataMemberAttribute(Name = "equity_info")]
		public List<EquityInfoItem> EquityInfo { get; set; }

	}
}
