using Dop.Core;
using Dop.Api.LogisticsGetCustomTemplateList.Data;

namespace Dop.Api.LogisticsGetCustomTemplateList
{
	public class LogisticsGetCustomTemplateListResponse : DoudianOpApiResponse<LogisticsGetCustomTemplateListData>
	{
	}
}
