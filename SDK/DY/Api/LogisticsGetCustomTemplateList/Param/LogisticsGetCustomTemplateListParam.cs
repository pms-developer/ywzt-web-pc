using System.Runtime.Serialization;

namespace Dop.Api.LogisticsGetCustomTemplateList.Param
{
	public class LogisticsGetCustomTemplateListParam
	{
		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

	}
}
