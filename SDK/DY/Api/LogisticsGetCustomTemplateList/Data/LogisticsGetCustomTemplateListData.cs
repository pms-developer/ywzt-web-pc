using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsGetCustomTemplateList.Data
{
	public class LogisticsGetCustomTemplateListData
	{
		[DataMemberAttribute(Name = "custom_template_data")]
		public List<CustomTemplateDataItem> CustomTemplateData { get; set; }

	}
}
