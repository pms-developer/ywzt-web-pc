using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsGetCustomTemplateList.Data
{
	public class CustomTemplateInfosItem
	{
		[DataMemberAttribute(Name = "custom_template_code")]
		public string CustomTemplateCode { get; set; }

		[DataMemberAttribute(Name = "custom_template_name")]
		public string CustomTemplateName { get; set; }

		[DataMemberAttribute(Name = "parent_template_code")]
		public string ParentTemplateCode { get; set; }

		[DataMemberAttribute(Name = "custom_template_url")]
		public string CustomTemplateUrl { get; set; }

		[DataMemberAttribute(Name = "custom_template_key_list")]
		public List<string> CustomTemplateKeyList { get; set; }

		[DataMemberAttribute(Name = "custom_template_id")]
		public long? CustomTemplateId { get; set; }

		[DataMemberAttribute(Name = "parent_template_id")]
		public long? ParentTemplateId { get; set; }

	}
}
