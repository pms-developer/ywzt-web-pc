using System;
using Dop.Core;
using Dop.Api.LogisticsGetCustomTemplateList.Param;

namespace Dop.Api.LogisticsGetCustomTemplateList
{
	public class LogisticsGetCustomTemplateListRequest : DoudianOpApiRequest<LogisticsGetCustomTemplateListParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/getCustomTemplateList";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsGetCustomTemplateListResponse);
		}

		public  LogisticsGetCustomTemplateListParam BuildParam()
		{
			return Param ?? (Param = new LogisticsGetCustomTemplateListParam());
		}

	}
}
