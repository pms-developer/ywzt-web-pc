using System.Runtime.Serialization;

namespace Dop.Api.LogisticsListShopNetsite.Param
{
	public class LogisticsListShopNetsiteParam
	{
		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

	}
}
