using System;
using Dop.Core;
using Dop.Api.LogisticsListShopNetsite.Param;

namespace Dop.Api.LogisticsListShopNetsite
{
	public class LogisticsListShopNetsiteRequest : DoudianOpApiRequest<LogisticsListShopNetsiteParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/listShopNetsite";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsListShopNetsiteResponse);
		}

		public  LogisticsListShopNetsiteParam BuildParam()
		{
			return Param ?? (Param = new LogisticsListShopNetsiteParam());
		}

	}
}
