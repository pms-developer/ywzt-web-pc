using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsListShopNetsite.Data
{
	public class LogisticsListShopNetsiteData
	{
		[DataMemberAttribute(Name = "netsites")]
		public List<NetsitesItem> Netsites { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "company_type")]
		public int? CompanyType { get; set; }

	}
}
