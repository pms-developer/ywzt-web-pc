using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsListShopNetsite.Data
{
	public class NetsitesItem
	{
		[DataMemberAttribute(Name = "netsite_code")]
		public string NetsiteCode { get; set; }

		[DataMemberAttribute(Name = "netsite_name")]
		public string NetsiteName { get; set; }

		[DataMemberAttribute(Name = "amount")]
		public string Amount { get; set; }

		[DataMemberAttribute(Name = "sender_address")]
		public List<SenderAddressItem> SenderAddress { get; set; }

		[DataMemberAttribute(Name = "allocated_quantity")]
		public long? AllocatedQuantity { get; set; }

		[DataMemberAttribute(Name = "cancelled_quantity")]
		public long? CancelledQuantity { get; set; }

		[DataMemberAttribute(Name = "recycled_quantity")]
		public long? RecycledQuantity { get; set; }

		[DataMemberAttribute(Name = "company")]
		public string Company { get; set; }

		[DataMemberAttribute(Name = "company_type")]
		public int? CompanyType { get; set; }

	}
}
