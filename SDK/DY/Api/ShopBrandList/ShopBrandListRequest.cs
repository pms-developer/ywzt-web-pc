using System;
using Dop.Core;
using Dop.Api.ShopBrandList.Param;

namespace Dop.Api.ShopBrandList
{
	public class ShopBrandListRequest : DoudianOpApiRequest<ShopBrandListParam>
	{
		public override string GetUrlPath()
		{
			return "/shop/brandList";
		}

		public override Type GetResponseType()
		{
			return typeof(ShopBrandListResponse);
		}

		public  ShopBrandListParam BuildParam()
		{
			return Param ?? (Param = new ShopBrandListParam());
		}

	}
}
