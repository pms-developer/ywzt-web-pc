using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ShopBrandList.Data
{
	public class ShopBrandListData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
