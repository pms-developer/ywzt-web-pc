using System.Runtime.Serialization;

namespace Dop.Api.ShopBrandList.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "brand_chinese_name")]
		public string BrandChineseName { get; set; }

		[DataMemberAttribute(Name = "brand_english_name")]
		public string BrandEnglishName { get; set; }

		[DataMemberAttribute(Name = "brand_reg_num")]
		public string BrandRegNum { get; set; }

	}
}
