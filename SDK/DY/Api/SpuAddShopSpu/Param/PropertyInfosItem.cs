using System.Runtime.Serialization;

namespace Dop.Api.SpuAddShopSpu.Param
{
	public class PropertyInfosItem
	{
		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "value_id")]
		public long? ValueId { get; set; }

		[DataMemberAttribute(Name = "parent_value_id")]
		public long? ParentValueId { get; set; }

		[DataMemberAttribute(Name = "level")]
		public long? Level { get; set; }

		[DataMemberAttribute(Name = "value_name")]
		public string ValueName { get; set; }

	}
}
