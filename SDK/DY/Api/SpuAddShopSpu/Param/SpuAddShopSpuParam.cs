using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuAddShopSpu.Param
{
	public class SpuAddShopSpuParam
	{
		[DataMemberAttribute(Name = "spu_name")]
		public string SpuName { get; set; }

		[DataMemberAttribute(Name = "upc_code")]
		public string UpcCode { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

		[DataMemberAttribute(Name = "brand_id")]
		public long? BrandId { get; set; }

		[DataMemberAttribute(Name = "property_infos")]
		public List<PropertyInfosItem> PropertyInfos { get; set; }

	}
}
