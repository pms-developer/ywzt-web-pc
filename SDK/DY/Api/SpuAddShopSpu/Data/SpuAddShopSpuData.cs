using System.Runtime.Serialization;

namespace Dop.Api.SpuAddShopSpu.Data
{
	public class SpuAddShopSpuData
	{
		[DataMemberAttribute(Name = "spu_id")]
		public string SpuId { get; set; }

	}
}
