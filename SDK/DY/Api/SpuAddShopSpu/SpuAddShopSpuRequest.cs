using System;
using Dop.Core;
using Dop.Api.SpuAddShopSpu.Param;

namespace Dop.Api.SpuAddShopSpu
{
	public class SpuAddShopSpuRequest : DoudianOpApiRequest<SpuAddShopSpuParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/addShopSpu";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuAddShopSpuResponse);
		}

		public  SpuAddShopSpuParam BuildParam()
		{
			return Param ?? (Param = new SpuAddShopSpuParam());
		}

	}
}
