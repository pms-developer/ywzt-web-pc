using System;
using Dop.Core;
using Dop.Api.MaterialEditFolder.Param;

namespace Dop.Api.MaterialEditFolder
{
	public class MaterialEditFolderRequest : DoudianOpApiRequest<MaterialEditFolderParam>
	{
		public override string GetUrlPath()
		{
			return "/material/editFolder";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialEditFolderResponse);
		}

		public  MaterialEditFolderParam BuildParam()
		{
			return Param ?? (Param = new MaterialEditFolderParam());
		}

	}
}
