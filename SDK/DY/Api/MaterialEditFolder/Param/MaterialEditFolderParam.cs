using System.Runtime.Serialization;

namespace Dop.Api.MaterialEditFolder.Param
{
	public class MaterialEditFolderParam
	{
		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "to_folder_id")]
		public string ToFolderId { get; set; }

	}
}
