using Dop.Core;
using Dop.Api.MaterialEditFolder.Data;

namespace Dop.Api.MaterialEditFolder
{
	public class MaterialEditFolderResponse : DoudianOpApiResponse<MaterialEditFolderData>
	{
	}
}
