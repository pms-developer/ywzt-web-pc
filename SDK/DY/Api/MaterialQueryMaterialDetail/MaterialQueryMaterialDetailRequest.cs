using System;
using Dop.Core;
using Dop.Api.MaterialQueryMaterialDetail.Param;

namespace Dop.Api.MaterialQueryMaterialDetail
{
	public class MaterialQueryMaterialDetailRequest : DoudianOpApiRequest<MaterialQueryMaterialDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/material/queryMaterialDetail";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialQueryMaterialDetailResponse);
		}

		public  MaterialQueryMaterialDetailParam BuildParam()
		{
			return Param ?? (Param = new MaterialQueryMaterialDetailParam());
		}

	}
}
