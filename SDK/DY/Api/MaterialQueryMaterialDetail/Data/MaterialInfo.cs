using System.Runtime.Serialization;

namespace Dop.Api.MaterialQueryMaterialDetail.Data
{
	public class MaterialInfo
	{
		[DataMemberAttribute(Name = "material_id")]
		public string MaterialId { get; set; }

		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "origin_url")]
		public string OriginUrl { get; set; }

		[DataMemberAttribute(Name = "byte_url")]
		public string ByteUrl { get; set; }

		[DataMemberAttribute(Name = "materil_name")]
		public string MaterilName { get; set; }

		[DataMemberAttribute(Name = "material_type")]
		public string MaterialType { get; set; }

		[DataMemberAttribute(Name = "operate_status")]
		public int? OperateStatus { get; set; }

		[DataMemberAttribute(Name = "audit_status")]
		public int? AuditStatus { get; set; }

		[DataMemberAttribute(Name = "audit_reject_desc")]
		public string AuditRejectDesc { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "photo_info")]
		public PhotoInfo PhotoInfo { get; set; }

		[DataMemberAttribute(Name = "video_info")]
		public VideoInfo VideoInfo { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public string UpdateTime { get; set; }

		[DataMemberAttribute(Name = "delete_time")]
		public string DeleteTime { get; set; }

	}
}
