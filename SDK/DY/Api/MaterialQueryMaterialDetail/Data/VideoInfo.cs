using System.Runtime.Serialization;

namespace Dop.Api.MaterialQueryMaterialDetail.Data
{
	public class VideoInfo
	{
		[DataMemberAttribute(Name = "format")]
		public string Format { get; set; }

		[DataMemberAttribute(Name = "duration")]
		public double? Duration { get; set; }

		[DataMemberAttribute(Name = "vid")]
		public string Vid { get; set; }

		[DataMemberAttribute(Name = "height")]
		public int? Height { get; set; }

		[DataMemberAttribute(Name = "width")]
		public int? Width { get; set; }

		[DataMemberAttribute(Name = "video_cover_url")]
		public string VideoCoverUrl { get; set; }

	}
}
