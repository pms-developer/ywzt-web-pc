using System.Runtime.Serialization;

namespace Dop.Api.MaterialQueryMaterialDetail.Data
{
	public class MaterialQueryMaterialDetailData
	{
		[DataMemberAttribute(Name = "material_info")]
		public MaterialInfo MaterialInfo { get; set; }

	}
}
