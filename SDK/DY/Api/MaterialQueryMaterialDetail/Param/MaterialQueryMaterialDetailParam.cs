using System.Runtime.Serialization;

namespace Dop.Api.MaterialQueryMaterialDetail.Param
{
	public class MaterialQueryMaterialDetailParam
	{
		[DataMemberAttribute(Name = "material_id")]
		public string MaterialId { get; set; }

	}
}
