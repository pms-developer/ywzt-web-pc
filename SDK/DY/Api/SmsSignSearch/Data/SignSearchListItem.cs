using System.Runtime.Serialization;

namespace Dop.Api.SmsSignSearch.Data
{
	public class SignSearchListItem
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "sign")]
		public string Sign { get; set; }

	}
}
