using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SmsSignSearch.Data
{
	public class SmsSignSearchData
	{
		[DataMemberAttribute(Name = "sign_search_list")]
		public List<SignSearchListItem> SignSearchList { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
