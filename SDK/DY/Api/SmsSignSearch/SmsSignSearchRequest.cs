using System;
using Dop.Core;
using Dop.Api.SmsSignSearch.Param;

namespace Dop.Api.SmsSignSearch
{
	public class SmsSignSearchRequest : DoudianOpApiRequest<SmsSignSearchParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/sign/search";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsSignSearchResponse);
		}

		public  SmsSignSearchParam BuildParam()
		{
			return Param ?? (Param = new SmsSignSearchParam());
		}

	}
}
