using System.Runtime.Serialization;

namespace Dop.Api.AntispamOrderQuery.Data
{
	public class AntispamOrderQueryData
	{
		[DataMemberAttribute(Name = "decision")]
		public Decision Decision { get; set; }

	}
}
