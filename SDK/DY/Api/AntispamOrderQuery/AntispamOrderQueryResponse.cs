using Dop.Core;
using Dop.Api.AntispamOrderQuery.Data;

namespace Dop.Api.AntispamOrderQuery
{
	public class AntispamOrderQueryResponse : DoudianOpApiResponse<AntispamOrderQueryData>
	{
	}
}
