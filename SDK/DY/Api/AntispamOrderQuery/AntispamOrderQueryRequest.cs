using System;
using Dop.Core;
using Dop.Api.AntispamOrderQuery.Param;

namespace Dop.Api.AntispamOrderQuery
{
	public class AntispamOrderQueryRequest : DoudianOpApiRequest<AntispamOrderQueryParam>
	{
		public override string GetUrlPath()
		{
			return "/antispam/orderQuery";
		}

		public override Type GetResponseType()
		{
			return typeof(AntispamOrderQueryResponse);
		}

		public  AntispamOrderQueryParam BuildParam()
		{
			return Param ?? (Param = new AntispamOrderQueryParam());
		}

	}
}
