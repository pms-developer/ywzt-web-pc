using System.Runtime.Serialization;

namespace Dop.Api.AntispamOrderQuery.Param
{
	public class User
	{
		[DataMemberAttribute(Name = "uid_type")]
		public int? UidType { get; set; }

		[DataMemberAttribute(Name = "user_id")]
		public long? UserId { get; set; }

	}
}
