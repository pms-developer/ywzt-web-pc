using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsTemplateList.Data
{
	public class LogisticsTemplateListData
	{
		[DataMemberAttribute(Name = "template_data")]
		public List<TemplateDataItem> TemplateData { get; set; }

	}
}
