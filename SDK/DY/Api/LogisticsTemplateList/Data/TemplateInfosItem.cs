using System.Runtime.Serialization;

namespace Dop.Api.LogisticsTemplateList.Data
{
	public class TemplateInfosItem
	{
		[DataMemberAttribute(Name = "template_id")]
		public long? TemplateId { get; set; }

		[DataMemberAttribute(Name = "template_code")]
		public string TemplateCode { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "template_url")]
		public string TemplateUrl { get; set; }

		[DataMemberAttribute(Name = "version")]
		public int? Version { get; set; }

		[DataMemberAttribute(Name = "template_type")]
		public int? TemplateType { get; set; }

		[DataMemberAttribute(Name = "perview_url")]
		public string PerviewUrl { get; set; }

	}
}
