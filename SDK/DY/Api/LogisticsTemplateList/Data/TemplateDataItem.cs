using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsTemplateList.Data
{
	public class TemplateDataItem
	{
		[DataMemberAttribute(Name = "template_infos")]
		public List<TemplateInfosItem> TemplateInfos { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

	}
}
