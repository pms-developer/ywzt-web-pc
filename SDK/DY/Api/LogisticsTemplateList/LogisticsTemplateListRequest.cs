using System;
using Dop.Core;
using Dop.Api.LogisticsTemplateList.Param;

namespace Dop.Api.LogisticsTemplateList
{
	public class LogisticsTemplateListRequest : DoudianOpApiRequest<LogisticsTemplateListParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/templateList";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsTemplateListResponse);
		}

		public  LogisticsTemplateListParam BuildParam()
		{
			return Param ?? (Param = new LogisticsTemplateListParam());
		}

	}
}
