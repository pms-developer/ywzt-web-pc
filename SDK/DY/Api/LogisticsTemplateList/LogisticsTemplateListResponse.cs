using Dop.Core;
using Dop.Api.LogisticsTemplateList.Data;

namespace Dop.Api.LogisticsTemplateList
{
	public class LogisticsTemplateListResponse : DoudianOpApiResponse<LogisticsTemplateListData>
	{
	}
}
