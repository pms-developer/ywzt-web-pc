using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SmsSendResult.Data
{
	public class SmsSendResultData
	{
		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "sms_send_result_list")]
		public List<SmsSendResultListItem> SmsSendResultList { get; set; }

	}
}
