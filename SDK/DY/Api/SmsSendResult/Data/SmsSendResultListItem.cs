using System.Runtime.Serialization;

namespace Dop.Api.SmsSendResult.Data
{
	public class SmsSendResultListItem
	{
		[DataMemberAttribute(Name = "send_time")]
		public long? SendTime { get; set; }

		[DataMemberAttribute(Name = "sms_content")]
		public string SmsContent { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "count")]
		public long? Count { get; set; }

		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

		[DataMemberAttribute(Name = "message_id")]
		public string MessageId { get; set; }

		[DataMemberAttribute(Name = "tag")]
		public string Tag { get; set; }

	}
}
