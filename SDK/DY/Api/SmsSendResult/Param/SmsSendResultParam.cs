using System.Runtime.Serialization;

namespace Dop.Api.SmsSendResult.Param
{
	public class SmsSendResultParam
	{
		[DataMemberAttribute(Name = "from_time")]
		public long? FromTime { get; set; }

		[DataMemberAttribute(Name = "to_time")]
		public long? ToTime { get; set; }

		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "template_id")]
		public string TemplateId { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "sign")]
		public string Sign { get; set; }

		[DataMemberAttribute(Name = "post_tel")]
		public string PostTel { get; set; }

		[DataMemberAttribute(Name = "message_id")]
		public string MessageId { get; set; }

	}
}
