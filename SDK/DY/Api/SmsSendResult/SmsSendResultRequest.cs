using System;
using Dop.Core;
using Dop.Api.SmsSendResult.Param;

namespace Dop.Api.SmsSendResult
{
	public class SmsSendResultRequest : DoudianOpApiRequest<SmsSendResultParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/sendResult";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsSendResultResponse);
		}

		public  SmsSendResultParam BuildParam()
		{
			return Param ?? (Param = new SmsSendResultParam());
		}

	}
}
