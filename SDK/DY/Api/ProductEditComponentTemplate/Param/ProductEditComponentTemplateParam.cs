using System.Runtime.Serialization;

namespace Dop.Api.ProductEditComponentTemplate.Param
{
	public class ProductEditComponentTemplateParam
	{
		[DataMemberAttribute(Name = "template_id")]
		public long? TemplateId { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "component_data")]
		public string ComponentData { get; set; }

		[DataMemberAttribute(Name = "shareable")]
		public bool? Shareable { get; set; }

	}
}
