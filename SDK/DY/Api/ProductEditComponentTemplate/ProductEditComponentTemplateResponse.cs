using Dop.Core;
using Dop.Api.ProductEditComponentTemplate.Data;

namespace Dop.Api.ProductEditComponentTemplate
{
	public class ProductEditComponentTemplateResponse : DoudianOpApiResponse<ProductEditComponentTemplateData>
	{
	}
}
