using System;
using Dop.Core;
using Dop.Api.ProductEditComponentTemplate.Param;

namespace Dop.Api.ProductEditComponentTemplate
{
	public class ProductEditComponentTemplateRequest : DoudianOpApiRequest<ProductEditComponentTemplateParam>
	{
		public override string GetUrlPath()
		{
			return "/product/editComponentTemplate";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductEditComponentTemplateResponse);
		}

		public  ProductEditComponentTemplateParam BuildParam()
		{
			return Param ?? (Param = new ProductEditComponentTemplateParam());
		}

	}
}
