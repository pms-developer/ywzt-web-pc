using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialDeleteFolder.Param
{
	public class MaterialDeleteFolderParam
	{
		[DataMemberAttribute(Name = "folder_ids")]
		public List<string> FolderIds { get; set; }

	}
}
