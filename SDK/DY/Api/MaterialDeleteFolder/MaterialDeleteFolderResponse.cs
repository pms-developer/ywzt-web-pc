using Dop.Core;
using Dop.Api.MaterialDeleteFolder.Data;

namespace Dop.Api.MaterialDeleteFolder
{
	public class MaterialDeleteFolderResponse : DoudianOpApiResponse<MaterialDeleteFolderData>
	{
	}
}
