using System;
using Dop.Core;
using Dop.Api.MaterialDeleteFolder.Param;

namespace Dop.Api.MaterialDeleteFolder
{
	public class MaterialDeleteFolderRequest : DoudianOpApiRequest<MaterialDeleteFolderParam>
	{
		public override string GetUrlPath()
		{
			return "/material/deleteFolder";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialDeleteFolderResponse);
		}

		public  MaterialDeleteFolderParam BuildParam()
		{
			return Param ?? (Param = new MaterialDeleteFolderParam());
		}

	}
}
