using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialDeleteFolder.Data
{
	public class MaterialDeleteFolderData
	{
		[DataMemberAttribute(Name = "success_ids")]
		public List<string> SuccessIds { get; set; }

		[DataMemberAttribute(Name = "failed_map")]
		public Dictionary<string,FailedMapItem> FailedMap { get; set; }

	}
}
