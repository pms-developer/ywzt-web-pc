using System.Runtime.Serialization;

namespace Dop.Api.MaterialDeleteFolder.Data
{
	public class FailedMapItem
	{
		[DataMemberAttribute(Name = "code")]
		public int? Code { get; set; }

		[DataMemberAttribute(Name = "msg")]
		public string Msg { get; set; }

	}
}
