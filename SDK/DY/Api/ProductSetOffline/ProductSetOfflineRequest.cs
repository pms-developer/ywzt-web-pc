using System;
using Dop.Core;
using Dop.Api.ProductSetOffline.Param;

namespace Dop.Api.ProductSetOffline
{
	public class ProductSetOfflineRequest : DoudianOpApiRequest<ProductSetOfflineParam>
	{
		public override string GetUrlPath()
		{
			return "/product/setOffline";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductSetOfflineResponse);
		}

		public  ProductSetOfflineParam BuildParam()
		{
			return Param ?? (Param = new ProductSetOfflineParam());
		}

	}
}
