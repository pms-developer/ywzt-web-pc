using System.Runtime.Serialization;

namespace Dop.Api.ProductSetOffline.Param
{
	public class ProductSetOfflineParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

	}
}
