using System.Runtime.Serialization;

namespace Dop.Api.AddressAreaList.Param
{
	public class AddressAreaListParam
	{
		[DataMemberAttribute(Name = "city_id")]
		public long? CityId { get; set; }

	}
}
