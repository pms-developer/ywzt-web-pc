using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AddressAreaList.Data
{
	public class AddressAreaListData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
