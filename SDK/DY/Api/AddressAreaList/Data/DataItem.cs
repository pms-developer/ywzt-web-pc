using System.Runtime.Serialization;

namespace Dop.Api.AddressAreaList.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "area_id")]
		public long? AreaId { get; set; }

		[DataMemberAttribute(Name = "area")]
		public string Area { get; set; }

		[DataMemberAttribute(Name = "father_id")]
		public long? FatherId { get; set; }

	}
}
