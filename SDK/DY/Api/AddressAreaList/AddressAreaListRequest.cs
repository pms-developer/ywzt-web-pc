using System;
using Dop.Core;
using Dop.Api.AddressAreaList.Param;

namespace Dop.Api.AddressAreaList
{
	public class AddressAreaListRequest : DoudianOpApiRequest<AddressAreaListParam>
	{
		public override string GetUrlPath()
		{
			return "/address/areaList";
		}

		public override Type GetResponseType()
		{
			return typeof(AddressAreaListResponse);
		}

		public  AddressAreaListParam BuildParam()
		{
			return Param ?? (Param = new AddressAreaListParam());
		}

	}
}
