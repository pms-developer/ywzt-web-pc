using System.Runtime.Serialization;

namespace Dop.Api.LogisticsRegisterPackageRoute.Param
{
	public class Sender
	{
		[DataMemberAttribute(Name = "contact")]
		public Contact Contact { get; set; }

		[DataMemberAttribute(Name = "address")]
		public Address Address { get; set; }

	}
}
