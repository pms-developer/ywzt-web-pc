using System.Runtime.Serialization;

namespace Dop.Api.LogisticsRegisterPackageRoute.Param
{
	public class Town
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

	}
}
