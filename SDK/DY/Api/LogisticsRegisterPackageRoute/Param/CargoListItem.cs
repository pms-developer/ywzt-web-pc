using System.Runtime.Serialization;

namespace Dop.Api.LogisticsRegisterPackageRoute.Param
{
	public class CargoListItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "quantity")]
		public int? Quantity { get; set; }

		[DataMemberAttribute(Name = "volume")]
		public int? Volume { get; set; }

		[DataMemberAttribute(Name = "total_weight")]
		public int? TotalWeight { get; set; }

		[DataMemberAttribute(Name = "total_net_weight")]
		public int? TotalNetWeight { get; set; }

		[DataMemberAttribute(Name = "unit")]
		public string Unit { get; set; }

	}
}
