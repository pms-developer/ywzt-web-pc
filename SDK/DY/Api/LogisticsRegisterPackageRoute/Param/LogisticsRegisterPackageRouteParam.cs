using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsRegisterPackageRoute.Param
{
	public class LogisticsRegisterPackageRouteParam
	{
		[DataMemberAttribute(Name = "express")]
		public string Express { get; set; }

		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "outer_order_id")]
		public string OuterOrderId { get; set; }

		[DataMemberAttribute(Name = "outer_sub_order_id")]
		public string OuterSubOrderId { get; set; }

		[DataMemberAttribute(Name = "callback_url")]
		public string CallbackUrl { get; set; }

		[DataMemberAttribute(Name = "receiver")]
		public Receiver Receiver { get; set; }

		[DataMemberAttribute(Name = "sender")]
		public Sender Sender { get; set; }

		[DataMemberAttribute(Name = "cargo_list")]
		public List<CargoListItem> CargoList { get; set; }

		[DataMemberAttribute(Name = "extend")]
		public Dictionary<string,string> Extend { get; set; }

	}
}
