using System.Runtime.Serialization;

namespace Dop.Api.LogisticsRegisterPackageRoute.Param
{
	public class Contact
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "phone")]
		public string Phone { get; set; }

		[DataMemberAttribute(Name = "email")]
		public string Email { get; set; }

	}
}
