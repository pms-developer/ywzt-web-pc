using System;
using Dop.Core;
using Dop.Api.LogisticsRegisterPackageRoute.Param;

namespace Dop.Api.LogisticsRegisterPackageRoute
{
	public class LogisticsRegisterPackageRouteRequest : DoudianOpApiRequest<LogisticsRegisterPackageRouteParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/registerPackageRoute";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsRegisterPackageRouteResponse);
		}

		public  LogisticsRegisterPackageRouteParam BuildParam()
		{
			return Param ?? (Param = new LogisticsRegisterPackageRouteParam());
		}

	}
}
