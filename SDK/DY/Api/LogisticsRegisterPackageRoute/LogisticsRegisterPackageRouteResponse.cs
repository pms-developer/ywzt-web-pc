using Dop.Core;
using Dop.Api.LogisticsRegisterPackageRoute.Data;

namespace Dop.Api.LogisticsRegisterPackageRoute
{
	public class LogisticsRegisterPackageRouteResponse : DoudianOpApiResponse<LogisticsRegisterPackageRouteData>
	{
	}
}
