using System;
using Dop.Core;
using Dop.Api.SkuEditPrice.Param;

namespace Dop.Api.SkuEditPrice
{
	public class SkuEditPriceRequest : DoudianOpApiRequest<SkuEditPriceParam>
	{
		public override string GetUrlPath()
		{
			return "/sku/editPrice";
		}

		public override Type GetResponseType()
		{
			return typeof(SkuEditPriceResponse);
		}

		public  SkuEditPriceParam BuildParam()
		{
			return Param ?? (Param = new SkuEditPriceParam());
		}

	}
}
