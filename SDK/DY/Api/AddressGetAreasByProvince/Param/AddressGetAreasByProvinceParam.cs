using System.Runtime.Serialization;

namespace Dop.Api.AddressGetAreasByProvince.Param
{
	public class AddressGetAreasByProvinceParam
	{
		[DataMemberAttribute(Name = "province_id")]
		public long? ProvinceId { get; set; }

	}
}
