using Dop.Core;
using Dop.Api.AddressGetAreasByProvince.Data;

namespace Dop.Api.AddressGetAreasByProvince
{
	public class AddressGetAreasByProvinceResponse : DoudianOpApiResponse<AddressGetAreasByProvinceData>
	{
	}
}
