using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AddressGetAreasByProvince.Data
{
	public class AddressGetAreasByProvinceData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
