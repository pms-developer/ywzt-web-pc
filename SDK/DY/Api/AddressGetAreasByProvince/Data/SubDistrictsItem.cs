using System.Runtime.Serialization;

namespace Dop.Api.AddressGetAreasByProvince.Data
{
	public class SubDistrictsItem
	{
		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "father_code")]
		public long? FatherCode { get; set; }

		[DataMemberAttribute(Name = "level")]
		public string Level { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

	}
}
