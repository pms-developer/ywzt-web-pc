using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AddressGetAreasByProvince.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "father_code")]
		public long? FatherCode { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "level")]
		public string Level { get; set; }

		[DataMemberAttribute(Name = "sub_districts")]
		public List<SubDistrictsItem_3> SubDistricts { get; set; }

	}
}
