using System;
using Dop.Core;
using Dop.Api.AddressGetAreasByProvince.Param;

namespace Dop.Api.AddressGetAreasByProvince
{
	public class AddressGetAreasByProvinceRequest : DoudianOpApiRequest<AddressGetAreasByProvinceParam>
	{
		public override string GetUrlPath()
		{
			return "/address/getAreasByProvince";
		}

		public override Type GetResponseType()
		{
			return typeof(AddressGetAreasByProvinceResponse);
		}

		public  AddressGetAreasByProvinceParam BuildParam()
		{
			return Param ?? (Param = new AddressGetAreasByProvinceParam());
		}

	}
}
