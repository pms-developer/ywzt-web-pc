using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderGetSettleBillDetailV3.Data
{
	public class OrderGetSettleBillDetailV3Data
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "code_msg")]
		public string CodeMsg { get; set; }

		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "next_start_index")]
		public string NextStartIndex { get; set; }

		[DataMemberAttribute(Name = "next_start_time")]
		public string NextStartTime { get; set; }

		[DataMemberAttribute(Name = "is_end")]
		public int? IsEnd { get; set; }

		[DataMemberAttribute(Name = "data_size")]
		public long? DataSize { get; set; }

	}
}
