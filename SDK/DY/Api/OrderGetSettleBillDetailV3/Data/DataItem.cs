using System.Runtime.Serialization;

namespace Dop.Api.OrderGetSettleBillDetailV3.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "settle_time")]
		public string SettleTime { get; set; }

		[DataMemberAttribute(Name = "request_no")]
		public string RequestNo { get; set; }

		[DataMemberAttribute(Name = "shop_order_id")]
		public string ShopOrderId { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "settle_amount")]
		public long? SettleAmount { get; set; }

		[DataMemberAttribute(Name = "pay_type_desc")]
		public string PayTypeDesc { get; set; }

		[DataMemberAttribute(Name = "trade_type")]
		public int? TradeType { get; set; }

		[DataMemberAttribute(Name = "is_contains_refund_before_settle")]
		public int? IsContainsRefundBeforeSettle { get; set; }

		[DataMemberAttribute(Name = "order_time")]
		public string OrderTime { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public string ProductId { get; set; }

		[DataMemberAttribute(Name = "goods_count")]
		public int? GoodsCount { get; set; }

		[DataMemberAttribute(Name = "flow_type_desc")]
		public string FlowTypeDesc { get; set; }

		[DataMemberAttribute(Name = "order_type")]
		public string OrderType { get; set; }

		[DataMemberAttribute(Name = "total_amount")]
		public long? TotalAmount { get; set; }

		[DataMemberAttribute(Name = "total_goods_amount")]
		public long? TotalGoodsAmount { get; set; }

		[DataMemberAttribute(Name = "post_amount")]
		public long? PostAmount { get; set; }

		[DataMemberAttribute(Name = "shop_coupon")]
		public long? ShopCoupon { get; set; }

		[DataMemberAttribute(Name = "refund_before_settle")]
		public long? RefundBeforeSettle { get; set; }

		[DataMemberAttribute(Name = "platform_coupon")]
		public long? PlatformCoupon { get; set; }

		[DataMemberAttribute(Name = "author_coupon")]
		public long? AuthorCoupon { get; set; }

		[DataMemberAttribute(Name = "zt_pay_promotion")]
		public long? ZtPayPromotion { get; set; }

		[DataMemberAttribute(Name = "zr_pay_promotion")]
		public long? ZrPayPromotion { get; set; }

		[DataMemberAttribute(Name = "real_pay_amount")]
		public long? RealPayAmount { get; set; }

		[DataMemberAttribute(Name = "total_income")]
		public long? TotalIncome { get; set; }

		[DataMemberAttribute(Name = "platform_service_fee")]
		public long? PlatformServiceFee { get; set; }

		[DataMemberAttribute(Name = "commission")]
		public long? Commission { get; set; }

		[DataMemberAttribute(Name = "good_learn_channel_fee")]
		public long? GoodLearnChannelFee { get; set; }

		[DataMemberAttribute(Name = "colonel_service_fee")]
		public long? ColonelServiceFee { get; set; }

		[DataMemberAttribute(Name = "channel_promotion_fee")]
		public long? ChannelPromotionFee { get; set; }

		[DataMemberAttribute(Name = "other_sharing_amount")]
		public long? OtherSharingAmount { get; set; }

		[DataMemberAttribute(Name = "total_outcome")]
		public long? TotalOutcome { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

	}
}
