using System;
using Dop.Core;
using Dop.Api.OrderGetSettleBillDetailV3.Param;

namespace Dop.Api.OrderGetSettleBillDetailV3
{
	public class OrderGetSettleBillDetailV3Request : DoudianOpApiRequest<OrderGetSettleBillDetailV3Param>
	{
		public override string GetUrlPath()
		{
			return "/order/getSettleBillDetailV3";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderGetSettleBillDetailV3Response);
		}

		public  OrderGetSettleBillDetailV3Param BuildParam()
		{
			return Param ?? (Param = new OrderGetSettleBillDetailV3Param());
		}

	}
}
