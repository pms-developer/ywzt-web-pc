using Dop.Core;
using Dop.Api.OrderGetSettleBillDetailV3.Data;

namespace Dop.Api.OrderGetSettleBillDetailV3
{
	public class OrderGetSettleBillDetailV3Response : DoudianOpApiResponse<OrderGetSettleBillDetailV3Data>
	{
	}
}
