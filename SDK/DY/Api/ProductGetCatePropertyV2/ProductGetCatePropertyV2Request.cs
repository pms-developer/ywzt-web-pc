using System;
using Dop.Core;
using Dop.Api.ProductGetCatePropertyV2.Param;

namespace Dop.Api.ProductGetCatePropertyV2
{
	public class ProductGetCatePropertyV2Request : DoudianOpApiRequest<ProductGetCatePropertyV2Param>
	{
		public override string GetUrlPath()
		{
			return "/product/getCatePropertyV2";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductGetCatePropertyV2Response);
		}

		public  ProductGetCatePropertyV2Param BuildParam()
		{
			return Param ?? (Param = new ProductGetCatePropertyV2Param());
		}

	}
}
