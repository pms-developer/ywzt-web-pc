using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductGetCatePropertyV2.Data
{
	public class ProductGetCatePropertyV2Data
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

		[DataMemberAttribute(Name = "tpl_type")]
		public long? TplType { get; set; }

	}
}
