using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductGetCatePropertyV2.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "cid")]
		public long? Cid { get; set; }

		[DataMemberAttribute(Name = "property_name")]
		public string PropertyName { get; set; }

		[DataMemberAttribute(Name = "options")]
		public List<OptionsItem> Options { get; set; }

		[DataMemberAttribute(Name = "required")]
		public long? Required { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "type")]
		public string Type { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

		[DataMemberAttribute(Name = "multi_select_max")]
		public long? MultiSelectMax { get; set; }

		[DataMemberAttribute(Name = "property_type")]
		public long? PropertyType { get; set; }

		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "sequence")]
		public long? Sequence { get; set; }

		[DataMemberAttribute(Name = "relation_id")]
		public long? RelationId { get; set; }

		[DataMemberAttribute(Name = "diy_type")]
		public long? DiyType { get; set; }

		[DataMemberAttribute(Name = "important_type")]
		public long? ImportantType { get; set; }

	}
}
