using System.Runtime.Serialization;

namespace Dop.Api.ProductGetCatePropertyV2.Param
{
	public class ProductGetCatePropertyV2Param
	{
		[DataMemberAttribute(Name = "category_leaf_id")]
		public long? CategoryLeafId { get; set; }

	}
}
