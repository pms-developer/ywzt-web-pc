using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductQualityList.Data
{
	public class ProductQualityListData
	{
		[DataMemberAttribute(Name = "quality_list")]
		public List<QualityListItem> QualityList { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
