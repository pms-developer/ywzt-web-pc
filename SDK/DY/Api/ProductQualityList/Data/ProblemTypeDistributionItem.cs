using System.Runtime.Serialization;

namespace Dop.Api.ProductQualityList.Data
{
	public class ProblemTypeDistributionItem
	{
		[DataMemberAttribute(Name = "type_name")]
		public string TypeName { get; set; }

		[DataMemberAttribute(Name = "num")]
		public long? Num { get; set; }

	}
}
