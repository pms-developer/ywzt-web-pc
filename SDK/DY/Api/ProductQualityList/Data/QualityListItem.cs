using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductQualityList.Data
{
	public class QualityListItem
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "problem_num_to_improve")]
		public long? ProblemNumToImprove { get; set; }

		[DataMemberAttribute(Name = "problem_type_distribution")]
		public List<ProblemTypeDistributionItem> ProblemTypeDistribution { get; set; }

		[DataMemberAttribute(Name = "meet_standard")]
		public long? MeetStandard { get; set; }

		[DataMemberAttribute(Name = "base_score")]
		public long? BaseScore { get; set; }

	}
}
