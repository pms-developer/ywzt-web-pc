using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductQualityList.Param
{
	public class ProductQualityListParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "order_by")]
		public string OrderBy { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "page_size")]
		public long? PageSize { get; set; }

		[DataMemberAttribute(Name = "task_id")]
		public long? TaskId { get; set; }

		[DataMemberAttribute(Name = "diagnose_status")]
		public List<long> DiagnoseStatus { get; set; }

	}
}
