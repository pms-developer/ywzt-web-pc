using System;
using Dop.Core;
using Dop.Api.ProductQualityList.Param;

namespace Dop.Api.ProductQualityList
{
	public class ProductQualityListRequest : DoudianOpApiRequest<ProductQualityListParam>
	{
		public override string GetUrlPath()
		{
			return "/product/qualityList";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductQualityListResponse);
		}

		public  ProductQualityListParam BuildParam()
		{
			return Param ?? (Param = new ProductQualityListParam());
		}

	}
}
