using System;
using Dop.Core;
using Dop.Api.SpuCreateSpu.Param;

namespace Dop.Api.SpuCreateSpu
{
	public class SpuCreateSpuRequest : DoudianOpApiRequest<SpuCreateSpuParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/createSpu";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuCreateSpuResponse);
		}

		public  SpuCreateSpuParam BuildParam()
		{
			return Param ?? (Param = new SpuCreateSpuParam());
		}

	}
}
