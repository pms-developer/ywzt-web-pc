using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuCreateSpu.Param
{
	public class SpuCreateSpuParam
	{
		[DataMemberAttribute(Name = "category_leaf_id")]
		public long? CategoryLeafId { get; set; }

		[DataMemberAttribute(Name = "spu_images")]
		public List<string> SpuImages { get; set; }

		[DataMemberAttribute(Name = "property_infos")]
		public List<PropertyInfosItem> PropertyInfos { get; set; }

		[DataMemberAttribute(Name = "spu_actual_images")]
		public List<string> SpuActualImages { get; set; }

	}
}
