using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuCreateSpu.Param
{
	public class PropertyInfosItem
	{
		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "values")]
		public List<ValuesItem> Values { get; set; }

	}
}
