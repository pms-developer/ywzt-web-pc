using System.Runtime.Serialization;

namespace Dop.Api.SpuCreateSpu.Data
{
	public class SpuCreateSpuData
	{
		[DataMemberAttribute(Name = "spu_id")]
		public string SpuId { get; set; }

	}
}
