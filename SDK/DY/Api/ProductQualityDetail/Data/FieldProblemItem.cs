using System.Runtime.Serialization;

namespace Dop.Api.ProductQualityDetail.Data
{
	public class FieldProblemItem
	{
		[DataMemberAttribute(Name = "field_key")]
		public string FieldKey { get; set; }

		[DataMemberAttribute(Name = "field_name")]
		public string FieldName { get; set; }

		[DataMemberAttribute(Name = "problem_key")]
		public long? ProblemKey { get; set; }

		[DataMemberAttribute(Name = "problem_name")]
		public string ProblemName { get; set; }

		[DataMemberAttribute(Name = "suggestion")]
		public string Suggestion { get; set; }

		[DataMemberAttribute(Name = "problem_type")]
		public long? ProblemType { get; set; }

	}
}
