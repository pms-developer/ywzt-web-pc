using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductQualityDetail.Data
{
	public class ProductQualityDetailData
	{
		[DataMemberAttribute(Name = "field_problem")]
		public List<FieldProblemItem> FieldProblem { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

	}
}
