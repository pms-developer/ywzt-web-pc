using System;
using Dop.Core;
using Dop.Api.ProductQualityDetail.Param;

namespace Dop.Api.ProductQualityDetail
{
	public class ProductQualityDetailRequest : DoudianOpApiRequest<ProductQualityDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/product/qualityDetail";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductQualityDetailResponse);
		}

		public  ProductQualityDetailParam BuildParam()
		{
			return Param ?? (Param = new ProductQualityDetailParam());
		}

	}
}
