using System.Runtime.Serialization;

namespace Dop.Api.ProductQualityDetail.Param
{
	public class ProductQualityDetailParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

	}
}
