using System;
using Dop.Core;
using Dop.Api.OrderAddressModify.Param;

namespace Dop.Api.OrderAddressModify
{
	public class OrderAddressModifyRequest : DoudianOpApiRequest<OrderAddressModifyParam>
	{
		public override string GetUrlPath()
		{
			return "/order/addressModify";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderAddressModifyResponse);
		}

		public  OrderAddressModifyParam BuildParam()
		{
			return Param ?? (Param = new OrderAddressModifyParam());
		}

	}
}
