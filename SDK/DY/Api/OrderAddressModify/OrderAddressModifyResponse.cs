using Dop.Core;
using Dop.Api.OrderAddressModify.Data;

namespace Dop.Api.OrderAddressModify
{
	public class OrderAddressModifyResponse : DoudianOpApiResponse<OrderAddressModifyData>
	{
	}
}
