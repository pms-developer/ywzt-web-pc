using System.Runtime.Serialization;

namespace Dop.Api.OrderAddressModify.Param
{
	public class OrderAddressModifyParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "post_addr")]
		public PostAddr PostAddr { get; set; }

		[DataMemberAttribute(Name = "post_receiver")]
		public string PostReceiver { get; set; }

		[DataMemberAttribute(Name = "post_tel")]
		public string PostTel { get; set; }

	}
}
