using System.Runtime.Serialization;

namespace Dop.Api.OrderAddressModify.Param
{
	public class PostAddr
	{
		[DataMemberAttribute(Name = "province")]
		public Province Province { get; set; }

		[DataMemberAttribute(Name = "city")]
		public City City { get; set; }

		[DataMemberAttribute(Name = "town")]
		public Town Town { get; set; }

		[DataMemberAttribute(Name = "street")]
		public Street Street { get; set; }

		[DataMemberAttribute(Name = "address_detail")]
		public string AddressDetail { get; set; }

		[DataMemberAttribute(Name = "land_mark")]
		public string LandMark { get; set; }

	}
}
