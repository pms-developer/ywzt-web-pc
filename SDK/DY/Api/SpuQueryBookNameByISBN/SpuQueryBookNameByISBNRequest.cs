using System;
using Dop.Core;
using Dop.Api.SpuQueryBookNameByISBN.Param;

namespace Dop.Api.SpuQueryBookNameByISBN
{
	public class SpuQueryBookNameByISBNRequest : DoudianOpApiRequest<SpuQueryBookNameByISBNParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/queryBookNameByISBN";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuQueryBookNameByISBNResponse);
		}

		public  SpuQueryBookNameByISBNParam BuildParam()
		{
			return Param ?? (Param = new SpuQueryBookNameByISBNParam());
		}

	}
}
