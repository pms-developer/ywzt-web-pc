using System.Runtime.Serialization;

namespace Dop.Api.SpuQueryBookNameByISBN.Param
{
	public class SpuQueryBookNameByISBNParam
	{
		[DataMemberAttribute(Name = "category_leaf_id")]
		public long? CategoryLeafId { get; set; }

		[DataMemberAttribute(Name = "isbn")]
		public string Isbn { get; set; }

		[DataMemberAttribute(Name = "page_no")]
		public long? PageNo { get; set; }

		[DataMemberAttribute(Name = "page_size")]
		public long? PageSize { get; set; }

	}
}
