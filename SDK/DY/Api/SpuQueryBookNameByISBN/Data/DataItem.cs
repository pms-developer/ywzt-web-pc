using System.Runtime.Serialization;

namespace Dop.Api.SpuQueryBookNameByISBN.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "spu_id")]
		public string SpuId { get; set; }

		[DataMemberAttribute(Name = "book_name")]
		public string BookName { get; set; }

		[DataMemberAttribute(Name = "category_leaf_id")]
		public long? CategoryLeafId { get; set; }

	}
}
