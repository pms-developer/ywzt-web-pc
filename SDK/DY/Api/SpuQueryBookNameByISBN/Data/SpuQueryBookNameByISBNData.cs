using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SpuQueryBookNameByISBN.Data
{
	public class SpuQueryBookNameByISBNData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
