using Dop.Core;
using Dop.Api.SpuQueryBookNameByISBN.Data;

namespace Dop.Api.SpuQueryBookNameByISBN
{
	public class SpuQueryBookNameByISBNResponse : DoudianOpApiResponse<SpuQueryBookNameByISBNData>
	{
	}
}
