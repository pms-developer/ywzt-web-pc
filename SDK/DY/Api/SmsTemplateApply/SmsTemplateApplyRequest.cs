using System;
using Dop.Core;
using Dop.Api.SmsTemplateApply.Param;

namespace Dop.Api.SmsTemplateApply
{
	public class SmsTemplateApplyRequest : DoudianOpApiRequest<SmsTemplateApplyParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/template/apply";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsTemplateApplyResponse);
		}

		public  SmsTemplateApplyParam BuildParam()
		{
			return Param ?? (Param = new SmsTemplateApplyParam());
		}

	}
}
