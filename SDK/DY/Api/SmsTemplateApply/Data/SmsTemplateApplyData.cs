using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateApply.Data
{
	public class SmsTemplateApplyData
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "template_content")]
		public string TemplateContent { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "sms_template_id")]
		public string SmsTemplateId { get; set; }

		[DataMemberAttribute(Name = "sms_template_apply_id")]
		public string SmsTemplateApplyId { get; set; }

		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

	}
}
