using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateApply.Param
{
	public class SmsTemplateApplyParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "template_type")]
		public string TemplateType { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "template_content")]
		public string TemplateContent { get; set; }

	}
}
