using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleList.Param
{
	public class AfterSaleListParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "aftersale_type")]
		public long? AftersaleType { get; set; }

		[DataMemberAttribute(Name = "aftersale_status")]
		public long? AftersaleStatus { get; set; }

		[DataMemberAttribute(Name = "reason")]
		public long? Reason { get; set; }

		[DataMemberAttribute(Name = "logistics_status")]
		public long? LogisticsStatus { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public long? PayType { get; set; }

		[DataMemberAttribute(Name = "refund_type")]
		public long? RefundType { get; set; }

		[DataMemberAttribute(Name = "arbitrate_status")]
		public List<long> ArbitrateStatus { get; set; }

		[DataMemberAttribute(Name = "order_flag")]
		public List<long> OrderFlag { get; set; }

		[DataMemberAttribute(Name = "start_time")]
		public long? StartTime { get; set; }

		[DataMemberAttribute(Name = "end_time")]
		public long? EndTime { get; set; }

		[DataMemberAttribute(Name = "amount_start")]
		public long? AmountStart { get; set; }

		[DataMemberAttribute(Name = "amount_end")]
		public long? AmountEnd { get; set; }

		[DataMemberAttribute(Name = "risk_flag")]
		public long? RiskFlag { get; set; }

		[DataMemberAttribute(Name = "order_by")]
		public List<string> OrderBy { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "aftersale_id")]
		public string AftersaleId { get; set; }

		[DataMemberAttribute(Name = "standard_aftersale_status")]
		public List<long> StandardAftersaleStatus { get; set; }

		[DataMemberAttribute(Name = "need_special_type")]
		public bool? NeedSpecialType { get; set; }

		[DataMemberAttribute(Name = "update_start_time")]
		public long? UpdateStartTime { get; set; }

		[DataMemberAttribute(Name = "update_end_time")]
		public long? UpdateEndTime { get; set; }

		[DataMemberAttribute(Name = "order_logistics_tracking_no")]
		public List<string> OrderLogisticsTrackingNo { get; set; }

		[DataMemberAttribute(Name = "order_logistics_state")]
		public List<long> OrderLogisticsState { get; set; }

		[DataMemberAttribute(Name = "agree_refuse_sign")]
		public List<long> AgreeRefuseSign { get; set; }

	}
}
