using System;
using Dop.Core;
using Dop.Api.AfterSaleList.Param;

namespace Dop.Api.AfterSaleList
{
	public class AfterSaleListRequest : DoudianOpApiRequest<AfterSaleListParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/List";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleListResponse);
		}

		public  AfterSaleListParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleListParam());
		}

	}
}
