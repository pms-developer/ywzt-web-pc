using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleList.Data
{
	public class AftersaleInfo
	{
		[DataMemberAttribute(Name = "aftersale_id")]
		public string AftersaleId { get; set; }

		[DataMemberAttribute(Name = "aftersale_order_type")]
		public long? AftersaleOrderType { get; set; }

		[DataMemberAttribute(Name = "aftersale_type")]
		public long? AftersaleType { get; set; }

		[DataMemberAttribute(Name = "aftersale_status")]
		public long? AftersaleStatus { get; set; }

		[DataMemberAttribute(Name = "related_id")]
		public string RelatedId { get; set; }

		[DataMemberAttribute(Name = "apply_time")]
		public long? ApplyTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public long? UpdateTime { get; set; }

		[DataMemberAttribute(Name = "status_deadline")]
		public long? StatusDeadline { get; set; }

		[DataMemberAttribute(Name = "refund_amount")]
		public long? RefundAmount { get; set; }

		[DataMemberAttribute(Name = "refund_post_amount")]
		public long? RefundPostAmount { get; set; }

		[DataMemberAttribute(Name = "aftersale_num")]
		public long? AftersaleNum { get; set; }

		[DataMemberAttribute(Name = "part_type")]
		public long? PartType { get; set; }

		[DataMemberAttribute(Name = "aftersale_refund_type")]
		public long? AftersaleRefundType { get; set; }

		[DataMemberAttribute(Name = "refund_type")]
		public long? RefundType { get; set; }

		[DataMemberAttribute(Name = "arbitrate_status")]
		public long? ArbitrateStatus { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "refund_tax_amount")]
		public long? RefundTaxAmount { get; set; }

		[DataMemberAttribute(Name = "left_urge_sms_count")]
		public long? LeftUrgeSmsCount { get; set; }

		[DataMemberAttribute(Name = "return_logistics_code")]
		public string ReturnLogisticsCode { get; set; }

		[DataMemberAttribute(Name = "risk_decision_code")]
		public long? RiskDecisionCode { get; set; }

		[DataMemberAttribute(Name = "risk_decision_reason")]
		public string RiskDecisionReason { get; set; }

		[DataMemberAttribute(Name = "risk_decision_description")]
		public string RiskDecisionDescription { get; set; }

		[DataMemberAttribute(Name = "return_promotion_amount")]
		public long? ReturnPromotionAmount { get; set; }

		[DataMemberAttribute(Name = "refund_status")]
		public long? RefundStatus { get; set; }

		[DataMemberAttribute(Name = "arbitrate_blame")]
		public long? ArbitrateBlame { get; set; }

		[DataMemberAttribute(Name = "exchange_sku_info")]
		public ExchangeSkuInfo ExchangeSkuInfo { get; set; }

		[DataMemberAttribute(Name = "return_logistics_company_name")]
		public string ReturnLogisticsCompanyName { get; set; }

		[DataMemberAttribute(Name = "exchange_logistics_company_name")]
		public string ExchangeLogisticsCompanyName { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "got_pkg")]
		public long? GotPkg { get; set; }

		[DataMemberAttribute(Name = "order_logistics")]
		public List<OrderLogisticsItem> OrderLogistics { get; set; }

		[DataMemberAttribute(Name = "is_agree_refuse_sign")]
		public long? IsAgreeRefuseSign { get; set; }

		[DataMemberAttribute(Name = "reason_second_labels")]
		public List<ReasonSecondLabelsItem> ReasonSecondLabels { get; set; }

		[DataMemberAttribute(Name = "aftersale_tags")]
		public List<AftersaleTagsItem> AftersaleTags { get; set; }

	}
}
