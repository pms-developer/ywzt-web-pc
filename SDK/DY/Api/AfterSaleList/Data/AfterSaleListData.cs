using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleList.Data
{
	public class AfterSaleListData
	{
		[DataMemberAttribute(Name = "items")]
		public List<ItemsItem> Items { get; set; }

		[DataMemberAttribute(Name = "has_more")]
		public bool? HasMore { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

	}
}
