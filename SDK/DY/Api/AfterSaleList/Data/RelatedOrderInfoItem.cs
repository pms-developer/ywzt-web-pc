using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleList.Data
{
	public class RelatedOrderInfoItem
	{
		[DataMemberAttribute(Name = "sku_order_id")]
		public string SkuOrderId { get; set; }

		[DataMemberAttribute(Name = "order_status")]
		public long? OrderStatus { get; set; }

		[DataMemberAttribute(Name = "pay_amount")]
		public long? PayAmount { get; set; }

		[DataMemberAttribute(Name = "post_amount")]
		public long? PostAmount { get; set; }

		[DataMemberAttribute(Name = "item_num")]
		public long? ItemNum { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "tax_amount")]
		public long? TaxAmount { get; set; }

		[DataMemberAttribute(Name = "is_oversea_order")]
		public long? IsOverseaOrder { get; set; }

		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "product_image")]
		public string ProductImage { get; set; }

		[DataMemberAttribute(Name = "tags")]
		public List<TagsItem> Tags { get; set; }

		[DataMemberAttribute(Name = "sku_spec")]
		public List<SkuSpecItem> SkuSpec { get; set; }

		[DataMemberAttribute(Name = "shop_sku_code")]
		public string ShopSkuCode { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "aftersale_pay_amount")]
		public long? AftersalePayAmount { get; set; }

		[DataMemberAttribute(Name = "aftersale_post_amount")]
		public long? AftersalePostAmount { get; set; }

		[DataMemberAttribute(Name = "aftersale_tax_amount")]
		public long? AftersaleTaxAmount { get; set; }

		[DataMemberAttribute(Name = "aftersale_item_num")]
		public long? AftersaleItemNum { get; set; }

		[DataMemberAttribute(Name = "promotion_pay_amount")]
		public long? PromotionPayAmount { get; set; }

		[DataMemberAttribute(Name = "price")]
		public long? Price { get; set; }

		[DataMemberAttribute(Name = "logistics_company_name")]
		public string LogisticsCompanyName { get; set; }

		[DataMemberAttribute(Name = "given_sku_order_ids")]
		public List<string> GivenSkuOrderIds { get; set; }

	}
}
