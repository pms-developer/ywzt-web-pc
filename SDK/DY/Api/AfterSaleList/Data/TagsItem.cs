using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleList.Data
{
	public class TagsItem
	{
		[DataMemberAttribute(Name = "tag_detail")]
		public string TagDetail { get; set; }

		[DataMemberAttribute(Name = "tag_detail_en")]
		public string TagDetailEn { get; set; }

		[DataMemberAttribute(Name = "tag_link_url")]
		public string TagLinkUrl { get; set; }

	}
}
