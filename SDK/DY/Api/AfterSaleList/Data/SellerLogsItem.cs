using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleList.Data
{
	public class SellerLogsItem
	{
		[DataMemberAttribute(Name = "content")]
		public string Content { get; set; }

		[DataMemberAttribute(Name = "op_name")]
		public string OpName { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "star")]
		public long? Star { get; set; }

	}
}
