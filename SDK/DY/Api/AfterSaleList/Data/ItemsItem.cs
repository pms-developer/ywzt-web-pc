using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleList.Data
{
	public class ItemsItem
	{
		[DataMemberAttribute(Name = "aftersale_info")]
		public AftersaleInfo AftersaleInfo { get; set; }

		[DataMemberAttribute(Name = "order_info")]
		public OrderInfo OrderInfo { get; set; }

		[DataMemberAttribute(Name = "text_part")]
		public TextPart TextPart { get; set; }

		[DataMemberAttribute(Name = "seller_logs")]
		public List<SellerLogsItem> SellerLogs { get; set; }

	}
}
