using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleList.Data
{
	public class TextPart
	{
		[DataMemberAttribute(Name = "logistics_text")]
		public string LogisticsText { get; set; }

		[DataMemberAttribute(Name = "aftersale_status_text")]
		public string AftersaleStatusText { get; set; }

		[DataMemberAttribute(Name = "aftersale_type_text")]
		public string AftersaleTypeText { get; set; }

		[DataMemberAttribute(Name = "return_logistics_text")]
		public string ReturnLogisticsText { get; set; }

		[DataMemberAttribute(Name = "aftersale_refund_type_text")]
		public string AftersaleRefundTypeText { get; set; }

		[DataMemberAttribute(Name = "reason_text")]
		public string ReasonText { get; set; }

		[DataMemberAttribute(Name = "bad_item_text")]
		public string BadItemText { get; set; }

		[DataMemberAttribute(Name = "arbitrate_status_text")]
		public string ArbitrateStatusText { get; set; }

	}
}
