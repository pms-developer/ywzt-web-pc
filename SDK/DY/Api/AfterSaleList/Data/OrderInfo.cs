using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleList.Data
{
	public class OrderInfo
	{
		[DataMemberAttribute(Name = "shop_order_id")]
		public string ShopOrderId { get; set; }

		[DataMemberAttribute(Name = "related_order_info")]
		public List<RelatedOrderInfoItem> RelatedOrderInfo { get; set; }

		[DataMemberAttribute(Name = "order_flag")]
		public long? OrderFlag { get; set; }

	}
}
