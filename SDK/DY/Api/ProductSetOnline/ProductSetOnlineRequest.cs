using System;
using Dop.Core;
using Dop.Api.ProductSetOnline.Param;

namespace Dop.Api.ProductSetOnline
{
	public class ProductSetOnlineRequest : DoudianOpApiRequest<ProductSetOnlineParam>
	{
		public override string GetUrlPath()
		{
			return "/product/setOnline";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductSetOnlineResponse);
		}

		public  ProductSetOnlineParam BuildParam()
		{
			return Param ?? (Param = new ProductSetOnlineParam());
		}

	}
}
