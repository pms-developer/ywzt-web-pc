using System.Runtime.Serialization;

namespace Dop.Api.ProductSetOnline.Param
{
	public class ProductSetOnlineParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

	}
}
