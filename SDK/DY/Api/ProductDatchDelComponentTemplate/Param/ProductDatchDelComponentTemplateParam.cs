using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductDatchDelComponentTemplate.Param
{
	public class ProductDatchDelComponentTemplateParam
	{
		[DataMemberAttribute(Name = "template_id")]
		public List<long> TemplateId { get; set; }

	}
}
