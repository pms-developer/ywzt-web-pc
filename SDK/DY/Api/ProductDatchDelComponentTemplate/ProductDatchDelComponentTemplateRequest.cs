using System;
using Dop.Core;
using Dop.Api.ProductDatchDelComponentTemplate.Param;

namespace Dop.Api.ProductDatchDelComponentTemplate
{
	public class ProductDatchDelComponentTemplateRequest : DoudianOpApiRequest<ProductDatchDelComponentTemplateParam>
	{
		public override string GetUrlPath()
		{
			return "/product/datchDelComponentTemplate";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductDatchDelComponentTemplateResponse);
		}

		public  ProductDatchDelComponentTemplateParam BuildParam()
		{
			return Param ?? (Param = new ProductDatchDelComponentTemplateParam());
		}

	}
}
