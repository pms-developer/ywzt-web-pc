using System.Runtime.Serialization;

namespace Dop.Api.SmsSignApply.Data
{
	public class SmsSignApplyData
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "sms_sign_apply_id")]
		public string SmsSignApplyId { get; set; }

		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

		[DataMemberAttribute(Name = "sign")]
		public string Sign { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

	}
}
