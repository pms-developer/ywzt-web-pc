using System;
using Dop.Core;
using Dop.Api.SmsSignApply.Param;

namespace Dop.Api.SmsSignApply
{
	public class SmsSignApplyRequest : DoudianOpApiRequest<SmsSignApplyParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/sign/apply";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsSignApplyResponse);
		}

		public  SmsSignApplyParam BuildParam()
		{
			return Param ?? (Param = new SmsSignApplyParam());
		}

	}
}
