using System;
using Dop.Core;
using Dop.Api.SkuEditCode.Param;

namespace Dop.Api.SkuEditCode
{
	public class SkuEditCodeRequest : DoudianOpApiRequest<SkuEditCodeParam>
	{
		public override string GetUrlPath()
		{
			return "/sku/editCode";
		}

		public override Type GetResponseType()
		{
			return typeof(SkuEditCodeResponse);
		}

		public  SkuEditCodeParam BuildParam()
		{
			return Param ?? (Param = new SkuEditCodeParam());
		}

	}
}
