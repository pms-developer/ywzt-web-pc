using System.Runtime.Serialization;

namespace Dop.Api.SkuEditCode.Param
{
	public class SkuEditCodeParam
	{
		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "out_sku_id")]
		public long? OutSkuId { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "out_product_id")]
		public long? OutProductId { get; set; }

	}
}
