using System.Runtime.Serialization;

namespace Dop.Api.MaterialCreateFolder.Param
{
	public class MaterialCreateFolderParam
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "parent_folder_id")]
		public string ParentFolderId { get; set; }

		[DataMemberAttribute(Name = "type")]
		public int? Type { get; set; }

	}
}
