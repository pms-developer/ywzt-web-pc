using Dop.Core;
using Dop.Api.MaterialCreateFolder.Data;

namespace Dop.Api.MaterialCreateFolder
{
	public class MaterialCreateFolderResponse : DoudianOpApiResponse<MaterialCreateFolderData>
	{
	}
}
