using System.Runtime.Serialization;

namespace Dop.Api.MaterialCreateFolder.Data
{
	public class MaterialCreateFolderData
	{
		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "parent_folder_id")]
		public string ParentFolderId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "type")]
		public int? Type { get; set; }

	}
}
