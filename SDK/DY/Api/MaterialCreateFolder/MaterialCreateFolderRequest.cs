using System;
using Dop.Core;
using Dop.Api.MaterialCreateFolder.Param;

namespace Dop.Api.MaterialCreateFolder
{
	public class MaterialCreateFolderRequest : DoudianOpApiRequest<MaterialCreateFolderParam>
	{
		public override string GetUrlPath()
		{
			return "/material/createFolder";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialCreateFolderResponse);
		}

		public  MaterialCreateFolderParam BuildParam()
		{
			return Param ?? (Param = new MaterialCreateFolderParam());
		}

	}
}
