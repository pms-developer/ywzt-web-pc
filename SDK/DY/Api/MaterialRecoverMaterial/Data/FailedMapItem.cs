using System.Runtime.Serialization;

namespace Dop.Api.MaterialRecoverMaterial.Data
{
	public class FailedMapItem
	{
		[DataMemberAttribute(Name = "code")]
		public int? Code { get; set; }

		[DataMemberAttribute(Name = "msg")]
		public string Msg { get; set; }

	}
}
