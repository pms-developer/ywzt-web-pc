using System;
using Dop.Core;
using Dop.Api.MaterialRecoverMaterial.Param;

namespace Dop.Api.MaterialRecoverMaterial
{
	public class MaterialRecoverMaterialRequest : DoudianOpApiRequest<MaterialRecoverMaterialParam>
	{
		public override string GetUrlPath()
		{
			return "/material/recoverMaterial";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialRecoverMaterialResponse);
		}

		public  MaterialRecoverMaterialParam BuildParam()
		{
			return Param ?? (Param = new MaterialRecoverMaterialParam());
		}

	}
}
