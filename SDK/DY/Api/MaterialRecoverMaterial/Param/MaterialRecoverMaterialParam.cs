using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialRecoverMaterial.Param
{
	public class MaterialRecoverMaterialParam
	{
		[DataMemberAttribute(Name = "material_ids")]
		public List<string> MaterialIds { get; set; }

	}
}
