using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.BrandGetSug.Data
{
	public class BrandGetSugData
	{
		[DataMemberAttribute(Name = "sug_list")]
		public List<SugListItem> SugList { get; set; }

	}
}
