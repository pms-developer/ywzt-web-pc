using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.BrandGetSug.Param
{
	public class FilterInfo
	{
		[DataMemberAttribute(Name = "brand_ids")]
		public List<long> BrandIds { get; set; }

		[DataMemberAttribute(Name = "brand_category")]
		public List<long> BrandCategory { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "related_ids")]
		public List<long> RelatedIds { get; set; }

		[DataMemberAttribute(Name = "trade_mark_ids")]
		public List<string> TradeMarkIds { get; set; }

		[DataMemberAttribute(Name = "audit_status")]
		public List<int> AuditStatus { get; set; }

	}
}
