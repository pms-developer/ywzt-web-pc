using System.Runtime.Serialization;

namespace Dop.Api.BrandGetSug.Param
{
	public class ExtraConfig
	{
		[DataMemberAttribute(Name = "use_origin_brand_info")]
		public bool? UseOriginBrandInfo { get; set; }

		[DataMemberAttribute(Name = "use_brand_info")]
		public bool? UseBrandInfo { get; set; }

		[DataMemberAttribute(Name = "use_brand_name_deduplicate")]
		public bool? UseBrandNameDeduplicate { get; set; }

	}
}
