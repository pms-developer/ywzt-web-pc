using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.BrandGetSug.Param
{
	public class BrandGetSugParam
	{
		[DataMemberAttribute(Name = "query")]
		public string Query { get; set; }

		[DataMemberAttribute(Name = "user_id")]
		public long? UserId { get; set; }

		[DataMemberAttribute(Name = "filter_info")]
		public FilterInfo FilterInfo { get; set; }

		[DataMemberAttribute(Name = "read_old")]
		public bool? ReadOld { get; set; }

		[DataMemberAttribute(Name = "biz_types")]
		public List<int> BizTypes { get; set; }

		[DataMemberAttribute(Name = "enable_deduplicate")]
		public bool? EnableDeduplicate { get; set; }

		[DataMemberAttribute(Name = "extra_config")]
		public ExtraConfig ExtraConfig { get; set; }

	}
}
