using System;
using Dop.Core;
using Dop.Api.BrandGetSug.Param;

namespace Dop.Api.BrandGetSug
{
	public class BrandGetSugRequest : DoudianOpApiRequest<BrandGetSugParam>
	{
		public override string GetUrlPath()
		{
			return "/brand/getSug";
		}

		public override Type GetResponseType()
		{
			return typeof(BrandGetSugResponse);
		}

		public  BrandGetSugParam BuildParam()
		{
			return Param ?? (Param = new BrandGetSugParam());
		}

	}
}
