using System.Runtime.Serialization;

namespace Dop.Api.SecurityBatchReportOrderSecurityEvent.Param
{
	public class IdentifyInfoListItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "encrypted")]
		public bool? Encrypted { get; set; }

	}
}
