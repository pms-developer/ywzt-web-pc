using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SecurityBatchReportOrderSecurityEvent.Param
{
	public class EventsItem
	{
		[DataMemberAttribute(Name = "account_id")]
		public string AccountId { get; set; }

		[DataMemberAttribute(Name = "shop_ids")]
		public List<string> ShopIds { get; set; }

		[DataMemberAttribute(Name = "order_ids")]
		public List<string> OrderIds { get; set; }

		[DataMemberAttribute(Name = "operation_type")]
		public int? OperationType { get; set; }

		[DataMemberAttribute(Name = "operate_time")]
		public string OperateTime { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

		[DataMemberAttribute(Name = "ip")]
		public string Ip { get; set; }

		[DataMemberAttribute(Name = "mac")]
		public string Mac { get; set; }

		[DataMemberAttribute(Name = "identify_info_list")]
		public List<IdentifyInfoListItem> IdentifyInfoList { get; set; }

		[DataMemberAttribute(Name = "device_type")]
		public string DeviceType { get; set; }

		[DataMemberAttribute(Name = "device_id")]
		public string DeviceId { get; set; }

		[DataMemberAttribute(Name = "referer")]
		public string Referer { get; set; }

		[DataMemberAttribute(Name = "user_agent")]
		public string UserAgent { get; set; }

		[DataMemberAttribute(Name = "order_related_shop_id")]
		public string OrderRelatedShopId { get; set; }

		[DataMemberAttribute(Name = "account_type")]
		public string AccountType { get; set; }

		[DataMemberAttribute(Name = "event_id")]
		public string EventId { get; set; }

	}
}
