using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SecurityBatchReportOrderSecurityEvent.Param
{
	public class SecurityBatchReportOrderSecurityEventParam
	{
		[DataMemberAttribute(Name = "event_type")]
		public int? EventType { get; set; }

		[DataMemberAttribute(Name = "events")]
		public List<EventsItem> Events { get; set; }

	}
}
