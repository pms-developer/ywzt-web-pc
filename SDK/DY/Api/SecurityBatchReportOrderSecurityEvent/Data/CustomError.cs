using System.Runtime.Serialization;

namespace Dop.Api.SecurityBatchReportOrderSecurityEvent.Data
{
	public class CustomError
	{
		[DataMemberAttribute(Name = "ErrNo")]
		public long? ErrNo { get; set; }

		[DataMemberAttribute(Name = "ErrMsg")]
		public string ErrMsg { get; set; }

	}
}
