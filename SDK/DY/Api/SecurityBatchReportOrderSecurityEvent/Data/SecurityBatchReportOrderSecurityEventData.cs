using System.Runtime.Serialization;

namespace Dop.Api.SecurityBatchReportOrderSecurityEvent.Data
{
	public class SecurityBatchReportOrderSecurityEventData
	{
		[DataMemberAttribute(Name = "CustomError")]
		public CustomError CustomError { get; set; }

	}
}
