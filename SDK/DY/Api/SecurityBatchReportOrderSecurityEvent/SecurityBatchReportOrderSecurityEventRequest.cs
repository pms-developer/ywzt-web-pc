using System;
using Dop.Core;
using Dop.Api.SecurityBatchReportOrderSecurityEvent.Param;

namespace Dop.Api.SecurityBatchReportOrderSecurityEvent
{
	public class SecurityBatchReportOrderSecurityEventRequest : DoudianOpApiRequest<SecurityBatchReportOrderSecurityEventParam>
	{
		public override string GetUrlPath()
		{
			return "/security/batchReportOrderSecurityEvent";
		}

		public override Type GetResponseType()
		{
			return typeof(SecurityBatchReportOrderSecurityEventResponse);
		}

		public  SecurityBatchReportOrderSecurityEventParam BuildParam()
		{
			return Param ?? (Param = new SecurityBatchReportOrderSecurityEventParam());
		}

	}
}
