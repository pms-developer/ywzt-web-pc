using Dop.Core;
using Dop.Api.SecurityBatchReportOrderSecurityEvent.Data;

namespace Dop.Api.SecurityBatchReportOrderSecurityEvent
{
	public class SecurityBatchReportOrderSecurityEventResponse : DoudianOpApiResponse<SecurityBatchReportOrderSecurityEventData>
	{
	}
}
