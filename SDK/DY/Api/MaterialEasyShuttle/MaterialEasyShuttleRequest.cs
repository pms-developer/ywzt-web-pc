using System;
using Dop.Core;
using Dop.Api.MaterialEasyShuttle.Param;

namespace Dop.Api.MaterialEasyShuttle
{
	public class MaterialEasyShuttleRequest : DoudianOpApiRequest<MaterialEasyShuttleParam>
	{
		public override string GetUrlPath()
		{
			return "/material/easyShuttle";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialEasyShuttleResponse);
		}

		public  MaterialEasyShuttleParam BuildParam()
		{
			return Param ?? (Param = new MaterialEasyShuttleParam());
		}

	}
}
