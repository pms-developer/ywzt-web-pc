using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialEasyShuttle.Param
{
	public class MaterialEasyShuttleParam
	{
		[DataMemberAttribute(Name = "folder_id_list")]
		public List<string> FolderIdList { get; set; }

		[DataMemberAttribute(Name = "operate_type")]
		public string OperateType { get; set; }

		[DataMemberAttribute(Name = "material_type_list")]
		public List<string> MaterialTypeList { get; set; }

		[DataMemberAttribute(Name = "only_material")]
		public bool? OnlyMaterial { get; set; }

		[DataMemberAttribute(Name = "create_time_end")]
		public string CreateTimeEnd { get; set; }

	}
}
