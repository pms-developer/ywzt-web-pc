using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateRevoke.Param
{
	public class SmsTemplateRevokeParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "sms_template_apply_id")]
		public string SmsTemplateApplyId { get; set; }

	}
}
