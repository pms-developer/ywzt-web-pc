using System;
using Dop.Core;
using Dop.Api.SmsTemplateRevoke.Param;

namespace Dop.Api.SmsTemplateRevoke
{
	public class SmsTemplateRevokeRequest : DoudianOpApiRequest<SmsTemplateRevokeParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/template/revoke";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsTemplateRevokeResponse);
		}

		public  SmsTemplateRevokeParam BuildParam()
		{
			return Param ?? (Param = new SmsTemplateRevokeParam());
		}

	}
}
