using Dop.Core;
using Dop.Api.SmsTemplateRevoke.Data;

namespace Dop.Api.SmsTemplateRevoke
{
	public class SmsTemplateRevokeResponse : DoudianOpApiResponse<SmsTemplateRevokeData>
	{
	}
}
