using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateRevoke.Data
{
	public class SmsTemplateRevokeData
	{
		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

	}
}
