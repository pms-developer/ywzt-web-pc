using System;
using Dop.Core;
using Dop.Api.OrderDownloadToShop.Param;

namespace Dop.Api.OrderDownloadToShop
{
	public class OrderDownloadToShopRequest : DoudianOpApiRequest<OrderDownloadToShopParam>
	{
		public override string GetUrlPath()
		{
			return "/order/downloadToShop";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderDownloadToShopResponse);
		}

		public  OrderDownloadToShopParam BuildParam()
		{
			return Param ?? (Param = new OrderDownloadToShopParam());
		}

	}
}
