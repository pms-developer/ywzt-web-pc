using System.Runtime.Serialization;

namespace Dop.Api.OrderDownloadToShop.Param
{
	public class OrderDownloadToShopParam
	{
		[DataMemberAttribute(Name = "download_id")]
		public string DownloadId { get; set; }

	}
}
