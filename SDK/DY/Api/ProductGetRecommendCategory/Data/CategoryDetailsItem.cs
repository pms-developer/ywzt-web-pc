using System.Runtime.Serialization;

namespace Dop.Api.ProductGetRecommendCategory.Data
{
	public class CategoryDetailsItem
	{
		[DataMemberAttribute(Name = "category_detail")]
		public CategoryDetail CategoryDetail { get; set; }

		[DataMemberAttribute(Name = "qualification_status")]
		public long? QualificationStatus { get; set; }

	}
}
