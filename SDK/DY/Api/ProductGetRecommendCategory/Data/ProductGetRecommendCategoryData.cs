using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductGetRecommendCategory.Data
{
	public class ProductGetRecommendCategoryData
	{
		[DataMemberAttribute(Name = "categoryDetails")]
		public List<CategoryDetailsItem> CategoryDetails { get; set; }

	}
}
