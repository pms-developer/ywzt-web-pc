using System.Runtime.Serialization;

namespace Dop.Api.ProductGetRecommendCategory.Data
{
	public class CategoryDetail
	{
		[DataMemberAttribute(Name = "first_cid")]
		public long? FirstCid { get; set; }

		[DataMemberAttribute(Name = "first_cname")]
		public string FirstCname { get; set; }

		[DataMemberAttribute(Name = "second_cid")]
		public long? SecondCid { get; set; }

		[DataMemberAttribute(Name = "second_cname")]
		public string SecondCname { get; set; }

		[DataMemberAttribute(Name = "third_cid")]
		public long? ThirdCid { get; set; }

		[DataMemberAttribute(Name = "third_cname")]
		public string ThirdCname { get; set; }

		[DataMemberAttribute(Name = "fourth_cid")]
		public long? FourthCid { get; set; }

		[DataMemberAttribute(Name = "fourth_cname")]
		public string FourthCname { get; set; }

	}
}
