using System;
using Dop.Core;
using Dop.Api.ProductGetRecommendCategory.Param;

namespace Dop.Api.ProductGetRecommendCategory
{
	public class ProductGetRecommendCategoryRequest : DoudianOpApiRequest<ProductGetRecommendCategoryParam>
	{
		public override string GetUrlPath()
		{
			return "/product/GetRecommendCategory";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductGetRecommendCategoryResponse);
		}

		public  ProductGetRecommendCategoryParam BuildParam()
		{
			return Param ?? (Param = new ProductGetRecommendCategoryParam());
		}

	}
}
