using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductGetRecommendCategory.Param
{
	public class ProductGetRecommendCategoryParam
	{
		[DataMemberAttribute(Name = "scene")]
		public string Scene { get; set; }

		[DataMemberAttribute(Name = "pic")]
		public List<PicItem> Pic { get; set; }

		[DataMemberAttribute(Name = "category_leaf_id")]
		public long? CategoryLeafId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "product_format_new")]
		public Dictionary<long,ProductFormatNewItem> ProductFormatNew { get; set; }

		[DataMemberAttribute(Name = "standard_brand_id")]
		public long? StandardBrandId { get; set; }

	}
}
