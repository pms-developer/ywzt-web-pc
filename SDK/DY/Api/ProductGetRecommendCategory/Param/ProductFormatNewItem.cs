using System.Runtime.Serialization;

namespace Dop.Api.ProductGetRecommendCategory.Param
{
	public class ProductFormatNewItem
	{
		[DataMemberAttribute(Name = "value")]
		public long? Value { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

	}
}
