using System.Runtime.Serialization;

namespace Dop.Api.ProductGetRecommendCategory.Param
{
	public class PicItem
	{
		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
