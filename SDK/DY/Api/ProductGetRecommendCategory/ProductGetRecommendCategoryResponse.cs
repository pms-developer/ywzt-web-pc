using Dop.Core;
using Dop.Api.ProductGetRecommendCategory.Data;

namespace Dop.Api.ProductGetRecommendCategory
{
	public class ProductGetRecommendCategoryResponse : DoudianOpApiResponse<ProductGetRecommendCategoryData>
	{
	}
}
