using Dop.Core;
using Dop.Api.OrderBatchDecrypt.Data;

namespace Dop.Api.OrderBatchDecrypt
{
	public class OrderBatchDecryptResponse : DoudianOpApiResponse<OrderBatchDecryptData>
	{
	}
}
