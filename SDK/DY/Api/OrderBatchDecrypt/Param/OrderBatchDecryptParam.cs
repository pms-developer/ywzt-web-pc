using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchDecrypt.Param
{
	public class OrderBatchDecryptParam
	{
		[DataMemberAttribute(Name = "cipher_infos")]
		public List<CipherInfosItem> CipherInfos { get; set; }

		[DataMemberAttribute(Name = "account_id")]
		public string AccountId { get; set; }

		[DataMemberAttribute(Name = "account_type")]
		public string AccountType { get; set; }

	}
}
