using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchDecrypt.Data
{
	public class OrderBatchDecryptData
	{
		[DataMemberAttribute(Name = "decrypt_infos")]
		public List<DecryptInfosItem> DecryptInfos { get; set; }

		[DataMemberAttribute(Name = "custom_err")]
		public CustomErr CustomErr { get; set; }

	}
}
