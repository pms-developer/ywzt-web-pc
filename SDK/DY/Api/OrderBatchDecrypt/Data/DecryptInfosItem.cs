using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchDecrypt.Data
{
	public class DecryptInfosItem
	{
		[DataMemberAttribute(Name = "auth_id")]
		public string AuthId { get; set; }

		[DataMemberAttribute(Name = "cipher_text")]
		public string CipherText { get; set; }

		[DataMemberAttribute(Name = "decrypt_text")]
		public string DecryptText { get; set; }

		[DataMemberAttribute(Name = "err_no")]
		public long? ErrNo { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

		[DataMemberAttribute(Name = "is_virtual_tel")]
		public bool? IsVirtualTel { get; set; }

		[DataMemberAttribute(Name = "expire_time")]
		public long? ExpireTime { get; set; }

		[DataMemberAttribute(Name = "phone_no_a")]
		public string PhoneNoA { get; set; }

		[DataMemberAttribute(Name = "phone_no_b")]
		public string PhoneNoB { get; set; }

	}
}
