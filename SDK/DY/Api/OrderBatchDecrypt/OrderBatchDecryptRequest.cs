using System;
using Dop.Core;
using Dop.Api.OrderBatchDecrypt.Param;

namespace Dop.Api.OrderBatchDecrypt
{
	public class OrderBatchDecryptRequest : DoudianOpApiRequest<OrderBatchDecryptParam>
	{
		public override string GetUrlPath()
		{
			return "/order/batchDecrypt";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderBatchDecryptResponse);
		}

		public  OrderBatchDecryptParam BuildParam()
		{
			return Param ?? (Param = new OrderBatchDecryptParam());
		}

	}
}
