using System;
using Dop.Core;
using Dop.Api.AfterSaleCancelSendGoodsSuccess.Param;

namespace Dop.Api.AfterSaleCancelSendGoodsSuccess
{
	public class AfterSaleCancelSendGoodsSuccessRequest : DoudianOpApiRequest<AfterSaleCancelSendGoodsSuccessParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/CancelSendGoodsSuccess";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleCancelSendGoodsSuccessResponse);
		}

		public  AfterSaleCancelSendGoodsSuccessParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleCancelSendGoodsSuccessParam());
		}

	}
}
