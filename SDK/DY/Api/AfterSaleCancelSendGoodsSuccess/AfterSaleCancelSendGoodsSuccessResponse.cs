using Dop.Core;
using Dop.Api.AfterSaleCancelSendGoodsSuccess.Data;

namespace Dop.Api.AfterSaleCancelSendGoodsSuccess
{
	public class AfterSaleCancelSendGoodsSuccessResponse : DoudianOpApiResponse<AfterSaleCancelSendGoodsSuccessData>
	{
	}
}
