using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleCancelSendGoodsSuccess.Param
{
	public class AfterSaleCancelSendGoodsSuccessParam
	{
		[DataMemberAttribute(Name = "aftersale_id")]
		public string AftersaleId { get; set; }

		[DataMemberAttribute(Name = "op_time")]
		public long? OpTime { get; set; }

	}
}
