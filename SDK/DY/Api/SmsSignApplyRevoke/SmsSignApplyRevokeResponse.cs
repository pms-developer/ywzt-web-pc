using Dop.Core;
using Dop.Api.SmsSignApplyRevoke.Data;

namespace Dop.Api.SmsSignApplyRevoke
{
	public class SmsSignApplyRevokeResponse : DoudianOpApiResponse<SmsSignApplyRevokeData>
	{
	}
}
