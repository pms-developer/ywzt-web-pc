using System;
using Dop.Core;
using Dop.Api.SmsSignApplyRevoke.Param;

namespace Dop.Api.SmsSignApplyRevoke
{
	public class SmsSignApplyRevokeRequest : DoudianOpApiRequest<SmsSignApplyRevokeParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/sign/apply/revoke";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsSignApplyRevokeResponse);
		}

		public  SmsSignApplyRevokeParam BuildParam()
		{
			return Param ?? (Param = new SmsSignApplyRevokeParam());
		}

	}
}
