using System.Runtime.Serialization;

namespace Dop.Api.SmsSignApplyRevoke.Data
{
	public class SmsSignApplyRevokeData
	{
		[DataMemberAttribute(Name = "code")]
		public long? Code { get; set; }

		[DataMemberAttribute(Name = "message")]
		public string Message { get; set; }

	}
}
