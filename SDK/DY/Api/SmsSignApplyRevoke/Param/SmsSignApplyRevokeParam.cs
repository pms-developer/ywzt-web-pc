using System.Runtime.Serialization;

namespace Dop.Api.SmsSignApplyRevoke.Param
{
	public class SmsSignApplyRevokeParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "sms_sign_apply_id")]
		public string SmsSignApplyId { get; set; }

	}
}
