using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchSensitive.Data
{
	public class CustomErr
	{
		[DataMemberAttribute(Name = "err_code")]
		public long? ErrCode { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

	}
}
