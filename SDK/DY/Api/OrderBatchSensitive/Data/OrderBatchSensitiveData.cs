using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchSensitive.Data
{
	public class OrderBatchSensitiveData
	{
		[DataMemberAttribute(Name = "decrypt_infos")]
		public List<DecryptInfosItem> DecryptInfos { get; set; }

		[DataMemberAttribute(Name = "custom_err")]
		public CustomErr CustomErr { get; set; }

	}
}
