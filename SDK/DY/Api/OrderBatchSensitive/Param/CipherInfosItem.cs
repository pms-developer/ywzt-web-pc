using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchSensitive.Param
{
	public class CipherInfosItem
	{
		[DataMemberAttribute(Name = "auth_id")]
		public string AuthId { get; set; }

		[DataMemberAttribute(Name = "cipher_text")]
		public string CipherText { get; set; }

	}
}
