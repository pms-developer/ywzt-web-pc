using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchSensitive.Param
{
	public class OrderBatchSensitiveParam
	{
		[DataMemberAttribute(Name = "cipher_infos")]
		public List<CipherInfosItem> CipherInfos { get; set; }

	}
}
