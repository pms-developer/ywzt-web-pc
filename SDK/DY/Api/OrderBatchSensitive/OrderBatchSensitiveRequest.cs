using System;
using Dop.Core;
using Dop.Api.OrderBatchSensitive.Param;

namespace Dop.Api.OrderBatchSensitive
{
	public class OrderBatchSensitiveRequest : DoudianOpApiRequest<OrderBatchSensitiveParam>
	{
		public override string GetUrlPath()
		{
			return "/order/batchSensitive";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderBatchSensitiveResponse);
		}

		public  OrderBatchSensitiveParam BuildParam()
		{
			return Param ?? (Param = new OrderBatchSensitiveParam());
		}

	}
}
