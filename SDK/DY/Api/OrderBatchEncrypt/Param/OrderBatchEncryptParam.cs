using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchEncrypt.Param
{
	public class OrderBatchEncryptParam
	{
		[DataMemberAttribute(Name = "batch_encrypt_list")]
		public List<BatchEncryptListItem> BatchEncryptList { get; set; }

		[DataMemberAttribute(Name = "sensitive_scene")]
		public string SensitiveScene { get; set; }

	}
}
