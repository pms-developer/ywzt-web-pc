using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchEncrypt.Param
{
	public class BatchEncryptListItem
	{
		[DataMemberAttribute(Name = "plain_text")]
		public string PlainText { get; set; }

		[DataMemberAttribute(Name = "auth_id")]
		public string AuthId { get; set; }

		[DataMemberAttribute(Name = "is_support_index")]
		public bool? IsSupportIndex { get; set; }

		[DataMemberAttribute(Name = "sensitive_type")]
		public int? SensitiveType { get; set; }

	}
}
