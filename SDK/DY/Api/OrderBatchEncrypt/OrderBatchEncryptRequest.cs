using System;
using Dop.Core;
using Dop.Api.OrderBatchEncrypt.Param;

namespace Dop.Api.OrderBatchEncrypt
{
	public class OrderBatchEncryptRequest : DoudianOpApiRequest<OrderBatchEncryptParam>
	{
		public override string GetUrlPath()
		{
			return "/order/batchEncrypt";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderBatchEncryptResponse);
		}

		public  OrderBatchEncryptParam BuildParam()
		{
			return Param ?? (Param = new OrderBatchEncryptParam());
		}

	}
}
