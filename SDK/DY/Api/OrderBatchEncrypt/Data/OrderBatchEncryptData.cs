using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchEncrypt.Data
{
	public class OrderBatchEncryptData
	{
		[DataMemberAttribute(Name = "encrypt_infos")]
		public List<EncryptInfosItem> EncryptInfos { get; set; }

		[DataMemberAttribute(Name = "custom_err")]
		public CustomErr CustomErr { get; set; }

	}
}
