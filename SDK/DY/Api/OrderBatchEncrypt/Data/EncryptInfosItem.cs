using System.Runtime.Serialization;

namespace Dop.Api.OrderBatchEncrypt.Data
{
	public class EncryptInfosItem
	{
		[DataMemberAttribute(Name = "auth_id")]
		public string AuthId { get; set; }

		[DataMemberAttribute(Name = "cipher_text")]
		public string CipherText { get; set; }

		[DataMemberAttribute(Name = "decrypt_text")]
		public string DecryptText { get; set; }

		[DataMemberAttribute(Name = "err_no")]
		public long? ErrNo { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

	}
}
