using System;
using Dop.Core;
using Dop.Api.SmsSignApplyList.Param;

namespace Dop.Api.SmsSignApplyList
{
	public class SmsSignApplyListRequest : DoudianOpApiRequest<SmsSignApplyListParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/sign/apply/list";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsSignApplyListResponse);
		}

		public  SmsSignApplyListParam BuildParam()
		{
			return Param ?? (Param = new SmsSignApplyListParam());
		}

	}
}
