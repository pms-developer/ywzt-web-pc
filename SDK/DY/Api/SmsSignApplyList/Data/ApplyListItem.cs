using System.Runtime.Serialization;

namespace Dop.Api.SmsSignApplyList.Data
{
	public class ApplyListItem
	{
		[DataMemberAttribute(Name = "sms_sign_apply_id")]
		public string SmsSignApplyId { get; set; }

		[DataMemberAttribute(Name = "sign")]
		public string Sign { get; set; }

		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "status")]
		public long? Status { get; set; }

		[DataMemberAttribute(Name = "status_remark")]
		public string StatusRemark { get; set; }

	}
}
