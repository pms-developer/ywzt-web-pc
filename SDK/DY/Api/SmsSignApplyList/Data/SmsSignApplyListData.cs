using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SmsSignApplyList.Data
{
	public class SmsSignApplyListData
	{
		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "apply_list")]
		public List<ApplyListItem> ApplyList { get; set; }

	}
}
