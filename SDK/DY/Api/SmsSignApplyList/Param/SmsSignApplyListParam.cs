using System.Runtime.Serialization;

namespace Dop.Api.SmsSignApplyList.Param
{
	public class SmsSignApplyListParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "like")]
		public string Like { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "sms_sign_apply_id")]
		public string SmsSignApplyId { get; set; }

	}
}
