using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsCompanyList.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

	}
}
