using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsCompanyList.Data
{
	public class OrderLogisticsCompanyListData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
