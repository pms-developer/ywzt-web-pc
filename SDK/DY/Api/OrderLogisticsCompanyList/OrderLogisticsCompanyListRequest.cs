using System;
using Dop.Core;
using Dop.Api.OrderLogisticsCompanyList.Param;

namespace Dop.Api.OrderLogisticsCompanyList
{
	public class OrderLogisticsCompanyListRequest : DoudianOpApiRequest<OrderLogisticsCompanyListParam>
	{
		public override string GetUrlPath()
		{
			return "/order/logisticsCompanyList";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderLogisticsCompanyListResponse);
		}

		public  OrderLogisticsCompanyListParam BuildParam()
		{
			return Param ?? (Param = new OrderLogisticsCompanyListParam());
		}

	}
}
