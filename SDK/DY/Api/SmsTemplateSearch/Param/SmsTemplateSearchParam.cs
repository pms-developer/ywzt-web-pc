using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateSearch.Param
{
	public class SmsTemplateSearchParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "template_id")]
		public string TemplateId { get; set; }

		[DataMemberAttribute(Name = "template_content")]
		public string TemplateContent { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

	}
}
