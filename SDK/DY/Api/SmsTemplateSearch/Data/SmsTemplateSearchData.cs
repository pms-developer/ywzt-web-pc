using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateSearch.Data
{
	public class SmsTemplateSearchData
	{
		[DataMemberAttribute(Name = "template_search_list")]
		public List<TemplateSearchListItem> TemplateSearchList { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
