using System.Runtime.Serialization;

namespace Dop.Api.SmsTemplateSearch.Data
{
	public class TemplateSearchListItem
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "template_content")]
		public string TemplateContent { get; set; }

		[DataMemberAttribute(Name = "template_id")]
		public string TemplateId { get; set; }

		[DataMemberAttribute(Name = "channel_type")]
		public string ChannelType { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

	}
}
