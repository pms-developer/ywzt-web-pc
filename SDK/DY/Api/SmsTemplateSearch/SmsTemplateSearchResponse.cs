using Dop.Core;
using Dop.Api.SmsTemplateSearch.Data;

namespace Dop.Api.SmsTemplateSearch
{
	public class SmsTemplateSearchResponse : DoudianOpApiResponse<SmsTemplateSearchData>
	{
	}
}
