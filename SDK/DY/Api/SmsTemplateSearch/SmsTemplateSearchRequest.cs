using System;
using Dop.Core;
using Dop.Api.SmsTemplateSearch.Param;

namespace Dop.Api.SmsTemplateSearch
{
	public class SmsTemplateSearchRequest : DoudianOpApiRequest<SmsTemplateSearchParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/template/search";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsTemplateSearchResponse);
		}

		public  SmsTemplateSearchParam BuildParam()
		{
			return Param ?? (Param = new SmsTemplateSearchParam());
		}

	}
}
