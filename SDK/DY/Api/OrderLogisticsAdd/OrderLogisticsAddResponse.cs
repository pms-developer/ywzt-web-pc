using Dop.Core;
using Dop.Api.OrderLogisticsAdd.Data;

namespace Dop.Api.OrderLogisticsAdd
{
	public class OrderLogisticsAddResponse : DoudianOpApiResponse<OrderLogisticsAddData>
	{
	}
}
