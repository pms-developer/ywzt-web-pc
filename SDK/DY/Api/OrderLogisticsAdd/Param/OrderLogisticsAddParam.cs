using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderLogisticsAdd.Param
{
	public class OrderLogisticsAddParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "logistics_id")]
		public long? LogisticsId { get; set; }

		[DataMemberAttribute(Name = "company")]
		public string Company { get; set; }

		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "is_refund_reject")]
		public bool? IsRefundReject { get; set; }

		[DataMemberAttribute(Name = "is_reject_refund")]
		public bool? IsRejectRefund { get; set; }

		[DataMemberAttribute(Name = "serial_number_list")]
		public List<string> SerialNumberList { get; set; }

		[DataMemberAttribute(Name = "address_id")]
		public long? AddressId { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

	}
}
