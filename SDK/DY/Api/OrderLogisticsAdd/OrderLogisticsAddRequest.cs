using System;
using Dop.Core;
using Dop.Api.OrderLogisticsAdd.Param;

namespace Dop.Api.OrderLogisticsAdd
{
	public class OrderLogisticsAddRequest : DoudianOpApiRequest<OrderLogisticsAddParam>
	{
		public override string GetUrlPath()
		{
			return "/order/logisticsAdd";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderLogisticsAddResponse);
		}

		public  OrderLogisticsAddParam BuildParam()
		{
			return Param ?? (Param = new OrderLogisticsAddParam());
		}

	}
}
