using System;
using Dop.Core;
using Dop.Api.MaterialUploadVideoAsync.Param;

namespace Dop.Api.MaterialUploadVideoAsync
{
	public class MaterialUploadVideoAsyncRequest : DoudianOpApiRequest<MaterialUploadVideoAsyncParam>
	{
		public override string GetUrlPath()
		{
			return "/material/uploadVideoAsync";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialUploadVideoAsyncResponse);
		}

		public  MaterialUploadVideoAsyncParam BuildParam()
		{
			return Param ?? (Param = new MaterialUploadVideoAsyncParam());
		}

	}
}
