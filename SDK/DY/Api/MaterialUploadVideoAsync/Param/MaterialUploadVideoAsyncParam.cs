using System.Runtime.Serialization;

namespace Dop.Api.MaterialUploadVideoAsync.Param
{
	public class MaterialUploadVideoAsyncParam
	{
		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

	}
}
