using Dop.Core;
using Dop.Api.MaterialUploadVideoAsync.Data;

namespace Dop.Api.MaterialUploadVideoAsync
{
	public class MaterialUploadVideoAsyncResponse : DoudianOpApiResponse<MaterialUploadVideoAsyncData>
	{
	}
}
