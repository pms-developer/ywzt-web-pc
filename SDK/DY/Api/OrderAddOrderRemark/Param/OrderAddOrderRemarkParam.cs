using System.Runtime.Serialization;

namespace Dop.Api.OrderAddOrderRemark.Param
{
	public class OrderAddOrderRemarkParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "is_add_star")]
		public string IsAddStar { get; set; }

		[DataMemberAttribute(Name = "star")]
		public string Star { get; set; }

	}
}
