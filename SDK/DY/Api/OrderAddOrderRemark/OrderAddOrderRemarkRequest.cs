using System;
using Dop.Core;
using Dop.Api.OrderAddOrderRemark.Param;

namespace Dop.Api.OrderAddOrderRemark
{
	public class OrderAddOrderRemarkRequest : DoudianOpApiRequest<OrderAddOrderRemarkParam>
	{
		public override string GetUrlPath()
		{
			return "/order/addOrderRemark";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderAddOrderRemarkResponse);
		}

		public  OrderAddOrderRemarkParam BuildParam()
		{
			return Param ?? (Param = new OrderAddOrderRemarkParam());
		}

	}
}
