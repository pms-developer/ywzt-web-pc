using System;
using Dop.Core;
using Dop.Api.MaterialRecoverFolder.Param;

namespace Dop.Api.MaterialRecoverFolder
{
	public class MaterialRecoverFolderRequest : DoudianOpApiRequest<MaterialRecoverFolderParam>
	{
		public override string GetUrlPath()
		{
			return "/material/recoverFolder";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialRecoverFolderResponse);
		}

		public  MaterialRecoverFolderParam BuildParam()
		{
			return Param ?? (Param = new MaterialRecoverFolderParam());
		}

	}
}
