using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialRecoverFolder.Data
{
	public class MaterialRecoverFolderData
	{
		[DataMemberAttribute(Name = "success_ids")]
		public List<string> SuccessIds { get; set; }

		[DataMemberAttribute(Name = "failed_map")]
		public Dictionary<long,FailedMapItem> FailedMap { get; set; }

	}
}
