using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialRecoverFolder.Param
{
	public class MaterialRecoverFolderParam
	{
		[DataMemberAttribute(Name = "folder_ids")]
		public List<string> FolderIds { get; set; }

	}
}
