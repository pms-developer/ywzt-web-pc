using System;
using Dop.Core;
using Dop.Api.OrderInsurance.Param;

namespace Dop.Api.OrderInsurance
{
	public class OrderInsuranceRequest : DoudianOpApiRequest<OrderInsuranceParam>
	{
		public override string GetUrlPath()
		{
			return "/order/insurance";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderInsuranceResponse);
		}

		public  OrderInsuranceParam BuildParam()
		{
			return Param ?? (Param = new OrderInsuranceParam());
		}

	}
}
