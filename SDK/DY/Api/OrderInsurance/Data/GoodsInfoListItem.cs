using System.Runtime.Serialization;

namespace Dop.Api.OrderInsurance.Data
{
	public class GoodsInfoListItem
	{
		[DataMemberAttribute(Name = "product_name")]
		public string ProductName { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public string CategoryId { get; set; }

		[DataMemberAttribute(Name = "show_page_url")]
		public string ShowPageUrl { get; set; }

		[DataMemberAttribute(Name = "amount")]
		public int? Amount { get; set; }

	}
}
