using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.OrderInsurance.Data
{
	public class PolicyInfoListItem
	{
		[DataMemberAttribute(Name = "ins_policy_no")]
		public string InsPolicyNo { get; set; }

		[DataMemberAttribute(Name = "approximate_return_fee")]
		public long? ApproximateReturnFee { get; set; }

		[DataMemberAttribute(Name = "total_premium")]
		public long? TotalPremium { get; set; }

		[DataMemberAttribute(Name = "user_premium")]
		public long? UserPremium { get; set; }

		[DataMemberAttribute(Name = "merchant_premium")]
		public long? MerchantPremium { get; set; }

		[DataMemberAttribute(Name = "platform_premium")]
		public long? PlatformPremium { get; set; }

		[DataMemberAttribute(Name = "status")]
		public int? Status { get; set; }

		[DataMemberAttribute(Name = "appeal_status")]
		public int? AppealStatus { get; set; }

		[DataMemberAttribute(Name = "claim_status")]
		public int? ClaimStatus { get; set; }

		[DataMemberAttribute(Name = "company_name")]
		public string CompanyName { get; set; }

		[DataMemberAttribute(Name = "is_allow_appeal")]
		public bool? IsAllowAppeal { get; set; }

		[DataMemberAttribute(Name = "goods_info_list")]
		public List<GoodsInfoListItem> GoodsInfoList { get; set; }

		[DataMemberAttribute(Name = "fail_reason")]
		public string FailReason { get; set; }

		[DataMemberAttribute(Name = "insurance_hotline")]
		public string InsuranceHotline { get; set; }

		[DataMemberAttribute(Name = "premium_status")]
		public int? PremiumStatus { get; set; }

		[DataMemberAttribute(Name = "ins_ensured_time_begin")]
		public string InsEnsuredTimeBegin { get; set; }

		[DataMemberAttribute(Name = "ins_ensured_time_end")]
		public string InsEnsuredTimeEnd { get; set; }

	}
}
