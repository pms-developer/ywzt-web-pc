using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.OrderInsurance.Data
{
	public class OrderInsuranceData
	{
		[DataMemberAttribute(Name = "policy_info_list")]
		public List<PolicyInfoListItem> PolicyInfoList { get; set; }

	}
}
