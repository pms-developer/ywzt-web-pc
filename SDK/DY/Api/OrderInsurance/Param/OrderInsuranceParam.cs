using System.Runtime.Serialization;

namespace Dop.Api.OrderInsurance.Param
{
	public class OrderInsuranceParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

	}
}
