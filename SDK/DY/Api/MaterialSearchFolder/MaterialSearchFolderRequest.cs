using System;
using Dop.Core;
using Dop.Api.MaterialSearchFolder.Param;

namespace Dop.Api.MaterialSearchFolder
{
	public class MaterialSearchFolderRequest : DoudianOpApiRequest<MaterialSearchFolderParam>
	{
		public override string GetUrlPath()
		{
			return "/material/searchFolder";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialSearchFolderResponse);
		}

		public  MaterialSearchFolderParam BuildParam()
		{
			return Param ?? (Param = new MaterialSearchFolderParam());
		}

	}
}
