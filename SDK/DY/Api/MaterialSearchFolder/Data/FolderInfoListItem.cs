using System.Runtime.Serialization;

namespace Dop.Api.MaterialSearchFolder.Data
{
	public class FolderInfoListItem
	{
		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "folder_type")]
		public int? FolderType { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "operate_status")]
		public int? OperateStatus { get; set; }

		[DataMemberAttribute(Name = "parent_folder_id")]
		public string ParentFolderId { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public string UpdateTime { get; set; }

		[DataMemberAttribute(Name = "delete_time")]
		public string DeleteTime { get; set; }

	}
}
