using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialSearchFolder.Data
{
	public class MaterialSearchFolderData
	{
		[DataMemberAttribute(Name = "folder_info_list")]
		public List<FolderInfoListItem> FolderInfoList { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
