using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.MaterialSearchFolder.Param
{
	public class MaterialSearchFolderParam
	{
		[DataMemberAttribute(Name = "order_by")]
		public int? OrderBy { get; set; }

		[DataMemberAttribute(Name = "page_num")]
		public int? PageNum { get; set; }

		[DataMemberAttribute(Name = "page_size")]
		public int? PageSize { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "create_time_start")]
		public string CreateTimeStart { get; set; }

		[DataMemberAttribute(Name = "create_time_end")]
		public string CreateTimeEnd { get; set; }

		[DataMemberAttribute(Name = "parent_folder_id")]
		public string ParentFolderId { get; set; }

		[DataMemberAttribute(Name = "operate_status")]
		public List<int> OperateStatus { get; set; }

	}
}
