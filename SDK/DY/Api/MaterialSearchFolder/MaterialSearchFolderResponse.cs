using Dop.Core;
using Dop.Api.MaterialSearchFolder.Data;

namespace Dop.Api.MaterialSearchFolder
{
	public class MaterialSearchFolderResponse : DoudianOpApiResponse<MaterialSearchFolderData>
	{
	}
}
