using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleTimeExtend.Param
{
	public class AfterSaleTimeExtendParam
	{
		[DataMemberAttribute(Name = "aftersale_id")]
		public long? AftersaleId { get; set; }

	}
}
