using System;
using Dop.Core;
using Dop.Api.AfterSaleTimeExtend.Param;

namespace Dop.Api.AfterSaleTimeExtend
{
	public class AfterSaleTimeExtendRequest : DoudianOpApiRequest<AfterSaleTimeExtendParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/timeExtend";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleTimeExtendResponse);
		}

		public  AfterSaleTimeExtendParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleTimeExtendParam());
		}

	}
}
