using System;
using Dop.Core;
using Dop.Api.SpuGetKeyPropertyByCid.Param;

namespace Dop.Api.SpuGetKeyPropertyByCid
{
	public class SpuGetKeyPropertyByCidRequest : DoudianOpApiRequest<SpuGetKeyPropertyByCidParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/getKeyPropertyByCid";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuGetKeyPropertyByCidResponse);
		}

		public  SpuGetKeyPropertyByCidParam BuildParam()
		{
			return Param ?? (Param = new SpuGetKeyPropertyByCidParam());
		}

	}
}
