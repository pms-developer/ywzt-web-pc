using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SpuGetKeyPropertyByCid.Data
{
	public class SpuGetKeyPropertyByCidData
	{
		[DataMemberAttribute(Name = "property_info")]
		public List<PropertyInfoItem> PropertyInfo { get; set; }

		[DataMemberAttribute(Name = "brand_info")]
		public List<BrandInfoItem> BrandInfo { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
