using System.Runtime.Serialization;

namespace Dop.Api.SpuGetKeyPropertyByCid.Data
{
	public class PropertyInfoItem
	{
		[DataMemberAttribute(Name = "rel_value_id")]
		public long? RelValueId { get; set; }

		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "property_name")]
		public string PropertyName { get; set; }

		[DataMemberAttribute(Name = "value_id")]
		public long? ValueId { get; set; }

		[DataMemberAttribute(Name = "value_name")]
		public string ValueName { get; set; }

	}
}
