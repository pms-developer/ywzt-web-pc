using System.Runtime.Serialization;

namespace Dop.Api.SpuGetKeyPropertyByCid.Param
{
	public class SpuGetKeyPropertyByCidParam
	{
		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

	}
}
