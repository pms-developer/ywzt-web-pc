﻿using System;
using Dop.Api.Token.Param;
using Dop.Core;
namespace Dop.Api.Token
{
    public class AccessTokenRequest : DoudianOpApiRequest<AccessTokenParam>
    {

        public override string GetUrlPath()
        {
            return "token/create";
        }

        public override Type GetResponseType()
        {
            return typeof(AccessTokenResponse);
        }

        public AccessTokenParam BuildParam()
        {
            return Param ?? (Param = new AccessTokenParam());
        }
    }
}
