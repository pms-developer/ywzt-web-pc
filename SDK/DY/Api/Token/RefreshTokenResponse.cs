﻿using Dop.Api.Token.Data;
using Dop.Core;

namespace Dop.Api.Token
{
    public class RefreshTokenResponse : DoudianOpApiResponse<RefreshTokenData>
    {
        public RefreshTokenResponse()
        {
        }
    }
}
