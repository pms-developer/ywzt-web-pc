﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.Token.Param
{
    public class RefreshTokenParam
    {

        [DataMemberAttribute(Name = "grant_type")]
        public string GrantType { get; set; }

        [DataMemberAttribute(Name = "refresh_token")]
        public string RefreshToken { get; set; }
    }
}
