﻿using System.Runtime.Serialization;

namespace Dop.Api.Token.Param
{
    public class AccessTokenParam
    {
        [DataMemberAttribute(Name = "code")]
        public string Code { get; set; }

        [DataMemberAttribute(Name = "grant_type")]
        public string GrantType { get; set; }

        [DataMemberAttribute(Name = "shop_id")]
        public long? ShopId { get; set; }
    }
}
