﻿using System;
using Dop.Api.Token.Param;
using Dop.Core;

namespace Dop.Api.Token
{
    public class RefreshTokenRequest : DoudianOpApiRequest<RefreshTokenParam>
    {
        public override string GetUrlPath()
        {
            return "token/create";
        }

        public override Type GetResponseType()
        {
            return typeof(AccessTokenResponse);
        }

        public RefreshTokenParam BuildParam()
        {
            return Param ?? (Param = new RefreshTokenParam());
        }
    }
}
