using System.Runtime.Serialization;

namespace Dop.Api.MaterialUploadImageSync.Param
{
	public class MaterialUploadImageSyncParam
	{
		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

		[DataMemberAttribute(Name = "material_name")]
		public string MaterialName { get; set; }

		[DataMemberAttribute(Name = "need_distinct")]
		public bool? NeedDistinct { get; set; }

		[DataMemberAttribute(Name = "request_id")]
		public string RequestId { get; set; }

	}
}
