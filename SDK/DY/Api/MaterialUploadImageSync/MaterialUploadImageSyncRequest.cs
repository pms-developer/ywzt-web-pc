using System;
using Dop.Core;
using Dop.Api.MaterialUploadImageSync.Param;

namespace Dop.Api.MaterialUploadImageSync
{
	public class MaterialUploadImageSyncRequest : DoudianOpApiRequest<MaterialUploadImageSyncParam>
	{
		public override string GetUrlPath()
		{
			return "/material/uploadImageSync";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialUploadImageSyncResponse);
		}

		public  MaterialUploadImageSyncParam BuildParam()
		{
			return Param ?? (Param = new MaterialUploadImageSyncParam());
		}

	}
}
