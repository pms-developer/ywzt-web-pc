using System.Runtime.Serialization;

namespace Dop.Api.MaterialUploadImageSync.Data
{
	public class MaterialUploadImageSyncData
	{
		[DataMemberAttribute(Name = "material_id")]
		public string MaterialId { get; set; }

		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "is_new")]
		public bool? IsNew { get; set; }

		[DataMemberAttribute(Name = "audit_status")]
		public int? AuditStatus { get; set; }

		[DataMemberAttribute(Name = "byte_url")]
		public string ByteUrl { get; set; }

	}
}
