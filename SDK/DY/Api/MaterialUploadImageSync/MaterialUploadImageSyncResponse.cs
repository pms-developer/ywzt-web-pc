using Dop.Core;
using Dop.Api.MaterialUploadImageSync.Data;

namespace Dop.Api.MaterialUploadImageSync
{
	public class MaterialUploadImageSyncResponse : DoudianOpApiResponse<MaterialUploadImageSyncData>
	{
	}
}
