using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCancelOrder.Data
{
	public class LogisticsCancelOrderData
	{
		[DataMemberAttribute(Name = "cancel_result")]
		public CancelResult CancelResult { get; set; }

	}
}
