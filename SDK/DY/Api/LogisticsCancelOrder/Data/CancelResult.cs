using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCancelOrder.Data
{
	public class CancelResult
	{
		[DataMemberAttribute(Name = "success")]
		public bool? Success { get; set; }

	}
}
