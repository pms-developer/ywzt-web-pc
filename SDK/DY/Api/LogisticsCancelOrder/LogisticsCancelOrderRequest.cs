using System;
using Dop.Core;
using Dop.Api.LogisticsCancelOrder.Param;

namespace Dop.Api.LogisticsCancelOrder
{
	public class LogisticsCancelOrderRequest : DoudianOpApiRequest<LogisticsCancelOrderParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/cancelOrder";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsCancelOrderResponse);
		}

		public  LogisticsCancelOrderParam BuildParam()
		{
			return Param ?? (Param = new LogisticsCancelOrderParam());
		}

	}
}
