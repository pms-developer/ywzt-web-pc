using System.Runtime.Serialization;

namespace Dop.Api.LogisticsCancelOrder.Param
{
	public class LogisticsCancelOrderParam
	{
		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "user_id")]
		public long? UserId { get; set; }

	}
}
