using Dop.Core;
using Dop.Api.LogisticsCancelOrder.Data;

namespace Dop.Api.LogisticsCancelOrder
{
	public class LogisticsCancelOrderResponse : DoudianOpApiResponse<LogisticsCancelOrderData>
	{
	}
}
