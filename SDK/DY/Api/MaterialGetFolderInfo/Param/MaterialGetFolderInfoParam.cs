using System.Runtime.Serialization;

namespace Dop.Api.MaterialGetFolderInfo.Param
{
	public class MaterialGetFolderInfoParam
	{
		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "page_num")]
		public int? PageNum { get; set; }

		[DataMemberAttribute(Name = "page_size")]
		public int? PageSize { get; set; }

	}
}
