using System;
using Dop.Core;
using Dop.Api.MaterialGetFolderInfo.Param;

namespace Dop.Api.MaterialGetFolderInfo
{
	public class MaterialGetFolderInfoRequest : DoudianOpApiRequest<MaterialGetFolderInfoParam>
	{
		public override string GetUrlPath()
		{
			return "/material/getFolderInfo";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialGetFolderInfoResponse);
		}

		public  MaterialGetFolderInfoParam BuildParam()
		{
			return Param ?? (Param = new MaterialGetFolderInfoParam());
		}

	}
}
