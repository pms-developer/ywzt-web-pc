using System.Runtime.Serialization;

namespace Dop.Api.MaterialGetFolderInfo.Data
{
	public class PhotoInfo
	{
		[DataMemberAttribute(Name = "height")]
		public int? Height { get; set; }

		[DataMemberAttribute(Name = "width")]
		public int? Width { get; set; }

		[DataMemberAttribute(Name = "format")]
		public string Format { get; set; }

	}
}
