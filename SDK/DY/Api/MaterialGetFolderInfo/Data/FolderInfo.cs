using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.MaterialGetFolderInfo.Data
{
	public class FolderInfo
	{
		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "folder_type")]
		public int? FolderType { get; set; }

		[DataMemberAttribute(Name = "folder_name")]
		public string FolderName { get; set; }

		[DataMemberAttribute(Name = "operate_status")]
		public int? OperateStatus { get; set; }

		[DataMemberAttribute(Name = "child_folder")]
		public List<ChildFolderItem> ChildFolder { get; set; }

		[DataMemberAttribute(Name = "child_material")]
		public List<ChildMaterialItem> ChildMaterial { get; set; }

		[DataMemberAttribute(Name = "parent_folder_id")]
		public string ParentFolderId { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public string CreateTime { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public string UpdateTime { get; set; }

		[DataMemberAttribute(Name = "delete_time")]
		public string DeleteTime { get; set; }

		[DataMemberAttribute(Name = "total_child_material_num")]
		public long? TotalChildMaterialNum { get; set; }

	}
}
