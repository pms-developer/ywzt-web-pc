using System.Runtime.Serialization;

namespace Dop.Api.MaterialGetFolderInfo.Data
{
	public class VideoInfo
	{
		[DataMemberAttribute(Name = "format")]
		public string Format { get; set; }

		[DataMemberAttribute(Name = "duration")]
		public double? Duration { get; set; }

	}
}
