using System.Runtime.Serialization;

namespace Dop.Api.MaterialGetFolderInfo.Data
{
	public class MaterialGetFolderInfoData
	{
		[DataMemberAttribute(Name = "folder_info")]
		public FolderInfo FolderInfo { get; set; }

	}
}
