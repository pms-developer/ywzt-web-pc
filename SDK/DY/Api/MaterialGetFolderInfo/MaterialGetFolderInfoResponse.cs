using Dop.Core;
using Dop.Api.MaterialGetFolderInfo.Data;

namespace Dop.Api.MaterialGetFolderInfo
{
	public class MaterialGetFolderInfoResponse : DoudianOpApiResponse<MaterialGetFolderInfoData>
	{
	}
}
