using System.Runtime.Serialization;

namespace Dop.Api.LogisticsTrackNoRouteDetail.Param
{
	public class LogisticsTrackNoRouteDetailParam
	{
		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

	}
}
