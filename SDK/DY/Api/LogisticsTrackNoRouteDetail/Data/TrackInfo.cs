using System.Runtime.Serialization;

namespace Dop.Api.LogisticsTrackNoRouteDetail.Data
{
	public class TrackInfo
	{
		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "express")]
		public string Express { get; set; }

	}
}
