using System.Runtime.Serialization;

namespace Dop.Api.LogisticsTrackNoRouteDetail.Data
{
	public class RouteNodeListItem
	{
		[DataMemberAttribute(Name = "content")]
		public string Content { get; set; }

		[DataMemberAttribute(Name = "timestamp")]
		public string Timestamp { get; set; }

		[DataMemberAttribute(Name = "state")]
		public string State { get; set; }

		[DataMemberAttribute(Name = "state_description")]
		public string StateDescription { get; set; }

		[DataMemberAttribute(Name = "site_name")]
		public string SiteName { get; set; }

	}
}
