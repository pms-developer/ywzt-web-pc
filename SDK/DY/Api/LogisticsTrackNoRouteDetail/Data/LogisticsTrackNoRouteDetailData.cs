using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsTrackNoRouteDetail.Data
{
	public class LogisticsTrackNoRouteDetailData
	{
		[DataMemberAttribute(Name = "route__node_list")]
		public List<RouteNodeListItem> RouteNodeList { get; set; }

		[DataMemberAttribute(Name = "track_info")]
		public TrackInfo TrackInfo { get; set; }

	}
}
