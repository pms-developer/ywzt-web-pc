using System;
using Dop.Core;
using Dop.Api.LogisticsTrackNoRouteDetail.Param;

namespace Dop.Api.LogisticsTrackNoRouteDetail
{
	public class LogisticsTrackNoRouteDetailRequest : DoudianOpApiRequest<LogisticsTrackNoRouteDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/trackNoRouteDetail";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsTrackNoRouteDetailResponse);
		}

		public  LogisticsTrackNoRouteDetailParam BuildParam()
		{
			return Param ?? (Param = new LogisticsTrackNoRouteDetailParam());
		}

	}
}
