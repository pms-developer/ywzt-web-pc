using Dop.Core;
using Dop.Api.LogisticsTrackNoRouteDetail.Data;

namespace Dop.Api.LogisticsTrackNoRouteDetail
{
	public class LogisticsTrackNoRouteDetailResponse : DoudianOpApiResponse<LogisticsTrackNoRouteDetailData>
	{
	}
}
