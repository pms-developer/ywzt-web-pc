using System;
using Dop.Core;
using Dop.Api.OrderLogisticsEditByPack.Param;

namespace Dop.Api.OrderLogisticsEditByPack
{
	public class OrderLogisticsEditByPackRequest : DoudianOpApiRequest<OrderLogisticsEditByPackParam>
	{
		public override string GetUrlPath()
		{
			return "/order/logisticsEditByPack";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderLogisticsEditByPackResponse);
		}

		public  OrderLogisticsEditByPackParam BuildParam()
		{
			return Param ?? (Param = new OrderLogisticsEditByPackParam());
		}

	}
}
