using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsEditByPack.Param
{
	public class OrderLogisticsEditByPackParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "pack_id")]
		public string PackId { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "logistics_id")]
		public string LogisticsId { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

	}
}
