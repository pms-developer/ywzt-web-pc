using System;
using Dop.Core;
using Dop.Api.AntispamOrderSend.Param;

namespace Dop.Api.AntispamOrderSend
{
	public class AntispamOrderSendRequest : DoudianOpApiRequest<AntispamOrderSendParam>
	{
		public override string GetUrlPath()
		{
			return "/antispam/orderSend";
		}

		public override Type GetResponseType()
		{
			return typeof(AntispamOrderSendResponse);
		}

		public  AntispamOrderSendParam BuildParam()
		{
			return Param ?? (Param = new AntispamOrderSendParam());
		}

	}
}
