using System.Runtime.Serialization;

namespace Dop.Api.AntispamOrderSend.Data
{
	public class Decision
	{
		[DataMemberAttribute(Name = "decision")]
		public string Decision_ { get; set; }

		[DataMemberAttribute(Name = "decision_detail")]
		public string DecisionDetail { get; set; }

		[DataMemberAttribute(Name = "hit_status")]
		public string HitStatus { get; set; }

	}
}
