using System.Runtime.Serialization;

namespace Dop.Api.AntispamOrderSend.Data
{
	public class AntispamOrderSendData
	{
		[DataMemberAttribute(Name = "decision")]
		public Decision Decision { get; set; }

	}
}
