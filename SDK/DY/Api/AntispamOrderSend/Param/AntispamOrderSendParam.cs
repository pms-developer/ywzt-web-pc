using System.Runtime.Serialization;

namespace Dop.Api.AntispamOrderSend.Param
{
	public class AntispamOrderSendParam
	{
		[DataMemberAttribute(Name = "event_time")]
		public long? EventTime { get; set; }

		[DataMemberAttribute(Name = "user")]
		public User User { get; set; }

		[DataMemberAttribute(Name = "params")]
		public string Params { get; set; }

	}
}
