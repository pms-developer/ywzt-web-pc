using System;
using Dop.Core;
using Dop.Api.SpuGetSpuTpl.Param;

namespace Dop.Api.SpuGetSpuTpl
{
	public class SpuGetSpuTplRequest : DoudianOpApiRequest<SpuGetSpuTplParam>
	{
		public override string GetUrlPath()
		{
			return "/spu/getSpuTpl";
		}

		public override Type GetResponseType()
		{
			return typeof(SpuGetSpuTplResponse);
		}

		public  SpuGetSpuTplParam BuildParam()
		{
			return Param ?? (Param = new SpuGetSpuTplParam());
		}

	}
}
