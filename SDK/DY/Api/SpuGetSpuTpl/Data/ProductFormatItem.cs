using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SpuGetSpuTpl.Data
{
	public class ProductFormatItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "options")]
		public List<OptionsItem> Options { get; set; }

		[DataMemberAttribute(Name = "require")]
		public long? Require { get; set; }

		[DataMemberAttribute(Name = "type")]
		public string Type { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

		[DataMemberAttribute(Name = "multi_select_max")]
		public long? MultiSelectMax { get; set; }

		[DataMemberAttribute(Name = "property_type")]
		public long? PropertyType { get; set; }

		[DataMemberAttribute(Name = "property_id")]
		public long? PropertyId { get; set; }

		[DataMemberAttribute(Name = "sequence")]
		public long? Sequence { get; set; }

	}
}
