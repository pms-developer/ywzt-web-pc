using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuTpl.Data
{
	public class SpuGetSpuTplData
	{
		[DataMemberAttribute(Name = "product_format")]
		public List<ProductFormatItem> ProductFormat { get; set; }

	}
}
