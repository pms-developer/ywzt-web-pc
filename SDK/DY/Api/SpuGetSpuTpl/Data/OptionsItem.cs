using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuTpl.Data
{
	public class OptionsItem
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "value_id")]
		public long? ValueId { get; set; }

		[DataMemberAttribute(Name = "sequence")]
		public long? Sequence { get; set; }

	}
}
