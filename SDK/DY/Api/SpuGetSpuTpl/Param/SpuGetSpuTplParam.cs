using System.Runtime.Serialization;

namespace Dop.Api.SpuGetSpuTpl.Param
{
	public class SpuGetSpuTplParam
	{
		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

	}
}
