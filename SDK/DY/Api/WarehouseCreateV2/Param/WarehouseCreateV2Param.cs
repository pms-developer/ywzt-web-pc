using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreateV2.Param
{
	public class WarehouseCreateV2Param
	{
		[DataMemberAttribute(Name = "warehouse")]
		public Warehouse Warehouse { get; set; }

		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "intro")]
		public string Intro { get; set; }

	}
}
