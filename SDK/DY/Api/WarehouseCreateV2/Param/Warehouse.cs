using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreateV2.Param
{
	public class Warehouse
	{
		[DataMemberAttribute(Name = "warehouse_location")]
		public WarehouseLocation WarehouseLocation { get; set; }

		[DataMemberAttribute(Name = "address_detail")]
		public string AddressDetail { get; set; }

	}
}
