using Dop.Core;
using Dop.Api.WarehouseCreateV2.Data;

namespace Dop.Api.WarehouseCreateV2
{
	public class WarehouseCreateV2Response : DoudianOpApiResponse<WarehouseCreateV2Data>
	{
	}
}
