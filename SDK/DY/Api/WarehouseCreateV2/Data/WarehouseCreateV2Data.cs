using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreateV2.Data
{
	public class WarehouseCreateV2Data
	{
		[DataMemberAttribute(Name = "warehouse_id")]
		public string WarehouseId { get; set; }

	}
}
