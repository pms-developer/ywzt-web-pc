using System;
using Dop.Core;
using Dop.Api.WarehouseCreateV2.Param;

namespace Dop.Api.WarehouseCreateV2
{
	public class WarehouseCreateV2Request : DoudianOpApiRequest<WarehouseCreateV2Param>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/createV2";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseCreateV2Response);
		}

		public  WarehouseCreateV2Param BuildParam()
		{
			return Param ?? (Param = new WarehouseCreateV2Param());
		}

	}
}
