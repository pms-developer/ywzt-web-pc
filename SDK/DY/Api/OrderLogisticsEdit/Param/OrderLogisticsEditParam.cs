using System.Runtime.Serialization;

namespace Dop.Api.OrderLogisticsEdit.Param
{
	public class OrderLogisticsEditParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "logistics_id")]
		public long? LogisticsId { get; set; }

		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "company")]
		public string Company { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

	}
}
