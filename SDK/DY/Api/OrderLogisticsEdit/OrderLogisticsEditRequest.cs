using System;
using Dop.Core;
using Dop.Api.OrderLogisticsEdit.Param;

namespace Dop.Api.OrderLogisticsEdit
{
	public class OrderLogisticsEditRequest : DoudianOpApiRequest<OrderLogisticsEditParam>
	{
		public override string GetUrlPath()
		{
			return "/order/logisticsEdit";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderLogisticsEditResponse);
		}

		public  OrderLogisticsEditParam BuildParam()
		{
			return Param ?? (Param = new OrderLogisticsEditParam());
		}

	}
}
