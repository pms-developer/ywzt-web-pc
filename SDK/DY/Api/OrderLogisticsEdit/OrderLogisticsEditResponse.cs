using Dop.Core;
using Dop.Api.OrderLogisticsEdit.Data;

namespace Dop.Api.OrderLogisticsEdit
{
	public class OrderLogisticsEditResponse : DoudianOpApiResponse<OrderLogisticsEditData>
	{
	}
}
