using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleOperate.Param
{
	public class AfterSaleOperateParam
	{
		[DataMemberAttribute(Name = "type")]
		public int? Type { get; set; }

		[DataMemberAttribute(Name = "items")]
		public List<ItemsItem> Items { get; set; }

	}
}
