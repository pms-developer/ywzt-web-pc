using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleOperate.Param
{
	public class ItemsItem
	{
		[DataMemberAttribute(Name = "aftersale_id")]
		public string AftersaleId { get; set; }

		[DataMemberAttribute(Name = "reason")]
		public string Reason { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "evidence")]
		public List<EvidenceItem> Evidence { get; set; }

		[DataMemberAttribute(Name = "logistics")]
		public Logistics Logistics { get; set; }

		[DataMemberAttribute(Name = "update_time")]
		public long? UpdateTime { get; set; }

		[DataMemberAttribute(Name = "reject_reason_code")]
		public long? RejectReasonCode { get; set; }

	}
}
