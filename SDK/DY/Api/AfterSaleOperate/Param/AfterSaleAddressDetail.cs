using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleOperate.Param
{
	public class AfterSaleAddressDetail
	{
		[DataMemberAttribute(Name = "province_name")]
		public string ProvinceName { get; set; }

		[DataMemberAttribute(Name = "city_name")]
		public string CityName { get; set; }

		[DataMemberAttribute(Name = "town_name")]
		public string TownName { get; set; }

		[DataMemberAttribute(Name = "detail")]
		public string Detail { get; set; }

		[DataMemberAttribute(Name = "user_name")]
		public string UserName { get; set; }

		[DataMemberAttribute(Name = "mobile")]
		public string Mobile { get; set; }

		[DataMemberAttribute(Name = "street_name")]
		public string StreetName { get; set; }

		[DataMemberAttribute(Name = "province_id")]
		public long? ProvinceId { get; set; }

		[DataMemberAttribute(Name = "city_id")]
		public long? CityId { get; set; }

		[DataMemberAttribute(Name = "town_id")]
		public long? TownId { get; set; }

		[DataMemberAttribute(Name = "street_id")]
		public long? StreetId { get; set; }

	}
}
