using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleOperate.Param
{
	public class EvidenceItem
	{
		[DataMemberAttribute(Name = "type")]
		public int? Type { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

		[DataMemberAttribute(Name = "desc")]
		public string Desc { get; set; }

	}
}
