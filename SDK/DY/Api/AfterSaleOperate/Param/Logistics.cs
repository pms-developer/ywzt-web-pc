using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleOperate.Param
{
	public class Logistics
	{
		[DataMemberAttribute(Name = "company_code")]
		public string CompanyCode { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "receiver_address_id")]
		public long? ReceiverAddressId { get; set; }

		[DataMemberAttribute(Name = "sender_address_id")]
		public long? SenderAddressId { get; set; }

		[DataMemberAttribute(Name = "after_sale_address_detail")]
		public AfterSaleAddressDetail AfterSaleAddressDetail { get; set; }

	}
}
