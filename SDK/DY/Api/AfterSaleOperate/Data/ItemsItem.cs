using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleOperate.Data
{
	public class ItemsItem
	{
		[DataMemberAttribute(Name = "aftersale_id")]
		public long? AftersaleId { get; set; }

		[DataMemberAttribute(Name = "status_code")]
		public long? StatusCode { get; set; }

		[DataMemberAttribute(Name = "status_msg")]
		public string StatusMsg { get; set; }

	}
}
