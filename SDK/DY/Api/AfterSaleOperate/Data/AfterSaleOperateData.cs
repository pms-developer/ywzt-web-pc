using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleOperate.Data
{
	public class AfterSaleOperateData
	{
		[DataMemberAttribute(Name = "items")]
		public List<ItemsItem> Items { get; set; }

	}
}
