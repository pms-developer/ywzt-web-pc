using System;
using Dop.Core;
using Dop.Api.AfterSaleOperate.Param;

namespace Dop.Api.AfterSaleOperate
{
	public class AfterSaleOperateRequest : DoudianOpApiRequest<AfterSaleOperateParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/operate";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleOperateResponse);
		}

		public  AfterSaleOperateParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleOperateParam());
		}

	}
}
