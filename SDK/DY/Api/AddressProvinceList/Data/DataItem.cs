using System.Runtime.Serialization;

namespace Dop.Api.AddressProvinceList.Data
{
	public class DataItem
	{
		[DataMemberAttribute(Name = "province_id")]
		public long? ProvinceId { get; set; }

		[DataMemberAttribute(Name = "province")]
		public string Province { get; set; }

		[DataMemberAttribute(Name = "father_id")]
		public long? FatherId { get; set; }

	}
}
