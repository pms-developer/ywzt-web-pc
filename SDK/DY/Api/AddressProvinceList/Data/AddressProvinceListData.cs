using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.AddressProvinceList.Data
{
	public class AddressProvinceListData
	{
		[DataMemberAttribute(Name = "data")]
		public List<DataItem> Data { get; set; }

	}
}
