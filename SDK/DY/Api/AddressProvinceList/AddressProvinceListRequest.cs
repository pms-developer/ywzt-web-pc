using System;
using Dop.Core;
using Dop.Api.AddressProvinceList.Param;

namespace Dop.Api.AddressProvinceList
{
	public class AddressProvinceListRequest : DoudianOpApiRequest<AddressProvinceListParam>
	{
		public override string GetUrlPath()
		{
			return "/address/provinceList";
		}

		public override Type GetResponseType()
		{
			return typeof(AddressProvinceListResponse);
		}

		public  AddressProvinceListParam BuildParam()
		{
			return Param ?? (Param = new AddressProvinceListParam());
		}

	}
}
