using System;
using Dop.Core;
using Dop.Api.OrderReplyService.Param;

namespace Dop.Api.OrderReplyService
{
	public class OrderReplyServiceRequest : DoudianOpApiRequest<OrderReplyServiceParam>
	{
		public override string GetUrlPath()
		{
			return "/order/replyService";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderReplyServiceResponse);
		}

		public  OrderReplyServiceParam BuildParam()
		{
			return Param ?? (Param = new OrderReplyServiceParam());
		}

	}
}
