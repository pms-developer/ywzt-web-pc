using System.Runtime.Serialization;

namespace Dop.Api.OrderReplyService.Param
{
	public class OrderReplyServiceParam
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "reply")]
		public string Reply { get; set; }

		[DataMemberAttribute(Name = "evidence")]
		public string Evidence { get; set; }

	}
}
