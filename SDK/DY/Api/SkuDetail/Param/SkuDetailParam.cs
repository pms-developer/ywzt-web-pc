using System.Runtime.Serialization;

namespace Dop.Api.SkuDetail.Param
{
	public class SkuDetailParam
	{
		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

	}
}
