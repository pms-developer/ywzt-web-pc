using System;
using Dop.Core;
using Dop.Api.SkuDetail.Param;

namespace Dop.Api.SkuDetail
{
	public class SkuDetailRequest : DoudianOpApiRequest<SkuDetailParam>
	{
		public override string GetUrlPath()
		{
			return "/sku/detail";
		}

		public override Type GetResponseType()
		{
			return typeof(SkuDetailResponse);
		}

		public  SkuDetailParam BuildParam()
		{
			return Param ?? (Param = new SkuDetailParam());
		}

	}
}
