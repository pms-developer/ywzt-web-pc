using System.Runtime.Serialization;

namespace Dop.Api.SkuDetail.Data
{
	public class SkuDetailData
	{
		[DataMemberAttribute(Name = "id")]
		public long? Id { get; set; }

		[DataMemberAttribute(Name = "open_user_id")]
		public long? OpenUserId { get; set; }

		[DataMemberAttribute(Name = "out_sku_id")]
		public long? OutSkuId { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id1")]
		public long? SpecDetailId1 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id2")]
		public long? SpecDetailId2 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_id3")]
		public long? SpecDetailId3 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_name1")]
		public string SpecDetailName1 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_name2")]
		public string SpecDetailName2 { get; set; }

		[DataMemberAttribute(Name = "spec_detail_name3")]
		public string SpecDetailName3 { get; set; }

		[DataMemberAttribute(Name = "customs_report_info")]
		public CustomsReportInfo CustomsReportInfo { get; set; }

		[DataMemberAttribute(Name = "price")]
		public long? Price { get; set; }

		[DataMemberAttribute(Name = "settlement_price")]
		public long? SettlementPrice { get; set; }

		[DataMemberAttribute(Name = "spec_id")]
		public long? SpecId { get; set; }

		[DataMemberAttribute(Name = "create_time")]
		public long? CreateTime { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "sku_type")]
		public long? SkuType { get; set; }

		[DataMemberAttribute(Name = "supplier_id")]
		public string SupplierId { get; set; }

		[DataMemberAttribute(Name = "stock_num")]
		public long? StockNum { get; set; }

		[DataMemberAttribute(Name = "prehold_stock_num")]
		public long? PreholdStockNum { get; set; }

		[DataMemberAttribute(Name = "prom_stock_num")]
		public long? PromStockNum { get; set; }

		[DataMemberAttribute(Name = "step_stock_num")]
		public long? StepStockNum { get; set; }

		[DataMemberAttribute(Name = "prehold_step_stock_num")]
		public long? PreholdStepStockNum { get; set; }

		[DataMemberAttribute(Name = "prom_step_stock_num")]
		public long? PromStepStockNum { get; set; }

		[DataMemberAttribute(Name = "product_id_str")]
		public string ProductIdStr { get; set; }

		[DataMemberAttribute(Name = "is_suit")]
		public int? IsSuit { get; set; }

		[DataMemberAttribute(Name = "suit_num")]
		public long? SuitNum { get; set; }

		[DataMemberAttribute(Name = "volume")]
		public long? Volume { get; set; }

	}
}
