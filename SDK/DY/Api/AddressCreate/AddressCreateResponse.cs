using Dop.Core;
using Dop.Api.AddressCreate.Data;

namespace Dop.Api.AddressCreate
{
	public class AddressCreateResponse : DoudianOpApiResponse<AddressCreateData>
	{
	}
}
