using System;
using Dop.Core;
using Dop.Api.AddressCreate.Param;

namespace Dop.Api.AddressCreate
{
	public class AddressCreateRequest : DoudianOpApiRequest<AddressCreateParam>
	{
		public override string GetUrlPath()
		{
			return "/address/create";
		}

		public override Type GetResponseType()
		{
			return typeof(AddressCreateResponse);
		}

		public  AddressCreateParam BuildParam()
		{
			return Param ?? (Param = new AddressCreateParam());
		}

	}
}
