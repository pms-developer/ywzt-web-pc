using System.Runtime.Serialization;

namespace Dop.Api.AddressCreate.Data
{
	public class AddressCreateData
	{
		[DataMemberAttribute(Name = "address_id")]
		public long? AddressId { get; set; }

	}
}
