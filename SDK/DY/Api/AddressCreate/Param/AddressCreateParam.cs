using System.Runtime.Serialization;

namespace Dop.Api.AddressCreate.Param
{
	public class AddressCreateParam
	{
		[DataMemberAttribute(Name = "address")]
		public Address Address { get; set; }

		[DataMemberAttribute(Name = "store_id")]
		public long? StoreId { get; set; }

	}
}
