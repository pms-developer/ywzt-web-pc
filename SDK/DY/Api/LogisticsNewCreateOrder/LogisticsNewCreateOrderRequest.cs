using System;
using Dop.Core;
using Dop.Api.LogisticsNewCreateOrder.Param;

namespace Dop.Api.LogisticsNewCreateOrder
{
	public class LogisticsNewCreateOrderRequest : DoudianOpApiRequest<LogisticsNewCreateOrderParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/newCreateOrder";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsNewCreateOrderResponse);
		}

		public  LogisticsNewCreateOrderParam BuildParam()
		{
			return Param ?? (Param = new LogisticsNewCreateOrderParam());
		}

	}
}
