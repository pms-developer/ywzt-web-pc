using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class NetInfo
	{
		[DataMemberAttribute(Name = "category")]
		public string Category { get; set; }

		[DataMemberAttribute(Name = "net_code")]
		public string NetCode { get; set; }

		[DataMemberAttribute(Name = "monthly_account")]
		public string MonthlyAccount { get; set; }

		[DataMemberAttribute(Name = "secret_key")]
		public string SecretKey { get; set; }

	}
}
