using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class SenderInfo
	{
		[DataMemberAttribute(Name = "address")]
		public Address Address { get; set; }

		[DataMemberAttribute(Name = "contact")]
		public Contact Contact { get; set; }

	}
}
