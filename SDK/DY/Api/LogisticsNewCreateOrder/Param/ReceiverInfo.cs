using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class ReceiverInfo
	{
		[DataMemberAttribute(Name = "address")]
		public Address_4_4 Address { get; set; }

		[DataMemberAttribute(Name = "contact")]
		public Contact Contact { get; set; }

	}
}
