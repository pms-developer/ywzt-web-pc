using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class Contact
	{
		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "phone")]
		public string Phone { get; set; }

		[DataMemberAttribute(Name = "mobile")]
		public string Mobile { get; set; }

	}
}
