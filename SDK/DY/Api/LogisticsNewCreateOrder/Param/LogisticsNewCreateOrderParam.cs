using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class LogisticsNewCreateOrderParam
	{
		[DataMemberAttribute(Name = "sender_info")]
		public SenderInfo SenderInfo { get; set; }

		[DataMemberAttribute(Name = "logistics_code")]
		public string LogisticsCode { get; set; }

		[DataMemberAttribute(Name = "order_infos")]
		public List<OrderInfosItem> OrderInfos { get; set; }

		[DataMemberAttribute(Name = "user_id")]
		public long? UserId { get; set; }

		[DataMemberAttribute(Name = "delivery_req")]
		public DeliveryReq DeliveryReq { get; set; }

		[DataMemberAttribute(Name = "order_channel")]
		public string OrderChannel { get; set; }

	}
}
