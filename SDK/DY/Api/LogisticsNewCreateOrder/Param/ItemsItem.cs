using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class ItemsItem
	{
		[DataMemberAttribute(Name = "item_name")]
		public string ItemName { get; set; }

		[DataMemberAttribute(Name = "item_specs")]
		public string ItemSpecs { get; set; }

		[DataMemberAttribute(Name = "item_count")]
		public int? ItemCount { get; set; }

		[DataMemberAttribute(Name = "item_volume")]
		public int? ItemVolume { get; set; }

		[DataMemberAttribute(Name = "item_weight")]
		public int? ItemWeight { get; set; }

		[DataMemberAttribute(Name = "item_net_weight")]
		public int? ItemNetWeight { get; set; }

	}
}
