using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class PodModelAddress
	{
		[DataMemberAttribute(Name = "country_code")]
		public string CountryCode { get; set; }

		[DataMemberAttribute(Name = "province_name")]
		public string ProvinceName { get; set; }

		[DataMemberAttribute(Name = "city_name")]
		public string CityName { get; set; }

		[DataMemberAttribute(Name = "district_name")]
		public string DistrictName { get; set; }

		[DataMemberAttribute(Name = "street_name")]
		public string StreetName { get; set; }

		[DataMemberAttribute(Name = "detail_address")]
		public string DetailAddress { get; set; }

	}
}
