using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class Warehouse
	{
		[DataMemberAttribute(Name = "is_sum_up")]
		public bool? IsSumUp { get; set; }

		[DataMemberAttribute(Name = "wh_code")]
		public string WhCode { get; set; }

		[DataMemberAttribute(Name = "wh_order_no")]
		public string WhOrderNo { get; set; }

		[DataMemberAttribute(Name = "delivery_type")]
		public string DeliveryType { get; set; }

	}
}
