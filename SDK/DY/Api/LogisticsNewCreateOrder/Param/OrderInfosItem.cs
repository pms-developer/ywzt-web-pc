using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsNewCreateOrder.Param
{
	public class OrderInfosItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "pack_id")]
		public string PackId { get; set; }

		[DataMemberAttribute(Name = "service_list")]
		public List<ServiceListItem> ServiceList { get; set; }

		[DataMemberAttribute(Name = "product_type")]
		public string ProductType { get; set; }

		[DataMemberAttribute(Name = "pay_method")]
		public int? PayMethod { get; set; }

		[DataMemberAttribute(Name = "pay_amount")]
		public long? PayAmount { get; set; }

		[DataMemberAttribute(Name = "pod_model_address")]
		public PodModelAddress PodModelAddress { get; set; }

		[DataMemberAttribute(Name = "receiver_info")]
		public ReceiverInfo ReceiverInfo { get; set; }

		[DataMemberAttribute(Name = "items")]
		public List<ItemsItem> Items { get; set; }

		[DataMemberAttribute(Name = "sender_fetch_time")]
		public string SenderFetchTime { get; set; }

		[DataMemberAttribute(Name = "is_sign_back")]
		public int? IsSignBack { get; set; }

		[DataMemberAttribute(Name = "remark")]
		public string Remark { get; set; }

		[DataMemberAttribute(Name = "extra")]
		public string Extra { get; set; }

		[DataMemberAttribute(Name = "total_pack_count")]
		public int? TotalPackCount { get; set; }

		[DataMemberAttribute(Name = "total_weight")]
		public string TotalWeight { get; set; }

		[DataMemberAttribute(Name = "total_length")]
		public string TotalLength { get; set; }

		[DataMemberAttribute(Name = "total_width")]
		public string TotalWidth { get; set; }

		[DataMemberAttribute(Name = "total_height")]
		public string TotalHeight { get; set; }

		[DataMemberAttribute(Name = "volume")]
		public string Volume { get; set; }

		[DataMemberAttribute(Name = "warehouse")]
		public Warehouse Warehouse { get; set; }

		[DataMemberAttribute(Name = "net_info")]
		public NetInfo NetInfo { get; set; }

		[DataMemberAttribute(Name = "shipping_code")]
		public string ShippingCode { get; set; }

		[DataMemberAttribute(Name = "special_delivery_type_code")]
		public string SpecialDeliveryTypeCode { get; set; }

		[DataMemberAttribute(Name = "special_delivery_type_value")]
		public string SpecialDeliveryTypeValue { get; set; }

		[DataMemberAttribute(Name = "package_weight")]
		public int? PackageWeight { get; set; }

	}
}
