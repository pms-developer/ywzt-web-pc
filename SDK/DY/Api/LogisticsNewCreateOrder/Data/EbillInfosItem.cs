using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Data
{
	public class EbillInfosItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "pack_id")]
		public string PackId { get; set; }

		[DataMemberAttribute(Name = "track_no")]
		public string TrackNo { get; set; }

		[DataMemberAttribute(Name = "sort_code")]
		public string SortCode { get; set; }

		[DataMemberAttribute(Name = "package_center_code")]
		public string PackageCenterCode { get; set; }

		[DataMemberAttribute(Name = "package_center_name")]
		public string PackageCenterName { get; set; }

		[DataMemberAttribute(Name = "short_address_code")]
		public string ShortAddressCode { get; set; }

		[DataMemberAttribute(Name = "short_address_name")]
		public string ShortAddressName { get; set; }

		[DataMemberAttribute(Name = "extra_resp")]
		public string ExtraResp { get; set; }

		[DataMemberAttribute(Name = "sub_waybill_codes")]
		public string SubWaybillCodes { get; set; }

		[DataMemberAttribute(Name = "order_channel")]
		public string OrderChannel { get; set; }

		[DataMemberAttribute(Name = "shipping_code")]
		public string ShippingCode { get; set; }

	}
}
