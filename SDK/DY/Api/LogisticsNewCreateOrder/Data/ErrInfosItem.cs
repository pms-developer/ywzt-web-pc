using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Data
{
	public class ErrInfosItem
	{
		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "pack_id")]
		public string PackId { get; set; }

		[DataMemberAttribute(Name = "err_code")]
		public string ErrCode { get; set; }

		[DataMemberAttribute(Name = "err_msg")]
		public string ErrMsg { get; set; }

		[DataMemberAttribute(Name = "order_channel")]
		public string OrderChannel { get; set; }

	}
}
