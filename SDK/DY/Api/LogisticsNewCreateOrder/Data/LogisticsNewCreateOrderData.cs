using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsNewCreateOrder.Data
{
	public class LogisticsNewCreateOrderData
	{
		[DataMemberAttribute(Name = "ebill_infos")]
		public List<EbillInfosItem> EbillInfos { get; set; }

		[DataMemberAttribute(Name = "err_infos")]
		public List<ErrInfosItem> ErrInfos { get; set; }

	}
}
