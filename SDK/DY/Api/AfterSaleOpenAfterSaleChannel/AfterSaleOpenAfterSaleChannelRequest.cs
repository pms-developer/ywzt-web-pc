using System;
using Dop.Core;
using Dop.Api.AfterSaleOpenAfterSaleChannel.Param;

namespace Dop.Api.AfterSaleOpenAfterSaleChannel
{
	public class AfterSaleOpenAfterSaleChannelRequest : DoudianOpApiRequest<AfterSaleOpenAfterSaleChannelParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/OpenAfterSaleChannel";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleOpenAfterSaleChannelResponse);
		}

		public  AfterSaleOpenAfterSaleChannelParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleOpenAfterSaleChannelParam());
		}

	}
}
