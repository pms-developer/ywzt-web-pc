using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleOpenAfterSaleChannel.Data
{
	public class AfterSaleOpenAfterSaleChannelData
	{
		[DataMemberAttribute(Name = "conclusion")]
		public Conclusion Conclusion { get; set; }

	}
}
