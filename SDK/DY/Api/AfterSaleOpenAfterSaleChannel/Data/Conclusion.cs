using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.AfterSaleOpenAfterSaleChannel.Data
{
	public class Conclusion
	{
		[DataMemberAttribute(Name = "match_conclusion")]
		public int? MatchConclusion { get; set; }

		[DataMemberAttribute(Name = "match_message")]
		public string MatchMessage { get; set; }

		[DataMemberAttribute(Name = "can_apply_type_list")]
		public List<long> CanApplyTypeList { get; set; }

		[DataMemberAttribute(Name = "match_success")]
		public bool? MatchSuccess { get; set; }

	}
}
