using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleOpenAfterSaleChannel.Param
{
	public class AfterSaleOpenAfterSaleChannelParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public long? OrderId { get; set; }

	}
}
