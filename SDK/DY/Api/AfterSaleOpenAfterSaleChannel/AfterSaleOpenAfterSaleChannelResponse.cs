using Dop.Core;
using Dop.Api.AfterSaleOpenAfterSaleChannel.Data;

namespace Dop.Api.AfterSaleOpenAfterSaleChannel
{
	public class AfterSaleOpenAfterSaleChannelResponse : DoudianOpApiResponse<AfterSaleOpenAfterSaleChannelData>
	{
	}
}
