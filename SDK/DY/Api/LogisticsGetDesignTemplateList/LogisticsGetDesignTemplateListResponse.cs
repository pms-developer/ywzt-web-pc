using Dop.Core;
using Dop.Api.LogisticsGetDesignTemplateList.Data;

namespace Dop.Api.LogisticsGetDesignTemplateList
{
	public class LogisticsGetDesignTemplateListResponse : DoudianOpApiResponse<LogisticsGetDesignTemplateListData>
	{
	}
}
