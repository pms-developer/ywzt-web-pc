using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.LogisticsGetDesignTemplateList.Data
{
	public class DesignTemplateDataItem
	{
		[DataMemberAttribute(Name = "design_template_code")]
		public string DesignTemplateCode { get; set; }

		[DataMemberAttribute(Name = "design_template_name")]
		public string DesignTemplateName { get; set; }

		[DataMemberAttribute(Name = "design_template_url")]
		public string DesignTemplateUrl { get; set; }

		[DataMemberAttribute(Name = "design_template_key_list")]
		public List<string> DesignTemplateKeyList { get; set; }

	}
}
