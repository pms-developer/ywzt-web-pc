using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.LogisticsGetDesignTemplateList.Data
{
	public class LogisticsGetDesignTemplateListData
	{
		[DataMemberAttribute(Name = "design_template_data")]
		public List<DesignTemplateDataItem> DesignTemplateData { get; set; }

	}
}
