using System;
using Dop.Core;
using Dop.Api.LogisticsGetDesignTemplateList.Param;

namespace Dop.Api.LogisticsGetDesignTemplateList
{
	public class LogisticsGetDesignTemplateListRequest : DoudianOpApiRequest<LogisticsGetDesignTemplateListParam>
	{
		public override string GetUrlPath()
		{
			return "/logistics/getDesignTemplateList";
		}

		public override Type GetResponseType()
		{
			return typeof(LogisticsGetDesignTemplateListResponse);
		}

		public  LogisticsGetDesignTemplateListParam BuildParam()
		{
			return Param ?? (Param = new LogisticsGetDesignTemplateListParam());
		}

	}
}
