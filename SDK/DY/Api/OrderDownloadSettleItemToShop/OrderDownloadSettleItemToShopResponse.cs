using Dop.Core;
using Dop.Api.OrderDownloadSettleItemToShop.Data;

namespace Dop.Api.OrderDownloadSettleItemToShop
{
	public class OrderDownloadSettleItemToShopResponse : DoudianOpApiResponse<OrderDownloadSettleItemToShopData>
	{
	}
}
