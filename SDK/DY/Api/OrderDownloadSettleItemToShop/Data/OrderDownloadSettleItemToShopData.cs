using System.Runtime.Serialization;

namespace Dop.Api.OrderDownloadSettleItemToShop.Data
{
	public class OrderDownloadSettleItemToShopData
	{
		[DataMemberAttribute(Name = "download_id")]
		public string DownloadId { get; set; }

		[DataMemberAttribute(Name = "code")]
		public string Code { get; set; }

		[DataMemberAttribute(Name = "code_msg")]
		public string CodeMsg { get; set; }

	}
}
