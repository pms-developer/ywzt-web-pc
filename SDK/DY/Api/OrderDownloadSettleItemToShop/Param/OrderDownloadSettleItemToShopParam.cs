using System.Runtime.Serialization;

namespace Dop.Api.OrderDownloadSettleItemToShop.Param
{
	public class OrderDownloadSettleItemToShopParam
	{
		[DataMemberAttribute(Name = "start_time")]
		public string StartTime { get; set; }

		[DataMemberAttribute(Name = "end_time")]
		public string EndTime { get; set; }

		[DataMemberAttribute(Name = "time_type")]
		public string TimeType { get; set; }

		[DataMemberAttribute(Name = "order_id")]
		public string OrderId { get; set; }

		[DataMemberAttribute(Name = "bill_id")]
		public string BillId { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public string ProductId { get; set; }

		[DataMemberAttribute(Name = "pay_type")]
		public string PayType { get; set; }

		[DataMemberAttribute(Name = "flow_type")]
		public string FlowType { get; set; }

	}
}
