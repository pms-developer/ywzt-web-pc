using System;
using Dop.Core;
using Dop.Api.OrderDownloadSettleItemToShop.Param;

namespace Dop.Api.OrderDownloadSettleItemToShop
{
	public class OrderDownloadSettleItemToShopRequest : DoudianOpApiRequest<OrderDownloadSettleItemToShopParam>
	{
		public override string GetUrlPath()
		{
			return "/order/downloadSettleItemToShop";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderDownloadSettleItemToShopResponse);
		}

		public  OrderDownloadSettleItemToShopParam BuildParam()
		{
			return Param ?? (Param = new OrderDownloadSettleItemToShopParam());
		}

	}
}
