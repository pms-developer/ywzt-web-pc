using Dop.Core;
using Dop.Api.WarehouseRemoveAddr.Data;

namespace Dop.Api.WarehouseRemoveAddr
{
	public class WarehouseRemoveAddrResponse : DoudianOpApiResponse<WarehouseRemoveAddrData>
	{
	}
}
