using System;
using Dop.Core;
using Dop.Api.WarehouseRemoveAddr.Param;

namespace Dop.Api.WarehouseRemoveAddr
{
	public class WarehouseRemoveAddrRequest : DoudianOpApiRequest<WarehouseRemoveAddrParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/removeAddr";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseRemoveAddrResponse);
		}

		public  WarehouseRemoveAddrParam BuildParam()
		{
			return Param ?? (Param = new WarehouseRemoveAddrParam());
		}

	}
}
