using System.Runtime.Serialization;

namespace Dop.Api.WarehouseRemoveAddr.Param
{
	public class WarehouseRemoveAddrParam
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "addr")]
		public Addr Addr { get; set; }

	}
}
