using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.BrandList.Data
{
	public class BrandListData
	{
		[DataMemberAttribute(Name = "brand_ids")]
		public List<long> BrandIds { get; set; }

		[DataMemberAttribute(Name = "brand_infos")]
		public Dictionary<long,BrandInfosItem> BrandInfos { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

		[DataMemberAttribute(Name = "has_more")]
		public bool? HasMore { get; set; }

		[DataMemberAttribute(Name = "auth_required")]
		public bool? AuthRequired { get; set; }

		[DataMemberAttribute(Name = "auth_brand_list")]
		public List<AuthBrandListItem> AuthBrandList { get; set; }

		[DataMemberAttribute(Name = "brand_list")]
		public List<BrandListItem> BrandList { get; set; }

	}
}
