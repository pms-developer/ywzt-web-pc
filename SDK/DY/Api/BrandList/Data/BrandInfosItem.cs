using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.BrandList.Data
{
	public class BrandInfosItem
	{
		[DataMemberAttribute(Name = "brand_id")]
		public long? BrandId { get; set; }

		[DataMemberAttribute(Name = "brand_name_c_n")]
		public string BrandNameCN { get; set; }

		[DataMemberAttribute(Name = "brand_name_e_n")]
		public string BrandNameEN { get; set; }

		[DataMemberAttribute(Name = "level")]
		public int? Level { get; set; }

		[DataMemberAttribute(Name = "status")]
		public int? Status { get; set; }

		[DataMemberAttribute(Name = "brand_alias")]
		public List<string> BrandAlias { get; set; }

		[DataMemberAttribute(Name = "create_timestamp")]
		public long? CreateTimestamp { get; set; }

		[DataMemberAttribute(Name = "update_timestamp")]
		public long? UpdateTimestamp { get; set; }

		[DataMemberAttribute(Name = "audit_status")]
		public int? AuditStatus { get; set; }

		[DataMemberAttribute(Name = "biz_type")]
		public int? BizType { get; set; }

		[DataMemberAttribute(Name = "logo")]
		public string Logo { get; set; }

	}
}
