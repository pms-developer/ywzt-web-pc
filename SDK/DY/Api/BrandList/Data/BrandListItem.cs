using System.Runtime.Serialization;

namespace Dop.Api.BrandList.Data
{
	public class BrandListItem
	{
		[DataMemberAttribute(Name = "brand_id")]
		public long? BrandId { get; set; }

		[DataMemberAttribute(Name = "name_cn")]
		public string NameCn { get; set; }

		[DataMemberAttribute(Name = "name_en")]
		public string NameEn { get; set; }

	}
}
