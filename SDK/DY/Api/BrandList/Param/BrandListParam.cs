using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.BrandList.Param
{
	public class BrandListParam
	{
		[DataMemberAttribute(Name = "categories")]
		public List<long> Categories { get; set; }

		[DataMemberAttribute(Name = "offset")]
		public long? Offset { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "sort")]
		public int? Sort { get; set; }

		[DataMemberAttribute(Name = "status")]
		public int? Status { get; set; }

		[DataMemberAttribute(Name = "full_brand_info")]
		public bool? FullBrandInfo { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

		[DataMemberAttribute(Name = "query")]
		public string Query { get; set; }

	}
}
