using System;
using Dop.Core;
using Dop.Api.BrandList.Param;

namespace Dop.Api.BrandList
{
	public class BrandListRequest : DoudianOpApiRequest<BrandListParam>
	{
		public override string GetUrlPath()
		{
			return "/brand/list";
		}

		public override Type GetResponseType()
		{
			return typeof(BrandListResponse);
		}

		public  BrandListParam BuildParam()
		{
			return Param ?? (Param = new BrandListParam());
		}

	}
}
