using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SkuSyncStockBatch.Param
{
	public class SkuSyncStockBatchParam
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "idempotent_id")]
		public string IdempotentId { get; set; }

		[DataMemberAttribute(Name = "incremental")]
		public bool? Incremental { get; set; }

		[DataMemberAttribute(Name = "sku_sync_list")]
		public List<SkuSyncListItem> SkuSyncList { get; set; }

	}
}
