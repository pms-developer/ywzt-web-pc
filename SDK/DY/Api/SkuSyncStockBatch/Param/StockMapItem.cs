using System.Runtime.Serialization;

namespace Dop.Api.SkuSyncStockBatch.Param
{
	public class StockMapItem
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "stock_num")]
		public long? StockNum { get; set; }

	}
}
