using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SkuSyncStockBatch.Param
{
	public class SkuSyncListItem
	{
		[DataMemberAttribute(Name = "sku_id")]
		public long? SkuId { get; set; }

		[DataMemberAttribute(Name = "sku_type")]
		public long? SkuType { get; set; }

		[DataMemberAttribute(Name = "stock_num")]
		public long? StockNum { get; set; }

		[DataMemberAttribute(Name = "step_stock_num")]
		public long? StepStockNum { get; set; }

		[DataMemberAttribute(Name = "stock_map")]
		public List<StockMapItem> StockMap { get; set; }

		[DataMemberAttribute(Name = "supplier_id")]
		public string SupplierId { get; set; }

	}
}
