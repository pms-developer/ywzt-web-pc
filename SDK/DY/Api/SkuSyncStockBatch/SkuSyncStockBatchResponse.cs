using Dop.Core;
using Dop.Api.SkuSyncStockBatch.Data;

namespace Dop.Api.SkuSyncStockBatch
{
	public class SkuSyncStockBatchResponse : DoudianOpApiResponse<SkuSyncStockBatchData>
	{
	}
}
