using System;
using Dop.Core;
using Dop.Api.SkuSyncStockBatch.Param;

namespace Dop.Api.SkuSyncStockBatch
{
	public class SkuSyncStockBatchRequest : DoudianOpApiRequest<SkuSyncStockBatchParam>
	{
		public override string GetUrlPath()
		{
			return "/sku/syncStockBatch";
		}

		public override Type GetResponseType()
		{
			return typeof(SkuSyncStockBatchResponse);
		}

		public  SkuSyncStockBatchParam BuildParam()
		{
			return Param ?? (Param = new SkuSyncStockBatchParam());
		}

	}
}
