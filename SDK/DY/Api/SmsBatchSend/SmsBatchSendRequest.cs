using System;
using Dop.Core;
using Dop.Api.SmsBatchSend.Param;

namespace Dop.Api.SmsBatchSend
{
	public class SmsBatchSendRequest : DoudianOpApiRequest<SmsBatchSendParam>
	{
		public override string GetUrlPath()
		{
			return "/sms/batchSend";
		}

		public override Type GetResponseType()
		{
			return typeof(SmsBatchSendResponse);
		}

		public  SmsBatchSendParam BuildParam()
		{
			return Param ?? (Param = new SmsBatchSendParam());
		}

	}
}
