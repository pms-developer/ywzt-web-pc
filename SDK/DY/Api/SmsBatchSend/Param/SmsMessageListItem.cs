using System.Runtime.Serialization;

namespace Dop.Api.SmsBatchSend.Param
{
	public class SmsMessageListItem
	{
		[DataMemberAttribute(Name = "post_tel")]
		public string PostTel { get; set; }

		[DataMemberAttribute(Name = "template_param")]
		public string TemplateParam { get; set; }

	}
}
