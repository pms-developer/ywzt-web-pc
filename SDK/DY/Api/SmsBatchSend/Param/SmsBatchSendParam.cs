using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.SmsBatchSend.Param
{
	public class SmsBatchSendParam
	{
		[DataMemberAttribute(Name = "sms_account")]
		public string SmsAccount { get; set; }

		[DataMemberAttribute(Name = "sign")]
		public string Sign { get; set; }

		[DataMemberAttribute(Name = "sms_message_list")]
		public List<SmsMessageListItem> SmsMessageList { get; set; }

		[DataMemberAttribute(Name = "template_id")]
		public string TemplateId { get; set; }

		[DataMemberAttribute(Name = "tag")]
		public string Tag { get; set; }

		[DataMemberAttribute(Name = "user_ext_code")]
		public string UserExtCode { get; set; }

	}
}
