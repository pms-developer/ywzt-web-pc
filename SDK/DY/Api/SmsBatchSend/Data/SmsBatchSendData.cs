using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.SmsBatchSend.Data
{
	public class SmsBatchSendData
	{
		[DataMemberAttribute(Name = "send_result_list")]
		public List<SendResultListItem> SendResultList { get; set; }

	}
}
