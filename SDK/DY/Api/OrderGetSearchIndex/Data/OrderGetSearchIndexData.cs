using System.Runtime.Serialization;

namespace Dop.Api.OrderGetSearchIndex.Data
{
	public class OrderGetSearchIndexData
	{
		[DataMemberAttribute(Name = "encrypt_index_text")]
		public string EncryptIndexText { get; set; }

	}
}
