using System.Runtime.Serialization;

namespace Dop.Api.OrderGetSearchIndex.Param
{
	public class OrderGetSearchIndexParam
	{
		[DataMemberAttribute(Name = "plain_text")]
		public string PlainText { get; set; }

		[DataMemberAttribute(Name = "sensitive_type")]
		public int? SensitiveType { get; set; }

	}
}
