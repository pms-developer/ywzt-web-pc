using System;
using Dop.Core;
using Dop.Api.OrderGetSearchIndex.Param;

namespace Dop.Api.OrderGetSearchIndex
{
	public class OrderGetSearchIndexRequest : DoudianOpApiRequest<OrderGetSearchIndexParam>
	{
		public override string GetUrlPath()
		{
			return "/order/getSearchIndex";
		}

		public override Type GetResponseType()
		{
			return typeof(OrderGetSearchIndexResponse);
		}

		public  OrderGetSearchIndexParam BuildParam()
		{
			return Param ?? (Param = new OrderGetSearchIndexParam());
		}

	}
}
