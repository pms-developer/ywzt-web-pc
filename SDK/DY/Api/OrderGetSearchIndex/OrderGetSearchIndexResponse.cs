using Dop.Core;
using Dop.Api.OrderGetSearchIndex.Data;

namespace Dop.Api.OrderGetSearchIndex
{
	public class OrderGetSearchIndexResponse : DoudianOpApiResponse<OrderGetSearchIndexData>
	{
	}
}
