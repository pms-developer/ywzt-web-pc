using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductQualificationConfig.Data
{
	public class ProductQualificationConfigData
	{
		[DataMemberAttribute(Name = "config_list")]
		public List<ConfigListItem> ConfigList { get; set; }

	}
}
