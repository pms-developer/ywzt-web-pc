using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductQualificationConfig.Data
{
	public class ConfigListItem
	{
		[DataMemberAttribute(Name = "key")]
		public string Key { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "text_list")]
		public List<string> TextList { get; set; }

		[DataMemberAttribute(Name = "is_required")]
		public bool? IsRequired { get; set; }

		[DataMemberAttribute(Name = "support_web_url_type")]
		public string SupportWebUrlType { get; set; }

	}
}
