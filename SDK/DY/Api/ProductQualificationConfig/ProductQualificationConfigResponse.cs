using Dop.Core;
using Dop.Api.ProductQualificationConfig.Data;

namespace Dop.Api.ProductQualificationConfig
{
	public class ProductQualificationConfigResponse : DoudianOpApiResponse<ProductQualificationConfigData>
	{
	}
}
