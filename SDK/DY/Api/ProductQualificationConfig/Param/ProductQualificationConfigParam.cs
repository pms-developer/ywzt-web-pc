using System.Runtime.Serialization;

namespace Dop.Api.ProductQualificationConfig.Param
{
	public class ProductQualificationConfigParam
	{
		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

	}
}
