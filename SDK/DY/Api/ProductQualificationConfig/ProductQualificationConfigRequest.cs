using System;
using Dop.Core;
using Dop.Api.ProductQualificationConfig.Param;

namespace Dop.Api.ProductQualificationConfig
{
	public class ProductQualificationConfigRequest : DoudianOpApiRequest<ProductQualificationConfigParam>
	{
		public override string GetUrlPath()
		{
			return "/product/qualificationConfig";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductQualificationConfigResponse);
		}

		public  ProductQualificationConfigParam BuildParam()
		{
			return Param ?? (Param = new ProductQualificationConfigParam());
		}

	}
}
