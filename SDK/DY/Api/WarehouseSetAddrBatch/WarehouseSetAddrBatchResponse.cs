using Dop.Core;
using Dop.Api.WarehouseSetAddrBatch.Data;

namespace Dop.Api.WarehouseSetAddrBatch
{
	public class WarehouseSetAddrBatchResponse : DoudianOpApiResponse<WarehouseSetAddrBatchData>
	{
	}
}
