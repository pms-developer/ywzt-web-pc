using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.WarehouseSetAddrBatch.Param
{
	public class WarehouseSetAddrBatchParam
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "addr_list")]
		public List<AddrListItem> AddrList { get; set; }

	}
}
