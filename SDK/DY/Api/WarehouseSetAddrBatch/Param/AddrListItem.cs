using System.Runtime.Serialization;

namespace Dop.Api.WarehouseSetAddrBatch.Param
{
	public class AddrListItem
	{
		[DataMemberAttribute(Name = "addr_id1")]
		public long? AddrId1 { get; set; }

		[DataMemberAttribute(Name = "addr_id2")]
		public long? AddrId2 { get; set; }

		[DataMemberAttribute(Name = "addr_id3")]
		public long? AddrId3 { get; set; }

		[DataMemberAttribute(Name = "addr_id4")]
		public long? AddrId4 { get; set; }

	}
}
