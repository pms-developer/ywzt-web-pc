using System;
using Dop.Core;
using Dop.Api.WarehouseSetAddrBatch.Param;

namespace Dop.Api.WarehouseSetAddrBatch
{
	public class WarehouseSetAddrBatchRequest : DoudianOpApiRequest<WarehouseSetAddrBatchParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/setAddrBatch";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseSetAddrBatchResponse);
		}

		public  WarehouseSetAddrBatchParam BuildParam()
		{
			return Param ?? (Param = new WarehouseSetAddrBatchParam());
		}

	}
}
