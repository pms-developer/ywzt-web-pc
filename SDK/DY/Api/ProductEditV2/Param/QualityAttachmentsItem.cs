using System.Runtime.Serialization;

namespace Dop.Api.ProductEditV2.Param
{
	public class QualityAttachmentsItem
	{
		[DataMemberAttribute(Name = "media_type")]
		public long? MediaType { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
