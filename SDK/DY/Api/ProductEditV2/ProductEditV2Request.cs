using System;
using Dop.Core;
using Dop.Api.ProductEditV2.Param;

namespace Dop.Api.ProductEditV2
{
	public class ProductEditV2Request : DoudianOpApiRequest<ProductEditV2Param>
	{
		public override string GetUrlPath()
		{
			return "/product/editV2";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductEditV2Response);
		}

		public  ProductEditV2Param BuildParam()
		{
			return Param ?? (Param = new ProductEditV2Param());
		}

	}
}
