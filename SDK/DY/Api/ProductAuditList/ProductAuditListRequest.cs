using System;
using Dop.Core;
using Dop.Api.ProductAuditList.Param;

namespace Dop.Api.ProductAuditList
{
	public class ProductAuditListRequest : DoudianOpApiRequest<ProductAuditListParam>
	{
		public override string GetUrlPath()
		{
			return "/product/auditList";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductAuditListResponse);
		}

		public  ProductAuditListParam BuildParam()
		{
			return Param ?? (Param = new ProductAuditListParam());
		}

	}
}
