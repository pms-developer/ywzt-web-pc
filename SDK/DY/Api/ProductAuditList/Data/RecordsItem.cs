using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Dop.Api.ProductAuditList.Data
{
	public class RecordsItem
	{
		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

		[DataMemberAttribute(Name = "title")]
		public string Title { get; set; }

		[DataMemberAttribute(Name = "img_url")]
		public string ImgUrl { get; set; }

		[DataMemberAttribute(Name = "publish_status")]
		public long? PublishStatus { get; set; }

		[DataMemberAttribute(Name = "publish_time")]
		public long? PublishTime { get; set; }

		[DataMemberAttribute(Name = "audit_time")]
		public long? AuditTime { get; set; }

		[DataMemberAttribute(Name = "audit_reason")]
		public Dictionary<string,List<string>> AuditReason { get; set; }

	}
}
