using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.ProductAuditList.Data
{
	public class ProductAuditListData
	{
		[DataMemberAttribute(Name = "records")]
		public List<RecordsItem> Records { get; set; }

		[DataMemberAttribute(Name = "total")]
		public long? Total { get; set; }

	}
}
