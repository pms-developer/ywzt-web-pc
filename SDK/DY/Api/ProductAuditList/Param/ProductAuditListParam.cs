using System.Runtime.Serialization;

namespace Dop.Api.ProductAuditList.Param
{
	public class ProductAuditListParam
	{
		[DataMemberAttribute(Name = "publish_status")]
		public long? PublishStatus { get; set; }

		[DataMemberAttribute(Name = "page")]
		public long? Page { get; set; }

		[DataMemberAttribute(Name = "size")]
		public long? Size { get; set; }

		[DataMemberAttribute(Name = "product_id")]
		public long? ProductId { get; set; }

	}
}
