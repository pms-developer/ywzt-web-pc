using Dop.Core;
using Dop.Api.WarehouseEdit.Data;

namespace Dop.Api.WarehouseEdit
{
	public class WarehouseEditResponse : DoudianOpApiResponse<WarehouseEditData>
	{
	}
}
