using System;
using Dop.Core;
using Dop.Api.WarehouseEdit.Param;

namespace Dop.Api.WarehouseEdit
{
	public class WarehouseEditRequest : DoudianOpApiRequest<WarehouseEditParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/edit";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseEditResponse);
		}

		public  WarehouseEditParam BuildParam()
		{
			return Param ?? (Param = new WarehouseEditParam());
		}

	}
}
