using System.Runtime.Serialization;

namespace Dop.Api.WarehouseEdit.Param
{
	public class WarehouseEditParam
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "intro")]
		public string Intro { get; set; }

		[DataMemberAttribute(Name = "address_id1")]
		public long? AddressId1 { get; set; }

		[DataMemberAttribute(Name = "address_id2")]
		public long? AddressId2 { get; set; }

		[DataMemberAttribute(Name = "address_id3")]
		public long? AddressId3 { get; set; }

		[DataMemberAttribute(Name = "address_id4")]
		public long? AddressId4 { get; set; }

		[DataMemberAttribute(Name = "address_detail")]
		public string AddressDetail { get; set; }

	}
}
