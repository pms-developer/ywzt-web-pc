using System.Runtime.Serialization;

namespace Dop.Api.WarehouseEdit.Data
{
	public class WarehouseEditData
	{
		[DataMemberAttribute(Name = "data")]
		public bool? Data { get; set; }

	}
}
