using System.Runtime.Serialization;

namespace Dop.Api.AfterSaleOpenOutAfterSale.Param
{
	public class AfterSaleOpenOutAfterSaleParam
	{
		[DataMemberAttribute(Name = "order_id")]
		public long? OrderId { get; set; }

	}
}
