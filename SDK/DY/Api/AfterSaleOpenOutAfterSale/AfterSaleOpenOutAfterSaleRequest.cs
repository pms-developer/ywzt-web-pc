using System;
using Dop.Core;
using Dop.Api.AfterSaleOpenOutAfterSale.Param;

namespace Dop.Api.AfterSaleOpenOutAfterSale
{
	public class AfterSaleOpenOutAfterSaleRequest : DoudianOpApiRequest<AfterSaleOpenOutAfterSaleParam>
	{
		public override string GetUrlPath()
		{
			return "/afterSale/openOutAfterSale";
		}

		public override Type GetResponseType()
		{
			return typeof(AfterSaleOpenOutAfterSaleResponse);
		}

		public  AfterSaleOpenOutAfterSaleParam BuildParam()
		{
			return Param ?? (Param = new AfterSaleOpenOutAfterSaleParam());
		}

	}
}
