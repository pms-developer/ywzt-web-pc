using System.Runtime.Serialization;

namespace Dop.Api.MaterialBatchUploadImageSync.Data
{
	public class SuccessMapItem
	{
		[DataMemberAttribute(Name = "MaterialId")]
		public string MaterialId { get; set; }

		[DataMemberAttribute(Name = "Name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "FolderId")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "OriginUrl")]
		public string OriginUrl { get; set; }

		[DataMemberAttribute(Name = "ByteUrl")]
		public string ByteUrl { get; set; }

		[DataMemberAttribute(Name = "AuditStatus")]
		public int? AuditStatus { get; set; }

		[DataMemberAttribute(Name = "IsNew")]
		public bool? IsNew { get; set; }

	}
}
