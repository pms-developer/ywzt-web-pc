using System.Runtime.Serialization;

namespace Dop.Api.MaterialBatchUploadImageSync.Param
{
	public class MaterialsItem
	{
		[DataMemberAttribute(Name = "request_id")]
		public string RequestId { get; set; }

		[DataMemberAttribute(Name = "folder_id")]
		public string FolderId { get; set; }

		[DataMemberAttribute(Name = "material_type")]
		public string MaterialType { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "url")]
		public string Url { get; set; }

	}
}
