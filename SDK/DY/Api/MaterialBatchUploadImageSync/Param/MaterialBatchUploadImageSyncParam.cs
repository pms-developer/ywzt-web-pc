using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dop.Api.MaterialBatchUploadImageSync.Param
{
	public class MaterialBatchUploadImageSyncParam
	{
		[DataMemberAttribute(Name = "materials")]
		public List<MaterialsItem> Materials { get; set; }

		[DataMemberAttribute(Name = "need_distinct")]
		public bool? NeedDistinct { get; set; }

	}
}
