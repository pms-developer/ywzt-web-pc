using System;
using Dop.Core;
using Dop.Api.MaterialBatchUploadImageSync.Param;

namespace Dop.Api.MaterialBatchUploadImageSync
{
	public class MaterialBatchUploadImageSyncRequest : DoudianOpApiRequest<MaterialBatchUploadImageSyncParam>
	{
		public override string GetUrlPath()
		{
			return "/material/batchUploadImageSync";
		}

		public override Type GetResponseType()
		{
			return typeof(MaterialBatchUploadImageSyncResponse);
		}

		public  MaterialBatchUploadImageSyncParam BuildParam()
		{
			return Param ?? (Param = new MaterialBatchUploadImageSyncParam());
		}

	}
}
