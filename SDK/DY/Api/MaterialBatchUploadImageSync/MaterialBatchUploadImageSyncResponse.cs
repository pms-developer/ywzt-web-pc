using Dop.Core;
using Dop.Api.MaterialBatchUploadImageSync.Data;

namespace Dop.Api.MaterialBatchUploadImageSync
{
	public class MaterialBatchUploadImageSyncResponse : DoudianOpApiResponse<MaterialBatchUploadImageSyncData>
	{
	}
}
