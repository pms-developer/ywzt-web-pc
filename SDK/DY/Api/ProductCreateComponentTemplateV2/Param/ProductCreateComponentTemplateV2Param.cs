using System.Runtime.Serialization;

namespace Dop.Api.ProductCreateComponentTemplateV2.Param
{
	public class ProductCreateComponentTemplateV2Param
	{
		[DataMemberAttribute(Name = "template_type")]
		public string TemplateType { get; set; }

		[DataMemberAttribute(Name = "template_sub_type")]
		public string TemplateSubType { get; set; }

		[DataMemberAttribute(Name = "template_name")]
		public string TemplateName { get; set; }

		[DataMemberAttribute(Name = "component_front_data")]
		public string ComponentFrontData { get; set; }

		[DataMemberAttribute(Name = "shareable")]
		public bool? Shareable { get; set; }

		[DataMemberAttribute(Name = "category_id")]
		public long? CategoryId { get; set; }

	}
}
