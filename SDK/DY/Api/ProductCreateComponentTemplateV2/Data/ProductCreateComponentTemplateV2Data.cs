using System.Runtime.Serialization;

namespace Dop.Api.ProductCreateComponentTemplateV2.Data
{
	public class ProductCreateComponentTemplateV2Data
	{
		[DataMemberAttribute(Name = "template_id")]
		public long? TemplateId { get; set; }

	}
}
