using Dop.Core;
using Dop.Api.ProductCreateComponentTemplateV2.Data;

namespace Dop.Api.ProductCreateComponentTemplateV2
{
	public class ProductCreateComponentTemplateV2Response : DoudianOpApiResponse<ProductCreateComponentTemplateV2Data>
	{
	}
}
