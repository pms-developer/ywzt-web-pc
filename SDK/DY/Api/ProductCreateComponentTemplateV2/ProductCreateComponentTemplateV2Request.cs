using System;
using Dop.Core;
using Dop.Api.ProductCreateComponentTemplateV2.Param;

namespace Dop.Api.ProductCreateComponentTemplateV2
{
	public class ProductCreateComponentTemplateV2Request : DoudianOpApiRequest<ProductCreateComponentTemplateV2Param>
	{
		public override string GetUrlPath()
		{
			return "/product/createComponentTemplateV2";
		}

		public override Type GetResponseType()
		{
			return typeof(ProductCreateComponentTemplateV2Response);
		}

		public  ProductCreateComponentTemplateV2Param BuildParam()
		{
			return Param ?? (Param = new ProductCreateComponentTemplateV2Param());
		}

	}
}
