using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreate.Data
{
	public class WarehouseCreateData
	{
		[DataMemberAttribute(Name = "data")]
		public long? Data { get; set; }

	}
}
