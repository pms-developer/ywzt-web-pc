using System;
using Dop.Core;
using Dop.Api.WarehouseCreate.Param;

namespace Dop.Api.WarehouseCreate
{
	public class WarehouseCreateRequest : DoudianOpApiRequest<WarehouseCreateParam>
	{
		public override string GetUrlPath()
		{
			return "/warehouse/create";
		}

		public override Type GetResponseType()
		{
			return typeof(WarehouseCreateResponse);
		}

		public  WarehouseCreateParam BuildParam()
		{
			return Param ?? (Param = new WarehouseCreateParam());
		}

	}
}
