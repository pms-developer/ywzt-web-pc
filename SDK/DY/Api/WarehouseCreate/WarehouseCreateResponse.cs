using Dop.Core;
using Dop.Api.WarehouseCreate.Data;

namespace Dop.Api.WarehouseCreate
{
	public class WarehouseCreateResponse : DoudianOpApiResponse<WarehouseCreateData>
	{
	}
}
