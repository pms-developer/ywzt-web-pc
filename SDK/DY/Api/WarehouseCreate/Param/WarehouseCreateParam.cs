using System.Runtime.Serialization;

namespace Dop.Api.WarehouseCreate.Param
{
	public class WarehouseCreateParam
	{
		[DataMemberAttribute(Name = "out_warehouse_id")]
		public string OutWarehouseId { get; set; }

		[DataMemberAttribute(Name = "name")]
		public string Name { get; set; }

		[DataMemberAttribute(Name = "intro")]
		public string Intro { get; set; }

	}
}
