﻿using System;
using System.Collections.Generic;
namespace Dop.Exception
{
    public class DoudianOpException : ApplicationException
    {

        private const string MessagePattern = "Code: %d, Desc: %s, ErrInfo: %s";

        private static Dictionary<DoudianOpExcepptionCode, String> DescMap = new Dictionary<DoudianOpExcepptionCode, string>();
        static DoudianOpException(){
            DescMap.Add(DoudianOpExcepptionCode.HttpPostException, "发送http请求异常");
            DescMap.Add(DoudianOpExcepptionCode.AppKeyEmptyException, "未设置app key");
            DescMap.Add(DoudianOpExcepptionCode.AppSecretEmptyException, "未设置app secret");
            DescMap.Add(DoudianOpExcepptionCode.HttpResponseCodeNot200, "网关http返回status code非200");
            DescMap.Add(DoudianOpExcepptionCode.JsonParseError, "Json序列化异常");
            
        }

        public DoudianOpException(DoudianOpExcepptionCode code, string errInfo) : base(string.Format(MessagePattern, code, DescMap[code], errInfo))
        {
            this.Code = code;
            this.Desc = DescMap[code];
        }

        public static DoudianOpException Build(DoudianOpExcepptionCode code)
        {
            return new DoudianOpException(code, "null");
        }

        public static DoudianOpException Build(DoudianOpExcepptionCode code, string errInfo)
        {
            return new DoudianOpException(code, errInfo);
        }

        public static DoudianOpException Build(DoudianOpExcepptionCode code, System.Exception cause)
        {
            return new DoudianOpException(code, cause.Message);
        }

        public String Desc { get; set; }
        public DoudianOpExcepptionCode Code { get; set; }
    }

    public enum DoudianOpExcepptionCode
    {
        HttpPostException = 100001,
        AppKeyEmptyException = 100002,
        AppSecretEmptyException = 100003,
        HttpResponseCodeNot200 = 100004,
        JsonParseError = 100005,
    }

 

}
