using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// WlbImportThreeplOfflineConsignResponse.
    /// </summary>
    public class WlbImportThreeplOfflineConsignResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public TopResultDomain Result { get; set; }

	/// <summary>
/// TopResultDomain Data Structure.
/// </summary>
[Serializable]

public class TopResultDomain : TopObject
{
	        /// <summary>
	        /// 错误code
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_msg")]
	        public string ErrorMsg { get; set; }
	
	        /// <summary>
	        /// 发货完成后的物流单号
	        /// </summary>
	        [XmlElement("lg_order_code")]
	        public string LgOrderCode { get; set; }
	
	        /// <summary>
	        /// 错误code
	        /// </summary>
	        [XmlElement("sub_error_code")]
	        public string SubErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("sub_error_msg")]
	        public string SubErrorMsg { get; set; }
	
	        /// <summary>
	        /// 是否成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
