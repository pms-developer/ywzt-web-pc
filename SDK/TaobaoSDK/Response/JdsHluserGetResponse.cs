using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// JdsHluserGetResponse.
    /// </summary>
    public class JdsHluserGetResponse : TopResponse
    {
        /// <summary>
        /// 回流用户信息
        /// </summary>
        [XmlElement("hl_user")]
        public Top.Api.Domain.HlUserDO HlUser { get; set; }

    }
}
