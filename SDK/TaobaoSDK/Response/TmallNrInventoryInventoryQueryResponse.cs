using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrInventoryInventoryQueryResponse.
    /// </summary>
    public class TmallNrInventoryInventoryQueryResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrInventoryQueryRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrInventoryQueryRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 天猫商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public long ItemId { get; set; }
	
	        /// <summary>
	        /// 占用库存数
	        /// </summary>
	        [XmlElement("occupy_quantity")]
	        public long OccupyQuantity { get; set; }
	
	        /// <summary>
	        /// 门店物理库存
	        /// </summary>
	        [XmlElement("quantity")]
	        public long Quantity { get; set; }
	
	        /// <summary>
	        /// 可售库存数
	        /// </summary>
	        [XmlElement("sellable_quantity")]
	        public long SellableQuantity { get; set; }
	
	        /// <summary>
	        /// 天猫门店id
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 预扣库存数
	        /// </summary>
	        [XmlElement("withholding_quantity")]
	        public long WithholdingQuantity { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlArray("result_datas")]
	        [XmlArrayItem("nr_inventory_query_resp_dto")]
	        public List<NrInventoryQueryRespDtoDomain> ResultDatas { get; set; }
	
	        /// <summary>
	        /// 是否成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
