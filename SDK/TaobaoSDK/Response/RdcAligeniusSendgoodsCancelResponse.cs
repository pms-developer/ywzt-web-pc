using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// RdcAligeniusSendgoodsCancelResponse.
    /// </summary>
    public class RdcAligeniusSendgoodsCancelResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// ResultdataDomain Data Structure.
/// </summary>
[Serializable]

public class ResultdataDomain : TopObject
{
	        /// <summary>
	        /// 退款单号
	        /// </summary>
	        [XmlElement("refund_id")]
	        public long RefundId { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// 异常编码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 异常信息
	        /// </summary>
	        [XmlElement("error_info")]
	        public string ErrorInfo { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public ResultdataDomain ResultData { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
