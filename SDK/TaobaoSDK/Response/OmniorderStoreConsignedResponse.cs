using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreConsignedResponse.
    /// </summary>
    public class OmniorderStoreConsignedResponse : TopResponse
    {
        /// <summary>
        /// data
        /// </summary>
        [XmlElement("data")]
        public StoreConsignedResponseDomain Data { get; set; }

        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("err_code")]
        public string ErrCodeField { get; set; }

        /// <summary>
        /// 错误内容
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// StoreConsignedResponseDomain Data Structure.
/// </summary>
[Serializable]

public class StoreConsignedResponseDomain : TopObject
{
	        /// <summary>
	        /// gotCode
	        /// </summary>
	        [XmlElement("got_code")]
	        public string GotCode { get; set; }
	
	        /// <summary>
	        /// mailNo
	        /// </summary>
	        [XmlElement("mail_no")]
	        public string MailNo { get; set; }
	
	        /// <summary>
	        /// shortId
	        /// </summary>
	        [XmlElement("short_id")]
	        public string ShortId { get; set; }
}

    }
}
