using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderAllocatedinfoSyncResponse.
    /// </summary>
    public class OmniorderAllocatedinfoSyncResponse : TopResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("err_code")]
        public string ErrCodeField { get; set; }

        /// <summary>
        /// 错误内容
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

    }
}
