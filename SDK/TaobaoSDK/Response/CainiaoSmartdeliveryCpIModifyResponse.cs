using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliveryCpIModifyResponse.
    /// </summary>
    public class CainiaoSmartdeliveryCpIModifyResponse : TopResponse
    {
        /// <summary>
        /// 更新智能发货智选cp返回结果
        /// </summary>
        [XmlElement("modify_smart_delivery_cp_response")]
        public ModifySmartDeliveryCpResponseDomain ModifySmartDeliveryCpResponse { get; set; }

	/// <summary>
/// SmartDeliveryCpInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SmartDeliveryCpInfoDomain : TopObject
{
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
}

	/// <summary>
/// WaybillCloudPrintInfoDomain Data Structure.
/// </summary>
[Serializable]

public class WaybillCloudPrintInfoDomain : TopObject
{
	        /// <summary>
	        /// 模板信息
	        /// </summary>
	        [XmlElement("print_data")]
	        public string PrintData { get; set; }
	
	        /// <summary>
	        /// 电子面单编码
	        /// </summary>
	        [XmlElement("waybill_code")]
	        public string WaybillCode { get; set; }
}

	/// <summary>
/// ModifySmartDeliveryCpResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ModifySmartDeliveryCpResponseDomain : TopObject
{
	        /// <summary>
	        /// 更改的发货cp信息
	        /// </summary>
	        [XmlElement("modified_delivery_cp_info")]
	        public SmartDeliveryCpInfoDomain ModifiedDeliveryCpInfo { get; set; }
	
	        /// <summary>
	        /// 电子面单云打印信息
	        /// </summary>
	        [XmlElement("waybill_cloud_print_info")]
	        public WaybillCloudPrintInfoDomain WaybillCloudPrintInfo { get; set; }
}

    }
}
