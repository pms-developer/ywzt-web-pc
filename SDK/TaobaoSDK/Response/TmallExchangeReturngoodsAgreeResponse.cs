using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallExchangeReturngoodsAgreeResponse.
    /// </summary>
    public class TmallExchangeReturngoodsAgreeResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public ExchangeBaseResponseDomain Result { get; set; }

	/// <summary>
/// ExchangeDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeDomain : TopObject
{
	        /// <summary>
	        /// 正向交易单号ID
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
	
	        /// <summary>
	        /// 换货单号ID
	        /// </summary>
	        [XmlElement("dispute_id")]
	        public string DisputeId { get; set; }
	
	        /// <summary>
	        /// 换货流程最新修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 当前换货状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

	/// <summary>
/// ExchangeBaseResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeBaseResponseDomain : TopObject
{
	        /// <summary>
	        /// 换货单号基本信息
	        /// </summary>
	        [XmlElement("exchange")]
	        public ExchangeDomain Exchange { get; set; }
	
	        /// <summary>
	        /// 返回结果的描述字段
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 返回结果的状态码
	        /// </summary>
	        [XmlElement("msg_code")]
	        public string MsgCode { get; set; }
	
	        /// <summary>
	        /// 是否成功调用
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
