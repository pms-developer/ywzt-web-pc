using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallExchangeConsigngoodsResponse.
    /// </summary>
    public class TmallExchangeConsigngoodsResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public RefundBaseResponseDomain Result { get; set; }

	/// <summary>
/// ExchangeDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeDomain : TopObject
{
	        /// <summary>
	        /// bizOrderId
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
	
	        /// <summary>
	        /// disputeId
	        /// </summary>
	        [XmlElement("dispute_id")]
	        public string DisputeId { get; set; }
	
	        /// <summary>
	        /// modified
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// status
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

	/// <summary>
/// RefundBaseResponseDomain Data Structure.
/// </summary>
[Serializable]

public class RefundBaseResponseDomain : TopObject
{
	        /// <summary>
	        /// exchange
	        /// </summary>
	        [XmlElement("exchange")]
	        public ExchangeDomain Exchange { get; set; }
	
	        /// <summary>
	        /// message
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// msgCode
	        /// </summary>
	        [XmlElement("msg_code")]
	        public string MsgCode { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
