using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliveryIGetResponse.
    /// </summary>
    public class CainiaoSmartdeliveryIGetResponse : TopResponse
    {
        /// <summary>
        /// <a href="http://open.taobao.com/docs/doc.htm?treeId=319&articleId=106295&docType=1">智能发货引擎</a>结果包装类
        /// </summary>
        [XmlArray("smart_delivery_response_wrapper_list")]
        [XmlArrayItem("smart_delivery_response_wrapper")]
        public List<SmartDeliveryResponseWrapperDomain> SmartDeliveryResponseWrapperList { get; set; }

	/// <summary>
/// SubErrorInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SubErrorInfoDomain : TopObject
{
	        /// <summary>
	        /// 电子面单错误码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 电子面单错误信息
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
}

	/// <summary>
/// WaybillCloudPrintInfoDomain Data Structure.
/// </summary>
[Serializable]

public class WaybillCloudPrintInfoDomain : TopObject
{
	        /// <summary>
	        /// 模板内容,具体解释见<a href="http://open.taobao.com/doc2/detail.htm?spm=a219a.7629140.0.0.8cf9Nj&treeId=17&articleId=105085&docType=1#12">链接</a>
	        /// </summary>
	        [XmlElement("print_data")]
	        public string PrintData { get; set; }
	
	        /// <summary>
	        /// 电子面单号
	        /// </summary>
	        [XmlElement("waybill_code")]
	        public string WaybillCode { get; set; }
}

	/// <summary>
/// SmartDeliveryCpInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SmartDeliveryCpInfoDomain : TopObject
{
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 推荐原因
	        /// </summary>
	        [XmlElement("recommand_reason")]
	        public string RecommandReason { get; set; }
}

	/// <summary>
/// SmartDeliveryResponseDomain Data Structure.
/// </summary>
[Serializable]

public class SmartDeliveryResponseDomain : TopObject
{
	        /// <summary>
	        /// 请求Id
	        /// </summary>
	        [XmlElement("object_id")]
	        public string ObjectId { get; set; }
	
	        /// <summary>
	        /// <a href="http://open.taobao.com/docs/doc.htm?treeId=319&articleId=106295&docType=1">智能发货引擎</a>推荐物流公司
	        /// </summary>
	        [XmlElement("smart_delivery_cp_info")]
	        public SmartDeliveryCpInfoDomain SmartDeliveryCpInfo { get; set; }
	
	        /// <summary>
	        /// 电子面单云打印信息
	        /// </summary>
	        [XmlElement("waybill_cloud_print_info")]
	        public WaybillCloudPrintInfoDomain WaybillCloudPrintInfo { get; set; }
}

	/// <summary>
/// SmartDeliveryResponseWrapperDomain Data Structure.
/// </summary>
[Serializable]

public class SmartDeliveryResponseWrapperDomain : TopObject
{
	        /// <summary>
	        /// <a href="http://open.taobao.com/docs/doc.htm?spm=a219a.7629140.0.0.9mPrfc&treeId=319&articleId=106307&docType=1">错误码</a>
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 智能发货引擎结果
	        /// </summary>
	        [XmlElement("smart_delivery_response")]
	        public SmartDeliveryResponseDomain SmartDeliveryResponse { get; set; }
	
	        /// <summary>
	        /// 如果是电子面单错误，会返回电子面单错误信息列表。没有错误不返回
	        /// </summary>
	        [XmlArray("sub_error_info_list")]
	        [XmlArrayItem("sub_error_info")]
	        public List<SubErrorInfoDomain> SubErrorInfoList { get; set; }
	
	        /// <summary>
	        /// 是否成功
	        /// </summary>
	        [XmlElement("success")]
	        public string Success { get; set; }
}

    }
}
