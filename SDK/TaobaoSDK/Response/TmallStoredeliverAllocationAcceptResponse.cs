using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallStoredeliverAllocationAcceptResponse.
    /// </summary>
    public class TmallStoredeliverAllocationAcceptResponse : TopResponse
    {
        /// <summary>
        /// 是否执行成功
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

    }
}
