using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoEndpointLockerTopStationAddorupdateResponse.
    /// </summary>
    public class CainiaoEndpointLockerTopStationAddorupdateResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public SingleResultDomain Result { get; set; }

	/// <summary>
/// SingleResultDomain Data Structure.
/// </summary>
[Serializable]

public class SingleResultDomain : TopObject
{
	        /// <summary>
	        /// 错误编码，见白皮书
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误描述
	        /// </summary>
	        [XmlElement("error_desc")]
	        public string ErrorDesc { get; set; }
	
	        /// <summary>
	        /// 本次调用是否成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
