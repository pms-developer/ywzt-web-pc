using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// AlibabaEinvoiceCreateResultGetResponse.
    /// </summary>
    public class AlibabaEinvoiceCreateResultGetResponse : TopResponse
    {
        /// <summary>
        /// 开票返回结果数据列表
        /// </summary>
        [XmlArray("invoice_result_list")]
        [XmlArrayItem("invoice_result")]
        public List<InvoiceResultDomain> InvoiceResultList { get; set; }

	/// <summary>
/// InvoiceItemDomain Data Structure.
/// </summary>
[Serializable]

public class InvoiceItemDomain : TopObject
{
	        /// <summary>
	        /// 价税合计。(等于sumPrice和tax之和)
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 淘宝子订单号，阿里发票平台自动开票时才有
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
	
	        /// <summary>
	        /// 是否为运费行，，阿里发票平台自动开票时才有
	        /// </summary>
	        [XmlElement("is_post_fee_row")]
	        public bool IsPostFeeRow { get; set; }
	
	        /// <summary>
	        /// 发票项目名称（或商品名称）
	        /// </summary>
	        [XmlElement("item_name")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 税收分类编码
	        /// </summary>
	        [XmlElement("item_no")]
	        public string ItemNo { get; set; }
	
	        /// <summary>
	        /// 商品的外部系统id，如果有sku则取sku的outerId，否则取item的outerId，，阿里发票平台自动开票时才有
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 单价，格式：100.00(不含税)
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public string Quantity { get; set; }
	
	        /// <summary>
	        /// 发票行性质。0表示正常行，1表示折扣行，2表示被折扣行。比如充电器单价100元，折扣10元，则明细为2行，充电器行性质为2，折扣行性质为1。如果充电器没有折扣，则值应为0
	        /// </summary>
	        [XmlElement("row_type")]
	        public string RowType { get; set; }
	
	        /// <summary>
	        /// 规格型号,可选
	        /// </summary>
	        [XmlElement("specification")]
	        public string Specification { get; set; }
	
	        /// <summary>
	        /// 总价，格式：100.00(不含税)
	        /// </summary>
	        [XmlElement("sum_price")]
	        public string SumPrice { get; set; }
	
	        /// <summary>
	        /// 税额
	        /// </summary>
	        [XmlElement("tax")]
	        public string Tax { get; set; }
	
	        /// <summary>
	        /// 税率。税率只能为0或0.03或0.04或0.06或0.11或0.13或0.17
	        /// </summary>
	        [XmlElement("tax_rate")]
	        public string TaxRate { get; set; }
	
	        /// <summary>
	        /// 单位
	        /// </summary>
	        [XmlElement("unit")]
	        public string Unit { get; set; }
	
	        /// <summary>
	        /// 0税率标识，0=出口零税率，1=免税，2=不征收，3=普通零税率
	        /// </summary>
	        [XmlElement("zero_rate_flag")]
	        public string ZeroRateFlag { get; set; }
}

	/// <summary>
/// InvoiceResultDomain Data Structure.
/// </summary>
[Serializable]

public class InvoiceResultDomain : TopObject
{
	        /// <summary>
	        /// 防伪码
	        /// </summary>
	        [XmlElement("anti_fake_code")]
	        public string AntiFakeCode { get; set; }
	
	        /// <summary>
	        /// 错误编码
	        /// </summary>
	        [XmlElement("biz_error_code")]
	        public string BizErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("biz_error_msg")]
	        public string BizErrorMsg { get; set; }
	
	        /// <summary>
	        /// 发票密文，密码区的字符串
	        /// </summary>
	        [XmlElement("ciphertext")]
	        public string Ciphertext { get; set; }
	
	        /// <summary>
	        /// 税控设备编号(新版电子发票有)
	        /// </summary>
	        [XmlElement("device_no")]
	        public string DeviceNo { get; set; }
	
	        /// <summary>
	        /// erp自定义单据号
	        /// </summary>
	        [XmlElement("erp_tid")]
	        public string ErpTid { get; set; }
	
	        /// <summary>
	        /// 文件类型(pdf,jpg,png)
	        /// </summary>
	        [XmlElement("file_data_type")]
	        public string FileDataType { get; set; }
	
	        /// <summary>
	        /// 发票PDF的下载地址(仅在单个查询接口上显示，批量查询不显示)
	        /// </summary>
	        [XmlElement("file_path")]
	        public string FilePath { get; set; }
	
	        /// <summary>
	        /// 开票金额
	        /// </summary>
	        [XmlElement("invoice_amount")]
	        public string InvoiceAmount { get; set; }
	
	        /// <summary>
	        /// 发票代码
	        /// </summary>
	        [XmlElement("invoice_code")]
	        public string InvoiceCode { get; set; }
	
	        /// <summary>
	        /// 开票日期
	        /// </summary>
	        [XmlElement("invoice_date")]
	        public string InvoiceDate { get; set; }
	
	        /// <summary>
	        /// 电子发票明细，erp开票默认不返回，如果erp需要获取阿里发票平台自动开票的结果，需要先找阿里小二开通权限
	        /// </summary>
	        [XmlArray("invoice_items")]
	        [XmlArrayItem("invoice_item")]
	        public List<InvoiceItemDomain> InvoiceItems { get; set; }
	
	        /// <summary>
	        /// 发票种类，0=电子发票，1=纸质发票，2=纸质专票
	        /// </summary>
	        [XmlElement("invoice_kind")]
	        public long InvoiceKind { get; set; }
	
	        /// <summary>
	        /// 发票号码
	        /// </summary>
	        [XmlElement("invoice_no")]
	        public string InvoiceNo { get; set; }
	
	        /// <summary>
	        /// 开票时间，时分秒格式（注意：2019-04-11之后开具的发票才返回）
	        /// </summary>
	        [XmlElement("invoice_time")]
	        public string InvoiceTime { get; set; }
	
	        /// <summary>
	        /// 发票类型，blue=蓝票，red=红票
	        /// </summary>
	        [XmlElement("invoice_type")]
	        public string InvoiceType { get; set; }
	
	        /// <summary>
	        /// 原蓝票发票代码，invoiceType=red时有值
	        /// </summary>
	        [XmlElement("normal_invoice_code")]
	        public string NormalInvoiceCode { get; set; }
	
	        /// <summary>
	        /// 原蓝票发票号码，invoiceType=red时有值
	        /// </summary>
	        [XmlElement("normal_invoice_no")]
	        public string NormalInvoiceNo { get; set; }
	
	        /// <summary>
	        /// 复核人，erp开票不返回，用来erp获取自动开票结果
	        /// </summary>
	        [XmlElement("payee_checker")]
	        public string PayeeChecker { get; set; }
	
	        /// <summary>
	        /// 开票人，erp开票不返回，用来erp获取自动开票结果
	        /// </summary>
	        [XmlElement("payee_operator")]
	        public string PayeeOperator { get; set; }
	
	        /// <summary>
	        /// 收款人，erp开票不返回，用来erp获取自动开票结果
	        /// </summary>
	        [XmlElement("payee_receiver")]
	        public string PayeeReceiver { get; set; }
	
	        /// <summary>
	        /// 销售方税号
	        /// </summary>
	        [XmlElement("payee_register_no")]
	        public string PayeeRegisterNo { get; set; }
	
	        /// <summary>
	        /// 购买方企业地址，erp开票不返回，用来erp获取自动开票结果
	        /// </summary>
	        [XmlElement("payer_address")]
	        public string PayerAddress { get; set; }
	
	        /// <summary>
	        /// 购买方企业银行及账号，erp开票不返回，用来erp获取自动开票结果
	        /// </summary>
	        [XmlElement("payer_bankaccount")]
	        public string PayerBankaccount { get; set; }
	
	        /// <summary>
	        /// 购买方抬头，erp开票不返回，用来erp获取自动开票结果
	        /// </summary>
	        [XmlElement("payer_name")]
	        public string PayerName { get; set; }
	
	        /// <summary>
	        /// 购买方企业电话，erp开票不返回，用来erp获取自动开票结果
	        /// </summary>
	        [XmlElement("payer_phone")]
	        public string PayerPhone { get; set; }
	
	        /// <summary>
	        /// 购买方税号，erp开票不返回，用来erp获取自动开票结果
	        /// </summary>
	        [XmlElement("payer_register_no")]
	        public string PayerRegisterNo { get; set; }
	
	        /// <summary>
	        /// 电商平台代码。淘宝：taobao，天猫：tmall
	        /// </summary>
	        [XmlElement("platform_code")]
	        public string PlatformCode { get; set; }
	
	        /// <summary>
	        /// 电商平台订单号
	        /// </summary>
	        [XmlElement("platform_tid")]
	        public string PlatformTid { get; set; }
	
	        /// <summary>
	        /// 开票流水号，唯一标志开票请求。如果两次请求流水号相同，则表示重复请求。
	        /// </summary>
	        [XmlElement("serial_no")]
	        public string SerialNo { get; set; }
	
	        /// <summary>
	        /// 开票状态 (waiting = 开票中) 、(create_success = 开票成功)、(create_failed = 开票失败)
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

    }
}
