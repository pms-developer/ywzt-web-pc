using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrOrderCancelResponse.
    /// </summary>
    public class TmallNrOrderCancelResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrCloseOrderRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrCloseOrderRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 线下门店订单号，即入参的线下门店交易单号
	        /// </summary>
	        [XmlElement("store_order_id")]
	        public string StoreOrderId { get; set; }
	
	        /// <summary>
	        /// 淘系交易订单号
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrCloseOrderRespDtoDomain ResultData { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
