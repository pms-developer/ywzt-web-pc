using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// RdcAligeniusAccountValidateResponse.
    /// </summary>
    public class RdcAligeniusAccountValidateResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// ResultdataDomain Data Structure.
/// </summary>
[Serializable]

public class ResultdataDomain : TopObject
{
	        /// <summary>
	        /// 1=是AG用户，0=非AG用户
	        /// </summary>
	        [XmlElement("ag_account")]
	        public long AgAccount { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorInfo
	        /// </summary>
	        [XmlElement("error_info")]
	        public string ErrorInfo { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public ResultdataDomain ResultData { get; set; }
	
	        /// <summary>
	        /// 为true时才有resultData
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
