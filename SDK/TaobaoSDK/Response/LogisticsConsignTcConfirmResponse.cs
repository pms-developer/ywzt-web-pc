using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// LogisticsConsignTcConfirmResponse.
    /// </summary>
    public class LogisticsConsignTcConfirmResponse : TopResponse
    {
        /// <summary>
        /// 菜鸟发货单据
        /// </summary>
        [XmlElement("order_consign_code")]
        public string OrderConsignCode { get; set; }

        /// <summary>
        /// 是否重试
        /// </summary>
        [XmlElement("retry")]
        public bool Retry { get; set; }

        /// <summary>
        /// 单次调用流程唯一id
        /// </summary>
        [XmlElement("trace_id")]
        public string TraceId { get; set; }

    }
}
