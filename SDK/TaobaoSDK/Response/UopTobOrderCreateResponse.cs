using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// UopTobOrderCreateResponse.
    /// </summary>
    public class UopTobOrderCreateResponse : TopResponse
    {
        /// <summary>
        /// 订单
        /// </summary>
        [XmlArray("delivery_orders")]
        [XmlArrayItem("deliveryorder")]
        public List<DeliveryorderDomain> DeliveryOrders { get; set; }

        /// <summary>
        /// flag
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// message
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// OrderlineDomain Data Structure.
/// </summary>
[Serializable]

public class OrderlineDomain : TopObject
{
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 商品编码
	        /// </summary>
	        [XmlElement("item_code")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 订单行号
	        /// </summary>
	        [XmlElement("order_line_no")]
	        public string OrderLineNo { get; set; }
}

	/// <summary>
/// DeliveryorderDomain Data Structure.
/// </summary>
[Serializable]

public class DeliveryorderDomain : TopObject
{
	        /// <summary>
	        /// 物流单号
	        /// </summary>
	        [XmlElement("cn_order_code")]
	        public string CnOrderCode { get; set; }
	
	        /// <summary>
	        /// 创建时间
	        /// </summary>
	        [XmlElement("create_time")]
	        public string CreateTime { get; set; }
	
	        /// <summary>
	        /// 订单行
	        /// </summary>
	        [XmlArray("order_lines")]
	        [XmlArrayItem("orderline")]
	        public List<OrderlineDomain> OrderLines { get; set; }
	
	        /// <summary>
	        /// 仓库编码
	        /// </summary>
	        [XmlElement("warehouse_code")]
	        public string WarehouseCode { get; set; }
}

    }
}
