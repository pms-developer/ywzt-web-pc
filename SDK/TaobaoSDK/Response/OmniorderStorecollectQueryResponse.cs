using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStorecollectQueryResponse.
    /// </summary>
    public class OmniorderStorecollectQueryResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// StoreCollectQueryOrderResponseDomain Data Structure.
/// </summary>
[Serializable]

public class StoreCollectQueryOrderResponseDomain : TopObject
{
	        /// <summary>
	        /// 主订单ID
	        /// </summary>
	        [XmlElement("main_order_id")]
	        public long MainOrderId { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// data
	        /// </summary>
	        [XmlElement("data")]
	        public StoreCollectQueryOrderResponseDomain Data { get; set; }
	
	        /// <summary>
	        /// 0表示码可用，其余值表示码不可用
	        /// </summary>
	        [XmlElement("err_code")]
	        public string ErrCode { get; set; }
	
	        /// <summary>
	        /// 码状态附加信息，若码可用则此处为空
	        /// </summary>
	        [XmlElement("err_msg")]
	        public string ErrMsg { get; set; }
}

    }
}
