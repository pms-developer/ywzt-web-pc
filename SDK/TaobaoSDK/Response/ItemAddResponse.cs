using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// ItemAddResponse.
    /// </summary>
    public class ItemAddResponse : TopResponse
    {
        /// <summary>
        /// 商品结构,仅有numIid和created返回
        /// </summary>
        [XmlElement("item")]
        public ItemDomain Item { get; set; }

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain : TopObject
{
	        /// <summary>
	        /// Item的发布时间，目前仅供taobao.item.add和taobao.item.get可用
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 商品iid
	        /// </summary>
	        [XmlElement("iid")]
	        public string Iid { get; set; }
	
	        /// <summary>
	        /// 商品数字id
	        /// </summary>
	        [XmlElement("num_iid")]
	        public long NumIid { get; set; }
}

    }
}
