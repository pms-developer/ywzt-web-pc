using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoNbaddAppointdeliverGetconsigninfoResponse.
    /// </summary>
    public class CainiaoNbaddAppointdeliverGetconsigninfoResponse : TopResponse
    {
        /// <summary>
        /// 调用是否成功，正常情况下都会成功
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 发货信息
        /// </summary>
        [XmlElement("result")]
        public ConsignSupportInfoDTODomain Result { get; set; }

        /// <summary>
        /// 错误编码
        /// </summary>
        [XmlElement("result_code")]
        public string ResultCode { get; set; }

        /// <summary>
        /// 错误描述
        /// </summary>
        [XmlElement("result_desc")]
        public string ResultDesc { get; set; }

	/// <summary>
/// ConsignSupportInfoDTODomain Data Structure.
/// </summary>
[Serializable]

public class ConsignSupportInfoDTODomain : TopObject
{
	        /// <summary>
	        /// 支持CP的code列表
	        /// </summary>
	        [XmlArray("support_cp_list")]
	        [XmlArrayItem("string")]
	        public List<string> SupportCpList { get; set; }
}

    }
}
