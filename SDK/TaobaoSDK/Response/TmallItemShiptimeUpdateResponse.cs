using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallItemShiptimeUpdateResponse.
    /// </summary>
    public class TmallItemShiptimeUpdateResponse : TopResponse
    {
        /// <summary>
        /// 被修改的商品ID
        /// </summary>
        [XmlElement("result")]
        public string Result { get; set; }

    }
}
