using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// QimenTagItemsQueryResponse.
    /// </summary>
    public class QimenTagItemsQueryResponse : TopResponse
    {
        /// <summary>
        /// flag
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// itemIds
        /// </summary>
        [XmlArray("item_ids")]
        [XmlArrayItem("number")]
        public List<long> ItemIds { get; set; }

        /// <summary>
        /// message
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// tagType
        /// </summary>
        [XmlElement("tag_type")]
        public string TagType { get; set; }

    }
}
