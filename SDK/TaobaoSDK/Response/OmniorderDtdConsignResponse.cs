using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderDtdConsignResponse.
    /// </summary>
    public class OmniorderDtdConsignResponse : TopResponse
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 错误码，为0表示成功，非0表示失败
        /// </summary>
        [XmlElement("result_code")]
        public string ResultCode { get; set; }

    }
}
