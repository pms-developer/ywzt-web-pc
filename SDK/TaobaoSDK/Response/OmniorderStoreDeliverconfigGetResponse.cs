using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreDeliverconfigGetResponse.
    /// </summary>
    public class OmniorderStoreDeliverconfigGetResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// StoreDeliverConfigDomain Data Structure.
/// </summary>
[Serializable]

public class StoreDeliverConfigDomain : TopObject
{
	        /// <summary>
	        /// 是否是活动期
	        /// </summary>
	        [XmlElement("activity")]
	        public bool Activity { get; set; }
	
	        /// <summary>
	        /// 当activity为true时返回，活动结束时间
	        /// </summary>
	        [XmlElement("activity_end_time")]
	        public string ActivityEndTime { get; set; }
	
	        /// <summary>
	        /// 当activity为true时返回,活动开始时间
	        /// </summary>
	        [XmlElement("activity_start_time")]
	        public string ActivityStartTime { get; set; }
	
	        /// <summary>
	        /// 每日接单阈值
	        /// </summary>
	        [XmlElement("deliver_threshold")]
	        public long DeliverThreshold { get; set; }
	
	        /// <summary>
	        /// 派单时间，格式为：[{"startTime":"08:40","endTime":"12:20"},{"startTime":"18:00","endTime":"22:00"}]
	        /// </summary>
	        [XmlElement("dispatch_time_range")]
	        public string DispatchTimeRange { get; set; }
	
	        /// <summary>
	        /// 优先级，取值0-10，0优先级最大，10优先级最小
	        /// </summary>
	        [XmlElement("priority")]
	        public long Priority { get; set; }
	
	        /// <summary>
	        /// 接单时间段，格式为 "09:00-12:00", "" 表示一直开启
	        /// </summary>
	        [XmlElement("working_time")]
	        public string WorkingTime { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// code
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// data
	        /// </summary>
	        [XmlElement("data")]
	        public StoreDeliverConfigDomain Data { get; set; }
	
	        /// <summary>
	        /// message
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
