using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// AlibabaEinvoiceRedCreatereqResponse.
    /// </summary>
    public class AlibabaEinvoiceRedCreatereqResponse : TopResponse
    {
        /// <summary>
        /// 是否冲红成功
        /// </summary>
        [XmlElement("is_success")]
        public string IsSuccess { get; set; }

    }
}
