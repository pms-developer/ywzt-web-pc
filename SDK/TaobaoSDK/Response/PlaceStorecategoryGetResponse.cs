using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// PlaceStorecategoryGetResponse.
    /// </summary>
    public class PlaceStorecategoryGetResponse : TopResponse
    {
        /// <summary>
        /// 门店类目格式
        /// </summary>
        [XmlElement("category_list")]
        public string CategoryList { get; set; }

    }
}
