using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// RdcAligeniusOrderEventUpdateResponse.
    /// </summary>
    public class RdcAligeniusOrderEventUpdateResponse : TopResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("fail_code")]
        public string FailCode { get; set; }

        /// <summary>
        /// 错误描述
        /// </summary>
        [XmlElement("fail_info")]
        public string FailInfo { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        [XmlElement("success_flag")]
        public bool SuccessFlag { get; set; }

    }
}
