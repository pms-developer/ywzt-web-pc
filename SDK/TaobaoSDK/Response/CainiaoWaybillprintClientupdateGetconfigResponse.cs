using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoWaybillprintClientupdateGetconfigResponse.
    /// </summary>
    public class CainiaoWaybillprintClientupdateGetconfigResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public UpdateConfigTopResultDomain Result { get; set; }

	/// <summary>
/// UpdateConfigTopResultDomain Data Structure.
/// </summary>
[Serializable]

public class UpdateConfigTopResultDomain : TopObject
{
	        /// <summary>
	        /// description
	        /// </summary>
	        [XmlElement("description")]
	        public string Description { get; set; }
	
	        /// <summary>
	        /// status
	        /// </summary>
	        [XmlElement("status")]
	        public long Status { get; set; }
}

    }
}
