using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// PlaceStoreDeleteResponse.
    /// </summary>
    public class PlaceStoreDeleteResponse : TopResponse
    {
        /// <summary>
        /// 门店删除结果
        /// </summary>
        [XmlElement("result")]
        public bool Result { get; set; }

    }
}
