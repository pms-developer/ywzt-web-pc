using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderItemTagOperateResponse.
    /// </summary>
    public class OmniorderItemTagOperateResponse : TopResponse
    {
        /// <summary>
        /// 0 正常，否则异常
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// code 不为 0时有值，代表异常信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

    }
}
