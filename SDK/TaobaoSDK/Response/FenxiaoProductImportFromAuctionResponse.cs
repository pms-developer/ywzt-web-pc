using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// FenxiaoProductImportFromAuctionResponse.
    /// </summary>
    public class FenxiaoProductImportFromAuctionResponse : TopResponse
    {
        /// <summary>
        /// 操作时间
        /// </summary>
        [XmlElement("opt_time")]
        public string OptTime { get; set; }

        /// <summary>
        /// 生成的产品id
        /// </summary>
        [XmlElement("pid")]
        public long Pid { get; set; }

    }
}
