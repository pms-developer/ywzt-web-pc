using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// QimenTradeUserAddResponse.
    /// </summary>
    public class QimenTradeUserAddResponse : TopResponse
    {
        /// <summary>
        /// appkey
        /// </summary>
        [XmlElement("appkey")]
        public string Appkey { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [XmlElement("gmt_create")]
        public string GmtCreate { get; set; }

        /// <summary>
        /// 卖家备注
        /// </summary>
        [XmlElement("memo")]
        public string Memo { get; set; }

    }
}
