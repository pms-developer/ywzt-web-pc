using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrInventoryInitialResponse.
    /// </summary>
    public class TmallNrInventoryInitialResponse : TopResponse
    {
        /// <summary>
        /// 错误编码
        /// </summary>
        [XmlElement("error_code2")]
        public string ErrorCode2 { get; set; }

        /// <summary>
        /// 错误信息提示
        /// </summary>
        [XmlElement("error_message")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// isSuccess
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 初始化成功
        /// </summary>
        [XmlElement("result_data")]
        public bool ResultData { get; set; }

    }
}
