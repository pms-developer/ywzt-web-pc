using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrFulfillCancelResponse.
    /// </summary>
    public class TmallNrFulfillCancelResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code2")]
	        public string ErrorCode2 { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 取消是否成功
	        /// </summary>
	        [XmlElement("result_data")]
	        public bool ResultData { get; set; }
	
	        /// <summary>
	        /// isSuccess
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
