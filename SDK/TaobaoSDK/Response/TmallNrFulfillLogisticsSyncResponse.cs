using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrFulfillLogisticsSyncResponse.
    /// </summary>
    public class TmallNrFulfillLogisticsSyncResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// 错误代码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误描述
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 成功失败标识,true:成功，false:失败
	        /// </summary>
	        [XmlElement("result_data")]
	        public bool ResultData { get; set; }
}

    }
}
