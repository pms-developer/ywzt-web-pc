using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// AlibabaEinvoicePaperInvalidResponse.
    /// </summary>
    public class AlibabaEinvoicePaperInvalidResponse : TopResponse
    {
        /// <summary>
        /// 接口调用是否成功，操作结果tmc异步返回alibaba_invoice_PaperOpsReturn
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

    }
}
