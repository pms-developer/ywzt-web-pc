using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallItemSimpleschemaUpdateResponse.
    /// </summary>
    public class TmallItemSimpleschemaUpdateResponse : TopResponse
    {
        /// <summary>
        /// 编辑商品操作成功时间
        /// </summary>
        [XmlElement("gmt_modified")]
        public string GmtModified { get; set; }

        /// <summary>
        /// sku与outerId映射信息
        /// </summary>
        [XmlElement("sku_map_json")]
        public string SkuMapJson { get; set; }

        /// <summary>
        /// 编辑商品的itemid
        /// </summary>
        [XmlElement("update_item_result")]
        public string UpdateItemResult { get; set; }

    }
}
