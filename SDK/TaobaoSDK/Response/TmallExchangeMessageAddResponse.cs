using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallExchangeMessageAddResponse.
    /// </summary>
    public class TmallExchangeMessageAddResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public ResultSetDomain Result { get; set; }

	/// <summary>
/// PicUrlDomain Data Structure.
/// </summary>
[Serializable]

public class PicUrlDomain : TopObject
{
	        /// <summary>
	        /// 图片url
	        /// </summary>
	        [XmlElement("url")]
	        public string Url { get; set; }
}

	/// <summary>
/// RefundMessageDomain Data Structure.
/// </summary>
[Serializable]

public class RefundMessageDomain : TopObject
{
	        /// <summary>
	        /// 留言内容
	        /// </summary>
	        [XmlElement("content")]
	        public string Content { get; set; }
	
	        /// <summary>
	        /// 留言创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 留言ID
	        /// </summary>
	        [XmlElement("id")]
	        public long Id { get; set; }
	
	        /// <summary>
	        /// 留言类型：系统或是人工
	        /// </summary>
	        [XmlElement("message_type")]
	        public string MessageType { get; set; }
	
	        /// <summary>
	        /// 留言者ID
	        /// </summary>
	        [XmlElement("owner_id")]
	        public long OwnerId { get; set; }
	
	        /// <summary>
	        /// 留言者昵称
	        /// </summary>
	        [XmlElement("owner_nick")]
	        public string OwnerNick { get; set; }
	
	        /// <summary>
	        /// 留言者橘色
	        /// </summary>
	        [XmlElement("owner_role")]
	        public string OwnerRole { get; set; }
	
	        /// <summary>
	        /// 凭证信息
	        /// </summary>
	        [XmlArray("pic_urls")]
	        [XmlArrayItem("pic_url")]
	        public List<PicUrlDomain> PicUrls { get; set; }
	
	        /// <summary>
	        /// 换货单号ID
	        /// </summary>
	        [XmlElement("refund_id")]
	        public long RefundId { get; set; }
}

	/// <summary>
/// ResultSetDomain Data Structure.
/// </summary>
[Serializable]

public class ResultSetDomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_msg")]
	        public string ErrorMsg { get; set; }
	
	        /// <summary>
	        /// 异常信息
	        /// </summary>
	        [XmlElement("exception")]
	        public string Exception { get; set; }
	
	        /// <summary>
	        /// 留言信息
	        /// </summary>
	        [XmlArray("results")]
	        [XmlArrayItem("refund_message")]
	        public List<RefundMessageDomain> Results { get; set; }
}

    }
}
