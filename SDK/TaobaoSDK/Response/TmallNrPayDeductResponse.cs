using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrPayDeductResponse.
    /// </summary>
    public class TmallNrPayDeductResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrDeductRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrDeductRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 渠道支付单号，例如支付宝扣款的会返回支付宝的订单ID，微信支付返回微信支付订单ID，现金场景不会回传
	        /// </summary>
	        [XmlElement("channel_pay_order_id")]
	        public string ChannelPayOrderId { get; set; }
	
	        /// <summary>
	        /// 本次实际支付金额，单位为分，例如订单总金额为200元，支付宝有优惠8元，实际应付192元，本次消费者受余额所限实际仅支付150元（同时有支付宝优惠8元），则应该返回 158元(15000分)
	        /// </summary>
	        [XmlElement("current_actual_pay_fee")]
	        public long CurrentActualPayFee { get; set; }
	
	        /// <summary>
	        /// 本次应该支付金额，单位为分,（支付宝支付、微信支付场景下在支付时能够享受优惠，导致消费者应付金额可能会大于实际支付金额），例如订单总金额为200元，本次消费者应付200元，支付宝有优惠8元，实际应付192元，则应该返回200元（20000分）
	        /// </summary>
	        [XmlElement("current_need_pay_fee")]
	        public long CurrentNeedPayFee { get; set; }
	
	        /// <summary>
	        /// 本次支付中所享受的支付优惠金额，单位为分，例如本次支付宝减免了8元，则应该返回8元(800分)
	        /// </summary>
	        [XmlElement("current_pay_discount_fee")]
	        public long CurrentPayDiscountFee { get; set; }
	
	        /// <summary>
	        /// 本次支付工具，具体枚举值参见入参描述中的pay_channel
	        /// </summary>
	        [XmlElement("pay_channel")]
	        public string PayChannel { get; set; }
	
	        /// <summary>
	        /// 本次支付请求所对应的流水ID；
	        /// </summary>
	        [XmlElement("pay_order_id")]
	        public string PayOrderId { get; set; }
	
	        /// <summary>
	        /// 剩余待支付金额，单位为分，例如订单总金额为200元，支付宝优惠8元，消费者本次支付150元，则应该回传42元(4200分)。
	        /// </summary>
	        [XmlElement("remain_fee")]
	        public long RemainFee { get; set; }
	
	        /// <summary>
	        /// 请求ID
	        /// </summary>
	        [XmlElement("request_id")]
	        public string RequestId { get; set; }
	
	        /// <summary>
	        /// 支付状态，总计有三种（如果是同步支付流水没有unknown状态）： 1. success 2. fail 3. unknown
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrDeductRespDtoDomain ResultData { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
