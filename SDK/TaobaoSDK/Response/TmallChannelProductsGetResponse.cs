using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallChannelProductsGetResponse.
    /// </summary>
    public class TmallChannelProductsGetResponse : TopResponse
    {
        /// <summary>
        /// 产品对象记录集
        /// </summary>
        [XmlArray("products")]
        [XmlArrayItem("top_product_d_o")]
        public List<TopProductDODomain> Products { get; set; }

        /// <summary>
        /// 查询结果记录数
        /// </summary>
        [XmlElement("total_results")]
        public long TotalResults { get; set; }

	/// <summary>
/// ProductSkuDoDomain Data Structure.
/// </summary>
[Serializable]

public class ProductSkuDoDomain : TopObject
{
	        /// <summary>
	        /// 关联的前端宝贝skuid
	        /// </summary>
	        [XmlElement("auction_sku_id")]
	        public long AuctionSkuId { get; set; }
	
	        /// <summary>
	        /// 代销采购价:单位分
	        /// </summary>
	        [XmlElement("cost_price_fen")]
	        public long CostPriceFen { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 经销采购价:单位分
	        /// </summary>
	        [XmlElement("price_cost_dealer_fen")]
	        public long PriceCostDealerFen { get; set; }
	
	        /// <summary>
	        /// sku的销售属性组合字符串。格式:pid:vid;pid:vid,如:1627207:3232483;1630696:3284570,表示:机身颜色:军绿色;手机套餐:一电一充。
	        /// </summary>
	        [XmlElement("properties")]
	        public string Properties { get; set; }
	
	        /// <summary>
	        /// 库存
	        /// </summary>
	        [XmlElement("quantity")]
	        public long Quantity { get; set; }
	
	        /// <summary>
	        /// 配额可用库存
	        /// </summary>
	        [XmlElement("quota_quantity")]
	        public long QuotaQuantity { get; set; }
	
	        /// <summary>
	        /// 预扣库存
	        /// </summary>
	        [XmlElement("reserved_quantity")]
	        public long ReservedQuantity { get; set; }
	
	        /// <summary>
	        /// 关联的后端商品id
	        /// </summary>
	        [XmlElement("scitem_id")]
	        public long ScitemId { get; set; }
	
	        /// <summary>
	        /// skuId
	        /// </summary>
	        [XmlElement("sku_id")]
	        public long SkuId { get; set; }
	
	        /// <summary>
	        /// 市场价单位分
	        /// </summary>
	        [XmlElement("standard_price_fen")]
	        public long StandardPriceFen { get; set; }
}

	/// <summary>
/// TopProductDODomain Data Structure.
/// </summary>
[Serializable]

public class TopProductDODomain : TopObject
{
	        /// <summary>
	        /// 关联的前台宝贝id
	        /// </summary>
	        [XmlElement("auction_id")]
	        public long AuctionId { get; set; }
	
	        /// <summary>
	        /// 所在地：市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 代销采购价格，单位：元。
	        /// </summary>
	        [XmlElement("cost_price_yuan")]
	        public string CostPriceYuan { get; set; }
	
	        /// <summary>
	        /// 创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 产品描述路径，通过http请求获取
	        /// </summary>
	        [XmlElement("desc_path")]
	        public string DescPath { get; set; }
	
	        /// <summary>
	        /// 是否有发票，可选值：false（否）、true（是）
	        /// </summary>
	        [XmlElement("have_invoice")]
	        public long HaveInvoice { get; set; }
	
	        /// <summary>
	        /// 是否有保修，可选值：false（否）、true（是）
	        /// </summary>
	        [XmlElement("have_quarantee")]
	        public long HaveQuarantee { get; set; }
	
	        /// <summary>
	        /// 下载人数
	        /// </summary>
	        [XmlElement("items_count")]
	        public long ItemsCount { get; set; }
	
	        /// <summary>
	        /// 更新时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 产品名称
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 累计采购次数
	        /// </summary>
	        [XmlElement("orders_count")]
	        public long OrdersCount { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 产品图片路径列表，用“,”分隔
	        /// </summary>
	        [XmlElement("pictures")]
	        public string Pictures { get; set; }
	
	        /// <summary>
	        /// 产品ID
	        /// </summary>
	        [XmlElement("pid")]
	        public long Pid { get; set; }
	
	        /// <summary>
	        /// ems费用，单位：元
	        /// </summary>
	        [XmlElement("postage_ems")]
	        public string PostageEms { get; set; }
	
	        /// <summary>
	        /// 快递费用，单位：元
	        /// </summary>
	        [XmlElement("postage_fast")]
	        public string PostageFast { get; set; }
	
	        /// <summary>
	        /// 运费模板ID
	        /// </summary>
	        [XmlElement("postage_id")]
	        public long PostageId { get; set; }
	
	        /// <summary>
	        /// 平邮费用，单位：元
	        /// </summary>
	        [XmlElement("postage_ordinary")]
	        public string PostageOrdinary { get; set; }
	
	        /// <summary>
	        /// 运费类型：1（供应商承担运费）、2（分销商承担运费）可选值：seller（供应商承担运费）、buyer（分销商承担运费）
	        /// </summary>
	        [XmlElement("postage_type")]
	        public long PostageType { get; set; }
	
	        /// <summary>
	        /// 产品所属产品线id
	        /// </summary>
	        [XmlElement("product_line_id")]
	        public long ProductLineId { get; set; }
	
	        /// <summary>
	        /// 所在地：省
	        /// </summary>
	        [XmlElement("prov")]
	        public string Prov { get; set; }
	
	        /// <summary>
	        /// 产品库存
	        /// </summary>
	        [XmlElement("quantity")]
	        public long Quantity { get; set; }
	
	        /// <summary>
	        /// 最高零售价，单位：元。
	        /// </summary>
	        [XmlElement("retail_price_high")]
	        public string RetailPriceHigh { get; set; }
	
	        /// <summary>
	        /// 最低零售价，单位：元。
	        /// </summary>
	        [XmlElement("retail_price_low")]
	        public string RetailPriceLow { get; set; }
	
	        /// <summary>
	        /// scItemId
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public long ScItemId { get; set; }
	
	        /// <summary>
	        /// 分销产品SKU列表
	        /// </summary>
	        [XmlArray("skus")]
	        [XmlArrayItem("product_sku_do")]
	        public List<ProductSkuDoDomain> Skus { get; set; }
	
	        /// <summary>
	        /// spuId
	        /// </summary>
	        [XmlElement("spu_id")]
	        public long SpuId { get; set; }
	
	        /// <summary>
	        /// 市场价：单位元
	        /// </summary>
	        [XmlElement("standard_price")]
	        public string StandardPrice { get; set; }
	
	        /// <summary>
	        /// 发布状态： 1 上架，2 下架，3 删除
	        /// </summary>
	        [XmlElement("status")]
	        public long Status { get; set; }
}

    }
}
