using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallExchangeGetResponse.
    /// </summary>
    public class TmallExchangeGetResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public ExchangeBaseResponseDomain Result { get; set; }

	/// <summary>
/// ExchangeDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeDomain : TopObject
{
	        /// <summary>
	        /// 卖家换货地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 先行垫付状态
	        /// </summary>
	        [XmlElement("advance_status")]
	        public long AdvanceStatus { get; set; }
	
	        /// <summary>
	        /// 支付宝单号ID
	        /// </summary>
	        [XmlElement("alipay_no")]
	        public string AlipayNo { get; set; }
	
	        /// <summary>
	        /// attributes
	        /// </summary>
	        [XmlElement("attributes")]
	        public string Attributes { get; set; }
	
	        /// <summary>
	        /// 正向订单号ID
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
	
	        /// <summary>
	        /// 所购买的商品sku
	        /// </summary>
	        [XmlElement("bought_sku")]
	        public string BoughtSku { get; set; }
	
	        /// <summary>
	        /// buyerAddress
	        /// </summary>
	        [XmlElement("buyer_address")]
	        public string BuyerAddress { get; set; }
	
	        /// <summary>
	        /// 买家发货物流公司名称
	        /// </summary>
	        [XmlElement("buyer_logistic_name")]
	        public string BuyerLogisticName { get; set; }
	
	        /// <summary>
	        /// 买家发货物流单号
	        /// </summary>
	        [XmlElement("buyer_logistic_no")]
	        public string BuyerLogisticNo { get; set; }
	
	        /// <summary>
	        /// buyerName
	        /// </summary>
	        [XmlElement("buyer_name")]
	        public string BuyerName { get; set; }
	
	        /// <summary>
	        /// 买家昵称
	        /// </summary>
	        [XmlElement("buyer_nick")]
	        public string BuyerNick { get; set; }
	
	        /// <summary>
	        /// 买家联系方式
	        /// </summary>
	        [XmlElement("buyer_phone")]
	        public string BuyerPhone { get; set; }
	
	        /// <summary>
	        /// 换货单创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 小二托管状态
	        /// </summary>
	        [XmlElement("cs_status")]
	        public long CsStatus { get; set; }
	
	        /// <summary>
	        /// 换货理由说明
	        /// </summary>
	        [XmlElement("desc")]
	        public string Desc { get; set; }
	
	        /// <summary>
	        /// 换货单号ID
	        /// </summary>
	        [XmlElement("dispute_id")]
	        public string DisputeId { get; set; }
	
	        /// <summary>
	        /// 换货商品的sku
	        /// </summary>
	        [XmlElement("exchange_sku")]
	        public string ExchangeSku { get; set; }
	
	        /// <summary>
	        /// 商品状态
	        /// </summary>
	        [XmlElement("good_status")]
	        public string GoodStatus { get; set; }
	
	        /// <summary>
	        /// 换货单修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 换货数量
	        /// </summary>
	        [XmlElement("num")]
	        public long Num { get; set; }
	
	        /// <summary>
	        /// 操作场景
	        /// </summary>
	        [XmlElement("operation_contraint")]
	        public string OperationContraint { get; set; }
	
	        /// <summary>
	        /// 价格
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 换货申请理由
	        /// </summary>
	        [XmlElement("reason")]
	        public string Reason { get; set; }
	
	        /// <summary>
	        /// 申请换货的状态：售中换货 or 售后换货
	        /// </summary>
	        [XmlElement("refund_phase")]
	        public string RefundPhase { get; set; }
	
	        /// <summary>
	        /// 换货版本
	        /// </summary>
	        [XmlElement("refund_version")]
	        public long RefundVersion { get; set; }
	
	        /// <summary>
	        /// 卖家发货物流公司名称
	        /// </summary>
	        [XmlElement("seller_logistic_name")]
	        public string SellerLogisticName { get; set; }
	
	        /// <summary>
	        /// 卖家发货快递单号
	        /// </summary>
	        [XmlElement("seller_logistic_no")]
	        public string SellerLogisticNo { get; set; }
	
	        /// <summary>
	        /// 卖家昵称
	        /// </summary>
	        [XmlElement("seller_nick")]
	        public string SellerNick { get; set; }
	
	        /// <summary>
	        /// 换货状态，具体包括：换货待处理(1), 待买家退货(2), 买家已退货，待收货(3),  换货关闭(4), 换货成功(5), 待买家修改(6), 待发出换货商品(12), 待买家收货(13), 请退款(14)
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 超时时间
	        /// </summary>
	        [XmlElement("time_out")]
	        public string TimeOut { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
}

	/// <summary>
/// ExchangeBaseResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeBaseResponseDomain : TopObject
{
	        /// <summary>
	        /// 换货单详情
	        /// </summary>
	        [XmlElement("exchange")]
	        public ExchangeDomain Exchange { get; set; }
	
	        /// <summary>
	        /// 错误信息说明
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("msg_code")]
	        public string MsgCode { get; set; }
	
	        /// <summary>
	        /// 返回结果是否符合要求
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
