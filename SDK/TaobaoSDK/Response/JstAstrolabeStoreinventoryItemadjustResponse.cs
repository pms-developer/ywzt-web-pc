using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// JstAstrolabeStoreinventoryItemadjustResponse.
    /// </summary>
    public class JstAstrolabeStoreinventoryItemadjustResponse : TopResponse
    {
        /// <summary>
        /// 响应标签
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应编码
        /// </summary>
        [XmlElement("qimen_code")]
        public string QimenCode { get; set; }

    }
}
