using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// LogisticsExpressModifyAppointResponse.
    /// </summary>
    public class LogisticsExpressModifyAppointResponse : TopResponse
    {
        /// <summary>
        /// 调用结果
        /// </summary>
        [XmlElement("result")]
        public SingleResultDtoDomain Result { get; set; }

	/// <summary>
/// ExpressModifyAppointTopResponseDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ExpressModifyAppointTopResponseDtoDomain : TopObject
{
	        /// <summary>
	        /// 订单号
	        /// </summary>
	        [XmlElement("order_code")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// 是否执行成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

	/// <summary>
/// SingleResultDtoDomain Data Structure.
/// </summary>
[Serializable]

public class SingleResultDtoDomain : TopObject
{
	        /// <summary>
	        /// 错误编码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误描述
	        /// </summary>
	        [XmlElement("error_desc")]
	        public string ErrorDesc { get; set; }
	
	        /// <summary>
	        /// 是否幂等
	        /// </summary>
	        [XmlElement("is_idempotent")]
	        public bool IsIdempotent { get; set; }
	
	        /// <summary>
	        /// 是否需要重试
	        /// </summary>
	        [XmlElement("is_retry")]
	        public bool IsRetry { get; set; }
	
	        /// <summary>
	        /// 业务返回结果
	        /// </summary>
	        [XmlElement("result")]
	        public ExpressModifyAppointTopResponseDtoDomain Result { get; set; }
	
	        /// <summary>
	        /// 是否调用成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
	
	        /// <summary>
	        /// 调用码
	        /// </summary>
	        [XmlElement("trace_id")]
	        public string TraceId { get; set; }
}

    }
}
