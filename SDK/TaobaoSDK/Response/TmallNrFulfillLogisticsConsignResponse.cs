using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrFulfillLogisticsConsignResponse.
    /// </summary>
    public class TmallNrFulfillLogisticsConsignResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrStoreGoodsReadyRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrStoreGoodsReadyRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 取消码
	        /// </summary>
	        [XmlElement("cancel_code")]
	        public string CancelCode { get; set; }
	
	        /// <summary>
	        /// 发货公司
	        /// </summary>
	        [XmlElement("company_name")]
	        public string CompanyName { get; set; }
	
	        /// <summary>
	        /// 发货单号
	        /// </summary>
	        [XmlElement("company_order_no")]
	        public string CompanyOrderNo { get; set; }
	
	        /// <summary>
	        /// 取件码
	        /// </summary>
	        [XmlElement("got_code")]
	        public string GotCode { get; set; }
	
	        /// <summary>
	        /// 核销码
	        /// </summary>
	        [XmlElement("ma_code")]
	        public string MaCode { get; set; }
	
	        /// <summary>
	        /// 主订单号
	        /// </summary>
	        [XmlElement("main_order_id")]
	        public long MainOrderId { get; set; }
	
	        /// <summary>
	        /// 根据门店+sellerId按日期生成从1到N的数据
	        /// </summary>
	        [XmlElement("short_id")]
	        public string ShortId { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// isSuccess
	        /// </summary>
	        [XmlElement("is_success")]
	        public bool IsSuccess { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrStoreGoodsReadyRespDtoDomain ResultData { get; set; }
}

    }
}
