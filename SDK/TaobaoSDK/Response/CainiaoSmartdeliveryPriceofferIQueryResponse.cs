using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliveryPriceofferIQueryResponse.
    /// </summary>
    public class CainiaoSmartdeliveryPriceofferIQueryResponse : TopResponse
    {
        /// <summary>
        /// 返回结果列表
        /// </summary>
        [XmlArray("cp_price_info_list")]
        [XmlArrayItem("cp_price_info")]
        public List<CpPriceInfoDomain> CpPriceInfoList { get; set; }

	/// <summary>
/// AddressDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// CostPriceDomain Data Structure.
/// </summary>
[Serializable]

public class CostPriceDomain : TopObject
{
	        /// <summary>
	        /// 续重价格，单位分
	        /// </summary>
	        [XmlElement("continus_measure_price")]
	        public long ContinusMeasurePrice { get; set; }
	
	        /// <summary>
	        /// 续重，单位g
	        /// </summary>
	        [XmlElement("continus_measure_weight")]
	        public long ContinusMeasureWeight { get; set; }
	
	        /// <summary>
	        /// 首重价格，单位分
	        /// </summary>
	        [XmlElement("first_measure_price")]
	        public long FirstMeasurePrice { get; set; }
	
	        /// <summary>
	        /// 续重，单位g
	        /// </summary>
	        [XmlElement("first_measure_weight")]
	        public long FirstMeasureWeight { get; set; }
}

	/// <summary>
/// WeightRangeDomain Data Structure.
/// </summary>
[Serializable]

public class WeightRangeDomain : TopObject
{
	        /// <summary>
	        /// 最小下线重量，单位g
	        /// </summary>
	        [XmlElement("lower_limit_weight")]
	        public long LowerLimitWeight { get; set; }
	
	        /// <summary>
	        /// 最大上线重量,单位g
	        /// </summary>
	        [XmlElement("upper_limit_weight")]
	        public long UpperLimitWeight { get; set; }
}

	/// <summary>
/// PriceRuleDomain Data Structure.
/// </summary>
[Serializable]

public class PriceRuleDomain : TopObject
{
	        /// <summary>
	        /// 成本价格
	        /// </summary>
	        [XmlElement("cost_price")]
	        public CostPriceDomain CostPrice { get; set; }
	
	        /// <summary>
	        /// 适用重量范围
	        /// </summary>
	        [XmlElement("weight_range")]
	        public WeightRangeDomain WeightRange { get; set; }
}

	/// <summary>
/// AddressAreaDomain Data Structure.
/// </summary>
[Serializable]

public class AddressAreaDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 对于发往全国的统一价格，返回该字段，表明记录是统一发往全国
	        /// </summary>
	        [XmlElement("country")]
	        public string Country { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
}

	/// <summary>
/// PriceOfferDomain Data Structure.
/// </summary>
[Serializable]

public class PriceOfferDomain : TopObject
{
	        /// <summary>
	        /// 报价规则
	        /// </summary>
	        [XmlArray("price_rule_list")]
	        [XmlArrayItem("price_rule")]
	        public List<PriceRuleDomain> PriceRuleList { get; set; }
	
	        /// <summary>
	        /// 到货区
	        /// </summary>
	        [XmlElement("receive_area")]
	        public AddressAreaDomain ReceiveArea { get; set; }
}

	/// <summary>
/// CpPriceInfoDomain Data Structure.
/// </summary>
[Serializable]

public class CpPriceInfoDomain : TopObject
{
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 报价列表
	        /// </summary>
	        [XmlArray("price_offer_list")]
	        [XmlArrayItem("price_offer")]
	        public List<PriceOfferDomain> PriceOfferList { get; set; }
	
	        /// <summary>
	        /// 发货地址
	        /// </summary>
	        [XmlElement("send_address")]
	        public AddressDomain SendAddress { get; set; }
	
	        /// <summary>
	        /// 仓id
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public long WarehouseId { get; set; }
}

    }
}
