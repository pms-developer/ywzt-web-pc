using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// JstAstrolabeStoreinventoryQueryResponse.
    /// </summary>
    public class JstAstrolabeStoreinventoryQueryResponse : TopResponse
    {
        /// <summary>
        /// 响应标示
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应编码
        /// </summary>
        [XmlElement("result_code")]
        public string ResultCode { get; set; }

        /// <summary>
        /// 门店列表
        /// </summary>
        [XmlArray("stores")]
        [XmlArrayItem("store")]
        public List<StoreDomain> Stores { get; set; }

	/// <summary>
/// QuantityDetailDomain Data Structure.
/// </summary>
[Serializable]

public class QuantityDetailDomain : TopObject
{
	        /// <summary>
	        /// 库存类型
	        /// </summary>
	        [XmlElement("inventory_type")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 当前类型库存数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public string Quantity { get; set; }
}

	/// <summary>
/// StoreInventoryDomain Data Structure.
/// </summary>
[Serializable]

public class StoreInventoryDomain : TopObject
{
	        /// <summary>
	        /// ISV系统中商品编码
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 库存量详情
	        /// </summary>
	        [XmlArray("quantity_details")]
	        [XmlArrayItem("quantity_detail")]
	        public List<QuantityDetailDomain> QuantityDetails { get; set; }
	
	        /// <summary>
	        /// 淘宝后端商品id
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public string ScItemId { get; set; }
}

	/// <summary>
/// StoreDomain Data Structure.
/// </summary>
[Serializable]

public class StoreDomain : TopObject
{
	        /// <summary>
	        /// 门店库存信息
	        /// </summary>
	        [XmlArray("store_inventories")]
	        [XmlArrayItem("store_inventory")]
	        public List<StoreInventoryDomain> StoreInventories { get; set; }
	
	        /// <summary>
	        /// 门店ID(商户中心) 或 电商仓ID
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public string WarehouseId { get; set; }
	
	        /// <summary>
	        /// 库存来源
	        /// </summary>
	        [XmlElement("warehouse_type")]
	        public string WarehouseType { get; set; }
}

    }
}
