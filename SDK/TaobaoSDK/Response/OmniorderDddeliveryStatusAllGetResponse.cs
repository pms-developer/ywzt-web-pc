using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderDddeliveryStatusAllGetResponse.
    /// </summary>
    public class OmniorderDddeliveryStatusAllGetResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// code
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 包括运单的当前状态和历史状态列表，运单状态各字段含义如下：deliveryOrderNo: 运单号, tradeId: 订单号, logisticsStatus: 运单状态, logisticsStatusName: 运单状态中文名称, spName: 配送商信息, delivererName: 配送员信息, delivererPhone: 配送员电话, delivererLocation: 配送员位置, failReason: 运单失败原因, failCode: 运单失败编码, logisticsTime: 运单状态状态产生时间
	        /// </summary>
	        [XmlElement("data")]
	        public string Data { get; set; }
	
	        /// <summary>
	        /// message
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
