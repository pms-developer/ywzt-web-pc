using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrItemTagOpsResponse.
    /// </summary>
    public class TmallNrItemTagOpsResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public NewRetailResultDomain Result { get; set; }

	/// <summary>
/// TagRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TagRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 失败描述
	        /// </summary>
	        [XmlArray("descs")]
	        [XmlArrayItem("string")]
	        public List<string> Descs { get; set; }
	
	        /// <summary>
	        /// 失败商品编码
	        /// </summary>
	        [XmlArray("fail_ids")]
	        [XmlArrayItem("number")]
	        public List<long> FailIds { get; set; }
	
	        /// <summary>
	        /// 成功商品编码
	        /// </summary>
	        [XmlArray("success_ids")]
	        [XmlArrayItem("number")]
	        public List<long> SuccessIds { get; set; }
}

	/// <summary>
/// NewRetailResultDomain Data Structure.
/// </summary>
[Serializable]

public class NewRetailResultDomain : TopObject
{
	        /// <summary>
	        /// 错误编码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 请求结果
	        /// </summary>
	        [XmlElement("result_data")]
	        public TagRespDtoDomain ResultData { get; set; }
	
	        /// <summary>
	        /// 成功或者失败
	        /// </summary>
	        [XmlElement("success_flag")]
	        public bool SuccessFlag { get; set; }
}

    }
}
