using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// JdsTradesStatisticsGetResponse.
    /// </summary>
    public class JdsTradesStatisticsGetResponse : TopResponse
    {
        /// <summary>
        /// 订单状态计数值
        /// </summary>
        [XmlArray("status_infos")]
        [XmlArrayItem("trade_stat")]
        public List<Top.Api.Domain.TradeStat> StatusInfos { get; set; }

    }
}
