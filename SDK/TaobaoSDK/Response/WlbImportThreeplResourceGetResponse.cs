using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// WlbImportThreeplResourceGetResponse.
    /// </summary>
    public class WlbImportThreeplResourceGetResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public TopResultDomain Result { get; set; }

	/// <summary>
/// ThreePlConsignResourceDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ThreePlConsignResourceDtoDomain : TopObject
{
	        /// <summary>
	        /// 达成率
	        /// </summary>
	        [XmlElement("achieving_rate")]
	        public long AchievingRate { get; set; }
	
	        /// <summary>
	        /// 首重价格
	        /// </summary>
	        [XmlElement("basic_weight")]
	        public long BasicWeight { get; set; }
	
	        /// <summary>
	        /// 首重价格
	        /// </summary>
	        [XmlElement("basic_weight_price")]
	        public long BasicWeightPrice { get; set; }
	
	        /// <summary>
	        /// 破损赔付
	        /// </summary>
	        [XmlElement("broken_compensate_price")]
	        public long BrokenCompensatePrice { get; set; }
	
	        /// <summary>
	        /// 达成时效
	        /// </summary>
	        [XmlElement("delivery_time")]
	        public long DeliveryTime { get; set; }
	
	        /// <summary>
	        /// 丢失赔付价格
	        /// </summary>
	        [XmlElement("missing_compensate_price")]
	        public long MissingCompensatePrice { get; set; }
	
	        /// <summary>
	        /// 资源code
	        /// </summary>
	        [XmlElement("res_code")]
	        public string ResCode { get; set; }
	
	        /// <summary>
	        /// 配送资源id
	        /// </summary>
	        [XmlElement("res_id")]
	        public long ResId { get; set; }
	
	        /// <summary>
	        /// 资源名称
	        /// </summary>
	        [XmlElement("res_name")]
	        public string ResName { get; set; }
	
	        /// <summary>
	        /// 续重价格
	        /// </summary>
	        [XmlElement("step_weight")]
	        public long StepWeight { get; set; }
	
	        /// <summary>
	        /// 续重价格
	        /// </summary>
	        [XmlElement("step_weight_price")]
	        public long StepWeightPrice { get; set; }
}

	/// <summary>
/// TopResultDomain Data Structure.
/// </summary>
[Serializable]

public class TopResultDomain : TopObject
{
	        /// <summary>
	        /// 错误代码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_msg")]
	        public string ErrorMsg { get; set; }
	
	        /// <summary>
	        /// 资源列表
	        /// </summary>
	        [XmlArray("resources")]
	        [XmlArrayItem("three_pl_consign_resource_dto")]
	        public List<ThreePlConsignResourceDtoDomain> Resources { get; set; }
	
	        /// <summary>
	        /// 错误代码
	        /// </summary>
	        [XmlElement("sub_error_code")]
	        public string SubErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("sub_error_msg")]
	        public string SubErrorMsg { get; set; }
	
	        /// <summary>
	        /// 是否成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
