using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrOrderQuerystatusResponse.
    /// </summary>
    public class TmallNrOrderQuerystatusResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrTradePayStatusRespDomain Data Structure.
/// </summary>
[Serializable]

public class NrTradePayStatusRespDomain : TopObject
{
	        /// <summary>
	        /// bizOrderId
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
	
	        /// <summary>
	        /// 支付状态
	        /// </summary>
	        [XmlElement("pay_status")]
	        public long PayStatus { get; set; }
	
	        /// <summary>
	        /// 是否支付成功
	        /// </summary>
	        [XmlElement("pay_success")]
	        public bool PaySuccess { get; set; }
	
	        /// <summary>
	        /// 支付状态描述
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrTradePayStatusRespDomain ResultData { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
