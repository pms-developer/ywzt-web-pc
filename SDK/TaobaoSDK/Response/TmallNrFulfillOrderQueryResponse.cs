using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrFulfillOrderQueryResponse.
    /// </summary>
    public class TmallNrFulfillOrderQueryResponse : TopResponse
    {
        /// <summary>
        /// 返回
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrOrderDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrOrderDtoDomain : TopObject
{
	        /// <summary>
	        /// 实际付款金额
	        /// </summary>
	        [XmlElement("actual_paid_fee")]
	        public long ActualPaidFee { get; set; }
	
	        /// <summary>
	        /// 修改价格修改的金额
	        /// </summary>
	        [XmlElement("adjust_fee")]
	        public long AdjustFee { get; set; }
	
	        /// <summary>
	        /// 商品价格
	        /// </summary>
	        [XmlElement("auction_price")]
	        public long AuctionPrice { get; set; }
	
	        /// <summary>
	        /// 买家昵称
	        /// </summary>
	        [XmlElement("buyer_nick")]
	        public string BuyerNick { get; set; }
	
	        /// <summary>
	        /// 创建时间
	        /// </summary>
	        [XmlElement("create_time")]
	        public string CreateTime { get; set; }
	
	        /// <summary>
	        /// 店铺优惠的分摊
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public long DiscountFee { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("item_id")]
	        public long ItemId { get; set; }
	
	        /// <summary>
	        /// 购买数量
	        /// </summary>
	        [XmlElement("num")]
	        public long Num { get; set; }
	
	        /// <summary>
	        /// 子订单号
	        /// </summary>
	        [XmlElement("order_id")]
	        public long OrderId { get; set; }
	
	        /// <summary>
	        /// outIdItemCode
	        /// </summary>
	        [XmlElement("out_id_item_code")]
	        public string OutIdItemCode { get; set; }
	
	        /// <summary>
	        /// 退款状态退款状态。退款状态。可选值WAIT_SELLER_AGREE(买家已经申请退款，等待卖家同意)，WAIT_BUYER_RETURN_GOODS(卖家已经同意退款，等待买家退货)，WAIT_SELLER_CONFIRM_GOODS(买家已经退货，等待卖家确认收货)，SELLER_REFUSE_BUYER(卖家拒绝退款)，CLOSED(退款关闭)，SUCCESS(退款成功)
	        /// </summary>
	        [XmlElement("refund_status")]
	        public string RefundStatus { get; set; }
	
	        /// <summary>
	        /// 卖家昵称
	        /// </summary>
	        [XmlElement("seller_nick")]
	        public string SellerNick { get; set; }
	
	        /// <summary>
	        /// skuId
	        /// </summary>
	        [XmlElement("sku_id")]
	        public long SkuId { get; set; }
	
	        /// <summary>
	        /// 商品标题
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
}

	/// <summary>
/// TradeOrderDetailDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrderDetailDtoDomain : TopObject
{
	        /// <summary>
	        /// 预约结束时间
	        /// </summary>
	        [XmlElement("appoint_end_time")]
	        public string AppointEndTime { get; set; }
	
	        /// <summary>
	        /// 预约开始时间
	        /// </summary>
	        [XmlElement("appoint_start_time")]
	        public string AppointStartTime { get; set; }
	
	        /// <summary>
	        /// 买家昵称
	        /// </summary>
	        [XmlElement("buyer_nick")]
	        public string BuyerNick { get; set; }
	
	        /// <summary>
	        /// createTime
	        /// </summary>
	        [XmlElement("create_time")]
	        public string CreateTime { get; set; }
	
	        /// <summary>
	        /// 子订单列表
	        /// </summary>
	        [XmlArray("order_d_t_os")]
	        [XmlArrayItem("nr_order_dto")]
	        public List<NrOrderDtoDomain> OrderDTOs { get; set; }
	
	        /// <summary>
	        /// 主订单
	        /// </summary>
	        [XmlElement("order_id")]
	        public long OrderId { get; set; }
	
	        /// <summary>
	        /// 门店的外部编号
	        /// </summary>
	        [XmlElement("out_id_store_code")]
	        public string OutIdStoreCode { get; set; }
	
	        /// <summary>
	        /// 支付时间
	        /// </summary>
	        [XmlElement("pay_time")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 收货详细地址_商家没有入塔不提供
	        /// </summary>
	        [XmlElement("receiver_address")]
	        public string ReceiverAddress { get; set; }
	
	        /// <summary>
	        /// 城市_商家没有入塔不提供
	        /// </summary>
	        [XmlElement("receiver_city")]
	        public string ReceiverCity { get; set; }
	
	        /// <summary>
	        /// 区_商家没有入塔不提供
	        /// </summary>
	        [XmlElement("receiver_district")]
	        public string ReceiverDistrict { get; set; }
	
	        /// <summary>
	        /// 手机_商家没有入塔不提供
	        /// </summary>
	        [XmlElement("receiver_mobile")]
	        public string ReceiverMobile { get; set; }
	
	        /// <summary>
	        /// 收货人_商家没有入塔不提供
	        /// </summary>
	        [XmlElement("receiver_name")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 电话_商家没有入塔不提供
	        /// </summary>
	        [XmlElement("receiver_phone")]
	        public string ReceiverPhone { get; set; }
	
	        /// <summary>
	        /// 省_商家没有入塔不提供
	        /// </summary>
	        [XmlElement("receiver_province")]
	        public string ReceiverProvince { get; set; }
	
	        /// <summary>
	        /// 镇_商家没有入塔不提供
	        /// </summary>
	        [XmlElement("receiver_town")]
	        public string ReceiverTown { get; set; }
	
	        /// <summary>
	        /// 卖家昵称
	        /// </summary>
	        [XmlElement("seller_nick")]
	        public string SellerNick { get; set; }
	
	        /// <summary>
	        /// 交易状态可选值:TRADE_NO_CREATE_PAY(没有创建支付宝交易)，WAIT_BUYER_PAY(等待买家付款)，SELLER_CONSIGNED_PART(卖家部分发货)，WAIT_SELLER_SEND_GOODS(等待卖家发货,即:买家已付款)，WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货,即:卖家已发货)，TRADE_BUYER_SIGNED(买家已签收,货到付款专用)，TRADE_FINISHED(交易成功)，TRADE_CLOSED(付款以后用户退款成功，交易自动关闭)，TRADE_CLOSED_BY_TAOBAO(付款以前，卖家或买家主动关闭交易)，PAY_PENDING(国际信用卡支付付款确认中) * WAIT_PRE_AUTH_CONFIRM(0元购合约中)，PAID_FORBID_CONSIGN(拼团中订单或者发货强管控的订单，已付款但禁止发货)
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 门店编码
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// isSuccess
	        /// </summary>
	        [XmlElement("is_success")]
	        public bool IsSuccess { get; set; }
	
	        /// <summary>
	        /// 返回数据
	        /// </summary>
	        [XmlElement("result_data")]
	        public TradeOrderDetailDtoDomain ResultData { get; set; }
}

    }
}
