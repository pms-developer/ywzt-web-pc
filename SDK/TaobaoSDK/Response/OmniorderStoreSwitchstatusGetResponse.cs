using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreSwitchstatusGetResponse.
    /// </summary>
    public class OmniorderStoreSwitchstatusGetResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public Top.Api.Domain.Result Result { get; set; }

    }
}
