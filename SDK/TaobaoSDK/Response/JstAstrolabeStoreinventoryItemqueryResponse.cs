using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// JstAstrolabeStoreinventoryItemqueryResponse.
    /// </summary>
    public class JstAstrolabeStoreinventoryItemqueryResponse : TopResponse
    {
        /// <summary>
        /// 响应标示
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// message
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应编码
        /// </summary>
        [XmlElement("qimen_code")]
        public string QimenCode { get; set; }

        /// <summary>
        /// 门店列表
        /// </summary>
        [XmlArray("stores")]
        [XmlArrayItem("store")]
        public List<StoreDomain> Stores { get; set; }

	/// <summary>
/// QuantityDetailDomain Data Structure.
/// </summary>
[Serializable]

public class QuantityDetailDomain : TopObject
{
	        /// <summary>
	        /// 库存类型
	        /// </summary>
	        [XmlElement("inventory_type")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 库存数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public long Quantity { get; set; }
}

	/// <summary>
/// StoreInventoryDomain Data Structure.
/// </summary>
[Serializable]

public class StoreInventoryDomain : TopObject
{
	        /// <summary>
	        /// 淘宝前端商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// ISV系统中商品编码
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 库存量详情列表
	        /// </summary>
	        [XmlArray("quantity_details")]
	        [XmlArrayItem("quantity_detail")]
	        public List<QuantityDetailDomain> QuantityDetails { get; set; }
	
	        /// <summary>
	        /// 商品的SKU编码
	        /// </summary>
	        [XmlElement("sku_id")]
	        public string SkuId { get; set; }
}

	/// <summary>
/// StoreDomain Data Structure.
/// </summary>
[Serializable]

public class StoreDomain : TopObject
{
	        /// <summary>
	        /// 门店库存列表
	        /// </summary>
	        [XmlArray("store_inventories")]
	        [XmlArrayItem("store_inventory")]
	        public List<StoreInventoryDomain> StoreInventories { get; set; }
	
	        /// <summary>
	        /// 门店ID(商户中心) 或 电商仓ID
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public string WarehouseId { get; set; }
	
	        /// <summary>
	        /// 库存来源
	        /// </summary>
	        [XmlElement("warehouse_type")]
	        public string WarehouseType { get; set; }
}

    }
}
