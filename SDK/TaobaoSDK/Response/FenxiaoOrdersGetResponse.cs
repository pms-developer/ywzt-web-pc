using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// FenxiaoOrdersGetResponse.
    /// </summary>
    public class FenxiaoOrdersGetResponse : TopResponse
    {
        /// <summary>
        /// 采购单及子采购单信息。返回 PurchaseOrder 包含的字段信息。
        /// </summary>
        [XmlArray("purchase_orders")]
        [XmlArrayItem("purchase_order")]
        public List<TopDpOrderDoDomain> PurchaseOrders { get; set; }

        /// <summary>
        /// 搜索到的采购单记录总数
        /// </summary>
        [XmlElement("total_results")]
        public long TotalResults { get; set; }

	/// <summary>
/// TopReceiverDoDomain Data Structure.
/// </summary>
[Serializable]

public class TopReceiverDoDomain : TopObject
{
	        /// <summary>
	        /// 收货人的详细地址信息
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 收货人的城市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 区/县
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 移动电话
	        /// </summary>
	        [XmlElement("mobile_phone")]
	        public string MobilePhone { get; set; }
	
	        /// <summary>
	        /// 收货人全名
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 固定电话
	        /// </summary>
	        [XmlElement("phone")]
	        public string Phone { get; set; }
	
	        /// <summary>
	        /// 收货人所在省份
	        /// </summary>
	        [XmlElement("state")]
	        public string State { get; set; }
	
	        /// <summary>
	        /// 邮政编码
	        /// </summary>
	        [XmlElement("zip")]
	        public string Zip { get; set; }
}

	/// <summary>
/// OrderMessagesDomain Data Structure.
/// </summary>
[Serializable]

public class OrderMessagesDomain : TopObject
{
	        /// <summary>
	        /// 留言内容
	        /// </summary>
	        [XmlElement("message_content")]
	        public string MessageContent { get; set; }
	
	        /// <summary>
	        /// 留言时间
	        /// </summary>
	        [XmlElement("message_time")]
	        public string MessageTime { get; set; }
	
	        /// <summary>
	        /// 留言标题，例如：分销商留言，供应商留言，买家留言
	        /// </summary>
	        [XmlElement("message_title")]
	        public string MessageTitle { get; set; }
	
	        /// <summary>
	        /// 留言时的图片地址
	        /// </summary>
	        [XmlElement("pic_url")]
	        public string PicUrl { get; set; }
}

	/// <summary>
/// FeatureDoDomain Data Structure.
/// </summary>
[Serializable]

public class FeatureDoDomain : TopObject
{
	        /// <summary>
	        /// 属性键
	        /// </summary>
	        [XmlElement("attr_key")]
	        public string AttrKey { get; set; }
	
	        /// <summary>
	        /// 属性值
	        /// </summary>
	        [XmlElement("attr_value")]
	        public string AttrValue { get; set; }
}

	/// <summary>
/// TopMemoAttachmentDomain Data Structure.
/// </summary>
[Serializable]

public class TopMemoAttachmentDomain : TopObject
{
	        /// <summary>
	        /// name
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// url
	        /// </summary>
	        [XmlElement("url")]
	        public string Url { get; set; }
}

	/// <summary>
/// TopMemoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TopMemoDtoDomain : TopObject
{
	        /// <summary>
	        /// attachments
	        /// </summary>
	        [XmlArray("attachments")]
	        [XmlArrayItem("top_memo_attachment")]
	        public List<TopMemoAttachmentDomain> Attachments { get; set; }
	
	        /// <summary>
	        /// operateUserNick
	        /// </summary>
	        [XmlElement("operate_user_nick")]
	        public string OperateUserNick { get; set; }
	
	        /// <summary>
	        /// remark
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
}

	/// <summary>
/// SubOrderDetailDomain Data Structure.
/// </summary>
[Serializable]

public class SubOrderDetailDomain : TopObject
{
	        /// <summary>
	        /// 前台分销商品的宝贝ID，不存在时为0。2015年4月15日之前创建的采购单该字段都是0。
	        /// </summary>
	        [XmlElement("auction_id")]
	        public long AuctionId { get; set; }
	
	        /// <summary>
	        /// 分销商店铺中宝贝一口价
	        /// </summary>
	        [XmlElement("auction_price")]
	        public string AuctionPrice { get; set; }
	
	        /// <summary>
	        /// 前台商品SKU ID，不存在时为0。2015年3月15日之前创建的采购单该字段都是0。
	        /// </summary>
	        [XmlElement("auction_sku_id")]
	        public long AuctionSkuId { get; set; }
	
	        /// <summary>
	        /// 发票应开金额。根据买家实际付款去除邮费后，按各个子单(商品)金额比例进行分摊后的金额，仅供开发票时做票面金额参考。
	        /// </summary>
	        [XmlElement("bill_fee")]
	        public string BillFee { get; set; }
	
	        /// <summary>
	        /// 买家订单上对应的子单零售金额，除以num（数量）后等于最终宝贝的零售价格（精确到2位小数;单位:元。如:200.07，表示:200元7分）
	        /// </summary>
	        [XmlElement("buyer_payment")]
	        public string BuyerPayment { get; set; }
	
	        /// <summary>
	        /// 创建时间。格式 yyyy-MM-dd HH:mm:ss 。
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 优惠活动的折扣金额
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public string DiscountFee { get; set; }
	
	        /// <summary>
	        /// 分销商实付金额=total_fee（分销商应付金额）+改价-优惠。（精确到2位小数;单位:元。如:200.07，表示:200元7分）
	        /// </summary>
	        [XmlElement("distributor_payment")]
	        public string DistributorPayment { get; set; }
	
	        /// <summary>
	        /// Feature对象列表目前已有的属性：attr_key为 www，attr_value为1 表示是www子订单；attr_key为 wwwStoreCode，attr_value是www子订单发货的仓库编码；attr_key为 isWt，attr_value为1 表示是网厅子订单；attr_key为wtInfo,attr_value为网厅相关合约信息；attr_key为shipper,attr_value为cn表示菜鸟发货；attr_key为 storeCode，attr_value为仓库信息； attr_key为 erpHold，attr_value为1表示强管控中， attr_value为2表示分单完成；
	        /// </summary>
	        [XmlArray("features")]
	        [XmlArrayItem("feature")]
	        public List<FeatureDoDomain> Features { get; set; }
	
	        /// <summary>
	        /// 分销平台的子采购单主键
	        /// </summary>
	        [XmlElement("fenxiao_id")]
	        public long FenxiaoId { get; set; }
	
	        /// <summary>
	        /// 子采购单id，淘宝交易主键，采购单未付款时为0.（只有支付宝 付款才有这个id，其余付款形式该字段为0）
	        /// </summary>
	        [XmlElement("id")]
	        public long Id { get; set; }
	
	        /// <summary>
	        /// 分销平台上的产品id，同FenxiaoProduct 的pid
	        /// </summary>
	        [XmlElement("item_id")]
	        public long ItemId { get; set; }
	
	        /// <summary>
	        /// 分销平台上商品商家编码。
	        /// </summary>
	        [XmlElement("item_outer_id")]
	        public string ItemOuterId { get; set; }
	
	        /// <summary>
	        /// 产品的采购数量。取值范围:大于零的整数
	        /// </summary>
	        [XmlElement("num")]
	        public long Num { get; set; }
	
	        /// <summary>
	        /// 老的SKU属性值。如: 颜色:红色:别名;尺码:L:别名
	        /// </summary>
	        [XmlElement("old_sku_properties")]
	        public string OldSkuProperties { get; set; }
	
	        /// <summary>
	        /// 代销采购单对应下游200订单状态。可选值：WAIT_SELLER_SEND_GOODS(已付款，待发货)WAIT_BUYER_CONFIRM_GOODS(已付款，已发货)TRADE_CLOSED(已退款成功)TRADE_REFUNDING(退款中)TRADE_FINISHED(交易成功)TRADE_CLOSED_BY_TAOBAO(交易关闭)
	        /// </summary>
	        [XmlElement("order_200_status")]
	        public string Order200Status { get; set; }
	
	        /// <summary>
	        /// 产品的采购价格。（精确到2位小数;单位:元。如:200.07，表示:200元7分）
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 优惠活动类型0=无优惠1=限时折
	        /// </summary>
	        [XmlElement("promotion_type")]
	        public string PromotionType { get; set; }
	
	        /// <summary>
	        /// 退款金额
	        /// </summary>
	        [XmlElement("refund_fee")]
	        public string RefundFee { get; set; }
	
	        /// <summary>
	        /// 后端商品id
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public long ScItemId { get; set; }
	
	        /// <summary>
	        /// 分销产品的SKU id。当存在时才会有值，建议使用sku_outer_id，sku_properties这两个值
	        /// </summary>
	        [XmlElement("sku_id")]
	        public long SkuId { get; set; }
	
	        /// <summary>
	        /// SKU商家编码。
	        /// </summary>
	        [XmlElement("sku_outer_id")]
	        public string SkuOuterId { get; set; }
	
	        /// <summary>
	        /// SKU属性值。如: 颜色:红色:别名;尺码:L:别名
	        /// </summary>
	        [XmlElement("sku_properties")]
	        public string SkuProperties { get; set; }
	
	        /// <summary>
	        /// 快照地址。
	        /// </summary>
	        [XmlElement("snapshot_url")]
	        public string SnapshotUrl { get; set; }
	
	        /// <summary>
	        /// 交易状态。可选值： <br>WAIT_BUYER_PAY(等待付款)<br> WAIT_SELLER_SEND_GOODS(已付款，待发货）<br> WAIT_BUYER_CONFIRM_GOODS(已付款，已发货)<br> PAID_FORBID_CONSIGN(已付款，禁止发货 ps:只有大快消行业的才有)<br> TRADE_FINISHED(交易成功)<br> TRADE_CLOSED(交易关闭)<br> WAIT_BUYER_CONFIRM_GOODS_ACOUNTED(已付款（已分账），已发货。只对代销分账支持)<br> PAY_ACOUNTED_GOODS_CONFIRM （已分账发货成功）<br> PAY_WAIT_ACOUNT_GOODS_CONFIRM（已付款，确认收货）
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 商品的卖出金额调整，金额增加时为正数，金额减少时为负数，单位是分，不带小数
	        /// </summary>
	        [XmlElement("tc_adjust_fee")]
	        public long TcAdjustFee { get; set; }
	
	        /// <summary>
	        /// 优惠金额，始终为正数，单位是分，不带小数
	        /// </summary>
	        [XmlElement("tc_discount_fee")]
	        public long TcDiscountFee { get; set; }
	
	        /// <summary>
	        /// TC子订单ID（经销不显示）
	        /// </summary>
	        [XmlElement("tc_order_id")]
	        public long TcOrderId { get; set; }
	
	        /// <summary>
	        /// 商品优惠类型：聚划算、秒杀或其他
	        /// </summary>
	        [XmlElement("tc_preferential_type")]
	        public string TcPreferentialType { get; set; }
	
	        /// <summary>
	        /// 采购的产品标题。
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
	
	        /// <summary>
	        /// topMemoDTO
	        /// </summary>
	        [XmlElement("top_memo")]
	        public TopMemoDtoDomain TopMemo { get; set; }
	
	        /// <summary>
	        /// 分销商应付金额=num(采购数量)*price(采购价)。（精确到2位小数;单位:元。如:200.07，表示:200元7分）
	        /// </summary>
	        [XmlElement("total_fee")]
	        public string TotalFee { get; set; }
}

	/// <summary>
/// ErpLogisticsInfoDomain Data Structure.
/// </summary>
[Serializable]

public class ErpLogisticsInfoDomain : TopObject
{
	        /// <summary>
	        /// 组合商品Code
	        /// </summary>
	        [XmlElement("combine_item_code")]
	        public string CombineItemCode { get; set; }
	
	        /// <summary>
	        /// 组合商品ID
	        /// </summary>
	        [XmlElement("combine_item_id")]
	        public string CombineItemId { get; set; }
	
	        /// <summary>
	        /// 发货类型 CN=菜鸟发货,SC的商家仓发货
	        /// </summary>
	        [XmlElement("consign_type")]
	        public string ConsignType { get; set; }
	
	        /// <summary>
	        /// 货品仓储code
	        /// </summary>
	        [XmlElement("item_code")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 货品仓储id
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 组合货品比例
	        /// </summary>
	        [XmlElement("item_ratio")]
	        public long ItemRatio { get; set; }
	
	        /// <summary>
	        /// 应发数量
	        /// </summary>
	        [XmlElement("need_consign_num")]
	        public long NeedConsignNum { get; set; }
	
	        /// <summary>
	        /// 商品数字编号
	        /// </summary>
	        [XmlElement("num_iid")]
	        public long NumIid { get; set; }
	
	        /// <summary>
	        /// 采购单号
	        /// </summary>
	        [XmlElement("order_id")]
	        public long OrderId { get; set; }
	
	        /// <summary>
	        /// 商品的最小库存单位Sku的id
	        /// </summary>
	        [XmlElement("sku_id")]
	        public string SkuId { get; set; }
	
	        /// <summary>
	        /// 如是菜鸟仓，则将菜鸟仓的区域仓code进行填充，如是商家仓发货则填充SC
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 采购单子单号
	        /// </summary>
	        [XmlElement("sub_order_id")]
	        public long SubOrderId { get; set; }
	
	        /// <summary>
	        /// 子订单类型:标示该子交易单来源交易，还是BMS增加的，枚举值(00=交易，10=BMS绑定)
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
}

	/// <summary>
/// TopDpOrderDoDomain Data Structure.
/// </summary>
[Serializable]

public class TopDpOrderDoDomain : TopObject
{
	        /// <summary>
	        /// 支付宝交易号。
	        /// </summary>
	        [XmlElement("alipay_no")]
	        public string AlipayNo { get; set; }
	
	        /// <summary>
	        /// 买家nick，供应商查询不会返回买家昵称，分销商查询才会返回。
	        /// </summary>
	        [XmlElement("buyer_nick")]
	        public string BuyerNick { get; set; }
	
	        /// <summary>
	        /// 买家支付给分销商的总金额。注意买家购买的商品可能不是全部来自同一供货商，请同时参考子单上的相关金额。（精确到2位小数;单位:元。如:200.07，表示:200元7分）
	        /// </summary>
	        [XmlElement("buyer_payment")]
	        public string BuyerPayment { get; set; }
	
	        /// <summary>
	        /// 加密后的买家淘宝ID，长度为32位
	        /// </summary>
	        [XmlElement("buyer_taobao_id")]
	        public string BuyerTaobaoId { get; set; }
	
	        /// <summary>
	        /// 物流发货时间。格式:yyyy-MM-dd HH:mm:ss
	        /// </summary>
	        [XmlElement("consign_time")]
	        public string ConsignTime { get; set; }
	
	        /// <summary>
	        /// 采购单创建时间。格式:yyyy-MM-dd HH:mm:ss
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 分销商来源网站（taobao）。
	        /// </summary>
	        [XmlElement("distributor_from")]
	        public string DistributorFrom { get; set; }
	
	        /// <summary>
	        /// 分销商实付金额。(精确到2位小数;单位:元。如:200.07，表示:200元7分 )
	        /// </summary>
	        [XmlElement("distributor_payment")]
	        public string DistributorPayment { get; set; }
	
	        /// <summary>
	        /// 分销商在来源网站的帐号名。
	        /// </summary>
	        [XmlElement("distributor_username")]
	        public string DistributorUsername { get; set; }
	
	        /// <summary>
	        /// 交易结束时间
	        /// </summary>
	        [XmlElement("end_time")]
	        public string EndTime { get; set; }
	
	        /// <summary>
	        /// 主订单属性信息，key-value形式：  orderNovice ：订单发票抬头；  orderNoviceContent ：代表发票明细
	        /// </summary>
	        [XmlArray("features")]
	        [XmlArrayItem("feature")]
	        public List<FeatureDoDomain> Features { get; set; }
	
	        /// <summary>
	        /// 分销流水号，分销平台产生的主键
	        /// </summary>
	        [XmlElement("fenxiao_id")]
	        public long FenxiaoId { get; set; }
	
	        /// <summary>
	        /// 供应商交易ID 非采购单ID，如果改发货状态 是需要该ID，ID在用户未付款前为0，付款后有具体值（发货时使用该ID）
	        /// </summary>
	        [XmlElement("id")]
	        public long Id { get; set; }
	
	        /// <summary>
	        /// 自定义key
	        /// </summary>
	        [XmlArray("isv_custom_key")]
	        [XmlArrayItem("string")]
	        public List<string> IsvCustomKey { get; set; }
	
	        /// <summary>
	        /// 自定义值
	        /// </summary>
	        [XmlArray("isv_custom_value")]
	        [XmlArrayItem("string")]
	        public List<string> IsvCustomValue { get; set; }
	
	        /// <summary>
	        /// 物流公司
	        /// </summary>
	        [XmlElement("logistics_company_name")]
	        public string LogisticsCompanyName { get; set; }
	
	        /// <summary>
	        /// 运单号
	        /// </summary>
	        [XmlElement("logistics_id")]
	        public string LogisticsId { get; set; }
	
	        /// <summary>
	        /// [架海金梁独有字段，非架海金梁用户请勿关心]子单物流发货信息
	        /// </summary>
	        [XmlArray("logistics_infos")]
	        [XmlArrayItem("erp_logistics_info")]
	        public List<ErpLogisticsInfoDomain> LogisticsInfos { get; set; }
	
	        /// <summary>
	        /// 采购单留言。（代销模式下信息包括买家留言和分销商留言）
	        /// </summary>
	        [XmlElement("memo")]
	        public string Memo { get; set; }
	
	        /// <summary>
	        /// 交易修改时间。格式:yyyy-MM-dd HH:mm:ss
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 采购单留言列表
	        /// </summary>
	        [XmlArray("order_messages")]
	        [XmlArrayItem("order_message")]
	        public List<OrderMessagesDomain> OrderMessages { get; set; }
	
	        /// <summary>
	        /// 付款时间。格式:yyyy-MM-dd HH:mm:ss
	        /// </summary>
	        [XmlElement("pay_time")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 支付方式：ALIPAY_SURETY（支付宝担保交易）、ALIPAY_CHAIN（分账交易）、TRANSFER（线下转账）、PREPAY（预存款）、IMMEDIATELY（即时到账）、CASHGOODS（先款后货）、ACCOUNT_PERIOD（账期支付）
	        /// </summary>
	        [XmlElement("pay_type")]
	        public string PayType { get; set; }
	
	        /// <summary>
	        /// 采购单邮费。(精确到2位小数;单位:元。如:200.07，表示:200元7分 )
	        /// </summary>
	        [XmlElement("post_fee")]
	        public string PostFee { get; set; }
	
	        /// <summary>
	        /// 买家详细信息
	        /// </summary>
	        [XmlElement("receiver")]
	        public TopReceiverDoDomain Receiver { get; set; }
	
	        /// <summary>
	        /// 配送方式，FAST(快速)、EMS、ORDINARY(平邮)、SELLER(卖家包邮)
	        /// </summary>
	        [XmlElement("shipping")]
	        public string Shipping { get; set; }
	
	        /// <summary>
	        /// 订单快照URL
	        /// </summary>
	        [XmlElement("snapshot_url")]
	        public string SnapshotUrl { get; set; }
	
	        /// <summary>
	        /// 采购单交易状态。可选值： <br>WAIT_BUYER_PAY(等待付款)<br> WAIT_SELLER_SEND_GOODS(已付款，待发货）<br> WAIT_BUYER_CONFIRM_GOODS(已付款，已发货)<br> PAID_FORBID_CONSIGN(已付款，禁止发货 ps:只有大快消行业的才有)<br> TRADE_FINISHED(交易成功)<br> TRADE_CLOSED(交易关闭)<br> WAIT_BUYER_CONFIRM_GOODS_ACOUNTED(已付款（已分账），已发货。只对代销分账支持)<br> PAY_ACOUNTED_GOODS_CONFIRM （已分账发货成功）<br> PAY_WAIT_ACOUNT_GOODS_CONFIRM（已付款，确认收货）
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 子订单的详细信息列表。
	        /// </summary>
	        [XmlArray("sub_purchase_orders")]
	        [XmlArrayItem("sub_purchase_order")]
	        public List<SubOrderDetailDomain> SubPurchaseOrders { get; set; }
	
	        /// <summary>
	        /// 返回供应商备注旗帜vlaue在1-5之间。非1-5之间，都采用1作为默认。 1:红色 2:黄色 3:绿色 4:蓝色 5:粉红色
	        /// </summary>
	        [XmlElement("supplier_flag")]
	        public long SupplierFlag { get; set; }
	
	        /// <summary>
	        /// 供应商来源网站, values: taobao, alibaba
	        /// </summary>
	        [XmlElement("supplier_from")]
	        public string SupplierFrom { get; set; }
	
	        /// <summary>
	        /// 供应商备注
	        /// </summary>
	        [XmlElement("supplier_memo")]
	        public string SupplierMemo { get; set; }
	
	        /// <summary>
	        /// 供应商在来源网站的帐号名。
	        /// </summary>
	        [XmlElement("supplier_username")]
	        public string SupplierUsername { get; set; }
	
	        /// <summary>
	        /// 主订单ID （经销不显示）
	        /// </summary>
	        [XmlElement("tc_order_id")]
	        public long TcOrderId { get; set; }
	
	        /// <summary>
	        /// 采购单总额（不含邮费,精确到2位小数;单位:元。如:200.07，表示:200元7分 )
	        /// </summary>
	        [XmlElement("total_fee")]
	        public string TotalFee { get; set; }
	
	        /// <summary>
	        /// 分销方式：AGENT（代销）、DEALER（经销）
	        /// </summary>
	        [XmlElement("trade_type")]
	        public string TradeType { get; set; }
}

    }
}
