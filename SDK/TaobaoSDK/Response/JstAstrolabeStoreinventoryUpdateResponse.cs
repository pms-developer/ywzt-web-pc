using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// JstAstrolabeStoreinventoryUpdateResponse.
    /// </summary>
    public class JstAstrolabeStoreinventoryUpdateResponse : TopResponse
    {
        /// <summary>
        /// 错误信息列表
        /// </summary>
        [XmlArray("error_descriptions")]
        [XmlArrayItem("error")]
        public List<ErrorDomain> ErrorDescriptions { get; set; }

        /// <summary>
        /// 响应标示
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应标签
        /// </summary>
        [XmlElement("result_code")]
        public string ResultCode { get; set; }

	/// <summary>
/// ErrorDomain Data Structure.
/// </summary>
[Serializable]

public class ErrorDomain : TopObject
{
	        /// <summary>
	        /// 错误描述
	        /// </summary>
	        [XmlElement("description")]
	        public string Description { get; set; }
	
	        /// <summary>
	        /// 处理失败的流水号
	        /// </summary>
	        [XmlElement("failed_bill_num")]
	        public string FailedBillNum { get; set; }
}

    }
}
