using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// ItemUpdateResponse.
    /// </summary>
    public class ItemUpdateResponse : TopResponse
    {
        /// <summary>
        /// 商品结构
        /// </summary>
        [XmlElement("item")]
        public ItemDomain Item { get; set; }

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain : TopObject
{
	        /// <summary>
	        /// 商品iid
	        /// </summary>
	        [XmlElement("iid")]
	        public string Iid { get; set; }
	
	        /// <summary>
	        /// 商品修改时间（格式：yyyy-MM-dd HH:mm:ss）
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 商品数字id
	        /// </summary>
	        [XmlElement("num_iid")]
	        public long NumIid { get; set; }
}

    }
}
