using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OcTradetagAttachResponse.
    /// </summary>
    public class OcTradetagAttachResponse : TopResponse
    {
        /// <summary>
        /// 操作成功或者操作失败
        /// </summary>
        [XmlElement("result")]
        public bool Result { get; set; }

    }
}
