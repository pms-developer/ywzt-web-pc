using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// AlibabaEinvoicePaperPrintResponse.
    /// </summary>
    public class AlibabaEinvoicePaperPrintResponse : TopResponse
    {
        /// <summary>
        /// 调用结果，打印结果tmc消息alibaba_invoice_PaperOpsReturn异步通知
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

    }
}
