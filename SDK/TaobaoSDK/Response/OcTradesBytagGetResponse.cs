using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OcTradesBytagGetResponse.
    /// </summary>
    public class OcTradesBytagGetResponse : TopResponse
    {
        /// <summary>
        /// 打了该标签的订单编号列表
        /// </summary>
        [XmlArray("tids")]
        [XmlArrayItem("number")]
        public List<long> Tids { get; set; }

        /// <summary>
        /// 总数
        /// </summary>
        [XmlElement("totals")]
        public long Totals { get; set; }

    }
}
