using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// AlibabaEinvoiceItemUpdateResponse.
    /// </summary>
    public class AlibabaEinvoiceItemUpdateResponse : TopResponse
    {
        /// <summary>
        /// 修改结果
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

    }
}
