using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// StreetestSessionGetResponse.
    /// </summary>
    public class StreetestSessionGetResponse : TopResponse
    {
        /// <summary>
        /// 压测账号对应的sessionKey
        /// </summary>
        [XmlElement("stree_test_session_key")]
        public string StreeTestSessionKey { get; set; }

    }
}
