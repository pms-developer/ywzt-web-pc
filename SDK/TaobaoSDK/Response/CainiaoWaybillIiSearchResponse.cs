using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoWaybillIiSearchResponse.
    /// </summary>
    public class CainiaoWaybillIiSearchResponse : TopResponse
    {
        /// <summary>
        /// CP网点信息及对应的商家的发货信息
        /// </summary>
        [XmlArray("waybill_apply_subscription_cols")]
        [XmlArrayItem("waybill_apply_subscription_info")]
        public List<WaybillApplySubscriptionInfoDomain> WaybillApplySubscriptionCols { get; set; }

	/// <summary>
/// AddressDtoDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDtoDomain : TopObject
{
	        /// <summary>
	        /// 市名称（二级地址）
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区名称（三级地址）
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省名称（一级地址）
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道\镇名称（四级地址）
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// ServiceAttributeDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ServiceAttributeDtoDomain : TopObject
{
	        /// <summary>
	        /// 属性的值，用户实际传入的值
	        /// </summary>
	        [XmlElement("attribute_code")]
	        public string AttributeCode { get; set; }
	
	        /// <summary>
	        /// 属性的名称，可以用于前端的展示
	        /// </summary>
	        [XmlElement("attribute_name")]
	        public string AttributeName { get; set; }
	
	        /// <summary>
	        /// 属性的类型，可能值有 [number, string, enum]
	        /// </summary>
	        [XmlElement("attribute_type")]
	        public string AttributeType { get; set; }
	
	        /// <summary>
	        /// 枚举类型的枚举值，key为用户选中的需要传值的数据，value为对应的描述，可以作为前端的展示
	        /// </summary>
	        [XmlElement("type_desc")]
	        public string TypeDesc { get; set; }
}

	/// <summary>
/// ServiceInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ServiceInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 该服务是否为必选服务
	        /// </summary>
	        [XmlElement("required")]
	        public bool Required { get; set; }
	
	        /// <summary>
	        /// 服务属性定义
	        /// </summary>
	        [XmlArray("service_attributes")]
	        [XmlArrayItem("service_attribute_dto")]
	        public List<ServiceAttributeDtoDomain> ServiceAttributes { get; set; }
	
	        /// <summary>
	        /// 服务编码
	        /// </summary>
	        [XmlElement("service_code")]
	        public string ServiceCode { get; set; }
	
	        /// <summary>
	        /// 服务的官方描述，可以用作前端展示
	        /// </summary>
	        [XmlElement("service_desc")]
	        public string ServiceDesc { get; set; }
	
	        /// <summary>
	        /// 服务名称
	        /// </summary>
	        [XmlElement("service_name")]
	        public string ServiceName { get; set; }
}

	/// <summary>
/// WaybillBranchAccountDomain Data Structure.
/// </summary>
[Serializable]

public class WaybillBranchAccountDomain : TopObject
{
	        /// <summary>
	        /// 已用面单数量
	        /// </summary>
	        [XmlElement("allocated_quantity")]
	        public long AllocatedQuantity { get; set; }
	
	        /// <summary>
	        /// 网点Code
	        /// </summary>
	        [XmlElement("branch_code")]
	        public string BranchCode { get; set; }
	
	        /// <summary>
	        /// 网点名称
	        /// </summary>
	        [XmlElement("branch_name")]
	        public string BranchName { get; set; }
	
	        /// <summary>
	        /// 网点状态
	        /// </summary>
	        [XmlElement("branch_status")]
	        public long BranchStatus { get; set; }
	
	        /// <summary>
	        /// 取消的面单总数
	        /// </summary>
	        [XmlElement("cancel_quantity")]
	        public long CancelQuantity { get; set; }
	
	        /// <summary>
	        /// 已经打印的面单总数
	        /// </summary>
	        [XmlElement("print_quantity")]
	        public long PrintQuantity { get; set; }
	
	        /// <summary>
	        /// 电子面单余额数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public long Quantity { get; set; }
	
	        /// <summary>
	        /// 号段信息
	        /// </summary>
	        [XmlElement("segment_code")]
	        public string SegmentCode { get; set; }
	
	        /// <summary>
	        /// 可用的服务信息列表
	        /// </summary>
	        [XmlArray("service_info_cols")]
	        [XmlArrayItem("service_info_dto")]
	        public List<ServiceInfoDtoDomain> ServiceInfoCols { get; set; }
	
	        /// <summary>
	        /// 当前网点下的发货地址
	        /// </summary>
	        [XmlArray("shipp_address_cols")]
	        [XmlArrayItem("address_dto")]
	        public List<AddressDtoDomain> ShippAddressCols { get; set; }
}

	/// <summary>
/// WaybillApplySubscriptionInfoDomain Data Structure.
/// </summary>
[Serializable]

public class WaybillApplySubscriptionInfoDomain : TopObject
{
	        /// <summary>
	        /// CP网点信息及对应的商家的发货信息
	        /// </summary>
	        [XmlArray("branch_account_cols")]
	        [XmlArrayItem("waybill_branch_account")]
	        public List<WaybillBranchAccountDomain> BranchAccountCols { get; set; }
	
	        /// <summary>
	        /// 物流服务商ID
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 物流服务商业务类型 1：直营 2：加盟 3：落地配 4：直营带网点
	        /// </summary>
	        [XmlElement("cp_type")]
	        public long CpType { get; set; }
}

    }
}
