using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallItemCalculateHscodeGetResponse.
    /// </summary>
    public class TmallItemCalculateHscodeGetResponse : TopResponse
    {
        /// <summary>
        /// 算法返回预测的hscode数据
        /// </summary>
        [XmlArray("results")]
        [XmlArrayItem("json")]
        public List<string> Results { get; set; }

    }
}
