using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// QimenTradeUsersGetResponse.
    /// </summary>
    public class QimenTradeUsersGetResponse : TopResponse
    {
        /// <summary>
        /// totalCount
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

        /// <summary>
        /// modal
        /// </summary>
        [XmlArray("users")]
        [XmlArrayItem("qimen_user")]
        public List<QimenUserDomain> Users { get; set; }

	/// <summary>
/// QimenUserDomain Data Structure.
/// </summary>
[Serializable]

public class QimenUserDomain : TopObject
{
	        /// <summary>
	        /// gmtCreate
	        /// </summary>
	        [XmlElement("gmt_create")]
	        public string GmtCreate { get; set; }
	
	        /// <summary>
	        /// memo
	        /// </summary>
	        [XmlElement("memo")]
	        public string Memo { get; set; }
	
	        /// <summary>
	        /// sellerNick
	        /// </summary>
	        [XmlElement("seller_nick")]
	        public string SellerNick { get; set; }
}

    }
}
