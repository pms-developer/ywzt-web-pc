using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// RdcAligeniusAutorefundsQueryResponse.
    /// </summary>
    public class RdcAligeniusAutorefundsQueryResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// RefundJobQueryBODomain Data Structure.
/// </summary>
[Serializable]

public class RefundJobQueryBODomain : TopObject
{
	        /// <summary>
	        /// dealerNick
	        /// </summary>
	        [XmlElement("dealer_nick")]
	        public string DealerNick { get; set; }
	
	        /// <summary>
	        /// operateTime
	        /// </summary>
	        [XmlElement("operate_time")]
	        public string OperateTime { get; set; }
	
	        /// <summary>
	        /// refundFee
	        /// </summary>
	        [XmlElement("refund_fee")]
	        public long RefundFee { get; set; }
	
	        /// <summary>
	        /// refundId
	        /// </summary>
	        [XmlElement("refund_id")]
	        public long RefundId { get; set; }
	
	        /// <summary>
	        /// status
	        /// </summary>
	        [XmlElement("status")]
	        public long Status { get; set; }
}

	/// <summary>
/// ResultdataDomain Data Structure.
/// </summary>
[Serializable]

public class ResultdataDomain : TopObject
{
	        /// <summary>
	        /// list
	        /// </summary>
	        [XmlArray("list")]
	        [XmlArrayItem("refund_job_query_b_o")]
	        public List<RefundJobQueryBODomain> List { get; set; }
	
	        /// <summary>
	        /// total
	        /// </summary>
	        [XmlElement("total")]
	        public long Total { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorInfo
	        /// </summary>
	        [XmlElement("error_info")]
	        public string ErrorInfo { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public ResultdataDomain ResultData { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
