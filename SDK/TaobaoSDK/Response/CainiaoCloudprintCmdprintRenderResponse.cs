using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoCloudprintCmdprintRenderResponse.
    /// </summary>
    public class CainiaoCloudprintCmdprintRenderResponse : TopResponse
    {
        /// <summary>
        /// 指令集内容串
        /// </summary>
        [XmlElement("cmd_content")]
        public string CmdContent { get; set; }

        /// <summary>
        /// 指令集编码方式：origin-原串 gzip-采用gzip压缩并base64编码
        /// </summary>
        [XmlElement("cmd_encoding")]
        public string CmdEncoding { get; set; }

        /// <summary>
        /// 0成功，非0失败
        /// </summary>
        [XmlElement("ret_code")]
        public string RetCode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("ret_msg")]
        public string RetMsg { get; set; }

    }
}
