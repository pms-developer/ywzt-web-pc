using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreCollectconfigGetResponse.
    /// </summary>
    public class OmniorderStoreCollectconfigGetResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// StoreCollectConfigDomain Data Structure.
/// </summary>
[Serializable]

public class StoreCollectConfigDomain : TopObject
{
	        /// <summary>
	        /// 是否是活动期
	        /// </summary>
	        [XmlElement("activity")]
	        public bool Activity { get; set; }
	
	        /// <summary>
	        /// 当activity为true时返回，活动结束时间
	        /// </summary>
	        [XmlElement("activity_end_time")]
	        public string ActivityEndTime { get; set; }
	
	        /// <summary>
	        /// 当activity为true时返回,活动开始时间
	        /// </summary>
	        [XmlElement("activity_start_time")]
	        public string ActivityStartTime { get; set; }
	
	        /// <summary>
	        /// 每日接单阈值
	        /// </summary>
	        [XmlElement("collect_threshold")]
	        public long CollectThreshold { get; set; }
	
	        /// <summary>
	        /// 接单时间段，格式为 "09:00-12:00", "" 表示一直开启
	        /// </summary>
	        [XmlElement("working_time")]
	        public string WorkingTime { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// code
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// data
	        /// </summary>
	        [XmlElement("data")]
	        public StoreCollectConfigDomain Data { get; set; }
	
	        /// <summary>
	        /// message
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
