using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallItemSimpleschemaAddResponse.
    /// </summary>
    public class TmallItemSimpleschemaAddResponse : TopResponse
    {
        /// <summary>
        /// 商品最后发布时间。
        /// </summary>
        [XmlElement("gmt_modified")]
        public string GmtModified { get; set; }

        /// <summary>
        /// 发布成功后返回商品ID
        /// </summary>
        [XmlElement("result")]
        public string Result { get; set; }

    }
}
