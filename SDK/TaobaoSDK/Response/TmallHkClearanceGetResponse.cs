using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallHkClearanceGetResponse.
    /// </summary>
    public class TmallHkClearanceGetResponse : TopResponse
    {
        /// <summary>
        /// 查询结果对象
        /// </summary>
        [XmlElement("result")]
        public CertifyQueryResultDomain Result { get; set; }

	/// <summary>
/// ConsigneeCertifyInfoDomain Data Structure.
/// </summary>
[Serializable]

public class ConsigneeCertifyInfoDomain : TopObject
{
	        /// <summary>
	        /// 身份证正面
	        /// </summary>
	        [XmlElement("credential1")]
	        public string Credential1 { get; set; }
	
	        /// <summary>
	        /// 身份证背面
	        /// </summary>
	        [XmlElement("credential2")]
	        public string Credential2 { get; set; }
	
	        /// <summary>
	        /// 有效期截止日期
	        /// </summary>
	        [XmlElement("ocr_exp")]
	        public string OcrExp { get; set; }
	
	        /// <summary>
	        /// 身份证号
	        /// </summary>
	        [XmlElement("ocr_id")]
	        public string OcrId { get; set; }
	
	        /// <summary>
	        /// 姓名
	        /// </summary>
	        [XmlElement("ocr_name")]
	        public string OcrName { get; set; }
	
	        /// <summary>
	        /// 订单号
	        /// </summary>
	        [XmlElement("order_id")]
	        public long OrderId { get; set; }
	
	        /// <summary>
	        /// 证件类型
	        /// </summary>
	        [XmlElement("type")]
	        public long Type { get; set; }
}

	/// <summary>
/// CertifyQueryResultDomain Data Structure.
/// </summary>
[Serializable]

public class CertifyQueryResultDomain : TopObject
{
	        /// <summary>
	        /// 错误代码
	        /// </summary>
	        [XmlElement("err_code")]
	        public string ErrCode { get; set; }
	
	        /// <summary>
	        /// 错误原因
	        /// </summary>
	        [XmlElement("err_msg")]
	        public string ErrMsg { get; set; }
	
	        /// <summary>
	        /// 清关信息对象
	        /// </summary>
	        [XmlElement("module")]
	        public ConsigneeCertifyInfoDomain Module { get; set; }
	
	        /// <summary>
	        /// 查询是否成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
