using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderGuideDataGetResponse.
    /// </summary>
    public class OmniorderGuideDataGetResponse : TopResponse
    {
        /// <summary>
        /// 拉取的数据数组，如果为空，表示数据拉取完毕。拉取的数据字段包括打点时间、商家id、商品id和门店id等，传入的类型不同，返回的字段有所不同，可以根据具体类型的返回结果具体处理
        /// </summary>
        [XmlArray("data_list")]
        [XmlArrayItem("json")]
        public List<string> DataList { get; set; }

    }
}
