using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// GearfactoryInterceptOrderCallbackResponse.
    /// </summary>
    public class GearfactoryInterceptOrderCallbackResponse : TopResponse
    {
        /// <summary>
        /// 截单结果
        /// </summary>
        [XmlElement("result")]
        public ApiResultDomain Result { get; set; }

	/// <summary>
/// ApiResultDomain Data Structure.
/// </summary>
[Serializable]

public class ApiResultDomain : TopObject
{
	        /// <summary>
	        /// 错误码："4001":"参数无效", "4002":"截单信息不存在", "4003":"erp截单超时", "4004":"服务不可用", "4005":"需要商家授权"
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息，同错误码部分
	        /// </summary>
	        [XmlElement("error_msg")]
	        public string ErrorMsg { get; set; }
	
	        /// <summary>
	        /// 成功标识，true：成功，false：失败
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
