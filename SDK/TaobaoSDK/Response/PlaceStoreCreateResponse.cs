using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// PlaceStoreCreateResponse.
    /// </summary>
    public class PlaceStoreCreateResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("store_id")]
        public long StoreId { get; set; }

    }
}
