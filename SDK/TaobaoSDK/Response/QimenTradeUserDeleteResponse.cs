using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// QimenTradeUserDeleteResponse.
    /// </summary>
    public class QimenTradeUserDeleteResponse : TopResponse
    {
        /// <summary>
        /// modal
        /// </summary>
        [XmlElement("modal")]
        public bool Modal { get; set; }

    }
}
