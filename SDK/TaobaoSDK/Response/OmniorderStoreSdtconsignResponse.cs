using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreSdtconsignResponse.
    /// </summary>
    public class OmniorderStoreSdtconsignResponse : TopResponse
    {
        /// <summary>
        /// data
        /// </summary>
        [XmlElement("data")]
        public SdtConsignResponseDomain Data { get; set; }

        /// <summary>
        /// 异常码 0 为正常，否则异常
        /// </summary>
        [XmlElement("err_code")]
        public string ErrCodeField { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// SdtConsignResponseDomain Data Structure.
/// </summary>
[Serializable]

public class SdtConsignResponseDomain : TopObject
{
	        /// <summary>
	        /// 接单公司
	        /// </summary>
	        [XmlElement("company_name")]
	        public string CompanyName { get; set; }
	
	        /// <summary>
	        /// 接单小件员姓名
	        /// </summary>
	        [XmlElement("delivery_user_name")]
	        public string DeliveryUserName { get; set; }
	
	        /// <summary>
	        /// 接单小件员电话
	        /// </summary>
	        [XmlElement("delivery_user_phone")]
	        public string DeliveryUserPhone { get; set; }
	
	        /// <summary>
	        /// 接单网点
	        /// </summary>
	        [XmlElement("dot_name")]
	        public string DotName { get; set; }
	
	        /// <summary>
	        /// 物流订单号list
	        /// </summary>
	        [XmlElement("lp_code_list")]
	        public string LpCodeList { get; set; }
	
	        /// <summary>
	        /// 包裹id
	        /// </summary>
	        [XmlElement("package_id")]
	        public long PackageId { get; set; }
	
	        /// <summary>
	        /// 商家id
	        /// </summary>
	        [XmlElement("seller_id")]
	        public long SellerId { get; set; }
}

    }
}
