using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliverySellerStatusIQueryResponse.
    /// </summary>
    public class CainiaoSmartdeliverySellerStatusIQueryResponse : TopResponse
    {
        /// <summary>
        /// 商家状态
        /// </summary>
        [XmlElement("seller_status")]
        public SellerStatusDomain SellerStatus { get; set; }

	/// <summary>
/// SellerStatusDomain Data Structure.
/// </summary>
[Serializable]

public class SellerStatusDomain : TopObject
{
	        /// <summary>
	        /// 状态：NO_SUBSCRIBE:未订购；NO_SETTING:订购未设置;NO_USING:订购未使用；USING:已使用;LOST:已流失；
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 是否订购
	        /// </summary>
	        [XmlElement("subscribe")]
	        public bool Subscribe { get; set; }
}

    }
}
