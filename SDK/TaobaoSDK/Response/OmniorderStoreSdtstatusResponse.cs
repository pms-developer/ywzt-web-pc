using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreSdtstatusResponse.
    /// </summary>
    public class OmniorderStoreSdtstatusResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// SdtStatusResponseDomain Data Structure.
/// </summary>
[Serializable]

public class SdtStatusResponseDomain : TopObject
{
	        /// <summary>
	        /// packageId
	        /// </summary>
	        [XmlElement("order_id")]
	        public long OrderId { get; set; }
	
	        /// <summary>
	        /// 取消原因
	        /// </summary>
	        [XmlElement("reason_desc")]
	        public string ReasonDesc { get; set; }
	
	        /// <summary>
	        /// 状态 0 取号，1 已发货 -1 商家取消 -2 运力端取消
	        /// </summary>
	        [XmlElement("status")]
	        public long Status { get; set; }
	
	        /// <summary>
	        /// 卖家ID，通sellerID
	        /// </summary>
	        [XmlElement("user_id")]
	        public long UserId { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// data
	        /// </summary>
	        [XmlElement("data")]
	        public SdtStatusResponseDomain Data { get; set; }
	
	        /// <summary>
	        /// 异常码 0  正常，否则异常
	        /// </summary>
	        [XmlElement("err_code")]
	        public string ErrCode { get; set; }
	
	        /// <summary>
	        /// 异常信息
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
