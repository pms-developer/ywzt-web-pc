using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// NextoneLogisticsWarehouseUpdateResponse.
    /// </summary>
    public class NextoneLogisticsWarehouseUpdateResponse : TopResponse
    {
        /// <summary>
        /// errorCode
        /// </summary>
        [XmlElement("err_code")]
        public string ErrCodeField { get; set; }

        /// <summary>
        /// errorInfo
        /// </summary>
        [XmlElement("err_info")]
        public string ErrInfo { get; set; }

        /// <summary>
        /// resultData
        /// </summary>
        [XmlElement("result_data")]
        public string ResultData { get; set; }

        /// <summary>
        /// success
        /// </summary>
        [XmlElement("succeed")]
        public bool Succeed { get; set; }

    }
}
