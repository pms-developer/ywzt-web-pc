using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreRefusedResponse.
    /// </summary>
    public class OmniorderStoreRefusedResponse : TopResponse
    {
        /// <summary>
        /// 正常为0,其他表示异常
        /// </summary>
        [XmlElement("err_code")]
        public string ErrCodeField { get; set; }

        /// <summary>
        /// message
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

    }
}
