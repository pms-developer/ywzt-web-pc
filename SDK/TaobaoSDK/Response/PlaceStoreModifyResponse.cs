using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// PlaceStoreModifyResponse.
    /// </summary>
    public class PlaceStoreModifyResponse : TopResponse
    {
        /// <summary>
        /// 是否修改成功
        /// </summary>
        [XmlElement("result")]
        public bool Result { get; set; }

    }
}
