using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// QimenItemsTagQueryResponse.
    /// </summary>
    public class QimenItemsTagQueryResponse : TopResponse
    {
        /// <summary>
        /// flag
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// itemTags
        /// </summary>
        [XmlArray("item_tags")]
        [XmlArrayItem("item_tag")]
        public List<ItemTagDomain> ItemTags { get; set; }

        /// <summary>
        /// message
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// ItemTagDomain Data Structure.
/// </summary>
[Serializable]

public class ItemTagDomain : TopObject
{
	        /// <summary>
	        /// itemId
	        /// </summary>
	        [XmlElement("item_id")]
	        public long ItemId { get; set; }
	
	        /// <summary>
	        /// tagType
	        /// </summary>
	        [XmlElement("tag_type")]
	        public string TagType { get; set; }
}

    }
}
