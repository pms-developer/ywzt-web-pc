using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliveryStrategyWarehouseIDeleteResponse.
    /// </summary>
    public class CainiaoSmartdeliveryStrategyWarehouseIDeleteResponse : TopResponse
    {
        /// <summary>
        /// data
        /// </summary>
        [XmlElement("is_delete_success")]
        public bool IsDeleteSuccess { get; set; }

    }
}
