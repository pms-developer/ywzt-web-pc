using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoMerchantInventoryAdjustResponse.
    /// </summary>
    public class CainiaoMerchantInventoryAdjustResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public SingleResultDtoDomain Result { get; set; }

	/// <summary>
/// SingleResultDtoDomain Data Structure.
/// </summary>
[Serializable]

public class SingleResultDtoDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("flag")]
	        public bool Flag { get; set; }
}

    }
}
