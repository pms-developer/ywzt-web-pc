using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliveryIssubscribeIQueryResponse.
    /// </summary>
    public class CainiaoSmartdeliveryIssubscribeIQueryResponse : TopResponse
    {
        /// <summary>
        /// true:商家已订购智能发货引擎服务,false:商家还没有订购或订购已过期
        /// </summary>
        [XmlElement("successful")]
        public bool Successful { get; set; }

    }
}
