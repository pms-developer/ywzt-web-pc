using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrFulfillLogisticsQueryResponse.
    /// </summary>
    public class TmallNrFulfillLogisticsQueryResponse : TopResponse
    {
        /// <summary>
        /// 返回对象
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrDeliveryBriefStatusDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrDeliveryBriefStatusDtoDomain : TopObject
{
	        /// <summary>
	        /// 快递员姓名
	        /// </summary>
	        [XmlElement("deliverer_name")]
	        public string DelivererName { get; set; }
	
	        /// <summary>
	        /// 快递员电话
	        /// </summary>
	        [XmlElement("deliverer_phone")]
	        public string DelivererPhone { get; set; }
	
	        /// <summary>
	        /// 取件失败的code
	        /// </summary>
	        [XmlElement("fail_code")]
	        public string FailCode { get; set; }
	
	        /// <summary>
	        /// 取件失败的原因
	        /// </summary>
	        [XmlElement("fail_reason")]
	        public string FailReason { get; set; }
	
	        /// <summary>
	        /// 包含[CREATE,GRASP,GOT,DELIVERYED]
	        /// </summary>
	        [XmlElement("logistics_status")]
	        public string LogisticsStatus { get; set; }
	
	        /// <summary>
	        /// logisticsStatusName状态的说明包含[下单,接单,取件,妥投,拒收，取消]
	        /// </summary>
	        [XmlElement("logistics_status_name")]
	        public string LogisticsStatusName { get; set; }
	
	        /// <summary>
	        /// 状态产生时间
	        /// </summary>
	        [XmlElement("logistics_time")]
	        public string LogisticsTime { get; set; }
	
	        /// <summary>
	        /// 服务商的cp
	        /// </summary>
	        [XmlElement("sp_name")]
	        public string SpName { get; set; }
}

	/// <summary>
/// NrTimingFulfillDetailQueryRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrTimingFulfillDetailQueryRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 当前状态
	        /// </summary>
	        [XmlElement("nr_delivery_brief_status_d_t_o")]
	        public NrDeliveryBriefStatusDtoDomain NrDeliveryBriefStatusDTO { get; set; }
	
	        /// <summary>
	        /// 历史状态
	        /// </summary>
	        [XmlArray("nr_delivery_brief_status_d_t_os")]
	        [XmlArrayItem("nr_delivery_brief_status_dto")]
	        public List<NrDeliveryBriefStatusDtoDomain> NrDeliveryBriefStatusDTOs { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// isSuccess
	        /// </summary>
	        [XmlElement("is_success")]
	        public bool IsSuccess { get; set; }
	
	        /// <summary>
	        /// 返回数据
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrTimingFulfillDetailQueryRespDtoDomain ResultData { get; set; }
}

    }
}
