using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoEndpointLockerTopOrderNoticesendQueryResponse.
    /// </summary>
    public class CainiaoEndpointLockerTopOrderNoticesendQueryResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public SingleResultDomain Result { get; set; }

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 快递公司编码
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 快递公司名称
	        /// </summary>
	        [XmlElement("cp_name")]
	        public string CpName { get; set; }
	
	        /// <summary>
	        /// 用于返回淘系包裹脱密手机号用作数据核对
	        /// </summary>
	        [XmlElement("getter_phone")]
	        public string GetterPhone { get; set; }
	
	        /// <summary>
	        /// 裹裹发送通知消息标识，false-?非裹裹发送，true-裹裹发送
	        /// </summary>
	        [XmlElement("guoguo_send_notice_flag")]
	        public bool GuoguoSendNoticeFlag { get; set; }
	
	        /// <summary>
	        /// 是否需要输入手机号，false-不需要，裹裹可以自己判断手机号，true-需要手动输入手机号
	        /// </summary>
	        [XmlElement("need_input_phone")]
	        public bool NeedInputPhone { get; set; }
}

	/// <summary>
/// SingleResultDomain Data Structure.
/// </summary>
[Serializable]

public class SingleResultDomain : TopObject
{
	        /// <summary>
	        /// data
	        /// </summary>
	        [XmlElement("data")]
	        public DataDomain Data { get; set; }
	
	        /// <summary>
	        /// 返回码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_desc")]
	        public string ErrorDesc { get; set; }
	
	        /// <summary>
	        /// 是否成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
