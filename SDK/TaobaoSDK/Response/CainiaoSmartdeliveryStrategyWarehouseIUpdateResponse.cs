using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliveryStrategyWarehouseIUpdateResponse.
    /// </summary>
    public class CainiaoSmartdeliveryStrategyWarehouseIUpdateResponse : TopResponse
    {
        /// <summary>
        /// 仓信息
        /// </summary>
        [XmlElement("warehouse_info")]
        public WarehouseDtoDomain WarehouseInfo { get; set; }

	/// <summary>
/// WarehouseDtoDomain Data Structure.
/// </summary>
[Serializable]

public class WarehouseDtoDomain : TopObject
{
	        /// <summary>
	        /// 仓id
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public long WarehouseId { get; set; }
	
	        /// <summary>
	        /// 仓名称
	        /// </summary>
	        [XmlElement("warehouse_name")]
	        public string WarehouseName { get; set; }
}

    }
}
