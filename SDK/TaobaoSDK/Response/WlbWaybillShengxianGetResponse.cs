using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// WlbWaybillShengxianGetResponse.
    /// </summary>
    public class WlbWaybillShengxianGetResponse : TopResponse
    {
        /// <summary>
        /// 成功后返回的生鲜电子面单信息
        /// </summary>
        [XmlElement("fresh_waybill")]
        public Top.Api.Domain.FreshWaybill FreshWaybill { get; set; }

        /// <summary>
        /// 生成是否成功
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

    }
}
