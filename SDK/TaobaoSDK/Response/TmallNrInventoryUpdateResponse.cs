using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrInventoryUpdateResponse.
    /// </summary>
    public class TmallNrInventoryUpdateResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrInventoryCheckDetailDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrInventoryCheckDetailDtoDomain : TopObject
{
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public long Quantity { get; set; }
	
	        /// <summary>
	        /// 商家商品编码
	        /// </summary>
	        [XmlElement("sc_item_code")]
	        public string ScItemCode { get; set; }
	
	        /// <summary>
	        /// 后端商品的Id号
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public long ScItemId { get; set; }
	
	        /// <summary>
	        /// 幂等值
	        /// </summary>
	        [XmlElement("sub_order_id")]
	        public string SubOrderId { get; set; }
}

	/// <summary>
/// InventoryUpdateRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class InventoryUpdateRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 失败的库存更新记录
	        /// </summary>
	        [XmlArray("fail_inventorys")]
	        [XmlArrayItem("nr_inventory_check_detail_dto")]
	        public List<NrInventoryCheckDetailDtoDomain> FailInventorys { get; set; }
	
	        /// <summary>
	        /// 返回成功的库存记录数
	        /// </summary>
	        [XmlArray("success_inventorys")]
	        [XmlArrayItem("nr_inventory_check_detail_dto")]
	        public List<NrInventoryCheckDetailDtoDomain> SuccessInventorys { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// 更新错误code
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 更新错误信息提示
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// isSuccess
	        /// </summary>
	        [XmlElement("is_success")]
	        public bool IsSuccess { get; set; }
	
	        /// <summary>
	        /// 返回数据
	        /// </summary>
	        [XmlElement("result_data")]
	        public InventoryUpdateRespDtoDomain ResultData { get; set; }
}

    }
}
