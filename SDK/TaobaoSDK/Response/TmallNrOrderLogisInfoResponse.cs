using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrOrderLogisInfoResponse.
    /// </summary>
    public class TmallNrOrderLogisInfoResponse : TopResponse
    {
        /// <summary>
        /// 返回结果实体
        /// </summary>
        [XmlElement("result")]
        public NewRetailResultDomain Result { get; set; }

	/// <summary>
/// NrFetchCodeDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrFetchCodeDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务标识（fn/cn）
	        /// </summary>
	        [XmlElement("biz_type")]
	        public string BizType { get; set; }
	
	        /// <summary>
	        /// 发货公司编码
	        /// </summary>
	        [XmlElement("consign_company_code")]
	        public string ConsignCompanyCode { get; set; }
	
	        /// <summary>
	        /// 发货公司名称
	        /// </summary>
	        [XmlElement("consign_company_name")]
	        public string ConsignCompanyName { get; set; }
	
	        /// <summary>
	        /// 物流cp内部的ID号
	        /// </summary>
	        [XmlElement("cp_out_id")]
	        public string CpOutId { get; set; }
	
	        /// <summary>
	        /// 面单号
	        /// </summary>
	        [XmlElement("face_sheet_id")]
	        public string FaceSheetId { get; set; }
	
	        /// <summary>
	        /// 取件码
	        /// </summary>
	        [XmlElement("fetch_code")]
	        public string FetchCode { get; set; }
	
	        /// <summary>
	        /// 主订单号
	        /// </summary>
	        [XmlElement("main_order_id")]
	        public long MainOrderId { get; set; }
	
	        /// <summary>
	        /// 打印内容
	        /// </summary>
	        [XmlElement("printdata")]
	        public string Printdata { get; set; }
	
	        /// <summary>
	        /// 收货地址
	        /// </summary>
	        [XmlElement("receive_addr")]
	        public string ReceiveAddr { get; set; }
	
	        /// <summary>
	        /// 收货人名称
	        /// </summary>
	        [XmlElement("receive_name")]
	        public string ReceiveName { get; set; }
	
	        /// <summary>
	        /// 收货人的手机号
	        /// </summary>
	        [XmlElement("receive_phone")]
	        public string ReceivePhone { get; set; }
	
	        /// <summary>
	        /// 发货详细地址
	        /// </summary>
	        [XmlElement("send_addr")]
	        public string SendAddr { get; set; }
	
	        /// <summary>
	        /// 发货城市
	        /// </summary>
	        [XmlElement("send_city")]
	        public string SendCity { get; set; }
	
	        /// <summary>
	        /// 发货省份
	        /// </summary>
	        [XmlElement("send_province")]
	        public string SendProvince { get; set; }
	
	        /// <summary>
	        /// 对货码
	        /// </summary>
	        [XmlElement("short_id")]
	        public long ShortId { get; set; }
	
	        /// <summary>
	        /// 菜鸟生成的标签号
	        /// </summary>
	        [XmlElement("tag_no")]
	        public string TagNo { get; set; }
	
	        /// <summary>
	        /// 核销码
	        /// </summary>
	        [XmlElement("write_off_code")]
	        public string WriteOffCode { get; set; }
}

	/// <summary>
/// ErrmsgDomain Data Structure.
/// </summary>
[Serializable]

public class ErrmsgDomain : TopObject
{
	        /// <summary>
	        /// 订单号
	        /// </summary>
	        [XmlElement("key")]
	        public long Key { get; set; }
	
	        /// <summary>
	        /// 错误编码
	        /// </summary>
	        [XmlElement("value")]
	        public string Value { get; set; }
}

	/// <summary>
/// NrFetchCodeQueryRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrFetchCodeQueryRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 返回的订单错误信息映射
	        /// </summary>
	        [XmlElement("err_msg")]
	        public ErrmsgDomain ErrMsg { get; set; }
	
	        /// <summary>
	        /// 实体信息
	        /// </summary>
	        [XmlArray("nr_fetch_code_d_t_o_list")]
	        [XmlArrayItem("nr_fetch_code_dto")]
	        public List<NrFetchCodeDtoDomain> NrFetchCodeDTOList { get; set; }
}

	/// <summary>
/// NewRetailResultDomain Data Structure.
/// </summary>
[Serializable]

public class NewRetailResultDomain : TopObject
{
	        /// <summary>
	        /// 服务错误编码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 服务错误信息
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 返回数据实体
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrFetchCodeQueryRespDtoDomain ResultData { get; set; }
	
	        /// <summary>
	        /// 是否成功
	        /// </summary>
	        [XmlElement("success_flag")]
	        public bool SuccessFlag { get; set; }
}

    }
}
