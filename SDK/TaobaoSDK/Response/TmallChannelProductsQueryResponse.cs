using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallChannelProductsQueryResponse.
    /// </summary>
    public class TmallChannelProductsQueryResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public PageResultDtoDomain Result { get; set; }

	/// <summary>
/// ProductSkuTopDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ProductSkuTopDtoDomain : TopObject
{
	        /// <summary>
	        /// 条形码
	        /// </summary>
	        [XmlElement("bar_code")]
	        public string BarCode { get; set; }
	
	        /// <summary>
	        /// 图片链接
	        /// </summary>
	        [XmlElement("picture_url")]
	        public string PictureUrl { get; set; }
	
	        /// <summary>
	        /// skuId
	        /// </summary>
	        [XmlElement("sku_id")]
	        public long SkuId { get; set; }
	
	        /// <summary>
	        /// sku商家编码
	        /// </summary>
	        [XmlElement("sku_number")]
	        public string SkuNumber { get; set; }
	
	        /// <summary>
	        /// sku后端货品
	        /// </summary>
	        [XmlElement("sku_sc_item_id")]
	        public long SkuScItemId { get; set; }
	
	        /// <summary>
	        /// 基准价
	        /// </summary>
	        [XmlElement("standard_price")]
	        public long StandardPrice { get; set; }
}

	/// <summary>
/// ProductTopDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ProductTopDtoDomain : TopObject
{
	        /// <summary>
	        /// 类目Id
	        /// </summary>
	        [XmlElement("category_id")]
	        public long CategoryId { get; set; }
	
	        /// <summary>
	        /// 产品描述地址
	        /// </summary>
	        [XmlElement("desc_path")]
	        public string DescPath { get; set; }
	
	        /// <summary>
	        /// 产品Id
	        /// </summary>
	        [XmlElement("product_id")]
	        public long ProductId { get; set; }
	
	        /// <summary>
	        /// 产品线ID
	        /// </summary>
	        [XmlElement("product_line_id")]
	        public long ProductLineId { get; set; }
	
	        /// <summary>
	        /// 产品编码
	        /// </summary>
	        [XmlElement("product_number")]
	        public string ProductNumber { get; set; }
	
	        /// <summary>
	        /// 没有sku的情况下，产品对应的后端商品id
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public long ScItemId { get; set; }
	
	        /// <summary>
	        /// sku列表
	        /// </summary>
	        [XmlArray("sku_list")]
	        [XmlArrayItem("product_sku_top_dto")]
	        public List<ProductSkuTopDtoDomain> SkuList { get; set; }
	
	        /// <summary>
	        /// spuId
	        /// </summary>
	        [XmlElement("spu_id")]
	        public long SpuId { get; set; }
	
	        /// <summary>
	        /// 基准价
	        /// </summary>
	        [XmlElement("standard_price")]
	        public long StandardPrice { get; set; }
	
	        /// <summary>
	        /// 供应商Id
	        /// </summary>
	        [XmlElement("supplier_id")]
	        public long SupplierId { get; set; }
	
	        /// <summary>
	        /// 标题
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
}

	/// <summary>
/// PageResultDtoDomain Data Structure.
/// </summary>
[Serializable]

public class PageResultDtoDomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 异常信息
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 是否有下一页
	        /// </summary>
	        [XmlElement("has_next")]
	        public bool HasNext { get; set; }
	
	        /// <summary>
	        /// 产品信息
	        /// </summary>
	        [XmlArray("product_list")]
	        [XmlArrayItem("product_top_dto")]
	        public List<ProductTopDtoDomain> ProductList { get; set; }
	
	        /// <summary>
	        /// 是否查询成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
	
	        /// <summary>
	        /// 总数
	        /// </summary>
	        [XmlElement("total_count")]
	        public long TotalCount { get; set; }
}

    }
}
