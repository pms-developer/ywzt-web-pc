using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// QimenOrderstatusUpdateResponse.
    /// </summary>
    public class QimenOrderstatusUpdateResponse : TopResponse
    {
        /// <summary>
        /// success
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// message
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// resultCode
        /// </summary>
        [XmlElement("result_code")]
        public string ResultCode { get; set; }

    }
}
