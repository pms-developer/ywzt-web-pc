using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliveryCocpsIQueryResponse.
    /// </summary>
    public class CainiaoSmartdeliveryCocpsIQueryResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlArray("smart_delivery_collaborate_cps_info_list")]
        [XmlArrayItem("smart_delivery_collaborate_cps_info")]
        public List<SmartDeliveryCollaborateCpsInfoDomain> SmartDeliveryCollaborateCpsInfoList { get; set; }

	/// <summary>
/// AddressDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDomain : TopObject
{
	        /// <summary>
	        /// city
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// SmartDeliveryCpInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SmartDeliveryCpInfoDomain : TopObject
{
	        /// <summary>
	        /// 网店code
	        /// </summary>
	        [XmlElement("branch_code")]
	        public string BranchCode { get; set; }
	
	        /// <summary>
	        /// 网店名称
	        /// </summary>
	        [XmlElement("branch_name")]
	        public string BranchName { get; set; }
	
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
}

	/// <summary>
/// SmartDeliveryCollaborateCpsInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SmartDeliveryCollaborateCpsInfoDomain : TopObject
{
	        /// <summary>
	        /// 发货地址
	        /// </summary>
	        [XmlElement("send_address")]
	        public AddressDomain SendAddress { get; set; }
	
	        /// <summary>
	        /// 电子面单中智能发货引擎支持的合作物流公司信息
	        /// </summary>
	        [XmlArray("smart_delivery_cp_info_list")]
	        [XmlArrayItem("smart_delivery_cp_info")]
	        public List<SmartDeliveryCpInfoDomain> SmartDeliveryCpInfoList { get; set; }
}

    }
}
