using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreWarehouseResponse.
    /// </summary>
    public class OmniorderStoreWarehouseResponse : TopResponse
    {
        /// <summary>
        /// 成功增加或者更新一条门店与区域仓的关联
        /// </summary>
        [XmlElement("data")]
        public string Data { get; set; }

        /// <summary>
        /// code
        /// </summary>
        [XmlElement("fail_code")]
        public string FailCode { get; set; }

        /// <summary>
        /// message
        /// </summary>
        [XmlElement("fail_message")]
        public string FailMessage { get; set; }

    }
}
