using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallExchangeReceiveGetResponse.
    /// </summary>
    public class TmallExchangeReceiveGetResponse : TopResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("error_codes")]
        public string ErrorCodes { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("error_msg")]
        public string ErrorMsg { get; set; }

        /// <summary>
        /// 所抛出异常
        /// </summary>
        [XmlElement("exception")]
        public string Exception { get; set; }

        /// <summary>
        /// 是否还有下一页
        /// </summary>
        [XmlElement("has_next")]
        public bool HasNext { get; set; }

        /// <summary>
        /// 当前页的换货单数量
        /// </summary>
        [XmlElement("page_results")]
        public long PageResults { get; set; }

        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlArray("results")]
        [XmlArrayItem("exchange")]
        public List<ExchangeDomain> Results { get; set; }

        /// <summary>
        /// 所有符合查询条件的换货单的数量
        /// </summary>
        [XmlElement("total_results")]
        public long TotalResults { get; set; }

	/// <summary>
/// ExchangeDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeDomain : TopObject
{
	        /// <summary>
	        /// 卖家换货地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 支付宝ID
	        /// </summary>
	        [XmlElement("alipay_no")]
	        public string AlipayNo { get; set; }
	
	        /// <summary>
	        /// 正向单号ID
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
	
	        /// <summary>
	        /// 购买的商品sku
	        /// </summary>
	        [XmlElement("bought_sku")]
	        public string BoughtSku { get; set; }
	
	        /// <summary>
	        /// 买家换货地址
	        /// </summary>
	        [XmlElement("buyer_address")]
	        public string BuyerAddress { get; set; }
	
	        /// <summary>
	        /// 买家发货物流公司
	        /// </summary>
	        [XmlElement("buyer_logistic_name")]
	        public string BuyerLogisticName { get; set; }
	
	        /// <summary>
	        /// 买家发货快递单号
	        /// </summary>
	        [XmlElement("buyer_logistic_no")]
	        public string BuyerLogisticNo { get; set; }
	
	        /// <summary>
	        /// buyerName
	        /// </summary>
	        [XmlElement("buyer_name")]
	        public string BuyerName { get; set; }
	
	        /// <summary>
	        /// 买家昵称
	        /// </summary>
	        [XmlElement("buyer_nick")]
	        public string BuyerNick { get; set; }
	
	        /// <summary>
	        /// 买家联系方式
	        /// </summary>
	        [XmlElement("buyer_phone")]
	        public string BuyerPhone { get; set; }
	
	        /// <summary>
	        /// 换货单创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 换货单号ID
	        /// </summary>
	        [XmlElement("dispute_id")]
	        public string DisputeId { get; set; }
	
	        /// <summary>
	        /// 买家申请换货的商品sku
	        /// </summary>
	        [XmlElement("exchange_sku")]
	        public string ExchangeSku { get; set; }
	
	        /// <summary>
	        /// 当前商品状态
	        /// </summary>
	        [XmlElement("good_status")]
	        public string GoodStatus { get; set; }
	
	        /// <summary>
	        /// 换货单最新修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 购买数
	        /// </summary>
	        [XmlElement("num")]
	        public long Num { get; set; }
	
	        /// <summary>
	        /// 支付费用
	        /// </summary>
	        [XmlElement("payment")]
	        public string Payment { get; set; }
	
	        /// <summary>
	        /// 价格
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 申请换货原因
	        /// </summary>
	        [XmlElement("reason")]
	        public string Reason { get; set; }
	
	        /// <summary>
	        /// 卖家发货物流公司
	        /// </summary>
	        [XmlElement("seller_logistic_name")]
	        public string SellerLogisticName { get; set; }
	
	        /// <summary>
	        /// 卖家发货快递单号
	        /// </summary>
	        [XmlElement("seller_logistic_no")]
	        public string SellerLogisticNo { get; set; }
	
	        /// <summary>
	        /// 卖家昵称
	        /// </summary>
	        [XmlElement("seller_nick")]
	        public string SellerNick { get; set; }
	
	        /// <summary>
	        /// 当前换货单状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 超时时间
	        /// </summary>
	        [XmlElement("time_out")]
	        public string TimeOut { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
}

    }
}
