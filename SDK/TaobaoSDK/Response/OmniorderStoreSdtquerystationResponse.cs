using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreSdtquerystationResponse.
    /// </summary>
    public class OmniorderStoreSdtquerystationResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// SdtStationDtoDomain Data Structure.
/// </summary>
[Serializable]

public class SdtStationDtoDomain : TopObject
{
	        /// <summary>
	        /// 站点操作时间
	        /// </summary>
	        [XmlElement("action_time")]
	        public string ActionTime { get; set; }
	
	        /// <summary>
	        /// 快递公司cpcode
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 快递公司名称
	        /// </summary>
	        [XmlElement("cp_name")]
	        public string CpName { get; set; }
	
	        /// <summary>
	        /// 站点code
	        /// </summary>
	        [XmlElement("station_code")]
	        public string StationCode { get; set; }
	
	        /// <summary>
	        /// 站点联系方式
	        /// </summary>
	        [XmlElement("station_contact")]
	        public string StationContact { get; set; }
	
	        /// <summary>
	        /// 站点负责人
	        /// </summary>
	        [XmlElement("station_master")]
	        public string StationMaster { get; set; }
	
	        /// <summary>
	        /// 站点名
	        /// </summary>
	        [XmlElement("station_name")]
	        public string StationName { get; set; }
	
	        /// <summary>
	        /// 站点类别（推荐站点、派送站点、揽收站点）
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
}

	/// <summary>
/// SdtQueryPackageResponseDomain Data Structure.
/// </summary>
[Serializable]

public class SdtQueryPackageResponseDomain : TopObject
{
	        /// <summary>
	        /// 站点信息
	        /// </summary>
	        [XmlArray("stations")]
	        [XmlArrayItem("sdt_station_dto")]
	        public List<SdtStationDtoDomain> Stations { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// data
	        /// </summary>
	        [XmlElement("data")]
	        public SdtQueryPackageResponseDomain Data { get; set; }
	
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("err_code")]
	        public string ErrCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("err_msg")]
	        public string ErrMsg { get; set; }
}

    }
}
