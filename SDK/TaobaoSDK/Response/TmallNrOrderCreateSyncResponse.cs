using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrOrderCreateSyncResponse.
    /// </summary>
    public class TmallNrOrderCreateSyncResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrSyncCreateOrderItemRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrSyncCreateOrderItemRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 商品的实付款，扣减单品优惠之后的金额
	        /// </summary>
	        [XmlElement("actual_fee")]
	        public long ActualFee { get; set; }
	
	        /// <summary>
	        /// 购买数量
	        /// </summary>
	        [XmlElement("buy_amount")]
	        public long BuyAmount { get; set; }
	
	        /// <summary>
	        /// 商品优惠抵扣金额，只包含单品优惠
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public long DiscountFee { get; set; }
	
	        /// <summary>
	        /// 商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public long ItemId { get; set; }
	
	        /// <summary>
	        /// 单品优惠详情，优惠名称:抵扣金额（单位：分）。异步创建订单时此字段为商品入参的promotion字段
	        /// </summary>
	        [XmlElement("item_promotion")]
	        public string ItemPromotion { get; set; }
	
	        /// <summary>
	        /// 商品sku id
	        /// </summary>
	        [XmlElement("sku_id")]
	        public long SkuId { get; set; }
	
	        /// <summary>
	        /// 子订单ID
	        /// </summary>
	        [XmlElement("sub_tid")]
	        public string SubTid { get; set; }
	
	        /// <summary>
	        /// 商品下单使用的金额，同步创建为线上商品金额，异步创建为外部传过来的商品金额，单位：分
	        /// </summary>
	        [XmlElement("unit_price")]
	        public long UnitPrice { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrSyncCreateOrderItemRespDtoDomain ResultData { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
