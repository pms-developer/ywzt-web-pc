using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrNoticeGoodsReadyResponse.
    /// </summary>
    public class TmallNrNoticeGoodsReadyResponse : TopResponse
    {
        /// <summary>
        /// errorCode
        /// </summary>
        [XmlElement("error_code2")]
        public string ErrorCode2 { get; set; }

        /// <summary>
        /// errorMessage
        /// </summary>
        [XmlElement("error_message")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// isSuccess
        /// </summary>
        [XmlElement("is_success")]
        public bool IsSuccess { get; set; }

        /// <summary>
        /// resultData
        /// </summary>
        [XmlElement("result_data")]
        public bool ResultData { get; set; }

    }
}
