using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallDisputeReceiveGetResponse.
    /// </summary>
    public class TmallDisputeReceiveGetResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultSetDomain Result { get; set; }

	/// <summary>
/// DisputeDomain Data Structure.
/// </summary>
[Serializable]

public class DisputeDomain : TopObject
{
	        /// <summary>
	        /// 卖家收货地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 支付宝单号
	        /// </summary>
	        [XmlElement("alipay_no")]
	        public string AlipayNo { get; set; }
	
	        /// <summary>
	        /// 纠纷单上的各项属性
	        /// </summary>
	        [XmlElement("attribute")]
	        public string Attribute { get; set; }
	
	        /// <summary>
	        /// 正向交易单号
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
	
	        /// <summary>
	        /// 买家收货地址（换货）
	        /// </summary>
	        [XmlElement("buyer_address")]
	        public string BuyerAddress { get; set; }
	
	        /// <summary>
	        /// 买家发货物流公司（换货）
	        /// </summary>
	        [XmlElement("buyer_logistic_name")]
	        public string BuyerLogisticName { get; set; }
	
	        /// <summary>
	        /// 买家发货物流单号（换货）
	        /// </summary>
	        [XmlElement("buyer_logistic_no")]
	        public string BuyerLogisticNo { get; set; }
	
	        /// <summary>
	        /// 买家收货人姓名（换货）
	        /// </summary>
	        [XmlElement("buyer_name")]
	        public string BuyerName { get; set; }
	
	        /// <summary>
	        /// 买家昵称
	        /// </summary>
	        [XmlElement("buyer_nick")]
	        public string BuyerNick { get; set; }
	
	        /// <summary>
	        /// 买家联系方式（换货）
	        /// </summary>
	        [XmlElement("buyer_phone")]
	        public string BuyerPhone { get; set; }
	
	        /// <summary>
	        /// 卖家发货物流单号
	        /// </summary>
	        [XmlElement("company_name")]
	        public string CompanyName { get; set; }
	
	        /// <summary>
	        /// 纠纷单创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 纠纷说明
	        /// </summary>
	        [XmlElement("desc")]
	        public string Desc { get; set; }
	
	        /// <summary>
	        /// 纠纷类型，常见的有：仅退款、退货退款、换货
	        /// </summary>
	        [XmlElement("dispute_request")]
	        public string DisputeRequest { get; set; }
	
	        /// <summary>
	        /// 买家退货时间
	        /// </summary>
	        [XmlElement("good_return_time")]
	        public string GoodReturnTime { get; set; }
	
	        /// <summary>
	        /// 货物状态
	        /// </summary>
	        [XmlElement("good_status")]
	        public string GoodStatus { get; set; }
	
	        /// <summary>
	        /// 买家是否需要退货。可选值:true(是),false(否)
	        /// </summary>
	        [XmlElement("has_good_return")]
	        public bool HasGoodReturn { get; set; }
	
	        /// <summary>
	        /// 纠纷单修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 买家购买数量
	        /// </summary>
	        [XmlElement("num")]
	        public long Num { get; set; }
	
	        /// <summary>
	        /// 子订单号。如果是单笔交易oid会等于tid
	        /// </summary>
	        [XmlElement("oid")]
	        public long Oid { get; set; }
	
	        /// <summary>
	        /// 退款对应的订单交易状态。可选值TRADE_NO_CREATE_PAY(没有创建支付宝交易) WAIT_BUYER_PAY(等待买家付款) WAIT_SELLER_SEND_GOODS(等待卖家发货,即:买家已付款) WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货,即:卖家已发货) TRADE_BUYER_SIGNED(买家已签收,货到付款专用) TRADE_FINISHED(交易成功) TRADE_CLOSED(交易关闭) TRADE_CLOSED_BY_TAOBAO(交易被淘宝关闭) ALL_WAIT_PAY(包含：WAIT_BUYER_PAY、TRADE_NO_CREATE_PAY) ALL_CLOSED(包含：TRADE_CLOSED、TRADE_CLOSED_BY_TAOBAO) 取自&quot;http://open.taobao.com/dev/index.php/%E4%BA%A4%E6%98%93%E7%8A%B6%E6%80%81&quot;
	        /// </summary>
	        [XmlElement("order_status")]
	        public string OrderStatus { get; set; }
	
	        /// <summary>
	        /// 退款原因
	        /// </summary>
	        [XmlElement("reason")]
	        public string Reason { get; set; }
	
	        /// <summary>
	        /// 退还金额(退还给买家的金额)。精确到2位小数;单位:元。如:200.07，表示:200元7分
	        /// </summary>
	        [XmlElement("refund_fee")]
	        public string RefundFee { get; set; }
	
	        /// <summary>
	        /// 纠纷单号id
	        /// </summary>
	        [XmlElement("refund_id")]
	        public string RefundId { get; set; }
	
	        /// <summary>
	        /// 退款阶段，可选值：onsale/aftersale
	        /// </summary>
	        [XmlElement("refund_phase")]
	        public string RefundPhase { get; set; }
	
	        /// <summary>
	        /// 卖家发货物流公司（换货）
	        /// </summary>
	        [XmlElement("seller_logistic_name")]
	        public string SellerLogisticName { get; set; }
	
	        /// <summary>
	        /// 卖家发货物流单号（换货）
	        /// </summary>
	        [XmlElement("seller_logistic_no")]
	        public string SellerLogisticNo { get; set; }
	
	        /// <summary>
	        /// 逆向纠纷状态。其中，仅退款/退货退款的状态为：(1, "买家已经申请退款，等待卖家同意"),(2, "卖家已经同意退款，等待买家退货"),(3, "买家已经退货，等待卖家确认收货"),(4, "退款关闭"),(5, "退款成功"),(6, "卖家拒绝退款”),(7, "等待买家确认重新邮寄的货物"),(8, "等待卖家确认退货地址")；换货的状态为：(1, "换货待处理"),(2, "待买家退货"),(3, "买家已退货，待收货"),(4, "换货关闭"),(5, "换货成功"),(6, "待买家修改"),(12, "待发出换货商品"),(13, "待买家收货"),(14, "请退款")
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 超时时间
	        /// </summary>
	        [XmlElement("time_out")]
	        public string TimeOut { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
}

	/// <summary>
/// ResultSetDomain Data Structure.
/// </summary>
[Serializable]

public class ResultSetDomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_msg")]
	        public string ErrorMsg { get; set; }
	
	        /// <summary>
	        /// 所抛出异常
	        /// </summary>
	        [XmlElement("exception")]
	        public string Exception { get; set; }
	
	        /// <summary>
	        /// 是否还有下一页
	        /// </summary>
	        [XmlElement("has_next")]
	        public bool HasNext { get; set; }
	
	        /// <summary>
	        /// 当前页面的纠纷单数量
	        /// </summary>
	        [XmlElement("page_results")]
	        public long PageResults { get; set; }
	
	        /// <summary>
	        /// results
	        /// </summary>
	        [XmlArray("results")]
	        [XmlArrayItem("dispute")]
	        public List<DisputeDomain> Results { get; set; }
	
	        /// <summary>
	        /// 所有符合查询条件的纠纷单数量
	        /// </summary>
	        [XmlElement("total_results")]
	        public long TotalResults { get; set; }
}

    }
}
