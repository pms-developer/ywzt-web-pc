using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderDtdQueryResponse.
    /// </summary>
    public class OmniorderDtdQueryResponse : TopResponse
    {
        /// <summary>
        /// data
        /// </summary>
        [XmlElement("data")]
        public Door2doorQueryResultDomain Data { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 错误码，为0表示成功，非0表示失败
        /// </summary>
        [XmlElement("result_code")]
        public string ResultCode { get; set; }

	/// <summary>
/// Door2doorQueryResultDomain Data Structure.
/// </summary>
[Serializable]

public class Door2doorQueryResultDomain : TopObject
{
	        /// <summary>
	        /// 码对应的淘宝主订单ID
	        /// </summary>
	        [XmlElement("main_order_id")]
	        public long MainOrderId { get; set; }
}

    }
}
