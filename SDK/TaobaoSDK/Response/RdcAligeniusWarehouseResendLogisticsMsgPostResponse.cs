using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// RdcAligeniusWarehouseResendLogisticsMsgPostResponse.
    /// </summary>
    public class RdcAligeniusWarehouseResendLogisticsMsgPostResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// 错误code
	        /// </summary>
	        [XmlElement("fail_code")]
	        public string FailCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("fail_info")]
	        public string FailInfo { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success_flag")]
	        public bool SuccessFlag { get; set; }
}

    }
}
