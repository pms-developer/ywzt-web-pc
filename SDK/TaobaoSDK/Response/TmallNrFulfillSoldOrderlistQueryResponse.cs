using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrFulfillSoldOrderlistQueryResponse.
    /// </summary>
    public class TmallNrFulfillSoldOrderlistQueryResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrZqsPlanDetailInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrZqsPlanDetailInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 计划配送时间
	        /// </summary>
	        [XmlElement("plan_date")]
	        public string PlanDate { get; set; }
	
	        /// <summary>
	        /// 配送期号从1开始，一直到N
	        /// </summary>
	        [XmlElement("sequence_no")]
	        public long SequenceNo { get; set; }
}

	/// <summary>
/// NrZqsPlanRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrZqsPlanRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 每次配送的周期天数（在cycleType为1时生效，其它时候为空）1表示每天送，2表示隔1天送
	        /// </summary>
	        [XmlElement("cycle_days")]
	        public long CycleDays { get; set; }
	
	        /// <summary>
	        /// 配送频率类型:1-隔N天送，2-周末送，3-工作日送
	        /// </summary>
	        [XmlElement("cycle_type")]
	        public long CycleType { get; set; }
	
	        /// <summary>
	        /// 每次配送件数
	        /// </summary>
	        [XmlElement("num_per_cycle")]
	        public long NumPerCycle { get; set; }
	
	        /// <summary>
	        /// 暂停/退款提前告知的天数
	        /// </summary>
	        [XmlElement("pause_ahead_days")]
	        public long PauseAheadDays { get; set; }
	
	        /// <summary>
	        /// 已生成的配送计划序号及配送日期
	        /// </summary>
	        [XmlArray("plan_list")]
	        [XmlArrayItem("nr_zqs_plan_detail_info_dto")]
	        public List<NrZqsPlanDetailInfoDtoDomain> PlanList { get; set; }
	
	        /// <summary>
	        /// 配送时间范围，结束时间，只取时分，HH:mm格式
	        /// </summary>
	        [XmlElement("send_end_time")]
	        public string SendEndTime { get; set; }
	
	        /// <summary>
	        /// 配送时间范围，起送时间，只取时分，HH:mm格式
	        /// </summary>
	        [XmlElement("send_start_time")]
	        public string SendStartTime { get; set; }
	
	        /// <summary>
	        /// 退款开始时间，注意，这个时间当天如果有配送还是会配送的，第二天开始之后的配送会取消
	        /// </summary>
	        [XmlElement("start_refund_date")]
	        public string StartRefundDate { get; set; }
}

	/// <summary>
/// NrOrderDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrOrderDtoDomain : TopObject
{
	        /// <summary>
	        /// 实际付款金额
	        /// </summary>
	        [XmlElement("actual_paid_fee")]
	        public long ActualPaidFee { get; set; }
	
	        /// <summary>
	        /// 修改价格修改的金额
	        /// </summary>
	        [XmlElement("adjust_fee")]
	        public long AdjustFee { get; set; }
	
	        /// <summary>
	        /// 商品价格
	        /// </summary>
	        [XmlElement("auction_price")]
	        public long AuctionPrice { get; set; }
	
	        /// <summary>
	        /// buyerNick
	        /// </summary>
	        [XmlElement("buyer_nick")]
	        public string BuyerNick { get; set; }
	
	        /// <summary>
	        /// createTime
	        /// </summary>
	        [XmlElement("create_time")]
	        public string CreateTime { get; set; }
	
	        /// <summary>
	        /// 店铺优惠的金额
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public long DiscountFee { get; set; }
	
	        /// <summary>
	        /// itemId
	        /// </summary>
	        [XmlElement("item_id")]
	        public long ItemId { get; set; }
	
	        /// <summary>
	        /// 配送计划的详情，仅做周期送业务需要
	        /// </summary>
	        [XmlElement("nr_zqs_plan_resp_d_t_o")]
	        public NrZqsPlanRespDtoDomain NrZqsPlanRespDTO { get; set; }
	
	        /// <summary>
	        /// 数据
	        /// </summary>
	        [XmlElement("num")]
	        public long Num { get; set; }
	
	        /// <summary>
	        /// orderId
	        /// </summary>
	        [XmlElement("order_id")]
	        public long OrderId { get; set; }
	
	        /// <summary>
	        /// 退款状态可选值WAIT_SELLER_AGREE(买家已经申请退款，等待卖家同意)，WAIT_BUYER_RETURN_GOODS(卖家已经同意退款，等待买家退货)，WAIT_SELLER_CONFIRM_GOODS(买家已经退货，等待卖家确认收货)，SELLER_REFUSE_BUYER(卖家拒绝退款)，CLOSED(退款关闭)，SUCCESS(退款成功)
	        /// </summary>
	        [XmlElement("refund_status")]
	        public string RefundStatus { get; set; }
	
	        /// <summary>
	        /// sellerNick
	        /// </summary>
	        [XmlElement("seller_nick")]
	        public string SellerNick { get; set; }
	
	        /// <summary>
	        /// skuId
	        /// </summary>
	        [XmlElement("sku_id")]
	        public long SkuId { get; set; }
	
	        /// <summary>
	        /// title
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
}

	/// <summary>
/// TradeOrderDetailDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrderDetailDtoDomain : TopObject
{
	        /// <summary>
	        /// appointEndTime
	        /// </summary>
	        [XmlElement("appoint_end_time")]
	        public string AppointEndTime { get; set; }
	
	        /// <summary>
	        /// appointStartTime
	        /// </summary>
	        [XmlElement("appoint_start_time")]
	        public string AppointStartTime { get; set; }
	
	        /// <summary>
	        /// buyerNick
	        /// </summary>
	        [XmlElement("buyer_nick")]
	        public string BuyerNick { get; set; }
	
	        /// <summary>
	        /// createTime
	        /// </summary>
	        [XmlElement("create_time")]
	        public string CreateTime { get; set; }
	
	        /// <summary>
	        /// 子订单列表
	        /// </summary>
	        [XmlArray("order_d_t_os")]
	        [XmlArrayItem("nr_order_dto")]
	        public List<NrOrderDtoDomain> OrderDTOs { get; set; }
	
	        /// <summary>
	        /// orderId
	        /// </summary>
	        [XmlElement("order_id")]
	        public string OrderId { get; set; }
	
	        /// <summary>
	        /// payTime
	        /// </summary>
	        [XmlElement("pay_time")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 邮费（单位分）
	        /// </summary>
	        [XmlElement("post_fee")]
	        public string PostFee { get; set; }
	
	        /// <summary>
	        /// 收货地址_未提供
	        /// </summary>
	        [XmlElement("receiver_address")]
	        public string ReceiverAddress { get; set; }
	
	        /// <summary>
	        /// 城市_未提供
	        /// </summary>
	        [XmlElement("receiver_city")]
	        public string ReceiverCity { get; set; }
	
	        /// <summary>
	        /// 区_未提供
	        /// </summary>
	        [XmlElement("receiver_district")]
	        public string ReceiverDistrict { get; set; }
	
	        /// <summary>
	        /// 手机_未提供
	        /// </summary>
	        [XmlElement("receiver_mobile")]
	        public string ReceiverMobile { get; set; }
	
	        /// <summary>
	        /// 收货人_未提供
	        /// </summary>
	        [XmlElement("receiver_name")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 电话_未提供
	        /// </summary>
	        [XmlElement("receiver_phone")]
	        public string ReceiverPhone { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("receiver_province")]
	        public string ReceiverProvince { get; set; }
	
	        /// <summary>
	        /// 镇/街道_未提供
	        /// </summary>
	        [XmlElement("receiver_town")]
	        public string ReceiverTown { get; set; }
	
	        /// <summary>
	        /// sellerNick
	        /// </summary>
	        [XmlElement("seller_nick")]
	        public string SellerNick { get; set; }
	
	        /// <summary>
	        /// 可选值:TRADE_NO_CREATE_PAY(没有创建支付宝交易)，WAIT_BUYER_PAY(等待买家付款)，SELLER_CONSIGNED_PART(卖家部分发货)，WAIT_SELLER_SEND_GOODS(等待卖家发货,即:买家已付款)，WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货,即:卖家已发货)，TRADE_BUYER_SIGNED(买家已签收,货到付款专用)，TRADE_FINISHED(交易成功)，TRADE_CLOSED(付款以后用户退款成功，交易自动关闭)，TRADE_CLOSED_BY_TAOBAO(付款以前，卖家或买家主动关闭交易)，PAY_PENDING(国际信用卡支付付款确认中) * WAIT_PRE_AUTH_CONFIRM(0元购合约中)，PAID_FORBID_CONSIGN(拼团中订单或者发货强管控的订单，已付款但禁止发货)
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// storeCode
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
}

	/// <summary>
/// NrTimingOrderSoldQueryRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrTimingOrderSoldQueryRespDtoDomain : TopObject
{
	        /// <summary>
	        /// pageNo
	        /// </summary>
	        [XmlElement("page_no")]
	        public long PageNo { get; set; }
	
	        /// <summary>
	        /// pageSize
	        /// </summary>
	        [XmlElement("page_size")]
	        public long PageSize { get; set; }
	
	        /// <summary>
	        /// totalNum
	        /// </summary>
	        [XmlElement("total_num")]
	        public long TotalNum { get; set; }
	
	        /// <summary>
	        /// 主订单列表
	        /// </summary>
	        [XmlArray("trade_order_detail_d_t_os")]
	        [XmlArrayItem("trade_order_detail_dto")]
	        public List<TradeOrderDetailDtoDomain> TradeOrderDetailDTOs { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// errorMessage
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// isSuccess
	        /// </summary>
	        [XmlElement("is_success")]
	        public bool IsSuccess { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrTimingOrderSoldQueryRespDtoDomain ResultData { get; set; }
}

    }
}
