using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// RdcAligeniusWarehouseResendUpdateResponse.
    /// </summary>
    public class RdcAligeniusWarehouseResendUpdateResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// errorCode
	        /// </summary>
	        [XmlElement("fail_code")]
	        public string FailCode { get; set; }
	
	        /// <summary>
	        /// errorInfo
	        /// </summary>
	        [XmlElement("fail_info")]
	        public string FailInfo { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success_flag")]
	        public bool SuccessFlag { get; set; }
}

    }
}
