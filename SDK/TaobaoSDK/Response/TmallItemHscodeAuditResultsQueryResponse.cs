using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallItemHscodeAuditResultsQueryResponse.
    /// </summary>
    public class TmallItemHscodeAuditResultsQueryResponse : TopResponse
    {
        /// <summary>
        /// 商品或sku的hscode信息审核状态。
        /// </summary>
        [XmlArray("result_list")]
        [XmlArrayItem("hscode_audit_info")]
        public List<HscodeAuditInfoDomain> ResultList { get; set; }

	/// <summary>
/// HscodeAuditInfoDomain Data Structure.
/// </summary>
[Serializable]

public class HscodeAuditInfoDomain : TopObject
{
	        /// <summary>
	        /// 商品或SKU使用的HS海关代码
	        /// </summary>
	        [XmlElement("hscode")]
	        public string Hscode { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("item_id")]
	        public long ItemId { get; set; }
	
	        /// <summary>
	        /// hscode信息当前审核状态的具体说明
	        /// </summary>
	        [XmlElement("reason")]
	        public string Reason { get; set; }
	
	        /// <summary>
	        /// SKU的ID
	        /// </summary>
	        [XmlElement("sku_id")]
	        public long SkuId { get; set; }
	
	        /// <summary>
	        /// hscode信息当前审核状态，HISTORY_ITEM：历史已上架商品，REJECT：审核未通过，AUDITING：审核中，PASS：审核通过，ERROR：获取审核状态异常
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

    }
}
