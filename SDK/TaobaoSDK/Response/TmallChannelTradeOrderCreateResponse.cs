using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallChannelTradeOrderCreateResponse.
    /// </summary>
    public class TmallChannelTradeOrderCreateResponse : TopResponse
    {
        /// <summary>
        /// 采购单号
        /// </summary>
        [XmlArray("main_purchase_order_list")]
        [XmlArrayItem("json")]
        public List<string> MainPurchaseOrderList { get; set; }

    }
}
