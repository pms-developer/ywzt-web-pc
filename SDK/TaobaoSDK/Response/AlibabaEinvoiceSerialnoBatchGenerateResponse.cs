using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// AlibabaEinvoiceSerialnoBatchGenerateResponse.
    /// </summary>
    public class AlibabaEinvoiceSerialnoBatchGenerateResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlArray("serial_no_list")]
        [XmlArrayItem("string")]
        public List<string> SerialNoList { get; set; }

    }
}
