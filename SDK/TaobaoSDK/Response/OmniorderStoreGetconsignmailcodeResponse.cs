using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStoreGetconsignmailcodeResponse.
    /// </summary>
    public class OmniorderStoreGetconsignmailcodeResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// GetStoreConsignCodeResponseDomain Data Structure.
/// </summary>
[Serializable]

public class GetStoreConsignCodeResponseDomain : TopObject
{
	        /// <summary>
	        /// 面单号
	        /// </summary>
	        [XmlElement("mail_no")]
	        public string MailNo { get; set; }
	
	        /// <summary>
	        /// 包裹Id, 后续发货需要使用
	        /// </summary>
	        [XmlElement("package_id")]
	        public string PackageId { get; set; }
	
	        /// <summary>
	        /// 打印机内容
	        /// </summary>
	        [XmlElement("print_data")]
	        public string PrintData { get; set; }
	
	        /// <summary>
	        /// 菜鸟生成的标签号
	        /// </summary>
	        [XmlElement("tag_code")]
	        public string TagCode { get; set; }
}

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// 发货信息
	        /// </summary>
	        [XmlElement("data")]
	        public GetStoreConsignCodeResponseDomain Data { get; set; }
	
	        /// <summary>
	        /// code
	        /// </summary>
	        [XmlElement("err_code")]
	        public string ErrCode { get; set; }
	
	        /// <summary>
	        /// message
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
