using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// FenxiaoTradePrepayOfflineAddResponse.
    /// </summary>
    public class FenxiaoTradePrepayOfflineAddResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultTopDoDomain Result { get; set; }

	/// <summary>
/// ResultTopDoDomain Data Structure.
/// </summary>
[Serializable]

public class ResultTopDoDomain : TopObject
{
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
