using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderDtdConsumeResponse.
    /// </summary>
    public class OmniorderDtdConsumeResponse : TopResponse
    {
        /// <summary>
        /// 错误西溪
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 错误码，为0表示成功，非0表示失败
        /// </summary>
        [XmlElement("result_code")]
        public string ResultCode { get; set; }

    }
}
