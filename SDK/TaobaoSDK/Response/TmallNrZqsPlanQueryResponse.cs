using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallNrZqsPlanQueryResponse.
    /// </summary>
    public class TmallNrZqsPlanQueryResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public NrResultDomain Result { get; set; }

	/// <summary>
/// NrZqsPlanDetailInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrZqsPlanDetailInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 计划配送时间
	        /// </summary>
	        [XmlElement("plan_date")]
	        public string PlanDate { get; set; }
	
	        /// <summary>
	        /// 配送期号
	        /// </summary>
	        [XmlElement("sequence_no")]
	        public long SequenceNo { get; set; }
}

	/// <summary>
/// NrZqsPauseInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrZqsPauseInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 暂停结束时间，包含该天
	        /// </summary>
	        [XmlElement("pause_end_day")]
	        public string PauseEndDay { get; set; }
	
	        /// <summary>
	        /// 暂停开始时间，包含该天
	        /// </summary>
	        [XmlElement("pause_start_day")]
	        public string PauseStartDay { get; set; }
}

	/// <summary>
/// NrZqsPlanRespDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrZqsPlanRespDtoDomain : TopObject
{
	        /// <summary>
	        /// 每次配送的周期天数（在cycleType为1时生效，其它时候为空），1表示每天送，2表示隔1天送
	        /// </summary>
	        [XmlElement("cycle_days")]
	        public long CycleDays { get; set; }
	
	        /// <summary>
	        /// 配送频率类型:1-隔N天送，2-周末送，3-工作日送
	        /// </summary>
	        [XmlElement("cycle_type")]
	        public long CycleType { get; set; }
	
	        /// <summary>
	        /// 每次配送件数
	        /// </summary>
	        [XmlElement("num_per_cycle")]
	        public long NumPerCycle { get; set; }
	
	        /// <summary>
	        /// 暂停/退款提前告知的天数
	        /// </summary>
	        [XmlElement("pause_ahead_days")]
	        public long PauseAheadDays { get; set; }
	
	        /// <summary>
	        /// pauseInfos
	        /// </summary>
	        [XmlArray("pause_infos")]
	        [XmlArrayItem("nr_zqs_pause_info_dto")]
	        public List<NrZqsPauseInfoDtoDomain> PauseInfos { get; set; }
	
	        /// <summary>
	        /// planList
	        /// </summary>
	        [XmlArray("plan_list")]
	        [XmlArrayItem("nr_zqs_plan_detail_info_dto")]
	        public List<NrZqsPlanDetailInfoDtoDomain> PlanList { get; set; }
	
	        /// <summary>
	        /// 配送时间范围，结束时间，只取时分，HH:mm格式
	        /// </summary>
	        [XmlElement("send_end_time")]
	        public string SendEndTime { get; set; }
	
	        /// <summary>
	        /// 配送时间范围，起送时间，只取时分，HH:mm格式
	        /// </summary>
	        [XmlElement("send_start_time")]
	        public string SendStartTime { get; set; }
	
	        /// <summary>
	        /// 退款开始时间，注意，这个时间当天如果有配送还是会配送的，第二天开始之后的配送会取消
	        /// </summary>
	        [XmlElement("start_refund_date")]
	        public string StartRefundDate { get; set; }
	
	        /// <summary>
	        /// 每周几送，在cycle_type=4时生效，其它时候为空， 1表示周日，2表示周一...7表示周六（以周日为每周的第一天）
	        /// </summary>
	        [XmlElement("week_day")]
	        public long WeekDay { get; set; }
}

	/// <summary>
/// NrResultDomain Data Structure.
/// </summary>
[Serializable]

public class NrResultDomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_message")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 是否成功
	        /// </summary>
	        [XmlElement("is_success")]
	        public bool IsSuccess { get; set; }
	
	        /// <summary>
	        /// resultData
	        /// </summary>
	        [XmlElement("result_data")]
	        public NrZqsPlanRespDtoDomain ResultData { get; set; }
}

    }
}
