using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallItemAddSimpleschemaGetResponse.
    /// </summary>
    public class TmallItemAddSimpleschemaGetResponse : TopResponse
    {
        /// <summary>
        /// 返回发布商品的规则文档
        /// </summary>
        [XmlElement("result")]
        public string Result { get; set; }

    }
}
