using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// QimenItemsMarkingResponse.
    /// </summary>
    public class QimenItemsMarkingResponse : TopResponse
    {
        /// <summary>
        /// flag
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// message
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

    }
}
