using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderStorecollectConsumeResponse.
    /// </summary>
    public class OmniorderStorecollectConsumeResponse : TopResponse
    {
        /// <summary>
        /// 0表示成功
        /// </summary>
        [XmlElement("err_code")]
        public string ErrCodeField { get; set; }

        /// <summary>
        /// 核销错误信息
        /// </summary>
        [XmlElement("err_msg")]
        public string ErrMsgField { get; set; }

    }
}
