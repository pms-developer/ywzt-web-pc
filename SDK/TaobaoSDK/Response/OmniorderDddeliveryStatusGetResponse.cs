using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// OmniorderDddeliveryStatusGetResponse.
    /// </summary>
    public class OmniorderDddeliveryStatusGetResponse : TopResponse
    {
        /// <summary>
        /// result
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// 0或者Success代表请求成功
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 返回点点送运单列表，运单字段含义：deliveryOrderNo: 运单号, title: 配送商品标题, shipperName: 发件人名称, shipperPhone: 发件人电话, storeId: 门店id，商户中心的门店唯一标识, storeName: 门店名称, consigneeName: 收件人名称, consigneePhone: 收件人电话, delivererName: 配送员名称, delivererPhone: 配送员电话, expectedTakeTime: 期望取件时间, expectedDelivery: 期望送达时间, actualDeliveryTime: 实际送达时间, shipAddress: 发件地址, deliveryAddress: 配送地址, orderStatus: 配送状态, orderStatusCode: 配送状态编码, orderStatusDesc: 配送状态描述, orderFrom: 运单来源, orderFromId: 运单来源id, deliverySpId: 配送商id, deliverySpName: 配送商id名称, extension: 扩展信息, item: 配送的宝贝, deliveryFee: 配送费, quantity: 配送数量, actualTakeTime: 实际取件时间, tradeStatus: 订单交易状态, maxBizWaitTime: 最长等待时间, tradeId: 交易订单id, shipperId: 发件人id, consigneeId: 收件人id, paytype: 支付类型, transferCode: 转单码，为纯数字, remindTimes: 催单次数
	        /// </summary>
	        [XmlElement("data")]
	        public string Data { get; set; }
	
	        /// <summary>
	        /// 错误消息(如果发生错误)
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
