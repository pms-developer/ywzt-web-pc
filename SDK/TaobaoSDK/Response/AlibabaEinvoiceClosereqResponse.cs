using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// AlibabaEinvoiceClosereqResponse.
    /// </summary>
    public class AlibabaEinvoiceClosereqResponse : TopResponse
    {
        /// <summary>
        /// 关闭是否成功
        /// </summary>
        [XmlElement("result")]
        public bool Result { get; set; }

    }
}
