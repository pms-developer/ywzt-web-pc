using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallExchangeRefusereasonGetResponse.
    /// </summary>
    public class TmallExchangeRefusereasonGetResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public ResultSetDomain Result { get; set; }

	/// <summary>
/// ReasonDomain Data Structure.
/// </summary>
[Serializable]

public class ReasonDomain : TopObject
{
	        /// <summary>
	        /// 拒绝原因ID
	        /// </summary>
	        [XmlElement("reason_id")]
	        public long ReasonId { get; set; }
	
	        /// <summary>
	        /// 拒绝原因内容
	        /// </summary>
	        [XmlElement("reason_text")]
	        public string ReasonText { get; set; }
}

	/// <summary>
/// ResultSetDomain Data Structure.
/// </summary>
[Serializable]

public class ResultSetDomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("error_code")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("error_msg")]
	        public string ErrorMsg { get; set; }
	
	        /// <summary>
	        /// 异常信息
	        /// </summary>
	        [XmlElement("exception")]
	        public string Exception { get; set; }
	
	        /// <summary>
	        /// 拒绝原因列表
	        /// </summary>
	        [XmlArray("results")]
	        [XmlArrayItem("reason")]
	        public List<ReasonDomain> Results { get; set; }
	
	        /// <summary>
	        /// 拒绝原因总数
	        /// </summary>
	        [XmlElement("total_results")]
	        public long TotalResults { get; set; }
}

    }
}
