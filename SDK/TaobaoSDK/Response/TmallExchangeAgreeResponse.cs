using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallExchangeAgreeResponse.
    /// </summary>
    public class TmallExchangeAgreeResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public ExchangeBaseResponseDomain Result { get; set; }

	/// <summary>
/// ExchangeDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeDomain : TopObject
{
	        /// <summary>
	        /// 正向交易单号ID
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
	
	        /// <summary>
	        /// 换货单号ID
	        /// </summary>
	        [XmlElement("dispute_id")]
	        public string DisputeId { get; set; }
	
	        /// <summary>
	        /// 修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 换货单状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

	/// <summary>
/// ExchangeBaseResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeBaseResponseDomain : TopObject
{
	        /// <summary>
	        /// 换货单信息
	        /// </summary>
	        [XmlElement("exchange")]
	        public ExchangeDomain Exchange { get; set; }
	
	        /// <summary>
	        /// 返回结果说明
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 返回结果码
	        /// </summary>
	        [XmlElement("msg_code")]
	        public string MsgCode { get; set; }
	
	        /// <summary>
	        /// 是否成功调用
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
