using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// TmallCloudstoreTradePosUpdateResponse.
    /// </summary>
    public class TmallCloudstoreTradePosUpdateResponse : TopResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public ResultDoDomain Result { get; set; }

	/// <summary>
/// PosPassBackDtoDomain Data Structure.
/// </summary>
[Serializable]

public class PosPassBackDtoDomain : TopObject
{
	        /// <summary>
	        /// 主订单号
	        /// </summary>
	        [XmlElement("order_id")]
	        public long OrderId { get; set; }
	
	        /// <summary>
	        /// 更新时间
	        /// </summary>
	        [XmlElement("update_time")]
	        public string UpdateTime { get; set; }
}

	/// <summary>
/// ResultDoDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDoDomain : TopObject
{
	        /// <summary>
	        /// 是否成功
	        /// </summary>
	        [XmlElement("data")]
	        public PosPassBackDtoDomain Data { get; set; }
	
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("err_code")]
	        public long ErrCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("err_msg")]
	        public string ErrMsg { get; set; }
	
	        /// <summary>
	        /// 调用是否成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
