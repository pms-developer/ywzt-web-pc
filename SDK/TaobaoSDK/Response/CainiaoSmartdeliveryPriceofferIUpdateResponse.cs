using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Top.Api.Response
{
    /// <summary>
    /// CainiaoSmartdeliveryPriceofferIUpdateResponse.
    /// </summary>
    public class CainiaoSmartdeliveryPriceofferIUpdateResponse : TopResponse
    {
        /// <summary>
        /// 设置是否成功
        /// </summary>
        [XmlElement("successful")]
        public bool Successful { get; set; }

    }
}
