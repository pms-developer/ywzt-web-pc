using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.fenxiao.trade.prepay.offline.add
    /// </summary>
    public class FenxiaoTradePrepayOfflineAddRequest : BaseTopRequest<Top.Api.Response.FenxiaoTradePrepayOfflineAddResponse>
    {
        /// <summary>
        /// 增加流水
        /// </summary>
        public string OfflineAddPrepayParam { get; set; }

        public TopOfflineAddPrepayDtoDomain OfflineAddPrepayParam_ { set { this.OfflineAddPrepayParam = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.fenxiao.trade.prepay.offline.add";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("offline_add_prepay_param", this.OfflineAddPrepayParam);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("offline_add_prepay_param", this.OfflineAddPrepayParam);
        }

	/// <summary>
/// TopOfflineAddPrepayDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TopOfflineAddPrepayDtoDomain : TopObject
{
	        /// <summary>
	        /// 汇票到期日期
	        /// </summary>
	        [XmlElement("accept_date")]
	        public Nullable<DateTime> AcceptDate { get; set; }
	
	        /// <summary>
	        /// 销商淘宝nick
	        /// </summary>
	        [XmlElement("dist_nick")]
	        public string DistNick { get; set; }
	
	        /// <summary>
	        /// 出票人账号
	        /// </summary>
	        [XmlElement("drawer_account_num")]
	        public string DrawerAccountNum { get; set; }
	
	        /// <summary>
	        /// 出票人全称
	        /// </summary>
	        [XmlElement("drawer_full_name")]
	        public string DrawerFullName { get; set; }
	
	        /// <summary>
	        /// 资金流水类型：1.纸质承兑； 2.电子承兑；3.现金；4.优惠返点；5.奖励
	        /// </summary>
	        [XmlElement("flow_type")]
	        public Nullable<long> FlowType { get; set; }
	
	        /// <summary>
	        /// 外部系统支付流水Id，用于商家上传流水时去重(外部保证唯一）
	        /// </summary>
	        [XmlElement("outer_pay_id")]
	        public string OuterPayId { get; set; }
	
	        /// <summary>
	        /// 付款行全称
	        /// </summary>
	        [XmlElement("pay_bank_full_name")]
	        public string PayBankFullName { get; set; }
	
	        /// <summary>
	        /// 付款行行号
	        /// </summary>
	        [XmlElement("pay_bank_num")]
	        public string PayBankNum { get; set; }
	
	        /// <summary>
	        /// 支付时间
	        /// </summary>
	        [XmlElement("pay_time")]
	        public Nullable<DateTime> PayTime { get; set; }
	
	        /// <summary>
	        /// 收款人账号
	        /// </summary>
	        [XmlElement("receiver_account_num")]
	        public string ReceiverAccountNum { get; set; }
	
	        /// <summary>
	        /// 收款开户银行
	        /// </summary>
	        [XmlElement("receiver_bank_full_name")]
	        public string ReceiverBankFullName { get; set; }
	
	        /// <summary>
	        /// 收款人全称
	        /// </summary>
	        [XmlElement("receiver_full_name")]
	        public string ReceiverFullName { get; set; }
	
	        /// <summary>
	        /// 承兑票据号（承兑必填）
	        /// </summary>
	        [XmlElement("ticket_id")]
	        public string TicketId { get; set; }
	
	        /// <summary>
	        /// 出票日期
	        /// </summary>
	        [XmlElement("ticket_issue_date")]
	        public Nullable<DateTime> TicketIssueDate { get; set; }
	
	        /// <summary>
	        /// 金额，单位分（必须为正数）
	        /// </summary>
	        [XmlElement("ticket_money")]
	        public Nullable<long> TicketMoney { get; set; }
}

        #endregion
    }
}
