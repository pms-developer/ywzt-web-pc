using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.tag.items.query
    /// </summary>
    public class QimenTagItemsQueryRequest : BaseTopRequest<Top.Api.Response.QimenTagItemsQueryResponse>
    {
        /// <summary>
        /// 备注，string（500）
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 打标值，string（50），TBKU=同步库存标，MDZT=门店自提标，必填
        /// </summary>
        public string TagType { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.tag.items.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("remark", this.Remark);
            parameters.Add("tag_type", this.TagType);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("tag_type", this.TagType);
        }

        #endregion
    }
}
