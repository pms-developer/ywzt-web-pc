using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.consigned
    /// </summary>
    public class OmniorderStoreConsignedRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreConsignedResponse>
    {
        /// <summary>
        /// 家装物流-安装收货人信息,如果为空,则取默认收货人信息
        /// </summary>
        public string InsReceiverTo { get; set; }

        public JzReceiverDtoDomain InsReceiverTo_ { set { this.InsReceiverTo = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 家装物流-安装公司信息,需要安装时,才填写
        /// </summary>
        public string InsTpDto { get; set; }

        public TpDtoDomain InsTpDto_ { set { this.InsTpDto = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 家装物流-家装收货人信息,如果为空,则取默认收货信息
        /// </summary>
        public string JzReceiverTo { get; set; }

        public JzReceiverDtoDomain JzReceiverTo_ { set { this.JzReceiverTo = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 家装物流-发货参数
        /// </summary>
        public string JzTopArgs { get; set; }

        public JzTopArgsDtoDomain JzTopArgs_ { set { this.JzTopArgs = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 家装物流-物流公司信息
        /// </summary>
        public string LgTpDto { get; set; }

        public TpDtoDomain LgTpDto_ { set { this.LgTpDto = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// ISV系统上报时间
        /// </summary>
        public Nullable<long> ReportTimestamp { get; set; }

        /// <summary>
        /// 卖家联系人地址库ID，可以通过taobao.logistics.address.search接口查询到地址库ID。如果为空，取的卖家的默认取货地址
        /// </summary>
        public Nullable<long> SenderId { get; set; }

        /// <summary>
        /// 子订单列表
        /// </summary>
        public string SubOrderList { get; set; }

        public List<StoreConsignedResultDomain> SubOrderList_ { set { this.SubOrderList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 淘宝交易主订单ID
        /// </summary>
        public Nullable<long> Tid { get; set; }

        /// <summary>
        /// 跟踪Id
        /// </summary>
        public string TraceId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.consigned";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("ins_receiver_to", this.InsReceiverTo);
            parameters.Add("ins_tp_dto", this.InsTpDto);
            parameters.Add("jz_receiver_to", this.JzReceiverTo);
            parameters.Add("jz_top_args", this.JzTopArgs);
            parameters.Add("lg_tp_dto", this.LgTpDto);
            parameters.Add("report_timestamp", this.ReportTimestamp);
            parameters.Add("sender_id", this.SenderId);
            parameters.Add("sub_order_list", this.SubOrderList);
            parameters.Add("tid", this.Tid);
            parameters.Add("trace_id", this.TraceId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("report_timestamp", this.ReportTimestamp);
            RequestValidator.ValidateObjectMaxListSize("sub_order_list", this.SubOrderList, 20);
            RequestValidator.ValidateRequired("tid", this.Tid);
        }

	/// <summary>
/// JzReceiverDtoDomain Data Structure.
/// </summary>
[Serializable]

public class JzReceiverDtoDomain : TopObject
{
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 收货人名称
	        /// </summary>
	        [XmlElement("contact_name")]
	        public string ContactName { get; set; }
	
	        /// <summary>
	        /// 国家
	        /// </summary>
	        [XmlElement("country")]
	        public string Country { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 手机号
	        /// </summary>
	        [XmlElement("mobile_phone")]
	        public string MobilePhone { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("street")]
	        public string Street { get; set; }
	
	        /// <summary>
	        /// 座机号
	        /// </summary>
	        [XmlElement("tele_phone")]
	        public string TelePhone { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("zip_code")]
	        public string ZipCode { get; set; }
}

	/// <summary>
/// StoreConsignedResultDomain Data Structure.
/// </summary>
[Serializable]

public class StoreConsignedResultDomain : TopObject
{
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("attributes")]
	        public string Attributes { get; set; }
	
	        /// <summary>
	        /// 0表示无系统异常
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 物流公司名称
	        /// </summary>
	        [XmlElement("logistic_company")]
	        public string LogisticCompany { get; set; }
	
	        /// <summary>
	        /// 物流公司code，如果id和code都填入，以code为准。点点送：DISTRIBUTOR_12006531；门店自送：DISTRIBUTOR_12709653；如果是菜鸟配送，code和company可以为空
	        /// </summary>
	        [XmlElement("logistic_company_code")]
	        public string LogisticCompanyCode { get; set; }
	
	        /// <summary>
	        /// 物流公司id
	        /// </summary>
	        [XmlElement("logistic_id")]
	        public string LogisticId { get; set; }
	
	        /// <summary>
	        /// 物流单号
	        /// </summary>
	        [XmlElement("logistic_no")]
	        public string LogisticNo { get; set; }
	
	        /// <summary>
	        /// 异常描述
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 操作者
	        /// </summary>
	        [XmlElement("operator")]
	        public string Operator { get; set; }
	
	        /// <summary>
	        /// 速店通packageId
	        /// </summary>
	        [XmlElement("package_id")]
	        public Nullable<long> PackageId { get; set; }
	
	        /// <summary>
	        /// 店铺Id, 可能是门店或者电商仓
	        /// </summary>
	        [XmlElement("store_id")]
	        public string StoreId { get; set; }
	
	        /// <summary>
	        /// 店铺名称
	        /// </summary>
	        [XmlElement("store_name")]
	        public string StoreName { get; set; }
	
	        /// <summary>
	        /// 店铺类型, 门店(Store)或者电商仓(Warehouse)
	        /// </summary>
	        [XmlElement("store_type")]
	        public string StoreType { get; set; }
	
	        /// <summary>
	        /// 子订单Id
	        /// </summary>
	        [XmlElement("sub_oid")]
	        public Nullable<long> SubOid { get; set; }
	
	        /// <summary>
	        /// 主订单Id
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
}

	/// <summary>
/// JzTopArgsDtoDomain Data Structure.
/// </summary>
[Serializable]

public class JzTopArgsDtoDomain : TopObject
{
	        /// <summary>
	        /// 运单号,用快递或商家自有发货时,必填
	        /// </summary>
	        [XmlElement("mail_no")]
	        public string MailNo { get; set; }
	
	        /// <summary>
	        /// 包裹数量
	        /// </summary>
	        [XmlElement("package_number")]
	        public string PackageNumber { get; set; }
	
	        /// <summary>
	        /// 包裹备注
	        /// </summary>
	        [XmlElement("package_remark")]
	        public string PackageRemark { get; set; }
	
	        /// <summary>
	        /// 包裹体积
	        /// </summary>
	        [XmlElement("package_volume")]
	        public string PackageVolume { get; set; }
	
	        /// <summary>
	        /// 包裹重量
	        /// </summary>
	        [XmlElement("package_weight")]
	        public string PackageWeight { get; set; }
	
	        /// <summary>
	        /// 自有物流公司名称
	        /// </summary>
	        [XmlElement("zy_company")]
	        public string ZyCompany { get; set; }
	
	        /// <summary>
	        /// 自有物流发货时间,时间不能早于当前时间
	        /// </summary>
	        [XmlElement("zy_consign_time")]
	        public string ZyConsignTime { get; set; }
	
	        /// <summary>
	        /// 自有物流公司电话
	        /// </summary>
	        [XmlElement("zy_phone_number")]
	        public string ZyPhoneNumber { get; set; }
}

	/// <summary>
/// TpDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TpDtoDomain : TopObject
{
	        /// <summary>
	        /// 公司编码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 公司名称
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
}

        #endregion
    }
}
