using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alibaba.einvoice.paper.print
    /// </summary>
    public class AlibabaEinvoicePaperPrintRequest : BaseTopRequest<Top.Api.Response.AlibabaEinvoicePaperPrintResponse>
    {
        /// <summary>
        /// 打印框设置，0=不弹打印设置框，1=弹出打印设置框
        /// </summary>
        public Nullable<long> DialogSettingFlag { get; set; }

        /// <summary>
        /// 是否强制打印，一般发票只能打印一次，但是因为打印机发票号码与待打印发票号码不一致，导致打印错误，需要重新打印
        /// </summary>
        public Nullable<bool> ForcePrint { get; set; }

        /// <summary>
        /// 销售方纳税人识别号
        /// </summary>
        public string PayeeRegisterNo { get; set; }

        /// <summary>
        /// 打印标记，0=打印发票；1=打印清单。发票明细超过8行时会生成清单页，需要打印清单。
        /// </summary>
        public Nullable<long> PrintFlag { get; set; }

        /// <summary>
        /// 开票流水号
        /// </summary>
        public string SerialNo { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alibaba.einvoice.paper.print";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("dialog_setting_flag", this.DialogSettingFlag);
            parameters.Add("force_print", this.ForcePrint);
            parameters.Add("payee_register_no", this.PayeeRegisterNo);
            parameters.Add("print_flag", this.PrintFlag);
            parameters.Add("serial_no", this.SerialNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("dialog_setting_flag", this.DialogSettingFlag);
            RequestValidator.ValidateRequired("force_print", this.ForcePrint);
            RequestValidator.ValidateRequired("payee_register_no", this.PayeeRegisterNo);
            RequestValidator.ValidateRequired("print_flag", this.PrintFlag);
            RequestValidator.ValidateRequired("serial_no", this.SerialNo);
        }

        #endregion
    }
}
