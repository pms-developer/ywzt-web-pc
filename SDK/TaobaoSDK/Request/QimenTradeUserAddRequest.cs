using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.trade.user.add
    /// </summary>
    public class QimenTradeUserAddRequest : BaseTopRequest<Top.Api.Response.QimenTradeUserAddResponse>
    {
        /// <summary>
        /// 商家备注
        /// </summary>
        public string Memo { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.trade.user.add";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("memo", this.Memo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
