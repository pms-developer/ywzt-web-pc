using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.strategy.warehouse.i.update
    /// </summary>
    public class CainiaoSmartdeliveryStrategyWarehouseIUpdateRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryStrategyWarehouseIUpdateResponse>
    {
        /// <summary>
        /// 智能发货设置请求参数
        /// </summary>
        public string DeliveryStrategySetRequest { get; set; }

        public DeliveryStrategySetRequestDomain DeliveryStrategySetRequest_ { set { this.DeliveryStrategySetRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.strategy.warehouse.i.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("delivery_strategy_set_request", this.DeliveryStrategySetRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("delivery_strategy_set_request", this.DeliveryStrategySetRequest);
        }

	/// <summary>
/// AddressDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// CpInfoDomain Data Structure.
/// </summary>
[Serializable]

public class CpInfoDomain : TopObject
{
	        /// <summary>
	        /// 地址信息
	        /// </summary>
	        [XmlElement("address")]
	        public AddressDomain Address { get; set; }
	
	        /// <summary>
	        /// 云打印模板
	        /// </summary>
	        [XmlElement("cloud_template_id")]
	        public string CloudTemplateId { get; set; }
	
	        /// <summary>
	        /// 快递公司
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 状态: 0-禁用, 1-启用
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
}

	/// <summary>
/// AddressAreaDomain Data Structure.
/// </summary>
[Serializable]

public class AddressAreaDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
}

	/// <summary>
/// SpecialRouteInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SpecialRouteInfoDomain : TopObject
{
	        /// <summary>
	        /// 快递公司code
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 到货区域
	        /// </summary>
	        [XmlElement("receive_area")]
	        public AddressAreaDomain ReceiveArea { get; set; }
}

	/// <summary>
/// DeliveryStrategyInfoDomain Data Structure.
/// </summary>
[Serializable]

public class DeliveryStrategyInfoDomain : TopObject
{
	        /// <summary>
	        /// 识别买家备注: 0-忽略, 1-识别, 2-仅识别合作cp
	        /// </summary>
	        [XmlElement("buyer_message_rule")]
	        public Nullable<long> BuyerMessageRule { get; set; }
	
	        /// <summary>
	        /// 合作CP信息
	        /// </summary>
	        [XmlArray("cocp_info_list")]
	        [XmlArrayItem("cp_info")]
	        public List<CpInfoDomain> CocpInfoList { get; set; }
	
	        /// <summary>
	        /// 特殊线路
	        /// </summary>
	        [XmlArray("special_route_info_list")]
	        [XmlArrayItem("special_route_info")]
	        public List<SpecialRouteInfoDomain> SpecialRouteInfoList { get; set; }
	
	        /// <summary>
	        /// 仓id
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public Nullable<long> WarehouseId { get; set; }
	
	        /// <summary>
	        /// 仓名称
	        /// </summary>
	        [XmlElement("warehouse_name")]
	        public string WarehouseName { get; set; }
}

	/// <summary>
/// DeliveryStrategySetRequestDomain Data Structure.
/// </summary>
[Serializable]

public class DeliveryStrategySetRequestDomain : TopObject
{
	        /// <summary>
	        /// 策略信息对象
	        /// </summary>
	        [XmlElement("delivery_strategy_info")]
	        public DeliveryStrategyInfoDomain DeliveryStrategyInfo { get; set; }
}

        #endregion
    }
}
