using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.storecollect.consume
    /// </summary>
    public class OmniorderStorecollectConsumeRequest : BaseTopRequest<Top.Api.Response.OmniorderStorecollectConsumeResponse>
    {
        /// <summary>
        /// 核销码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 淘宝主订单ID
        /// </summary>
        public Nullable<long> MainOrderId { get; set; }

        /// <summary>
        /// 核销操作人信息
        /// </summary>
        public string Operator { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.storecollect.consume";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("code", this.Code);
            parameters.Add("main_order_id", this.MainOrderId);
            parameters.Add("operator", this.Operator);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("code", this.Code);
            RequestValidator.ValidateRequired("main_order_id", this.MainOrderId);
        }

        #endregion
    }
}
