using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.logistics.order.create
    /// </summary>
    public class LogisticsOrderCreateRequest : BaseTopRequest<Top.Api.Response.LogisticsOrderCreateResponse>
    {
        /// <summary>
        /// 运送的货物名称列表，用|号隔开
        /// </summary>
        public string GoodsNames { get; set; }

        /// <summary>
        /// 运送货物的数量列表，用|号隔开
        /// </summary>
        public string GoodsQuantities { get; set; }

        /// <summary>
        /// 创建订单同时是否进行发货，默认发货。
        /// </summary>
        public Nullable<bool> IsConsign { get; set; }

        /// <summary>
        /// 运送货物的单价列表(注意：单位为分），用|号隔开
        /// </summary>
        public string ItemValues { get; set; }

        /// <summary>
        /// 发货的物流公司代码，如申通=STO，圆通=YTO。is_consign=true时，此项必填。
        /// </summary>
        public string LogisCompanyCode { get; set; }

        /// <summary>
        /// 发货方式,默认为自己联系发货。可选值:ONLINE(在线下单)、OFFLINE(自己联系)。
        /// </summary>
        public string LogisType { get; set; }

        /// <summary>
        /// 发货的物流公司运单号。在logis_type=OFFLINE且is_consign=true时，此项必填。
        /// </summary>
        public string MailNo { get; set; }

        /// <summary>
        /// 物流发货类型：1-普通订单  不填为默认类型 1-普通订单
        /// </summary>
        public Nullable<long> OrderType { get; set; }

        /// <summary>
        /// 卖家旺旺号
        /// </summary>
        public string SellerWangwangId { get; set; }

        /// <summary>
        /// 运费承担方式。1为买家承担运费，2为卖家承担运费，其他值为错误参数。
        /// </summary>
        public Nullable<long> Shipping { get; set; }

        /// <summary>
        /// 订单的交易号码
        /// </summary>
        public Nullable<long> TradeId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.logistics.order.create";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("goods_names", this.GoodsNames);
            parameters.Add("goods_quantities", this.GoodsQuantities);
            parameters.Add("is_consign", this.IsConsign);
            parameters.Add("item_values", this.ItemValues);
            parameters.Add("logis_company_code", this.LogisCompanyCode);
            parameters.Add("logis_type", this.LogisType);
            parameters.Add("mail_no", this.MailNo);
            parameters.Add("order_type", this.OrderType);
            parameters.Add("seller_wangwang_id", this.SellerWangwangId);
            parameters.Add("shipping", this.Shipping);
            parameters.Add("trade_id", this.TradeId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("goods_names", this.GoodsNames);
            RequestValidator.ValidateRequired("goods_quantities", this.GoodsQuantities);
            RequestValidator.ValidateRequired("item_values", this.ItemValues);
            RequestValidator.ValidateRequired("seller_wangwang_id", this.SellerWangwangId);
        }

        #endregion
    }
}
