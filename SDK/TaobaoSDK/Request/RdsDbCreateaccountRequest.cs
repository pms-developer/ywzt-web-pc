using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rds.db.createaccount
    /// </summary>
    public class RdsDbCreateaccountRequest : BaseTopRequest<Top.Api.Response.RdsDbCreateaccountResponse>
    {
        /// <summary>
        /// 入参对象
        /// </summary>
        public string Param0 { get; set; }

        public RequestDbAccountModelDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rds.db.createaccount";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// RequestDbAccountModelDomain Data Structure.
/// </summary>
[Serializable]

public class RequestDbAccountModelDomain : TopObject
{
	        /// <summary>
	        /// 账户描述
	        /// </summary>
	        [XmlElement("account_desc")]
	        public string AccountDesc { get; set; }
	
	        /// <summary>
	        /// 账户名称
	        /// </summary>
	        [XmlElement("account_name")]
	        public string AccountName { get; set; }
	
	        /// <summary>
	        /// 新建db名称
	        /// </summary>
	        [XmlElement("db_name")]
	        public string DbName { get; set; }
	
	        /// <summary>
	        /// rds实例名称
	        /// </summary>
	        [XmlElement("instance_name")]
	        public string InstanceName { get; set; }
	
	        /// <summary>
	        /// 账户密码
	        /// </summary>
	        [XmlElement("password")]
	        public string Password { get; set; }
}

        #endregion
    }
}
