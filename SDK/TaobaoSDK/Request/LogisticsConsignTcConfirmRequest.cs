using System;
using System.Xml.Serialization;
using Top.Api.Domain;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.logistics.consign.tc.confirm
    /// </summary>
    public class LogisticsConsignTcConfirmRequest : BaseTopRequest<Top.Api.Response.LogisticsConsignTcConfirmResponse>
    {
        /// <summary>
        /// ERP的名称
        /// </summary>
        public string AppName { get; set; }

        /// <summary>
        /// 扩展字段 K:V;
        /// </summary>
        public string ExtendFields { get; set; }

        /// <summary>
        /// 发货的包裹
        /// </summary>
        public string GoodsList { get; set; }

        public List<ConfirmConsignGoodsDto> GoodsList_ { set { this.GoodsList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 已发货的外部订单号
        /// </summary>
        public string OutTradeNo { get; set; }

        /// <summary>
        /// 货主id
        /// </summary>
        public Nullable<long> ProviderId { get; set; }

        /// <summary>
        /// 卖家id
        /// </summary>
        public Nullable<long> SellerId { get; set; }

        /// <summary>
        /// 待发货的淘宝主交易号
        /// </summary>
        public Nullable<long> TradeId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.logistics.consign.tc.confirm";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("app_name", this.AppName);
            parameters.Add("extend_fields", this.ExtendFields);
            parameters.Add("goods_list", this.GoodsList);
            parameters.Add("out_trade_no", this.OutTradeNo);
            parameters.Add("provider_id", this.ProviderId);
            parameters.Add("seller_id", this.SellerId);
            parameters.Add("trade_id", this.TradeId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("app_name", this.AppName);
            RequestValidator.ValidateObjectMaxListSize("goods_list", this.GoodsList, 20);
            RequestValidator.ValidateRequired("out_trade_no", this.OutTradeNo);
            RequestValidator.ValidateRequired("provider_id", this.ProviderId);
            RequestValidator.ValidateRequired("seller_id", this.SellerId);
            RequestValidator.ValidateRequired("trade_id", this.TradeId);
        }

        #endregion
    }
}
