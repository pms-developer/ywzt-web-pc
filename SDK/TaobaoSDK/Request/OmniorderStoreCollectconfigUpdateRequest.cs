using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.collectconfig.update
    /// </summary>
    public class OmniorderStoreCollectconfigUpdateRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreCollectconfigUpdateResponse>
    {
        /// <summary>
        /// 门店自提配置
        /// </summary>
        public string StoreCollectConfig { get; set; }

        public StoreCollectConfigDomain StoreCollectConfig_ { set { this.StoreCollectConfig = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 门店ID
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.collectconfig.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("store_collect_config", this.StoreCollectConfig);
            parameters.Add("store_id", this.StoreId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("store_collect_config", this.StoreCollectConfig);
            RequestValidator.ValidateRequired("store_id", this.StoreId);
        }

	/// <summary>
/// StoreCollectConfigDomain Data Structure.
/// </summary>
[Serializable]

public class StoreCollectConfigDomain : TopObject
{
	        /// <summary>
	        /// 是否是活动期
	        /// </summary>
	        [XmlElement("activity")]
	        public Nullable<bool> Activity { get; set; }
	
	        /// <summary>
	        /// 活动开始时间
	        /// </summary>
	        [XmlElement("activity_end_time")]
	        public Nullable<DateTime> ActivityEndTime { get; set; }
	
	        /// <summary>
	        /// 活动结束时间
	        /// </summary>
	        [XmlElement("activity_start_time")]
	        public Nullable<DateTime> ActivityStartTime { get; set; }
	
	        /// <summary>
	        /// 每日接单阈值
	        /// </summary>
	        [XmlElement("collect_threshold")]
	        public Nullable<long> CollectThreshold { get; set; }
	
	        /// <summary>
	        /// 接单时间段，格式为 "09:00-12:00", "" 表示一直开启
	        /// </summary>
	        [XmlElement("working_time")]
	        public string WorkingTime { get; set; }
}

        #endregion
    }
}
