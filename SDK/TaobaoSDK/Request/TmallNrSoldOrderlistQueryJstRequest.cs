using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.sold.orderlist.query.jst
    /// </summary>
    public class TmallNrSoldOrderlistQueryJstRequest : BaseTopRequest<Top.Api.Response.TmallNrSoldOrderlistQueryJstResponse>
    {
        /// <summary>
        /// 入参对象
        /// </summary>
        public string Param0 { get; set; }

        public NrTimingOrderSoldQueryReqDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.sold.orderlist.query.jst";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// NrTimingOrderSoldQueryReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrTimingOrderSoldQueryReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务标识，dss标识定时送业务；jsd表示极速达业务
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 要查询订单数据的商家的sellerID
	        /// </summary>
	        [XmlElement("brand_seller_id")]
	        public Nullable<long> BrandSellerId { get; set; }
	
	        /// <summary>
	        /// 要查询的订单创建结束时间，开始时间和结束时间之间最多相隔72小时
	        /// </summary>
	        [XmlElement("end_created")]
	        public Nullable<DateTime> EndCreated { get; set; }
	
	        /// <summary>
	        /// 页码
	        /// </summary>
	        [XmlElement("page_no")]
	        public Nullable<long> PageNo { get; set; }
	
	        /// <summary>
	        /// 每页大小--当前限制了20
	        /// </summary>
	        [XmlElement("page_size")]
	        public Nullable<long> PageSize { get; set; }
	
	        /// <summary>
	        /// 要查询的订单创建开始时间
	        /// </summary>
	        [XmlElement("start_created")]
	        public Nullable<DateTime> StartCreated { get; set; }
}

        #endregion
    }
}
