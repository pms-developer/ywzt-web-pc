using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rds.db.get
    /// </summary>
    public class RdsDbGetRequest : BaseTopRequest<Top.Api.Response.RdsDbGetResponse>
    {
        /// <summary>
        /// 数据库状态，默认值1
        /// </summary>
        public Nullable<long> DbStatus { get; set; }

        /// <summary>
        /// rds的实例名
        /// </summary>
        public string InstanceName { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rds.db.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("db_status", this.DbStatus);
            parameters.Add("instance_name", this.InstanceName);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxValue("db_status", this.DbStatus, 3);
            RequestValidator.ValidateMinValue("db_status", this.DbStatus, 0);
            RequestValidator.ValidateRequired("instance_name", this.InstanceName);
            RequestValidator.ValidateMaxLength("instance_name", this.InstanceName, 30);
        }

        #endregion
    }
}
