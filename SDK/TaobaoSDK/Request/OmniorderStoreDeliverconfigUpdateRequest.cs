using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.deliverconfig.update
    /// </summary>
    public class OmniorderStoreDeliverconfigUpdateRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreDeliverconfigUpdateResponse>
    {
        /// <summary>
        /// 卖家发货配置
        /// </summary>
        public string StoreDeliverConfig { get; set; }

        public StoreDeliverConfigDomain StoreDeliverConfig_ { set { this.StoreDeliverConfig = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 门店ID
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.deliverconfig.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("store_deliver_config", this.StoreDeliverConfig);
            parameters.Add("store_id", this.StoreId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("store_deliver_config", this.StoreDeliverConfig);
            RequestValidator.ValidateRequired("store_id", this.StoreId);
        }

	/// <summary>
/// StoreDeliverConfigDomain Data Structure.
/// </summary>
[Serializable]

public class StoreDeliverConfigDomain : TopObject
{
	        /// <summary>
	        /// 是否是活动期
	        /// </summary>
	        [XmlElement("activity")]
	        public Nullable<bool> Activity { get; set; }
	
	        /// <summary>
	        /// 活动结束时间
	        /// </summary>
	        [XmlElement("activity_end_time")]
	        public Nullable<DateTime> ActivityEndTime { get; set; }
	
	        /// <summary>
	        /// 活动开始时间
	        /// </summary>
	        [XmlElement("activity_start_time")]
	        public Nullable<DateTime> ActivityStartTime { get; set; }
	
	        /// <summary>
	        /// 每日接单阈值
	        /// </summary>
	        [XmlElement("deliver_threshold")]
	        public Nullable<long> DeliverThreshold { get; set; }
	
	        /// <summary>
	        /// 派单时间，格式为：[{"startTime":"08:40","endTime":"12:20"},{"startTime":"18:00","endTime":"22:00"}]
	        /// </summary>
	        [XmlElement("dispatch_time_range")]
	        public string DispatchTimeRange { get; set; }
	
	        /// <summary>
	        /// 优先级，取值范围为0-10,0最大，10最小
	        /// </summary>
	        [XmlElement("priority")]
	        public Nullable<long> Priority { get; set; }
	
	        /// <summary>
	        /// 接单时间段，格式为 "09:00-12:00", "" 表示一直开启
	        /// </summary>
	        [XmlElement("working_time")]
	        public string WorkingTime { get; set; }
}

        #endregion
    }
}
