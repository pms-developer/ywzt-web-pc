using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.switchstatus.get
    /// </summary>
    public class OmniorderStoreSwitchstatusGetRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreSwitchstatusGetResponse>
    {
        /// <summary>
        /// 卖家ID
        /// </summary>
        public Nullable<long> SellerId { get; set; }

        /// <summary>
        /// 门店ID
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.switchstatus.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("seller_id", this.SellerId);
            parameters.Add("store_id", this.StoreId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("seller_id", this.SellerId);
            RequestValidator.ValidateRequired("store_id", this.StoreId);
        }

        #endregion
    }
}
