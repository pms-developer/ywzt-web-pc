using System;
using System.Collections.Generic;
using Top.Api.Response;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.exchange.agree
    /// </summary>
    public class TmallExchangeAgreeRequest : BaseTopRequest<TmallExchangeAgreeResponse>, ITopUploadRequest<TmallExchangeAgreeResponse>
    {
        /// <summary>
        /// 收货地址id，如需获取请调用该top接口：taobao.logistics.address.search，对应属性为contact_id
        /// </summary>
        public Nullable<long> AddressId { get; set; }

        /// <summary>
        /// 详细收货地址
        /// </summary>
        public string CompleteAddress { get; set; }

        /// <summary>
        /// 换货单号ID
        /// </summary>
        public Nullable<long> DisputeId { get; set; }

        /// <summary>
        /// 返回字段。当前支持的有 dispute_id, bizorder_id, modified, status
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 卖家留言
        /// </summary>
        public string LeaveMessage { get; set; }

        /// <summary>
        /// 上传图片举证
        /// </summary>
        public FileItem LeaveMessagePics { get; set; }

        /// <summary>
        /// 收货人手机号
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 邮政编码
        /// </summary>
        public string Post { get; set; }

        #region BaseTopRequest Members

        public override string GetApiName()
        {
            return "tmall.exchange.agree";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("address_id", this.AddressId);
            parameters.Add("complete_address", this.CompleteAddress);
            parameters.Add("dispute_id", this.DisputeId);
            parameters.Add("fields", this.Fields);
            parameters.Add("leave_message", this.LeaveMessage);
            parameters.Add("mobile", this.Mobile);
            parameters.Add("post", this.Post);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("address_id", this.AddressId);
            RequestValidator.ValidateRequired("dispute_id", this.DisputeId);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxListSize("fields", this.Fields, 20);
        }

        #endregion

        #region ITopUploadRequest Members

        public IDictionary<string, FileItem> GetFileParameters()
        {
            IDictionary<string, FileItem> parameters = new Dictionary<string, FileItem>();
            parameters.Add("leave_message_pics", this.LeaveMessagePics);
            return parameters;
        }

        #endregion
    }
}
