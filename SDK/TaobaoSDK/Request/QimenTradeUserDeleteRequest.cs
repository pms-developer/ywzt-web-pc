using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.trade.user.delete
    /// </summary>
    public class QimenTradeUserDeleteRequest : BaseTopRequest<Top.Api.Response.QimenTradeUserDeleteResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.trade.user.delete";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
