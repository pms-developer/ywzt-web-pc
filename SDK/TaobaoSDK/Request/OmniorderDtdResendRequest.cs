using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.dtd.resend
    /// </summary>
    public class OmniorderDtdResendRequest : BaseTopRequest<Top.Api.Response.OmniorderDtdResendResponse>
    {
        /// <summary>
        /// 淘宝主订单ID
        /// </summary>
        public Nullable<long> MainOrderId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.dtd.resend";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("main_order_id", this.MainOrderId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("main_order_id", this.MainOrderId);
        }

        #endregion
    }
}
