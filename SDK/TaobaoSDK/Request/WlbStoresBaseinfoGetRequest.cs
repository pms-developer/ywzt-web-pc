using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.wlb.stores.baseinfo.get
    /// </summary>
    public class WlbStoresBaseinfoGetRequest : BaseTopRequest<Top.Api.Response.WlbStoresBaseinfoGetResponse>
    {
        /// <summary>
        /// 0.商家仓库.1.菜鸟仓库.2全部被划分的仓库
        /// </summary>
        public Nullable<long> Type { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.wlb.stores.baseinfo.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("type", this.Type);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
