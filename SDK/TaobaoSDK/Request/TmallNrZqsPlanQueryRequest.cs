using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.zqs.plan.query
    /// </summary>
    public class TmallNrZqsPlanQueryRequest : BaseTopRequest<Top.Api.Response.TmallNrZqsPlanQueryResponse>
    {
        /// <summary>
        /// 交易子订单id
        /// </summary>
        public Nullable<long> DetailOrderId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.zqs.plan.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("detail_order_id", this.DetailOrderId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("detail_order_id", this.DetailOrderId);
        }

        #endregion
    }
}
