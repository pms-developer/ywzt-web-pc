using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.cloudstore.trade.pos.update
    /// </summary>
    public class TmallCloudstoreTradePosUpdateRequest : BaseTopRequest<Top.Api.Response.TmallCloudstoreTradePosUpdateResponse>
    {
        /// <summary>
        /// 主订单号
        /// </summary>
        public Nullable<long> OrderId { get; set; }

        /// <summary>
        /// pos单号
        /// </summary>
        public string PosNo { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        public Nullable<DateTime> RequestTime { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string SerialNo { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.cloudstore.trade.pos.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("order_id", this.OrderId);
            parameters.Add("pos_no", this.PosNo);
            parameters.Add("request_time", this.RequestTime);
            parameters.Add("serial_no", this.SerialNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
