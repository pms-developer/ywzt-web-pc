using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.strategy.warehouse.i.delete
    /// </summary>
    public class CainiaoSmartdeliveryStrategyWarehouseIDeleteRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryStrategyWarehouseIDeleteResponse>
    {
        /// <summary>
        /// 仓id
        /// </summary>
        public Nullable<long> WarehouseId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.strategy.warehouse.i.delete";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("warehouse_id", this.WarehouseId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
