using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.warehouse.reverse.event.update
    /// </summary>
    public class RdcAligeniusWarehouseReverseEventUpdateRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusWarehouseReverseEventUpdateResponse>
    {
        /// <summary>
        /// 参数
        /// </summary>
        public string Param0 { get; set; }

        public ReverseEventInfoDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.warehouse.reverse.event.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// ReverseEventInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ReverseEventInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extra")]
	        public string Extra { get; set; }
	
	        /// <summary>
	        /// 销退单ID
	        /// </summary>
	        [XmlElement("id")]
	        public string Id { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 类型(1=销退单状态变更)
	        /// </summary>
	        [XmlElement("type")]
	        public Nullable<long> Type { get; set; }
	
	        /// <summary>
	        /// 值
	        /// </summary>
	        [XmlElement("value")]
	        public string Value { get; set; }
}

        #endregion
    }
}
