using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.exchange.consigngoods
    /// </summary>
    public class TmallExchangeConsigngoodsRequest : BaseTopRequest<Top.Api.Response.TmallExchangeConsigngoodsResponse>
    {
        /// <summary>
        /// 换货单号ID
        /// </summary>
        public Nullable<long> DisputeId { get; set; }

        /// <summary>
        /// 返回字段
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 卖家发货的快递公司
        /// </summary>
        public string LogisticsCompanyName { get; set; }

        /// <summary>
        /// 卖家发货的物流单号
        /// </summary>
        public string LogisticsNo { get; set; }

        /// <summary>
        /// 卖家发货的物流类型，100表示平邮，200表示快递
        /// </summary>
        public Nullable<long> LogisticsType { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.exchange.consigngoods";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("dispute_id", this.DisputeId);
            parameters.Add("fields", this.Fields);
            parameters.Add("logistics_company_name", this.LogisticsCompanyName);
            parameters.Add("logistics_no", this.LogisticsNo);
            parameters.Add("logistics_type", this.LogisticsType);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("dispute_id", this.DisputeId);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxListSize("fields", this.Fields, 100);
            RequestValidator.ValidateRequired("logistics_company_name", this.LogisticsCompanyName);
            RequestValidator.ValidateRequired("logistics_no", this.LogisticsNo);
            RequestValidator.ValidateRequired("logistics_type", this.LogisticsType);
        }

        #endregion
    }
}
