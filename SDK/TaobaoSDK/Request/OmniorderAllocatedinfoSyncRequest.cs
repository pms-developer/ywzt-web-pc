using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.allocatedinfo.sync
    /// </summary>
    public class OmniorderAllocatedinfoSyncRequest : BaseTopRequest<Top.Api.Response.OmniorderAllocatedinfoSyncResponse>
    {
        /// <summary>
        /// 分单结果消息, 如果status为AllocateFail, 则表示失败的理由.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 1231243213213
        /// </summary>
        public Nullable<long> ReportTimestamp { get; set; }

        /// <summary>
        /// 分单状态，如： 等待中(Waiting)，已分单(Allocated)，分单失败(AllocateFail)
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 门店的分单列表
        /// </summary>
        public string SubOrderList { get; set; }

        public List<StoreAllocatedResultDomain> SubOrderList_ { set { this.SubOrderList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 淘宝交易主订单ID
        /// </summary>
        public Nullable<long> Tid { get; set; }

        /// <summary>
        /// 跟踪Id
        /// </summary>
        public string TraceId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.allocatedinfo.sync";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("message", this.Message);
            parameters.Add("report_timestamp", this.ReportTimestamp);
            parameters.Add("status", this.Status);
            parameters.Add("sub_order_list", this.SubOrderList);
            parameters.Add("tid", this.Tid);
            parameters.Add("trace_id", this.TraceId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("report_timestamp", this.ReportTimestamp);
            RequestValidator.ValidateRequired("status", this.Status);
            RequestValidator.ValidateRequired("sub_order_list", this.SubOrderList);
            RequestValidator.ValidateObjectMaxListSize("sub_order_list", this.SubOrderList, 50);
            RequestValidator.ValidateRequired("tid", this.Tid);
        }

	/// <summary>
/// StoreAllocatedResultDomain Data Structure.
/// </summary>
[Serializable]

public class StoreAllocatedResultDomain : TopObject
{
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("attributes")]
	        public string Attributes { get; set; }
	
	        /// <summary>
	        /// 0表示无系统异常
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 异常描述
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 订单分单状态, 可选值: Waiting(仍在派单中) Allocated(派单成功) AllocateFail(派单失败)
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 店铺Id, 可能是门店或者电商仓
	        /// </summary>
	        [XmlElement("store_id")]
	        public string StoreId { get; set; }
	
	        /// <summary>
	        /// 店铺名称
	        /// </summary>
	        [XmlElement("store_name")]
	        public string StoreName { get; set; }
	
	        /// <summary>
	        /// 店铺类型, 门店(Store)或者电商仓(Warehouse)
	        /// </summary>
	        [XmlElement("store_type")]
	        public string StoreType { get; set; }
	
	        /// <summary>
	        /// 子订单Id
	        /// </summary>
	        [XmlElement("sub_oid")]
	        public Nullable<long> SubOid { get; set; }
	
	        /// <summary>
	        /// 主订单Id
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
}

        #endregion
    }
}
