using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.place.store.modify
    /// </summary>
    public class PlaceStoreModifyRequest : BaseTopRequest<Top.Api.Response.PlaceStoreModifyResponse>
    {
        /// <summary>
        /// 门店创建入参
        /// </summary>
        public string StoreUpdate { get; set; }

        public StoreUpdateTopDtoDomain StoreUpdate_ { set { this.StoreUpdate = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.place.store.modify";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("store_update", this.StoreUpdate);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// StoreAdressDtoDomain Data Structure.
/// </summary>
[Serializable]

public class StoreAdressDtoDomain : TopObject
{
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("area")]
	        public string Area { get; set; }
	
	        /// <summary>
	        /// 城市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail_address")]
	        public string DetailAddress { get; set; }
	
	        /// <summary>
	        /// 经度
	        /// </summary>
	        [XmlElement("pos_x")]
	        public string PosX { get; set; }
	
	        /// <summary>
	        /// 维度
	        /// </summary>
	        [XmlElement("pos_y")]
	        public string PosY { get; set; }
	
	        /// <summary>
	        /// 省份
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// StoreKeeperDtoDomain Data Structure.
/// </summary>
[Serializable]

public class StoreKeeperDtoDomain : TopObject
{
	        /// <summary>
	        /// 传真
	        /// </summary>
	        [XmlElement("fax")]
	        public string Fax { get; set; }
	
	        /// <summary>
	        /// 移动电话
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 门店联系人
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 电话
	        /// </summary>
	        [XmlElement("tel")]
	        public string Tel { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("zip_code")]
	        public string ZipCode { get; set; }
}

	/// <summary>
/// StoreUpdateTopDtoDomain Data Structure.
/// </summary>
[Serializable]

public class StoreUpdateTopDtoDomain : TopObject
{
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("description")]
	        public string Description { get; set; }
	
	        /// <summary>
	        /// 门店结束营业时间
	        /// </summary>
	        [XmlElement("end_time")]
	        public string EndTime { get; set; }
	
	        /// <summary>
	        /// 门店主类目
	        /// </summary>
	        [XmlElement("main_category")]
	        public Nullable<long> MainCategory { get; set; }
	
	        /// <summary>
	        /// 门店主名
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 门店外部编码
	        /// </summary>
	        [XmlElement("outer_code")]
	        public string OuterCode { get; set; }
	
	        /// <summary>
	        /// 店铺id
	        /// </summary>
	        [XmlElement("shop_id")]
	        public Nullable<long> ShopId { get; set; }
	
	        /// <summary>
	        /// 门店开始营业时间
	        /// </summary>
	        [XmlElement("start_time")]
	        public string StartTime { get; set; }
	
	        /// <summary>
	        /// 门店状态，枚举值。NORMAL：正常。CLOSE：关店。HOLD: 暂停营业
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 门店地址
	        /// </summary>
	        [XmlElement("store_adress")]
	        public StoreAdressDtoDomain StoreAdress { get; set; }
	
	        /// <summary>
	        /// 门店id
	        /// </summary>
	        [XmlElement("store_id")]
	        public Nullable<long> StoreId { get; set; }
	
	        /// <summary>
	        /// 门店信息
	        /// </summary>
	        [XmlElement("store_keeper")]
	        public StoreKeeperDtoDomain StoreKeeper { get; set; }
	
	        /// <summary>
	        /// 门店类型，枚举值。NORMAL：普通门店。暂时统一使用这个值
	        /// </summary>
	        [XmlElement("store_type")]
	        public string StoreType { get; set; }
	
	        /// <summary>
	        /// 分店名称
	        /// </summary>
	        [XmlElement("sub_name")]
	        public string SubName { get; set; }
}

        #endregion
    }
}
