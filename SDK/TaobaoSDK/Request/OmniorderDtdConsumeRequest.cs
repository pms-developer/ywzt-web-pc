using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.dtd.consume
    /// </summary>
    public class OmniorderDtdConsumeRequest : BaseTopRequest<Top.Api.Response.OmniorderDtdConsumeResponse>
    {
        /// <summary>
        /// 核销信息
        /// </summary>
        public string ParamDoor2doorConsumeRequest { get; set; }

        public Door2doorConsumeRequestDomain ParamDoor2doorConsumeRequest_ { set { this.ParamDoor2doorConsumeRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.dtd.consume";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param_door2door_consume_request", this.ParamDoor2doorConsumeRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// Door2doorConsumeRequestDomain Data Structure.
/// </summary>
[Serializable]

public class Door2doorConsumeRequestDomain : TopObject
{
	        /// <summary>
	        /// 核销码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 淘宝主订单ID
	        /// </summary>
	        [XmlElement("main_order_id")]
	        public Nullable<long> MainOrderId { get; set; }
	
	        /// <summary>
	        /// 操作人
	        /// </summary>
	        [XmlElement("operator")]
	        public string Operator { get; set; }
}

        #endregion
    }
}
