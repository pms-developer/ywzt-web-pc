using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.wlb.waybill.shengxian.get
    /// </summary>
    public class WlbWaybillShengxianGetRequest : BaseTopRequest<Top.Api.Response.WlbWaybillShengxianGetResponse>
    {
        /// <summary>
        /// 物流服务方代码，生鲜配送：YXSR
        /// </summary>
        public string BizCode { get; set; }

        /// <summary>
        /// 物流服务类型。冷链：602，常温：502
        /// </summary>
        public string DeliveryType { get; set; }

        /// <summary>
        /// 预留扩展字段
        /// </summary>
        public string Feature { get; set; }

        /// <summary>
        /// 订单渠道： 淘宝平台订单："TB"; 天猫平台订单："TM"; 京东："JD"; 拍拍："PP"; 易迅平台订单："YX"; 一号店平台订单："YHD"; 当当网平台订单："DD"; EBAY："EBAY"; QQ网购："QQ"; 苏宁："SN"; 国美："GM"; 唯品会："WPH"; 聚美："Jm"; 乐峰："LF"; 蘑菇街："MGJ"; 聚尚："JS"; 银泰："YT"; VANCL："VANCL"; 邮乐："YL"; 优购："YG"; 拍鞋："PX"; 1688平台："1688";
        /// </summary>
        public string OrderChannelsType { get; set; }

        /// <summary>
        /// 商家淘宝地址信息ID
        /// </summary>
        public string SenderAddressId { get; set; }

        /// <summary>
        /// 仓库的服务代码标示，代码一个仓库的实体。(可以通过taobao.wlb.stores.baseinfo.get接口查询)
        /// </summary>
        public string ServiceCode { get; set; }

        /// <summary>
        /// 交易号，传入交易号不能存在拆单场景。
        /// </summary>
        public string TradeId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.wlb.waybill.shengxian.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("biz_code", this.BizCode);
            parameters.Add("delivery_type", this.DeliveryType);
            parameters.Add("feature", this.Feature);
            parameters.Add("order_channels_type", this.OrderChannelsType);
            parameters.Add("sender_address_id", this.SenderAddressId);
            parameters.Add("service_code", this.ServiceCode);
            parameters.Add("trade_id", this.TradeId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("biz_code", this.BizCode);
            RequestValidator.ValidateMaxLength("biz_code", this.BizCode, 64);
            RequestValidator.ValidateRequired("delivery_type", this.DeliveryType);
            RequestValidator.ValidateMaxLength("delivery_type", this.DeliveryType, 64);
            RequestValidator.ValidateMaxLength("feature", this.Feature, 512);
            RequestValidator.ValidateRequired("order_channels_type", this.OrderChannelsType);
            RequestValidator.ValidateMaxLength("order_channels_type", this.OrderChannelsType, 64);
            RequestValidator.ValidateMaxLength("sender_address_id", this.SenderAddressId, 64);
            RequestValidator.ValidateMaxLength("service_code", this.ServiceCode, 128);
            RequestValidator.ValidateRequired("trade_id", this.TradeId);
            RequestValidator.ValidateMaxLength("trade_id", this.TradeId, 128);
        }

        #endregion
    }
}
