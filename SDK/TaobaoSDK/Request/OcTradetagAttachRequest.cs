using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.oc.tradetag.attach
    /// </summary>
    public class OcTradetagAttachRequest : BaseTopRequest<Top.Api.Response.OcTradetagAttachResponse>
    {
        /// <summary>
        /// 标签名称
        /// </summary>
        public string TagName { get; set; }

        /// <summary>
        /// 标签类型       1：官方标签      2：自定义标签
        /// </summary>
        public Nullable<long> TagType { get; set; }

        /// <summary>
        /// 标签值，json格式
        /// </summary>
        public string TagValue { get; set; }

        /// <summary>
        /// 订单id
        /// </summary>
        public Nullable<long> Tid { get; set; }

        /// <summary>
        /// 该标签在消费者端是否显示,0:不显示,1：显示
        /// </summary>
        public Nullable<long> Visible { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.oc.tradetag.attach";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("tag_name", this.TagName);
            parameters.Add("tag_type", this.TagType);
            parameters.Add("tag_value", this.TagValue);
            parameters.Add("tid", this.Tid);
            parameters.Add("visible", this.Visible);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("tag_name", this.TagName);
            RequestValidator.ValidateRequired("tag_value", this.TagValue);
            RequestValidator.ValidateRequired("tid", this.Tid);
        }

        #endregion
    }
}
