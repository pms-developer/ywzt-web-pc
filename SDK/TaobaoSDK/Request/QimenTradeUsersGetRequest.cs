using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.trade.users.get
    /// </summary>
    public class QimenTradeUsersGetRequest : BaseTopRequest<Top.Api.Response.QimenTradeUsersGetResponse>
    {
        /// <summary>
        /// 每页的数量
        /// </summary>
        public Nullable<long> PageIndex { get; set; }

        /// <summary>
        /// 页数
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.trade.users.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("page_index", this.PageIndex);
            parameters.Add("page_size", this.PageSize);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("page_index", this.PageIndex);
            RequestValidator.ValidateRequired("page_size", this.PageSize);
        }

        #endregion
    }
}
