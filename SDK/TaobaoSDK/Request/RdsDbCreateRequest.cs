using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rds.db.create
    /// </summary>
    public class RdsDbCreateRequest : BaseTopRequest<Top.Api.Response.RdsDbCreateResponse>
    {
        /// <summary>
        /// 已存在账号名
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 数据库名
        /// </summary>
        public string DbName { get; set; }

        /// <summary>
        /// rds的实例名
        /// </summary>
        public string InstanceName { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rds.db.create";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("account_name", this.AccountName);
            parameters.Add("db_name", this.DbName);
            parameters.Add("instance_name", this.InstanceName);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxLength("account_name", this.AccountName, 13);
            RequestValidator.ValidateRequired("db_name", this.DbName);
            RequestValidator.ValidateMaxLength("db_name", this.DbName, 64);
            RequestValidator.ValidateRequired("instance_name", this.InstanceName);
            RequestValidator.ValidateMaxLength("instance_name", this.InstanceName, 30);
        }

        #endregion
    }
}
