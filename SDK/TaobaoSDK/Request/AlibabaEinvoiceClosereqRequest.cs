using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alibaba.einvoice.closereq
    /// </summary>
    public class AlibabaEinvoiceClosereqRequest : BaseTopRequest<Top.Api.Response.AlibabaEinvoiceClosereqResponse>
    {
        /// <summary>
        /// 税号
        /// </summary>
        public string PayeeRegisterNo { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string SerialNo { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alibaba.einvoice.closereq";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("payee_register_no", this.PayeeRegisterNo);
            parameters.Add("serial_no", this.SerialNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("payee_register_no", this.PayeeRegisterNo);
            RequestValidator.ValidateRequired("serial_no", this.SerialNo);
        }

        #endregion
    }
}
