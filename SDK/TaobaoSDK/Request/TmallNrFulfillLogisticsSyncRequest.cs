using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.fulfill.logistics.sync
    /// </summary>
    public class TmallNrFulfillLogisticsSyncRequest : BaseTopRequest<Top.Api.Response.TmallNrFulfillLogisticsSyncResponse>
    {
        /// <summary>
        /// 物流回传参数
        /// </summary>
        public string Param0 { get; set; }

        public NrLogisticsInfoSynReqDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.fulfill.logistics.sync";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// NrLogisticsInfoSynReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrLogisticsInfoSynReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务类型（oneHour:一小时达,zqs:周期送,dss:定时送，周期送业务:zqs）如果自配送传入：other
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 取消配送原因
	        /// </summary>
	        [XmlElement("cancel_reason")]
	        public string CancelReason { get; set; }
	
	        /// <summary>
	        /// 取消配送编号
	        /// </summary>
	        [XmlElement("cancel_reason_code")]
	        public string CancelReasonCode { get; set; }
	
	        /// <summary>
	        /// 流转节点的当前城市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 快递业务员联系方式，手机号码或电话。
	        /// </summary>
	        [XmlElement("delivery_phone")]
	        public string DeliveryPhone { get; set; }
	
	        /// <summary>
	        /// 快递员id
	        /// </summary>
	        [XmlElement("delivery_user_id")]
	        public Nullable<long> DeliveryUserId { get; set; }
	
	        /// <summary>
	        /// 快递员的姓名
	        /// </summary>
	        [XmlElement("delivery_user_name")]
	        public string DeliveryUserName { get; set; }
	
	        /// <summary>
	        /// 运力类型，0: 第三方快递员,  1:商家自动
	        /// </summary>
	        [XmlElement("delivery_user_type")]
	        public Nullable<long> DeliveryUserType { get; set; }
	
	        /// <summary>
	        /// 流转节点的详细地址及操作描述
	        /// </summary>
	        [XmlElement("desc")]
	        public string Desc { get; set; }
	
	        /// <summary>
	        /// 事件编码,10:已下发等待接单,20:骑手已接单，待提货,40:揽收,999:妥投,50:拒收,-999:取消
	        /// </summary>
	        [XmlElement("event")]
	        public Nullable<long> Event { get; set; }
	
	        /// <summary>
	        /// 流转节点发生时间
	        /// </summary>
	        [XmlElement("event_create_time")]
	        public Nullable<DateTime> EventCreateTime { get; set; }
	
	        /// <summary>
	        /// 操作人类型:1寄件人,3客服小二,4快递员,5CP,6收件人,100系统
	        /// </summary>
	        [XmlElement("event_oper_type")]
	        public Nullable<long> EventOperType { get; set; }
	
	        /// <summary>
	        /// 站点名称
	        /// </summary>
	        [XmlElement("facility_name")]
	        public string FacilityName { get; set; }
	
	        /// <summary>
	        /// 运单所属CP，自配送传入：OTHER
	        /// </summary>
	        [XmlElement("mail_cp")]
	        public string MailCp { get; set; }
	
	        /// <summary>
	        /// 快递公司名称，自配送传入：其他
	        /// </summary>
	        [XmlElement("mail_cp_name")]
	        public string MailCpName { get; set; }
	
	        /// <summary>
	        /// 快递单号。各个快递公司的运单号格式不同。
	        /// </summary>
	        [XmlElement("mail_no")]
	        public string MailNo { get; set; }
	
	        /// <summary>
	        /// 淘系交易主订单号
	        /// </summary>
	        [XmlElement("main_biz_order_id")]
	        public Nullable<long> MainBizOrderId { get; set; }
	
	        /// <summary>
	        /// 配送日期，周期送业务必传
	        /// </summary>
	        [XmlElement("plan_date")]
	        public string PlanDate { get; set; }
	
	        /// <summary>
	        /// 配送序号
	        /// </summary>
	        [XmlElement("sequence_no")]
	        public Nullable<long> SequenceNo { get; set; }
	
	        /// <summary>
	        /// 淘宝交易子订单号，周期送业务必传
	        /// </summary>
	        [XmlElement("sub_biz_order_id")]
	        public Nullable<long> SubBizOrderId { get; set; }
	
	        /// <summary>
	        /// 请求编号
	        /// </summary>
	        [XmlElement("trace_id")]
	        public string TraceId { get; set; }
}

        #endregion
    }
}
