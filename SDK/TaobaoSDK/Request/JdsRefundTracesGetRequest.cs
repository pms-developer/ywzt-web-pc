using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.jds.refund.traces.get
    /// </summary>
    public class JdsRefundTracesGetRequest : BaseTopRequest<Top.Api.Response.JdsRefundTracesGetResponse>
    {
        /// <summary>
        /// 淘宝的退款编号
        /// </summary>
        public Nullable<long> RefundId { get; set; }

        /// <summary>
        /// 是否返回用户状态(是否存在)
        /// </summary>
        public Nullable<bool> ReturnUserStatus { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.jds.refund.traces.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("refund_id", this.RefundId);
            parameters.Add("return_user_status", this.ReturnUserStatus);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("refund_id", this.RefundId);
            RequestValidator.ValidateMinValue("refund_id", this.RefundId, 1);
        }

        #endregion
    }
}
