using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.inventory.initial
    /// </summary>
    public class TmallNrInventoryInitialRequest : BaseTopRequest<Top.Api.Response.TmallNrInventoryInitialResponse>
    {
        /// <summary>
        /// 请求入参
        /// </summary>
        public string Param0 { get; set; }

        public NrStoreInvItemInitialReqDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.inventory.initial";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// StoreInvetoryDtoDomain Data Structure.
/// </summary>
[Serializable]

public class StoreInvetoryDtoDomain : TopObject
{
	        /// <summary>
	        /// 单据流水号，用于幂等操作
	        /// </summary>
	        [XmlElement("bill_num")]
	        public string BillNum { get; set; }
	
	        /// <summary>
	        /// CERTAINTY 表示确定性库存
	        /// </summary>
	        [XmlElement("inventory_type")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 天猫商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public Nullable<long> ItemId { get; set; }
	
	        /// <summary>
	        /// 商家的外部商品编码，写入值。
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 库存数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public Nullable<long> Quantity { get; set; }
	
	        /// <summary>
	        /// 天猫后端商品id
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public Nullable<long> ScItemId { get; set; }
	
	        /// <summary>
	        /// sku号
	        /// </summary>
	        [XmlElement("sku_id")]
	        public Nullable<long> SkuId { get; set; }
}

	/// <summary>
/// StoreDtoDomain Data Structure.
/// </summary>
[Serializable]

public class StoreDtoDomain : TopObject
{
	        /// <summary>
	        /// 门店商品，最大列表长度：20
	        /// </summary>
	        [XmlArray("store_inventories")]
	        [XmlArrayItem("store_invetory_dto")]
	        public List<StoreInvetoryDtoDomain> StoreInventories { get; set; }
	
	        /// <summary>
	        /// 门店对应的storeID值
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public string WarehouseId { get; set; }
	
	        /// <summary>
	        /// 库存来源的类型；STORE表示门店
	        /// </summary>
	        [XmlElement("warehouse_type")]
	        public string WarehouseType { get; set; }
}

	/// <summary>
/// NrStoreInvItemInitialReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrStoreInvItemInitialReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 操作时间
	        /// </summary>
	        [XmlElement("operation_time")]
	        public string OperationTime { get; set; }
	
	        /// <summary>
	        /// 操作者
	        /// </summary>
	        [XmlElement("operator")]
	        public string Operator { get; set; }
	
	        /// <summary>
	        /// 门店信息
	        /// </summary>
	        [XmlArray("stores")]
	        [XmlArrayItem("store_dto")]
	        public List<StoreDtoDomain> Stores { get; set; }
	
	        /// <summary>
	        /// 如果是品牌商家填写商家的sellerID，如果是零售商需要填写品牌商的sellerID，但是需要平台授权；
	        /// </summary>
	        [XmlElement("user_id")]
	        public Nullable<long> UserId { get; set; }
}

        #endregion
    }
}
