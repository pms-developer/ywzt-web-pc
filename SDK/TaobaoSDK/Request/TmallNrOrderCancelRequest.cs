using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.order.cancel
    /// </summary>
    public class TmallNrOrderCancelRequest : BaseTopRequest<Top.Api.Response.TmallNrOrderCancelResponse>
    {
        /// <summary>
        /// 业务身份标识
        /// </summary>
        public string BizIdentity { get; set; }

        /// <summary>
        /// 买家id
        /// </summary>
        public Nullable<long> BuyerId { get; set; }

        /// <summary>
        /// 门店code
        /// </summary>
        public string StoreCode { get; set; }

        /// <summary>
        /// 线下门店交易单号
        /// </summary>
        public string StoreOrderId { get; set; }

        /// <summary>
        /// 淘系交易订单ID
        /// </summary>
        public string Tid { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.order.cancel";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("biz_identity", this.BizIdentity);
            parameters.Add("buyer_id", this.BuyerId);
            parameters.Add("store_code", this.StoreCode);
            parameters.Add("store_order_id", this.StoreOrderId);
            parameters.Add("tid", this.Tid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("biz_identity", this.BizIdentity);
            RequestValidator.ValidateRequired("store_code", this.StoreCode);
            RequestValidator.ValidateRequired("store_order_id", this.StoreOrderId);
            RequestValidator.ValidateRequired("tid", this.Tid);
        }

        #endregion
    }
}
