using System;
using System.Collections.Generic;
using Top.Api.Response;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.exchange.returngoods.refuse
    /// </summary>
    public class TmallExchangeReturngoodsRefuseRequest : BaseTopRequest<TmallExchangeReturngoodsRefuseResponse>, ITopUploadRequest<TmallExchangeReturngoodsRefuseResponse>
    {
        /// <summary>
        /// 换货单号ID
        /// </summary>
        public Nullable<long> DisputeId { get; set; }

        /// <summary>
        /// 留言说明
        /// </summary>
        public string LeaveMessage { get; set; }

        /// <summary>
        /// 凭证图片
        /// </summary>
        public FileItem LeaveMessagePics { get; set; }

        /// <summary>
        /// 拒绝原因ID
        /// </summary>
        public Nullable<long> SellerRefuseReasonId { get; set; }

        #region BaseTopRequest Members

        public override string GetApiName()
        {
            return "tmall.exchange.returngoods.refuse";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("dispute_id", this.DisputeId);
            parameters.Add("leave_message", this.LeaveMessage);
            parameters.Add("seller_refuse_reason_id", this.SellerRefuseReasonId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("dispute_id", this.DisputeId);
            RequestValidator.ValidateRequired("seller_refuse_reason_id", this.SellerRefuseReasonId);
        }

        #endregion

        #region ITopUploadRequest Members

        public IDictionary<string, FileItem> GetFileParameters()
        {
            IDictionary<string, FileItem> parameters = new Dictionary<string, FileItem>();
            parameters.Add("leave_message_pics", this.LeaveMessagePics);
            return parameters;
        }

        #endregion
    }
}
