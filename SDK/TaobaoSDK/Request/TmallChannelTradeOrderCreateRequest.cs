using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.channel.trade.order.create
    /// </summary>
    public class TmallChannelTradeOrderCreateRequest : BaseTopRequest<Top.Api.Response.TmallChannelTradeOrderCreateResponse>
    {
        /// <summary>
        /// 入参
        /// </summary>
        public string Param0 { get; set; }

        public TopChannelPurchaseOrderCreateParamDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.channel.trade.order.create";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// TopChannelSubPurchaseOrderCreateParamDomain Data Structure.
/// </summary>
[Serializable]

public class TopChannelSubPurchaseOrderCreateParamDomain : TopObject
{
	        /// <summary>
	        /// 采购数量
	        /// </summary>
	        [XmlElement("buy_quantity")]
	        public Nullable<long> BuyQuantity { get; set; }
	
	        /// <summary>
	        /// 采购货品ID
	        /// </summary>
	        [XmlElement("product_id")]
	        public Nullable<long> ProductId { get; set; }
	
	        /// <summary>
	        /// 采购货品SKU ID
	        /// </summary>
	        [XmlElement("product_sku_id")]
	        public Nullable<long> ProductSkuId { get; set; }
}

	/// <summary>
/// TopChannelPurchaseOrderCreateParamDomain Data Structure.
/// </summary>
[Serializable]

public class TopChannelPurchaseOrderCreateParamDomain : TopObject
{
	        /// <summary>
	        /// 是否自动审批
	        /// </summary>
	        [XmlElement("auto_audit")]
	        public Nullable<bool> AutoAudit { get; set; }
	
	        /// <summary>
	        /// 渠道编码，11-线下网批
	        /// </summary>
	        [XmlElement("channel")]
	        public Nullable<long> Channel { get; set; }
	
	        /// <summary>
	        /// 分销商淘宝数字ID，如为空，down_user_nick必须输入
	        /// </summary>
	        [XmlElement("down_account_id")]
	        public Nullable<long> DownAccountId { get; set; }
	
	        /// <summary>
	        /// 分销商用户类型，默认淘宝用户
	        /// </summary>
	        [XmlElement("down_account_type")]
	        public Nullable<long> DownAccountType { get; set; }
	
	        /// <summary>
	        /// 分销商渠道角色，默认分销终端商
	        /// </summary>
	        [XmlElement("down_role_type")]
	        public Nullable<long> DownRoleType { get; set; }
	
	        /// <summary>
	        /// 分销商昵称
	        /// </summary>
	        [XmlElement("down_user_nick")]
	        public string DownUserNick { get; set; }
	
	        /// <summary>
	        /// 内部编码
	        /// </summary>
	        [XmlElement("internal_code")]
	        public string InternalCode { get; set; }
	
	        /// <summary>
	        /// 采购明细
	        /// </summary>
	        [XmlArray("items")]
	        [XmlArrayItem("top_channel_sub_purchase_order_create_param")]
	        public List<TopChannelSubPurchaseOrderCreateParamDomain> Items { get; set; }
	
	        /// <summary>
	        /// 请求编码
	        /// </summary>
	        [XmlElement("request_no")]
	        public string RequestNo { get; set; }
}

        #endregion
    }
}
