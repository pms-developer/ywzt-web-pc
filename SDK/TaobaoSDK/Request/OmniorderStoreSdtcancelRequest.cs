using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.sdtcancel
    /// </summary>
    public class OmniorderStoreSdtcancelRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreSdtcancelResponse>
    {
        /// <summary>
        /// 取号返回的packageId
        /// </summary>
        public Nullable<long> PackageId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.sdtcancel";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("package_id", this.PackageId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("package_id", this.PackageId);
        }

        #endregion
    }
}
