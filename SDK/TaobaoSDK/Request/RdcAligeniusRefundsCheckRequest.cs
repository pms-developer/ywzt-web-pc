using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.refunds.check
    /// </summary>
    public class RdcAligeniusRefundsCheckRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusRefundsCheckResponse>
    {
        /// <summary>
        /// 入参
        /// </summary>
        public string Param { get; set; }

        public RefundCheckDtoDomain Param_ { set { this.Param = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.refunds.check";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param", this.Param);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param", this.Param);
        }

	/// <summary>
/// RefundCheckDtoDomain Data Structure.
/// </summary>
[Serializable]

public class RefundCheckDtoDomain : TopObject
{
	        /// <summary>
	        /// 审核描述
	        /// </summary>
	        [XmlElement("msg")]
	        public string Msg { get; set; }
	
	        /// <summary>
	        /// 子订单ID
	        /// </summary>
	        [XmlElement("oid")]
	        public Nullable<long> Oid { get; set; }
	
	        /// <summary>
	        /// 审核操作时间
	        /// </summary>
	        [XmlElement("operate_time")]
	        public Nullable<DateTime> OperateTime { get; set; }
	
	        /// <summary>
	        /// 退款金额
	        /// </summary>
	        [XmlElement("refund_fee")]
	        public Nullable<long> RefundFee { get; set; }
	
	        /// <summary>
	        /// 退款单ID
	        /// </summary>
	        [XmlElement("refund_id")]
	        public Nullable<long> RefundId { get; set; }
	
	        /// <summary>
	        /// 审核状态 恒为 SUCCESS
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 主订单ID
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
}

        #endregion
    }
}
