using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.dtd.consign
    /// </summary>
    public class OmniorderDtdConsignRequest : BaseTopRequest<Top.Api.Response.OmniorderDtdConsignResponse>
    {
        /// <summary>
        /// 淘宝订单主订单号
        /// </summary>
        public Nullable<long> MainOrderId { get; set; }

        /// <summary>
        /// 发货对应的商户中心门店ID
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.dtd.consign";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("main_order_id", this.MainOrderId);
            parameters.Add("store_id", this.StoreId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("main_order_id", this.MainOrderId);
        }

        #endregion
    }
}
