using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.fenxiao.product.import.from.auction
    /// </summary>
    public class FenxiaoProductImportFromAuctionRequest : BaseTopRequest<Top.Api.Response.FenxiaoProductImportFromAuctionResponse>
    {
        /// <summary>
        /// 店铺宝贝id
        /// </summary>
        public Nullable<long> AuctionId { get; set; }

        /// <summary>
        /// 产品线id
        /// </summary>
        public Nullable<long> ProductLineId { get; set; }

        /// <summary>
        /// 导入产品需要支持的交易类型：[1 代销][ 2 经销 ][3 代销和经销]
        /// </summary>
        public Nullable<long> TradeType { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.fenxiao.product.import.from.auction";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("auction_id", this.AuctionId);
            parameters.Add("product_line_id", this.ProductLineId);
            parameters.Add("trade_type", this.TradeType);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("auction_id", this.AuctionId);
            RequestValidator.ValidateRequired("product_line_id", this.ProductLineId);
            RequestValidator.ValidateRequired("trade_type", this.TradeType);
        }

        #endregion
    }
}
