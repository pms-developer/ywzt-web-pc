using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.exchange.messages.get
    /// </summary>
    public class TmallExchangeMessagesGetRequest : BaseTopRequest<Top.Api.Response.TmallExchangeMessagesGetResponse>
    {
        /// <summary>
        /// 换货单号ID
        /// </summary>
        public Nullable<long> DisputeId { get; set; }

        /// <summary>
        /// 返回的字段。具体包括：id,refund_id,owner_id,owner_nick,owner_role,content,pic_urls,created,message_type
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 留言创建角色。具体包括：卖家主账户(1)、卖家子账户(2)、小二(3)、买家(4)、系统(5)、系统超时(6)
        /// </summary>
        public string OperatorRoles { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 每页条数
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.exchange.messages.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("dispute_id", this.DisputeId);
            parameters.Add("fields", this.Fields);
            parameters.Add("operator_roles", this.OperatorRoles);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("dispute_id", this.DisputeId);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxListSize("fields", this.Fields, 20);
            RequestValidator.ValidateMaxListSize("operator_roles", this.OperatorRoles, 20);
        }

        #endregion
    }
}
