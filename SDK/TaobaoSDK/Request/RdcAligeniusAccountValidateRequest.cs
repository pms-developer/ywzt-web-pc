using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.account.validate
    /// </summary>
    public class RdcAligeniusAccountValidateRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusAccountValidateResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.account.validate";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
