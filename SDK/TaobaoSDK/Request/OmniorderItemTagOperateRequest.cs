using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.item.tag.operate
    /// </summary>
    public class OmniorderItemTagOperateRequest : BaseTopRequest<Top.Api.Response.OmniorderItemTagOperateResponse>
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public Nullable<long> ItemId { get; set; }

        /// <summary>
        /// 分单&接单设置
        /// </summary>
        public string OmniSetting { get; set; }

        public OmniSettingDtoDomain OmniSetting_ { set { this.OmniSetting = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 操作状态， 填 1 代表打标，填 -1 代表去标
        /// </summary>
        public Nullable<long> Status { get; set; }

        /// <summary>
        /// 商品标,storeDeliver代表门店发货, AllocateByFront代表前置拆单, storeCollect代表门店自提
        /// </summary>
        public string Types { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.item.tag.operate";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("item_id", this.ItemId);
            parameters.Add("omni_setting", this.OmniSetting);
            parameters.Add("status", this.Status);
            parameters.Add("types", this.Types);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("item_id", this.ItemId);
            RequestValidator.ValidateRequired("status", this.Status);
            RequestValidator.ValidateRequired("types", this.Types);
            RequestValidator.ValidateMaxListSize("types", this.Types, 20);
        }

	/// <summary>
/// OmniSettingDtoDomain Data Structure.
/// </summary>
[Serializable]

public class OmniSettingDtoDomain : TopObject
{
	        /// <summary>
	        /// 接单系统，填 0 代表店掌柜，填 1 代表 POS
	        /// </summary>
	        [XmlElement("accepted_system")]
	        public Nullable<long> AcceptedSystem { get; set; }
	
	        /// <summary>
	        /// 分单系统，填 astrolabe 代表阿里分单，填 RDS的 appkey 代表自行分单
	        /// </summary>
	        [XmlElement("allocated_system")]
	        public string AllocatedSystem { get; set; }
}

        #endregion
    }
}
