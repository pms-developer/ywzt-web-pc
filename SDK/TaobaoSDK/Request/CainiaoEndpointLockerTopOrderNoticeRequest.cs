using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.endpoint.locker.top.order.notice
    /// </summary>
    public class CainiaoEndpointLockerTopOrderNoticeRequest : BaseTopRequest<Top.Api.Response.CainiaoEndpointLockerTopOrderNoticeResponse>
    {
        /// <summary>
        /// 运单号
        /// </summary>
        public string MailNo { get; set; }

        /// <summary>
        /// 合作公司唯一订单编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 场景编号：0：重发短信，1：催取短信
        /// </summary>
        public Nullable<long> SceneCode { get; set; }

        /// <summary>
        /// 站点ID
        /// </summary>
        public string StationId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.endpoint.locker.top.order.notice";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("mail_no", this.MailNo);
            parameters.Add("order_code", this.OrderCode);
            parameters.Add("scene_code", this.SceneCode);
            parameters.Add("station_id", this.StationId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("mail_no", this.MailNo);
            RequestValidator.ValidateRequired("order_code", this.OrderCode);
            RequestValidator.ValidateRequired("scene_code", this.SceneCode);
            RequestValidator.ValidateRequired("station_id", this.StationId);
        }

        #endregion
    }
}
