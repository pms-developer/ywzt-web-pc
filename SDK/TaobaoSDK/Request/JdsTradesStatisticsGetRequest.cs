using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.jds.trades.statistics.get
    /// </summary>
    public class JdsTradesStatisticsGetRequest : BaseTopRequest<Top.Api.Response.JdsTradesStatisticsGetResponse>
    {
        /// <summary>
        /// 查询的日期，格式如YYYYMMDD的日期对应的数字
        /// </summary>
        public Nullable<long> Date { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.jds.trades.statistics.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("date", this.Date);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("date", this.Date);
        }

        #endregion
    }
}
