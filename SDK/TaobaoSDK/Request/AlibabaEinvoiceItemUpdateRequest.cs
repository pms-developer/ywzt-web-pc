using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alibaba.einvoice.item.update
    /// </summary>
    public class AlibabaEinvoiceItemUpdateRequest : BaseTopRequest<Top.Api.Response.AlibabaEinvoiceItemUpdateResponse>
    {
        /// <summary>
        /// 商品的开票名称，对应发票的货物劳务名称，值DELETE时表示删除
        /// </summary>
        public string InvoiceName { get; set; }

        /// <summary>
        /// 商品id，优先级高于outerId，商品必须归属于店铺，itemId和outerId不能同时为空
        /// </summary>
        public Nullable<long> ItemId { get; set; }

        /// <summary>
        /// 税收分类编码，需要精确到叶子节点，必须和taxRate同时修改或删除，值DELETE时表示删除
        /// </summary>
        public string ItemNo { get; set; }

        /// <summary>
        /// 商家外部商品id，如果outerId对应了多个天猫sku，则会更新所有的sku开票信息。itemId和outerId不能同时为空
        /// </summary>
        public string OuterId { get; set; }

        /// <summary>
        /// skuId，必须是itemId下的sku，填写skuId后，修改和删除sku的开票信息
        /// </summary>
        public Nullable<long> SkuId { get; set; }

        /// <summary>
        /// 规格型号，值DELETE时表示删除
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 税率，可选值0，3，4，5，6，10，11，13， 16，17，必须和itemNo同时修改或删除,值为DELETE时表示删除
        /// </summary>
        public string TaxRate { get; set; }

        /// <summary>
        /// 单位，值DELETE时表示删除
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 是否根据outerId更新所有对应sku的开票信息，true=更新，false=开票信息维护在发票平台；自动开票时，根据skuId获取outerId，再根据outerId查询开票信息。outerId不为空时必填
        /// </summary>
        public Nullable<bool> UpdateSku { get; set; }

        /// <summary>
        /// 0税率标识，只有税率为0的情况才有值，0=出口零税率，1=免税，2=不征收，3=普通零税率，值为DELETE时表示删除
        /// </summary>
        public string ZeroRateFlag { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alibaba.einvoice.item.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("invoice_name", this.InvoiceName);
            parameters.Add("item_id", this.ItemId);
            parameters.Add("item_no", this.ItemNo);
            parameters.Add("outer_id", this.OuterId);
            parameters.Add("sku_id", this.SkuId);
            parameters.Add("specification", this.Specification);
            parameters.Add("tax_rate", this.TaxRate);
            parameters.Add("unit", this.Unit);
            parameters.Add("update_sku", this.UpdateSku);
            parameters.Add("zero_rate_flag", this.ZeroRateFlag);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxLength("invoice_name", this.InvoiceName, 65);
            RequestValidator.ValidateMaxLength("item_no", this.ItemNo, 19);
            RequestValidator.ValidateMaxLength("specification", this.Specification, 20);
            RequestValidator.ValidateMaxLength("tax_rate", this.TaxRate, 6);
            RequestValidator.ValidateMaxLength("unit", this.Unit, 10);
            RequestValidator.ValidateMaxLength("zero_rate_flag", this.ZeroRateFlag, 6);
        }

        #endregion
    }
}
