using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.nbadd.appointdeliver.getconsigninfo
    /// </summary>
    public class CainiaoNbaddAppointdeliverGetconsigninfoRequest : BaseTopRequest<Top.Api.Response.CainiaoNbaddAppointdeliverGetconsigninfoResponse>
    {
        /// <summary>
        /// 淘宝交易订单id
        /// </summary>
        public Nullable<long> TradeOrderId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.nbadd.appointdeliver.getconsigninfo";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("trade_order_id", this.TradeOrderId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("trade_order_id", this.TradeOrderId);
        }

        #endregion
    }
}
