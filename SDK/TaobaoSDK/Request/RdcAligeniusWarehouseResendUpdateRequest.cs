using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.warehouse.resend.update
    /// </summary>
    public class RdcAligeniusWarehouseResendUpdateRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusWarehouseResendUpdateResponse>
    {
        /// <summary>
        /// 参数
        /// </summary>
        public string Param0 { get; set; }

        public UpdateResendStatusDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.warehouse.resend.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// UpdateResendStatusDtoDomain Data Structure.
/// </summary>
[Serializable]

public class UpdateResendStatusDtoDomain : TopObject
{
	        /// <summary>
	        /// 描述
	        /// </summary>
	        [XmlElement("msg")]
	        public string Msg { get; set; }
	
	        /// <summary>
	        /// erp补发单
	        /// </summary>
	        [XmlElement("reissue_id")]
	        public string ReissueId { get; set; }
	
	        /// <summary>
	        /// 平台补发单唯一标识
	        /// </summary>
	        [XmlElement("source_id")]
	        public string SourceId { get; set; }
	
	        /// <summary>
	        /// 补发单状态（-1=关闭，1=补发成功，2=部分成功）
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
	
	        /// <summary>
	        /// 主订单
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
}

        #endregion
    }
}
