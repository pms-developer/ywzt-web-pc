using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.uop.tob.order.create
    /// </summary>
    public class UopTobOrderCreateRequest : BaseTopRequest<Top.Api.Response.UopTobOrderCreateResponse>
    {
        /// <summary>
        /// ERP出库对象
        /// </summary>
        public string DeliveryOrder { get; set; }

        public DeliveryOrderDomain DeliveryOrder_ { set { this.DeliveryOrder = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.uop.tob.order.create";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("delivery_order", this.DeliveryOrder);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// OrderLineDomain Data Structure.
/// </summary>
[Serializable]

public class OrderLineDomain : TopObject
{
	        /// <summary>
	        /// 批次编码
	        /// </summary>
	        [XmlElement("batch_code")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// 过期日期，生产日期(YYYY-MM-DD)
	        /// </summary>
	        [XmlElement("expire_date")]
	        public Nullable<DateTime> ExpireDate { get; set; }
	
	        /// <summary>
	        /// 库存类型，ZP=正品、CC=残次
	        /// </summary>
	        [XmlElement("inventory_type")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 商品编码
	        /// </summary>
	        [XmlElement("item_code")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("item_name")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 商品数量
	        /// </summary>
	        [XmlElement("item_quantity")]
	        public Nullable<long> ItemQuantity { get; set; }
	
	        /// <summary>
	        /// 订单行号
	        /// </summary>
	        [XmlElement("order_line_no")]
	        public string OrderLineNo { get; set; }
	
	        /// <summary>
	        /// 生产批号
	        /// </summary>
	        [XmlElement("produce_code")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// 生产日期，生产日期(YYYY-MM-DD)
	        /// </summary>
	        [XmlElement("product_date")]
	        public Nullable<DateTime> ProductDate { get; set; }
	
	        /// <summary>
	        /// 原交易单，供销平台交易单号、分销平台单号
	        /// </summary>
	        [XmlElement("source_order_code")]
	        public string SourceOrderCode { get; set; }
	
	        /// <summary>
	        /// 子交易单号
	        /// </summary>
	        [XmlElement("sub_source_order_code")]
	        public string SubSourceOrderCode { get; set; }
}

	/// <summary>
/// ReceiverInfoDomain Data Structure.
/// </summary>
[Serializable]

public class ReceiverInfoDomain : TopObject
{
	        /// <summary>
	        /// 收货人区
	        /// </summary>
	        [XmlElement("area")]
	        public string Area { get; set; }
	
	        /// <summary>
	        /// 收货人市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 收货人详细地址
	        /// </summary>
	        [XmlElement("detail_address")]
	        public string DetailAddress { get; set; }
	
	        /// <summary>
	        /// 收货人移动电话
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 收货人姓名
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 收货人省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 收货人镇
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// DeliveryOrderDomain Data Structure.
/// </summary>
[Serializable]

public class DeliveryOrderDomain : TopObject
{
	        /// <summary>
	        /// 到货渠道类型，VIP＝1、门店＝2、经销商＝3
	        /// </summary>
	        [XmlElement("arrive_channel_type")]
	        public string ArriveChannelType { get; set; }
	
	        /// <summary>
	        /// 发货单创建时间
	        /// </summary>
	        [XmlElement("create_time")]
	        public Nullable<DateTime> CreateTime { get; set; }
	
	        /// <summary>
	        /// ERP出库单号,ERP系统内本次出库的唯一标示
	        /// </summary>
	        [XmlElement("delivery_order_code")]
	        public string DeliveryOrderCode { get; set; }
	
	        /// <summary>
	        /// 扩展信息
	        /// </summary>
	        [XmlElement("extend_props")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 是否自提
	        /// </summary>
	        [XmlElement("is_self_lifting")]
	        public string IsSelfLifting { get; set; }
	
	        /// <summary>
	        /// 最晚到货时间
	        /// </summary>
	        [XmlElement("last_arrive_date")]
	        public Nullable<DateTime> LastArriveDate { get; set; }
	
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("logistics_code")]
	        public string LogisticsCode { get; set; }
	
	        /// <summary>
	        /// 物流公司名称
	        /// </summary>
	        [XmlElement("logistics_name")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// 订单信息
	        /// </summary>
	        [XmlArray("order_line")]
	        [XmlArrayItem("order_line")]
	        public List<OrderLineDomain> OrderLine { get; set; }
	
	        /// <summary>
	        /// 单据类型,出库单类型(JYCK=一般交易出库单;HHCK=换货出库单;BFCK=补发出库单;QTCK=其他出库单;TOBCK=TOB出库;BIGTOBCK=大B2B发货)
	        /// </summary>
	        [XmlElement("order_type")]
	        public string OrderType { get; set; }
	
	        /// <summary>
	        /// 收货人信息
	        /// </summary>
	        [XmlElement("receiver_info")]
	        public ReceiverInfoDomain ReceiverInfo { get; set; }
	
	        /// <summary>
	        /// 交接入库单号,例如唯品会入库单号或者门店收货单号、商家仓入库单号等
	        /// </summary>
	        [XmlElement("rel_in_bound_order_code")]
	        public string RelInBoundOrderCode { get; set; }
	
	        /// <summary>
	        /// 收货时间区间
	        /// </summary>
	        [XmlElement("sign_time")]
	        public string SignTime { get; set; }
	
	        /// <summary>
	        /// 发货仓库
	        /// </summary>
	        [XmlElement("warehouse_code")]
	        public string WarehouseCode { get; set; }
}

        #endregion
    }
}
