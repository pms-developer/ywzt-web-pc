using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.sdtconsign
    /// </summary>
    public class OmniorderStoreSdtconsignRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreSdtconsignResponse>
    {
        /// <summary>
        /// 取号接口返回的包裹id
        /// </summary>
        public string PackageId { get; set; }

        /// <summary>
        /// 发货标签号
        /// </summary>
        public string TagCode { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.sdtconsign";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("package_id", this.PackageId);
            parameters.Add("tag_code", this.TagCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("package_id", this.PackageId);
        }

        #endregion
    }
}
