using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alibaba.einvoice.paper.invalid
    /// </summary>
    public class AlibabaEinvoicePaperInvalidRequest : BaseTopRequest<Top.Api.Response.AlibabaEinvoicePaperInvalidResponse>
    {
        /// <summary>
        /// 作废操作人
        /// </summary>
        public string InvalidOperator { get; set; }

        /// <summary>
        /// 作废类型, 0=空白发票(有残缺 的纸张发票，不能做为有效报销)作废, 1=已开发票作废
        /// </summary>
        public Nullable<long> InvalidType { get; set; }

        /// <summary>
        /// 发票代码，空白作废时必填
        /// </summary>
        public string InvoiceCode { get; set; }

        /// <summary>
        /// 发票号码，空白作废时必填
        /// </summary>
        public string InvoiceNo { get; set; }

        /// <summary>
        /// 销售方纳税人识别号
        /// </summary>
        public string PayeeRegisterNo { get; set; }

        /// <summary>
        /// 开票流水号
        /// </summary>
        public string SerialNo { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alibaba.einvoice.paper.invalid";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("invalid_operator", this.InvalidOperator);
            parameters.Add("invalid_type", this.InvalidType);
            parameters.Add("invoice_code", this.InvoiceCode);
            parameters.Add("invoice_no", this.InvoiceNo);
            parameters.Add("payee_register_no", this.PayeeRegisterNo);
            parameters.Add("serial_no", this.SerialNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("invalid_operator", this.InvalidOperator);
            RequestValidator.ValidateRequired("invalid_type", this.InvalidType);
            RequestValidator.ValidateRequired("payee_register_no", this.PayeeRegisterNo);
            RequestValidator.ValidateRequired("serial_no", this.SerialNo);
        }

        #endregion
    }
}
