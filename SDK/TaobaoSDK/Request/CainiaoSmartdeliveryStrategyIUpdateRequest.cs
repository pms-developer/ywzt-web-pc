using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.strategy.i.update
    /// </summary>
    public class CainiaoSmartdeliveryStrategyIUpdateRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryStrategyIUpdateResponse>
    {
        /// <summary>
        /// 发货策略信息
        /// </summary>
        public string DeliveryStrategyInfo { get; set; }

        public DeliveryStrategyInfoDomain DeliveryStrategyInfo_ { set { this.DeliveryStrategyInfo = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.strategy.i.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("delivery_strategy_info", this.DeliveryStrategyInfo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("delivery_strategy_info", this.DeliveryStrategyInfo);
        }

	/// <summary>
/// AddressDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细信息
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// CpInfoDomain Data Structure.
/// </summary>
[Serializable]

public class CpInfoDomain : TopObject
{
	        /// <summary>
	        /// 云打印模板id
	        /// </summary>
	        [XmlElement("cloud_template_id")]
	        public string CloudTemplateId { get; set; }
	
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 状态: 0-禁用, 1-启用
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
}

	/// <summary>
/// AddressAreaDomain Data Structure.
/// </summary>
[Serializable]

public class AddressAreaDomain : TopObject
{
	        /// <summary>
	        /// 特殊路线到货市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 特殊路线到货省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
}

	/// <summary>
/// SpecialRouteInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SpecialRouteInfoDomain : TopObject
{
	        /// <summary>
	        /// 特殊路线设置的物流公司，目前支持5家物流公司的特殊路线设置顺丰(SF)、EMS经济快递(EYB)、EMS标准快递(EMS)、邮政小包(POSTB)、邮政标准快递(5000000007756),特殊路线的设置必须要求特殊路线的cp在启用的物流公司中
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 特殊路线到货区域
	        /// </summary>
	        [XmlElement("receive_area")]
	        public AddressAreaDomain ReceiveArea { get; set; }
}

	/// <summary>
/// DeliveryStrategyInfoDomain Data Structure.
/// </summary>
[Serializable]

public class DeliveryStrategyInfoDomain : TopObject
{
	        /// <summary>
	        /// 识别买家备注: 0-忽略, 1-识别, 2-仅识别合作cp
	        /// </summary>
	        [XmlElement("buyer_message_rule")]
	        public Nullable<long> BuyerMessageRule { get; set; }
	
	        /// <summary>
	        /// 合作物流公司信息
	        /// </summary>
	        [XmlArray("cocp_info_list")]
	        [XmlArrayItem("cp_info")]
	        public List<CpInfoDomain> CocpInfoList { get; set; }
	
	        /// <summary>
	        /// 识别卖家备注: 0-忽略, 1-识别, 2-仅识别合作cp
	        /// </summary>
	        [XmlElement("seller_memo_rule")]
	        public Nullable<long> SellerMemoRule { get; set; }
	
	        /// <summary>
	        /// 发货地址
	        /// </summary>
	        [XmlElement("send_address")]
	        public AddressDomain SendAddress { get; set; }
	
	        /// <summary>
	        /// 特殊路线设置信息，如果特殊路线设置传空，表名需要删除之前设置的特殊路线。最多设置10条特殊路线
	        /// </summary>
	        [XmlArray("special_route_info_list")]
	        [XmlArrayItem("special_route_info")]
	        public List<SpecialRouteInfoDomain> SpecialRouteInfoList { get; set; }
}

        #endregion
    }
}
