using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.channel.products.query
    /// </summary>
    public class TmallChannelProductsQueryRequest : BaseTopRequest<Top.Api.Response.TmallChannelProductsQueryResponse>
    {
        /// <summary>
        /// 页码数，从1开始
        /// </summary>
        public Nullable<long> PageNum { get; set; }

        /// <summary>
        /// 分页大小
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 产品Id
        /// </summary>
        public string ProductIds { get; set; }

        /// <summary>
        /// 产品线Id
        /// </summary>
        public Nullable<long> ProductLineId { get; set; }

        /// <summary>
        /// 商家产品编码
        /// </summary>
        public string ProductNumber { get; set; }

        /// <summary>
        /// 商家SKU编码
        /// </summary>
        public string SkuNumber { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.channel.products.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("page_num", this.PageNum);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("product_ids", this.ProductIds);
            parameters.Add("product_line_id", this.ProductLineId);
            parameters.Add("product_number", this.ProductNumber);
            parameters.Add("sku_number", this.SkuNumber);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("page_num", this.PageNum);
            RequestValidator.ValidateRequired("page_size", this.PageSize);
            RequestValidator.ValidateMaxListSize("product_ids", this.ProductIds, 20);
        }

        #endregion
    }
}
