using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.order.event.update
    /// </summary>
    public class RdcAligeniusOrderEventUpdateRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusOrderEventUpdateResponse>
    {
        /// <summary>
        /// 参数
        /// </summary>
        public string Param0 { get; set; }

        public OrderEventDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.order.event.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// OrderEventDtoDomain Data Structure.
/// </summary>
[Serializable]

public class OrderEventDtoDomain : TopObject
{
	        /// <summary>
	        /// 扩展信息
	        /// </summary>
	        [XmlElement("extra")]
	        public string Extra { get; set; }
	
	        /// <summary>
	        /// 子订单ID
	        /// </summary>
	        [XmlElement("oid")]
	        public Nullable<long> Oid { get; set; }
	
	        /// <summary>
	        /// 描述
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 主订单编号
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
	
	        /// <summary>
	        /// 类型（1=创建销退单结果，2=作废销退单结果，3=入仓结果）
	        /// </summary>
	        [XmlElement("type")]
	        public Nullable<long> Type { get; set; }
	
	        /// <summary>
	        /// 变更值（当type=1时value可选值：1=成功，2=失败； 当type=2时value可选值：1=成功，2=失败； 当type=3时value可选值：1=成功）
	        /// </summary>
	        [XmlElement("value")]
	        public string Value { get; set; }
}

        #endregion
    }
}
