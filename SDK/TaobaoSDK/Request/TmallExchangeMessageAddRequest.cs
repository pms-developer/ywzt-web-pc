using System;
using System.Collections.Generic;
using Top.Api.Response;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.exchange.message.add
    /// </summary>
    public class TmallExchangeMessageAddRequest : BaseTopRequest<TmallExchangeMessageAddResponse>, ITopUploadRequest<TmallExchangeMessageAddResponse>
    {
        /// <summary>
        /// 留言内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 换货单号ID
        /// </summary>
        public Nullable<long> DisputeId { get; set; }

        /// <summary>
        /// 返回字段。目前支持id,refund_id,owner_id,owner_nick,owner_role,content,pic_urls,created,message_type
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 凭证图片列表
        /// </summary>
        public FileItem MessagePics { get; set; }

        #region BaseTopRequest Members

        public override string GetApiName()
        {
            return "tmall.exchange.message.add";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("content", this.Content);
            parameters.Add("dispute_id", this.DisputeId);
            parameters.Add("fields", this.Fields);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("content", this.Content);
            RequestValidator.ValidateRequired("dispute_id", this.DisputeId);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxListSize("fields", this.Fields, 20);
        }

        #endregion

        #region ITopUploadRequest Members

        public IDictionary<string, FileItem> GetFileParameters()
        {
            IDictionary<string, FileItem> parameters = new Dictionary<string, FileItem>();
            parameters.Add("message_pics", this.MessagePics);
            return parameters;
        }

        #endregion
    }
}
