using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.storedeliver.allocation.accept
    /// </summary>
    public class TmallStoredeliverAllocationAcceptRequest : BaseTopRequest<Top.Api.Response.TmallStoredeliverAllocationAcceptResponse>
    {
        /// <summary>
        /// 派单号
        /// </summary>
        public string AllocationCode { get; set; }

        /// <summary>
        /// 是否接单
        /// </summary>
        public Nullable<bool> IsAccept { get; set; }

        /// <summary>
        /// 子订单号必须和派单号匹配
        /// </summary>
        public string SubOrderCode { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.storedeliver.allocation.accept";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("allocation_code", this.AllocationCode);
            parameters.Add("is_accept", this.IsAccept);
            parameters.Add("sub_order_code", this.SubOrderCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("allocation_code", this.AllocationCode);
            RequestValidator.ValidateRequired("is_accept", this.IsAccept);
            RequestValidator.ValidateRequired("sub_order_code", this.SubOrderCode);
            RequestValidator.ValidateMaxListSize("sub_order_code", this.SubOrderCode, 20);
        }

        #endregion
    }
}
