using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.dddelivery.status.all.get
    /// </summary>
    public class OmniorderDddeliveryStatusAllGetRequest : BaseTopRequest<Top.Api.Response.OmniorderDddeliveryStatusAllGetResponse>
    {
        /// <summary>
        /// 点点送运单id
        /// </summary>
        public Nullable<long> DeliveryOrderNo { get; set; }

        /// <summary>
        /// 交易id（交易id与点点送运单id两个二选一，如果都写，需要运单同时满足两者）
        /// </summary>
        public Nullable<long> TradeId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.dddelivery.status.all.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("delivery_order_no", this.DeliveryOrderNo);
            parameters.Add("trade_id", this.TradeId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
