using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.fulfill.cancel
    /// </summary>
    public class TmallNrFulfillCancelRequest : BaseTopRequest<Top.Api.Response.TmallNrFulfillCancelResponse>
    {
        /// <summary>
        /// 入参
        /// </summary>
        public string Req { get; set; }

        public NrCancelFulfillReqDtoDomain Req_ { set { this.Req = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.fulfill.cancel";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("req", this.Req);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// NrCancelFulfillReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrCancelFulfillReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 取消类型。操作人类型:1寄件人,3客服小二,4快递员, 5CP, 6收件人,8门店取消,100系统
	        /// </summary>
	        [XmlElement("cancel_oper_type")]
	        public Nullable<long> CancelOperType { get; set; }
	
	        /// <summary>
	        /// 操作取消人员ID号
	        /// </summary>
	        [XmlElement("cancel_oper_user_id")]
	        public Nullable<long> CancelOperUserId { get; set; }
	
	        /// <summary>
	        /// 操作取消人员姓名
	        /// </summary>
	        [XmlElement("cancel_oper_user_name")]
	        public string CancelOperUserName { get; set; }
	
	        /// <summary>
	        /// 取消原因的说明
	        /// </summary>
	        [XmlElement("cancel_reason")]
	        public string CancelReason { get; set; }
	
	        /// <summary>
	        /// 取消的对应编码
	        /// </summary>
	        [XmlElement("cancel_reason_code")]
	        public Nullable<long> CancelReasonCode { get; set; }
	
	        /// <summary>
	        /// 淘宝交易的主订单号
	        /// </summary>
	        [XmlElement("main_order_id")]
	        public Nullable<long> MainOrderId { get; set; }
	
	        /// <summary>
	        /// 淘宝商家的sellerID
	        /// </summary>
	        [XmlElement("seller_id")]
	        public Nullable<long> SellerId { get; set; }
}

        #endregion
    }
}
