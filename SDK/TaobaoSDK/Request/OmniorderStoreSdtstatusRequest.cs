using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.sdtstatus
    /// </summary>
    public class OmniorderStoreSdtstatusRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreSdtstatusResponse>
    {
        /// <summary>
        /// 菜鸟裹裹的包裹ID
        /// </summary>
        public Nullable<long> PackageId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.sdtstatus";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("package_id", this.PackageId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
