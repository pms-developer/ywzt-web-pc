using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.inventory.update
    /// </summary>
    public class TmallNrInventoryUpdateRequest : BaseTopRequest<Top.Api.Response.TmallNrInventoryUpdateResponse>
    {
        /// <summary>
        /// 入参
        /// </summary>
        public string Param0 { get; set; }

        public NrInventoryUpdateReqDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.inventory.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// NrInventoryCheckDetailDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrInventoryCheckDetailDtoDomain : TopObject
{
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public Nullable<long> Quantity { get; set; }
	
	        /// <summary>
	        /// 商家的商品编码
	        /// </summary>
	        [XmlElement("sc_item_code")]
	        public string ScItemCode { get; set; }
	
	        /// <summary>
	        /// 淘宝后端商品的id号
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public Nullable<long> ScItemId { get; set; }
	
	        /// <summary>
	        /// 幂等值
	        /// </summary>
	        [XmlElement("sub_order_id")]
	        public string SubOrderId { get; set; }
}

	/// <summary>
/// NrInventoryUpdateReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrInventoryUpdateReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 定时送dss，jsd极速达
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 1表示全量，2表示增量
	        /// </summary>
	        [XmlElement("check_mode")]
	        public Nullable<long> CheckMode { get; set; }
	
	        /// <summary>
	        /// 更新库存的列表值
	        /// </summary>
	        [XmlArray("detail_list")]
	        [XmlArrayItem("nr_inventory_check_detail_dto")]
	        public List<NrInventoryCheckDetailDtoDomain> DetailList { get; set; }
	
	        /// <summary>
	        /// 幂等值
	        /// </summary>
	        [XmlElement("order_id")]
	        public string OrderId { get; set; }
	
	        /// <summary>
	        /// 商家的sellerID，如果是零售商需要给品牌的sellerId
	        /// </summary>
	        [XmlElement("owner_id")]
	        public Nullable<long> OwnerId { get; set; }
	
	        /// <summary>
	        /// 门店编号
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
}

        #endregion
    }
}
