using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.item.simpleschema.add
    /// </summary>
    public class TmallItemSimpleschemaAddRequest : BaseTopRequest<Top.Api.Response.TmallItemSimpleschemaAddResponse>
    {
        /// <summary>
        /// 根据tmall.item.add.simpleschema.get生成的商品发布规则入参数据
        /// </summary>
        public string SchemaXmlFields { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.item.simpleschema.add";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("schema_xml_fields", this.SchemaXmlFields);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("schema_xml_fields", this.SchemaXmlFields);
        }

        #endregion
    }
}
