using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.pay.deduct
    /// </summary>
    public class TmallNrPayDeductRequest : BaseTopRequest<Top.Api.Response.TmallNrPayDeductResponse>
    {
        /// <summary>
        /// 业务身份标识
        /// </summary>
        public string BizIdentity { get; set; }

        /// <summary>
        /// 买家id
        /// </summary>
        public Nullable<long> BuyerId { get; set; }

        /// <summary>
        /// 付款码、卡号等信息,例如支付宝付款码、微信付款码等。
        /// </summary>
        public string ChannelCode { get; set; }

        /// <summary>
        /// 线下支付流水号，用来唯一关联一次线下支付操作
        /// </summary>
        public string CurrentTransactionId { get; set; }

        /// <summary>
        /// 线下收款操作员标识
        /// </summary>
        public string OperatorId { get; set; }

        /// <summary>
        /// 支付渠道标识，枚举类型包括: 1. 微信支付（wechat_pay） 2. 支付宝支付（alipay） 3. 银联支付（bank_card） 4. 线下储值卡支付（deposit_card） 5. 现金支付（cash）。 6. 其他(other)
        /// </summary>
        public string PayChannel { get; set; }

        /// <summary>
        /// 线下实付款金额，单位为分，例如订单金额128.07元，抹零后为128.1元，透传的金额为12810。
        /// </summary>
        public Nullable<long> PayFee { get; set; }

        /// <summary>
        /// 线下支付操作完成时间，时间格式按照“yyyy-MM-dd HH:mm:ss”格式来输入
        /// </summary>
        public string PayTime { get; set; }

        /// <summary>
        /// 线下支付抹零金额，单位为分，例如订单金额128.07元，按照四舍五入抹零规则，则抹零金额为增加0.03元，抹零导致实付款变少的场景值为负数。或者由线下门店提前在天猫端配置抹零规则，则该参数无需上传。
        /// </summary>
        public Nullable<long> SmallChange { get; set; }

        /// <summary>
        /// 门店编码
        /// </summary>
        public string StoreCode { get; set; }

        /// <summary>
        /// 线下门店交易单号
        /// </summary>
        public string StoreOrderId { get; set; }

        /// <summary>
        /// 淘系交易订单号
        /// </summary>
        public string Tid { get; set; }

        /// <summary>
        /// 收银设备唯一ID（也可以是唯一编号）
        /// </summary>
        public string Utdid { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.pay.deduct";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("biz_identity", this.BizIdentity);
            parameters.Add("buyer_id", this.BuyerId);
            parameters.Add("channel_code", this.ChannelCode);
            parameters.Add("current_transaction_id", this.CurrentTransactionId);
            parameters.Add("operator_id", this.OperatorId);
            parameters.Add("pay_channel", this.PayChannel);
            parameters.Add("pay_fee", this.PayFee);
            parameters.Add("pay_time", this.PayTime);
            parameters.Add("small_change", this.SmallChange);
            parameters.Add("store_code", this.StoreCode);
            parameters.Add("store_order_id", this.StoreOrderId);
            parameters.Add("tid", this.Tid);
            parameters.Add("utdid", this.Utdid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("biz_identity", this.BizIdentity);
            RequestValidator.ValidateRequired("current_transaction_id", this.CurrentTransactionId);
            RequestValidator.ValidateRequired("pay_channel", this.PayChannel);
            RequestValidator.ValidateRequired("pay_fee", this.PayFee);
            RequestValidator.ValidateRequired("pay_time", this.PayTime);
            RequestValidator.ValidateRequired("store_code", this.StoreCode);
            RequestValidator.ValidateRequired("store_order_id", this.StoreOrderId);
            RequestValidator.ValidateRequired("tid", this.Tid);
            RequestValidator.ValidateRequired("utdid", this.Utdid);
        }

        #endregion
    }
}
