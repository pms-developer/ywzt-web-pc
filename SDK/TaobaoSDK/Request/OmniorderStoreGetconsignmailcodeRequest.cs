using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.getconsignmailcode
    /// </summary>
    public class OmniorderStoreGetconsignmailcodeRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreGetconsignmailcodeResponse>
    {
        /// <summary>
        /// 淘宝(TB)、天猫(TM)、京东(JD)、当当(DD)、拍拍(PP)、易讯(YX)、ebay(EBAY)、QQ网购(QQ)      、亚马逊(AMAZON)、苏宁(SN)、国美(GM)、唯品会(WPH)、聚美(JM)、乐蜂(LF)、蘑菇街(MGJ)      、聚尚(JS)、拍鞋(PX)、银泰(YT)、1号店(YHD)、凡客(VANCL)、邮乐(YL)、优购(YG)、阿里      巴巴(1688)、其他(OTHERS)
        /// </summary>
        public string Channel { get; set; }

        /// <summary>
        /// 收件人信息
        /// </summary>
        public string Receiver { get; set; }

        public ReceiverDtoDomain Receiver_ { set { this.Receiver = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 扩展信息
        /// </summary>
        public string SdtExtendInfoDTO { get; set; }

        public SdtExtendInfoDtoDomain SdtExtendInfoDTO_ { set { this.SdtExtendInfoDTO = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 发件人联系电话，如空则表示使用门店信息中的电话号码
        /// </summary>
        public string SenderContact { get; set; }

        /// <summary>
        /// 门店ID
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        /// <summary>
        /// 订单信息，目前一次请求只支持一个主订单
        /// </summary>
        public string Trades { get; set; }

        public List<TradeOrderInfoDtoDomain> Trades_ { set { this.Trades = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.getconsignmailcode";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("channel", this.Channel);
            parameters.Add("receiver", this.Receiver);
            parameters.Add("sdt_extend_info_d_t_o", this.SdtExtendInfoDTO);
            parameters.Add("sender_contact", this.SenderContact);
            parameters.Add("store_id", this.StoreId);
            parameters.Add("trades", this.Trades);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("channel", this.Channel);
            RequestValidator.ValidateRequired("store_id", this.StoreId);
            RequestValidator.ValidateRequired("trades", this.Trades);
            RequestValidator.ValidateObjectMaxListSize("trades", this.Trades, 20);
        }

	/// <summary>
/// TradeItemInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TradeItemInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 商品单价，单位为分
	        /// </summary>
	        [XmlElement("amount")]
	        public Nullable<long> Amount { get; set; }
	
	        /// <summary>
	        /// 商品数量
	        /// </summary>
	        [XmlElement("count")]
	        public Nullable<long> Count { get; set; }
	
	        /// <summary>
	        /// 商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 体积，单位为升
	        /// </summary>
	        [XmlElement("volumn")]
	        public Nullable<long> Volumn { get; set; }
	
	        /// <summary>
	        /// 重量，单位为克
	        /// </summary>
	        [XmlElement("weight")]
	        public Nullable<long> Weight { get; set; }
}

	/// <summary>
/// TradeOrderDetailDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrderDetailDtoDomain : TopObject
{
	        /// <summary>
	        /// 商品信息，非淘及散件必填，淘系订单以订单在淘宝系统内容为准
	        /// </summary>
	        [XmlArray("items")]
	        [XmlArrayItem("trade_item_info_dto")]
	        public List<TradeItemInfoDtoDomain> Items { get; set; }
	
	        /// <summary>
	        /// 子订单ID，如果是单一订单，则主订单ID和子订单ID均填入同一值
	        /// </summary>
	        [XmlElement("sub_order_id")]
	        public string SubOrderId { get; set; }
}

	/// <summary>
/// TradeOrderInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrderInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 主订单ID
	        /// </summary>
	        [XmlElement("main_order_id")]
	        public string MainOrderId { get; set; }
	
	        /// <summary>
	        /// 子订单信息
	        /// </summary>
	        [XmlArray("sub_orders")]
	        [XmlArrayItem("trade_order_detail_dto")]
	        public List<TradeOrderDetailDtoDomain> SubOrders { get; set; }
}

	/// <summary>
/// ReceiverDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ReceiverDtoDomain : TopObject
{
	        /// <summary>
	        /// 收件人详细地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 城市
	        /// </summary>
	        [XmlElement("city_name")]
	        public string CityName { get; set; }
	
	        /// <summary>
	        /// 区县
	        /// </summary>
	        [XmlElement("district_name")]
	        public string DistrictName { get; set; }
	
	        /// <summary>
	        /// 收件人手机号，该字段与收件人电话二者必填至少其一
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 收件人电话
	        /// </summary>
	        [XmlElement("phone")]
	        public string Phone { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("pro_name")]
	        public string ProName { get; set; }
	
	        /// <summary>
	        /// 收件人姓名
	        /// </summary>
	        [XmlElement("receiver_name")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("street_name")]
	        public string StreetName { get; set; }
}

	/// <summary>
/// SdtExtendInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class SdtExtendInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 电子面单上打印的发件地址
	        /// </summary>
	        [XmlElement("waybill_print_address")]
	        public string WaybillPrintAddress { get; set; }
}

        #endregion
    }
}
