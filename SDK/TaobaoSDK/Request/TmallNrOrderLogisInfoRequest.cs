using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.order.logis.info
    /// </summary>
    public class TmallNrOrderLogisInfoRequest : BaseTopRequest<Top.Api.Response.TmallNrOrderLogisInfoResponse>
    {
        /// <summary>
        /// 来源标识
        /// </summary>
        public string Channel { get; set; }

        /// <summary>
        /// 主订单号
        /// </summary>
        public string MainOrderIds { get; set; }

        /// <summary>
        /// 卖家ID
        /// </summary>
        public Nullable<long> SellerId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.order.logis.info";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("channel", this.Channel);
            parameters.Add("main_order_ids", this.MainOrderIds);
            parameters.Add("seller_id", this.SellerId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("channel", this.Channel);
            RequestValidator.ValidateRequired("main_order_ids", this.MainOrderIds);
            RequestValidator.ValidateMaxListSize("main_order_ids", this.MainOrderIds, 20);
            RequestValidator.ValidateRequired("seller_id", this.SellerId);
        }

        #endregion
    }
}
