using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.orderstatus.update
    /// </summary>
    public class QimenOrderstatusUpdateRequest : BaseTopRequest<Top.Api.Response.QimenOrderstatusUpdateResponse>
    {
        /// <summary>
        /// 事件发生时间
        /// </summary>
        public string ActionTime { get; set; }

        /// <summary>
        /// 星盘派单号
        /// </summary>
        public string AllocationCode { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// 淘系子订单号
        /// </summary>
        public string OrderCodes { get; set; }

        /// <summary>
        /// 订单状态，门店发货包括X_SHOP_ALLOCATION、X_SHOP_DENYX_SHOP_HANDLED、X_SHOP_CANCEL_CONFIRM、X_SHOP_CANCEL_DENIED、X_MATCHED；门店自提包括X_COMMODITY_CONFIRMX_COMMODITY_TRANSER、X_TRANSFER _SUCCESS、X_SHOP_CANCEL_CONFIRM、X_MATCHED、X_SHOP_DENY
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 目标门店的商户中心门店编码
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        /// <summary>
        /// 业务类型，（枚举值：FAHUO、ZITI）
        /// </summary>
        public string Type { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.orderstatus.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("action_time", this.ActionTime);
            parameters.Add("allocation_code", this.AllocationCode);
            parameters.Add("operator", this.Operator);
            parameters.Add("order_codes", this.OrderCodes);
            parameters.Add("status", this.Status);
            parameters.Add("store_id", this.StoreId);
            parameters.Add("type", this.Type);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("allocation_code", this.AllocationCode);
            RequestValidator.ValidateRequired("operator", this.Operator);
            RequestValidator.ValidateRequired("order_codes", this.OrderCodes);
            RequestValidator.ValidateMaxListSize("order_codes", this.OrderCodes, 100);
            RequestValidator.ValidateRequired("status", this.Status);
            RequestValidator.ValidateRequired("store_id", this.StoreId);
            RequestValidator.ValidateRequired("type", this.Type);
        }

        #endregion
    }
}
