using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.notice.goods.ready
    /// </summary>
    public class TmallNrNoticeGoodsReadyRequest : BaseTopRequest<Top.Api.Response.TmallNrNoticeGoodsReadyResponse>
    {
        /// <summary>
        /// 入参
        /// </summary>
        public string Param0 { get; set; }

        public NrZqsGoodsReadyReqDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.notice.goods.ready";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// NrZqsGoodsReadyReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrZqsGoodsReadyReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务标识dds，zqs等
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 淘宝交易订单号
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public Nullable<long> BizOrderId { get; set; }
	
	        /// <summary>
	        /// 经销商的姓名
	        /// </summary>
	        [XmlElement("dealer_name")]
	        public string DealerName { get; set; }
	
	        /// <summary>
	        /// 经销商的电话
	        /// </summary>
	        [XmlElement("dealer_phone")]
	        public string DealerPhone { get; set; }
	
	        /// <summary>
	        /// 提货码
	        /// </summary>
	        [XmlElement("outer_got_code")]
	        public string OuterGotCode { get; set; }
	
	        /// <summary>
	        /// 配送人员姓名
	        /// </summary>
	        [XmlElement("performer_name")]
	        public string PerformerName { get; set; }
	
	        /// <summary>
	        /// 配送人员电话号码
	        /// </summary>
	        [XmlElement("performer_phone")]
	        public string PerformerPhone { get; set; }
	
	        /// <summary>
	        /// 卖家的sellerId
	        /// </summary>
	        [XmlElement("seller_id")]
	        public Nullable<long> SellerId { get; set; }
	
	        /// <summary>
	        /// 可选参数，编码
	        /// </summary>
	        [XmlElement("trace_id")]
	        public string TraceId { get; set; }
}

        #endregion
    }
}
