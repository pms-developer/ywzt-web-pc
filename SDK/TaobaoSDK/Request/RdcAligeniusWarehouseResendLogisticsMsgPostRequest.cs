using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.warehouse.resend.logistics.msg.post
    /// </summary>
    public class RdcAligeniusWarehouseResendLogisticsMsgPostRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusWarehouseResendLogisticsMsgPostResponse>
    {
        /// <summary>
        /// 参数
        /// </summary>
        public string Param0 { get; set; }

        public SendResendLogisticsMsgDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.warehouse.resend.logistics.msg.post";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// GoodsItemDomain Data Structure.
/// </summary>
[Serializable]

public class GoodsItemDomain : TopObject
{
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品数量
	        /// </summary>
	        [XmlElement("num")]
	        public Nullable<long> Num { get; set; }
	
	        /// <summary>
	        /// 子订单编号
	        /// </summary>
	        [XmlElement("oid")]
	        public Nullable<long> Oid { get; set; }
}

	/// <summary>
/// SendResendLogisticsMsgDtoDomain Data Structure.
/// </summary>
[Serializable]

public class SendResendLogisticsMsgDtoDomain : TopObject
{
	        /// <summary>
	        /// 发货单唯一标识
	        /// </summary>
	        [XmlElement("biz_id")]
	        public string BizId { get; set; }
	
	        /// <summary>
	        /// 物流公司名称
	        /// </summary>
	        [XmlElement("company_name")]
	        public string CompanyName { get; set; }
	
	        /// <summary>
	        /// 该运单所包含的货品列表
	        /// </summary>
	        [XmlArray("goods_item_list")]
	        [XmlArrayItem("goods_item")]
	        public List<GoodsItemDomain> GoodsItemList { get; set; }
	
	        /// <summary>
	        /// 运单号
	        /// </summary>
	        [XmlElement("mail_no")]
	        public string MailNo { get; set; }
	
	        /// <summary>
	        /// 描述
	        /// </summary>
	        [XmlElement("msg")]
	        public string Msg { get; set; }
	
	        /// <summary>
	        /// 成功发货的发货单运单号，菜鸟为LP单号
	        /// </summary>
	        [XmlElement("order_code")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// 平台补发单唯一标识
	        /// </summary>
	        [XmlElement("source_id")]
	        public string SourceId { get; set; }
	
	        /// <summary>
	        /// 运单状态（-1=废单，1=已下发）
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
	
	        /// <summary>
	        /// 主订单
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
}

        #endregion
    }
}
