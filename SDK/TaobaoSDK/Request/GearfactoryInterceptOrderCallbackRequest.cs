using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.gearfactory.intercept.order.callback
    /// </summary>
    public class GearfactoryInterceptOrderCallbackRequest : BaseTopRequest<Top.Api.Response.GearfactoryInterceptOrderCallbackResponse>
    {
        /// <summary>
        /// ("1001", "已发货拦截失败"), ("1002", "已出库拦截失败"), ("1003", "已配货拦截失败"), ("1004", "已拣货拦截失败"), ("1005", "在途中拦截失败"), ("1006", "库存占用拦截失败"), ("1007", "未获取订单拦截失败"), ("1008", "未定义异常失败"), ("1009", "奇门接口调用异常")
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// 截单的子订单id列表
        /// </summary>
        public string SubOrderIds { get; set; }

        /// <summary>
        /// 是否截单成功，true：成功，false：失败
        /// </summary>
        public Nullable<bool> Success { get; set; }

        /// <summary>
        /// 主订单id
        /// </summary>
        public string Tid { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.gearfactory.intercept.order.callback";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("error_code", this.ErrorCode);
            parameters.Add("sub_order_ids", this.SubOrderIds);
            parameters.Add("success", this.Success);
            parameters.Add("tid", this.Tid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxListSize("sub_order_ids", this.SubOrderIds, 20);
        }

        #endregion
    }
}
