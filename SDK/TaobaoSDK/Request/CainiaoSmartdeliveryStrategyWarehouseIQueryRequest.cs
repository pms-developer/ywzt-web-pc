using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.strategy.warehouse.i.query
    /// </summary>
    public class CainiaoSmartdeliveryStrategyWarehouseIQueryRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryStrategyWarehouseIQueryResponse>
    {
        /// <summary>
        /// 查询请求参数
        /// </summary>
        public string ParamQueryDeliveryStrategyRequest { get; set; }

        public QueryDeliveryStrategyRequestDomain ParamQueryDeliveryStrategyRequest_ { set { this.ParamQueryDeliveryStrategyRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.strategy.warehouse.i.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param_query_delivery_strategy_request", this.ParamQueryDeliveryStrategyRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// AddressDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// QueryDeliveryStrategyRequestDomain Data Structure.
/// </summary>
[Serializable]

public class QueryDeliveryStrategyRequestDomain : TopObject
{
	        /// <summary>
	        /// 发货地址
	        /// </summary>
	        [XmlElement("send_address")]
	        public AddressDomain SendAddress { get; set; }
	
	        /// <summary>
	        /// 仓id
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public Nullable<long> WarehouseId { get; set; }
}

        #endregion
    }
}
