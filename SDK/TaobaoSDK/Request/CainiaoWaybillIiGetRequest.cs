using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.waybill.ii.get
    /// </summary>
    public class CainiaoWaybillIiGetRequest : BaseTopRequest<Top.Api.Response.CainiaoWaybillIiGetResponse>
    {
        /// <summary>
        /// 入参信息
        /// </summary>
        public string ParamWaybillCloudPrintApplyNewRequest { get; set; }

        public WaybillCloudPrintApplyNewRequestDomain ParamWaybillCloudPrintApplyNewRequest_ { set { this.ParamWaybillCloudPrintApplyNewRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.waybill.ii.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param_waybill_cloud_print_apply_new_request", this.ParamWaybillCloudPrintApplyNewRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param_waybill_cloud_print_apply_new_request", this.ParamWaybillCloudPrintApplyNewRequest);
        }

	/// <summary>
/// AddressDtoDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDtoDomain : TopObject
{
	        /// <summary>
	        /// 城市，长度小于20
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址，长度小于256
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区，长度小于20
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省，长度小于20
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道，长度小于30
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// UserInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class UserInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 发货地址需要通过<a href="http://open.taobao.com/doc2/detail.htm?spm=a219a.7629140.0.0.3OFCPk&treeId=17&articleId=104860&docType=1">search接口</a>
	        /// </summary>
	        [XmlElement("address")]
	        public AddressDtoDomain Address { get; set; }
	
	        /// <summary>
	        /// 手机号码（手机号和固定电话不能同时为空），长度小于20
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 姓名，长度小于40
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 固定电话（手机号和固定电话不能同时为空），长度小于20
	        /// </summary>
	        [XmlElement("phone")]
	        public string Phone { get; set; }
}

	/// <summary>
/// OrderInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class OrderInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// <a href="http://open.taobao.com/doc2/detail.htm?spm=a219a.7629140.0.0.8cf9Nj&treeId=17&articleId=105085&docType=1#2">订单渠道平台编码</a>
	        /// </summary>
	        [XmlElement("order_channels_type")]
	        public string OrderChannelsType { get; set; }
	
	        /// <summary>
	        /// 订单号,数量限制100，订单号（只限传入数字、字母、下划线和中划线，为避免出现冲突，请按电商平台真实订单号传入，请避免使用同个订单号重复取号）
	        /// </summary>
	        [XmlArray("trade_order_list")]
	        [XmlArrayItem("string")]
	        public List<string> TradeOrderList { get; set; }
}

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain : TopObject
{
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("count")]
	        public Nullable<long> Count { get; set; }
	
	        /// <summary>
	        /// 名称
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
}

	/// <summary>
/// PackageInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class PackageInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 大件快运中的货品描述，比如服装，家具
	        /// </summary>
	        [XmlElement("goods_description")]
	        public string GoodsDescription { get; set; }
	
	        /// <summary>
	        /// 包裹id，用于拆合单场景（只能传入数字、字母和下划线；批量请求时值不得重复，大小写敏感，即123A,123a 不可当做不同ID，否则存在一定可能取号失败）
	        /// </summary>
	        [XmlElement("id")]
	        public string Id { get; set; }
	
	        /// <summary>
	        /// 商品信息,数量限制为100
	        /// </summary>
	        [XmlArray("items")]
	        [XmlArrayItem("item")]
	        public List<ItemDomain> Items { get; set; }
	
	        /// <summary>
	        /// 大件快运中的包装方式描述
	        /// </summary>
	        [XmlElement("packaging_description")]
	        public string PackagingDescription { get; set; }
	
	        /// <summary>
	        /// 子母件模式中的总包裹数/总件数，用于打印当前包裹处于总件数的位置比如5-2，可以表示总包裹数为5，当前为第2个包裹，只有快运公司需要传入，其他的可以不用传入
	        /// </summary>
	        [XmlElement("total_packages_count")]
	        public Nullable<long> TotalPackagesCount { get; set; }
	
	        /// <summary>
	        /// 体积, 单位 ml
	        /// </summary>
	        [XmlElement("volume")]
	        public Nullable<long> Volume { get; set; }
	
	        /// <summary>
	        /// 重量,单位 g
	        /// </summary>
	        [XmlElement("weight")]
	        public Nullable<long> Weight { get; set; }
}

	/// <summary>
/// TradeOrderInfoDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrderInfoDtoDomain : TopObject
{
	        /// <summary>
	        /// 物流服务值（详见https://support-cnkuaidi.taobao.com/doc.htm#?docId=106156&docType=1，如无特殊服务请置空）
	        /// </summary>
	        [XmlElement("logistics_services")]
	        public string LogisticsServices { get; set; }
	
	        /// <summary>
	        /// <a href="http://open.taobao.com/docs/doc.htm?docType=1&articleId=105086&treeId=17&platformId=17#6">请求ID</a>
	        /// </summary>
	        [XmlElement("object_id")]
	        public string ObjectId { get; set; }
	
	        /// <summary>
	        /// 订单信息
	        /// </summary>
	        [XmlElement("order_info")]
	        public OrderInfoDtoDomain OrderInfo { get; set; }
	
	        /// <summary>
	        /// 包裹信息
	        /// </summary>
	        [XmlElement("package_info")]
	        public PackageInfoDtoDomain PackageInfo { get; set; }
	
	        /// <summary>
	        /// 收件人信息
	        /// </summary>
	        [XmlElement("recipient")]
	        public UserInfoDtoDomain Recipient { get; set; }
	
	        /// <summary>
	        /// 云打印标准模板URL（组装云打印结果使用，值格式http://cloudprint.cainiao.com/template/standard/${模板ID}）
	        /// </summary>
	        [XmlElement("template_url")]
	        public string TemplateUrl { get; set; }
	
	        /// <summary>
	        /// 使用者ID（使用电子面单账号的实际商家ID，如存在一个电子面单账号多个店铺使用时，请传入店铺的商家ID）
	        /// </summary>
	        [XmlElement("user_id")]
	        public Nullable<long> UserId { get; set; }
}

	/// <summary>
/// WaybillCloudPrintApplyNewRequestDomain Data Structure.
/// </summary>
[Serializable]

public class WaybillCloudPrintApplyNewRequestDomain : TopObject
{
	        /// <summary>
	        /// <a href="http://open.taobao.com/doc2/detail.htm?spm=a219a.7629140.0.0.8cf9Nj&treeId=17&articleId=105085&docType=1#1">物流公司Code</a>，长度小于20
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 是否使用智分宝预分拣， 仓库WMS系统对接落地配业务，其它场景请不要使用
	        /// </summary>
	        [XmlElement("dms_sorting")]
	        public Nullable<bool> DmsSorting { get; set; }
	
	        /// <summary>
	        /// 设定取号返回的云打印报文是否加密
	        /// </summary>
	        [XmlElement("need_encrypt")]
	        public Nullable<bool> NeedEncrypt { get; set; }
	
	        /// <summary>
	        /// 目前已经不推荐使用此字段，请不要使用
	        /// </summary>
	        [XmlElement("product_code")]
	        public string ProductCode { get; set; }
	
	        /// <summary>
	        /// 配送资源code， 仓库WMS系统对接落地配业务，其它场景请不要使用
	        /// </summary>
	        [XmlElement("resource_code")]
	        public string ResourceCode { get; set; }
	
	        /// <summary>
	        /// 发货人信息
	        /// </summary>
	        [XmlElement("sender")]
	        public UserInfoDtoDomain Sender { get; set; }
	
	        /// <summary>
	        /// 仓code， 仓库WMS系统对接落地配业务，其它场景请不要使用
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 订单上是否带3PLtiming属性, 该属性需要严格与订单上属性保持一致，如果不确定，请使用默认false。
	        /// </summary>
	        [XmlElement("three_pl_timing")]
	        public Nullable<bool> ThreePlTiming { get; set; }
	
	        /// <summary>
	        /// 请求面单信息，数量限制为10
	        /// </summary>
	        [XmlArray("trade_order_info_dtos")]
	        [XmlArrayItem("trade_order_info_dto")]
	        public List<TradeOrderInfoDtoDomain> TradeOrderInfoDtos { get; set; }
}

        #endregion
    }
}
