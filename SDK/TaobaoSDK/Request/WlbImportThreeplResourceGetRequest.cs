using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.wlb.import.threepl.resource.get
    /// </summary>
    public class WlbImportThreeplResourceGetRequest : BaseTopRequest<Top.Api.Response.WlbImportThreeplResourceGetResponse>
    {
        /// <summary>
        /// 发货地区域id
        /// </summary>
        public Nullable<long> FromId { get; set; }

        /// <summary>
        /// 收件人地址
        /// </summary>
        public string ToAddress { get; set; }

        public AddressDtoDomain ToAddress_ { set { this.ToAddress = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// ONLINE或者OFFLINE
        /// </summary>
        public string Type { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.wlb.import.threepl.resource.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("from_id", this.FromId);
            parameters.Add("to_address", this.ToAddress);
            parameters.Add("type", this.Type);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("from_id", this.FromId);
            RequestValidator.ValidateRequired("to_address", this.ToAddress);
            RequestValidator.ValidateRequired("type", this.Type);
        }

	/// <summary>
/// AddressDtoDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDtoDomain : TopObject
{
	        /// <summary>
	        /// 市级地址
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 国家地址
	        /// </summary>
	        [XmlElement("country")]
	        public string Country { get; set; }
	
	        /// <summary>
	        /// 区级地址
	        /// </summary>
	        [XmlElement("county")]
	        public string County { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail_address")]
	        public string DetailAddress { get; set; }
	
	        /// <summary>
	        /// 省级地址
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道地址
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

        #endregion
    }
}
