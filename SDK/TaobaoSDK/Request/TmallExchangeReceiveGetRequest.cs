using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.exchange.receive.get
    /// </summary>
    public class TmallExchangeReceiveGetRequest : BaseTopRequest<Top.Api.Response.TmallExchangeReceiveGetResponse>
    {
        /// <summary>
        /// 正向订单号
        /// </summary>
        public Nullable<long> BizOrderId { get; set; }

        /// <summary>
        /// 买家id
        /// </summary>
        public Nullable<long> BuyerId { get; set; }

        /// <summary>
        /// 买家昵称
        /// </summary>
        public string BuyerNick { get; set; }

        /// <summary>
        /// 换货状态，具体包括：换货待处理(1), 待买家退货(2), 买家已退货，待收货(3),  换货关闭(4), 换货成功(5), 待买家修改(6), 待发出换货商品(12), 待买家收货(13), 请退款(14)
        /// </summary>
        public string DisputeStatusArray { get; set; }

        /// <summary>
        /// 查询申请时间段的结束时间点
        /// </summary>
        public Nullable<DateTime> EndCreatedTime { get; set; }

        /// <summary>
        /// 查询修改时间段的结束时间点
        /// </summary>
        public Nullable<DateTime> EndGmtModifedTime { get; set; }

        /// <summary>
        /// 返回字段。目前支持dispute_id, bizorder_id, num, buyer_nick, status, created, modified, reason, title, buyer_logistic_no, seller_logistic_no, bought_sku, exchange_sku, buyer_address, address, buyer_phone, buyer_logistic_name, seller_logistic_name, alipay_no, buyer_name, seller_nick
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 快递单号
        /// </summary>
        public string LogisticNo { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 每页条数
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 退款单号ID列表，最多只能输入20个id
        /// </summary>
        public string RefundIdArray { get; set; }

        /// <summary>
        /// 查询申请时间段的开始时间点
        /// </summary>
        public Nullable<DateTime> StartCreatedTime { get; set; }

        /// <summary>
        /// 查询修改时间段的开始时间点
        /// </summary>
        public Nullable<DateTime> StartGmtModifiedTime { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.exchange.receive.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("biz_order_id", this.BizOrderId);
            parameters.Add("buyer_id", this.BuyerId);
            parameters.Add("buyer_nick", this.BuyerNick);
            parameters.Add("dispute_status_array", this.DisputeStatusArray);
            parameters.Add("end_created_time", this.EndCreatedTime);
            parameters.Add("end_gmt_modifed_time", this.EndGmtModifedTime);
            parameters.Add("fields", this.Fields);
            parameters.Add("logistic_no", this.LogisticNo);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("refund_id_array", this.RefundIdArray);
            parameters.Add("start_created_time", this.StartCreatedTime);
            parameters.Add("start_gmt_modified_time", this.StartGmtModifiedTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxListSize("dispute_status_array", this.DisputeStatusArray, 20);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxListSize("fields", this.Fields, 100);
            RequestValidator.ValidateMaxListSize("refund_id_array", this.RefundIdArray, 20);
        }

        #endregion
    }
}
