using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.items.marking
    /// </summary>
    public class QimenItemsMarkingRequest : BaseTopRequest<Top.Api.Response.QimenItemsMarkingResponse>
    {
        /// <summary>
        /// 操作类型，string（50），ADD=打标，DELETE=去标，必填
        /// </summary>
        public string ActionType { get; set; }

        /// <summary>
        /// 线上商品ID，long，必填
        /// </summary>
        public string ItemIds { get; set; }

        /// <summary>
        /// 备注，string（500）
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 打标值，string（50），TBKU=同步库存标，MDZT=门店自提标，必填
        /// </summary>
        public string TagType { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.items.marking";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("action_type", this.ActionType);
            parameters.Add("item_ids", this.ItemIds);
            parameters.Add("remark", this.Remark);
            parameters.Add("tag_type", this.TagType);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("action_type", this.ActionType);
            RequestValidator.ValidateRequired("item_ids", this.ItemIds);
            RequestValidator.ValidateMaxListSize("item_ids", this.ItemIds, 20);
            RequestValidator.ValidateRequired("tag_type", this.TagType);
        }

        #endregion
    }
}
