using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.hk.clearance.get
    /// </summary>
    public class TmallHkClearanceGetRequest : BaseTopRequest<Top.Api.Response.TmallHkClearanceGetResponse>
    {
        /// <summary>
        /// 是否需要身份证图片，不需要可以缩短接口响应时间
        /// </summary>
        public Nullable<bool> NeedImage { get; set; }

        /// <summary>
        /// 天猫国际订单号
        /// </summary>
        public Nullable<long> OrderId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.hk.clearance.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("need_image", this.NeedImage);
            parameters.Add("order_id", this.OrderId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("order_id", this.OrderId);
        }

        #endregion
    }
}
