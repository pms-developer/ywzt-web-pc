using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.merchant.inventory.adjust
    /// </summary>
    public class CainiaoMerchantInventoryAdjustRequest : BaseTopRequest<Top.Api.Response.CainiaoMerchantInventoryAdjustResponse>
    {
        /// <summary>
        /// 商家仓编辑库存
        /// </summary>
        public string AdjustRequest { get; set; }

        public List<MerStoreInvAdjustDtoDomain> AdjustRequest_ { set { this.AdjustRequest = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 调用方应用名
        /// </summary>
        public string AppName { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        public string Operation { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.merchant.inventory.adjust";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("adjust_request", this.AdjustRequest);
            parameters.Add("app_name", this.AppName);
            parameters.Add("operation", this.Operation);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("adjust_request", this.AdjustRequest);
            RequestValidator.ValidateObjectMaxListSize("adjust_request", this.AdjustRequest, 20);
            RequestValidator.ValidateRequired("app_name", this.AppName);
        }

	/// <summary>
/// MerStoreInvAdjustDtoDomain Data Structure.
/// </summary>
[Serializable]

public class MerStoreInvAdjustDtoDomain : TopObject
{
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("attribute")]
	        public string Attribute { get; set; }
	
	        /// <summary>
	        /// 库存类型
	        /// </summary>
	        [XmlElement("inventory_type")]
	        public Nullable<long> InventoryType { get; set; }
	
	        /// <summary>
	        /// 外部操作唯一编码
	        /// </summary>
	        [XmlElement("out_biz_code")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public Nullable<long> Quantity { get; set; }
	
	        /// <summary>
	        /// 货品id
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public Nullable<long> ScItemId { get; set; }
	
	        /// <summary>
	        /// 仓库编码
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
}

        #endregion
    }
}
