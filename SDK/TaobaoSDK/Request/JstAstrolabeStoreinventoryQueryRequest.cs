using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.jst.astrolabe.storeinventory.query
    /// </summary>
    public class JstAstrolabeStoreinventoryQueryRequest : BaseTopRequest<Top.Api.Response.JstAstrolabeStoreinventoryQueryResponse>
    {
        /// <summary>
        /// 门店
        /// </summary>
        public string Stores { get; set; }

        public List<StoreDomain> Stores_ { set { this.Stores = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.jst.astrolabe.storeinventory.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("stores", this.Stores);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("stores", this.Stores, 20);
        }

	/// <summary>
/// StoreInventoryDomain Data Structure.
/// </summary>
[Serializable]

public class StoreInventoryDomain : TopObject
{
	        /// <summary>
	        /// ISV系统中商品编码
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 淘宝后端商品id
	        /// </summary>
	        [XmlElement("sc_item_id")]
	        public Nullable<long> ScItemId { get; set; }
}

	/// <summary>
/// StoreDomain Data Structure.
/// </summary>
[Serializable]

public class StoreDomain : TopObject
{
	        /// <summary>
	        /// 门店库存
	        /// </summary>
	        [XmlArray("store_inventories")]
	        [XmlArrayItem("store_inventory")]
	        public List<StoreInventoryDomain> StoreInventories { get; set; }
	
	        /// <summary>
	        /// 门店ID(商户中心) 或 电商仓ID
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public string WarehouseId { get; set; }
	
	        /// <summary>
	        /// 库存来源
	        /// </summary>
	        [XmlElement("warehouse_type")]
	        public string WarehouseType { get; set; }
}

        #endregion
    }
}
