using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.item.tag.ops
    /// </summary>
    public class TmallNrItemTagOpsRequest : BaseTopRequest<Top.Api.Response.TmallNrItemTagOpsResponse>
    {
        /// <summary>
        /// 请求入参
        /// </summary>
        public string TagReqDTO { get; set; }

        public TagReqDtoDomain TagReqDTO_ { set { this.TagReqDTO = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.item.tag.ops";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("tag_req_d_t_o", this.TagReqDTO);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("tag_req_d_t_o", this.TagReqDTO);
        }

	/// <summary>
/// TagReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class TagReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务身份标识,dss定时送;FN蜂鸟,CN菜鸟
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 商品编码列表
	        /// </summary>
	        [XmlArray("item_ids")]
	        [XmlArrayItem("big_decimal")]
	        public List<string> ItemIds { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("seller_id")]
	        public Nullable<long> SellerId { get; set; }
	
	        /// <summary>
	        /// 信息追踪串,用于后续排查问题,以及与外部厂商对账等场景下使用
	        /// </summary>
	        [XmlElement("trace_id")]
	        public string TraceId { get; set; }
	
	        /// <summary>
	        /// 商品标操作类型，1:打标，2:去标
	        /// </summary>
	        [XmlElement("type")]
	        public Nullable<long> Type { get; set; }
}

        #endregion
    }
}
