using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.ordermsg.update
    /// </summary>
    public class RdcAligeniusOrdermsgUpdateRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusOrdermsgUpdateResponse>
    {
        /// <summary>
        /// 子订单（消息中传的子订单）
        /// </summary>
        public Nullable<long> Oid { get; set; }

        /// <summary>
        /// 处理状态，1=成功，2=处理失败
        /// </summary>
        public Nullable<long> Status { get; set; }

        /// <summary>
        /// 主订单（消息中传的主订单）
        /// </summary>
        public Nullable<long> Tid { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.ordermsg.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("oid", this.Oid);
            parameters.Add("status", this.Status);
            parameters.Add("tid", this.Tid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("oid", this.Oid);
            RequestValidator.ValidateRequired("status", this.Status);
            RequestValidator.ValidateRequired("tid", this.Tid);
        }

        #endregion
    }
}
