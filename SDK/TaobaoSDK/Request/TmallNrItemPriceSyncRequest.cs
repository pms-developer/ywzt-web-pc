using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.item.price.sync
    /// </summary>
    public class TmallNrItemPriceSyncRequest : BaseTopRequest<Top.Api.Response.TmallNrItemPriceSyncResponse>
    {
        /// <summary>
        /// 入参
        /// </summary>
        public string Param0 { get; set; }

        public NrSyncItemStorePriceReqDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.item.price.sync";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// NrSyncItemStorePriceDetailReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrSyncItemStorePriceDetailReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 商品门店价格
	        /// </summary>
	        [XmlElement("price")]
	        public Nullable<long> Price { get; set; }
	
	        /// <summary>
	        /// 天猫门店id
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
}

	/// <summary>
/// NrSyncItemStorePriceReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrSyncItemStorePriceReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务身份标识
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// list
	        /// </summary>
	        [XmlArray("detail_list")]
	        [XmlArrayItem("nr_sync_item_store_price_detail_req_dto")]
	        public List<NrSyncItemStorePriceDetailReqDtoDomain> DetailList { get; set; }
	
	        /// <summary>
	        /// 天猫商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public Nullable<long> ItemId { get; set; }
}

        #endregion
    }
}
