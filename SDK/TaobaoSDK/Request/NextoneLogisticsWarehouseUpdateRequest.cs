using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.nextone.logistics.warehouse.update
    /// </summary>
    public class NextoneLogisticsWarehouseUpdateRequest : BaseTopRequest<Top.Api.Response.NextoneLogisticsWarehouseUpdateResponse>
    {
        /// <summary>
        /// 退款编号
        /// </summary>
        public Nullable<long> RefundId { get; set; }

        /// <summary>
        /// 退货入仓状态 1.已入仓
        /// </summary>
        public Nullable<long> WarehouseStatus { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.nextone.logistics.warehouse.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("refund_id", this.RefundId);
            parameters.Add("warehouse_status", this.WarehouseStatus);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("refund_id", this.RefundId);
            RequestValidator.ValidateRequired("warehouse_status", this.WarehouseStatus);
        }

        #endregion
    }
}
