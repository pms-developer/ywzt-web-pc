using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.sdtquerystation
    /// </summary>
    public class OmniorderStoreSdtquerystationRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreSdtquerystationResponse>
    {
        /// <summary>
        /// 取号时返回的packageId
        /// </summary>
        public Nullable<long> ParamLong2 { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.sdtquerystation";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param_long2", this.ParamLong2);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param_long2", this.ParamLong2);
        }

        #endregion
    }
}
