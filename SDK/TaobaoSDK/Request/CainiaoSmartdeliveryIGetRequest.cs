using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.i.get
    /// </summary>
    public class CainiaoSmartdeliveryIGetRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryIGetResponse>
    {
        /// <summary>
        /// <a href="http://open.taobao.com/docs/doc.htm?treeId=319&articleId=106295&docType=1">智能发货引擎</a>批量请求参数
        /// </summary>
        public string SmartDeliveryBatchRequest { get; set; }

        public SmartDeliveryBatchRequestDomain SmartDeliveryBatchRequest_ { set { this.SmartDeliveryBatchRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.i.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("smart_delivery_batch_request", this.SmartDeliveryBatchRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("smart_delivery_batch_request", this.SmartDeliveryBatchRequest);
        }

	/// <summary>
/// AddressDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// UserInfoDomain Data Structure.
/// </summary>
[Serializable]

public class UserInfoDomain : TopObject
{
	        /// <summary>
	        /// 发货地址信息
	        /// </summary>
	        [XmlElement("address")]
	        public AddressDomain Address { get; set; }
	
	        /// <summary>
	        /// 手机号码
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 姓名
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 固定电话
	        /// </summary>
	        [XmlElement("phone")]
	        public string Phone { get; set; }
	
	        /// <summary>
	        /// 仓库id
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public Nullable<long> WarehouseId { get; set; }
}

	/// <summary>
/// TradeOrderDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrderDomain : TopObject
{
	        /// <summary>
	        /// 买家留言
	        /// </summary>
	        [XmlElement("buyer_message")]
	        public string BuyerMessage { get; set; }
	
	        /// <summary>
	        /// 卖家备注
	        /// </summary>
	        [XmlElement("seller_memo")]
	        public string SellerMemo { get; set; }
	
	        /// <summary>
	        /// 订单号，特别注意：对于淘系订单，务必要传淘系订单号
	        /// </summary>
	        [XmlElement("trade_order_id")]
	        public string TradeOrderId { get; set; }
}

	/// <summary>
/// OrderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class OrderInfoDomain : TopObject
{
	        /// <summary>
	        /// <a href="http://open.taobao.com/doc2/detail.htm?spm=a219a.7629140.0.0.8cf9Nj&treeId=17&articleId=105085&docType=1#2">订单渠道平台编码</a>
	        /// </summary>
	        [XmlElement("order_channel_type")]
	        public string OrderChannelType { get; set; }
	
	        /// <summary>
	        /// 订单信息，数量限制100
	        /// </summary>
	        [XmlArray("trade_order_list")]
	        [XmlArrayItem("trade_order")]
	        public List<TradeOrderDomain> TradeOrderList { get; set; }
}

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain : TopObject
{
	        /// <summary>
	        /// 商品编码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("count")]
	        public Nullable<long> Count { get; set; }
	
	        /// <summary>
	        /// 名称
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
}

	/// <summary>
/// PackageInfoDomain Data Structure.
/// </summary>
[Serializable]

public class PackageInfoDomain : TopObject
{
	        /// <summary>
	        /// 包裹id,拆合单使用，<a href="http://open.taobao.com/doc2/detail.htm?spm=a219a.7629140.0.0.8cf9Nj&treeId=17&articleId=105085&docType=1#10">使用方式</a>
	        /// </summary>
	        [XmlElement("id")]
	        public string Id { get; set; }
	
	        /// <summary>
	        /// 商品信息,数量限制为100
	        /// </summary>
	        [XmlArray("item_list")]
	        [XmlArrayItem("item")]
	        public List<ItemDomain> ItemList { get; set; }
	
	        /// <summary>
	        /// 体积，单位ml
	        /// </summary>
	        [XmlElement("volume")]
	        public Nullable<long> Volume { get; set; }
	
	        /// <summary>
	        /// 重量，单位g
	        /// </summary>
	        [XmlElement("weight")]
	        public Nullable<long> Weight { get; set; }
}

	/// <summary>
/// TradeOrderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrderInfoDomain : TopObject
{
	        /// <summary>
	        /// <a href="http://open.taobao.com/docs/doc.htm?docType=1&articleId=105086&treeId=17&platformId=17#6">请求ID</a>
	        /// </summary>
	        [XmlElement("object_id")]
	        public string ObjectId { get; set; }
	
	        /// <summary>
	        /// 订单信息
	        /// </summary>
	        [XmlElement("order_info")]
	        public OrderInfoDomain OrderInfo { get; set; }
	
	        /// <summary>
	        /// 包裹信息
	        /// </summary>
	        [XmlElement("package_info")]
	        public PackageInfoDomain PackageInfo { get; set; }
	
	        /// <summary>
	        /// 收件人信息
	        /// </summary>
	        [XmlElement("recipient")]
	        public UserInfoDomain Recipient { get; set; }
	
	        /// <summary>
	        /// 使用者ID<a href="http://open.taobao.com/support/hotProblemDetail.htm?spm=a219a.7386793.0.0.4mwx9s&id=244622&tagId=">获取方式</a>
	        /// </summary>
	        [XmlElement("user_id")]
	        public Nullable<long> UserId { get; set; }
}

	/// <summary>
/// SmartDeliveryBatchRequestDomain Data Structure.
/// </summary>
[Serializable]

public class SmartDeliveryBatchRequestDomain : TopObject
{
	        /// <summary>
	        /// 电子面单信息是否需要加密 true: 加密; false: 不加密
	        /// </summary>
	        [XmlElement("need_encrypt")]
	        public Nullable<bool> NeedEncrypt { get; set; }
	
	        /// <summary>
	        /// 用户信息
	        /// </summary>
	        [XmlElement("sender")]
	        public UserInfoDomain Sender { get; set; }
	
	        /// <summary>
	        /// 交易订单信息，数量限制为10
	        /// </summary>
	        [XmlArray("trade_order_info_list")]
	        [XmlArrayItem("trade_order_info")]
	        public List<TradeOrderInfoDomain> TradeOrderInfoList { get; set; }
}

        #endregion
    }
}
