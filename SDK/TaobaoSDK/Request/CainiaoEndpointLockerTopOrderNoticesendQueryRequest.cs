using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.endpoint.locker.top.order.noticesend.query
    /// </summary>
    public class CainiaoEndpointLockerTopOrderNoticesendQueryRequest : BaseTopRequest<Top.Api.Response.CainiaoEndpointLockerTopOrderNoticesendQueryResponse>
    {
        /// <summary>
        /// 收件人手机号
        /// </summary>
        public string GetterPhone { get; set; }

        /// <summary>
        /// 运单号
        /// </summary>
        public string MailNo { get; set; }

        /// <summary>
        /// 站点id
        /// </summary>
        public string StationId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.endpoint.locker.top.order.noticesend.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("getter_phone", this.GetterPhone);
            parameters.Add("mail_no", this.MailNo);
            parameters.Add("station_id", this.StationId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("mail_no", this.MailNo);
            RequestValidator.ValidateRequired("station_id", this.StationId);
        }

        #endregion
    }
}
