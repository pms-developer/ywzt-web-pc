using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.distribution.logistics.cancel
    /// </summary>
    public class RdcAligeniusDistributionLogisticsCancelRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusDistributionLogisticsCancelResponse>
    {
        /// <summary>
        /// 参数
        /// </summary>
        public string Param0 { get; set; }

        public CancelDistributionDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.distribution.logistics.cancel";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// CancelDistributionDtoDomain Data Structure.
/// </summary>
[Serializable]

public class CancelDistributionDtoDomain : TopObject
{
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("logistics_company_code")]
	        public string LogisticsCompanyCode { get; set; }
	
	        /// <summary>
	        /// 物流公司名称
	        /// </summary>
	        [XmlElement("logistics_company_name")]
	        public string LogisticsCompanyName { get; set; }
	
	        /// <summary>
	        /// 物流运单号
	        /// </summary>
	        [XmlElement("mail_no")]
	        public string MailNo { get; set; }
	
	        /// <summary>
	        /// 截配描述信息
	        /// </summary>
	        [XmlElement("msg")]
	        public string Msg { get; set; }
	
	        /// <summary>
	        /// 子订单号
	        /// </summary>
	        [XmlElement("oid")]
	        public Nullable<long> Oid { get; set; }
	
	        /// <summary>
	        /// 截配操作时间
	        /// </summary>
	        [XmlElement("operate_time")]
	        public Nullable<DateTime> OperateTime { get; set; }
	
	        /// <summary>
	        /// ERP截配单号
	        /// </summary>
	        [XmlElement("out_id")]
	        public string OutId { get; set; }
	
	        /// <summary>
	        /// 退款单号
	        /// </summary>
	        [XmlElement("refund_id")]
	        public Nullable<long> RefundId { get; set; }
	
	        /// <summary>
	        /// 截配状态，1=截配成功
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
	
	        /// <summary>
	        /// 主订单号
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
}

        #endregion
    }
}
