using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.warehouse
    /// </summary>
    public class OmniorderStoreWarehouseRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreWarehouseResponse>
    {
        /// <summary>
        /// 操作，1表示增加或者更新，2表示删除，3表示查询
        /// </summary>
        public Nullable<long> Operation { get; set; }

        /// <summary>
        /// 门店id
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        /// <summary>
        /// 仓联系地址
        /// </summary>
        public string WarehouseAddress { get; set; }

        /// <summary>
        /// 仓编码
        /// </summary>
        public string WarehouseCode { get; set; }

        /// <summary>
        /// 仓联系人
        /// </summary>
        public string WarehouseContact { get; set; }

        /// <summary>
        /// 仓联系电话
        /// </summary>
        public string WarehouseMobile { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.warehouse";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("operation", this.Operation);
            parameters.Add("store_id", this.StoreId);
            parameters.Add("warehouse_address", this.WarehouseAddress);
            parameters.Add("warehouse_code", this.WarehouseCode);
            parameters.Add("warehouse_contact", this.WarehouseContact);
            parameters.Add("warehouse_mobile", this.WarehouseMobile);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("operation", this.Operation);
        }

        #endregion
    }
}
