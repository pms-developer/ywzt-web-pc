using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.priceoffer.i.query
    /// </summary>
    public class CainiaoSmartdeliveryPriceofferIQueryRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryPriceofferIQueryResponse>
    {
        /// <summary>
        /// 请求参数
        /// </summary>
        public string QueryCpPriceInfoRequest { get; set; }

        public QueryCpPriceInfoRequestDomain QueryCpPriceInfoRequest_ { set { this.QueryCpPriceInfoRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.priceoffer.i.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("query_cp_price_info_request", this.QueryCpPriceInfoRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("query_cp_price_info_request", this.QueryCpPriceInfoRequest);
        }

	/// <summary>
/// AddressDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// QueryCpPriceInfoRequestDomain Data Structure.
/// </summary>
[Serializable]

public class QueryCpPriceInfoRequestDomain : TopObject
{
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 发货地址
	        /// </summary>
	        [XmlElement("send_address")]
	        public AddressDomain SendAddress { get; set; }
	
	        /// <summary>
	        /// 仓id
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public Nullable<long> WarehouseId { get; set; }
}

        #endregion
    }
}
