using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.issubscribe.i.query
    /// </summary>
    public class CainiaoSmartdeliveryIssubscribeIQueryRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryIssubscribeIQueryResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.issubscribe.i.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
