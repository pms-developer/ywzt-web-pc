using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.items.tag.query
    /// </summary>
    public class QimenItemsTagQueryRequest : BaseTopRequest<Top.Api.Response.QimenItemsTagQueryResponse>
    {
        /// <summary>
        /// 线上淘宝商品ID，long，必填
        /// </summary>
        public string ItemIds { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.items.tag.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("item_ids", this.ItemIds);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("item_ids", this.ItemIds);
            RequestValidator.ValidateMaxListSize("item_ids", this.ItemIds, 20);
        }

        #endregion
    }
}
