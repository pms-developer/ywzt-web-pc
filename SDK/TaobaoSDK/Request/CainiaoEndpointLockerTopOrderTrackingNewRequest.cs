using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.endpoint.locker.top.order.tracking.new
    /// </summary>
    public class CainiaoEndpointLockerTopOrderTrackingNewRequest : BaseTopRequest<Top.Api.Response.CainiaoEndpointLockerTopOrderTrackingNewResponse>
    {
        /// <summary>
        /// 回传信息
        /// </summary>
        public string TrackInfo { get; set; }

        public CollectTrackingInfoDomain TrackInfo_ { set { this.TrackInfo = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.endpoint.locker.top.order.tracking.new";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("track_info", this.TrackInfo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("track_info", this.TrackInfo);
        }

	/// <summary>
/// CollectTrackingInfoDomain Data Structure.
/// </summary>
[Serializable]

public class CollectTrackingInfoDomain : TopObject
{
	        /// <summary>
	        /// 动作编码
	        /// </summary>
	        [XmlElement("action_code")]
	        public string ActionCode { get; set; }
	
	        /// <summary>
	        /// 动作发生时间戳，单位：毫秒
	        /// </summary>
	        [XmlElement("action_time")]
	        public Nullable<long> ActionTime { get; set; }
	
	        /// <summary>
	        /// 快递公司编号
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 扩展数据（JSON格式的键值对），如果是取件码取件，请返回取件使用的取件码
	        /// </summary>
	        [XmlElement("extra")]
	        public string Extra { get; set; }
	
	        /// <summary>
	        /// 订单对应的取件人电话
	        /// </summary>
	        [XmlElement("getter_phone")]
	        public string GetterPhone { get; set; }
	
	        /// <summary>
	        /// 运单号
	        /// </summary>
	        [XmlElement("mail_no")]
	        public string MailNo { get; set; }
	
	        /// <summary>
	        /// 站点订单编码
	        /// </summary>
	        [XmlElement("order_code")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// 订单类型(0-代收业务)
	        /// </summary>
	        [XmlElement("order_type")]
	        public Nullable<long> OrderType { get; set; }
	
	        /// <summary>
	        /// 订单对应的投件人电话
	        /// </summary>
	        [XmlElement("post_phone")]
	        public string PostPhone { get; set; }
	
	        /// <summary>
	        /// 站点id
	        /// </summary>
	        [XmlElement("station_id")]
	        public string StationId { get; set; }
	
	        /// <summary>
	        /// 站点编码
	        /// </summary>
	        [XmlElement("station_no")]
	        public string StationNo { get; set; }
}

        #endregion
    }
}
