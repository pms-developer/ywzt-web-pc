using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.fulfill.logistics.consign
    /// </summary>
    public class TmallNrFulfillLogisticsConsignRequest : BaseTopRequest<Top.Api.Response.TmallNrFulfillLogisticsConsignResponse>
    {
        /// <summary>
        /// 入参对象
        /// </summary>
        public string Param0 { get; set; }

        public NrStoreGoodsReadyReqDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.fulfill.logistics.consign";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// NrStoreGoodsReadyReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrStoreGoodsReadyReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务标识，dss标识定时送业务；jsd表示极速达业务
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 发货公司编码
	        /// </summary>
	        [XmlElement("company_code")]
	        public string CompanyCode { get; set; }
	
	        /// <summary>
	        /// 发货公司
	        /// </summary>
	        [XmlElement("company_name")]
	        public string CompanyName { get; set; }
	
	        /// <summary>
	        /// 发货编码
	        /// </summary>
	        [XmlElement("company_order_no")]
	        public string CompanyOrderNo { get; set; }
	
	        /// <summary>
	        /// 交易主订单号
	        /// </summary>
	        [XmlElement("main_order_id")]
	        public Nullable<long> MainOrderId { get; set; }
	
	        /// <summary>
	        /// 配送人员的姓名
	        /// </summary>
	        [XmlElement("performer_name")]
	        public string PerformerName { get; set; }
	
	        /// <summary>
	        /// 配送人员的电话
	        /// </summary>
	        [XmlElement("performer_phone")]
	        public string PerformerPhone { get; set; }
}

        #endregion
    }
}
