using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.refused
    /// </summary>
    public class OmniorderStoreRefusedRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreRefusedResponse>
    {
        /// <summary>
        /// ISV的系统时间
        /// </summary>
        public Nullable<long> ReportTimestamp { get; set; }

        /// <summary>
        /// 子订单列表
        /// </summary>
        public string SubOrderList { get; set; }

        public List<SubOrderDomain> SubOrderList_ { set { this.SubOrderList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 淘宝交易主订单ID
        /// </summary>
        public Nullable<long> Tid { get; set; }

        /// <summary>
        /// 跟踪Id
        /// </summary>
        public string TraceId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.refused";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("report_timestamp", this.ReportTimestamp);
            parameters.Add("sub_order_list", this.SubOrderList);
            parameters.Add("tid", this.Tid);
            parameters.Add("trace_id", this.TraceId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("report_timestamp", this.ReportTimestamp);
            RequestValidator.ValidateRequired("sub_order_list", this.SubOrderList);
            RequestValidator.ValidateObjectMaxListSize("sub_order_list", this.SubOrderList, 20);
            RequestValidator.ValidateRequired("tid", this.Tid);
        }

	/// <summary>
/// SubOrderDomain Data Structure.
/// </summary>
[Serializable]

public class SubOrderDomain : TopObject
{
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("attributes")]
	        public string Attributes { get; set; }
	
	        /// <summary>
	        /// 0表示无系统异常
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 异常描述
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 操作者
	        /// </summary>
	        [XmlElement("operator")]
	        public string Operator { get; set; }
	
	        /// <summary>
	        /// 店铺Id, 可能是门店或者电商仓
	        /// </summary>
	        [XmlElement("store_id")]
	        public string StoreId { get; set; }
	
	        /// <summary>
	        /// 店铺名称
	        /// </summary>
	        [XmlElement("store_name")]
	        public string StoreName { get; set; }
	
	        /// <summary>
	        /// 店铺类型, 门店或者电商仓
	        /// </summary>
	        [XmlElement("store_type")]
	        public string StoreType { get; set; }
	
	        /// <summary>
	        /// 子订单Id
	        /// </summary>
	        [XmlElement("sub_oid")]
	        public Nullable<long> SubOid { get; set; }
	
	        /// <summary>
	        /// 主订单Id
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
}

        #endregion
    }
}
