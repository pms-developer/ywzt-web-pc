using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.priceoffer.i.update
    /// </summary>
    public class CainiaoSmartdeliveryPriceofferIUpdateRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryPriceofferIUpdateResponse>
    {
        /// <summary>
        /// 物流公司价格信息
        /// </summary>
        public string CpPriceInfo { get; set; }

        public CpPriceInfoDomain CpPriceInfo_ { set { this.CpPriceInfo = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.priceoffer.i.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("cp_price_info", this.CpPriceInfo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("cp_price_info", this.CpPriceInfo);
        }

	/// <summary>
/// AddressDomain Data Structure.
/// </summary>
[Serializable]

public class AddressDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("detail")]
	        public string Detail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// CostPriceDomain Data Structure.
/// </summary>
[Serializable]

public class CostPriceDomain : TopObject
{
	        /// <summary>
	        /// 续重价格，单位分
	        /// </summary>
	        [XmlElement("continus_measure_price")]
	        public Nullable<long> ContinusMeasurePrice { get; set; }
	
	        /// <summary>
	        /// 续重，单位g
	        /// </summary>
	        [XmlElement("continus_measure_weight")]
	        public Nullable<long> ContinusMeasureWeight { get; set; }
	
	        /// <summary>
	        /// 首重价格，单位分
	        /// </summary>
	        [XmlElement("first_measure_price")]
	        public Nullable<long> FirstMeasurePrice { get; set; }
	
	        /// <summary>
	        /// 首重，单位g
	        /// </summary>
	        [XmlElement("first_measure_weight")]
	        public Nullable<long> FirstMeasureWeight { get; set; }
}

	/// <summary>
/// WeightRangeDomain Data Structure.
/// </summary>
[Serializable]

public class WeightRangeDomain : TopObject
{
	        /// <summary>
	        /// 最小限制重量
	        /// </summary>
	        [XmlElement("lower_limit_weight")]
	        public Nullable<long> LowerLimitWeight { get; set; }
	
	        /// <summary>
	        /// 最大限制重量
	        /// </summary>
	        [XmlElement("upper_limit_weight")]
	        public Nullable<long> UpperLimitWeight { get; set; }
}

	/// <summary>
/// PriceRuleDomain Data Structure.
/// </summary>
[Serializable]

public class PriceRuleDomain : TopObject
{
	        /// <summary>
	        /// 成本报价
	        /// </summary>
	        [XmlElement("cost_price")]
	        public CostPriceDomain CostPrice { get; set; }
	
	        /// <summary>
	        /// 适用重量范围
	        /// </summary>
	        [XmlElement("weight_range")]
	        public WeightRangeDomain WeightRange { get; set; }
}

	/// <summary>
/// AddressAreaDomain Data Structure.
/// </summary>
[Serializable]

public class AddressAreaDomain : TopObject
{
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 对于全国统一价格的，不需要设置省市区信息，只需要填写国家为中国即可：表明发往全国的价格，目前仅支持填写中国
	        /// </summary>
	        [XmlElement("country")]
	        public string Country { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
}

	/// <summary>
/// PriceOfferDomain Data Structure.
/// </summary>
[Serializable]

public class PriceOfferDomain : TopObject
{
	        /// <summary>
	        /// 报价规则
	        /// </summary>
	        [XmlArray("price_rule_list")]
	        [XmlArrayItem("price_rule")]
	        public List<PriceRuleDomain> PriceRuleList { get; set; }
	
	        /// <summary>
	        /// 到货地区
	        /// </summary>
	        [XmlElement("receive_area")]
	        public AddressAreaDomain ReceiveArea { get; set; }
}

	/// <summary>
/// CpPriceInfoDomain Data Structure.
/// </summary>
[Serializable]

public class CpPriceInfoDomain : TopObject
{
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 报价列表
	        /// </summary>
	        [XmlArray("price_offer_list")]
	        [XmlArrayItem("price_offer")]
	        public List<PriceOfferDomain> PriceOfferList { get; set; }
	
	        /// <summary>
	        /// 发货地址
	        /// </summary>
	        [XmlElement("send_address")]
	        public AddressDomain SendAddress { get; set; }
	
	        /// <summary>
	        /// 仓id
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public Nullable<long> WarehouseId { get; set; }
}

        #endregion
    }
}
