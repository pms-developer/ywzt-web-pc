using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.jst.astrolabe.storeinventory.iteminitial
    /// </summary>
    public class JstAstrolabeStoreinventoryIteminitialRequest : BaseTopRequest<Top.Api.Response.JstAstrolabeStoreinventoryIteminitialResponse>
    {
        /// <summary>
        /// 操作时间
        /// </summary>
        public string OperationTime { get; set; }

        /// <summary>
        /// 门店列表
        /// </summary>
        public string Stores { get; set; }

        public List<StoreDomain> Stores_ { set { this.Stores = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.jst.astrolabe.storeinventory.iteminitial";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("operation_time", this.OperationTime);
            parameters.Add("stores", this.Stores);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("operation_time", this.OperationTime);
            RequestValidator.ValidateObjectMaxListSize("stores", this.Stores, 20);
        }

	/// <summary>
/// StoreInventoryDomain Data Structure.
/// </summary>
[Serializable]

public class StoreInventoryDomain : TopObject
{
	        /// <summary>
	        /// 单据流水号，用于幂等操作
	        /// </summary>
	        [XmlElement("bill_num")]
	        public string BillNum { get; set; }
	
	        /// <summary>
	        /// 库存类型
	        /// </summary>
	        [XmlElement("inventory_type")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 淘宝前端商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// ISV系统中商品编码
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 对应类型的库存数量（正数）
	        /// </summary>
	        [XmlElement("quantity")]
	        public string Quantity { get; set; }
	
	        /// <summary>
	        /// 商品的SKU编码
	        /// </summary>
	        [XmlElement("sku_id")]
	        public string SkuId { get; set; }
}

	/// <summary>
/// StoreDomain Data Structure.
/// </summary>
[Serializable]

public class StoreDomain : TopObject
{
	        /// <summary>
	        /// 门店库存列表
	        /// </summary>
	        [XmlArray("store_inventories")]
	        [XmlArrayItem("store_inventory")]
	        public List<StoreInventoryDomain> StoreInventories { get; set; }
	
	        /// <summary>
	        /// 门店ID(商户中心)或 电商仓ID
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public string WarehouseId { get; set; }
	
	        /// <summary>
	        /// 库存来源的类型
	        /// </summary>
	        [XmlElement("warehouse_type")]
	        public string WarehouseType { get; set; }
}

        #endregion
    }
}
