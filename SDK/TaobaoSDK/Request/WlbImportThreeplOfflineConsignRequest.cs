using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.wlb.import.threepl.offline.consign
    /// </summary>
    public class WlbImportThreeplOfflineConsignRequest : BaseTopRequest<Top.Api.Response.WlbImportThreeplOfflineConsignResponse>
    {
        /// <summary>
        /// 发件人地址库id
        /// </summary>
        public Nullable<long> FromId { get; set; }

        /// <summary>
        /// 资源code
        /// </summary>
        public string ResCode { get; set; }

        /// <summary>
        /// 资源id
        /// </summary>
        public Nullable<long> ResId { get; set; }

        /// <summary>
        /// 交易单号
        /// </summary>
        public Nullable<long> TradeId { get; set; }

        /// <summary>
        /// 运单号
        /// </summary>
        public string WaybillNo { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.wlb.import.threepl.offline.consign";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("from_id", this.FromId);
            parameters.Add("res_code", this.ResCode);
            parameters.Add("res_id", this.ResId);
            parameters.Add("trade_id", this.TradeId);
            parameters.Add("waybill_no", this.WaybillNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
