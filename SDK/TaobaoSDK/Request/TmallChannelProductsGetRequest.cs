using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.channel.products.get
    /// </summary>
    public class TmallChannelProductsGetRequest : BaseTopRequest<Top.Api.Response.TmallChannelProductsGetResponse>
    {
        /// <summary>
        /// top_query_product_d_o
        /// </summary>
        public string TopQueryProductDO { get; set; }

        public TopQueryProductDoDomain TopQueryProductDO_ { set { this.TopQueryProductDO = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.channel.products.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("top_query_product_d_o", this.TopQueryProductDO);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// TopQueryProductDoDomain Data Structure.
/// </summary>
[Serializable]

public class TopQueryProductDoDomain : TopObject
{
	        /// <summary>
	        /// 渠道[21 零售plus]
	        /// </summary>
	        [XmlElement("channel")]
	        public Nullable<long> Channel { get; set; }
	
	        /// <summary>
	        /// 要查询的产品id 列表
	        /// </summary>
	        [XmlArray("ids")]
	        [XmlArrayItem("number")]
	        public List<string> Ids { get; set; }
	
	        /// <summary>
	        /// 关联的前端宝贝id列表
	        /// </summary>
	        [XmlArray("item_ids")]
	        [XmlArrayItem("number")]
	        public List<string> ItemIds { get; set; }
	
	        /// <summary>
	        /// 当前要查看的页数
	        /// </summary>
	        [XmlElement("page_num")]
	        public Nullable<long> PageNum { get; set; }
	
	        /// <summary>
	        /// 分页大小
	        /// </summary>
	        [XmlElement("page_size")]
	        public Nullable<long> PageSize { get; set; }
	
	        /// <summary>
	        /// 产品线id
	        /// </summary>
	        [XmlElement("product_line_id")]
	        public Nullable<long> ProductLineId { get; set; }
	
	        /// <summary>
	        /// 产品商家编码
	        /// </summary>
	        [XmlElement("product_outer_id")]
	        public string ProductOuterId { get; set; }
	
	        /// <summary>
	        /// sku商家编码
	        /// </summary>
	        [XmlElement("sku_outer_id")]
	        public string SkuOuterId { get; set; }
}

        #endregion
    }
}
