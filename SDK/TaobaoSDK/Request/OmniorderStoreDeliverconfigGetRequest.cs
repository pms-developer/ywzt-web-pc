using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.deliverconfig.get
    /// </summary>
    public class OmniorderStoreDeliverconfigGetRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreDeliverconfigGetResponse>
    {
        /// <summary>
        /// 是否是活动期
        /// </summary>
        public Nullable<bool> Activity { get; set; }

        /// <summary>
        /// 门店ID
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.deliverconfig.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("activity", this.Activity);
            parameters.Add("store_id", this.StoreId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("store_id", this.StoreId);
        }

        #endregion
    }
}
