using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.fulfill.logistics.query
    /// </summary>
    public class TmallNrFulfillLogisticsQueryRequest : BaseTopRequest<Top.Api.Response.TmallNrFulfillLogisticsQueryResponse>
    {
        /// <summary>
        /// 业务标识，dss标识定时送业务；jsd表示极速达业务
        /// </summary>
        public string BizIdentity { get; set; }

        /// <summary>
        /// 交易主订单号
        /// </summary>
        public Nullable<long> MainOrderId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.fulfill.logistics.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("biz_identity", this.BizIdentity);
            parameters.Add("main_order_id", this.MainOrderId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
