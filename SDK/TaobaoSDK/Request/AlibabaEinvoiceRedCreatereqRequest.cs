using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: alibaba.einvoice.red.createreq
    /// </summary>
    public class AlibabaEinvoiceRedCreatereqRequest : BaseTopRequest<Top.Api.Response.AlibabaEinvoiceRedCreatereqResponse>
    {
        /// <summary>
        /// 蓝票流水号，优先级高于发票代码+发票号码
        /// </summary>
        public string BlueSerialNo { get; set; }

        /// <summary>
        /// 蓝票发票代码
        /// </summary>
        public string InvoiceCode { get; set; }

        /// <summary>
        /// 蓝票发票号码
        /// </summary>
        public string InvoiceNo { get; set; }

        /// <summary>
        /// 销售方税号
        /// </summary>
        public string PayeeRegisterNo { get; set; }

        /// <summary>
        /// 红票流水号
        /// </summary>
        public string RedSerialNo { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "alibaba.einvoice.red.createreq";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("blue_serial_no", this.BlueSerialNo);
            parameters.Add("invoice_code", this.InvoiceCode);
            parameters.Add("invoice_no", this.InvoiceNo);
            parameters.Add("payee_register_no", this.PayeeRegisterNo);
            parameters.Add("red_serial_no", this.RedSerialNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxLength("blue_serial_no", this.BlueSerialNo, 20);
            RequestValidator.ValidateMaxLength("invoice_code", this.InvoiceCode, 16);
            RequestValidator.ValidateMaxLength("invoice_no", this.InvoiceNo, 12);
            RequestValidator.ValidateRequired("payee_register_no", this.PayeeRegisterNo);
            RequestValidator.ValidateRequired("red_serial_no", this.RedSerialNo);
            RequestValidator.ValidateMaxLength("red_serial_no", this.RedSerialNo, 20);
        }

        #endregion
    }
}
