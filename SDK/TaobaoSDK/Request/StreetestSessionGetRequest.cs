using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.streetest.session.get
    /// </summary>
    public class StreetestSessionGetRequest : BaseTopRequest<Top.Api.Response.StreetestSessionGetResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.streetest.session.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
