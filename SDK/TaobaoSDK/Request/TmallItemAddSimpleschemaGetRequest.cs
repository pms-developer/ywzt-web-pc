using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.item.add.simpleschema.get
    /// </summary>
    public class TmallItemAddSimpleschemaGetRequest : BaseTopRequest<Top.Api.Response.TmallItemAddSimpleschemaGetResponse>
    {
        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.item.add.simpleschema.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
