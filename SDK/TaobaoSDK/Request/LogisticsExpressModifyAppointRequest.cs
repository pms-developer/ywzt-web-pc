using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.logistics.express.modify.appoint
    /// </summary>
    public class LogisticsExpressModifyAppointRequest : BaseTopRequest<Top.Api.Response.LogisticsExpressModifyAppointResponse>
    {
        /// <summary>
        /// 改约请求对象
        /// </summary>
        public string ExpressModifyAppointTopRequest { get; set; }

        public ExpressModifyAppointTopRequestDtoDomain ExpressModifyAppointTopRequest_ { set { this.ExpressModifyAppointTopRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.logistics.express.modify.appoint";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("express_modify_appoint_top_request", this.ExpressModifyAppointTopRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("express_modify_appoint_top_request", this.ExpressModifyAppointTopRequest);
        }

	/// <summary>
/// ExpressModifyAppointTopRequestDtoDomain Data Structure.
/// </summary>
[Serializable]

public class ExpressModifyAppointTopRequestDtoDomain : TopObject
{
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("feature")]
	        public string Feature { get; set; }
	
	        /// <summary>
	        /// 改约日期
	        /// </summary>
	        [XmlElement("os_date")]
	        public Nullable<DateTime> OsDate { get; set; }
	
	        /// <summary>
	        /// 外部订单号
	        /// </summary>
	        [XmlElement("out_order_code")]
	        public string OutOrderCode { get; set; }
	
	        /// <summary>
	        /// 收货人地址
	        /// </summary>
	        [XmlElement("receiver_address")]
	        public string ReceiverAddress { get; set; }
	
	        /// <summary>
	        /// 收货人电话
	        /// </summary>
	        [XmlElement("receiver_mobile")]
	        public string ReceiverMobile { get; set; }
	
	        /// <summary>
	        /// 收货人姓名
	        /// </summary>
	        [XmlElement("receiver_name")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 应到达日期
	        /// </summary>
	        [XmlElement("sc_date")]
	        public Nullable<DateTime> ScDate { get; set; }
	
	        /// <summary>
	        /// 卖家Id
	        /// </summary>
	        [XmlElement("seller_id")]
	        public Nullable<long> SellerId { get; set; }
	
	        /// <summary>
	        /// 子交易单号
	        /// </summary>
	        [XmlArray("sub_trade_ids")]
	        [XmlArrayItem("string")]
	        public List<string> SubTradeIds { get; set; }
	
	        /// <summary>
	        /// 交易号
	        /// </summary>
	        [XmlElement("trade_id")]
	        public string TradeId { get; set; }
}

        #endregion
    }
}
