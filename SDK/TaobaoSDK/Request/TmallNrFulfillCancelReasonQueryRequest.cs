using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.fulfill.cancel.reason.query
    /// </summary>
    public class TmallNrFulfillCancelReasonQueryRequest : BaseTopRequest<Top.Api.Response.TmallNrFulfillCancelReasonQueryResponse>
    {
        /// <summary>
        /// 淘宝交易的主订单号
        /// </summary>
        public Nullable<long> MainOrderId { get; set; }

        /// <summary>
        /// 商家的sellerID
        /// </summary>
        public Nullable<long> SellerId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.fulfill.cancel.reason.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("main_order_id", this.MainOrderId);
            parameters.Add("seller_id", this.SellerId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
