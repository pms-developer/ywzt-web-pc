using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.order.create.sync
    /// </summary>
    public class TmallNrOrderCreateSyncRequest : BaseTopRequest<Top.Api.Response.TmallNrOrderCreateSyncResponse>
    {
        /// <summary>
        /// 买家id
        /// </summary>
        public Nullable<long> BuyerId { get; set; }

        /// <summary>
        /// 1
        /// </summary>
        public string Param0 { get; set; }

        public NrSyncCreateOrderReqDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.order.create.sync";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("buyer_id", this.BuyerId);
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// NrSyncCreateOrderItemReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrSyncCreateOrderItemReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 购买数量
	        /// </summary>
	        [XmlElement("buy_amount")]
	        public Nullable<long> BuyAmount { get; set; }
	
	        /// <summary>
	        /// 优惠信息，已乘购买数量。同步创建时使用，异步创建时只在订单上记录，不做金额的扣减。单品抵扣金额，单位：分；
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public Nullable<long> DiscountFee { get; set; }
	
	        /// <summary>
	        /// 商品id，淘系的商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public Nullable<long> ItemId { get; set; }
	
	        /// <summary>
	        /// 商品sku id，淘系商品skuId
	        /// </summary>
	        [XmlElement("sku_id")]
	        public Nullable<long> SkuId { get; set; }
	
	        /// <summary>
	        /// 异步创建订单的商品单价，同步创建时使用淘系线上的价格。单位：分
	        /// </summary>
	        [XmlElement("unit_price")]
	        public Nullable<long> UnitPrice { get; set; }
}

	/// <summary>
/// NrSyncCreateOrderReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrSyncCreateOrderReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务身份标识
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 商品列表
	        /// </summary>
	        [XmlArray("items")]
	        [XmlArrayItem("nr_sync_create_order_item_req_dto")]
	        public List<NrSyncCreateOrderItemReqDtoDomain> Items { get; set; }
	
	        /// <summary>
	        /// 线下会员码
	        /// </summary>
	        [XmlElement("out_member_code")]
	        public string OutMemberCode { get; set; }
	
	        /// <summary>
	        /// 天猫门店code
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 线下门店交易单号
	        /// </summary>
	        [XmlElement("store_order_id")]
	        public string StoreOrderId { get; set; }
	
	        /// <summary>
	        /// 淘系会员码，优先级高
	        /// </summary>
	        [XmlElement("taobao_member_code")]
	        public string TaobaoMemberCode { get; set; }
}

        #endregion
    }
}
