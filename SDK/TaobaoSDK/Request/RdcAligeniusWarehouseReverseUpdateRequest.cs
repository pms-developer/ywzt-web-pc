using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.warehouse.reverse.update
    /// </summary>
    public class RdcAligeniusWarehouseReverseUpdateRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusWarehouseReverseUpdateResponse>
    {
        /// <summary>
        /// 参数
        /// </summary>
        public string Param0 { get; set; }

        public UpdateReverseStatusDtoDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.warehouse.reverse.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// WarehouseReverseRefundGoodsItemDtoDomain Data Structure.
/// </summary>
[Serializable]

public class WarehouseReverseRefundGoodsItemDtoDomain : TopObject
{
	        /// <summary>
	        /// 实付金额（单位：分）
	        /// </summary>
	        [XmlElement("actual_fee")]
	        public Nullable<long> ActualFee { get; set; }
	
	        /// <summary>
	        /// 实发数量
	        /// </summary>
	        [XmlElement("actual_qty")]
	        public Nullable<long> ActualQty { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("auction_name")]
	        public string AuctionName { get; set; }
	
	        /// <summary>
	        /// 扩展字段，JSONObject格式
	        /// </summary>
	        [XmlElement("extra")]
	        public string Extra { get; set; }
	
	        /// <summary>
	        /// 货品行ID
	        /// </summary>
	        [XmlElement("id")]
	        public string Id { get; set; }
	
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("item_code")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 货品仓储系统编码
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("item_name")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 子订单ID
	        /// </summary>
	        [XmlElement("oid")]
	        public Nullable<long> Oid { get; set; }
	
	        /// <summary>
	        /// 货主
	        /// </summary>
	        [XmlElement("owner_nick")]
	        public string OwnerNick { get; set; }
	
	        /// <summary>
	        /// 计划数量
	        /// </summary>
	        [XmlElement("plan_qty")]
	        public Nullable<long> PlanQty { get; set; }
	
	        /// <summary>
	        /// 计划状态（1=正品；2=残品；3=部分正品）
	        /// </summary>
	        [XmlElement("plan_status")]
	        public Nullable<long> PlanStatus { get; set; }
	
	        /// <summary>
	        /// 单价（单位：分）
	        /// </summary>
	        [XmlElement("price")]
	        public Nullable<long> Price { get; set; }
	
	        /// <summary>
	        /// 商品条码
	        /// </summary>
	        [XmlElement("qr_code")]
	        public string QrCode { get; set; }
	
	        /// <summary>
	        /// 状态（1=正品；2=残品；3=部分正品；4=未确认）
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
	
	        /// <summary>
	        /// 1=淘系子订单，2=赠品,3=未知
	        /// </summary>
	        [XmlElement("type")]
	        public Nullable<long> Type { get; set; }
}

	/// <summary>
/// UpdateReverseStatusDtoDomain Data Structure.
/// </summary>
[Serializable]

public class UpdateReverseStatusDtoDomain : TopObject
{
	        /// <summary>
	        /// 详细地址
	        /// </summary>
	        [XmlElement("adr")]
	        public string Adr { get; set; }
	
	        /// <summary>
	        /// 城市
	        /// </summary>
	        [XmlElement("city_name")]
	        public string CityName { get; set; }
	
	        /// <summary>
	        /// 国家
	        /// </summary>
	        [XmlElement("country_name")]
	        public string CountryName { get; set; }
	
	        /// <summary>
	        /// 物流公司code
	        /// </summary>
	        [XmlElement("cp_code")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 物流公司名称
	        /// </summary>
	        [XmlElement("cp_name")]
	        public string CpName { get; set; }
	
	        /// <summary>
	        /// 创建时间
	        /// </summary>
	        [XmlElement("create_time")]
	        public string CreateTime { get; set; }
	
	        /// <summary>
	        /// 县区
	        /// </summary>
	        [XmlElement("district_name")]
	        public string DistrictName { get; set; }
	
	        /// <summary>
	        /// 运单编号
	        /// </summary>
	        [XmlElement("mail_no")]
	        public string MailNo { get; set; }
	
	        /// <summary>
	        /// 描述信息
	        /// </summary>
	        [XmlElement("msg")]
	        public string Msg { get; set; }
	
	        /// <summary>
	        /// 子订单号
	        /// </summary>
	        [XmlElement("oid")]
	        public Nullable<long> Oid { get; set; }
	
	        /// <summary>
	        /// 省份
	        /// </summary>
	        [XmlElement("province_name")]
	        public string ProvinceName { get; set; }
	
	        /// <summary>
	        /// 退款单号
	        /// </summary>
	        [XmlElement("refund_id")]
	        public Nullable<long> RefundId { get; set; }
	
	        /// <summary>
	        /// 销退单号
	        /// </summary>
	        [XmlElement("return_order_id")]
	        public string ReturnOrderId { get; set; }
	
	        /// <summary>
	        /// 销退单状态(1=已创建；2=待入库；3=已入库，5=已取消)
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
	
	        /// <summary>
	        /// 主订单号
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
	
	        /// <summary>
	        /// 仓库名称
	        /// </summary>
	        [XmlElement("warehouse_name")]
	        public string WarehouseName { get; set; }
	
	        /// <summary>
	        /// 货品列表
	        /// </summary>
	        [XmlArray("warehouse_reverse_refund_goods_item_d_t_o_list")]
	        [XmlArrayItem("warehouse_reverse_refund_goods_item_dto")]
	        public List<WarehouseReverseRefundGoodsItemDtoDomain> WarehouseReverseRefundGoodsItemDTOList { get; set; }
}

        #endregion
    }
}
