using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rds.db.getdb
    /// </summary>
    public class RdsDbGetdbRequest : BaseTopRequest<Top.Api.Response.RdsDbGetdbResponse>
    {
        /// <summary>
        /// 账户名
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// 实例名
        /// </summary>
        public string InstanceName { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rds.db.getdb";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("account_name", this.AccountName);
            parameters.Add("instance_name", this.InstanceName);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("instance_name", this.InstanceName);
        }

        #endregion
    }
}
