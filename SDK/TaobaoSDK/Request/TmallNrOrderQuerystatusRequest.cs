using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.order.querystatus
    /// </summary>
    public class TmallNrOrderQuerystatusRequest : BaseTopRequest<Top.Api.Response.TmallNrOrderQuerystatusResponse>
    {
        /// <summary>
        /// 可用于透传的买家id，常用于外部技术体系的买家信息透传
        /// </summary>
        public Nullable<long> BuyerId { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string Param1 { get; set; }

        public NrPayStatusReqDomain Param1_ { set { this.Param1 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.order.querystatus";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("buyer_id", this.BuyerId);
            parameters.Add("param1", this.Param1);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param1", this.Param1);
        }

	/// <summary>
/// NrPayStatusReqDomain Data Structure.
/// </summary>
[Serializable]

public class NrPayStatusReqDomain : TopObject
{
	        /// <summary>
	        /// 新零售交易平台分配给每个场景特定的code
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 淘系订单id
	        /// </summary>
	        [XmlElement("biz_order_id")]
	        public string BizOrderId { get; set; }
}

        #endregion
    }
}
