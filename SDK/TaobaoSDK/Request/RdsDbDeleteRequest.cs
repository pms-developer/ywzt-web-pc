using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rds.db.delete
    /// </summary>
    public class RdsDbDeleteRequest : BaseTopRequest<Top.Api.Response.RdsDbDeleteResponse>
    {
        /// <summary>
        /// 数据库的name，可以通过 taobao.rds.db.get 获取
        /// </summary>
        public string DbName { get; set; }

        /// <summary>
        /// rds的实例名
        /// </summary>
        public string InstanceName { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rds.db.delete";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("db_name", this.DbName);
            parameters.Add("instance_name", this.InstanceName);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("db_name", this.DbName);
            RequestValidator.ValidateRequired("instance_name", this.InstanceName);
            RequestValidator.ValidateMaxLength("instance_name", this.InstanceName, 30);
        }

        #endregion
    }
}
