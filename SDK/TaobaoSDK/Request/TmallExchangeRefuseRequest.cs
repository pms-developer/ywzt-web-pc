using System;
using System.Collections.Generic;
using Top.Api.Response;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.exchange.refuse
    /// </summary>
    public class TmallExchangeRefuseRequest : BaseTopRequest<TmallExchangeRefuseResponse>, ITopUploadRequest<TmallExchangeRefuseResponse>
    {
        /// <summary>
        /// 换货单号ID
        /// </summary>
        public Nullable<long> DisputeId { get; set; }

        /// <summary>
        /// 返回字段。目前支持dispute_id, bizorder_id, modified, status
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 拒绝换货申请时的留言
        /// </summary>
        public string LeaveMessage { get; set; }

        /// <summary>
        /// 凭证图片
        /// </summary>
        public FileItem LeaveMessagePics { get; set; }

        /// <summary>
        /// 换货原因对应ID
        /// </summary>
        public Nullable<long> SellerRefuseReasonId { get; set; }

        #region BaseTopRequest Members

        public override string GetApiName()
        {
            return "tmall.exchange.refuse";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("dispute_id", this.DisputeId);
            parameters.Add("fields", this.Fields);
            parameters.Add("leave_message", this.LeaveMessage);
            parameters.Add("seller_refuse_reason_id", this.SellerRefuseReasonId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("dispute_id", this.DisputeId);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxListSize("fields", this.Fields, 20);
            RequestValidator.ValidateRequired("seller_refuse_reason_id", this.SellerRefuseReasonId);
        }

        #endregion

        #region ITopUploadRequest Members

        public IDictionary<string, FileItem> GetFileParameters()
        {
            IDictionary<string, FileItem> parameters = new Dictionary<string, FileItem>();
            parameters.Add("leave_message_pics", this.LeaveMessagePics);
            return parameters;
        }

        #endregion
    }
}
