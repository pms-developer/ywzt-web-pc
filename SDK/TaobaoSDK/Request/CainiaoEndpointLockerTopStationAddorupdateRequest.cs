using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.endpoint.locker.top.station.addorupdate
    /// </summary>
    public class CainiaoEndpointLockerTopStationAddorupdateRequest : BaseTopRequest<Top.Api.Response.CainiaoEndpointLockerTopStationAddorupdateResponse>
    {
        /// <summary>
        /// 站点信息
        /// </summary>
        public string StationInfo { get; set; }

        public StationInfoDomain StationInfo_ { set { this.StationInfo = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.endpoint.locker.top.station.addorupdate";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("station_info", this.StationInfo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// StationInfoDomain Data Structure.
/// </summary>
[Serializable]

public class StationInfoDomain : TopObject
{
	        /// <summary>
	        /// 城市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 站点联系方式
	        /// </summary>
	        [XmlElement("contact")]
	        public string Contact { get; set; }
	
	        /// <summary>
	        /// 坐标类型，MARS-火星坐标（高德坐标），BAIDU-百度坐标,GPS-GPS坐标
	        /// </summary>
	        [XmlElement("coord_type")]
	        public string CoordType { get; set; }
	
	        /// <summary>
	        /// 区县
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 站点服务能力描述
	        /// </summary>
	        [XmlElement("extra")]
	        public string Extra { get; set; }
	
	        /// <summary>
	        /// 小区
	        /// </summary>
	        [XmlElement("housing_estate")]
	        public string HousingEstate { get; set; }
	
	        /// <summary>
	        /// 站点照片url
	        /// </summary>
	        [XmlElement("img_url")]
	        public string ImgUrl { get; set; }
	
	        /// <summary>
	        /// 省份
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 站点详细地址
	        /// </summary>
	        [XmlElement("station_addr")]
	        public string StationAddr { get; set; }
	
	        /// <summary>
	        /// 站点id
	        /// </summary>
	        [XmlElement("station_id")]
	        public string StationId { get; set; }
	
	        /// <summary>
	        /// 站点纬度
	        /// </summary>
	        [XmlElement("station_lat")]
	        public string StationLat { get; set; }
	
	        /// <summary>
	        /// 站点经度
	        /// </summary>
	        [XmlElement("station_lng")]
	        public string StationLng { get; set; }
	
	        /// <summary>
	        /// 站点名字
	        /// </summary>
	        [XmlElement("station_name")]
	        public string StationName { get; set; }
	
	        /// <summary>
	        /// 站点编码
	        /// </summary>
	        [XmlElement("station_no")]
	        public string StationNo { get; set; }
	
	        /// <summary>
	        /// 站点类型：100-代收点
	        /// </summary>
	        [XmlElement("station_type")]
	        public Nullable<long> StationType { get; set; }
	
	        /// <summary>
	        /// 0-上线，1-下线
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
	
	        /// <summary>
	        /// 街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("zip")]
	        public string Zip { get; set; }
}

        #endregion
    }
}
