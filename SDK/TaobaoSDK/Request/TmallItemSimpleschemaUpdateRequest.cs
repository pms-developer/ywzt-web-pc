using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.item.simpleschema.update
    /// </summary>
    public class TmallItemSimpleschemaUpdateRequest : BaseTopRequest<Top.Api.Response.TmallItemSimpleschemaUpdateResponse>
    {
        /// <summary>
        /// 商品id
        /// </summary>
        public Nullable<long> ItemId { get; set; }

        /// <summary>
        /// 编辑商品时提交的xml信息
        /// </summary>
        public string SchemaXmlFields { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.item.simpleschema.update";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("item_id", this.ItemId);
            parameters.Add("schema_xml_fields", this.SchemaXmlFields);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("item_id", this.ItemId);
            RequestValidator.ValidateRequired("schema_xml_fields", this.SchemaXmlFields);
        }

        #endregion
    }
}
