using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.fenxiao.product.to.channel.import
    /// </summary>
    public class FenxiaoProductToChannelImportRequest : BaseTopRequest<Top.Api.Response.FenxiaoProductToChannelImportResponse>
    {
        /// <summary>
        /// 要导入的渠道[21 零售PLUS]目前仅支持此渠道
        /// </summary>
        public Nullable<long> Channel { get; set; }

        /// <summary>
        /// 要导入的产品id
        /// </summary>
        public Nullable<long> ProductId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.fenxiao.product.to.channel.import";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("channel", this.Channel);
            parameters.Add("product_id", this.ProductId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("channel", this.Channel);
            RequestValidator.ValidateRequired("product_id", this.ProductId);
        }

        #endregion
    }
}
