using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.rdc.aligenius.sendgoods.cancel
    /// </summary>
    public class RdcAligeniusSendgoodsCancelRequest : BaseTopRequest<Top.Api.Response.RdcAligeniusSendgoodsCancelResponse>
    {
        /// <summary>
        /// 请求参数
        /// </summary>
        public string Param { get; set; }

        public CancelGoodsDtoDomain Param_ { set { this.Param = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.rdc.aligenius.sendgoods.cancel";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param", this.Param);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param", this.Param);
        }

	/// <summary>
/// CancelGoodsDtoDomain Data Structure.
/// </summary>
[Serializable]

public class CancelGoodsDtoDomain : TopObject
{
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("auction_id")]
	        public Nullable<long> AuctionId { get; set; }
	
	        /// <summary>
	        /// 商品数量
	        /// </summary>
	        [XmlElement("auction_num")]
	        public Nullable<long> AuctionNum { get; set; }
	
	        /// <summary>
	        /// 描述
	        /// </summary>
	        [XmlElement("msg")]
	        public string Msg { get; set; }
	
	        /// <summary>
	        /// 子订单ID
	        /// </summary>
	        [XmlElement("oid")]
	        public Nullable<long> Oid { get; set; }
	
	        /// <summary>
	        /// 操作时间
	        /// </summary>
	        [XmlElement("operate_time")]
	        public Nullable<DateTime> OperateTime { get; set; }
	
	        /// <summary>
	        /// 商家商品ID
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 退款金额 单位 分
	        /// </summary>
	        [XmlElement("refund_fee")]
	        public Nullable<long> RefundFee { get; set; }
	
	        /// <summary>
	        /// 退款单ID
	        /// </summary>
	        [XmlElement("refund_id")]
	        public Nullable<long> RefundId { get; set; }
	
	        /// <summary>
	        /// 状态SUCCESS、FAIL
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 主订单ID
	        /// </summary>
	        [XmlElement("tid")]
	        public Nullable<long> Tid { get; set; }
}

        #endregion
    }
}
