using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.smartdelivery.cp.i.modify
    /// </summary>
    public class CainiaoSmartdeliveryCpIModifyRequest : BaseTopRequest<Top.Api.Response.CainiaoSmartdeliveryCpIModifyResponse>
    {
        /// <summary>
        /// 修改智选CP请求
        /// </summary>
        public string ModifySmartDeliveryCpRequest { get; set; }

        public ModifySmartDeliveryCpRequestDomain ModifySmartDeliveryCpRequest_ { set { this.ModifySmartDeliveryCpRequest = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.smartdelivery.cp.i.modify";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("modify_smart_delivery_cp_request", this.ModifySmartDeliveryCpRequest);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("modify_smart_delivery_cp_request", this.ModifySmartDeliveryCpRequest);
        }

	/// <summary>
/// ModifySmartDeliveryCpRequestDomain Data Structure.
/// </summary>
[Serializable]

public class ModifySmartDeliveryCpRequestDomain : TopObject
{
	        /// <summary>
	        /// 修改的物流公司编码
	        /// </summary>
	        [XmlElement("modified_cp_code")]
	        public string ModifiedCpCode { get; set; }
	
	        /// <summary>
	        /// 原有的物流公司编码
	        /// </summary>
	        [XmlElement("origin_cp_code")]
	        public string OriginCpCode { get; set; }
	
	        /// <summary>
	        /// 电子面单号
	        /// </summary>
	        [XmlElement("waybill_code")]
	        public string WaybillCode { get; set; }
}

        #endregion
    }
}
