using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.fulfill.order.query
    /// </summary>
    public class TmallNrFulfillOrderQueryRequest : BaseTopRequest<Top.Api.Response.TmallNrFulfillOrderQueryResponse>
    {
        /// <summary>
        /// 业务标识，dss标识定时送业务；jsd表示极速达业务
        /// </summary>
        public string BizIdentity { get; set; }

        /// <summary>
        /// 预留-扩展信息
        /// </summary>
        public string ExtParam { get; set; }

        /// <summary>
        /// 交易主订单号
        /// </summary>
        public Nullable<long> OrderId { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.fulfill.order.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("biz_identity", this.BizIdentity);
            parameters.Add("ext_param", this.ExtParam);
            parameters.Add("order_id", this.OrderId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
