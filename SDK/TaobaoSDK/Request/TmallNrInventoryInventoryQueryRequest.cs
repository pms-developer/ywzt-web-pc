using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: tmall.nr.inventory.inventory.query
    /// </summary>
    public class TmallNrInventoryInventoryQueryRequest : BaseTopRequest<Top.Api.Response.TmallNrInventoryInventoryQueryResponse>
    {
        /// <summary>
        /// 入参
        /// </summary>
        public string ReqDto { get; set; }

        public NrInventoryQueryReqDtoDomain ReqDto_ { set { this.ReqDto = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "tmall.nr.inventory.inventory.query";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("req_dto", this.ReqDto);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("req_dto", this.ReqDto);
        }

	/// <summary>
/// NrInventoryQueryDetailReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrInventoryQueryDetailReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 天猫商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public Nullable<long> ItemId { get; set; }
	
	        /// <summary>
	        /// 天猫门店id
	        /// </summary>
	        [XmlElement("store_code")]
	        public string StoreCode { get; set; }
}

	/// <summary>
/// NrInventoryQueryReqDtoDomain Data Structure.
/// </summary>
[Serializable]

public class NrInventoryQueryReqDtoDomain : TopObject
{
	        /// <summary>
	        /// 业务身份标识
	        /// </summary>
	        [XmlElement("biz_identity")]
	        public string BizIdentity { get; set; }
	
	        /// <summary>
	        /// 明细
	        /// </summary>
	        [XmlArray("detail_req_dtos")]
	        [XmlArrayItem("nr_inventory_query_detail_req_dto")]
	        public List<NrInventoryQueryDetailReqDtoDomain> DetailReqDtos { get; set; }
}

        #endregion
    }
}
