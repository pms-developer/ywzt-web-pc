using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.dddelivery.status.get
    /// </summary>
    public class OmniorderDddeliveryStatusGetRequest : BaseTopRequest<Top.Api.Response.OmniorderDddeliveryStatusGetResponse>
    {
        /// <summary>
        /// 点点送配送状态查询参数，优先级“点点送运单号”>"订单id">"点点送运单号列表"
        /// </summary>
        public string Param0 { get; set; }

        public DdDeliveryQueryDomain Param0_ { set { this.Param0 = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.dddelivery.status.get";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("param0", this.Param0);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("param0", this.Param0);
        }

	/// <summary>
/// DdDeliveryQueryDomain Data Structure.
/// </summary>
[Serializable]

public class DdDeliveryQueryDomain : TopObject
{
	        /// <summary>
	        /// 点点送运单号
	        /// </summary>
	        [XmlElement("delivery_order_id")]
	        public Nullable<long> DeliveryOrderId { get; set; }
	
	        /// <summary>
	        /// 点点送运单号列表
	        /// </summary>
	        [XmlArray("delivery_order_ids")]
	        [XmlArrayItem("number")]
	        public List<string> DeliveryOrderIds { get; set; }
	
	        /// <summary>
	        /// 订单id
	        /// </summary>
	        [XmlElement("trade_id")]
	        public Nullable<long> TradeId { get; set; }
}

        #endregion
    }
}
