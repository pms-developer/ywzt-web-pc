using System;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.omniorder.store.reallocate
    /// </summary>
    public class OmniorderStoreReallocateRequest : BaseTopRequest<Top.Api.Response.OmniorderStoreReallocateResponse>
    {
        /// <summary>
        /// 主订单号
        /// </summary>
        public Nullable<long> MainOrderId { get; set; }

        /// <summary>
        /// 门店Id
        /// </summary>
        public Nullable<long> StoreId { get; set; }

        /// <summary>
        /// 子订单号
        /// </summary>
        public string SubOrderIds { get; set; }

        /// <summary>
        /// 电商仓code
        /// </summary>
        public string WarehouseCode { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.omniorder.store.reallocate";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("main_order_id", this.MainOrderId);
            parameters.Add("store_id", this.StoreId);
            parameters.Add("sub_order_ids", this.SubOrderIds);
            parameters.Add("warehouse_code", this.WarehouseCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("main_order_id", this.MainOrderId);
            RequestValidator.ValidateRequired("sub_order_ids", this.SubOrderIds);
            RequestValidator.ValidateMaxListSize("sub_order_ids", this.SubOrderIds, 100);
        }

        #endregion
    }
}
