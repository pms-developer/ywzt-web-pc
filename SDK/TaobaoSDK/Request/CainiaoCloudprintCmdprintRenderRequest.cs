using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: cainiao.cloudprint.cmdprint.render
    /// </summary>
    public class CainiaoCloudprintCmdprintRenderRequest : BaseTopRequest<Top.Api.Response.CainiaoCloudprintCmdprintRenderResponse>
    {
        /// <summary>
        /// 参数对象
        /// </summary>
        public string Params { get; set; }

        public CmdRenderParamsDomain Params_ { set { this.Params = TopUtils.ObjectToJson(value); } } 

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "cainiao.cloudprint.cmdprint.render";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("params", this.Params);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("params", this.Params);
        }

	/// <summary>
/// RenderContentDomain Data Structure.
/// </summary>
[Serializable]

public class RenderContentDomain : TopObject
{
	        /// <summary>
	        /// 附加数据(用于修改数据)
	        /// </summary>
	        [XmlElement("add_data")]
	        public string AddData { get; set; }
	
	        /// <summary>
	        /// 是否获取加密数据
	        /// </summary>
	        [XmlElement("encrypted")]
	        public Nullable<bool> Encrypted { get; set; }
	
	        /// <summary>
	        /// 打印数据
	        /// </summary>
	        [XmlElement("print_data")]
	        public string PrintData { get; set; }
	
	        /// <summary>
	        /// 数据签名
	        /// </summary>
	        [XmlElement("signature")]
	        public string Signature { get; set; }
	
	        /// <summary>
	        /// 模板url
	        /// </summary>
	        [XmlElement("template_url")]
	        public string TemplateUrl { get; set; }
	
	        /// <summary>
	        /// 加密数据使用秘钥版本
	        /// </summary>
	        [XmlElement("ver")]
	        public string Ver { get; set; }
}

	/// <summary>
/// RenderDocumentDomain Data Structure.
/// </summary>
[Serializable]

public class RenderDocumentDomain : TopObject
{
	        /// <summary>
	        /// 包含的区域列表。对于有自定义区的文档，content会包含两条，即第一条是标准模板区域内容、第二条是自定义区域内容
	        /// </summary>
	        [XmlArray("contents")]
	        [XmlArrayItem("render_content")]
	        public List<RenderContentDomain> Contents { get; set; }
}

	/// <summary>
/// RenderConfigDomain Data Structure.
/// </summary>
[Serializable]

public class RenderConfigDomain : TopObject
{
	        /// <summary>
	        /// 下联logo
	        /// </summary>
	        [XmlElement("need_bottom_logo")]
	        public Nullable<bool> NeedBottomLogo { get; set; }
	
	        /// <summary>
	        /// 中间logo
	        /// </summary>
	        [XmlElement("need_middle_logo")]
	        public Nullable<bool> NeedMiddleLogo { get; set; }
	
	        /// <summary>
	        /// 上联logo
	        /// </summary>
	        [XmlElement("need_top_logo")]
	        public Nullable<bool> NeedTopLogo { get; set; }
	
	        /// <summary>
	        /// 打印方向：normal-正常 reverse-翻转(旋转180°)
	        /// </summary>
	        [XmlElement("orientation")]
	        public string Orientation { get; set; }
}

	/// <summary>
/// CmdRenderParamsDomain Data Structure.
/// </summary>
[Serializable]

public class CmdRenderParamsDomain : TopObject
{
	        /// <summary>
	        /// 客户端ID
	        /// </summary>
	        [XmlElement("client_id")]
	        public string ClientId { get; set; }
	
	        /// <summary>
	        /// 客户端类型：weixin/alipay/native
	        /// </summary>
	        [XmlElement("client_type")]
	        public string ClientType { get; set; }
	
	        /// <summary>
	        /// 打印配置
	        /// </summary>
	        [XmlElement("config")]
	        public RenderConfigDomain Config { get; set; }
	
	        /// <summary>
	        /// 需要打印的文档，包括模板地址、打印数据
	        /// </summary>
	        [XmlElement("document")]
	        public RenderDocumentDomain Document { get; set; }
	
	        /// <summary>
	        /// 打印机名称
	        /// </summary>
	        [XmlElement("printer_name")]
	        public string PrinterName { get; set; }
}

        #endregion
    }
}
