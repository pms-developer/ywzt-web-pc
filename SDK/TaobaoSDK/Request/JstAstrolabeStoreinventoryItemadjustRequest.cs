using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api.Util;

namespace Top.Api.Request
{
    /// <summary>
    /// TOP API: taobao.jst.astrolabe.storeinventory.itemadjust
    /// </summary>
    public class JstAstrolabeStoreinventoryItemadjustRequest : BaseTopRequest<Top.Api.Response.JstAstrolabeStoreinventoryItemadjustResponse>
    {
        /// <summary>
        /// 库存调整信息
        /// </summary>
        public string InventoryAdjustInfo { get; set; }

        public InventoryAdjustInfoDomain InventoryAdjustInfo_ { set { this.InventoryAdjustInfo = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 操作时间
        /// </summary>
        public string OperationTime { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.jst.astrolabe.storeinventory.itemadjust";
        }

        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("inventory_adjust_info", this.InventoryAdjustInfo);
            parameters.Add("operation_time", this.OperationTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("operation_time", this.OperationTime);
        }

	/// <summary>
/// InventoryAdjustInfoDomain Data Structure.
/// </summary>
[Serializable]

public class InventoryAdjustInfoDomain : TopObject
{
	        /// <summary>
	        /// 调整类型
	        /// </summary>
	        [XmlElement("adjust_type")]
	        public string AdjustType { get; set; }
	
	        /// <summary>
	        /// 流水号
	        /// </summary>
	        [XmlElement("bill_num")]
	        public string BillNum { get; set; }
	
	        /// <summary>
	        /// 库存类型
	        /// </summary>
	        [XmlElement("inventory_type")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 淘宝前端商品id
	        /// </summary>
	        [XmlElement("item_id")]
	        public Nullable<long> ItemId { get; set; }
	
	        /// <summary>
	        /// 需要调整的原始门店ID(商户中心) 或 电商仓ID
	        /// </summary>
	        [XmlElement("original_warehouse_id")]
	        public string OriginalWarehouseId { get; set; }
	
	        /// <summary>
	        /// ISV系统中商品编码
	        /// </summary>
	        [XmlElement("outer_id")]
	        public string OuterId { get; set; }
	
	        /// <summary>
	        /// 需调整的库存数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public Nullable<long> Quantity { get; set; }
	
	        /// <summary>
	        /// 商品的SKU编码
	        /// </summary>
	        [XmlElement("sku_id")]
	        public Nullable<long> SkuId { get; set; }
	
	        /// <summary>
	        /// 需要调整到的目标门店ID(商户中心) 或电商仓ID
	        /// </summary>
	        [XmlElement("target_warehouse_id")]
	        public string TargetWarehouseId { get; set; }
	
	        /// <summary>
	        /// 淘宝子订单号
	        /// </summary>
	        [XmlElement("tb_sub_trade_order")]
	        public string TbSubTradeOrder { get; set; }
	
	        /// <summary>
	        /// 淘宝订单号
	        /// </summary>
	        [XmlElement("tb_trade_order")]
	        public string TbTradeOrder { get; set; }
}

        #endregion
    }
}
