using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.api.goods.stock.change.query
    /// </summary>
    public class WdtApiGoodsStockChangeQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtApiGoodsStockChangeQueryResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 最多返回条数
        /// </summary>
        public Nullable<long> Limit { get; set; }

        /// <summary>
        /// 店铺编号
        /// </summary>
        public string ShopNo { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.api.goods.stock.change.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("limit", this.Limit);
            parameters.Add("shop_no", this.ShopNo);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("limit", this.Limit);
            RequestValidator.ValidateRequired("shop_no", this.ShopNo);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

        #endregion
    }
}
