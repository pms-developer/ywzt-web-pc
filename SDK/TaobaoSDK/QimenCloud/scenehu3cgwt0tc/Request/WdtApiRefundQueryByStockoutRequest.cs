using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.api.refund.query.by.stockout
    /// </summary>
    public class WdtApiRefundQueryByStockoutRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtApiRefundQueryByStockoutResponse>
    {
        /// <summary>
        /// appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 出库单号
        /// </summary>
        public string StockoutNo { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.api.refund.query.by.stockout";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("sid", this.Sid);
            parameters.Add("stockout_no", this.StockoutNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("stockout_no", this.StockoutNo);
        }

        #endregion
    }
}
