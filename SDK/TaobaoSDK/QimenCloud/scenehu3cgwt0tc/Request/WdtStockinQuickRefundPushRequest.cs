using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.stockin.quick.refund.push
    /// </summary>
    public class WdtStockinQuickRefundPushRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtStockinQuickRefundPushResponse>
    {
        /// <summary>
        /// 退货单信息
        /// </summary>
        public string RefundList { get; set; }

        public ArrayDomain RefundList_ { set { this.RefundList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.stockin.quick.refund.push";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("refund_list", this.RefundList);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("refund_list", this.RefundList);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 原始子订单
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
	
	        /// <summary>
	        /// 1
	        /// </summary>
	        [XmlElement("order_num")]
	        public string OrderNum { get; set; }
	
	        /// <summary>
	        /// 1
	        /// </summary>
	        [XmlElement("platform_id")]
	        public string PlatformId { get; set; }
	
	        /// <summary>
	        /// 1
	        /// </summary>
	        [XmlElement("refund_num")]
	        public string RefundNum { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 1
	        /// </summary>
	        [XmlElement("stockin_price")]
	        public string StockinPrice { get; set; }
}

        #endregion
    }
}
