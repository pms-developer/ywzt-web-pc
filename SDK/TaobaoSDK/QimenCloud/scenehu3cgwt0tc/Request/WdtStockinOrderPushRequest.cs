using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.stockin.order.push
    /// </summary>
    public class WdtStockinOrderPushRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtStockinOrderPushResponse>
    {
        /// <summary>
        /// appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 入库单列表
        /// </summary>
        public string StockinInfo { get; set; }

        public ArrayDomain StockinInfo_ { set { this.StockinInfo = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.stockin.order.push";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("sid", this.Sid);
            parameters.Add("stockin_info", this.StockinInfo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("stockin_info", this.StockinInfo);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 批次号
	        /// </summary>
	        [XmlElement("batch_no")]
	        public string BatchNo { get; set; }
	
	        /// <summary>
	        /// 货位编号
	        /// </summary>
	        [XmlElement("position_no")]
	        public string PositionNo { get; set; }
	
	        /// <summary>
	        /// 生产日期
	        /// </summary>
	        [XmlElement("production_date")]
	        public string ProductionDate { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 成本价
	        /// </summary>
	        [XmlElement("src_price")]
	        public string SrcPrice { get; set; }
	
	        /// <summary>
	        /// 入库数量
	        /// </summary>
	        [XmlElement("stockin_num")]
	        public string StockinNum { get; set; }
	
	        /// <summary>
	        /// 入库价
	        /// </summary>
	        [XmlElement("stockin_price")]
	        public string StockinPrice { get; set; }
	
	        /// <summary>
	        /// 税率
	        /// </summary>
	        [XmlElement("tax")]
	        public string Tax { get; set; }
	
	        /// <summary>
	        /// 有效期天数
	        /// </summary>
	        [XmlElement("validity_days")]
	        public string ValidityDays { get; set; }
}

        #endregion
    }
}
