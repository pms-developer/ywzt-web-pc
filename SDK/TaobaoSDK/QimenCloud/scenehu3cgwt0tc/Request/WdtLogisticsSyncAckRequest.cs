using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.logistics.sync.ack
    /// </summary>
    public class WdtLogisticsSyncAckRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtLogisticsSyncAckResponse>
    {
        /// <summary>
        /// appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 物流同步状态回传列表
        /// </summary>
        public string LogisticsList { get; set; }

        public List<ArrayDomain> LogisticsList_ { set { this.LogisticsList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.logistics.sync.ack";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("logistics_list", this.LogisticsList);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("logistics_list", this.LogisticsList);
            RequestValidator.ValidateObjectMaxListSize("logistics_list", this.LogisticsList, 999999);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 相关描述信息,可在erp的物流同步界面看到
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 回写的记录id
	        /// </summary>
	        [XmlElement("rec_id")]
	        public Nullable<long> RecId { get; set; }
	
	        /// <summary>
	        /// 回写状态: 0成功 1失败
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
}

        #endregion
    }
}
