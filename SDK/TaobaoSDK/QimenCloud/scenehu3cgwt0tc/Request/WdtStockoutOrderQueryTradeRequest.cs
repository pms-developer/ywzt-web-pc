using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.stockout.order.query.trade
    /// </summary>
    public class WdtStockoutOrderQueryTradeRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtStockoutOrderQueryTradeResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 结束日期
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 页号,默认0，从0页开始。
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 分页大小（最大不超过30条，默认返回30条）
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 开始日期
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// 状态，55已审核,5,已取消 (此参数查询时间为最后更新时间)，不传此参数默认按出库时间查询，已发货已完成单据。
        /// </summary>
        public Nullable<long> Status { get; set; }

        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseNo { get; set; }

        public string SrcOrderNo { get; set; }

        public string SrcTid { get; set; }

        public string StockoutNo { get; set; }

        public string ShopNo { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.stockout.order.query.trade";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("end_time", this.EndTime);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("sid", this.Sid);
            parameters.Add("start_time", this.StartTime);
            parameters.Add("status", this.Status);
            parameters.Add("warehouse_no", this.WarehouseNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            //RequestValidator.ValidateRequired("end_time", this.EndTime);
            RequestValidator.ValidateRequired("sid", this.Sid);
            //RequestValidator.ValidateRequired("start_time", this.StartTime);
        }

        #endregion
    }
}
