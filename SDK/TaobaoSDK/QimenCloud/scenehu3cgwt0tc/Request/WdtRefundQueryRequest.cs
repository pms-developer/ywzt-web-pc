using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.refund.query
    /// </summary>
    public class WdtRefundQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtRefundQueryResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 分页大小
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 退换单处理状态 5 补款 10已取消 20待审核 30已同意 40已拒绝 50待财审 60待收货 70部分到货 80待结算 90已完成
        /// </summary>
        public Nullable<long> ProcessStatus { get; set; }

        /// <summary>
        /// Erp内退换单编号
        /// </summary>
        public string RefundNo { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 平台原始退换单号
        /// </summary>
        public string SrcRefundNo { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// 请求时间类型
        /// </summary>
        public Nullable<long> TimeType { get; set; }

        public string TradeNo { get; set; }

        public string Tid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.refund.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("end_time", this.EndTime);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("process_status", this.ProcessStatus);
            parameters.Add("refund_no", this.RefundNo);
            parameters.Add("sid", this.Sid);
            parameters.Add("src_refund_no", this.SrcRefundNo);
            parameters.Add("start_time", this.StartTime);
            parameters.Add("time_type", this.TimeType);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            //RequestValidator.ValidateRequired("end_time", this.EndTime);
            //RequestValidator.ValidateRequired("page_no", this.PageNo);
            //RequestValidator.ValidateRequired("page_size", this.PageSize);
            RequestValidator.ValidateRequired("sid", this.Sid);
            //RequestValidator.ValidateRequired("start_time", this.StartTime);
        }

        #endregion
    }
}
