using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.purchase.provider.create
    /// </summary>
    public class WdtPurchaseProviderCreateRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtPurchaseProviderCreateResponse>
    {
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 到货周期
        /// </summary>
        public Nullable<long> ArriveCycleDays { get; set; }

        /// <summary>
        /// 结算周期
        /// </summary>
        public Nullable<long> ChargeCycleDays { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contact { get; set; }

        /// <summary>
        /// email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 传真
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        public Nullable<long> IsDisabled { get; set; }

        /// <summary>
        /// 最后采购日期
        /// </summary>
        public string LastPurchaseTime { get; set; }

        /// <summary>
        /// 最小采购数量
        /// </summary>
        public string MinPurchaseNum { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string ProviderNo { get; set; }

        /// <summary>
        /// 采购周期
        /// </summary>
        public Nullable<long> PurchaseCycleDays { get; set; }

        /// <summary>
        /// QQ
        /// </summary>
        public string Qq { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 座机
        /// </summary>
        public string Telno { get; set; }

        /// <summary>
        /// 旺旺
        /// </summary>
        public string Wangwang { get; set; }

        /// <summary>
        /// 网址
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// 邮编
        /// </summary>
        public string Zip { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.purchase.provider.create";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("address", this.Address);
            parameters.Add("appkey", this.Appkey);
            parameters.Add("arrive_cycle_days", this.ArriveCycleDays);
            parameters.Add("charge_cycle_days", this.ChargeCycleDays);
            parameters.Add("contact", this.Contact);
            parameters.Add("email", this.Email);
            parameters.Add("fax", this.Fax);
            parameters.Add("is_disabled", this.IsDisabled);
            parameters.Add("last_purchase_time", this.LastPurchaseTime);
            parameters.Add("min_purchase_num", this.MinPurchaseNum);
            parameters.Add("mobile", this.Mobile);
            parameters.Add("provider_name", this.ProviderName);
            parameters.Add("provider_no", this.ProviderNo);
            parameters.Add("purchase_cycle_days", this.PurchaseCycleDays);
            parameters.Add("qq", this.Qq);
            parameters.Add("remark", this.Remark);
            parameters.Add("sid", this.Sid);
            parameters.Add("telno", this.Telno);
            parameters.Add("wangwang", this.Wangwang);
            parameters.Add("website", this.Website);
            parameters.Add("zip", this.Zip);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("arrive_cycle_days", this.ArriveCycleDays);
            RequestValidator.ValidateRequired("charge_cycle_days", this.ChargeCycleDays);
            RequestValidator.ValidateRequired("min_purchase_num", this.MinPurchaseNum);
            RequestValidator.ValidateRequired("provider_name", this.ProviderName);
            RequestValidator.ValidateRequired("provider_no", this.ProviderNo);
            RequestValidator.ValidateRequired("purchase_cycle_days", this.PurchaseCycleDays);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

        #endregion
    }
}
