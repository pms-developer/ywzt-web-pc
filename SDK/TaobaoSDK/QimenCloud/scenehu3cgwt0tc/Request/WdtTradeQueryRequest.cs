using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.trade.query
    /// </summary>
    public class WdtTradeQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtTradeQueryResponse>
    {
        /// <summary>
        /// 卖家appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 使用税率
        /// </summary>
        public Nullable<long> Goodstax { get; set; }

        /// <summary>
        /// 物流单号限制
        /// </summary>
        public Nullable<long> HasLogisticsNo { get; set; }

        /// <summary>
        /// 货品信息是否返回图片
        /// </summary>
        public Nullable<long> ImgUrl { get; set; }

        /// <summary>
        /// 物流单号
        /// </summary>
        public string LogisticsNo { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int PageNo { get; set; }

        /// <summary>
        /// 分页大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 店铺编号
        /// </summary>
        public string ShopNo { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 是否返回交易流水号
        /// </summary>
        public string Src { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Nullable<long> Status { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string TradeNo { get; set; }

        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseNo { get; set; }

        public string SrcTid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.trade.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("end_time", this.EndTime);
            parameters.Add("goodstax", this.Goodstax);
            parameters.Add("has_logistics_no", this.HasLogisticsNo);
            parameters.Add("img_url", this.ImgUrl);
            parameters.Add("logistics_no", this.LogisticsNo);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("shop_no", this.ShopNo);
            parameters.Add("sid", this.Sid);
            parameters.Add("src", this.Src);
            parameters.Add("start_time", this.StartTime);
            parameters.Add("status", this.Status);
            parameters.Add("trade_no", this.TradeNo);
            parameters.Add("src_tid", this.SrcTid);
            parameters.Add("warehouse_no", this.WarehouseNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            //RequestValidator.ValidateRequired("end_time", this.EndTime);
            //RequestValidator.ValidateRequired("page_no", this.PageNo);
            //RequestValidator.ValidateRequired("page_size", this.PageSize);
            RequestValidator.ValidateRequired("sid", this.Sid);
            //RequestValidator.ValidateRequired("start_time", this.StartTime);
            //RequestValidator.ValidateRequired("status", this.Status);
        }

        #endregion
    }
}
