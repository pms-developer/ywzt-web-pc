using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.api.goodsspec.push
    /// </summary>
    public class WdtApiGoodsspecPushRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtApiGoodsspecPushResponse>
    {
        /// <summary>
        /// 平台货品信息
        /// </summary>
        public string ApiGoodsInfo { get; set; }

        public ArrayDomain ApiGoodsInfo_ { set { this.ApiGoodsInfo = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.api.goodsspec.push";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("api_goods_info", this.ApiGoodsInfo);
            parameters.Add("appkey", this.Appkey);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("api_goods_info", this.ApiGoodsInfo);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 平台类目
	        /// </summary>
	        [XmlElement("cid")]
	        public string Cid { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 图片url
	        /// </summary>
	        [XmlElement("pic_url")]
	        public string PicUrl { get; set; }
	
	        /// <summary>
	        /// 价格
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 平台规格码
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 规格编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
	
	        /// <summary>
	        /// 平台库存
	        /// </summary>
	        [XmlElement("stock_num")]
	        public string StockNum { get; set; }
}

        #endregion
    }
}
