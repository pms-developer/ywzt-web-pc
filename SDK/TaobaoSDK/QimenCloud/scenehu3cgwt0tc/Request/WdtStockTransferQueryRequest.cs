using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.stock.transfer.query
    /// </summary>
    public class WdtStockTransferQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtStockTransferQueryResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 最后修改时间
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 源仓库
        /// </summary>
        public string FromWarehouseNo { get; set; }

        /// <summary>
        /// 页码，从0页开始，默认值0。page_no=0时，返回total_count
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 每页返回条数，默认30条，不超过30
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 最后修改时间
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// 调拨单状态10已取消20编辑中30待审核40已审核50部分出库60全部出库70部分入库80待结算90调拨完成
        /// </summary>
        public Nullable<long> Status { get; set; }

        /// <summary>
        /// 目标仓库
        /// </summary>
        public string ToWarehouseNo { get; set; }

        /// <summary>
        /// 调拨单编号
        /// </summary>
        public string TransferNo { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.stock.transfer.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("end_time", this.EndTime);
            parameters.Add("from_warehouse_no", this.FromWarehouseNo);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("sid", this.Sid);
            parameters.Add("start_time", this.StartTime);
            parameters.Add("status", this.Status);
            parameters.Add("to_warehouse_no", this.ToWarehouseNo);
            parameters.Add("transfer_no", this.TransferNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("end_time", this.EndTime);
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("start_time", this.StartTime);
        }

        #endregion
    }
}
