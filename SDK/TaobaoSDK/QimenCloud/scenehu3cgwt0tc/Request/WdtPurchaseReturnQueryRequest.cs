using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.purchase.return.query
    /// </summary>
    public class WdtPurchaseReturnQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtPurchaseReturnQueryResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 结束日期
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 页号，从0页开始
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 分页大小（最大不超过30条）
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 开始日期
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// 采购退货单状态：10,已取消,20,编辑中,30,待审核,40,已审核,42,待推送,44,推送失败,46,委外待出库,50,部分出库,60,已完成
        /// </summary>
        public Nullable<long> Status { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.purchase.return.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("end_time", this.EndTime);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("sid", this.Sid);
            parameters.Add("start_time", this.StartTime);
            parameters.Add("status", this.Status);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("end_time", this.EndTime);
            RequestValidator.ValidateRequired("page_no", this.PageNo);
            RequestValidator.ValidateRequired("page_size", this.PageSize);
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("start_time", this.StartTime);
        }

        #endregion
    }
}
