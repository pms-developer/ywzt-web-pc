using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.goods.push
    /// </summary>
    public class WdtGoodsPushRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtGoodsPushResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 货品信息
        /// </summary>
        public string GoodsList { get; set; }

        public List<ArrayDomain> GoodsList_ { set { this.GoodsList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.goods.push";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("goods_list", this.GoodsList);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("goods_list", this.GoodsList);
            RequestValidator.ValidateObjectMaxListSize("goods_list", this.GoodsList, 999999);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 主条码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 高
	        /// </summary>
	        [XmlElement("height")]
	        public string Height { get; set; }
	
	        /// <summary>
	        /// 图片url地址
	        /// </summary>
	        [XmlElement("img_url")]
	        public string ImgUrl { get; set; }
	
	        /// <summary>
	        /// 允许负库存
	        /// </summary>
	        [XmlElement("is_allow_neg_stock")]
	        public Nullable<long> IsAllowNegStock { get; set; }
	
	        /// <summary>
	        /// 长
	        /// </summary>
	        [XmlElement("length")]
	        public string Length { get; set; }
	
	        /// <summary>
	        /// 最低价
	        /// </summary>
	        [XmlElement("lowest_price")]
	        public string LowestPrice { get; set; }
	
	        /// <summary>
	        /// 市场价
	        /// </summary>
	        [XmlElement("market_price")]
	        public string MarketPrice { get; set; }
	
	        /// <summary>
	        /// 会员价
	        /// </summary>
	        [XmlElement("member_price")]
	        public string MemberPrice { get; set; }
	
	        /// <summary>
	        /// 打包积分
	        /// </summary>
	        [XmlElement("pack_score")]
	        public string PackScore { get; set; }
	
	        /// <summary>
	        /// 拣货积分
	        /// </summary>
	        [XmlElement("pick_score")]
	        public string PickScore { get; set; }
	
	        /// <summary>
	        /// 自定义1
	        /// </summary>
	        [XmlElement("prop1")]
	        public string Prop1 { get; set; }
	
	        /// <summary>
	        /// 自定义2
	        /// </summary>
	        [XmlElement("prop2")]
	        public string Prop2 { get; set; }
	
	        /// <summary>
	        /// 自定义3
	        /// </summary>
	        [XmlElement("prop3")]
	        public string Prop3 { get; set; }
	
	        /// <summary>
	        /// 自定义4
	        /// </summary>
	        [XmlElement("prop4")]
	        public string Prop4 { get; set; }
	
	        /// <summary>
	        /// 自定义5
	        /// </summary>
	        [XmlElement("prop5")]
	        public string Prop5 { get; set; }
	
	        /// <summary>
	        /// 自定义6
	        /// </summary>
	        [XmlElement("prop6")]
	        public string Prop6 { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 零售价
	        /// </summary>
	        [XmlElement("retail_price")]
	        public string RetailPrice { get; set; }
	
	        /// <summary>
	        /// 销售积分
	        /// </summary>
	        [XmlElement("sale_score")]
	        public string SaleScore { get; set; }
	
	        /// <summary>
	        /// 规格码
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 基本单位
	        /// </summary>
	        [XmlElement("spec_unit_name")]
	        public string SpecUnitName { get; set; }
	
	        /// <summary>
	        /// 税率
	        /// </summary>
	        [XmlElement("tax_rate")]
	        public string TaxRate { get; set; }
	
	        /// <summary>
	        /// 有效期天数
	        /// </summary>
	        [XmlElement("validity_days")]
	        public string ValidityDays { get; set; }
	
	        /// <summary>
	        /// 重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
	
	        /// <summary>
	        /// 批发价
	        /// </summary>
	        [XmlElement("wholesale_price")]
	        public string WholesalePrice { get; set; }
	
	        /// <summary>
	        /// 宽
	        /// </summary>
	        [XmlElement("width")]
	        public string Width { get; set; }
}

        #endregion
    }
}
