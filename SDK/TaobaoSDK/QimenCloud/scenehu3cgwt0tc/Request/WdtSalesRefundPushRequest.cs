using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.sales.refund.push
    /// </summary>
    public class WdtSalesRefundPushRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtSalesRefundPushResponse>
    {
        /// <summary>
        /// 退款单信息
        /// </summary>
        public string ApiRefundList { get; set; }

        public List<ArrayDomain> ApiRefundList_ { set { this.ApiRefundList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.sales.refund.push";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("api_refund_list", this.ApiRefundList);
            parameters.Add("appkey", this.Appkey);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("api_refund_list", this.ApiRefundList);
            RequestValidator.ValidateObjectMaxListSize("api_refund_list", this.ApiRefundList, 999999);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 售后数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// 平台订单子订单编号
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
}

        #endregion
    }
}
