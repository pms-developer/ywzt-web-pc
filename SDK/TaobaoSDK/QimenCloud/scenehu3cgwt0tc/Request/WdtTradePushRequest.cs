using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.trade.push
    /// </summary>
    public class WdtTradePushRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtTradePushResponse>
    {
        /// <summary>
        /// appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 店铺编号
        /// </summary>
        public string ShopNo { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 订单数据
        /// </summary>
        public string TradeList { get; set; }

        public List<ArrayDomain> TradeList_ { set { this.TradeList = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.trade.push";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("shop_no", this.ShopNo);
            parameters.Add("sid", this.Sid);
            parameters.Add("trade_list", this.TradeList);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("appkey", this.Appkey);
            RequestValidator.ValidateRequired("shop_no", this.ShopNo);
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("trade_list", this.TradeList);
            RequestValidator.ValidateObjectMaxListSize("trade_list", this.TradeList, 999999);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 调整
	        /// </summary>
	        [XmlElement("adjust_amount")]
	        public string AdjustAmount { get; set; }
	
	        /// <summary>
	        /// 优惠
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 平台货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// 子订单编号
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 退款状态
	        /// </summary>
	        [XmlElement("refund_status")]
	        public Nullable<long> RefundStatus { get; set; }
	
	        /// <summary>
	        /// 分摊优惠
	        /// </summary>
	        [XmlElement("share_discount")]
	        public string ShareDiscount { get; set; }
	
	        /// <summary>
	        /// 平台规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 规格编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("status")]
	        public Nullable<long> Status { get; set; }
}

        #endregion
    }
}
