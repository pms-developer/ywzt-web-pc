using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.api.refund.query.by.logistics
    /// </summary>
    public class WdtApiRefundQueryByLogisticsRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtApiRefundQueryByLogisticsResponse>
    {
        /// <summary>
        /// 物流编号
        /// </summary>
        public string LogisticsNo { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.api.refund.query.by.logistics";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("logistics_no", this.LogisticsNo);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("logistics_no", this.LogisticsNo);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

        #endregion
    }
}
