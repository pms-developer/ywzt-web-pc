using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.stock.sync.by.pd
    /// </summary>
    public class WdtStockSyncByPdRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtStockSyncByPdResponse>
    {
        /// <summary>
        /// appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 商品列表
        /// </summary>
        public string GoodsList { get; set; }

        public List<ArrayDomain> GoodsList_ { set { this.GoodsList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 是否调整库存
        /// </summary>
        public Nullable<long> IsAdjustStock { get; set; }

        /// <summary>
        /// 是否添加库存记录
        /// </summary>
        public Nullable<long> IsCreateStock { get; set; }

        /// <summary>
        /// 是否允许部分成功
        /// </summary>
        public Nullable<long> IsPostError { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseNo { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.stock.sync.by.pd";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("goods_list", this.GoodsList);
            parameters.Add("is_adjust_stock", this.IsAdjustStock);
            parameters.Add("is_create_stock", this.IsCreateStock);
            parameters.Add("is_post_error", this.IsPostError);
            parameters.Add("sid", this.Sid);
            parameters.Add("warehouse_no", this.WarehouseNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("goods_list", this.GoodsList);
            RequestValidator.ValidateObjectMaxListSize("goods_list", this.GoodsList, 999999);
            RequestValidator.ValidateRequired("is_adjust_stock", this.IsAdjustStock);
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("warehouse_no", this.WarehouseNo);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 库存数量
	        /// </summary>
	        [XmlElement("stock_num")]
	        public string StockNum { get; set; }
}

        #endregion
    }
}
