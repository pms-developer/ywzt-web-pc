using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.api.goods.stock.change.ack
    /// </summary>
    public class WdtApiGoodsStockChangeAckRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtApiGoodsStockChangeAckResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 待同步库存信息
        /// </summary>
        public string StockSyncList { get; set; }

        public List<ArrayDomain> StockSyncList_ { set { this.StockSyncList = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.api.goods.stock.change.ack";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("sid", this.Sid);
            parameters.Add("stock_sync_list", this.StockSyncList);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("stock_sync_list", this.StockSyncList);
            RequestValidator.ValidateObjectMaxListSize("stock_sync_list", this.StockSyncList, 999999);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// Erp内平台货品表主键id
	        /// </summary>
	        [XmlElement("rec_id")]
	        public Nullable<long> RecId { get; set; }
	
	        /// <summary>
	        /// 库存变化时自增
	        /// </summary>
	        [XmlElement("stock_change_count")]
	        public Nullable<long> StockChangeCount { get; set; }
	
	        /// <summary>
	        /// 货品库存
	        /// </summary>
	        [XmlElement("sync_stock")]
	        public Nullable<long> SyncStock { get; set; }
}

        #endregion
    }
}
