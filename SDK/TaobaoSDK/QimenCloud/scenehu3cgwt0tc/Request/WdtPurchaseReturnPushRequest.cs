using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.purchase.return.push
    /// </summary>
    public class WdtPurchaseReturnPushRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtPurchaseReturnPushResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 采购退货信息
        /// </summary>
        public string ReturnInfo { get; set; }

        public ArrayDomain ReturnInfo_ { set { this.ReturnInfo = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.purchase.return.push";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("return_info", this.ReturnInfo);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("return_info", this.ReturnInfo);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("array")]
public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("detail_remark")]
	        public string DetailRemark { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 退货数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
}

        #endregion
    }
}
