using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.stockout.order.query
    /// </summary>
    public class WdtStockoutOrderQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtStockoutOrderQueryResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 出库单类型（1销售订单2,调拨出库3,采购退货出库4,盘亏出库5, 生产出库6现款销售出库7,其他出库）
        /// </summary>
        public Nullable<long> OrderType { get; set; }

        /// <summary>
        /// 页号,默认0，从0页开始。
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 分页大小（最大不超过30条，默认返回30条）
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 源单号
        /// </summary>
        public string SrcOrderNo { get; set; }

        /// <summary>
        /// 开始日期
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseNo { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.stockout.order.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("end_time", this.EndTime);
            parameters.Add("order_type", this.OrderType);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("sid", this.Sid);
            parameters.Add("src_order_no", this.SrcOrderNo);
            parameters.Add("start_time", this.StartTime);
            parameters.Add("warehouse_no", this.WarehouseNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("end_time", this.EndTime);
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("start_time", this.StartTime);
        }

        #endregion
    }
}
