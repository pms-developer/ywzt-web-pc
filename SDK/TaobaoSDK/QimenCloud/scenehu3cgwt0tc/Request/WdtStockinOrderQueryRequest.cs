using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.stockin.order.query
    /// </summary>
    public class WdtStockinOrderQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtStockinOrderQueryResponse>
    {
        /// <summary>
        /// 秘钥
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 源单据类别 1采购入库, 2调拨入库, 3退货入库, 4盘盈入库, 5生产入库, 6其他入库, 7保修入库, 8纠错入库, 9初始化入库 10 预入库 11 JIT退货入库
        /// </summary>
        public Nullable<long> OrderType { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 分页大小
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// 入库单状态 10已取消30待审核60待结算80已完成（按照状态查询时必须传原单据类别，如果未传status则默认查询80已完成单据）
        /// </summary>
        public Nullable<long> Status { get; set; }

        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseNo { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.stockin.order.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("end_time", this.EndTime);
            parameters.Add("order_type", this.OrderType);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("sid", this.Sid);
            parameters.Add("start_time", this.StartTime);
            parameters.Add("status", this.Status);
            parameters.Add("warehouse_no", this.WarehouseNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("end_time", this.EndTime);
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("start_time", this.StartTime);
        }

        #endregion
    }
}
