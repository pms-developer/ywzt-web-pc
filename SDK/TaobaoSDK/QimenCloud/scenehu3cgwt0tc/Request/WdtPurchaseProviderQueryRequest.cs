using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.purchase.provider.query
    /// </summary>
    public class WdtPurchaseProviderQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtPurchaseProviderQueryResponse>
    {
        /// <summary>
        /// appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 需要返回的字段（"provider_id","provider_no","provider_name","contact","telno","mobile","fax","zip","email","qq","wangwang","address","website","remark","is_disabled","deleted","modified","created"）
        /// </summary>
        public string Column { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 分页大小
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string ProviderNo { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.purchase.provider.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("column", this.Column);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("provider_name", this.ProviderName);
            parameters.Add("provider_no", this.ProviderNo);
            parameters.Add("sid", this.Sid);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("page_no", this.PageNo);
            RequestValidator.ValidateRequired("page_size", this.PageSize);
            RequestValidator.ValidateRequired("sid", this.Sid);
        }

        #endregion
    }
}
