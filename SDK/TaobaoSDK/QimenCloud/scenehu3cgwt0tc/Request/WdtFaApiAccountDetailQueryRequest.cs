using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenehu3cgwt0tc.Request
{
    /// <summary>
    /// TOP API: wdt.fa.api.account.detail.query
    /// </summary>
    public class WdtFaApiAccountDetailQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenehu3cgwt0tc.Response.WdtFaApiAccountDetailQueryResponse>
    {
        /// <summary>
        /// 结束时间
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public string PageNo { get; set; }

        /// <summary>
        /// 页面大小
        /// </summary>
        public string PageSize { get; set; }

        /// <summary>
        /// 店铺编号
        /// </summary>
        public string ShopNo { get; set; }

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public string StartTime { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.fa.api.account.detail.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("end_time", this.EndTime);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("page_size", this.PageSize);
            parameters.Add("shop_no", this.ShopNo);
            parameters.Add("sid", this.Sid);
            parameters.Add("start_time", this.StartTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

        #endregion
    }
}
