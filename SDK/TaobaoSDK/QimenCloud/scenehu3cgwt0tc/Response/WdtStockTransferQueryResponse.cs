using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtStockTransferQueryResponse.
    /// </summary>
    public class WdtStockTransferQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

        /// <summary>
        /// 调拨单信息
        /// </summary>
        [XmlArray("transfer_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> TransferList { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 辅助数量
	        /// </summary>
	        [XmlElement("aux_num")]
	        public string AuxNum { get; set; }
	
	        /// <summary>
	        /// 辅助单位
	        /// </summary>
	        [XmlElement("aux_unit_name")]
	        public string AuxUnitName { get; set; }
	
	        /// <summary>
	        /// 条码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 批次
	        /// </summary>
	        [XmlElement("batch_id")]
	        public string BatchId { get; set; }
	
	        /// <summary>
	        /// 有效期
	        /// </summary>
	        [XmlElement("expire_date")]
	        public string ExpireDate { get; set; }
	
	        /// <summary>
	        /// 源货位
	        /// </summary>
	        [XmlElement("from_position")]
	        public string FromPosition { get; set; }
	
	        /// <summary>
	        /// 入库数量
	        /// </summary>
	        [XmlElement("in_num")]
	        public string InNum { get; set; }
	
	        /// <summary>
	        /// 调拨数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// 出库数量
	        /// </summary>
	        [XmlElement("out_num")]
	        public string OutNum { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 规格码
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 库存
	        /// </summary>
	        [XmlElement("stock_num")]
	        public string StockNum { get; set; }
	
	        /// <summary>
	        /// 目标货位
	        /// </summary>
	        [XmlElement("to_position")]
	        public string ToPosition { get; set; }
	
	        /// <summary>
	        /// 调拨主键id
	        /// </summary>
	        [XmlElement("transfer_id")]
	        public long TransferId { get; set; }
	
	        /// <summary>
	        /// 基本单位
	        /// </summary>
	        [XmlElement("unit_name")]
	        public string UnitName { get; set; }
}

    }
}
