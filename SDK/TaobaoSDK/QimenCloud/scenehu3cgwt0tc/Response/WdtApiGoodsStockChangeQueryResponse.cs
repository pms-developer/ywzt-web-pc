using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtApiGoodsStockChangeQueryResponse.
    /// </summary>
    public class WdtApiGoodsStockChangeQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 库存信息列表
        /// </summary>
        [XmlArray("stock_change_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> StockChangeList { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// Erp内平台货品表主键id
	        /// </summary>
	        [XmlElement("rec_id")]
	        public long RecId { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// 库存变化时自增
	        /// </summary>
	        [XmlElement("stock_change_count")]
	        public string StockChangeCount { get; set; }
	
	        /// <summary>
	        /// Erp内库存
	        /// </summary>
	        [XmlElement("sync_stock")]
	        public long SyncStock { get; set; }
}

    }
}
