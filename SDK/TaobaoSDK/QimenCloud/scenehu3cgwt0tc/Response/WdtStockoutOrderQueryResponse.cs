using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtStockoutOrderQueryResponse.
    /// </summary>
    public class WdtStockoutOrderQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 出库单数据信息
        /// </summary>
        [XmlArray("stockout_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> StockoutList { get; set; }

        /// <summary>
        /// 总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 打印批次
	        /// </summary>
	        [XmlElement("batch_no")]
	        public string BatchNo { get; set; }
	
	        /// <summary>
	        /// 品牌名称
	        /// </summary>
	        [XmlElement("brand_name")]
	        public string BrandName { get; set; }
	
	        /// <summary>
	        /// 品牌编号
	        /// </summary>
	        [XmlElement("brand_no")]
	        public string BrandNo { get; set; }
	
	        /// <summary>
	        /// 成本价
	        /// </summary>
	        [XmlElement("cost_price")]
	        public string CostPrice { get; set; }
	
	        /// <summary>
	        /// 货品数量
	        /// </summary>
	        [XmlElement("goods_count")]
	        public string GoodsCount { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 1销售商品 2原材料 3包装 4周转材料5虚拟商品 0其它
	        /// </summary>
	        [XmlElement("goods_type")]
	        public long GoodsType { get; set; }
	
	        /// <summary>
	        /// 基本货品单位，库存用的单位，个，件
	        /// </summary>
	        [XmlElement("goods_unit")]
	        public string GoodsUnit { get; set; }
	
	        /// <summary>
	        /// erp订单表的id
	        /// </summary>
	        [XmlElement("rec_id")]
	        public long RecId { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 销售价
	        /// </summary>
	        [XmlElement("sell_price")]
	        public string SellPrice { get; set; }
	
	        /// <summary>
	        /// 规格编号
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 出库单主键id
	        /// </summary>
	        [XmlElement("stockout_id")]
	        public long StockoutId { get; set; }
	
	        /// <summary>
	        /// 总销售金额
	        /// </summary>
	        [XmlElement("total_amount")]
	        public string TotalAmount { get; set; }
	
	        /// <summary>
	        /// 重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
}

    }
}
