using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtPurchaseReturnQueryResponse.
    /// </summary>
    public class WdtPurchaseReturnQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 采购退货单数据
        /// </summary>
        [XmlArray("purchase_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> PurchaseList { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 基本单位
	        /// </summary>
	        [XmlElement("base_unit_id")]
	        public long BaseUnitId { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 退货数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// 辅助退货量
	        /// </summary>
	        [XmlElement("num2")]
	        public string Num2 { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 供货商货号
	        /// </summary>
	        [XmlElement("provider_goods_no")]
	        public long ProviderGoodsNo { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 采购退货单id
	        /// </summary>
	        [XmlElement("return_id")]
	        public long ReturnId { get; set; }
	
	        /// <summary>
	        /// 规格码
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 库存单位
	        /// </summary>
	        [XmlElement("unit_id")]
	        public long UnitId { get; set; }
	
	        /// <summary>
	        /// 单位变换率
	        /// </summary>
	        [XmlElement("unit_ratio")]
	        public string UnitRatio { get; set; }
}

    }
}
