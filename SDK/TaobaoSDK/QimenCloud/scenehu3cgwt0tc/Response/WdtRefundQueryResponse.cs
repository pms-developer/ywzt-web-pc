using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtRefundQueryResponse.
    /// </summary>
    public class WdtRefundQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 退换单信息
        /// </summary>
        [XmlArray("refunds")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> Refunds { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 优惠
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 原始子订单id
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
	
	        /// <summary>
	        /// erp子订单主键
	        /// </summary>
	        [XmlElement("order_id")]
	        public long OrderId { get; set; }
	
	        /// <summary>
	        /// 实际数量
	        /// </summary>
	        [XmlElement("order_num")]
	        public string OrderNum { get; set; }
	
	        /// <summary>
	        /// 原价
	        /// </summary>
	        [XmlElement("original_price")]
	        public string OriginalPrice { get; set; }
	
	        /// <summary>
	        /// 已付金额
	        /// </summary>
	        [XmlElement("paid")]
	        public string Paid { get; set; }
	
	        /// <summary>
	        /// 价格
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 处理状态
	        /// </summary>
	        [XmlElement("process_status")]
	        public long ProcessStatus { get; set; }
	
	        /// <summary>
	        /// 退换单主键
	        /// </summary>
	        [XmlElement("refund_id")]
	        public long RefundId { get; set; }
	
	        /// <summary>
	        /// 退款数量
	        /// </summary>
	        [XmlElement("refund_num")]
	        public string RefundNum { get; set; }
	
	        /// <summary>
	        /// 明细实际退款金额
	        /// </summary>
	        [XmlElement("refund_order_amount")]
	        public string RefundOrderAmount { get; set; }
	
	        /// <summary>
	        /// 退款说明
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 销售订单编号
	        /// </summary>
	        [XmlElement("sales_tid")]
	        public string SalesTid { get; set; }
	
	        /// <summary>
	        /// erp商品主键
	        /// </summary>
	        [XmlElement("spec_id")]
	        public long SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 退货入库数量
	        /// </summary>
	        [XmlElement("stockin_num")]
	        public string StockinNum { get; set; }
	
	        /// <summary>
	        /// 组合装名称
	        /// </summary>
	        [XmlElement("suite_name")]
	        public string SuiteName { get; set; }
	
	        /// <summary>
	        /// 组合装编号
	        /// </summary>
	        [XmlElement("suite_no")]
	        public string SuiteNo { get; set; }
	
	        /// <summary>
	        /// 组合装数量
	        /// </summary>
	        [XmlElement("suite_num")]
	        public string SuiteNum { get; set; }
	
	        /// <summary>
	        /// 订单子订单的原始单号
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
	
	        /// <summary>
	        /// 退款总金额
	        /// </summary>
	        [XmlElement("total_amount")]
	        public string TotalAmount { get; set; }
}

    }
}
