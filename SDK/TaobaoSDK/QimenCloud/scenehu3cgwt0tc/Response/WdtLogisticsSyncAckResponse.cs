using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtLogisticsSyncAckResponse.
    /// </summary>
    public class WdtLogisticsSyncAckResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlArray("errors")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> Errors { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 错误信息的描述
	        /// </summary>
	        [XmlElement("error")]
	        public string Error { get; set; }
	
	        /// <summary>
	        /// 回写的记录id
	        /// </summary>
	        [XmlElement("rec_id")]
	        public long RecId { get; set; }
}

    }
}
