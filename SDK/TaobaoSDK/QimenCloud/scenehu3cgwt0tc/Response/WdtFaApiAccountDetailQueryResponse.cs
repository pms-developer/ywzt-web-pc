using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtFaApiAccountDetailQueryResponse.
    /// </summary>
    public class WdtFaApiAccountDetailQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 帐务明细
        /// </summary>
        [XmlArray("account_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> AccountList { get; set; }

        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public string Errorcode { get; set; }

        /// <summary>
        /// 错误描述
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 数据条数
        /// </summary>
        [XmlElement("total_count")]
        public string TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 1
	        /// </summary>
	        [XmlElement("balance")]
	        public string Balance { get; set; }
	
	        /// <summary>
	        /// 记账时间
	        /// </summary>
	        [XmlElement("create_time")]
	        public string CreateTime { get; set; }
	
	        /// <summary>
	        /// 创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 收入金额
	        /// </summary>
	        [XmlElement("in_amount")]
	        public string InAmount { get; set; }
	
	        /// <summary>
	        /// 账务类型
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 账务类型名称
	        /// </summary>
	        [XmlElement("item_name")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 编号
	        /// </summary>
	        [XmlElement("order_no")]
	        public string OrderNo { get; set; }
	
	        /// <summary>
	        /// 支出金额
	        /// </summary>
	        [XmlElement("out_amount")]
	        public string OutAmount { get; set; }
	
	        /// <summary>
	        /// 平台支付单号
	        /// </summary>
	        [XmlElement("pay_order_no")]
	        public string PayOrderNo { get; set; }
	
	        /// <summary>
	        /// 平台id
	        /// </summary>
	        [XmlElement("platform_id")]
	        public string PlatformId { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("post_status")]
	        public string PostStatus { get; set; }
	
	        /// <summary>
	        /// 关联订单号
	        /// </summary>
	        [XmlElement("raw_order_no")]
	        public string RawOrderNo { get; set; }
	
	        /// <summary>
	        /// 账款明细主键id
	        /// </summary>
	        [XmlElement("rec_id")]
	        public string RecId { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 关联订单号
	        /// </summary>
	        [XmlElement("row_order_no")]
	        public string RowOrderNo { get; set; }
	
	        /// <summary>
	        /// 店铺id
	        /// </summary>
	        [XmlElement("shop_id")]
	        public string ShopId { get; set; }
	
	        /// <summary>
	        /// 店铺名称
	        /// </summary>
	        [XmlElement("shop_name")]
	        public string ShopName { get; set; }
	
	        /// <summary>
	        /// 店铺编号
	        /// </summary>
	        [XmlElement("shop_no")]
	        public string ShopNo { get; set; }
	
	        /// <summary>
	        /// 子业务类型
	        /// </summary>
	        [XmlElement("sub_item_id")]
	        public string SubItemId { get; set; }
	
	        /// <summary>
	        /// 生成凭证id
	        /// </summary>
	        [XmlElement("voucher_id")]
	        public string VoucherId { get; set; }
}

    }
}
