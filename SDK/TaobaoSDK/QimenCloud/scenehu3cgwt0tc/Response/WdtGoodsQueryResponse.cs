using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtGoodsQueryResponse.
    /// </summary>
    public class WdtGoodsQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 货品数据详情
        /// </summary>
        [XmlArray("goods_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> GoodsList { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 主条码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 高
	        /// </summary>
	        [XmlElement("height")]
	        public string Height { get; set; }
	
	        /// <summary>
	        /// 图片
	        /// </summary>
	        [XmlElement("img_url")]
	        public string ImgUrl { get; set; }
	
	        /// <summary>
	        /// 允许负库存
	        /// </summary>
	        [XmlElement("is_allow_neg_stock")]
	        public string IsAllowNegStock { get; set; }
	
	        /// <summary>
	        /// 允许低于成本价
	        /// </summary>
	        [XmlElement("is_lower_cost")]
	        public string IsLowerCost { get; set; }
	
	        /// <summary>
	        /// 出库不验货
	        /// </summary>
	        [XmlElement("is_not_need_examine")]
	        public string IsNotNeedExamine { get; set; }
	
	        /// <summary>
	        /// 航空禁运
	        /// </summary>
	        [XmlElement("is_not_use_air")]
	        public string IsNotUseAir { get; set; }
	
	        /// <summary>
	        /// 启用序列号
	        /// </summary>
	        [XmlElement("is_sn_enable")]
	        public string IsSnEnable { get; set; }
	
	        /// <summary>
	        /// 允许0成本
	        /// </summary>
	        [XmlElement("is_zero_cost")]
	        public string IsZeroCost { get; set; }
	
	        /// <summary>
	        /// 大件类别
	        /// </summary>
	        [XmlElement("large_type")]
	        public string LargeType { get; set; }
	
	        /// <summary>
	        /// 长
	        /// </summary>
	        [XmlElement("length")]
	        public string Length { get; set; }
	
	        /// <summary>
	        /// 最低价
	        /// </summary>
	        [XmlElement("lowest_price")]
	        public string LowestPrice { get; set; }
	
	        /// <summary>
	        /// 市场价
	        /// </summary>
	        [XmlElement("market_price")]
	        public string MarketPrice { get; set; }
	
	        /// <summary>
	        /// 会员价
	        /// </summary>
	        [XmlElement("member_price")]
	        public string MemberPrice { get; set; }
	
	        /// <summary>
	        /// 最后修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 打包积分
	        /// </summary>
	        [XmlElement("pack_score")]
	        public long PackScore { get; set; }
	
	        /// <summary>
	        /// 拣货积分
	        /// </summary>
	        [XmlElement("pick_score")]
	        public long PickScore { get; set; }
	
	        /// <summary>
	        /// 自定义1
	        /// </summary>
	        [XmlElement("prop1")]
	        public string Prop1 { get; set; }
	
	        /// <summary>
	        /// 自定义2
	        /// </summary>
	        [XmlElement("prop2")]
	        public string Prop2 { get; set; }
	
	        /// <summary>
	        /// 自定义3
	        /// </summary>
	        [XmlElement("prop3")]
	        public string Prop3 { get; set; }
	
	        /// <summary>
	        /// 自定义4
	        /// </summary>
	        [XmlElement("prop4")]
	        public string Prop4 { get; set; }
	
	        /// <summary>
	        /// 自定义5
	        /// </summary>
	        [XmlElement("prop5")]
	        public string Prop5 { get; set; }
	
	        /// <summary>
	        /// 自定义6
	        /// </summary>
	        [XmlElement("prop6")]
	        public string Prop6 { get; set; }
	
	        /// <summary>
	        /// 最佳收获天数
	        /// </summary>
	        [XmlElement("receive_days")]
	        public long ReceiveDays { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 零售价
	        /// </summary>
	        [XmlElement("retail_price")]
	        public string RetailPrice { get; set; }
	
	        /// <summary>
	        /// 销售积分
	        /// </summary>
	        [XmlElement("sale_score")]
	        public long SaleScore { get; set; }
	
	        /// <summary>
	        /// 最佳销售天数
	        /// </summary>
	        [XmlElement("sales_days")]
	        public long SalesDays { get; set; }
	
	        /// <summary>
	        /// 辅助单位
	        /// </summary>
	        [XmlElement("spec_aux_unit_name")]
	        public string SpecAuxUnitName { get; set; }
	
	        /// <summary>
	        /// 规格码
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 商品最后修改时间
	        /// </summary>
	        [XmlElement("spec_modified")]
	        public string SpecModified { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 基本单位
	        /// </summary>
	        [XmlElement("spec_unit_name")]
	        public string SpecUnitName { get; set; }
	
	        /// <summary>
	        /// 税率
	        /// </summary>
	        [XmlElement("tax_rate")]
	        public string TaxRate { get; set; }
	
	        /// <summary>
	        /// 有效期天数
	        /// </summary>
	        [XmlElement("validity_days")]
	        public long ValidityDays { get; set; }
	
	        /// <summary>
	        /// 重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
	
	        /// <summary>
	        /// 批发价
	        /// </summary>
	        [XmlElement("wholesale_price")]
	        public string WholesalePrice { get; set; }
	
	        /// <summary>
	        /// 宽
	        /// </summary>
	        [XmlElement("width")]
	        public string Width { get; set; }
}

    }
}
