using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtShopQueryResponse.
    /// </summary>
    public class WdtShopQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 店铺信息
        /// </summary>
        [XmlArray("shoplist")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> Shoplist { get; set; }

        /// <summary>
        /// 返回数据条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 平台授权账号ID
	        /// </summary>
	        [XmlElement("account_id")]
	        public string AccountId { get; set; }
	
	        /// <summary>
	        /// 平台授权账号昵称
	        /// </summary>
	        [XmlElement("account_nick")]
	        public string AccountNick { get; set; }
	
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 联系人
	        /// </summary>
	        [XmlElement("contact")]
	        public string Contact { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("district")]
	        public string District { get; set; }
	
	        /// <summary>
	        /// 收款方id
	        /// </summary>
	        [XmlElement("invoice_payee_id")]
	        public string InvoicePayeeId { get; set; }
	
	        /// <summary>
	        /// 服务方id
	        /// </summary>
	        [XmlElement("invoice_provider_id")]
	        public string InvoiceProviderId { get; set; }
	
	        /// <summary>
	        /// 移动电话
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 平台ID
	        /// </summary>
	        [XmlElement("platform_id")]
	        public long PlatformId { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 店铺ID
	        /// </summary>
	        [XmlElement("shop_id")]
	        public long ShopId { get; set; }
	
	        /// <summary>
	        /// 店铺名称
	        /// </summary>
	        [XmlElement("shop_name")]
	        public string ShopName { get; set; }
	
	        /// <summary>
	        /// 店铺编号
	        /// </summary>
	        [XmlElement("shop_no")]
	        public string ShopNo { get; set; }
	
	        /// <summary>
	        /// 仓库名称
	        /// </summary>
	        [XmlElement("sub_platform_id")]
	        public string SubPlatformId { get; set; }
	
	        /// <summary>
	        /// 固定电话
	        /// </summary>
	        [XmlElement("telno")]
	        public string Telno { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("zip")]
	        public string Zip { get; set; }
}

    }
}
