using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtPurchaseProviderQueryResponse.
    /// </summary>
    public class WdtPurchaseProviderQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 供应商信息
        /// </summary>
        [XmlArray("provider_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> ProviderList { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 联系人
	        /// </summary>
	        [XmlElement("contact")]
	        public string Contact { get; set; }
	
	        /// <summary>
	        /// 创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 删除时间
	        /// </summary>
	        [XmlElement("deleted")]
	        public string Deleted { get; set; }
	
	        /// <summary>
	        /// email
	        /// </summary>
	        [XmlElement("email")]
	        public string Email { get; set; }
	
	        /// <summary>
	        /// 传真
	        /// </summary>
	        [XmlElement("fax")]
	        public string Fax { get; set; }
	
	        /// <summary>
	        /// 是否禁用
	        /// </summary>
	        [XmlElement("is_disabled")]
	        public long IsDisabled { get; set; }
	
	        /// <summary>
	        /// 手机
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 最后修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 供应商id
	        /// </summary>
	        [XmlElement("provider_id")]
	        public long ProviderId { get; set; }
	
	        /// <summary>
	        /// 供应商名称
	        /// </summary>
	        [XmlElement("provider_name")]
	        public string ProviderName { get; set; }
	
	        /// <summary>
	        /// 供应商编码
	        /// </summary>
	        [XmlElement("provider_no")]
	        public string ProviderNo { get; set; }
	
	        /// <summary>
	        /// QQ
	        /// </summary>
	        [XmlElement("qq")]
	        public string Qq { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 电话
	        /// </summary>
	        [XmlElement("telno")]
	        public string Telno { get; set; }
	
	        /// <summary>
	        /// 旺旺号
	        /// </summary>
	        [XmlElement("wangwang")]
	        public string Wangwang { get; set; }
	
	        /// <summary>
	        /// 网址
	        /// </summary>
	        [XmlElement("website")]
	        public string Website { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("zip")]
	        public string Zip { get; set; }
}

    }
}
