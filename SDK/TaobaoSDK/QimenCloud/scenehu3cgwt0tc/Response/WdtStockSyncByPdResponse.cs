using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtStockSyncByPdResponse.
    /// </summary>
    public class WdtStockSyncByPdResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 返回空字符串说明商品在当前仓库中都存在库存记录
        /// </summary>
        [XmlElement("warning")]
        public string Warning { get; set; }

    }
}
