using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtTradeQueryResponse.
    /// </summary>
    public class WdtTradeQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

        /// <summary>
        /// 当前页的订单数据
        /// </summary>
        [XmlArray("trades")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> Trades { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 实发数量
	        /// </summary>
	        [XmlElement("actual_num")]
	        public string ActualNum { get; set; }
	
	        /// <summary>
	        /// 手工调整价
	        /// </summary>
	        [XmlElement("adjust")]
	        public string Adjust { get; set; }
	
	        /// <summary>
	        /// 平台货品名称
	        /// </summary>
	        [XmlElement("api_goods_name")]
	        public string ApiGoodsName { get; set; }
	
	        /// <summary>
	        /// 平台规格名称
	        /// </summary>
	        [XmlElement("api_spec_name")]
	        public string ApiSpecName { get; set; }
	
	        /// <summary>
	        /// 基本单位
	        /// </summary>
	        [XmlElement("base_unit_id")]
	        public long BaseUnitId { get; set; }
	
	        /// <summary>
	        /// 关联发货
	        /// </summary>
	        [XmlElement("bind_oid")]
	        public string BindOid { get; set; }
	
	        /// <summary>
	        /// 类目id
	        /// </summary>
	        [XmlElement("cid")]
	        public long Cid { get; set; }
	
	        /// <summary>
	        /// 佣金
	        /// </summary>
	        [XmlElement("commission")]
	        public string Commission { get; set; }
	
	        /// <summary>
	        /// 创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 发货条件
	        /// </summary>
	        [XmlElement("delivery_term")]
	        public long DeliveryTerm { get; set; }
	
	        /// <summary>
	        /// 总折扣金额
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// flag
	        /// </summary>
	        [XmlElement("flag")]
	        public long Flag { get; set; }
	
	        /// <summary>
	        /// 原始单标记
	        /// </summary>
	        [XmlElement("from_mask")]
	        public long FromMask { get; set; }
	
	        /// <summary>
	        /// 是否是赠品 0非赠品 1自动赠送 2手工赠送 3 回购自动送赠品
	        /// </summary>
	        [XmlElement("gift_type")]
	        public long GiftType { get; set; }
	
	        /// <summary>
	        /// 赠品方式
	        /// </summary>
	        [XmlElement("gift_typeint")]
	        public long GiftTypeint { get; set; }
	
	        /// <summary>
	        /// 货品id
	        /// </summary>
	        [XmlElement("goods_id")]
	        public long GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 货品类别
	        /// </summary>
	        [XmlElement("goods_type")]
	        public long GoodsType { get; set; }
	
	        /// <summary>
	        /// 担保方式
	        /// </summary>
	        [XmlElement("guarantee_mode")]
	        public string GuaranteeMode { get; set; }
	
	        /// <summary>
	        /// 货品图片url
	        /// </summary>
	        [XmlElement("img_url")]
	        public string ImgUrl { get; set; }
	
	        /// <summary>
	        /// 发票
	        /// </summary>
	        [XmlElement("invoice_content")]
	        public string InvoiceContent { get; set; }
	
	        /// <summary>
	        /// 发票类别
	        /// </summary>
	        [XmlElement("invoice_type")]
	        public long InvoiceType { get; set; }
	
	        /// <summary>
	        /// 大件类型
	        /// </summary>
	        [XmlElement("large_type")]
	        public long LargeType { get; set; }
	
	        /// <summary>
	        /// 最后修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 下单数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// 成交价
	        /// </summary>
	        [XmlElement("order_price")]
	        public string OrderPrice { get; set; }
	
	        /// <summary>
	        /// 已付
	        /// </summary>
	        [XmlElement("paid")]
	        public string Paid { get; set; }
	
	        /// <summary>
	        /// 交易流水单号
	        /// </summary>
	        [XmlElement("pay_id")]
	        public string PayId { get; set; }
	
	        /// <summary>
	        /// 平台货品id
	        /// </summary>
	        [XmlElement("platform_goods_id")]
	        public string PlatformGoodsId { get; set; }
	
	        /// <summary>
	        /// 平台
	        /// </summary>
	        [XmlElement("platform_id")]
	        public long PlatformId { get; set; }
	
	        /// <summary>
	        /// 平台商品id
	        /// </summary>
	        [XmlElement("platform_spec_id")]
	        public string PlatformSpecId { get; set; }
	
	        /// <summary>
	        /// 标价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 自定义属性2
	        /// </summary>
	        [XmlElement("prop2")]
	        public string Prop2 { get; set; }
	
	        /// <summary>
	        /// erp子订单主键
	        /// </summary>
	        [XmlElement("rec_id")]
	        public long RecId { get; set; }
	
	        /// <summary>
	        /// 售后退款数量
	        /// </summary>
	        [XmlElement("refund_num")]
	        public string RefundNum { get; set; }
	
	        /// <summary>
	        /// 退款状态
	        /// </summary>
	        [XmlElement("refund_status")]
	        public long RefundStatus { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 分摊后总价
	        /// </summary>
	        [XmlElement("share_amount")]
	        public string ShareAmount { get; set; }
	
	        /// <summary>
	        /// 分摊邮费
	        /// </summary>
	        [XmlElement("share_post")]
	        public string SharePost { get; set; }
	
	        /// <summary>
	        /// 分摊后价格
	        /// </summary>
	        [XmlElement("share_price")]
	        public string SharePrice { get; set; }
	
	        /// <summary>
	        /// 规格码
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// erp内商品主键
	        /// </summary>
	        [XmlElement("spec_id")]
	        public long SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 线上包裹拆分数
	        /// </summary>
	        [XmlElement("split_package_num")]
	        public long SplitPackageNum { get; set; }
	
	        /// <summary>
	        /// 子订单编号
	        /// </summary>
	        [XmlElement("src_oid")]
	        public string SrcOid { get; set; }
	
	        /// <summary>
	        /// 原始订单编号
	        /// </summary>
	        [XmlElement("src_tid")]
	        public string SrcTid { get; set; }
	
	        /// <summary>
	        /// 组合装分摊后总价
	        /// </summary>
	        [XmlElement("suite_amount")]
	        public string SuiteAmount { get; set; }
	
	        /// <summary>
	        /// 组合装优惠
	        /// </summary>
	        [XmlElement("suite_discount")]
	        public string SuiteDiscount { get; set; }
	
	        /// <summary>
	        /// 组合装ID
	        /// </summary>
	        [XmlElement("suite_id")]
	        public long SuiteId { get; set; }
	
	        /// <summary>
	        /// 拆自组合装
	        /// </summary>
	        [XmlElement("suite_name")]
	        public string SuiteName { get; set; }
	
	        /// <summary>
	        /// 组合装编码
	        /// </summary>
	        [XmlElement("suite_no")]
	        public string SuiteNo { get; set; }
	
	        /// <summary>
	        /// 组合装数量
	        /// </summary>
	        [XmlElement("suite_num")]
	        public string SuiteNum { get; set; }
	
	        /// <summary>
	        /// 税率
	        /// </summary>
	        [XmlElement("tax_rate")]
	        public string TaxRate { get; set; }
	
	        /// <summary>
	        /// erp订单主键
	        /// </summary>
	        [XmlElement("trade_id")]
	        public long TradeId { get; set; }
	
	        /// <summary>
	        /// 基本单位名称
	        /// </summary>
	        [XmlElement("unit_name")]
	        public string UnitName { get; set; }
	
	        /// <summary>
	        /// 估重
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
}

    }
}
