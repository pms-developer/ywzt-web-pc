using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtStockoutOrderQueryTradeResponse.
    /// </summary>
    public class WdtStockoutOrderQueryTradeResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误描述
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 出库单详情
        /// </summary>
        [XmlArray("stockout_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> StockoutList { get; set; }

        /// <summary>
        /// 数据条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 条码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 打印批次
	        /// </summary>
	        [XmlElement("batch_no")]
	        public string BatchNo { get; set; }
	
	        /// <summary>
	        /// 品牌名称
	        /// </summary>
	        [XmlElement("brand_name")]
	        public string BrandName { get; set; }
	
	        /// <summary>
	        /// 品牌编号
	        /// </summary>
	        [XmlElement("brand_no")]
	        public string BrandNo { get; set; }
	
	        /// <summary>
	        /// 成本价
	        /// </summary>
	        [XmlElement("cost_price")]
	        public string CostPrice { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 1手机订单 2聚划算 4服务子订单 8家装 16二次付款 32开具电子发票 1024库存不足 2048当日达 4096次日达 8192预约时效
	        /// </summary>
	        [XmlElement("from_mask")]
	        public string FromMask { get; set; }
	
	        /// <summary>
	        /// 是否是赠品 0非赠品 1自动赠送 2手工赠送
	        /// </summary>
	        [XmlElement("gift_type")]
	        public long GiftType { get; set; }
	
	        /// <summary>
	        /// 货品档案自定义属性1
	        /// </summary>
	        [XmlElement("good_prop1")]
	        public string GoodProp1 { get; set; }
	
	        /// <summary>
	        /// 货品档案自定义属性2
	        /// </summary>
	        [XmlElement("good_prop2")]
	        public string GoodProp2 { get; set; }
	
	        /// <summary>
	        /// 货品档案自定义属性3
	        /// </summary>
	        [XmlElement("good_prop3")]
	        public string GoodProp3 { get; set; }
	
	        /// <summary>
	        /// 货品档案自定义属性4
	        /// </summary>
	        [XmlElement("good_prop4")]
	        public string GoodProp4 { get; set; }
	
	        /// <summary>
	        /// 货品档案自定义属性5
	        /// </summary>
	        [XmlElement("good_prop5")]
	        public string GoodProp5 { get; set; }
	
	        /// <summary>
	        /// 货品档案自定义属性6
	        /// </summary>
	        [XmlElement("good_prop6")]
	        public string GoodProp6 { get; set; }
	
	        /// <summary>
	        /// 货品数量
	        /// </summary>
	        [XmlElement("goods_count")]
	        public string GoodsCount { get; set; }
	
	        /// <summary>
	        /// 货品id
	        /// </summary>
	        [XmlElement("goods_id")]
	        public long GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 货品类型： 1销售商品 2原材料 3包装 4周转材料5虚拟商品6固定资产 0其它
	        /// </summary>
	        [XmlElement("goods_type")]
	        public long GoodsType { get; set; }
	
	        /// <summary>
	        /// 市场价
	        /// </summary>
	        [XmlElement("market_price")]
	        public string MarketPrice { get; set; }
	
	        /// <summary>
	        /// 已支付金额
	        /// </summary>
	        [XmlElement("paid")]
	        public string Paid { get; set; }
	
	        /// <summary>
	        /// 平台ID
	        /// </summary>
	        [XmlElement("platform_id")]
	        public long PlatformId { get; set; }
	
	        /// <summary>
	        /// 单品列表自定义属性1
	        /// </summary>
	        [XmlElement("prop1")]
	        public string Prop1 { get; set; }
	
	        /// <summary>
	        /// 单品列表自定义属性2
	        /// </summary>
	        [XmlElement("prop2")]
	        public string Prop2 { get; set; }
	
	        /// <summary>
	        /// 单品列表自定义属性3
	        /// </summary>
	        [XmlElement("prop3")]
	        public string Prop3 { get; set; }
	
	        /// <summary>
	        /// 单品列表自定义属性4
	        /// </summary>
	        [XmlElement("prop4")]
	        public string Prop4 { get; set; }
	
	        /// <summary>
	        /// 单品列表自定义属性5
	        /// </summary>
	        [XmlElement("prop5")]
	        public string Prop5 { get; set; }
	
	        /// <summary>
	        /// 单品列表自定义属性6
	        /// </summary>
	        [XmlElement("prop6")]
	        public string Prop6 { get; set; }
	
	        /// <summary>
	        /// erp子订单ID
	        /// </summary>
	        [XmlElement("rec_id")]
	        public long RecId { get; set; }
	
	        /// <summary>
	        /// 退款状态 0无退款 1申请退款 2部分退款 3全部退款
	        /// </summary>
	        [XmlElement("refund_status")]
	        public long RefundStatus { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 订单详情主键
	        /// </summary>
	        [XmlElement("sale_order_id")]
	        public long SaleOrderId { get; set; }
	
	        /// <summary>
	        /// 销售价,最终交易价格,扣除优惠
	        /// </summary>
	        [XmlElement("sell_price")]
	        public string SellPrice { get; set; }
	
	        /// <summary>
	        /// 分摊后合计应收=share_price*num,share_price是根据share_amount反推的,因此share_price可能有精度损失
	        /// </summary>
	        [XmlElement("share_amount")]
	        public string ShareAmount { get; set; }
	
	        /// <summary>
	        /// 邮费分摊
	        /// </summary>
	        [XmlElement("share_post")]
	        public string SharePost { get; set; }
	
	        /// <summary>
	        /// 规格编号
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 商品主键
	        /// </summary>
	        [XmlElement("spec_id")]
	        public long SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 原始子订单号
	        /// </summary>
	        [XmlElement("src_oid")]
	        public string SrcOid { get; set; }
	
	        /// <summary>
	        /// 原始订单号
	        /// </summary>
	        [XmlElement("src_tid")]
	        public string SrcTid { get; set; }
	
	        /// <summary>
	        /// 出库单主键id
	        /// </summary>
	        [XmlElement("stockout_id")]
	        public long StockoutId { get; set; }
	
	        /// <summary>
	        /// 组合装编码
	        /// </summary>
	        [XmlElement("suite_no")]
	        public string SuiteNo { get; set; }
	
	        /// <summary>
	        /// 税率
	        /// </summary>
	        [XmlElement("tax_rate")]
	        public string TaxRate { get; set; }
	
	        /// <summary>
	        /// 总货款,sell_price*goods_count=total_amount
	        /// </summary>
	        [XmlElement("total_amount")]
	        public string TotalAmount { get; set; }
	
	        /// <summary>
	        /// 基本单位名称
	        /// </summary>
	        [XmlElement("unit_name")]
	        public string UnitName { get; set; }
	
	        /// <summary>
	        /// 重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
}

    }
}
