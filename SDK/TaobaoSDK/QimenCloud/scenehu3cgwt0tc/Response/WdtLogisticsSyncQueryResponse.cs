using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtLogisticsSyncQueryResponse.
    /// </summary>
    public class WdtLogisticsSyncQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 待同步物流订单信息列表
        /// </summary>
        [XmlArray("trades")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> Trades { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 发货时间
	        /// </summary>
	        [XmlElement("consign_time")]
	        public string ConsignTime { get; set; }
	
	        /// <summary>
	        /// 联系人
	        /// </summary>
	        [XmlElement("contact")]
	        public string Contact { get; set; }
	
	        /// <summary>
	        /// 发货条件
	        /// </summary>
	        [XmlElement("delivery_term")]
	        public long DeliveryTerm { get; set; }
	
	        /// <summary>
	        /// 是否拆分发货
	        /// </summary>
	        [XmlElement("is_part_sync")]
	        public long IsPartSync { get; set; }
	
	        /// <summary>
	        /// erp物流编号
	        /// </summary>
	        [XmlElement("logistics_code_erp")]
	        public string LogisticsCodeErp { get; set; }
	
	        /// <summary>
	        /// 物流公司名称
	        /// </summary>
	        [XmlElement("logistics_name")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// erp物流公司名称
	        /// </summary>
	        [XmlElement("logistics_name_erp")]
	        public string LogisticsNameErp { get; set; }
	
	        /// <summary>
	        /// 物流编号
	        /// </summary>
	        [XmlElement("logistics_no")]
	        public string LogisticsNo { get; set; }
	
	        /// <summary>
	        /// 物流方式
	        /// </summary>
	        [XmlElement("logistics_type")]
	        public long LogisticsType { get; set; }
	
	        /// <summary>
	        /// 手机
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 原始子订单
	        /// </summary>
	        [XmlElement("oids")]
	        public string Oids { get; set; }
	
	        /// <summary>
	        /// 平台ID
	        /// </summary>
	        [XmlElement("platform_id")]
	        public long PlatformId { get; set; }
	
	        /// <summary>
	        /// 主键
	        /// </summary>
	        [XmlElement("rec_id")]
	        public long RecId { get; set; }
	
	        /// <summary>
	        /// 店铺编号
	        /// </summary>
	        [XmlElement("shop_no")]
	        public string ShopNo { get; set; }
	
	        /// <summary>
	        /// 电话
	        /// </summary>
	        [XmlElement("telon")]
	        public string Telon { get; set; }
	
	        /// <summary>
	        /// 原始订单
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
	
	        /// <summary>
	        /// 订单ID
	        /// </summary>
	        [XmlElement("trade_id")]
	        public long TradeId { get; set; }
}

    }
}
