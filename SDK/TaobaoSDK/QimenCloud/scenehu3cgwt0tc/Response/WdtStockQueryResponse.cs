using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtStockQueryResponse.
    /// </summary>
    public class WdtStockQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 库存信息
        /// </summary>
        [XmlArray("stocks")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> Stocks { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 条形码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 成本价
	        /// </summary>
	        [XmlElement("cost_price")]
	        public string CostPrice { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 商品图片url
	        /// </summary>
	        [XmlElement("img_url")]
	        public string ImgUrl { get; set; }
	
	        /// <summary>
	        /// 锁定库存
	        /// </summary>
	        [XmlElement("lock_num")]
	        public string LockNum { get; set; }
	
	        /// <summary>
	        /// 待审核量
	        /// </summary>
	        [XmlElement("order_num")]
	        public string OrderNum { get; set; }
	
	        /// <summary>
	        /// 采购到货量
	        /// </summary>
	        [XmlElement("purchase_arrive_num")]
	        public string PurchaseArriveNum { get; set; }
	
	        /// <summary>
	        /// 采购在途量
	        /// </summary>
	        [XmlElement("purchase_num")]
	        public string PurchaseNum { get; set; }
	
	        /// <summary>
	        /// 销售换货在途量(从买家回到卖家)
	        /// </summary>
	        [XmlElement("refund_onway_num")]
	        public string RefundOnwayNum { get; set; }
	
	        /// <summary>
	        /// 待发货量
	        /// </summary>
	        [XmlElement("sending_num")]
	        public string SendingNum { get; set; }
	
	        /// <summary>
	        /// 规格码
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 主键
	        /// </summary>
	        [XmlElement("spec_id")]
	        public long SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 外部WMS中的编码
	        /// </summary>
	        [XmlElement("spec_wh_no")]
	        public string SpecWhNo { get; set; }
	
	        /// <summary>
	        /// 库存量
	        /// </summary>
	        [XmlElement("stock_num")]
	        public string StockNum { get; set; }
	
	        /// <summary>
	        /// 预订单量
	        /// </summary>
	        [XmlElement("subscribe_num")]
	        public string SubscribeNum { get; set; }
	
	        /// <summary>
	        /// 待采购量
	        /// </summary>
	        [XmlElement("to_purchase_num")]
	        public string ToPurchaseNum { get; set; }
	
	        /// <summary>
	        /// 待调拨量
	        /// </summary>
	        [XmlElement("to_transfer_num")]
	        public string ToTransferNum { get; set; }
	
	        /// <summary>
	        /// 调拨在途量
	        /// </summary>
	        [XmlElement("transfer_num")]
	        public string TransferNum { get; set; }
	
	        /// <summary>
	        /// 未付款量
	        /// </summary>
	        [XmlElement("unpay_num")]
	        public string UnpayNum { get; set; }
	
	        /// <summary>
	        /// 仓库主键id
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public long WarehouseId { get; set; }
	
	        /// <summary>
	        /// 仓库名称
	        /// </summary>
	        [XmlElement("warehouse_name")]
	        public string WarehouseName { get; set; }
	
	        /// <summary>
	        /// 仓库编号
	        /// </summary>
	        [XmlElement("warehouse_no")]
	        public string WarehouseNo { get; set; }
	
	        /// <summary>
	        /// 仓库类型
	        /// </summary>
	        [XmlElement("warehouse_type")]
	        public long WarehouseType { get; set; }
	
	        /// <summary>
	        /// 重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
	
	        /// <summary>
	        /// 外部WMS同步时占用库存
	        /// </summary>
	        [XmlElement("wms_preempty_stock")]
	        public string WmsPreemptyStock { get; set; }
	
	        /// <summary>
	        /// 外部WMS同步时,与系统库存的差
	        /// </summary>
	        [XmlElement("wms_stock_diff")]
	        public string WmsStockDiff { get; set; }
	
	        /// <summary>
	        /// 外部WMS同步时库存
	        /// </summary>
	        [XmlElement("wms_sync_stock")]
	        public string WmsSyncStock { get; set; }
	
	        /// <summary>
	        /// 与外部WMS同步时间
	        /// </summary>
	        [XmlElement("wms_sync_time")]
	        public string WmsSyncTime { get; set; }
}

    }
}
