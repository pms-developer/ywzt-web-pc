using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtLogisticsQueryResponse.
    /// </summary>
    public class WdtLogisticsQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 物流档案信息
        /// </summary>
        [XmlArray("logistics_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> LogisticsList { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("address")]
	        public string Address { get; set; }
	
	        /// <summary>
	        /// 物流佣金比例
	        /// </summary>
	        [XmlElement("commission_ratio")]
	        public string CommissionRatio { get; set; }
	
	        /// <summary>
	        /// 联系人
	        /// </summary>
	        [XmlElement("contact")]
	        public string Contact { get; set; }
	
	        /// <summary>
	        /// 是否禁用
	        /// </summary>
	        [XmlElement("is_disabled")]
	        public long IsDisabled { get; set; }
	
	        /// <summary>
	        /// 物流公司名称
	        /// </summary>
	        [XmlElement("logistics_name")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// 物流编号
	        /// </summary>
	        [XmlElement("logistics_no")]
	        public string LogisticsNo { get; set; }
	
	        /// <summary>
	        /// 物流方式
	        /// </summary>
	        [XmlElement("logistics_type")]
	        public long LogisticsType { get; set; }
	
	        /// <summary>
	        /// 自定义属性1
	        /// </summary>
	        [XmlElement("prop1")]
	        public string Prop1 { get; set; }
	
	        /// <summary>
	        /// 自定义属性2
	        /// </summary>
	        [XmlElement("prop2")]
	        public string Prop2 { get; set; }
	
	        /// <summary>
	        /// 自定义属性3
	        /// </summary>
	        [XmlElement("prop3")]
	        public string Prop3 { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 货运方式:0.标准快递、1.生鲜速配、2.冷运速配
	        /// </summary>
	        [XmlElement("send_type")]
	        public long SendType { get; set; }
	
	        /// <summary>
	        /// 电话
	        /// </summary>
	        [XmlElement("telno")]
	        public string Telno { get; set; }
}

    }
}
