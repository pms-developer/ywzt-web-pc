using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtStockinOrderQueryResponse.
    /// </summary>
    public class WdtStockinOrderQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 入库单信息
        /// </summary>
        [XmlArray("stockin_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> StockinList { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 批次号
	        /// </summary>
	        [XmlElement("batch_no")]
	        public string BatchNo { get; set; }
	
	        /// <summary>
	        /// 品牌名称
	        /// </summary>
	        [XmlElement("brand_name")]
	        public string BrandName { get; set; }
	
	        /// <summary>
	        /// 品牌编号
	        /// </summary>
	        [XmlElement("brand_no")]
	        public string BrandNo { get; set; }
	
	        /// <summary>
	        /// 成本价
	        /// </summary>
	        [XmlElement("cost_price")]
	        public string CostPrice { get; set; }
	
	        /// <summary>
	        /// 优惠金额
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 货品数量
	        /// </summary>
	        [XmlElement("goods_count")]
	        public string GoodsCount { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 基本货品单位，库存用的单位，个，件
	        /// </summary>
	        [XmlElement("goods_unit")]
	        public string GoodsUnit { get; set; }
	
	        /// <summary>
	        /// 自定义
	        /// </summary>
	        [XmlElement("prop2")]
	        public string Prop2 { get; set; }
	
	        /// <summary>
	        /// erp子订单主键
	        /// </summary>
	        [XmlElement("rec_id")]
	        public long RecId { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 调整后数量
	        /// </summary>
	        [XmlElement("right_num")]
	        public string RightNum { get; set; }
	
	        /// <summary>
	        /// 规格
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 原价
	        /// </summary>
	        [XmlElement("src_price")]
	        public string SrcPrice { get; set; }
	
	        /// <summary>
	        /// 入库单主键id
	        /// </summary>
	        [XmlElement("stockin_id")]
	        public long StockinId { get; set; }
	
	        /// <summary>
	        /// 税率
	        /// </summary>
	        [XmlElement("tax")]
	        public string Tax { get; set; }
	
	        /// <summary>
	        /// 税后金额
	        /// </summary>
	        [XmlElement("tax_amount")]
	        public string TaxAmount { get; set; }
	
	        /// <summary>
	        /// 税后单价
	        /// </summary>
	        [XmlElement("tax_price")]
	        public string TaxPrice { get; set; }
	
	        /// <summary>
	        /// 总成本
	        /// </summary>
	        [XmlElement("total_cost")]
	        public string TotalCost { get; set; }
}

    }
}
