using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtPurchaseOrderQueryResponse.
    /// </summary>
    public class WdtPurchaseOrderQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 采购单信息
        /// </summary>
        [XmlArray("purchase_list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> PurchaseList { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [XmlElement("total_count")]
        public long TotalCount { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 总金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 优惠
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 采购单货入库总金额
	        /// </summary>
	        [XmlElement("stockin_amount")]
	        public string StockinAmount { get; set; }
	
	        /// <summary>
	        /// 采购到货入库数量
	        /// </summary>
	        [XmlElement("stockin_num")]
	        public string StockinNum { get; set; }
	
	        /// <summary>
	        /// 税率
	        /// </summary>
	        [XmlElement("tax")]
	        public string Tax { get; set; }
	
	        /// <summary>
	        /// 税后总金额
	        /// </summary>
	        [XmlElement("tax_amount")]
	        public string TaxAmount { get; set; }
	
	        /// <summary>
	        /// 税后单价
	        /// </summary>
	        [XmlElement("tax_price")]
	        public string TaxPrice { get; set; }
}

    }
}
