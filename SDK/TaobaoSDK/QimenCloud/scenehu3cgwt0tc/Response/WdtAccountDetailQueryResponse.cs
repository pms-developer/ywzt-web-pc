using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtAccountDetailQueryResponse.
    /// </summary>
    public class WdtAccountDetailQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public long Errorcode { get; set; }

        /// <summary>
        /// 列表
        /// </summary>
        [XmlArray("list")]
        [XmlArrayItem("array")]
        public List<ArrayDomain> List { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// ArrayDomain Data Structure.
/// </summary>
[Serializable]

public class ArrayDomain : TopObject
{
	        /// <summary>
	        /// 平衡
	        /// </summary>
	        [XmlElement("balance")]
	        public string Balance { get; set; }
	
	        /// <summary>
	        /// 收入金额
	        /// </summary>
	        [XmlElement("in_amount")]
	        public string InAmount { get; set; }
	
	        /// <summary>
	        /// 财务类型
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 订单编号
	        /// </summary>
	        [XmlElement("order_no")]
	        public string OrderNo { get; set; }
	
	        /// <summary>
	        /// 支出金额
	        /// </summary>
	        [XmlElement("out_amount")]
	        public string OutAmount { get; set; }
	
	        /// <summary>
	        /// 平台支付单号
	        /// </summary>
	        [XmlElement("pay_order_no")]
	        public string PayOrderNo { get; set; }
	
	        /// <summary>
	        /// 平台id
	        /// </summary>
	        [XmlElement("platform_id")]
	        public long PlatformId { get; set; }
	
	        /// <summary>
	        /// 生成凭证
	        /// </summary>
	        [XmlElement("post_status")]
	        public string PostStatus { get; set; }
	
	        /// <summary>
	        /// 关联订单号
	        /// </summary>
	        [XmlElement("raw_order_no")]
	        public string RawOrderNo { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 店铺id
	        /// </summary>
	        [XmlElement("shop_id")]
	        public long ShopId { get; set; }
	
	        /// <summary>
	        /// 子业务类型
	        /// </summary>
	        [XmlElement("sub_item_id")]
	        public string SubItemId { get; set; }
	
	        /// <summary>
	        /// 生成凭证id
	        /// </summary>
	        [XmlElement("voucher_id")]
	        public long VoucherId { get; set; }
}

    }
}
