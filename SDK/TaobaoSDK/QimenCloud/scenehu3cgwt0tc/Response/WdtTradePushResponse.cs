using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtTradePushResponse.
    /// </summary>
    public class WdtTradePushResponse : QimenCloudResponse
    {
        /// <summary>
        /// 修改数量
        /// </summary>
        [XmlElement("chg_count")]
        public long ChgCount { get; set; }

        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public string Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 新增数量
        /// </summary>
        [XmlElement("new_count")]
        public long NewCount { get; set; }

    }
}
