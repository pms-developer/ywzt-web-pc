using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenehu3cgwt0tc.Response
{
    /// <summary>
    /// WdtStockinQuickRefundPushResponse.
    /// </summary>
    public class WdtStockinQuickRefundPushResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorcode")]
        public string Errorcode { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("messgae")]
        public string Messgae { get; set; }

        /// <summary>
        /// 信息
        /// </summary>
        [XmlElement("refund")]
        public string Refund { get; set; }

    }
}
