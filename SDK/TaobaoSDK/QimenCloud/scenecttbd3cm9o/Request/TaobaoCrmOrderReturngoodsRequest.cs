using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenecttbd3cm9o.Request
{
    /// <summary>
    /// TOP API: taobao.crm.order.returngoods
    /// </summary>
    public class TaobaoCrmOrderReturngoodsRequest : BaseQimenCloudRequest<QimenCloud.Api.scenecttbd3cm9o.Response.TaobaoCrmOrderReturngoodsResponse>
    {
        /// <summary>
        /// code
        /// </summary>
        public Nullable<long> Code { get; set; }

        /// <summary>
        /// 退货订单
        /// </summary>
        public string Data { get; set; }

        public DataDomain Data_ { set { this.Data = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// total_count
        /// </summary>
        public string TotalCount { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.crm.order.returngoods";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("code", this.Code);
            parameters.Add("data", this.Data);
            parameters.Add("message", this.Message);
            parameters.Add("total_count", this.TotalCount);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("data")]
public class DataDomain : TopObject
{
	        /// <summary>
	        /// cost_price
	        /// </summary>
	        [XmlElement("cost_price")]
	        public string CostPrice { get; set; }
	
	        /// <summary>
	        /// discount
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// goods_name
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// goods_no
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// market_price
	        /// </summary>
	        [XmlElement("market_price")]
	        public string MarketPrice { get; set; }
	
	        /// <summary>
	        /// oid
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
	
	        /// <summary>
	        /// order_id
	        /// </summary>
	        [XmlElement("order_id")]
	        public string OrderId { get; set; }
	
	        /// <summary>
	        /// order_num
	        /// </summary>
	        [XmlElement("order_num")]
	        public string OrderNum { get; set; }
	
	        /// <summary>
	        /// original_price
	        /// </summary>
	        [XmlElement("original_price")]
	        public string OriginalPrice { get; set; }
	
	        /// <summary>
	        /// paid
	        /// </summary>
	        [XmlElement("paid")]
	        public string Paid { get; set; }
	
	        /// <summary>
	        /// price
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// process_status
	        /// </summary>
	        [XmlElement("process_status")]
	        public string ProcessStatus { get; set; }
	
	        /// <summary>
	        /// refund_id
	        /// </summary>
	        [XmlElement("refund_id")]
	        public string RefundId { get; set; }
	
	        /// <summary>
	        /// refund_num
	        /// </summary>
	        [XmlElement("refund_num")]
	        public string RefundNum { get; set; }
	
	        /// <summary>
	        /// refund_order_amount
	        /// </summary>
	        [XmlElement("refund_order_amount")]
	        public string RefundOrderAmount { get; set; }
	
	        /// <summary>
	        /// remark
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// spec_code
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// spec_id
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// spec_name
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// spec_no
	        /// </summary>
	        [XmlElement("spec_no")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// stockin_num
	        /// </summary>
	        [XmlElement("stockin_num")]
	        public string StockinNum { get; set; }
	
	        /// <summary>
	        /// suite_name
	        /// </summary>
	        [XmlElement("suite_name")]
	        public string SuiteName { get; set; }
	
	        /// <summary>
	        /// suite_no
	        /// </summary>
	        [XmlElement("suite_no")]
	        public string SuiteNo { get; set; }
	
	        /// <summary>
	        /// suite_num
	        /// </summary>
	        [XmlElement("suite_num")]
	        public string SuiteNum { get; set; }
	
	        /// <summary>
	        /// tid
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
	
	        /// <summary>
	        /// total_amount
	        /// </summary>
	        [XmlElement("total_amount")]
	        public string TotalAmount { get; set; }
}

        #endregion
    }
}
