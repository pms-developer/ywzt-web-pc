using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenecttbd3cm9o.Response
{
    /// <summary>
    /// TaobaoCrmOrderReturngoodsResponse.
    /// </summary>
    public class TaobaoCrmOrderReturngoodsResponse : QimenCloudResponse
    {
        /// <summary>
        /// 0成功(其他失败)
        /// </summary>
        [XmlElement("returncode")]
        public string Returncode { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("returnflag")]
        public string Returnflag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlArray("returnmessage")]
        [XmlArrayItem("string")]
        public List<string> Returnmessage { get; set; }

    }
}
