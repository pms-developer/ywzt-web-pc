using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.deliveryorder.get
    /// </summary>
    public class TaobaoPosDeliveryorderGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosDeliveryorderGetResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 门店发发货单创建结束时间
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 收货人手机号
        /// </summary>
        public string Num { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public string OrderStatus { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string Orderid { get; set; }

        /// <summary>
        /// 当前页
        /// </summary>
        public Nullable<long> Page { get; set; }

        /// <summary>
        /// 每页条数
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 门店发货单创建时间
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// 门店编码
        /// </summary>
        public string StoreCode { get; set; }

        /// <summary>
        /// 提货方式
        /// </summary>
        public string TransportMode { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.deliveryorder.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("endTime", this.EndTime);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("num", this.Num);
            parameters.Add("orderStatus", this.OrderStatus);
            parameters.Add("orderid", this.Orderid);
            parameters.Add("page", this.Page);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("startTime", this.StartTime);
            parameters.Add("storeCode", this.StoreCode);
            parameters.Add("transportMode", this.TransportMode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateMaxLength("endTime", this.EndTime, 19);
            RequestValidator.ValidateRequired("orderStatus", this.OrderStatus);
            RequestValidator.ValidateMaxLength("orderStatus", this.OrderStatus, 50);
            RequestValidator.ValidateRequired("page", this.Page);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateMaxLength("startTime", this.StartTime, 19);
            RequestValidator.ValidateMaxLength("storeCode", this.StoreCode, 50);
            RequestValidator.ValidateMaxLength("transportMode", this.TransportMode, 50);
        }

        #endregion
    }
}
