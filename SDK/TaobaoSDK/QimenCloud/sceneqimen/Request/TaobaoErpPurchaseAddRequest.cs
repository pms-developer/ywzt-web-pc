using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.purchase.add
    /// </summary>
    public class TaobaoErpPurchaseAddRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpPurchaseAddResponse>
    {
        /// <summary>
        /// 制单人
        /// </summary>
        public string CreateEmp { get; set; }

        /// <summary>
        /// 创建采购单时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 路由参数
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string Purchaseid { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// sku详情
        /// </summary>
        public string Sku { get; set; }

        public List<SkuDomain> Sku_ { set { this.Sku = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 供货商代码
        /// </summary>
        public string SupplierCode { get; set; }

        /// <summary>
        /// 仓库代码
        /// </summary>
        public string WareHouseCode { get; set; }

        /// <summary>
        /// 库区代码
        /// </summary>
        public string WhareaTypeCode { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.purchase.add";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("createEmp", this.CreateEmp);
            parameters.Add("createTime", this.CreateTime);
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("purchaseid", this.Purchaseid);
            parameters.Add("remark", this.Remark);
            parameters.Add("sku", this.Sku);
            parameters.Add("supplierCode", this.SupplierCode);
            parameters.Add("wareHouseCode", this.WareHouseCode);
            parameters.Add("whareaTypeCode", this.WhareaTypeCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("purchaseid", this.Purchaseid);
            RequestValidator.ValidateObjectMaxListSize("sku", this.Sku, 3000);
        }

	/// <summary>
/// SkuDomain Data Structure.
/// </summary>
[Serializable]

public class SkuDomain : TopObject
{
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("Remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("number")]
	        public string Number { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// Sku编码
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku_ { get; set; }
	
	        /// <summary>
	        /// Sku编码
	        /// </summary>
	        [XmlElement("skucode")]
	        public string Skucode { get; set; }
	
	        /// <summary>
	        /// 标准价
	        /// </summary>
	        [XmlElement("stdprice")]
	        public string Stdprice { get; set; }
}

        #endregion
    }
}
