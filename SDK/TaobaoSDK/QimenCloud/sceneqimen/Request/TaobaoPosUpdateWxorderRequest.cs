using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.update.wxorder
    /// </summary>
    public class TaobaoPosUpdateWxorderRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosUpdateWxorderResponse>
    {
        /// <summary>
        /// 取消类型
        /// </summary>
        public string CancelType { get; set; }

        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderCode { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.update.wxorder";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("cancelType", this.CancelType);
            parameters.Add("customerid", this.Customerid);
            parameters.Add("orderCode", this.OrderCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("cancelType", this.CancelType);
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateRequired("orderCode", this.OrderCode);
        }

        #endregion
    }
}
