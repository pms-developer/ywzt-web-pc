using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.update.salesorder
    /// </summary>
    public class TaobaoPosUpdateSalesorderRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosUpdateSalesorderResponse>
    {
        /// <summary>
        /// 作废人
        /// </summary>
        public string Canceler { get; set; }

        /// <summary>
        /// 作废
        /// </summary>
        public string Cancle { get; set; }

        /// <summary>
        /// 作废日期
        /// </summary>
        public string CancleDate { get; set; }

        /// <summary>
        /// 确认
        /// </summary>
        public string Confirm { get; set; }

        /// <summary>
        /// 确认日期
        /// </summary>
        public string ConfirmDate { get; set; }

        /// <summary>
        /// 确认人
        /// </summary>
        public string Confirmor { get; set; }

        /// <summary>
        /// 路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 取货
        /// </summary>
        public string Delivery { get; set; }

        /// <summary>
        /// 取货日期
        /// </summary>
        public string DeliveryDate { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 发货仓编码
        /// </summary>
        public string FhckCode { get; set; }

        /// <summary>
        /// 完成
        /// </summary>
        public string Finish { get; set; }

        /// <summary>
        /// 完成日期
        /// </summary>
        public string FinishDate { get; set; }

        /// <summary>
        /// 明细列表
        /// </summary>
        public string MxList { get; set; }

        public List<MxListDomain> MxList_ { set { this.MxList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 单据编号
        /// </summary>
        public string OrderBillCode { get; set; }

        /// <summary>
        /// 快递代码
        /// </summary>
        public string ShippingCode { get; set; }

        /// <summary>
        /// 快递单号
        /// </summary>
        public string ShippingSn { get; set; }

        /// <summary>
        /// 状态类型
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 作废原因
        /// </summary>
        public string ZfMessage { get; set; }

        /// <summary>
        /// 作废类型
        /// </summary>
        public string ZfType { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.update.salesorder";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("canceler", this.Canceler);
            parameters.Add("cancle", this.Cancle);
            parameters.Add("cancleDate", this.CancleDate);
            parameters.Add("confirm", this.Confirm);
            parameters.Add("confirmDate", this.ConfirmDate);
            parameters.Add("confirmor", this.Confirmor);
            parameters.Add("customerid", this.Customerid);
            parameters.Add("delivery", this.Delivery);
            parameters.Add("deliveryDate", this.DeliveryDate);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("fhck_code", this.FhckCode);
            parameters.Add("finish", this.Finish);
            parameters.Add("finishDate", this.FinishDate);
            parameters.Add("mx_list", this.MxList);
            parameters.Add("orderBillCode", this.OrderBillCode);
            parameters.Add("shippingCode", this.ShippingCode);
            parameters.Add("shippingSn", this.ShippingSn);
            parameters.Add("status", this.Status);
            parameters.Add("zf_message", this.ZfMessage);
            parameters.Add("zf_type", this.ZfType);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateObjectMaxListSize("mx_list", this.MxList, 999);
            RequestValidator.ValidateRequired("orderBillCode", this.OrderBillCode);
            RequestValidator.ValidateRequired("status", this.Status);
        }

	/// <summary>
/// MxListDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("mxlist")]
public class MxListDomain : TopObject
{
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("goods_number")]
	        public string GoodsNumber { get; set; }
	
	        /// <summary>
	        /// sku
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku { get; set; }
}

        #endregion
    }
}
