using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.crm.order.sync
    /// </summary>
    public class TaobaoCrmOrderSyncRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoCrmOrderSyncResponse>
    {
        /// <summary>
        /// customerid
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 订单数据
        /// </summary>
        public string Data { get; set; }

        public DataDomain Data_ { set { this.Data = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 数据类型（zx_order直销订单;fx_order分销订单）
        /// </summary>
        public string DataType { get; set; }

        /// <summary>
        /// 数据来源ID
        /// </summary>
        public string FromNodeId { get; set; }

        /// <summary>
        /// 数据来源类型（taobao淘宝;JD京东;yihaodian一号店;dangdang当当;suning苏宁易购;amazon亚马逊;yinati银泰;mogujie蘑菇街;alibaba阿里巴巴;vop唯品会;meilishuo美丽说;youzan有赞;weixin微信;other其他）
        /// </summary>
        public string FromType { get; set; }

        /// <summary>
        /// 全局唯一任务编号
        /// </summary>
        public string MsgId { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.crm.order.sync";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("data", this.Data);
            parameters.Add("data_type", this.DataType);
            parameters.Add("from_node_id", this.FromNodeId);
            parameters.Add("from_type", this.FromType);
            parameters.Add("msg_id", this.MsgId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
        }

	/// <summary>
/// PromotionDetailsDomain Data Structure.
/// </summary>
[Serializable]

public class PromotionDetailsDomain : TopObject
{
	        /// <summary>
	        /// 如果赠品则为赠品商品ID
	        /// </summary>
	        [XmlElement("gift_item_id")]
	        public string GiftItemId { get; set; }
	
	        /// <summary>
	        /// 如果赠品则为赠品商品名称
	        /// </summary>
	        [XmlElement("gift_item_name")]
	        public string GiftItemName { get; set; }
	
	        /// <summary>
	        /// 如果赠品则为赠品数量
	        /// </summary>
	        [XmlElement("gift_item_num")]
	        public string GiftItemNum { get; set; }
	
	        /// <summary>
	        /// 优惠对应的订单ID（可能是交易号tid;也可弄是oid;ID决定优惠是在交易上海市子订单上）
	        /// </summary>
	        [XmlElement("pmt_id")]
	        public string PmtId { get; set; }
	
	        /// <summary>
	        /// 赠品类型
	        /// </summary>
	        [XmlElement("pmt_type")]
	        public string PmtType { get; set; }
	
	        /// <summary>
	        /// 优惠活动描述
	        /// </summary>
	        [XmlElement("promotion_desc")]
	        public string PromotionDesc { get; set; }
	
	        /// <summary>
	        /// 优惠金额
	        /// </summary>
	        [XmlElement("promotion_fee")]
	        public string PromotionFee { get; set; }
	
	        /// <summary>
	        /// 优惠id
	        /// </summary>
	        [XmlElement("promotion_id")]
	        public string PromotionId { get; set; }
	
	        /// <summary>
	        /// 优惠描述
	        /// </summary>
	        [XmlElement("promotion_name")]
	        public string PromotionName { get; set; }
}

	/// <summary>
/// OrdersItemsDomain Data Structure.
/// </summary>
[Serializable]

public class OrdersItemsDomain : TopObject
{
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("bn")]
	        public string Bn { get; set; }
	
	        /// <summary>
	        /// SKU成本价
	        /// </summary>
	        [XmlElement("cost")]
	        public string Cost { get; set; }
	
	        /// <summary>
	        /// 商品优惠金额
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public string DiscountFee { get; set; }
	
	        /// <summary>
	        /// SKU所属商品ID
	        /// </summary>
	        [XmlElement("iid")]
	        public string Iid { get; set; }
	
	        /// <summary>
	        /// 货品状态（normal正常;cancel删除）
	        /// </summary>
	        [XmlElement("item_status")]
	        public string ItemStatus { get; set; }
	
	        /// <summary>
	        /// SKU类型（product货品;gift赠品;adjunct配件;pkg捆绑）
	        /// </summary>
	        [XmlElement("item_type")]
	        public string ItemType { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// SKU购买数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// SKU原价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 促销方案ID
	        /// </summary>
	        [XmlElement("promotion_id")]
	        public string PromotionId { get; set; }
	
	        /// <summary>
	        /// SKU单价
	        /// </summary>
	        [XmlElement("sale_price")]
	        public string SalePrice { get; set; }
	
	        /// <summary>
	        /// SKU积分
	        /// </summary>
	        [XmlElement("score")]
	        public string Score { get; set; }
	
	        /// <summary>
	        /// SKU已发数量
	        /// </summary>
	        [XmlElement("sendnum")]
	        public string Sendnum { get; set; }
	
	        /// <summary>
	        /// 商品的最小库存单位
	        /// </summary>
	        [XmlElement("sku_id")]
	        public string SkuId { get; set; }
	
	        /// <summary>
	        /// SKU的值
	        /// </summary>
	        [XmlElement("sku_properties")]
	        public string SkuProperties { get; set; }
	
	        /// <summary>
	        /// SKU金额小计
	        /// </summary>
	        [XmlElement("total_item_fee")]
	        public string TotalItemFee { get; set; }
	
	        /// <summary>
	        /// SKU重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
}

	/// <summary>
/// OrdersDomain Data Structure.
/// </summary>
[Serializable]

public class OrdersDomain : TopObject
{
	        /// <summary>
	        /// 订单发货时间
	        /// </summary>
	        [XmlElement("consign_time")]
	        public string ConsignTime { get; set; }
	
	        /// <summary>
	        /// 订单优惠金额
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public string DiscountFee { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("iid")]
	        public string Iid { get; set; }
	
	        /// <summary>
	        /// 是否超卖（true超卖;false正常）
	        /// </summary>
	        [XmlElement("is_oversold")]
	        public string IsOversold { get; set; }
	
	        /// <summary>
	        /// 订单下的商品数量
	        /// </summary>
	        [XmlElement("items_num")]
	        public string ItemsNum { get; set; }
	
	        /// <summary>
	        /// 子订单ID
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
	
	        /// <summary>
	        /// 订单商品详情
	        /// </summary>
	        [XmlElement("orders_items")]
	        public OrdersItemsDomain OrdersItems { get; set; }
	
	        /// <summary>
	        /// 支付状态(可选值：pay_no未付款;pay_finish已付款;pay_to_medium付款至担保交易;pay_part部分付款;refund_part部分退款;refund_all全额退款）
	        /// </summary>
	        [XmlElement("refund_status")]
	        public string RefundStatus { get; set; }
	
	        /// <summary>
	        /// 子订单销售价格
	        /// </summary>
	        [XmlElement("sale_price")]
	        public string SalePrice { get; set; }
	
	        /// <summary>
	        /// 发货状态(可选值：ship_no未发货;ship_prepare配货中;ship_part部分发货;ship_finish全部发货;reship_part部分退货;reship_all全部退货）
	        /// </summary>
	        [XmlElement("ship_status")]
	        public string ShipStatus { get; set; }
	
	        /// <summary>
	        /// 订单状态（trade_active交易处理中;trade_closed交易关闭;trade_finished交易成功）
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
	
	        /// <summary>
	        /// 订单金额
	        /// </summary>
	        [XmlElement("total_order_fee")]
	        public string TotalOrderFee { get; set; }
	
	        /// <summary>
	        /// 订单类型（goods商品;gift赠品）
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
	
	        /// <summary>
	        /// 订单类型别名
	        /// </summary>
	        [XmlElement("type_alias")]
	        public string TypeAlias { get; set; }
	
	        /// <summary>
	        /// 订单货品总数量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
}

	/// <summary>
/// PaymentListsDomain Data Structure.
/// </summary>
[Serializable]

public class PaymentListsDomain : TopObject
{
	        /// <summary>
	        /// 买家会员名
	        /// </summary>
	        [XmlElement("buy_name")]
	        public string BuyName { get; set; }
	
	        /// <summary>
	        /// 买家支付账号
	        /// </summary>
	        [XmlElement("buyer_account")]
	        public string BuyerAccount { get; set; }
	
	        /// <summary>
	        /// 买家会员ID
	        /// </summary>
	        [XmlElement("buyer_id")]
	        public string BuyerId { get; set; }
	
	        /// <summary>
	        /// 支付货币类型（CNY人民币;USD美元;JPY日元;默认为CNY）
	        /// </summary>
	        [XmlElement("currency")]
	        public string Currency { get; set; }
	
	        /// <summary>
	        /// 实际支付金额
	        /// </summary>
	        [XmlElement("currency_fee")]
	        public string CurrencyFee { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("memo")]
	        public string Memo { get; set; }
	
	        /// <summary>
	        /// 支付网关内部交易号
	        /// </summary>
	        [XmlElement("outer_no")]
	        public string OuterNo { get; set; }
	
	        /// <summary>
	        /// 支付金额
	        /// </summary>
	        [XmlElement("pay_fee")]
	        public string PayFee { get; set; }
	
	        /// <summary>
	        /// 支付实际
	        /// </summary>
	        [XmlElement("pay_time")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 支付类型（online在线;offline线下;deposit预存款
	        /// </summary>
	        [XmlElement("pay_type")]
	        public string PayType { get; set; }
	
	        /// <summary>
	        /// 支付花费
	        /// </summary>
	        [XmlElement("paycost")]
	        public string Paycost { get; set; }
	
	        /// <summary>
	        /// 支付方式编码
	        /// </summary>
	        [XmlElement("payment_code")]
	        public string PaymentCode { get; set; }
	
	        /// <summary>
	        /// 支付单编号
	        /// </summary>
	        [XmlElement("payment_id")]
	        public string PaymentId { get; set; }
	
	        /// <summary>
	        /// 支付方式名称
	        /// </summary>
	        [XmlElement("payment_name")]
	        public string PaymentName { get; set; }
	
	        /// <summary>
	        /// 卖家收款银行
	        /// </summary>
	        [XmlElement("seller_bank")]
	        public string SellerBank { get; set; }
	
	        /// <summary>
	        /// 支付状态（succ支付成功;failed支付失败;cancel未支付;error参数异常;invalid校验错误;progress处理中;timeout超时;reday准备中）
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 支付创建时间
	        /// </summary>
	        [XmlElement("t_begin")]
	        public string TBegin { get; set; }
	
	        /// <summary>
	        /// 支付单对应的交易ID
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 代销人区域
	        /// </summary>
	        [XmlElement("agent_address")]
	        public string AgentAddress { get; set; }
	
	        /// <summary>
	        /// 代销人出生日期
	        /// </summary>
	        [XmlElement("agent_birthdate")]
	        public string AgentBirthdate { get; set; }
	
	        /// <summary>
	        /// 代销人城市
	        /// </summary>
	        [XmlElement("agent_city")]
	        public string AgentCity { get; set; }
	
	        /// <summary>
	        /// 代销人区域
	        /// </summary>
	        [XmlElement("agent_district")]
	        public string AgentDistrict { get; set; }
	
	        /// <summary>
	        /// 代销人邮编
	        /// </summary>
	        [XmlElement("agent_email")]
	        public string AgentEmail { get; set; }
	
	        /// <summary>
	        /// 代销人会员等级
	        /// </summary>
	        [XmlElement("agent_level")]
	        public string AgentLevel { get; set; }
	
	        /// <summary>
	        /// 代销人电话号码
	        /// </summary>
	        [XmlElement("agent_mobile")]
	        public string AgentMobile { get; set; }
	
	        /// <summary>
	        /// 代销人姓名
	        /// </summary>
	        [XmlElement("agent_name")]
	        public string AgentName { get; set; }
	
	        /// <summary>
	        /// 代销人电话号码
	        /// </summary>
	        [XmlElement("agent_phone")]
	        public string AgentPhone { get; set; }
	
	        /// <summary>
	        /// 代销人qq
	        /// </summary>
	        [XmlElement("agent_qq")]
	        public string AgentQq { get; set; }
	
	        /// <summary>
	        /// 代销人性别
	        /// </summary>
	        [XmlElement("agent_sex")]
	        public string AgentSex { get; set; }
	
	        /// <summary>
	        /// 代销人网店地址
	        /// </summary>
	        [XmlElement("agent_shop_name")]
	        public string AgentShopName { get; set; }
	
	        /// <summary>
	        /// 代销人网店地址
	        /// </summary>
	        [XmlElement("agent_shop_url")]
	        public string AgentShopUrl { get; set; }
	
	        /// <summary>
	        /// 代销人省份
	        /// </summary>
	        [XmlElement("agent_state")]
	        public string AgentState { get; set; }
	
	        /// <summary>
	        /// 代销人账号
	        /// </summary>
	        [XmlElement("agent_uname")]
	        public string AgentUname { get; set; }
	
	        /// <summary>
	        /// 代销人邮编
	        /// </summary>
	        [XmlElement("agent_zip")]
	        public string AgentZip { get; set; }
	
	        /// <summary>
	        /// 买家详细地址
	        /// </summary>
	        [XmlElement("buyer_address")]
	        public string BuyerAddress { get; set; }
	
	        /// <summary>
	        /// 买家支付宝
	        /// </summary>
	        [XmlElement("buyer_alipay_no")]
	        public string BuyerAlipayNo { get; set; }
	
	        /// <summary>
	        /// 买家城市
	        /// </summary>
	        [XmlElement("buyer_city")]
	        public string BuyerCity { get; set; }
	
	        /// <summary>
	        /// 买家地区
	        /// </summary>
	        [XmlElement("buyer_district")]
	        public string BuyerDistrict { get; set; }
	
	        /// <summary>
	        /// 买家email
	        /// </summary>
	        [XmlElement("buyer_email")]
	        public string BuyerEmail { get; set; }
	
	        /// <summary>
	        /// 买家ID
	        /// </summary>
	        [XmlElement("buyer_id")]
	        public string BuyerId { get; set; }
	
	        /// <summary>
	        /// 买家备注
	        /// </summary>
	        [XmlElement("buyer_memo")]
	        public string BuyerMemo { get; set; }
	
	        /// <summary>
	        /// 买家手机号码
	        /// </summary>
	        [XmlElement("buyer_mobile")]
	        public string BuyerMobile { get; set; }
	
	        /// <summary>
	        /// 买家姓名
	        /// </summary>
	        [XmlElement("buyer_name")]
	        public string BuyerName { get; set; }
	
	        /// <summary>
	        /// 买家获得积分
	        /// </summary>
	        [XmlElement("buyer_obtain_point_fee")]
	        public string BuyerObtainPointFee { get; set; }
	
	        /// <summary>
	        /// 买家手机号码
	        /// </summary>
	        [XmlElement("buyer_phone")]
	        public string BuyerPhone { get; set; }
	
	        /// <summary>
	        /// 买家是否已评价（true已评价;false未评价）
	        /// </summary>
	        [XmlElement("buyer_rate")]
	        public string BuyerRate { get; set; }
	
	        /// <summary>
	        /// 买家省份
	        /// </summary>
	        [XmlElement("buyer_state")]
	        public string BuyerState { get; set; }
	
	        /// <summary>
	        /// 买家账号
	        /// </summary>
	        [XmlElement("buyer_uname")]
	        public string BuyerUname { get; set; }
	
	        /// <summary>
	        /// 买家邮编
	        /// </summary>
	        [XmlElement("buyer_zip")]
	        public string BuyerZip { get; set; }
	
	        /// <summary>
	        /// 是否签收（0未签收;1已签收）
	        /// </summary>
	        [XmlElement("cod_status")]
	        public string CodStatus { get; set; }
	
	        /// <summary>
	        /// 交易佣金（紧缺到2位小数）
	        /// </summary>
	        [XmlElement("commission_fee")]
	        public string CommissionFee { get; set; }
	
	        /// <summary>
	        /// 订单确认时间
	        /// </summary>
	        [XmlElement("confirm_time")]
	        public string ConfirmTime { get; set; }
	
	        /// <summary>
	        /// 卖家发货时间
	        /// </summary>
	        [XmlElement("consign_time")]
	        public string ConsignTime { get; set; }
	
	        /// <summary>
	        /// 交易创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 交易货币类型（CNY人民币;USD美元;JPY日元;默认为CNY）
	        /// </summary>
	        [XmlElement("currency")]
	        public string Currency { get; set; }
	
	        /// <summary>
	        /// 当前货币汇率
	        /// </summary>
	        [XmlElement("currency_rate")]
	        public string CurrencyRate { get; set; }
	
	        /// <summary>
	        /// 手工调价的总和（把所有子订单的手工调价相加）
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public string DiscountFee { get; set; }
	
	        /// <summary>
	        /// 交易成功时间（更新交易状态为成功的通顺更新）
	        /// </summary>
	        [XmlElement("end_time")]
	        public string EndTime { get; set; }
	
	        /// <summary>
	        /// 商品优惠金额
	        /// </summary>
	        [XmlElement("goods_discount_fee")]
	        public string GoodsDiscountFee { get; set; }
	
	        /// <summary>
	        /// 是否开发票(可选值：true开；false不开）
	        /// </summary>
	        [XmlElement("has_invoice")]
	        public string HasInvoice { get; set; }
	
	        /// <summary>
	        /// 发票内部
	        /// </summary>
	        [XmlElement("invoice_desc")]
	        public string InvoiceDesc { get; set; }
	
	        /// <summary>
	        /// 发票税金
	        /// </summary>
	        [XmlElement("invoice_fee")]
	        public string InvoiceFee { get; set; }
	
	        /// <summary>
	        /// 发票标题(如果has_invoice为true则此参数为必须）
	        /// </summary>
	        [XmlElement("invoice_title")]
	        public string InvoiceTitle { get; set; }
	
	        /// <summary>
	        /// 品牌特卖订单标识（normal普通;agentsale特卖）
	        /// </summary>
	        [XmlElement("is_brand_sale")]
	        public string IsBrandSale { get; set; }
	
	        /// <summary>
	        /// 是否货到付款（true是;false否）
	        /// </summary>
	        [XmlElement("is_cod")]
	        public string IsCod { get; set; }
	
	        /// <summary>
	        /// 是否实体配送（true是;false否）
	        /// </summary>
	        [XmlElement("is_delivery")]
	        public string IsDelivery { get; set; }
	
	        /// <summary>
	        /// 是否异常订单（true是;false否）
	        /// </summary>
	        [XmlElement("is_errortrade")]
	        public string IsErrortrade { get; set; }
	
	        /// <summary>
	        /// 是否保价（针对物流配送）
	        /// </summary>
	        [XmlElement("is_protect")]
	        public string IsProtect { get; set; }
	
	        /// <summary>
	        /// 订单修改时间（用户对订单的操作会更新此字段）
	        /// </summary>
	        [XmlElement("lastmodify")]
	        public string Lastmodify { get; set; }
	
	        /// <summary>
	        /// 运单号
	        /// </summary>
	        [XmlElement("logistics_no")]
	        public string LogisticsNo { get; set; }
	
	        /// <summary>
	        /// 异常订单处理意见
	        /// </summary>
	        [XmlElement("mark_desc")]
	        public string MarkDesc { get; set; }
	
	        /// <summary>
	        /// 订单列表
	        /// </summary>
	        [XmlElement("orders")]
	        public OrdersDomain Orders { get; set; }
	
	        /// <summary>
	        /// 订单优惠金额
	        /// </summary>
	        [XmlElement("orders_discount_fee")]
	        public string OrdersDiscountFee { get; set; }
	
	        /// <summary>
	        /// 当前交易下订单数量
	        /// </summary>
	        [XmlElement("orders_number")]
	        public string OrdersNumber { get; set; }
	
	        /// <summary>
	        /// 支付手续费
	        /// </summary>
	        [XmlElement("pay_cost")]
	        public string PayCost { get; set; }
	
	        /// <summary>
	        /// 交易支付状态(可选值：pay_no未付款;pay_finish已付款;pay_to_medium付款至担保交易;pay_part部分付款;refund_part部分退款;refund_all全额退款）
	        /// </summary>
	        [XmlElement("pay_status")]
	        public string PayStatus { get; set; }
	
	        /// <summary>
	        /// 付款时间
	        /// </summary>
	        [XmlElement("pay_time")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 手工调价的总和（把所有子订单的手工调价相加）
	        /// </summary>
	        [XmlElement("payed_fee")]
	        public string PayedFee { get; set; }
	
	        /// <summary>
	        /// 订单支付信息列表
	        /// </summary>
	        [XmlElement("payment_lists")]
	        public PaymentListsDomain PaymentLists { get; set; }
	
	        /// <summary>
	        /// 支付方式ID
	        /// </summary>
	        [XmlElement("payment_tid")]
	        public string PaymentTid { get; set; }
	
	        /// <summary>
	        /// 支付方式名称
	        /// </summary>
	        [XmlElement("payment_type")]
	        public string PaymentType { get; set; }
	
	        /// <summary>
	        /// 买家使用积分
	        /// </summary>
	        [XmlElement("point_fee")]
	        public string PointFee { get; set; }
	
	        /// <summary>
	        /// 优惠详情
	        /// </summary>
	        [XmlElement("promotion_details")]
	        public PromotionDetailsDomain PromotionDetails { get; set; }
	
	        /// <summary>
	        /// 保价费用（针对物流配送）
	        /// </summary>
	        [XmlElement("protect_fee")]
	        public string ProtectFee { get; set; }
	
	        /// <summary>
	        /// 收货人详细地址
	        /// </summary>
	        [XmlElement("receiver_address")]
	        public string ReceiverAddress { get; set; }
	
	        /// <summary>
	        /// 收货人城市
	        /// </summary>
	        [XmlElement("receiver_city")]
	        public string ReceiverCity { get; set; }
	
	        /// <summary>
	        /// 收货人地区
	        /// </summary>
	        [XmlElement("receiver_district")]
	        public string ReceiverDistrict { get; set; }
	
	        /// <summary>
	        /// 收货人姓名
	        /// </summary>
	        [XmlElement("receiver_email")]
	        public string ReceiverEmail { get; set; }
	
	        /// <summary>
	        /// 收货人姓名
	        /// </summary>
	        [XmlElement("receiver_mobile")]
	        public string ReceiverMobile { get; set; }
	
	        /// <summary>
	        /// 收货人姓名
	        /// </summary>
	        [XmlElement("receiver_name")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 收货人省份
	        /// </summary>
	        [XmlElement("receiver_state")]
	        public string ReceiverState { get; set; }
	
	        /// <summary>
	        /// 收货人要求到货时间
	        /// </summary>
	        [XmlElement("receiver_time")]
	        public string ReceiverTime { get; set; }
	
	        /// <summary>
	        /// 收货人邮编
	        /// </summary>
	        [XmlElement("receiver_zip")]
	        public string ReceiverZip { get; set; }
	
	        /// <summary>
	        /// 卖家是否已评价（true已评价;false未评价）
	        /// </summary>
	        [XmlElement("seller_rate")]
	        public string SellerRate { get; set; }
	
	        /// <summary>
	        /// 卖家账号
	        /// </summary>
	        [XmlElement("seller_uname")]
	        public string SellerUname { get; set; }
	
	        /// <summary>
	        /// 交易物流状态(可选值：ship_no未发货;ship_prepare配货中;ship_part部分发货;ship_finish全部发货;reship_part部分退货;reship_all全部退货）
	        /// </summary>
	        [XmlElement("ship_status")]
	        public string ShipStatus { get; set; }
	
	        /// <summary>
	        /// 交易物流配送费用
	        /// </summary>
	        [XmlElement("shipping_fee")]
	        public string ShippingFee { get; set; }
	
	        /// <summary>
	        /// 创建交易时的物流方式ID
	        /// </summary>
	        [XmlElement("shipping_tid")]
	        public string ShippingTid { get; set; }
	
	        /// <summary>
	        /// 交易物流方式名称
	        /// </summary>
	        [XmlElement("shipping_type")]
	        public string ShippingType { get; set; }
	
	        /// <summary>
	        /// 交易状态(可选值: * TRADE_NO_CREATE_PAY(没有创建支付宝交易) * WAIT_BUYER_PAY(等待买家付款) * SELLER_CONSIGNED_PART(卖家部分发货) * WAIT_SELLER_SEND_GOODS(等待卖家发货,即:买家已付款) * WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货,即:卖家已发货) * TRADE_BUYER_SIGNED(买家已签收,货到付款专用) * TRADE_FINISHED(交易成功) * TRADE_CLOSED(付款以后用户退款成功，交易自动关闭) * TRADE_CLOSED_BY_TAOBAO(付款以前，卖家或买家主动关闭交易)
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 分阶段付款金额
	        /// </summary>
	        [XmlElement("step_paid_fee")]
	        public string StepPaidFee { get; set; }
	
	        /// <summary>
	        /// 分阶段订单状态（pront_nopaid_final_nopaod定金未付尾款未付;pront_paid_final_nopaod定金已付尾款未付;pront_paid_final_paod定金已付尾款已付
	        /// </summary>
	        [XmlElement("step_trade_status")]
	        public string StepTradeStatus { get; set; }
	
	        /// <summary>
	        /// 交易编号
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
	
	        /// <summary>
	        /// 交易标题
	        /// </summary>
	        [XmlElement("title")]
	        public string Title { get; set; }
	
	        /// <summary>
	        /// 当前货币订单总额
	        /// </summary>
	        [XmlElement("total_currency_fee")]
	        public string TotalCurrencyFee { get; set; }
	
	        /// <summary>
	        /// 商品总额（不包括配送费用）
	        /// </summary>
	        [XmlElement("total_goods_fee")]
	        public string TotalGoodsFee { get; set; }
	
	        /// <summary>
	        /// 交易应付总额（包括商品价格+配送费用）
	        /// </summary>
	        [XmlElement("total_trade_fee")]
	        public string TotalTradeFee { get; set; }
	
	        /// <summary>
	        /// 该笔交易中所有商品的总数量
	        /// </summary>
	        [XmlElement("total_weight")]
	        public string TotalWeight { get; set; }
	
	        /// <summary>
	        /// 交易的备注内容
	        /// </summary>
	        [XmlElement("trade_memo")]
	        public string TradeMemo { get; set; }
	
	        /// <summary>
	        /// 交易类型（fixed一口价;auction拍卖;guarantee_trade一口价拍卖;auto_delivery自动发货;ec直冲;cod货到付款;step万人团）
	        /// </summary>
	        [XmlElement("trade_type")]
	        public string TradeType { get; set; }
	
	        /// <summary>
	        /// 交易类型(可选值：fixed一口价；auction拍卖）
	        /// </summary>
	        [XmlElement("tradetype")]
	        public string Tradetype { get; set; }
}

        #endregion
    }
}
