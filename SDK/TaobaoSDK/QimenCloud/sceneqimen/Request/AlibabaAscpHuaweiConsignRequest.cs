using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: alibaba.ascp.huawei.consign
    /// </summary>
    public class AlibabaAscpHuaweiConsignRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.AlibabaAscpHuaweiConsignResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 请求内容根节点
        /// </summary>
        public string Request { get; set; }

        public RequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "alibaba.ascp.huawei.consign";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
            RequestValidator.ValidateRequired("request", this.Request);
        }

	/// <summary>
/// SnCodeListDomain Data Structure.
/// </summary>
[Serializable]

public class SnCodeListDomain : TopObject
{
	        /// <summary>
	        /// sn编码
	        /// </summary>
	        [XmlArray("snCode")]
	        [XmlArrayItem("string")]
	        public List<string> SnCode { get; set; }
}

	/// <summary>
/// AttributeDomain Data Structure.
/// </summary>
[Serializable]

public class AttributeDomain : TopObject
{
	        /// <summary>
	        /// b-part(不重复)
	        /// </summary>
	        [XmlElement("bpcode")]
	        public string Bpcode { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("itemQuantity")]
	        public Nullable<long> ItemQuantity { get; set; }
	
	        /// <summary>
	        /// sn列表
	        /// </summary>
	        [XmlElement("snCodeList")]
	        public SnCodeListDomain SnCodeList { get; set; }
}

	/// <summary>
/// AttributesDomain Data Structure.
/// </summary>
[Serializable]

public class AttributesDomain : TopObject
{
	        /// <summary>
	        /// 属性
	        /// </summary>
	        [XmlArray("attribute")]
	        [XmlArrayItem("attribute")]
	        public List<AttributeDomain> Attribute { get; set; }
}

	/// <summary>
/// OrderItemDomain Data Structure.
/// </summary>
[Serializable]

public class OrderItemDomain : TopObject
{
	        /// <summary>
	        /// 属性列表
	        /// </summary>
	        [XmlElement("attributes")]
	        public AttributesDomain Attributes { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 货品id
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 主单的交易单号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
	
	        /// <summary>
	        /// 子交易单号
	        /// </summary>
	        [XmlElement("tradeItemId")]
	        public string TradeItemId { get; set; }
}

	/// <summary>
/// OrderItemsDomain Data Structure.
/// </summary>
[Serializable]

public class OrderItemsDomain : TopObject
{
	        /// <summary>
	        /// 订单商品明细
	        /// </summary>
	        [XmlArray("orderItem")]
	        [XmlArrayItem("order_item")]
	        public List<OrderItemDomain> OrderItem { get; set; }
}

	/// <summary>
/// TmsOrderDomain Data Structure.
/// </summary>
[Serializable]

public class TmsOrderDomain : TopObject
{
	        /// <summary>
	        /// 运单信息列表
	        /// </summary>
	        [XmlElement("tmsCode")]
	        public string TmsCode { get; set; }
	
	        /// <summary>
	        /// 快递单号
	        /// </summary>
	        [XmlElement("tmsOrderCode")]
	        public string TmsOrderCode { get; set; }
}

	/// <summary>
/// TmsOrdersDomain Data Structure.
/// </summary>
[Serializable]

public class TmsOrdersDomain : TopObject
{
	        /// <summary>
	        /// 运单信息
	        /// </summary>
	        [XmlArray("tmsOrder")]
	        [XmlArrayItem("tms_order")]
	        public List<TmsOrderDomain> TmsOrder { get; set; }
}

	/// <summary>
/// RequestDomain Data Structure.
/// </summary>
[Serializable]

public class RequestDomain : TopObject
{
	        /// <summary>
	        /// 发货时间
	        /// </summary>
	        [XmlElement("consignTime")]
	        public string ConsignTime { get; set; }
	
	        /// <summary>
	        /// 订单商品明细
	        /// </summary>
	        [XmlElement("orderItems")]
	        public OrderItemsDomain OrderItems { get; set; }
	
	        /// <summary>
	        /// 货主编码
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 货主编码
	        /// </summary>
	        [XmlElement("shopName")]
	        public string ShopName { get; set; }
	
	        /// <summary>
	        /// 发货仓库
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 仓储作业单号
	        /// </summary>
	        [XmlElement("storeOrderCode")]
	        public string StoreOrderCode { get; set; }
	
	        /// <summary>
	        /// 运单信息列表
	        /// </summary>
	        [XmlElement("tmsOrders")]
	        public TmsOrdersDomain TmsOrders { get; set; }
}

        #endregion
    }
}
