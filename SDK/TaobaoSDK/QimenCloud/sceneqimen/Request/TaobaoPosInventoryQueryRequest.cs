using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.inventory.query
    /// </summary>
    public class TaobaoPosInventoryQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosInventoryQueryResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 商品sku编码
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// 商品spu编码
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// 需要查询的门店(仓库)编码
        /// </summary>
        public string StoreCode { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.inventory.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("itemCode", this.ItemCode);
            parameters.Add("productCode", this.ProductCode);
            parameters.Add("storeCode", this.StoreCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateMaxLength("itemCode", this.ItemCode, 50);
            RequestValidator.ValidateMaxLength("productCode", this.ProductCode, 50);
            RequestValidator.ValidateRequired("storeCode", this.StoreCode);
            RequestValidator.ValidateMaxLength("storeCode", this.StoreCode, 50);
        }

        #endregion
    }
}
