using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.wholesale.return.add
    /// </summary>
    public class TaobaoErpWholesaleReturnAddRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpWholesaleReturnAddResponse>
    {
        /// <summary>
        /// 制单人
        /// </summary>
        public string CreateEmp { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 仓库代码
        /// </summary>
        public string WareHouseCode { get; set; }

        /// <summary>
        /// 路由参数
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// sku详情
        /// </summary>
        public string Sku { get; set; }

        public List<StructDomain> Sku_ { set { this.Sku = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 批发退货单号
        /// </summary>
        public string Wholesalereturn { get; set; }

        /// <summary>
        /// 制单日期
        /// </summary>
        public string ZdTime { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.wholesale.return.add";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("CreateEmp", this.CreateEmp);
            parameters.Add("Remark", this.Remark);
            parameters.Add("WareHouseCode", this.WareHouseCode);
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("sku", this.Sku);
            parameters.Add("wholesalereturn", this.Wholesalereturn);
            parameters.Add("zdTime", this.ZdTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("sku", this.Sku, 3000);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("number")]
	        public string Number { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// Sku编码
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku { get; set; }
}

        #endregion
    }
}
