using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.return.oder.sync
    /// </summary>
    public class TaobaoErpReturnOderSyncRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpReturnOderSyncResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 退换ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 每页条数(不超过100)
        /// </summary>
        public string PageSize { get; set; }

        /// <summary>
        /// 当前页从1开始
        /// </summary>
        public string PageNo { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// 退换单状态(0待收货;1待结算;2被取消;3已完成;4待审核
        /// </summary>
        public string TradeStatus { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.return.oder.sync";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("endDate", this.EndDate);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("id", this.Id);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("startDate", this.StartDate);
            parameters.Add("trade_status", this.TradeStatus);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateRequired("page_no", this.PageNo);
        }

        #endregion
    }
}
