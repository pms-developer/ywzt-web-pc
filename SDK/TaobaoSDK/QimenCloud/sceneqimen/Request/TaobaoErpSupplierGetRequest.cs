using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.supplier.get
    /// </summary>
    public class TaobaoErpSupplierGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpSupplierGetResponse>
    {
        /// <summary>
        /// 最后修改的结束时间
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 当前页（从1开始）
        /// </summary>
        public Nullable<long> Page { get; set; }

        /// <summary>
        /// 每页条数(不超过100)
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string StartDate { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.supplier.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("endDate", this.EndDate);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("page", this.Page);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("startDate", this.StartDate);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("page", this.Page);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
        }

        #endregion
    }
}
