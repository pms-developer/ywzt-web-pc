using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.bms.erptrade.intercept
    /// </summary>
    public class TaobaoBmsErptradeInterceptRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoBmsErptradeInterceptResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 请求主体
        /// </summary>
        public string Request { get; set; }

        public BmsTaobaoOrderIntercepteRequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.bms.erptrade.intercept";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// OrderItemsDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("orderItem")]
public class OrderItemsDomain : TopObject
{
	        /// <summary>
	        /// 明细类型( 1.交易赠品 ;2.促销赠品; 3.手工录入赠品 ;4.交易正品; 5.手工录入正品)
	        /// </summary>
	        [XmlElement("itemGiftType")]
	        public string ItemGiftType { get; set; }
	
	        /// <summary>
	        /// 商品数字编号,00,string(2),非必填,
	        /// </summary>
	        [XmlElement("numIid")]
	        public string NumIid { get; set; }
	
	        /// <summary>
	        /// skuId
	        /// </summary>
	        [XmlElement("skuId")]
	        public string SkuId { get; set; }
	
	        /// <summary>
	        /// 交易平台订单编码
	        /// </summary>
	        [XmlElement("sourceOrderCode")]
	        public string SourceOrderCode { get; set; }
	
	        /// <summary>
	        /// 交易平台子订单号
	        /// </summary>
	        [XmlElement("subTradeId")]
	        public string SubTradeId { get; set; }
	
	        /// <summary>
	        /// 交易单号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
}

	/// <summary>
/// BmsTaobaoOrderIntercepteRequestDomain Data Structure.
/// </summary>
[Serializable]

public class BmsTaobaoOrderIntercepteRequestDomain : TopObject
{
	        /// <summary>
	        /// 明细
	        /// </summary>
	        [XmlArray("orderItems")]
	        [XmlArrayItem("orderItem")]
	        public List<OrderItemsDomain> OrderItems { get; set; }
	
	        /// <summary>
	        /// 货主ID
	        /// </summary>
	        [XmlElement("ownerUserId")]
	        public string OwnerUserId { get; set; }
	
	        /// <summary>
	        /// 拦截id
	        /// </summary>
	        [XmlElement("refundId")]
	        public string RefundId { get; set; }
	
	        /// <summary>
	        /// 店铺昵称
	        /// </summary>
	        [XmlElement("shopNick")]
	        public string ShopNick { get; set; }
	
	        /// <summary>
	        /// 交易号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
	
	        /// <summary>
	        /// 拦截类型
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
	
	        /// <summary>
	        /// 店铺ID
	        /// </summary>
	        [XmlElement("userId")]
	        public string UserId { get; set; }
}

        #endregion
    }
}
