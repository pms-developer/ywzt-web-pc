using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.store.synchronize
    /// </summary>
    public class TaobaoPosStoreSynchronizeRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosStoreSynchronizeResponse>
    {
        /// <summary>
        /// 操作
        /// </summary>
        public string ActionType { get; set; }

        /// <summary>
        /// 路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// null
        /// </summary>
        public string Store { get; set; }

        public List<StructDomain> Store_ { set { this.Store = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 所有的总条数
        /// </summary>
        public Nullable<long> Total { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.store.synchronize";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("actionType", this.ActionType);
            parameters.Add("customerid", this.Customerid);
            parameters.Add("store", this.Store);
            parameters.Add("total", this.Total);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("actionType", this.ActionType);
            RequestValidator.ValidateMaxLength("actionType", this.ActionType, 50);
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateObjectMaxListSize("store", this.Store, 20);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 结算币制
	        /// </summary>
	        [XmlElement("currency")]
	        public string Currency { get; set; }
	
	        /// <summary>
	        /// 结算类型(0=不结算 1=发货结算 2=零售结算)
	        /// </summary>
	        [XmlElement("settlementType")]
	        public Nullable<long> SettlementType { get; set; }
}

        #endregion
    }
}
