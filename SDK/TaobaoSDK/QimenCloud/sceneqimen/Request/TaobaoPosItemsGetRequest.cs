using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.items.get
    /// </summary>
    public class TaobaoPosItemsGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosItemsGetResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 最后修改的结束时间
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 商品SKU编码
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// 当前页
        /// </summary>
        public Nullable<long> Page { get; set; }

        /// <summary>
        /// 每页条数
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 商品的SPU编码
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string StartTime { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.items.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("endTime", this.EndTime);
            parameters.Add("itemCode", this.ItemCode);
            parameters.Add("page", this.Page);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("productCode", this.ProductCode);
            parameters.Add("startTime", this.StartTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateMaxLength("endTime", this.EndTime, 19);
            RequestValidator.ValidateMaxLength("itemCode", this.ItemCode, 50);
            RequestValidator.ValidateRequired("page", this.Page);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateMaxLength("productCode", this.ProductCode, 50);
            RequestValidator.ValidateMaxLength("startTime", this.StartTime, 19);
        }

        #endregion
    }
}
