using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.icp.order.stockinordecanceltoerp
    /// </summary>
    public class TaobaoIcpOrderStockinordecanceltoerpRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoIcpOrderStockinordecanceltoerpResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 订单列表
        /// </summary>
        public string EntryOrderlist { get; set; }

        public EntryOrderlistDomain EntryOrderlist_ { set { this.EntryOrderlist = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.icp.order.stockinordecanceltoerp";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("entryOrderlist", this.EntryOrderlist);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
        }

	/// <summary>
/// EntryOrderDomain Data Structure.
/// </summary>
[Serializable]

public class EntryOrderDomain : TopObject
{
	        /// <summary>
	        /// 入库单号
	        /// </summary>
	        [XmlElement("entryOrderCode")]
	        public string EntryOrderCode { get; set; }
	
	        /// <summary>
	        /// 入库单类型
	        /// </summary>
	        [XmlElement("entryOrderType")]
	        public string EntryOrderType { get; set; }
	
	        /// <summary>
	        /// 商家code
	        /// </summary>
	        [XmlElement("ownerInCode")]
	        public string OwnerInCode { get; set; }
}

	/// <summary>
/// EntryOrderlistDomain Data Structure.
/// </summary>
[Serializable]

public class EntryOrderlistDomain : TopObject
{
	        /// <summary>
	        /// 订单
	        /// </summary>
	        [XmlElement("entryOrder")]
	        public EntryOrderDomain EntryOrder { get; set; }
}

        #endregion
    }
}
