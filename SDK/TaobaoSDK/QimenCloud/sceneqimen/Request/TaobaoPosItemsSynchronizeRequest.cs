using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.items.synchronize
    /// </summary>
    public class TaobaoPosItemsSynchronizeRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosItemsSynchronizeResponse>
    {
        /// <summary>
        /// 操作
        /// </summary>
        public string ActionType { get; set; }

        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// null
        /// </summary>
        public string Item { get; set; }

        public List<StructDomain> Item_ { set { this.Item = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 所有的总条数
        /// </summary>
        public Nullable<long> Total { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.items.synchronize";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("actionType", this.ActionType);
            parameters.Add("customerid", this.Customerid);
            parameters.Add("item", this.Item);
            parameters.Add("total", this.Total);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("actionType", this.ActionType);
            RequestValidator.ValidateMaxLength("actionType", this.ActionType, 50);
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateObjectMaxListSize("item", this.Item, 20);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 调价结束日期
	        /// </summary>
	        [XmlElement("endDate")]
	        public string EndDate { get; set; }
	
	        /// <summary>
	        /// 调价后的价格
	        /// </summary>
	        [XmlElement("standardPrice")]
	        public string StandardPrice { get; set; }
	
	        /// <summary>
	        /// 调价开始日期
	        /// </summary>
	        [XmlElement("startDate")]
	        public string StartDate { get; set; }
	
	        /// <summary>
	        /// 调价类型
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
}

        #endregion
    }
}
