using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.uop.tob.deliveryorder.confirm
    /// </summary>
    public class TaobaoUopTobDeliveryorderConfirmRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoUopTobDeliveryorderConfirmResponse>
    {
        /// <summary>
        /// customerId
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// tob出库确认对象
        /// </summary>
        public string Request { get; set; }

        public ToBDeliveryOrderConfirmRequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.uop.tob.deliveryorder.confirm";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
        }

	/// <summary>
/// BatchDomain Data Structure.
/// </summary>
[Serializable]

public class BatchDomain : TopObject
{
	        /// <summary>
	        /// 批次编码
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// 到期日期
	        /// </summary>
	        [XmlElement("dueDate")]
	        public string DueDate { get; set; }
	
	        /// <summary>
	        /// 保质期
	        /// </summary>
	        [XmlElement("guaranteePeriod")]
	        public string GuaranteePeriod { get; set; }
	
	        /// <summary>
	        /// 单位(天，月，年)
	        /// </summary>
	        [XmlElement("guaranteeUnit")]
	        public string GuaranteeUnit { get; set; }
	
	        /// <summary>
	        /// 库存类型
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 生产地址
	        /// </summary>
	        [XmlElement("produceArea")]
	        public string ProduceArea { get; set; }
	
	        /// <summary>
	        /// 生产PIN码
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// 生产日期
	        /// </summary>
	        [XmlElement("produceDate")]
	        public string ProduceDate { get; set; }
	
	        /// <summary>
	        /// 二维码
	        /// </summary>
	        [XmlElement("qrCode")]
	        public string QrCode { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public string Quantity { get; set; }
	
	        /// <summary>
	        /// SN编码
	        /// </summary>
	        [XmlElement("snCode")]
	        public string SnCode { get; set; }
}

	/// <summary>
/// OrderLineDomain Data Structure.
/// </summary>
[Serializable]

public class OrderLineDomain : TopObject
{
	        /// <summary>
	        /// 实际出库数量
	        /// </summary>
	        [XmlElement("actualQty")]
	        public string ActualQty { get; set; }
	
	        /// <summary>
	        /// 批次信息列表
	        /// </summary>
	        [XmlArray("batchs")]
	        [XmlArrayItem("batch")]
	        public List<BatchDomain> Batchs { get; set; }
	
	        /// <summary>
	        /// 高
	        /// </summary>
	        [XmlElement("height")]
	        public string Height { get; set; }
	
	        /// <summary>
	        /// 是否最后一次，默认最后一次
	        /// </summary>
	        [XmlElement("isCompleted")]
	        public string IsCompleted { get; set; }
	
	        /// <summary>
	        /// 货品商家编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 菜鸟货品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 长
	        /// </summary>
	        [XmlElement("length")]
	        public string Length { get; set; }
	
	        /// <summary>
	        /// 订单行号
	        /// </summary>
	        [XmlElement("orderLineNo")]
	        public string OrderLineNo { get; set; }
	
	        /// <summary>
	        /// 货主ID
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 计划出库数量
	        /// </summary>
	        [XmlElement("planQty")]
	        public string PlanQty { get; set; }
	
	        /// <summary>
	        /// 订单平台交易号
	        /// </summary>
	        [XmlElement("sourceOrderCode")]
	        public string SourceOrderCode { get; set; }
	
	        /// <summary>
	        /// 订单平台子交易号
	        /// </summary>
	        [XmlElement("subSourceOrderCode")]
	        public string SubSourceOrderCode { get; set; }
	
	        /// <summary>
	        /// 体积
	        /// </summary>
	        [XmlElement("volume")]
	        public string Volume { get; set; }
	
	        /// <summary>
	        /// 重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
	
	        /// <summary>
	        /// 宽
	        /// </summary>
	        [XmlElement("width")]
	        public string Width { get; set; }
}

	/// <summary>
/// PackageMaterialDomain Data Structure.
/// </summary>
[Serializable]

public class PackageMaterialDomain : TopObject
{
	        /// <summary>
	        /// 包材数量
	        /// </summary>
	        [XmlElement("materialQuantity")]
	        public string MaterialQuantity { get; set; }
	
	        /// <summary>
	        /// 包材类型
	        /// </summary>
	        [XmlElement("materialType")]
	        public string MaterialType { get; set; }
}

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain : TopObject
{
	        /// <summary>
	        /// 批次
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// 到期日期
	        /// </summary>
	        [XmlElement("dueDate")]
	        public string DueDate { get; set; }
	
	        /// <summary>
	        /// 货品商家编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 菜鸟货品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 货品数量
	        /// </summary>
	        [XmlElement("itemQuantity")]
	        public string ItemQuantity { get; set; }
	
	        /// <summary>
	        /// 生产品号
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// 生产日期
	        /// </summary>
	        [XmlElement("produceDate")]
	        public string ProduceDate { get; set; }
}

	/// <summary>
/// PackageDomain Data Structure.
/// </summary>
[Serializable]

public class PackageDomain : TopObject
{
	        /// <summary>
	        /// 物流公司运单号
	        /// </summary>
	        [XmlElement("expressCode")]
	        public string ExpressCode { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendField")]
	        public string ExtendField { get; set; }
	
	        /// <summary>
	        /// 包裹明细列表
	        /// </summary>
	        [XmlArray("items")]
	        [XmlArrayItem("item")]
	        public List<ItemDomain> Items { get; set; }
	
	        /// <summary>
	        /// 包裹号
	        /// </summary>
	        [XmlElement("packageCode")]
	        public string PackageCode { get; set; }
	
	        /// <summary>
	        /// 包裹高
	        /// </summary>
	        [XmlElement("packageHeight")]
	        public string PackageHeight { get; set; }
	
	        /// <summary>
	        /// 包裹长
	        /// </summary>
	        [XmlElement("packageLength")]
	        public string PackageLength { get; set; }
	
	        /// <summary>
	        /// 包材列表
	        /// </summary>
	        [XmlArray("packageMaterialList")]
	        [XmlArrayItem("package_material")]
	        public List<PackageMaterialDomain> PackageMaterialList { get; set; }
	
	        /// <summary>
	        /// 包裹体积
	        /// </summary>
	        [XmlElement("packageVolume")]
	        public string PackageVolume { get; set; }
	
	        /// <summary>
	        /// 包裹重量
	        /// </summary>
	        [XmlElement("packageWeight")]
	        public string PackageWeight { get; set; }
	
	        /// <summary>
	        /// 包裹宽
	        /// </summary>
	        [XmlElement("packageWidth")]
	        public string PackageWidth { get; set; }
}

	/// <summary>
/// InvoinceConfirmDomain Data Structure.
/// </summary>
[Serializable]

public class InvoinceConfirmDomain : TopObject
{
	        /// <summary>
	        /// ERP发票ID
	        /// </summary>
	        [XmlElement("billId")]
	        public string BillId { get; set; }
	
	        /// <summary>
	        /// 发票代码
	        /// </summary>
	        [XmlElement("invoiceCode")]
	        public string InvoiceCode { get; set; }
	
	        /// <summary>
	        /// 发票号
	        /// </summary>
	        [XmlElement("invoiceNumber")]
	        public string InvoiceNumber { get; set; }
	
	        /// <summary>
	        /// 交易号
	        /// </summary>
	        [XmlElement("tradeNumber")]
	        public string TradeNumber { get; set; }
}

	/// <summary>
/// DeliveryRequirementDomain Data Structure.
/// </summary>
[Serializable]

public class DeliveryRequirementDomain : TopObject
{
	        /// <summary>
	        /// 预计送达时间
	        /// </summary>
	        [XmlElement("scheduleEstimateDate")]
	        public string ScheduleEstimateDate { get; set; }
}

	/// <summary>
/// ToBDeliveryOrderConfirmRequestDomain Data Structure.
/// </summary>
[Serializable]

public class ToBDeliveryOrderConfirmRequestDomain : TopObject
{
	        /// <summary>
	        /// 到货渠道类型，VIP＝1、门店＝2、经销商＝3
	        /// </summary>
	        [XmlElement("arriveChannelType")]
	        public string ArriveChannelType { get; set; }
	
	        /// <summary>
	        /// 菜鸟仓作业单号
	        /// </summary>
	        [XmlElement("cnOrderCode")]
	        public string CnOrderCode { get; set; }
	
	        /// <summary>
	        /// 出库状态，默认只有1,：全部出库
	        /// </summary>
	        [XmlElement("confirmType")]
	        public string ConfirmType { get; set; }
	
	        /// <summary>
	        /// ERP单号
	        /// </summary>
	        [XmlElement("deliveryOrderCode")]
	        public string DeliveryOrderCode { get; set; }
	
	        /// <summary>
	        /// ERP菜鸟单号
	        /// </summary>
	        [XmlElement("deliveryReceiptId")]
	        public string DeliveryReceiptId { get; set; }
	
	        /// <summary>
	        /// 发货要求
	        /// </summary>
	        [XmlElement("deliveryRequirement")]
	        public DeliveryRequirementDomain DeliveryRequirement { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendField")]
	        public string ExtendField { get; set; }
	
	        /// <summary>
	        /// 发票信息
	        /// </summary>
	        [XmlArray("invoinceConfirms")]
	        [XmlArrayItem("invoince_confirm")]
	        public List<InvoinceConfirmDomain> InvoinceConfirms { get; set; }
	
	        /// <summary>
	        /// 物流公司编码
	        /// </summary>
	        [XmlElement("logisticsCode")]
	        public string LogisticsCode { get; set; }
	
	        /// <summary>
	        /// 物流公司名称
	        /// </summary>
	        [XmlElement("logisticsName")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// 订单出库时间
	        /// </summary>
	        [XmlElement("orderConfirmTime")]
	        public string OrderConfirmTime { get; set; }
	
	        /// <summary>
	        /// 订单明细列表
	        /// </summary>
	        [XmlArray("orderLines")]
	        [XmlArrayItem("order_line")]
	        public List<OrderLineDomain> OrderLines { get; set; }
	
	        /// <summary>
	        /// 单据类型,出库单类型(JYCK=一般交易出库单;HHCK=换货出库单;BFCK=补发出库单;QTCK=其他出库单;TOBCK=TOB出库)
	        /// </summary>
	        [XmlElement("orderType")]
	        public string OrderType { get; set; }
	
	        /// <summary>
	        /// 消息去重吗
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 货主ID
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 包裹列表
	        /// </summary>
	        [XmlArray("packages")]
	        [XmlArrayItem("package")]
	        public List<PackageDomain> Packages { get; set; }
	
	        /// <summary>
	        /// 店铺ID
	        /// </summary>
	        [XmlElement("sellerId")]
	        public string SellerId { get; set; }
	
	        /// <summary>
	        /// 菜鸟仓code
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 时间戳
	        /// </summary>
	        [XmlElement("timeZone")]
	        public string TimeZone { get; set; }
	
	        /// <summary>
	        /// 发货仓库作业单号
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

        #endregion
    }
}
