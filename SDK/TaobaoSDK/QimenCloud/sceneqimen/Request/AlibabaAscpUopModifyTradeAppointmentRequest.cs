using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: alibaba.ascp.uop.modify.trade.appointment
    /// </summary>
    public class AlibabaAscpUopModifyTradeAppointmentRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.AlibabaAscpUopModifyTradeAppointmentResponse>
    {
        /// <summary>
        /// 请求内容根节点
        /// </summary>
        public string Request { get; set; }

        public RequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "alibaba.ascp.uop.modify.trade.appointment";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// SubTradeIdListDomain Data Structure.
/// </summary>
[Serializable]

public class SubTradeIdListDomain : TopObject
{
	        /// <summary>
	        /// 子订单号
	        /// </summary>
	        [XmlElement("subTradeId")]
	        public string SubTradeId { get; set; }
}

	/// <summary>
/// RequestDomain Data Structure.
/// </summary>
[Serializable]

public class RequestDomain : TopObject
{
	        /// <summary>
	        /// 单据来源 - 对应交易渠道编码
	        /// </summary>
	        [XmlElement("orderSourceCode")]
	        public string OrderSourceCode { get; set; }
	
	        /// <summary>
	        /// 原预约日期
	        /// </summary>
	        [XmlElement("originOsDate")]
	        public string OriginOsDate { get; set; }
	
	        /// <summary>
	        /// 原预约时间范围
	        /// </summary>
	        [XmlElement("originOsRange")]
	        public string OriginOsRange { get; set; }
	
	        /// <summary>
	        /// 需改约的目标预约日期
	        /// </summary>
	        [XmlElement("osDate")]
	        public string OsDate { get; set; }
	
	        /// <summary>
	        /// 需改约的目标预约时间范围
	        /// </summary>
	        [XmlElement("osRange")]
	        public string OsRange { get; set; }
	
	        /// <summary>
	        /// 申请单号，外部ERP用于幂等
	        /// </summary>
	        [XmlElement("requestOrderNo")]
	        public string RequestOrderNo { get; set; }
	
	        /// <summary>
	        /// 店铺NICK
	        /// </summary>
	        [XmlElement("sellerNick")]
	        public string SellerNick { get; set; }
	
	        /// <summary>
	        /// 子订单数组
	        /// </summary>
	        [XmlElement("subTradeIdList")]
	        public SubTradeIdListDomain SubTradeIdList { get; set; }
	
	        /// <summary>
	        /// 申请提交时间
	        /// </summary>
	        [XmlElement("submitTime")]
	        public string SubmitTime { get; set; }
	
	        /// <summary>
	        /// 主订单号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
}

        #endregion
    }
}
