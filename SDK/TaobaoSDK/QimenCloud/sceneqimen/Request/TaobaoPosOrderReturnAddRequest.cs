using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.order.return.add
    /// </summary>
    public class TaobaoPosOrderReturnAddRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosOrderReturnAddResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 退单信息
        /// </summary>
        public string SellReturnRecord { get; set; }

        public SellReturnRecordDomain SellReturnRecord_ { set { this.SellReturnRecord = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.order.return.add";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("sell_return_record", this.SellReturnRecord);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
        }

	/// <summary>
/// OrderReturnGoodsDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("OrderReturnGoods")]
public class OrderReturnGoodsDomain : TopObject
{
	        /// <summary>
	        /// 商品数量
	        /// </summary>
	        [XmlElement("goods_number")]
	        public string GoodsNumber { get; set; }
	
	        /// <summary>
	        /// 商品实际成交价
	        /// </summary>
	        [XmlElement("goods_price")]
	        public string GoodsPrice { get; set; }
	
	        /// <summary>
	        /// 商品吊牌价
	        /// </summary>
	        [XmlElement("market_price")]
	        public string MarketPrice { get; set; }
	
	        /// <summary>
	        /// 商品售价
	        /// </summary>
	        [XmlElement("shop_price")]
	        public string ShopPrice { get; set; }
	
	        /// <summary>
	        /// 商品SKU
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku { get; set; }
}

	/// <summary>
/// SellReturnRecordDomain Data Structure.
/// </summary>
[Serializable]

public class SellReturnRecordDomain : TopObject
{
	        /// <summary>
	        /// 退单商品信息
	        /// </summary>
	        [XmlArray("order_return_goods")]
	        [XmlArrayItem("OrderReturnGoods")]
	        public List<OrderReturnGoodsDomain> OrderReturnGoods { get; set; }
	
	        /// <summary>
	        /// 订单编号
	        /// </summary>
	        [XmlElement("order_sn")]
	        public string OrderSn { get; set; }
	
	        /// <summary>
	        /// 退货仓库代码
	        /// </summary>
	        [XmlElement("return_ck_code")]
	        public string ReturnCkCode { get; set; }
	
	        /// <summary>
	        /// 退货库位代码
	        /// </summary>
	        [XmlElement("return_kw_code")]
	        public string ReturnKwCode { get; set; }
	
	        /// <summary>
	        /// 退单类型（1退货2追件3拒收）
	        /// </summary>
	        [XmlElement("return_type")]
	        public string ReturnType { get; set; }
}

        #endregion
    }
}
