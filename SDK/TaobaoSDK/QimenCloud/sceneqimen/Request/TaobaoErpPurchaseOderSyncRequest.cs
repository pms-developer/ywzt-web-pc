using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.purchase.oder.sync
    /// </summary>
    public class TaobaoErpPurchaseOderSyncRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpPurchaseOderSyncResponse>
    {
        /// <summary>
        /// 采购单ID
        /// </summary>
        public string OrderID { get; set; }

        /// <summary>
        /// 采购单号
        /// </summary>
        public string OrderNO { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 每页条数(不超过100)
        /// </summary>
        public string PageSize { get; set; }

        /// <summary>
        /// 当前页从1开始
        /// </summary>
        public string PageNo { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// 查询时间类型（0:登记时间 1:审核时间(默认登记时间）
        /// </summary>
        public string TimeType { get; set; }

        /// <summary>
        /// 采购单状态(0待审核;1执行中;2被取消;3已完成;4被终止)
        /// </summary>
        public string TradeStatus { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.purchase.oder.sync";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("OrderID", this.OrderID);
            parameters.Add("OrderNO", this.OrderNO);
            parameters.Add("endDate", this.EndDate);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("startDate", this.StartDate);
            parameters.Add("time_type", this.TimeType);
            parameters.Add("trade_status", this.TradeStatus);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateRequired("page_no", this.PageNo);
        }

        #endregion
    }
}
