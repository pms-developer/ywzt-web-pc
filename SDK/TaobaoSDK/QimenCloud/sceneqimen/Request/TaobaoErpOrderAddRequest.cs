using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.order.add
    /// </summary>
    public class TaobaoErpOrderAddRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpOrderAddResponse>
    {
        /// <summary>
        /// 数据详情
        /// </summary>
        public string Data { get; set; }

        public DataDomain Data_ { set { this.Data = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 总计个数
        /// </summary>
        public Nullable<long> Total { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.order.add";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("data", this.Data);
            parameters.Add("total", this.Total);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// ItemsDomain Data Structure.
/// </summary>
[Serializable]

public class ItemsDomain : TopObject
{
	        /// <summary>
	        /// 颜色代码
	        /// </summary>
	        [XmlElement("color_code")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 平台交易号（外部系统对接此字段为准）
	        /// </summary>
	        [XmlElement("deal_code")]
	        public string DealCode { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 是否礼品（0:正常商品;1:礼品;）
	        /// </summary>
	        [XmlElement("is_gift")]
	        public Nullable<long> IsGift { get; set; }
	
	        /// <summary>
	        /// 商品货号
	        /// </summary>
	        [XmlElement("item_id")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("item_name")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("item_number")]
	        public Nullable<long> ItemNumber { get; set; }
	
	        /// <summary>
	        /// 商品实际成交价
	        /// </summary>
	        [XmlElement("item_price")]
	        public string ItemPrice { get; set; }
	
	        /// <summary>
	        /// 商品吊牌价
	        /// </summary>
	        [XmlElement("market_price")]
	        public string MarketPrice { get; set; }
	
	        /// <summary>
	        /// 订单编号（外部系统对接此字段无效）
	        /// </summary>
	        [XmlElement("order_sn")]
	        public string OrderSn { get; set; }
	
	        /// <summary>
	        /// 商品售价
	        /// </summary>
	        [XmlElement("shop_price")]
	        public string ShopPrice { get; set; }
	
	        /// <summary>
	        /// 尺码代码
	        /// </summary>
	        [XmlElement("size_code")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 商品SKU编码
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 订单下单时间
	        /// </summary>
	        [XmlElement("add_time")]
	        public string AddTime { get; set; }
	
	        /// <summary>
	        /// 平台交易号（外部系统对接此字段为准）
	        /// </summary>
	        [XmlElement("deal_code")]
	        public string DealCode { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 发货优先级（0-9越大优先级越高）
	        /// </summary>
	        [XmlElement("fh_priority")]
	        public string FhPriority { get; set; }
	
	        /// <summary>
	        /// 交易类型 0:款到发货;1:货到付款
	        /// </summary>
	        [XmlElement("is_cod")]
	        public string IsCod { get; set; }
	
	        /// <summary>
	        /// 详情
	        /// </summary>
	        [XmlElement("items")]
	        public ItemsDomain Items { get; set; }
	
	        /// <summary>
	        /// 应付金额
	        /// </summary>
	        [XmlElement("order_amount")]
	        public string OrderAmount { get; set; }
	
	        /// <summary>
	        /// 订单编号（外部系统对接此字段无效）
	        /// </summary>
	        [XmlElement("order_sn")]
	        public string OrderSn { get; set; }
	
	        /// <summary>
	        /// 支付方式代码
	        /// </summary>
	        [XmlElement("pay_code")]
	        public string PayCode { get; set; }
	
	        /// <summary>
	        /// 付款状态（0:未付款;1:已付款）
	        /// </summary>
	        [XmlElement("pay_status")]
	        public string PayStatus { get; set; }
	
	        /// <summary>
	        /// 订单支付时间
	        /// </summary>
	        [XmlElement("pay_time")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 已付金额
	        /// </summary>
	        [XmlElement("payment")]
	        public string Payment { get; set; }
	
	        /// <summary>
	        /// 收货人的详细地址
	        /// </summary>
	        [XmlElement("receiver_address")]
	        public string ReceiverAddress { get; set; }
	
	        /// <summary>
	        /// 收货人的城市
	        /// </summary>
	        [XmlElement("receiver_city")]
	        public string ReceiverCity { get; set; }
	
	        /// <summary>
	        /// 收货人的国家
	        /// </summary>
	        [XmlElement("receiver_country")]
	        public string ReceiverCountry { get; set; }
	
	        /// <summary>
	        /// 收货人的地区
	        /// </summary>
	        [XmlElement("receiver_district")]
	        public string ReceiverDistrict { get; set; }
	
	        /// <summary>
	        /// 收货人的电子邮件
	        /// </summary>
	        [XmlElement("receiver_email")]
	        public string ReceiverEmail { get; set; }
	
	        /// <summary>
	        /// 收货人的手机号码（电话号码和手机号码两个必须有一个不能为空）
	        /// </summary>
	        [XmlElement("receiver_mobile")]
	        public string ReceiverMobile { get; set; }
	
	        /// <summary>
	        /// 收货人姓名
	        /// </summary>
	        [XmlElement("receiver_name")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 收货人的省份
	        /// </summary>
	        [XmlElement("receiver_province")]
	        public string ReceiverProvince { get; set; }
	
	        /// <summary>
	        /// 收货人的电话号码
	        /// </summary>
	        [XmlElement("receiver_tel")]
	        public string ReceiverTel { get; set; }
	
	        /// <summary>
	        /// 收货人的邮政编码
	        /// </summary>
	        [XmlElement("receiver_zip")]
	        public string ReceiverZip { get; set; }
	
	        /// <summary>
	        /// 店铺代码
	        /// </summary>
	        [XmlElement("sd_code")]
	        public string SdCode { get; set; }
	
	        /// <summary>
	        /// 快递方式代码
	        /// </summary>
	        [XmlElement("shipping_code")]
	        public string ShippingCode { get; set; }
	
	        /// <summary>
	        /// 运费
	        /// </summary>
	        [XmlElement("shipping_fee")]
	        public string ShippingFee { get; set; }
	
	        /// <summary>
	        /// 快递号
	        /// </summary>
	        [XmlElement("shipping_sn")]
	        public string ShippingSn { get; set; }
	
	        /// <summary>
	        /// 订单来源（API:默认;TM_CLOUD:聚石塔;JD_CLOUD:云鼎等）
	        /// </summary>
	        [XmlElement("source")]
	        public string Source { get; set; }
	
	        /// <summary>
	        /// 订单子来源（TM_CLOUD1:聚石塔1; TM_CLOUD2:聚石塔2等）
	        /// </summary>
	        [XmlElement("sub_source")]
	        public string SubSource { get; set; }
	
	        /// <summary>
	        /// 会员名称
	        /// </summary>
	        [XmlElement("user_name")]
	        public string UserName { get; set; }
}

        #endregion
    }
}
