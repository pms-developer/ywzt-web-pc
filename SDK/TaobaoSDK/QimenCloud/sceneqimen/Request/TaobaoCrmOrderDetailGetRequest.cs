using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.crm.order.detail.get
    /// </summary>
    public class TaobaoCrmOrderDetailGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoCrmOrderDetailGetResponse>
    {
        /// <summary>
        /// 路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 查询结束时间： 2000-11-30 23:59:59
        /// </summary>
        public string EndModified { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 支持的传入字段：order_id,order_sn,deal_code,pay_status,order_status,shipping_status,lylx,shipping_code,shipping_sn,shipping_fee,sd_id,lastchanged,is_split,split_new_orders,is_combine,combine_new_orders,order_msg,fhck_id,shipping_time_fh,qd_id,tbfx_id,ori_deal_code,user_name,receiver_name,receiver_address,receiver_tel,receiver_mobile,shipping_name,seller_msg,weigh,goods_sn,sku,color_id,size_id,goods_number,brand_id
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderSn { get; set; }

        /// <summary>
        /// 页码: 取值范围:大于零的整数; 默认值:1
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 每页条数。取值范围:大于零的整数; 默认值:20;最大值:100
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 店铺代码
        /// </summary>
        public string SdCode { get; set; }

        /// <summary>
        /// 查询起始时间： 2011-09-01 00:00:00
        /// </summary>
        public string StartModified { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.crm.order.detail.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("endModified", this.EndModified);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("fields", this.Fields);
            parameters.Add("orderSn", this.OrderSn);
            parameters.Add("pageNo", this.PageNo);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("sd_code", this.SdCode);
            parameters.Add("startModified", this.StartModified);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("endModified", this.EndModified);
            RequestValidator.ValidateMaxLength("endModified", this.EndModified, 64);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateMaxLength("fields", this.Fields, 255);
            RequestValidator.ValidateMaxLength("orderSn", this.OrderSn, 64);
            RequestValidator.ValidateRequired("pageNo", this.PageNo);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateMaxLength("sd_code", this.SdCode, 64);
            RequestValidator.ValidateRequired("startModified", this.StartModified);
            RequestValidator.ValidateMaxLength("startModified", this.StartModified, 64);
        }

        #endregion
    }
}
