using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.auto.returnorder.confirm
    /// </summary>
    public class TaobaoAutoReturnorderConfirmRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoAutoReturnorderConfirmResponse>
    {
        /// <summary>
        /// customerId
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// null
        /// </summary>
        public string Request { get; set; }

        public AutoReturnInOrderConfirmRequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.auto.returnorder.confirm";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
        }

	/// <summary>
/// SenderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SenderInfoDomain : TopObject
{
	        /// <summary>
	        /// 余杭区
	        /// </summary>
	        [XmlElement("area")]
	        public string Area { get; set; }
	
	        /// <summary>
	        /// 杭州市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 甲乙丙丁
	        /// </summary>
	        [XmlElement("company")]
	        public string Company { get; set; }
	
	        /// <summary>
	        /// 123
	        /// </summary>
	        [XmlElement("countryCode")]
	        public string CountryCode { get; set; }
	
	        /// <summary>
	        /// 文一西路969号
	        /// </summary>
	        [XmlElement("detailAddress")]
	        public string DetailAddress { get; set; }
	
	        /// <summary>
	        /// 123
	        /// </summary>
	        [XmlElement("email")]
	        public string Email { get; set; }
	
	        /// <summary>
	        /// 13889012790
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 李四
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 浙江省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 0571-12345679
	        /// </summary>
	        [XmlElement("tel")]
	        public string Tel { get; set; }
	
	        /// <summary>
	        /// 五常街道
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
	
	        /// <summary>
	        /// 3100000
	        /// </summary>
	        [XmlElement("zipCode")]
	        public string ZipCode { get; set; }
}

	/// <summary>
/// BatchsDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("batch")]
public class BatchsDomain : TopObject
{
	        /// <summary>
	        /// 2
	        /// </summary>
	        [XmlElement("actualQty")]
	        public string ActualQty { get; set; }
	
	        /// <summary>
	        /// PO8217136
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// 2015-11-29 13:20:00
	        /// </summary>
	        [XmlElement("expireDate")]
	        public string ExpireDate { get; set; }
	
	        /// <summary>
	        /// 1
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 20151129001
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// 2014-10-15 13:20:00
	        /// </summary>
	        [XmlElement("produceDate")]
	        public string ProduceDate { get; set; }
}

	/// <summary>
/// OrderItemsDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("orderItem")]
public class OrderItemsDomain : TopObject
{
	        /// <summary>
	        /// 2
	        /// </summary>
	        [XmlElement("actualQty")]
	        public string ActualQty { get; set; }
	
	        /// <summary>
	        /// 123
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// 批次信息
	        /// </summary>
	        [XmlArray("batchs")]
	        [XmlArrayItem("batch")]
	        public List<BatchsDomain> Batchs { get; set; }
	
	        /// <summary>
	        /// 2015-11-29 13:20:00
	        /// </summary>
	        [XmlElement("dueDate")]
	        public string DueDate { get; set; }
	
	        /// <summary>
	        /// 1
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 69889900002
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 526332459429
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 123
	        /// </summary>
	        [XmlElement("orderItemId")]
	        public string OrderItemId { get; set; }
	
	        /// <summary>
	        /// 415670368
	        /// </summary>
	        [XmlElement("ownUserId")]
	        public string OwnUserId { get; set; }
	
	        /// <summary>
	        /// 2
	        /// </summary>
	        [XmlElement("planQty")]
	        public string PlanQty { get; set; }
	
	        /// <summary>
	        /// 20151129001
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// 2014-10-15 13:20:00
	        /// </summary>
	        [XmlElement("produceDate")]
	        public string ProduceDate { get; set; }
	
	        /// <summary>
	        /// 123123
	        /// </summary>
	        [XmlElement("qrCode")]
	        public string QrCode { get; set; }
	
	        /// <summary>
	        /// 淘系退款单号
	        /// </summary>
	        [XmlElement("refundId")]
	        public string RefundId { get; set; }
	
	        /// <summary>
	        /// 123
	        /// </summary>
	        [XmlElement("sourceOrderCode")]
	        public string SourceOrderCode { get; set; }
	
	        /// <summary>
	        /// 123
	        /// </summary>
	        [XmlElement("subSourceOrderCode")]
	        public string SubSourceOrderCode { get; set; }
}

	/// <summary>
/// AutoReturnInOrderConfirmRequestDomain Data Structure.
/// </summary>
[Serializable]

public class AutoReturnInOrderConfirmRequestDomain : TopObject
{
	        /// <summary>
	        /// 12233344
	        /// </summary>
	        [XmlElement("expressCode")]
	        public string ExpressCode { get; set; }
	
	        /// <summary>
	        /// YUNDA
	        /// </summary>
	        [XmlElement("logisticsCode")]
	        public string LogisticsCode { get; set; }
	
	        /// <summary>
	        /// 顺丰
	        /// </summary>
	        [XmlElement("logisticsName")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// 2018-10-15 13:20:00
	        /// </summary>
	        [XmlElement("orderConfirmTime")]
	        public string OrderConfirmTime { get; set; }
	
	        /// <summary>
	        /// 订单明细行
	        /// </summary>
	        [XmlArray("orderItems")]
	        [XmlArrayItem("orderItem")]
	        public List<OrderItemsDomain> OrderItems { get; set; }
	
	        /// <summary>
	        /// THRK
	        /// </summary>
	        [XmlElement("orderType")]
	        public string OrderType { get; set; }
	
	        /// <summary>
	        /// LP000123123
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 1223334444
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// LP000123123
	        /// </summary>
	        [XmlElement("returnOrderId")]
	        public string ReturnOrderId { get; set; }
	
	        /// <summary>
	        /// 损坏
	        /// </summary>
	        [XmlElement("returnReason")]
	        public string ReturnReason { get; set; }
	
	        /// <summary>
	        /// 销退类型： 0=拒签销退； 1=配拦截销退； 2=上门取退销退； 3=普通退货销退
	        /// </summary>
	        [XmlElement("returnType")]
	        public string ReturnType { get; set; }
	
	        /// <summary>
	        /// 发件人信息
	        /// </summary>
	        [XmlElement("senderInfo")]
	        public SenderInfoDomain SenderInfo { get; set; }
	
	        /// <summary>
	        /// +0800
	        /// </summary>
	        [XmlElement("timeZone")]
	        public string TimeZone { get; set; }
	
	        /// <summary>
	        /// STORE007
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

        #endregion
    }
}
