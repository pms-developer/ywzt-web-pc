using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.auto.entryorder.giftitemcancel
    /// </summary>
    public class TaobaoAutoEntryorderGiftitemcancelRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoAutoEntryorderGiftitemcancelResponse>
    {
        /// <summary>
        /// customerId
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 请求
        /// </summary>
        public string Request { get; set; }

        public ErpBmsOrderGiftCancelRequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.auto.entryorder.giftitemcancel";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
        }

	/// <summary>
/// OrderItemsDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("orderItem")]
public class OrderItemsDomain : TopObject
{
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 菜鸟货品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 交易平台订单编码
	        /// </summary>
	        [XmlElement("sourceOrderCode")]
	        public string SourceOrderCode { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 子交易号
	        /// </summary>
	        [XmlElement("subTradeId")]
	        public string SubTradeId { get; set; }
	
	        /// <summary>
	        /// 交易号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
}

	/// <summary>
/// ErpBmsOrderGiftCancelRequestDomain Data Structure.
/// </summary>
[Serializable]

public class ErpBmsOrderGiftCancelRequestDomain : TopObject
{
	        /// <summary>
	        /// 订单详情
	        /// </summary>
	        [XmlArray("orderItems")]
	        [XmlArrayItem("orderItem")]
	        public List<OrderItemsDomain> OrderItems { get; set; }
	
	        /// <summary>
	        /// 货主ID
	        /// </summary>
	        [XmlElement("ownerUserId")]
	        public string OwnerUserId { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 交易号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
	
	        /// <summary>
	        /// 店铺ID
	        /// </summary>
	        [XmlElement("userId")]
	        public string UserId { get; set; }
}

        #endregion
    }
}
