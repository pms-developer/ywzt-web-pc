using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.icp.order.stockoutordermessagetoerp
    /// </summary>
    public class TaobaoIcpOrderStockoutordermessagetoerpRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoIcpOrderStockoutordermessagetoerpResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 出库单记录集
        /// </summary>
        public string EntryOutOrderlist { get; set; }

        public EntryOutOrderlistDomain EntryOutOrderlist_ { set { this.EntryOutOrderlist = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.icp.order.stockoutordermessagetoerp";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("entryOutOrderlist", this.EntryOutOrderlist);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
        }

	/// <summary>
/// PickerInfoDomain Data Structure.
/// </summary>
[Serializable]

public class PickerInfoDomain : TopObject
{
	        /// <summary>
	        /// 车牌号，string(50)
	        /// </summary>
	        [XmlElement("carNo")]
	        public string CarNo { get; set; }
	
	        /// <summary>
	        /// 公司名称, string (200)
	        /// </summary>
	        [XmlElement("company")]
	        public string Company { get; set; }
	
	        /// <summary>
	        /// 证件号，string(50)
	        /// </summary>
	        [XmlElement("id")]
	        public string Id { get; set; }
	
	        /// <summary>
	        /// 移动电话, string (50)
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 姓名, string (50)
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 固定电话, string (50)
	        /// </summary>
	        [XmlElement("tel")]
	        public string Tel { get; set; }
}

	/// <summary>
/// SenderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SenderInfoDomain : TopObject
{
	        /// <summary>
	        /// 区域, string (50)
	        /// </summary>
	        [XmlElement("senderInfoarea")]
	        public string SenderInfoarea { get; set; }
	
	        /// <summary>
	        /// 城市, string (50) ,
	        /// </summary>
	        [XmlElement("senderInfocity")]
	        public string SenderInfocity { get; set; }
	
	        /// <summary>
	        /// 公司名称, string (200)
	        /// </summary>
	        [XmlElement("senderInfocompany")]
	        public string SenderInfocompany { get; set; }
	
	        /// <summary>
	        /// 国家二字码，string（50）
	        /// </summary>
	        [XmlElement("senderInfocountryCode")]
	        public string SenderInfocountryCode { get; set; }
	
	        /// <summary>
	        /// 详细地址, string (200)
	        /// </summary>
	        [XmlElement("senderInfodetailAddress")]
	        public string SenderInfodetailAddress { get; set; }
	
	        /// <summary>
	        /// 电子邮箱, string (50)
	        /// </summary>
	        [XmlElement("senderInfoemail")]
	        public string SenderInfoemail { get; set; }
	
	        /// <summary>
	        /// 证件号，string(50)
	        /// </summary>
	        [XmlElement("senderInfoid")]
	        public string SenderInfoid { get; set; }
	
	        /// <summary>
	        /// 移动电话, string (50) ,
	        /// </summary>
	        [XmlElement("senderInfomobile")]
	        public string SenderInfomobile { get; set; }
	
	        /// <summary>
	        /// 姓名, string (50) ,
	        /// </summary>
	        [XmlElement("senderInfoname")]
	        public string SenderInfoname { get; set; }
	
	        /// <summary>
	        /// 省份, string (50) ,
	        /// </summary>
	        [XmlElement("senderInfoprovince")]
	        public string SenderInfoprovince { get; set; }
	
	        /// <summary>
	        /// 固定电话, string (50)
	        /// </summary>
	        [XmlElement("senderInfotel")]
	        public string SenderInfotel { get; set; }
	
	        /// <summary>
	        /// 村镇, string (50)
	        /// </summary>
	        [XmlElement("senderInfotown")]
	        public string SenderInfotown { get; set; }
	
	        /// <summary>
	        /// 邮编, string (50)
	        /// </summary>
	        [XmlElement("senderInfozipCode")]
	        public string SenderInfozipCode { get; set; }
}

	/// <summary>
/// ReceiverInfoDomain Data Structure.
/// </summary>
[Serializable]

public class ReceiverInfoDomain : TopObject
{
	        /// <summary>
	        /// 区域, string (50)
	        /// </summary>
	        [XmlElement("receiverInfoarea")]
	        public string ReceiverInfoarea { get; set; }
	
	        /// <summary>
	        /// 城市, string (50) , 必填
	        /// </summary>
	        [XmlElement("receiverInfocity")]
	        public string ReceiverInfocity { get; set; }
	
	        /// <summary>
	        /// 公司名称, string (200)
	        /// </summary>
	        [XmlElement("receiverInfocompany")]
	        public string ReceiverInfocompany { get; set; }
	
	        /// <summary>
	        /// 国家二字码，string（50）
	        /// </summary>
	        [XmlElement("receiverInfocountryCode")]
	        public string ReceiverInfocountryCode { get; set; }
	
	        /// <summary>
	        /// 详细地址, string (200) , 必填
	        /// </summary>
	        [XmlElement("receiverInfodetailAddress")]
	        public string ReceiverInfodetailAddress { get; set; }
	
	        /// <summary>
	        /// 电子邮箱, string (50)
	        /// </summary>
	        [XmlElement("receiverInfoemail")]
	        public string ReceiverInfoemail { get; set; }
	
	        /// <summary>
	        /// 证件号，string(50)
	        /// </summary>
	        [XmlElement("receiverInfoid")]
	        public string ReceiverInfoid { get; set; }
	
	        /// <summary>
	        /// 移动电话, string (50) , 必填
	        /// </summary>
	        [XmlElement("receiverInfomobile")]
	        public string ReceiverInfomobile { get; set; }
	
	        /// <summary>
	        /// 姓名（注：当出库为调拨出库时，这里填写为目标仓库编码）, string (50) , 必填
	        /// </summary>
	        [XmlElement("receiverInfoname")]
	        public string ReceiverInfoname { get; set; }
	
	        /// <summary>
	        /// 省份, string (50) , 必填
	        /// </summary>
	        [XmlElement("receiverInfoprovince")]
	        public string ReceiverInfoprovince { get; set; }
	
	        /// <summary>
	        /// 固定电话, string (50)
	        /// </summary>
	        [XmlElement("receiverInfotel")]
	        public string ReceiverInfotel { get; set; }
	
	        /// <summary>
	        /// 村镇, string (50)
	        /// </summary>
	        [XmlElement("receiverInfotown")]
	        public string ReceiverInfotown { get; set; }
	
	        /// <summary>
	        /// 邮编, string (50)
	        /// </summary>
	        [XmlElement("receiverInfozipCode")]
	        public string ReceiverInfozipCode { get; set; }
}

	/// <summary>
/// DeliveryOrderDomain Data Structure.
/// </summary>
[Serializable]

public class DeliveryOrderDomain : TopObject
{
	        /// <summary>
	        /// 交易订单号, string (50)
	        /// </summary>
	        [XmlElement("buyOrderCode")]
	        public string BuyOrderCode { get; set; }
	
	        /// <summary>
	        /// 出库单创建时间, string (19) , YYYY-MM-DD HH:MM:SS, 必填
	        /// </summary>
	        [XmlElement("createTime")]
	        public string CreateTime { get; set; }
	
	        /// <summary>
	        /// 对应单号
	        /// </summary>
	        [XmlElement("entryInOrderCode")]
	        public string EntryInOrderCode { get; set; }
	
	        /// <summary>
	        /// 物流单号
	        /// </summary>
	        [XmlElement("entryOrderId")]
	        public string EntryOrderId { get; set; }
	
	        /// <summary>
	        /// 外部单号(以前出库单编码)
	        /// </summary>
	        [XmlElement("entryOutOrderCode")]
	        public string EntryOutOrderCode { get; set; }
	
	        /// <summary>
	        /// 出库单货主编码, string (50) ，必填
	        /// </summary>
	        [XmlElement("entryOutownerCode")]
	        public string EntryOutownerCode { get; set; }
	
	        /// <summary>
	        /// 拓展属性数据
	        /// </summary>
	        [XmlElement("extendFields")]
	        public string ExtendFields { get; set; }
	
	        /// <summary>
	        /// 物流公司编码, string (50) , SF=顺丰、EMS=标准快递、EYB=经济快件、ZJS=宅急送、YTO=圆通  、ZTO=中通 (ZTO) 、HTKY=百世汇通、UC=优速、STO=申通、TTKDEX=天天快递  、QFKD=全峰、FAST=快捷、POSTB=邮政小包  、GTO=国通、YUNDA=韵达、JD=京东配送、DD=当当宅配、AMAZON=亚马逊物流、OTHER=其他 ，(只传英文编码)
	        /// </summary>
	        [XmlElement("logisticsCode")]
	        public string LogisticsCode { get; set; }
	
	        /// <summary>
	        /// 物流公司名称（包括干线物流公司等）, string (200)
	        /// </summary>
	        [XmlElement("logisticsName")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// 出库单类型, string (50) , 必填, PTCK=普通出库单 ，B2BCK=B2B出库
	        /// </summary>
	        [XmlElement("orderType")]
	        public string OrderType { get; set; }
	
	        /// <summary>
	        /// 外部业务编码
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 提货人信息
	        /// </summary>
	        [XmlElement("pickerInfo")]
	        public PickerInfoDomain PickerInfo { get; set; }
	
	        /// <summary>
	        /// 收货人信息
	        /// </summary>
	        [XmlElement("receiverInfo")]
	        public ReceiverInfoDomain ReceiverInfo { get; set; }
	
	        /// <summary>
	        /// 备注, string (500)
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 要求出库时间, string (10) , YYYY-MM-DD
	        /// </summary>
	        [XmlElement("scheduleDate")]
	        public string ScheduleDate { get; set; }
	
	        /// <summary>
	        /// 送货人信息
	        /// </summary>
	        [XmlElement("senderInfo")]
	        public SenderInfoDomain SenderInfo { get; set; }
	
	        /// <summary>
	        /// 出库单状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 供应商编码 string (50)
	        /// </summary>
	        [XmlElement("supplierCode")]
	        public string SupplierCode { get; set; }
	
	        /// <summary>
	        /// 供应商名称 string (200)
	        /// </summary>
	        [XmlElement("supplierName")]
	        public string SupplierName { get; set; }
	
	        /// <summary>
	        /// 提货方式（到仓自提，快递，干线物流）
	        /// </summary>
	        [XmlElement("transportMode")]
	        public string TransportMode { get; set; }
	
	        /// <summary>
	        /// 仓库编码, string (50)，必填 ，统仓统配等无需ERP指定仓储编码的情况填OTHER
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

	/// <summary>
/// OrderLinesDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("orderLine")]
public class OrderLinesDomain : TopObject
{
	        /// <summary>
	        /// （无）批次编码
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// （无）商品过期日期
	        /// </summary>
	        [XmlElement("expireDate")]
	        public string ExpireDate { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("feature")]
	        public string Feature { get; set; }
	
	        /// <summary>
	        /// 库存类型(ZP=正品、CC=残次、JS=机损、XS=箱损、默认为ZP、(收到商品总数=正品数+残品数+机损数+箱损数))
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 商品编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 仓储系统商品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 单据行号
	        /// </summary>
	        [XmlElement("orderLineNo")]
	        public string OrderLineNo { get; set; }
	
	        /// <summary>
	        /// 消息ID用于去重当单据需要分批次发送时使用)
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 应出数量
	        /// </summary>
	        [XmlElement("planQty")]
	        public string PlanQty { get; set; }
	
	        /// <summary>
	        /// （无）生产批号, string (50)
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// （无）商品生产日期,YYYY-MM-DD
	        /// </summary>
	        [XmlElement("productDate")]
	        public string ProductDate { get; set; }
}

	/// <summary>
/// EntryOutOrderlistDomain Data Structure.
/// </summary>
[Serializable]

public class EntryOutOrderlistDomain : TopObject
{
	        /// <summary>
	        /// 出库单记录
	        /// </summary>
	        [XmlElement("deliveryOrder")]
	        public DeliveryOrderDomain DeliveryOrder { get; set; }
	
	        /// <summary>
	        /// 出库单明细信息
	        /// </summary>
	        [XmlArray("orderLines")]
	        [XmlArrayItem("orderLine")]
	        public List<OrderLinesDomain> OrderLines { get; set; }
}

        #endregion
    }
}
