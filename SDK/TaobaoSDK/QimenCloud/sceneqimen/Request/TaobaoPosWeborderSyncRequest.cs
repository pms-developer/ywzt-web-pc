using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.weborder.sync
    /// </summary>
    public class TaobaoPosWeborderSyncRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosWeborderSyncResponse>
    {
        /// <summary>
        /// 街道
        /// </summary>
        public string Addres { get; set; }

        /// <summary>
        /// 营业日期
        /// </summary>
        public string BillTime { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 制单人
        /// </summary>
        public string Creater { get; set; }

        /// <summary>
        /// 制单日期
        /// </summary>
        public string CreationDate { get; set; }

        /// <summary>
        /// 路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 折扣
        /// </summary>
        public string Discount { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// '顾客留言
        /// </summary>
        public string Gkly { get; set; }

        /// <summary>
        /// 商品明细
        /// </summary>
        public string Item { get; set; }

        public List<ItemDomain> Item_ { set { this.Item = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// '客服备注
        /// </summary>
        public string Kfbz { get; set; }

        /// <summary>
        /// '来源渠道代码
        /// </summary>
        public string LyorgDm { get; set; }

        /// <summary>
        /// '来源渠道名称
        /// </summary>
        public string LyorgMc { get; set; }

        /// <summary>
        /// 来源平台
        /// </summary>
        public string Lypt { get; set; }

        /// <summary>
        /// '来源店铺代码
        /// </summary>
        public string LyzdDm { get; set; }

        /// <summary>
        /// '来源店铺名称
        /// </summary>
        public string LyzdMc { get; set; }

        /// <summary>
        /// 标准价格
        /// </summary>
        public string Money { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>
        public string OrderBillCode { get; set; }

        /// <summary>
        /// 原单据号
        /// </summary>
        public string OrderWebCod { get; set; }

        /// <summary>
        /// 支付方式
        /// </summary>
        public string Paymethod { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 结算代码
        /// </summary>
        public string PosOuterCode { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public Nullable<long> Quantity { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public string RealMoney { get; set; }

        /// <summary>
        /// 店员代码
        /// </summary>
        public string SalerEmployeeNo { get; set; }

        /// <summary>
        /// 快递地址
        /// </summary>
        public string ShippingAddress { get; set; }

        /// <summary>
        /// 快递代码
        /// </summary>
        public string ShippingCode { get; set; }

        /// <summary>
        /// 快递单号
        /// </summary>
        public string ShippingSn { get; set; }

        /// <summary>
        /// 门店代码
        /// </summary>
        public string ShopCode { get; set; }

        /// <summary>
        /// 来源系统
        /// </summary>
        public string System { get; set; }

        /// <summary>
        /// 退单
        /// </summary>
        public string ThAct { get; set; }

        /// <summary>
        /// '下单门店代码
        /// </summary>
        public string XdzdDm { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.weborder.sync";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("addres", this.Addres);
            parameters.Add("billTime", this.BillTime);
            parameters.Add("city", this.City);
            parameters.Add("creater", this.Creater);
            parameters.Add("creationDate", this.CreationDate);
            parameters.Add("customerid", this.Customerid);
            parameters.Add("discount", this.Discount);
            parameters.Add("district", this.District);
            parameters.Add("extend_props", this.ExtendProps);
            parameters.Add("gkly", this.Gkly);
            parameters.Add("item", this.Item);
            parameters.Add("kfbz", this.Kfbz);
            parameters.Add("lyorg_dm", this.LyorgDm);
            parameters.Add("lyorg_mc", this.LyorgMc);
            parameters.Add("lypt", this.Lypt);
            parameters.Add("lyzd_dm", this.LyzdDm);
            parameters.Add("lyzd_mc", this.LyzdMc);
            parameters.Add("money", this.Money);
            parameters.Add("name", this.Name);
            parameters.Add("note", this.Note);
            parameters.Add("orderBillCode", this.OrderBillCode);
            parameters.Add("orderWebCod", this.OrderWebCod);
            parameters.Add("paymethod", this.Paymethod);
            parameters.Add("phone", this.Phone);
            parameters.Add("pos_outer_code", this.PosOuterCode);
            parameters.Add("province", this.Province);
            parameters.Add("quantity", this.Quantity);
            parameters.Add("realMoney", this.RealMoney);
            parameters.Add("salerEmployeeNo", this.SalerEmployeeNo);
            parameters.Add("shippingAddress", this.ShippingAddress);
            parameters.Add("shippingCode", this.ShippingCode);
            parameters.Add("shippingSn", this.ShippingSn);
            parameters.Add("shopCode", this.ShopCode);
            parameters.Add("system", this.System);
            parameters.Add("th_act", this.ThAct);
            parameters.Add("xdzd_dm", this.XdzdDm);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("billTime", this.BillTime);
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateRequired("discount", this.Discount);
            RequestValidator.ValidateRequired("gkly", this.Gkly);
            RequestValidator.ValidateObjectMaxListSize("item", this.Item, 999);
            RequestValidator.ValidateRequired("kfbz", this.Kfbz);
            RequestValidator.ValidateRequired("lyorg_dm", this.LyorgDm);
            RequestValidator.ValidateRequired("lyorg_mc", this.LyorgMc);
            RequestValidator.ValidateRequired("lypt", this.Lypt);
            RequestValidator.ValidateRequired("lyzd_dm", this.LyzdDm);
            RequestValidator.ValidateRequired("lyzd_mc", this.LyzdMc);
            RequestValidator.ValidateRequired("money", this.Money);
            RequestValidator.ValidateRequired("name", this.Name);
            RequestValidator.ValidateRequired("orderBillCode", this.OrderBillCode);
            RequestValidator.ValidateRequired("orderWebCod", this.OrderWebCod);
            RequestValidator.ValidateRequired("paymethod", this.Paymethod);
            RequestValidator.ValidateRequired("phone", this.Phone);
            RequestValidator.ValidateRequired("pos_outer_code", this.PosOuterCode);
            RequestValidator.ValidateRequired("quantity", this.Quantity);
            RequestValidator.ValidateRequired("realMoney", this.RealMoney);
            RequestValidator.ValidateRequired("shopCode", this.ShopCode);
            RequestValidator.ValidateRequired("system", this.System);
            RequestValidator.ValidateRequired("th_act", this.ThAct);
            RequestValidator.ValidateRequired("xdzd_dm", this.XdzdDm);
        }

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("items")]
public class ItemDomain : TopObject
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 颜色代码
	        /// </summary>
	        [XmlElement("colorCode")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 商品代码
	        /// </summary>
	        [XmlElement("goodsCode")]
	        public string GoodsCode { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public string Quantity { get; set; }
	
	        /// <summary>
	        /// 参考金额
	        /// </summary>
	        [XmlElement("referenceAmount")]
	        public string ReferenceAmount { get; set; }
	
	        /// <summary>
	        /// 参考价
	        /// </summary>
	        [XmlElement("referencePrice")]
	        public string ReferencePrice { get; set; }
	
	        /// <summary>
	        /// 尺码代码
	        /// </summary>
	        [XmlElement("sizeCode")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 商品条码
	        /// </summary>
	        [XmlElement("skuCode")]
	        public string SkuCode { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

        #endregion
    }
}
