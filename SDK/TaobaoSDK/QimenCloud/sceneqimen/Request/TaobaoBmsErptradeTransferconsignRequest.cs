using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.bms.erptrade.transferconsign
    /// </summary>
    public class TaobaoBmsErptradeTransferconsignRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoBmsErptradeTransferconsignResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 请求体
        /// </summary>
        public string Request { get; set; }

        public BmsErptradeTransferConsignRequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.bms.erptrade.transferconsign";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// OrderItemsDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("orderItem")]
public class OrderItemsDomain : TopObject
{
	        /// <summary>
	        /// SC:商家仓；CN：菜鸟仓
	        /// </summary>
	        [XmlElement("consignType")]
	        public string ConsignType { get; set; }
	
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 明细类型( 1.交易赠品 ;2.促销赠品; 3.手工录入赠品 ;4.交易正品; 5.手工录入正品)
	        /// </summary>
	        [XmlElement("itemGiftType")]
	        public string ItemGiftType { get; set; }
	
	        /// <summary>
	        /// 菜鸟货品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 应发数量
	        /// </summary>
	        [XmlElement("needConsignNum")]
	        public string NeedConsignNum { get; set; }
	
	        /// <summary>
	        /// 商品数字编号
	        /// </summary>
	        [XmlElement("numIid")]
	        public string NumIid { get; set; }
	
	        /// <summary>
	        /// skuId
	        /// </summary>
	        [XmlElement("skuId")]
	        public string SkuId { get; set; }
	
	        /// <summary>
	        /// 交易平台订单编码
	        /// </summary>
	        [XmlElement("sourceOrderCode")]
	        public string SourceOrderCode { get; set; }
	
	        /// <summary>
	        /// 仓库编码
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 交易平台子订单编码
	        /// </summary>
	        [XmlElement("subTradeId")]
	        public string SubTradeId { get; set; }
	
	        /// <summary>
	        /// 交易单号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
}

	/// <summary>
/// BmsErptradeTransferConsignRequestDomain Data Structure.
/// </summary>
[Serializable]

public class BmsErptradeTransferConsignRequestDomain : TopObject
{
	        /// <summary>
	        /// 操作类型(1、商家仓转菜鸟，2、菜鸟仓转商家仓)
	        /// </summary>
	        [XmlElement("operateType")]
	        public string OperateType { get; set; }
	
	        /// <summary>
	        /// 明细信息
	        /// </summary>
	        [XmlArray("orderItems")]
	        [XmlArrayItem("orderItem")]
	        public List<OrderItemsDomain> OrderItems { get; set; }
	
	        /// <summary>
	        /// 货主ID
	        /// </summary>
	        [XmlElement("ownerUserId")]
	        public string OwnerUserId { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 交易号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
	
	        /// <summary>
	        /// 店铺ID
	        /// </summary>
	        [XmlElement("userId")]
	        public string UserId { get; set; }
}

        #endregion
    }
}
