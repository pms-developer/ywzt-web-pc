using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.entryorder.add
    /// </summary>
    public class TaobaoErpEntryorderAddRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpEntryorderAddResponse>
    {
        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 合计金额
        /// </summary>
        public string GoodsTotal { get; set; }

        /// <summary>
        /// 货品主键类型
        /// </summary>
        public string Keytype { get; set; }

        /// <summary>
        /// 货运费用
        /// </summary>
        public string LogisticFee { get; set; }

        /// <summary>
        /// 货运单号
        /// </summary>
        public string LogisticNo { get; set; }

        /// <summary>
        /// 单据类型（0其他入库）
        /// </summary>
        public string Operationtype { get; set; }

        /// <summary>
        /// 登记人
        /// </summary>
        public string OperatorPers { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        public string OrderInfo { get; set; }

        public OrderInfoDomain OrderInfo_ { set { this.OrderInfo = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 其他费用
        /// </summary>
        public string OtherFee { get; set; }

        /// <summary>
        /// 供应商id
        /// </summary>
        public string Providerid { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 入库单状态(待审核;已审核;被取消)
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 入库单号
        /// </summary>
        public string StockinNo { get; set; }

        /// <summary>
        /// 入库原因
        /// </summary>
        public string Thecause { get; set; }

        /// <summary>
        /// 仓库id
        /// </summary>
        public string WarehouseId { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.entryorder.add";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("goods_total", this.GoodsTotal);
            parameters.Add("keytype", this.Keytype);
            parameters.Add("logistic_fee", this.LogisticFee);
            parameters.Add("logistic_no", this.LogisticNo);
            parameters.Add("operationtype", this.Operationtype);
            parameters.Add("operator_pers", this.OperatorPers);
            parameters.Add("order_info", this.OrderInfo);
            parameters.Add("other_fee", this.OtherFee);
            parameters.Add("providerid", this.Providerid);
            parameters.Add("remark", this.Remark);
            parameters.Add("status", this.Status);
            parameters.Add("stockin_no", this.StockinNo);
            parameters.Add("thecause", this.Thecause);
            parameters.Add("warehouse_id", this.WarehouseId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// OrderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class OrderInfoDomain : TopObject
{
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_code")]
	        public string GoodsCode { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("sell_count")]
	        public string SellCount { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
}

        #endregion
    }
}
