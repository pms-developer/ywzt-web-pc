using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.crm.ext.refund.get
    /// </summary>
    public class TaobaoCrmExtRefundGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoCrmExtRefundGetResponse>
    {
        /// <summary>
        /// customerid
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 退单列表修改结束时间
        /// </summary>
        public string EndModified { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 页数
        /// </summary>
        public string PageNo { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public string PageSize { get; set; }

        /// <summary>
        /// 商店编码
        /// </summary>
        public string SdCode { get; set; }

        /// <summary>
        /// 退单列表修改开始时间
        /// </summary>
        public string StartModified { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.crm.ext.refund.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("endModified", this.EndModified);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("pageNo", this.PageNo);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("sd_code", this.SdCode);
            parameters.Add("startModified", this.StartModified);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 255);
            RequestValidator.ValidateRequired("endModified", this.EndModified);
            RequestValidator.ValidateMaxLength("endModified", this.EndModified, 64);
            RequestValidator.ValidateRequired("pageNo", this.PageNo);
            RequestValidator.ValidateMaxLength("pageNo", this.PageNo, 11);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateMaxLength("pageSize", this.PageSize, 11);
            RequestValidator.ValidateMaxLength("sd_code", this.SdCode, 64);
            RequestValidator.ValidateRequired("startModified", this.StartModified);
            RequestValidator.ValidateMaxLength("startModified", this.StartModified, 64);
        }

        #endregion
    }
}
