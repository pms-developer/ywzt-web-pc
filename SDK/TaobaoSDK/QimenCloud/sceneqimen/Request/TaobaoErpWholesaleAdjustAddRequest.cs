using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.wholesale.adjust.add
    /// </summary>
    public class TaobaoErpWholesaleAdjustAddRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpWholesaleAdjustAddResponse>
    {
        /// <summary>
        /// 制单人
        /// </summary>
        public string CreateEmp { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 仓库代码
        /// </summary>
        public string WareHouseCode { get; set; }

        /// <summary>
        /// 渠道代码
        /// </summary>
        public string ChannelCode { get; set; }

        /// <summary>
        /// 下单日期
        /// </summary>
        public string Created { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 业务类型(1 期初调整,2 盈亏调整,3差异调整,4 组装拆卸))
        /// </summary>
        public string OrderType { get; set; }

        /// <summary>
        /// sku详情
        /// </summary>
        public string Sku { get; set; }

        public List<SkuDomain> Sku_ { set { this.Sku = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 库区代码
        /// </summary>
        public string WhareaTypeCode { get; set; }

        /// <summary>
        /// 库存调整单号
        /// </summary>
        public string Wholesaleadjust { get; set; }

        /// <summary>
        /// 制单日期
        /// </summary>
        public string ZdTime { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.wholesale.adjust.add";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("CreateEmp", this.CreateEmp);
            parameters.Add("Remark", this.Remark);
            parameters.Add("WareHouseCode", this.WareHouseCode);
            parameters.Add("channelCode", this.ChannelCode);
            parameters.Add("created", this.Created);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("orderType", this.OrderType);
            parameters.Add("sku", this.Sku);
            parameters.Add("whareaTypeCode", this.WhareaTypeCode);
            parameters.Add("wholesaleadjust", this.Wholesaleadjust);
            parameters.Add("zdTime", this.ZdTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("sku", this.Sku, 3000);
        }

	/// <summary>
/// SkuDomain Data Structure.
/// </summary>
[Serializable]

public class SkuDomain : TopObject
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 颜色编码
	        /// </summary>
	        [XmlElement("colorCode")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 商品编码（上游系统）
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("number")]
	        public string Number { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 尺寸编号
	        /// </summary>
	        [XmlElement("sizeCode")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// Sku编码
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku_ { get; set; }
	
	        /// <summary>
	        /// 标准价
	        /// </summary>
	        [XmlElement("stdprice")]
	        public string Stdprice { get; set; }
}

        #endregion
    }
}
