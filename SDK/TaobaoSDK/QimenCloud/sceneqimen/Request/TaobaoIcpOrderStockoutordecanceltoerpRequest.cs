using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.icp.order.stockoutordecanceltoerp
    /// </summary>
    public class TaobaoIcpOrderStockoutordecanceltoerpRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoIcpOrderStockoutordecanceltoerpResponse>
    {
        /// <summary>
        /// 出库单所属货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 出入库单信息（一单）
        /// </summary>
        public string EntryOrderlist { get; set; }

        public EntryOrderlistDomain EntryOrderlist_ { set { this.EntryOrderlist = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.icp.order.stockoutordecanceltoerp";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("entryOrderlist", this.EntryOrderlist);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
        }

	/// <summary>
/// EntryOrderDomain Data Structure.
/// </summary>
[Serializable]

public class EntryOrderDomain : TopObject
{
	        /// <summary>
	        /// 物流单号
	        /// </summary>
	        [XmlElement("entryOrderId")]
	        public string EntryOrderId { get; set; }
	
	        /// <summary>
	        /// 外部erp单号（以前出库单编码）
	        /// </summary>
	        [XmlElement("entryOutOrderCode")]
	        public string EntryOutOrderCode { get; set; }
	
	        /// <summary>
	        /// 单据类型（PTCK=普通出库单，DBCK=调拨出库，B2BRK=B2B入库，B2BCK=B2B出库，CGRK=采购入库，DBRK=调拨入库，QTRK=其他入库）
	        /// </summary>
	        [XmlElement("entryOutOrderType")]
	        public string EntryOutOrderType { get; set; }
	
	        /// <summary>
	        /// 拓展属性数据
	        /// </summary>
	        [XmlElement("extendFields")]
	        public string ExtendFields { get; set; }
	
	        /// <summary>
	        /// 货主编码
	        /// </summary>
	        [XmlElement("ownerInCode")]
	        public string OwnerInCode { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
}

	/// <summary>
/// EntryOrderlistDomain Data Structure.
/// </summary>
[Serializable]

public class EntryOrderlistDomain : TopObject
{
	        /// <summary>
	        /// 出入库单信息（一单）
	        /// </summary>
	        [XmlElement("entryOrder")]
	        public EntryOrderDomain EntryOrder { get; set; }
}

        #endregion
    }
}
