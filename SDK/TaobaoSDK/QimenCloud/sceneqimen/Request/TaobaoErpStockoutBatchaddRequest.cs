using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.stockout.batchadd
    /// </summary>
    public class TaobaoErpStockoutBatchaddRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpStockoutBatchaddResponse>
    {
        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 货品主键类型(0:货品编码;1:条码+附加码)
        /// </summary>
        public string Keytype { get; set; }

        /// <summary>
        /// 出库类型(0:其它出库)
        /// </summary>
        public string Operationtype { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public string OperatorPers { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        public string OrderInfo { get; set; }

        public List<OrderInfoDomain> OrderInfo_ { set { this.OrderInfo = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 其他费用
        /// </summary>
        public string OtherFee { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 出库单号
        /// </summary>
        public string StockoutNo { get; set; }

        /// <summary>
        /// 出库原因
        /// </summary>
        public string Thecause { get; set; }

        /// <summary>
        /// 仓库id
        /// </summary>
        public string WarehouseId { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.stockout.batchadd";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("keytype", this.Keytype);
            parameters.Add("operationtype", this.Operationtype);
            parameters.Add("operator_pers", this.OperatorPers);
            parameters.Add("order_info", this.OrderInfo);
            parameters.Add("other_fee", this.OtherFee);
            parameters.Add("remark", this.Remark);
            parameters.Add("stockout_no", this.StockoutNo);
            parameters.Add("thecause", this.Thecause);
            parameters.Add("warehouse_id", this.WarehouseId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("order_info", this.OrderInfo, 999);
        }

	/// <summary>
/// OrderInfoDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("orderinfo")]
public class OrderInfoDomain : TopObject
{
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_code")]
	        public string GoodsCode { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("sell_count")]
	        public string SellCount { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
}

        #endregion
    }
}
