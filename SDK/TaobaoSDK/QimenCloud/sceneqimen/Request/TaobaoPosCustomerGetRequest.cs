using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.customer.get
    /// </summary>
    public class TaobaoPosCustomerGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosCustomerGetResponse>
    {
        /// <summary>
        /// 顾客编码
        /// </summary>
        public string CustomerCode { get; set; }

        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 最后修改的结束时间
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// 当前页
        /// </summary>
        public Nullable<long> Page { get; set; }

        /// <summary>
        /// 每页条数
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string StartTime { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.customer.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerCode", this.CustomerCode);
            parameters.Add("customerid", this.Customerid);
            parameters.Add("endTime", this.EndTime);
            parameters.Add("page", this.Page);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("startTime", this.StartTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxLength("customerCode", this.CustomerCode, 50);
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateMaxLength("endTime", this.EndTime, 50);
            RequestValidator.ValidateRequired("page", this.Page);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateMaxLength("startTime", this.StartTime, 50);
        }

        #endregion
    }
}
