using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.crm.qd.get
    /// </summary>
    public class TaobaoCrmQdGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoCrmQdGetResponse>
    {
        /// <summary>
        /// 路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 查询内结束时间： 2000-11-3023:59:59
        /// </summary>
        public string EndModified { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 页码: 取值范围:大于零的整数; 默认值:1
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 页码: 取值范围:大于零的整数; 默认值:1
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 渠道代码
        /// </summary>
        public string Qddm { get; set; }

        /// <summary>
        /// 查询起始时间： 2011-09-01 00:00:00
        /// </summary>
        public string StartModified { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.crm.qd.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("endModified", this.EndModified);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("pageNo", this.PageNo);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("qddm", this.Qddm);
            parameters.Add("startModified", this.StartModified);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 64);
            RequestValidator.ValidateRequired("endModified", this.EndModified);
            RequestValidator.ValidateMaxLength("endModified", this.EndModified, 64);
            RequestValidator.ValidateRequired("pageNo", this.PageNo);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateMaxLength("qddm", this.Qddm, 64);
            RequestValidator.ValidateRequired("startModified", this.StartModified);
            RequestValidator.ValidateMaxLength("startModified", this.StartModified, 64);
        }

        #endregion
    }
}
