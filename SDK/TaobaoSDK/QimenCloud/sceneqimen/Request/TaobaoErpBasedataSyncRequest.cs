using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.basedata.sync
    /// </summary>
    public class TaobaoErpBasedataSyncRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpBasedataSyncResponse>
    {
        /// <summary>
        /// 基础数据代码（多个基础数据 分号“;” 分开）
        /// </summary>
        public string BasedataID { get; set; }

        /// <summary>
        /// 基础数据类别（1-仓库;2-库区;3-库位;4-计量单位;5-币别;6-产品分类;7-组织;8-员工）
        /// </summary>
        public string BasedataType { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 每页条数(不超过100)
        /// </summary>
        public string PageSize { get; set; }

        /// <summary>
        /// 当前页从1开始
        /// </summary>
        public string PageNo { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string StartDate { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.basedata.sync";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("basedataID", this.BasedataID);
            parameters.Add("basedataType", this.BasedataType);
            parameters.Add("endDate", this.EndDate);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("startDate", this.StartDate);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("basedataType", this.BasedataType);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateRequired("page_no", this.PageNo);
        }

        #endregion
    }
}
