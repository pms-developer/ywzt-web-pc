using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.stockout.report
    /// </summary>
    public class TaobaoErpStockoutReportRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpStockoutReportResponse>
    {
        /// <summary>
        /// 详情
        /// </summary>
        public string Bill { get; set; }

        public BillDomain Bill_ { set { this.Bill = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.stockout.report";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("bill", this.Bill);
            parameters.Add("extendProps", this.ExtendProps);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// BillDomain Data Structure.
/// </summary>
[Serializable]

public class BillDomain : TopObject
{
	        /// <summary>
	        /// OMS出库单号
	        /// </summary>
	        [XmlElement("billCode")]
	        public string BillCode { get; set; }
	
	        /// <summary>
	        /// 单据类型
	        /// </summary>
	        [XmlElement("billType")]
	        public string BillType { get; set; }
	
	        /// <summary>
	        /// 操作码
	        /// </summary>
	        [XmlElement("operateType")]
	        public string OperateType { get; set; }
	
	        /// <summary>
	        /// ERP出库单号
	        /// </summary>
	        [XmlElement("sourceCode")]
	        public string SourceCode { get; set; }
}

        #endregion
    }
}
