using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: alibaba.ascp.huawei.inv.change
    /// </summary>
    public class AlibabaAscpHuaweiInvChangeRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.AlibabaAscpHuaweiInvChangeResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 请求内容根节点
        /// </summary>
        public string Request { get; set; }

        public RequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "alibaba.ascp.huawei.inv.change";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
            RequestValidator.ValidateRequired("request", this.Request);
        }

	/// <summary>
/// OrderItemDomain Data Structure.
/// </summary>
[Serializable]

public class OrderItemDomain : TopObject
{
	        /// <summary>
	        /// B-PART
	        /// </summary>
	        [XmlElement("bpcode")]
	        public string Bpcode { get; set; }
	
	        /// <summary>
	        /// 库存增减类型：1增、2减
	        /// </summary>
	        [XmlElement("invChangeType")]
	        public Nullable<long> InvChangeType { get; set; }
	
	        /// <summary>
	        /// 库存类型：1 可销售库存 (正品) 101 类型用来定义残次品 201 冻结类型库存 301 在途库存
	        /// </summary>
	        [XmlElement("invType")]
	        public Nullable<long> InvType { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 货品id
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("quantity")]
	        public Nullable<long> Quantity { get; set; }
}

	/// <summary>
/// OrderItemsDomain Data Structure.
/// </summary>
[Serializable]

public class OrderItemsDomain : TopObject
{
	        /// <summary>
	        /// 订单明细
	        /// </summary>
	        [XmlArray("orderItem")]
	        [XmlArrayItem("order_item")]
	        public List<OrderItemDomain> OrderItem { get; set; }
}

	/// <summary>
/// RequestDomain Data Structure.
/// </summary>
[Serializable]

public class RequestDomain : TopObject
{
	        /// <summary>
	        /// 订单明细
	        /// </summary>
	        [XmlElement("orderItems")]
	        public OrderItemsDomain OrderItems { get; set; }
	
	        /// <summary>
	        /// 类型：1.入库单、2.出库单、3.盘点单、4.调整单
	        /// </summary>
	        [XmlElement("orderType")]
	        public Nullable<long> OrderType { get; set; }
	
	        /// <summary>
	        /// 仓库code
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 仓库作业单号(异常错误查询条件)
	        /// </summary>
	        [XmlElement("storeOrderCode")]
	        public string StoreOrderCode { get; set; }
}

        #endregion
    }
}
