using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.wholesale.order.add
    /// </summary>
    public class TaobaoErpWholesaleOrderAddRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpWholesaleOrderAddResponse>
    {
        /// <summary>
        /// 制单人
        /// </summary>
        public string CreateEmp { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 仓库代码
        /// </summary>
        public string WareHouseCode { get; set; }

        /// <summary>
        /// 收货地址（省市区之间用空格)
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 客户代码
        /// </summary>
        public string CustomerCode { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 物流公司大写代码
        /// </summary>
        public string Logistics { get; set; }

        /// <summary>
        /// 收货人
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 收货人手机
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// sku详情
        /// </summary>
        public string Sku { get; set; }

        public List<SkuDomain> Sku_ { set { this.Sku = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 批发通知单号
        /// </summary>
        public string WholesaleorderId { get; set; }

        /// <summary>
        /// 制单日期
        /// </summary>
        public string ZdTime { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.wholesale.order.add";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("CreateEmp", this.CreateEmp);
            parameters.Add("CreateTime", this.CreateTime);
            parameters.Add("Remark", this.Remark);
            parameters.Add("WareHouseCode", this.WareHouseCode);
            parameters.Add("address", this.Address);
            parameters.Add("customerCode", this.CustomerCode);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("logistics", this.Logistics);
            parameters.Add("name", this.Name);
            parameters.Add("phone", this.Phone);
            parameters.Add("sku", this.Sku);
            parameters.Add("wholesaleorder_id", this.WholesaleorderId);
            parameters.Add("zdTime", this.ZdTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("sku", this.Sku, 3000);
        }

	/// <summary>
/// SkuDomain Data Structure.
/// </summary>
[Serializable]

public class SkuDomain : TopObject
{
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("number")]
	        public string Number { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// Sku编码
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku_ { get; set; }
}

        #endregion
    }
}
