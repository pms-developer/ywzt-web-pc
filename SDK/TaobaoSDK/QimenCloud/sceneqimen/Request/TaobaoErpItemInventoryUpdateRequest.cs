using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.item.inventory.update
    /// </summary>
    public class TaobaoErpItemInventoryUpdateRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpItemInventoryUpdateResponse>
    {
        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        public string Items { get; set; }

        public StructDomain Items_ { set { this.Items = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 仓库代码
        /// </summary>
        public string WarehouseCode { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.item.inventory.update";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("items", this.Items);
            parameters.Add("warehouseCode", this.WarehouseCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("warehouseCode", this.WarehouseCode);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 颜色编号
	        /// </summary>
	        [XmlElement("ColorCode")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 尺码代码
	        /// </summary>
	        [XmlElement("SizeCode")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 商品代码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 库存数量
	        /// </summary>
	        [XmlElement("number")]
	        public Nullable<long> Number { get; set; }
	
	        /// <summary>
	        /// 商品SKU编号
	        /// </summary>
	        [XmlElement("skuCode")]
	        public Nullable<long> SkuCode { get; set; }
}

        #endregion
    }
}
