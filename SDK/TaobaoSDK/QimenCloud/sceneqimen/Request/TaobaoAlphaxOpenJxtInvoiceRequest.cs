using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.alphax.open.jxt.invoice
    /// </summary>
    public class TaobaoAlphaxOpenJxtInvoiceRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoAlphaxOpenJxtInvoiceResponse>
    {
        /// <summary>
        /// 公司抬头
        /// </summary>
        public string CompanyTitle { get; set; }

        /// <summary>
        /// api新增字段，主要用于扩展参数，例如增值税扩展字段（registered_address 注册地址、registered_phone 注册电话、bank 开户行、账户 ）
        /// </summary>
        public string ExtendArg { get; set; }

        /// <summary>
        /// 发票属性(0:公司；1：个人)
        /// </summary>
        public Nullable<long> InvoiceAttr { get; set; }

        /// <summary>
        /// 发票形态 （1:电子发票;   2：纸质发票)
        /// </summary>
        public Nullable<long> InvoiceKind { get; set; }

        /// <summary>
        /// 发票类型（1:普通发票；2：增值税专用发票）
        /// </summary>
        public Nullable<long> InvoiceType { get; set; }

        /// <summary>
        /// 订单id
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 卖家主账号id
        /// </summary>
        public string SellerId { get; set; }

        /// <summary>
        /// 卖家名称
        /// </summary>
        public string SellerNick { get; set; }

        /// <summary>
        /// 税号
        /// </summary>
        public string TaxNo { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.alphax.open.jxt.invoice";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("company_title", this.CompanyTitle);
            parameters.Add("extend_arg", this.ExtendArg);
            parameters.Add("invoice_attr", this.InvoiceAttr);
            parameters.Add("invoice_kind", this.InvoiceKind);
            parameters.Add("invoice_type", this.InvoiceType);
            parameters.Add("order_id", this.OrderId);
            parameters.Add("seller_id", this.SellerId);
            parameters.Add("seller_nick", this.SellerNick);
            parameters.Add("tax_no", this.TaxNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("invoice_attr", this.InvoiceAttr);
            RequestValidator.ValidateRequired("invoice_kind", this.InvoiceKind);
            RequestValidator.ValidateRequired("invoice_type", this.InvoiceType);
            RequestValidator.ValidateRequired("order_id", this.OrderId);
            RequestValidator.ValidateRequired("seller_id", this.SellerId);
            RequestValidator.ValidateRequired("seller_nick", this.SellerNick);
        }

        #endregion
    }
}
