using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.order.update
    /// </summary>
    public class TaobaoErpOrderUpdateRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpOrderUpdateResponse>
    {
        /// <summary>
        /// 手续费
        /// </summary>
        public string CodFee { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        public string Items { get; set; }

        public ItemsDomain Items_ { set { this.Items = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 订单应付款
        /// </summary>
        public string OrderAmount { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderSn { get; set; }

        /// <summary>
        /// 订单已付款
        /// </summary>
        public string Payment { get; set; }

        /// <summary>
        /// 快递费
        /// </summary>
        public string ShippingFee { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.order.update";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("cod_fee", this.CodFee);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("items", this.Items);
            parameters.Add("order_amount", this.OrderAmount);
            parameters.Add("order_sn", this.OrderSn);
            parameters.Add("payment", this.Payment);
            parameters.Add("shipping_fee", this.ShippingFee);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateMaxLength("cod_fee", this.CodFee, 21);
            RequestValidator.ValidateMaxLength("order_amount", this.OrderAmount, 21);
            RequestValidator.ValidateMaxLength("order_sn", this.OrderSn, 64);
            RequestValidator.ValidateMaxLength("payment", this.Payment, 21);
            RequestValidator.ValidateMaxLength("shipping_fee", this.ShippingFee, 21);
        }

	/// <summary>
/// ItemsDomain Data Structure.
/// </summary>
[Serializable]

public class ItemsDomain : TopObject
{
	        /// <summary>
	        /// 颜色代码
	        /// </summary>
	        [XmlElement("color_code")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 平台交易号（外部系统对接此字段为准）
	        /// </summary>
	        [XmlElement("deal_code")]
	        public string DealCode { get; set; }
	
	        /// <summary>
	        /// 是否礼品（0:正常商品;1:礼品;）
	        /// </summary>
	        [XmlElement("is_gift")]
	        public Nullable<long> IsGift { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("items_name")]
	        public string ItemsName { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("items_number")]
	        public Nullable<long> ItemsNumber { get; set; }
	
	        /// <summary>
	        /// 商品实际成交价
	        /// </summary>
	        [XmlElement("items_price")]
	        public string ItemsPrice { get; set; }
	
	        /// <summary>
	        /// 商品货号
	        /// </summary>
	        [XmlElement("items_sn")]
	        public string ItemsSn { get; set; }
	
	        /// <summary>
	        /// 商品吊牌价
	        /// </summary>
	        [XmlElement("market_price")]
	        public string MarketPrice { get; set; }
	
	        /// <summary>
	        /// 订单编号（外部系统对接此字段无效）
	        /// </summary>
	        [XmlElement("order_sn")]
	        public string OrderSn { get; set; }
	
	        /// <summary>
	        /// 商品售价
	        /// </summary>
	        [XmlElement("shop_price")]
	        public string ShopPrice { get; set; }
	
	        /// <summary>
	        /// 尺码代码
	        /// </summary>
	        [XmlElement("size_code")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 商品SKU编码
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku { get; set; }
}

        #endregion
    }
}
