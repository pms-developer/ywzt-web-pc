using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.entryorder.sync
    /// </summary>
    public class TaobaoErpEntryorderSyncRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpEntryorderSyncResponse>
    {
        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 商品信息
        /// </summary>
        public string Items { get; set; }

        public List<ItemsDomain> Items_ { set { this.Items = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 订单详情
        /// </summary>
        public string Order { get; set; }

        public OrderDomain Order_ { set { this.Order = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.entryorder.sync";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("items", this.Items);
            parameters.Add("order", this.Order);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("items", this.Items, 3000);
        }

	/// <summary>
/// OrderDomain Data Structure.
/// </summary>
[Serializable]

public class OrderDomain : TopObject
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 品牌代码
	        /// </summary>
	        [XmlElement("brandID")]
	        public string BrandID { get; set; }
	
	        /// <summary>
	        /// 渠道代码
	        /// </summary>
	        [XmlElement("channelCode")]
	        public string ChannelCode { get; set; }
	
	        /// <summary>
	        /// 制单人
	        /// </summary>
	        [XmlElement("createEmp")]
	        public string CreateEmp { get; set; }
	
	        /// <summary>
	        /// 客户代码
	        /// </summary>
	        [XmlElement("customerCode")]
	        public string CustomerCode { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("number")]
	        public string Number { get; set; }
	
	        /// <summary>
	        /// 通知单编号
	        /// </summary>
	        [XmlElement("orderCode")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// 订单创建日期
	        /// </summary>
	        [XmlElement("orderCreateTime")]
	        public string OrderCreateTime { get; set; }
	
	        /// <summary>
	        /// 源单据号
	        /// </summary>
	        [XmlElement("orderId")]
	        public string OrderId { get; set; }
	
	        /// <summary>
	        /// 业务类型(B2BCK=批发销货单/B2BRK=批发退货单/PHCK=商店配货单/PHRK=商店退货单/CGCK=商品进货单/CGRK=商品退货单/DBCK=渠道调拨单/DBRK=渠道调拨退货单/YCCK=商品移仓单出库/YCRK =移仓单入库
	        /// </summary>
	        [XmlElement("orderType")]
	        public string OrderType { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 供货商代码(采购单据)
	        /// </summary>
	        [XmlElement("supplierCode")]
	        public string SupplierCode { get; set; }
	
	        /// <summary>
	        /// 仓库代码（发货仓库）
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
	
	        /// <summary>
	        /// 仓库代码（移入仓库）移仓单回传使用
	        /// </summary>
	        [XmlElement("warehouseCodeIn")]
	        public string WarehouseCodeIn { get; set; }
	
	        /// <summary>
	        /// 库区代码
	        /// </summary>
	        [XmlElement("whareaTypeCode")]
	        public string WhareaTypeCode { get; set; }
}

	/// <summary>
/// ItemsDomain Data Structure.
/// </summary>
[Serializable]

public class ItemsDomain : TopObject
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 颜色编号
	        /// </summary>
	        [XmlElement("colorCode")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 颜色名称
	        /// </summary>
	        [XmlElement("colorName")]
	        public string ColorName { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 商品编码（上游系统）
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("number")]
	        public string Number { get; set; }
	
	        /// <summary>
	        /// 采购价
	        /// </summary>
	        [XmlElement("purchasePrice")]
	        public string PurchasePrice { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 零售价
	        /// </summary>
	        [XmlElement("retailPrice")]
	        public string RetailPrice { get; set; }
	
	        /// <summary>
	        /// 尺寸编号
	        /// </summary>
	        [XmlElement("sizeCode")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 尺寸名称
	        /// </summary>
	        [XmlElement("sizeName")]
	        public string SizeName { get; set; }
	
	        /// <summary>
	        /// 商品属性
	        /// </summary>
	        [XmlElement("skuProperty")]
	        public string SkuProperty { get; set; }
	
	        /// <summary>
	        /// 标准价
	        /// </summary>
	        [XmlElement("stdprice")]
	        public string Stdprice { get; set; }
	
	        /// <summary>
	        /// 款编号
	        /// </summary>
	        [XmlElement("styleCode")]
	        public string StyleCode { get; set; }
	
	        /// <summary>
	        /// 款名
	        /// </summary>
	        [XmlElement("styleName")]
	        public string StyleName { get; set; }
}

        #endregion
    }
}
