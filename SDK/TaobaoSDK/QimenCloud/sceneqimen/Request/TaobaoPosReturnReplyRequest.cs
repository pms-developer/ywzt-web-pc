using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.return.reply
    /// </summary>
    public class TaobaoPosReturnReplyRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosReturnReplyResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// null
        /// </summary>
        public string Operator { get; set; }

        public List<StructDomain> Operator_ { set { this.Operator = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 退仓申请单单据编码
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 单据类型
        /// </summary>
        public string OrderType { get; set; }

        /// <summary>
        /// 组织编码
        /// </summary>
        public string OrgCode { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 提出申请的门店编码
        /// </summary>
        public string StoreCode { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.return.reply";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("operator", this.Operator);
            parameters.Add("orderCode", this.OrderCode);
            parameters.Add("orderType", this.OrderType);
            parameters.Add("orgCode", this.OrgCode);
            parameters.Add("status", this.Status);
            parameters.Add("storeCode", this.StoreCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateObjectMaxListSize("operator", this.Operator, 20);
            RequestValidator.ValidateRequired("orderCode", this.OrderCode);
            RequestValidator.ValidateMaxLength("orderCode", this.OrderCode, 50);
            RequestValidator.ValidateRequired("orderType", this.OrderType);
            RequestValidator.ValidateMaxLength("orderType", this.OrderType, 50);
            RequestValidator.ValidateMaxLength("orgCode", this.OrgCode, 50);
            RequestValidator.ValidateRequired("status", this.Status);
            RequestValidator.ValidateMaxLength("status", this.Status, 50);
            RequestValidator.ValidateRequired("storeCode", this.StoreCode);
            RequestValidator.ValidateMaxLength("storeCode", this.StoreCode, 50);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 操作时间
	        /// </summary>
	        [XmlElement("operateTime")]
	        public string OperateTime { get; set; }
	
	        /// <summary>
	        /// 操作员编码
	        /// </summary>
	        [XmlElement("operatorCode")]
	        public string OperatorCode { get; set; }
	
	        /// <summary>
	        /// 操作员名称
	        /// </summary>
	        [XmlElement("operatorName")]
	        public string OperatorName { get; set; }
	
	        /// <summary>
	        /// 操作员类型
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
}

        #endregion
    }
}
