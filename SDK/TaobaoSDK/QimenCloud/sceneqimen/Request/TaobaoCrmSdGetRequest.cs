using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.crm.sd.get
    /// </summary>
    public class TaobaoCrmSdGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoCrmSdGetResponse>
    {
        /// <summary>
        /// 查询内结束时间： 2000-11-3023:59:59
        /// </summary>
        public string EndModified { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 页码: 取值范围:大于零的整数; 默认值:1
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 每页条数。取值范围:大于零的整数; 默认值:20;最大值:100
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 渠道代码 如果传此字段代表指定查询
        /// </summary>
        public string Sddm { get; set; }

        /// <summary>
        /// 查询起始时间： 2011-09-01 00:00:00
        /// </summary>
        public string StartModified { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.crm.sd.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("endModified", this.EndModified);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("pageNo", this.PageNo);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("sddm", this.Sddm);
            parameters.Add("startModified", this.StartModified);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("endModified", this.EndModified);
            RequestValidator.ValidateMaxLength("endModified", this.EndModified, 64);
            RequestValidator.ValidateRequired("pageNo", this.PageNo);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateMaxLength("sddm", this.Sddm, 64);
            RequestValidator.ValidateRequired("startModified", this.StartModified);
            RequestValidator.ValidateMaxLength("startModified", this.StartModified, 64);
        }

        #endregion
    }
}
