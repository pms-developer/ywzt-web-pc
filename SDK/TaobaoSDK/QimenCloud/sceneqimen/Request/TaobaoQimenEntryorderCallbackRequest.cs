using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.entryorder.callback
    /// </summary>
    public class TaobaoQimenEntryorderCallbackRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoQimenEntryorderCallbackResponse>
    {
        /// <summary>
        /// 应用在奇门申请的appkey
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 入库单信息列表
        /// </summary>
        public string Entryorderlist { get; set; }

        public StructDomain Entryorderlist_ { set { this.Entryorderlist = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 货主在奇门授权的ID
        /// </summary>
        public string UserId { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.entryorder.callback";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("entryorderlist", this.Entryorderlist);
            parameters.Add("userId", this.UserId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("appkey", this.Appkey);
            RequestValidator.ValidateRequired("entryorderlist", this.Entryorderlist);
            RequestValidator.ValidateRequired("userId", this.UserId);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 实际出库数量
	        /// </summary>
	        [XmlElement("actualQty")]
	        public Nullable<long> ActualQty { get; set; }
	
	        /// <summary>
	        /// 商品编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 仓储系统商品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
}

        #endregion
    }
}
