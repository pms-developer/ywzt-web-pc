using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.return.apply
    /// </summary>
    public class TaobaoPosReturnApplyRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosReturnApplyResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// null
        /// </summary>
        public string OrderInfo { get; set; }

        public StructDomain OrderInfo_ { set { this.OrderInfo = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// null
        /// </summary>
        public string OrderLine { get; set; }

        public List<StructDomain> OrderLine_ { set { this.OrderLine = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.return.apply";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("orderInfo", this.OrderInfo);
            parameters.Add("orderLine", this.OrderLine);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateObjectMaxListSize("orderLine", this.OrderLine, 20);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 操作时间
	        /// </summary>
	        [XmlElement("operateTime")]
	        public string OperateTime { get; set; }
	
	        /// <summary>
	        /// 操作员编码
	        /// </summary>
	        [XmlElement("operatorCode")]
	        public string OperatorCode { get; set; }
	
	        /// <summary>
	        /// 操作员名称
	        /// </summary>
	        [XmlElement("operatorName")]
	        public string OperatorName { get; set; }
	
	        /// <summary>
	        /// 操作员类型
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
}

        #endregion
    }
}
