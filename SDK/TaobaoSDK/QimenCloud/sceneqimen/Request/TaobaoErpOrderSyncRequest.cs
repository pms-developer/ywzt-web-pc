using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.order.sync
    /// </summary>
    public class TaobaoErpOrderSyncRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpOrderSyncResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 每页条数(不超过100)
        /// </summary>
        public string PageSize { get; set; }

        /// <summary>
        /// 当前页从1开始
        /// </summary>
        public string PageNo { get; set; }

        /// <summary>
        /// 店铺名称
        /// </summary>
        public string ShopName { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// 交易类型(0被取消;1等待单;2待审核;3预订单;4待结算;5打单出库;6生产等待;7待发货;8发货在途;9代销发货;10委外发货;11已完成)
        /// </summary>
        public string TimeType { get; set; }

        /// <summary>
        /// 查询类型(0查询未归档订单;1查询归档订单)
        /// </summary>
        public string TradeArc { get; set; }

        /// <summary>
        /// 订单类型(0被取消;1等待单;2待审核;3预订单;4待结算;5打单出库;6生产等待;7待发货;8发货在途;9代销发货;10委外发货;11已完成)
        /// </summary>
        public string TradeStatus { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.order.sync";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("endDate", this.EndDate);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("shop_name", this.ShopName);
            parameters.Add("startDate", this.StartDate);
            parameters.Add("time_type", this.TimeType);
            parameters.Add("trade_arc", this.TradeArc);
            parameters.Add("trade_status", this.TradeStatus);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateRequired("page_no", this.PageNo);
        }

        #endregion
    }
}
