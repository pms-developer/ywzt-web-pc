using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.icp.order.stockinordermessagetoerp
    /// </summary>
    public class TaobaoIcpOrderStockinordermessagetoerpRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoIcpOrderStockinordermessagetoerpResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 入库单记录集
        /// </summary>
        public string EntryOrderlist { get; set; }

        public EntryOrderlistDomain EntryOrderlist_ { set { this.EntryOrderlist = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.icp.order.stockinordermessagetoerp";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("entryOrderlist", this.EntryOrderlist);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
            RequestValidator.ValidateMaxLength("customerId", this.CustomerId, 64);
        }

	/// <summary>
/// SenderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SenderInfoDomain : TopObject
{
	        /// <summary>
	        /// 区域, string (50)
	        /// </summary>
	        [XmlElement("senderInfoarea")]
	        public string SenderInfoarea { get; set; }
	
	        /// <summary>
	        /// 城市, string (50)
	        /// </summary>
	        [XmlElement("senderInfocity")]
	        public string SenderInfocity { get; set; }
	
	        /// <summary>
	        /// 公司名称
	        /// </summary>
	        [XmlElement("senderInfocompany")]
	        public string SenderInfocompany { get; set; }
	
	        /// <summary>
	        /// 国家二字码
	        /// </summary>
	        [XmlElement("senderInfocountryCode")]
	        public string SenderInfocountryCode { get; set; }
	
	        /// <summary>
	        /// 详细地址, string (200)
	        /// </summary>
	        [XmlElement("senderInfodetailAddress")]
	        public string SenderInfodetailAddress { get; set; }
	
	        /// <summary>
	        /// 电子邮箱
	        /// </summary>
	        [XmlElement("senderInfoemail")]
	        public string SenderInfoemail { get; set; }
	
	        /// <summary>
	        /// 证件号，string(50)
	        /// </summary>
	        [XmlElement("senderInfoid")]
	        public string SenderInfoid { get; set; }
	
	        /// <summary>
	        /// 移动电话
	        /// </summary>
	        [XmlElement("senderInfomobile")]
	        public string SenderInfomobile { get; set; }
	
	        /// <summary>
	        /// 姓名
	        /// </summary>
	        [XmlElement("senderInfoname")]
	        public string SenderInfoname { get; set; }
	
	        /// <summary>
	        /// 省份, string (50)
	        /// </summary>
	        [XmlElement("senderInfoprovince")]
	        public string SenderInfoprovince { get; set; }
	
	        /// <summary>
	        /// 固定电话
	        /// </summary>
	        [XmlElement("senderInfotel")]
	        public string SenderInfotel { get; set; }
	
	        /// <summary>
	        /// 村镇, string (50)
	        /// </summary>
	        [XmlElement("senderInfotown")]
	        public string SenderInfotown { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("senderInfozipCode")]
	        public string SenderInfozipCode { get; set; }
}

	/// <summary>
/// ReceiverInfoDomain Data Structure.
/// </summary>
[Serializable]

public class ReceiverInfoDomain : TopObject
{
	        /// <summary>
	        /// 区域, string (50)
	        /// </summary>
	        [XmlElement("receiverInfoarea")]
	        public string ReceiverInfoarea { get; set; }
	
	        /// <summary>
	        /// 城市, string (50) , 必填
	        /// </summary>
	        [XmlElement("receiverInfocity")]
	        public string ReceiverInfocity { get; set; }
	
	        /// <summary>
	        /// 公司名称, string (200)
	        /// </summary>
	        [XmlElement("receiverInfocompany")]
	        public string ReceiverInfocompany { get; set; }
	
	        /// <summary>
	        /// 国家二字码，string（50）
	        /// </summary>
	        [XmlElement("receiverInfocountryCode")]
	        public string ReceiverInfocountryCode { get; set; }
	
	        /// <summary>
	        /// 详细地址, string (200) , 必填
	        /// </summary>
	        [XmlElement("receiverInfodetailAddress")]
	        public string ReceiverInfodetailAddress { get; set; }
	
	        /// <summary>
	        /// 电子邮箱, string (50)
	        /// </summary>
	        [XmlElement("receiverInfoemail")]
	        public string ReceiverInfoemail { get; set; }
	
	        /// <summary>
	        /// 证件号，string(50)
	        /// </summary>
	        [XmlElement("receiverInfoid")]
	        public string ReceiverInfoid { get; set; }
	
	        /// <summary>
	        /// 移动电话, string (50)
	        /// </summary>
	        [XmlElement("receiverInfomobile")]
	        public string ReceiverInfomobile { get; set; }
	
	        /// <summary>
	        /// 姓名
	        /// </summary>
	        [XmlElement("receiverInfoname")]
	        public string ReceiverInfoname { get; set; }
	
	        /// <summary>
	        /// 省份, string (50) , 必填
	        /// </summary>
	        [XmlElement("receiverInfoprovince")]
	        public string ReceiverInfoprovince { get; set; }
	
	        /// <summary>
	        /// 固定电话, string (50)
	        /// </summary>
	        [XmlElement("receiverInfotel")]
	        public string ReceiverInfotel { get; set; }
	
	        /// <summary>
	        /// 村镇, string (50)
	        /// </summary>
	        [XmlElement("receiverInfotown")]
	        public string ReceiverInfotown { get; set; }
	
	        /// <summary>
	        /// 邮编, string (50)
	        /// </summary>
	        [XmlElement("receiverInfozipCode")]
	        public string ReceiverInfozipCode { get; set; }
}

	/// <summary>
/// EntryOrderDomain Data Structure.
/// </summary>
[Serializable]

public class EntryOrderDomain : TopObject
{
	        /// <summary>
	        /// 交易单号
	        /// </summary>
	        [XmlElement("buyOrderCode")]
	        public string BuyOrderCode { get; set; }
	
	        /// <summary>
	        /// 外部单号(以前入库单编码)
	        /// </summary>
	        [XmlElement("entryInOrderCode")]
	        public string EntryInOrderCode { get; set; }
	
	        /// <summary>
	        /// 物流单号（以前仓储系统入库单ID）
	        /// </summary>
	        [XmlElement("entryOrderId")]
	        public string EntryOrderId { get; set; }
	
	        /// <summary>
	        /// 入库单类型
	        /// </summary>
	        [XmlElement("entryOrderType")]
	        public string EntryOrderType { get; set; }
	
	        /// <summary>
	        /// 对应单号编码
	        /// </summary>
	        [XmlElement("entryOutOrderCode")]
	        public string EntryOutOrderCode { get; set; }
	
	        /// <summary>
	        /// 外部ERP单号
	        /// </summary>
	        [XmlElement("erpordercode")]
	        public string Erpordercode { get; set; }
	
	        /// <summary>
	        /// 拓展属性数据
	        /// </summary>
	        [XmlElement("extendFields")]
	        public string ExtendFields { get; set; }
	
	        /// <summary>
	        /// 操作时间
	        /// </summary>
	        [XmlElement("operateTime")]
	        public string OperateTime { get; set; }
	
	        /// <summary>
	        /// 外部业务编码
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 入库货主编码
	        /// </summary>
	        [XmlElement("ownerInCode")]
	        public string OwnerInCode { get; set; }
	
	        /// <summary>
	        /// 对应单号货主编码
	        /// </summary>
	        [XmlElement("ownerOutCode")]
	        public string OwnerOutCode { get; set; }
	
	        /// <summary>
	        /// 收货人信息
	        /// </summary>
	        [XmlElement("receiverInfo")]
	        public ReceiverInfoDomain ReceiverInfo { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 入库单预约日期
	        /// </summary>
	        [XmlElement("reservationdate")]
	        public string Reservationdate { get; set; }
	
	        /// <summary>
	        /// 送货人信息
	        /// </summary>
	        [XmlElement("senderInfo")]
	        public SenderInfoDomain SenderInfo { get; set; }
	
	        /// <summary>
	        /// 入库单状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 明细总行数
	        /// </summary>
	        [XmlElement("totalOrderLines")]
	        public Nullable<long> TotalOrderLines { get; set; }
	
	        /// <summary>
	        /// 仓库编码
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

	/// <summary>
/// OrderLinesDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("orderLine")]
public class OrderLinesDomain : TopObject
{
	        /// <summary>
	        /// （无）批次编码
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// （无）商品过期日期
	        /// </summary>
	        [XmlElement("expireDate")]
	        public string ExpireDate { get; set; }
	
	        /// <summary>
	        /// 货品拓展属性
	        /// </summary>
	        [XmlElement("feature")]
	        public string Feature { get; set; }
	
	        /// <summary>
	        /// 库存类型(ZP=正品、CC=残次、JS=机损、XS=箱损、默认为ZP、(收到商品总数=正品数+残品数+机损数+箱损数))
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 商品编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 仓储系统商品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 单据行号
	        /// </summary>
	        [XmlElement("orderLineNo")]
	        public string OrderLineNo { get; set; }
	
	        /// <summary>
	        /// 外部业务编码
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 货主编码
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 应收数量
	        /// </summary>
	        [XmlElement("planQty")]
	        public Nullable<long> PlanQty { get; set; }
	
	        /// <summary>
	        /// （无）生产批号
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// （无）商品生产日期,YYYY-MM-DD
	        /// </summary>
	        [XmlElement("productDate")]
	        public string ProductDate { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
}

	/// <summary>
/// EntryOrderlistDomain Data Structure.
/// </summary>
[Serializable]

public class EntryOrderlistDomain : TopObject
{
	        /// <summary>
	        /// 入库单信息
	        /// </summary>
	        [XmlElement("entryOrder")]
	        public EntryOrderDomain EntryOrder { get; set; }
	
	        /// <summary>
	        /// 入库单明细list
	        /// </summary>
	        [XmlArray("orderLines")]
	        [XmlArrayItem("orderLine")]
	        public List<OrderLinesDomain> OrderLines { get; set; }
}

        #endregion
    }
}
