using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: alibaba.ascp.huawei.order.process
    /// </summary>
    public class AlibabaAscpHuaweiOrderProcessRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.AlibabaAscpHuaweiOrderProcessResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 请求内容根节点
        /// </summary>
        public string Request { get; set; }

        public RequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "alibaba.ascp.huawei.order.process";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
            RequestValidator.ValidateRequired("request", this.Request);
        }

	/// <summary>
/// OrderLineDomain Data Structure.
/// </summary>
[Serializable]

public class OrderLineDomain : TopObject
{
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 外部业务编码, 消息ID, 用于去重
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 平台主交易号
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
	
	        /// <summary>
	        /// 平台子交易号
	        /// </summary>
	        [XmlElement("tradeItemId")]
	        public string TradeItemId { get; set; }
}

	/// <summary>
/// OrderLinesDomain Data Structure.
/// </summary>
[Serializable]

public class OrderLinesDomain : TopObject
{
	        /// <summary>
	        /// 订单明细
	        /// </summary>
	        [XmlArray("orderLine")]
	        [XmlArrayItem("order_line")]
	        public List<OrderLineDomain> OrderLine { get; set; }
}

	/// <summary>
/// ProcessDomain Data Structure.
/// </summary>
[Serializable]

public class ProcessDomain : TopObject
{
	        /// <summary>
	        /// 运单号
	        /// </summary>
	        [XmlElement("expressCode")]
	        public string ExpressCode { get; set; }
	
	        /// <summary>
	        /// 操作内容
	        /// </summary>
	        [XmlElement("operateInfo")]
	        public string OperateInfo { get; set; }
	
	        /// <summary>
	        /// 当前状态操作时间
	        /// </summary>
	        [XmlElement("operateTime")]
	        public string OperateTime { get; set; }
	
	        /// <summary>
	        /// 单据状态 distribution_sign(配送签收)、distribution_cut（配送拦截）、distribution_reject（配送拒签）
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

	/// <summary>
/// RequestDomain Data Structure.
/// </summary>
[Serializable]

public class RequestDomain : TopObject
{
	        /// <summary>
	        /// 订单明细列表
	        /// </summary>
	        [XmlElement("orderLines")]
	        public OrderLinesDomain OrderLines { get; set; }
	
	        /// <summary>
	        /// 单据类型
	        /// </summary>
	        [XmlElement("orderType")]
	        public Nullable<long> OrderType { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("process")]
	        public ProcessDomain Process { get; set; }
	
	        /// <summary>
	        /// 店铺名称
	        /// </summary>
	        [XmlElement("shopName")]
	        public string ShopName { get; set; }
	
	        /// <summary>
	        /// 仓库编码
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
	
	        /// <summary>
	        /// 仓储系统单据号
	        /// </summary>
	        [XmlElement("warehouseOrderCode")]
	        public string WarehouseOrderCode { get; set; }
}

        #endregion
    }
}
