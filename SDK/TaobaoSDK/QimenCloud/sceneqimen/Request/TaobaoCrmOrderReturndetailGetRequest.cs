using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.crm.order.returndetail.get
    /// </summary>
    public class TaobaoCrmOrderReturndetailGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoCrmOrderReturndetailGetResponse>
    {
        /// <summary>
        /// 退单列表修改时间的结束时间，格式为 2013-11-12 12:00:00
        /// </summary>
        public string EndModified { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 页数
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// E3退单编号
        /// </summary>
        public string ReturnOrderSn { get; set; }

        /// <summary>
        /// E3商店编码
        /// </summary>
        public string SdCode { get; set; }

        /// <summary>
        /// 退单列表修改时间开始时间，格式为  2013-11-12 12:00:00
        /// </summary>
        public string StartModified { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.crm.order.returndetail.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("endModified", this.EndModified);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("pageNo", this.PageNo);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("return_order_sn", this.ReturnOrderSn);
            parameters.Add("sd_code", this.SdCode);
            parameters.Add("startModified", this.StartModified);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("endModified", this.EndModified);
            RequestValidator.ValidateRequired("pageNo", this.PageNo);
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateMaxLength("return_order_sn", this.ReturnOrderSn, 64);
            RequestValidator.ValidateMaxLength("sd_code", this.SdCode, 64);
            RequestValidator.ValidateRequired("startModified", this.StartModified);
        }

        #endregion
    }
}
