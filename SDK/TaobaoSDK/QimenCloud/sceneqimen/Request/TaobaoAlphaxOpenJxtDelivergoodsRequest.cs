using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.alphax.open.jxt.delivergoods
    /// </summary>
    public class TaobaoAlphaxOpenJxtDelivergoodsRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoAlphaxOpenJxtDelivergoodsResponse>
    {
        /// <summary>
        /// 主订单id
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 卖家主账号
        /// </summary>
        public string SellerId { get; set; }

        /// <summary>
        /// 卖家名称
        /// </summary>
        public string SellerNick { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.alphax.open.jxt.delivergoods";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("order_id", this.OrderId);
            parameters.Add("seller_id", this.SellerId);
            parameters.Add("seller_nick", this.SellerNick);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("order_id", this.OrderId);
            RequestValidator.ValidateRequired("seller_id", this.SellerId);
            RequestValidator.ValidateRequired("seller_nick", this.SellerNick);
        }

        #endregion
    }
}
