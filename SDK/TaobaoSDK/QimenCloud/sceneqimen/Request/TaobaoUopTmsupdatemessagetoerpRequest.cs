using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.uop.tmsupdatemessagetoerp
    /// </summary>
    public class TaobaoUopTmsupdatemessagetoerpRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoUopTmsupdatemessagetoerpResponse>
    {
        /// <summary>
        /// null
        /// </summary>
        public string Request { get; set; }

        public StructDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.uop.tmsupdatemessagetoerp";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// PackageMaterialDomain Data Structure.
/// </summary>
[Serializable]

public class PackageMaterialDomain : TopObject
{
	        /// <summary>
	        /// 包材的数量
	        /// </summary>
	        [XmlElement("materialQuantity")]
	        public Nullable<long> MaterialQuantity { get; set; }
	
	        /// <summary>
	        /// 淘宝指定的包材型号
	        /// </summary>
	        [XmlElement("materialType")]
	        public string MaterialType { get; set; }
}

	/// <summary>
/// PackageMaterialListDomain Data Structure.
/// </summary>
[Serializable]

public class PackageMaterialListDomain : TopObject
{
	        /// <summary>
	        /// 包裹材料
	        /// </summary>
	        [XmlElement("packageMaterial")]
	        public PackageMaterialDomain PackageMaterial { get; set; }
}

	/// <summary>
/// TmsItemDomain Data Structure.
/// </summary>
[Serializable]

public class TmsItemDomain : TopObject
{
	        /// <summary>
	        /// 出库数量
	        /// </summary>
	        [XmlElement("actualItemQuantity")]
	        public string ActualItemQuantity { get; set; }
	
	        /// <summary>
	        /// 批次号
	        /// </summary>
	        [XmlElement("batchCode")]
	        public string BatchCode { get; set; }
	
	        /// <summary>
	        /// 包裹状态(配签收TMS_SIGN|配拒签TMS_FAILED|部分签收TMS_PART_SIGN)
	        /// </summary>
	        [XmlElement("extendFields")]
	        public string ExtendFields { get; set; }
	
	        /// <summary>
	        /// ERP商品id
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 菜鸟商品id
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 明细数量
	        /// </summary>
	        [XmlElement("itemQuantity")]
	        public string ItemQuantity { get; set; }
	
	        /// <summary>
	        /// 明细状态(配签收TMS_SIGN|配拒签TMS_FAILED)
	        /// </summary>
	        [XmlElement("itemStatus")]
	        public string ItemStatus { get; set; }
	
	        /// <summary>
	        /// order_item_id
	        /// </summary>
	        [XmlElement("orderItemId")]
	        public string OrderItemId { get; set; }
	
	        /// <summary>
	        /// 生产批号
	        /// </summary>
	        [XmlElement("produceCode")]
	        public string ProduceCode { get; set; }
	
	        /// <summary>
	        /// 生产日期
	        /// </summary>
	        [XmlElement("productDate")]
	        public string ProductDate { get; set; }
	
	        /// <summary>
	        /// 备注原因（损坏BORKEN|丢失LOST|其他原因）
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
}

	/// <summary>
/// TmsOrderDomain Data Structure.
/// </summary>
[Serializable]

public class TmsOrderDomain : TopObject
{
	        /// <summary>
	        /// 物流商编码
	        /// </summary>
	        [XmlElement("cpCode")]
	        public string CpCode { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendFields")]
	        public string ExtendFields { get; set; }
	
	        /// <summary>
	        /// 包裹号
	        /// </summary>
	        [XmlElement("packageCode")]
	        public string PackageCode { get; set; }
	
	        /// <summary>
	        /// 包裹高度，单位：毫米
	        /// </summary>
	        [XmlElement("packageHeight")]
	        public Nullable<long> PackageHeight { get; set; }
	
	        /// <summary>
	        /// 包裹长度，单位：毫米
	        /// </summary>
	        [XmlElement("packageLength")]
	        public Nullable<long> PackageLength { get; set; }
	
	        /// <summary>
	        /// 包裹材料列表
	        /// </summary>
	        [XmlElement("packageMaterialList")]
	        public PackageMaterialListDomain PackageMaterialList { get; set; }
	
	        /// <summary>
	        /// 包裹状态(配签收TMS_SIGN|配拒签TMS_FAILED|部分签收TMS_PART_SIGN)
	        /// </summary>
	        [XmlElement("packageStatus")]
	        public string PackageStatus { get; set; }
	
	        /// <summary>
	        /// 包裹重量，单位：克
	        /// </summary>
	        [XmlElement("packageWeight")]
	        public Nullable<long> PackageWeight { get; set; }
	
	        /// <summary>
	        /// 包裹宽度，单位：毫米
	        /// </summary>
	        [XmlElement("packageWidth")]
	        public Nullable<long> PackageWidth { get; set; }
	
	        /// <summary>
	        /// 备注如拒签原因
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 资源编码
	        /// </summary>
	        [XmlElement("resCode")]
	        public string ResCode { get; set; }
	
	        /// <summary>
	        /// 快递公司服务编码
	        /// </summary>
	        [XmlElement("tmsCode")]
	        public string TmsCode { get; set; }
	
	        /// <summary>
	        /// 配明细列表
	        /// </summary>
	        [XmlArray("tmsItems")]
	        [XmlArrayItem("tms_item")]
	        public List<TmsItemDomain> TmsItems { get; set; }
	
	        /// <summary>
	        /// 运单编码
	        /// </summary>
	        [XmlElement("tmsOrderCode")]
	        public string TmsOrderCode { get; set; }
	
	        /// <summary>
	        /// 包裹的揽收/签收/ 拒签时间
	        /// </summary>
	        [XmlElement("updateTime")]
	        public string UpdateTime { get; set; }
}

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 菜鸟订单编码
	        /// </summary>
	        [XmlElement("cnOrderCode")]
	        public string CnOrderCode { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendFields")]
	        public string ExtendFields { get; set; }
	
	        /// <summary>
	        /// 库存模式（实仓:1;虚仓:2）
	        /// </summary>
	        [XmlElement("invMode")]
	        public string InvMode { get; set; }
	
	        /// <summary>
	        /// 商家订单编码
	        /// </summary>
	        [XmlElement("orderCode")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// 外部业务编码
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 零售通门店订单号
	        /// </summary>
	        [XmlElement("outerBizOrderId")]
	        public string OuterBizOrderId { get; set; }
	
	        /// <summary>
	        /// 配送状态(配揽收TMS_ACCEPT|配签收TMS_SIGN|配拒签TMS_FAILED|部分签收TMS_PART_SIGN)
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 菜鸟订单编码
	        /// </summary>
	        [XmlElement("storeOrderCode")]
	        public string StoreOrderCode { get; set; }
	
	        /// <summary>
	        /// 配送订单
	        /// </summary>
	        [XmlElement("tmsOrder")]
	        public TmsOrderDomain TmsOrder { get; set; }
	
	        /// <summary>
	        /// 货主id
	        /// </summary>
	        [XmlElement("userId")]
	        public string UserId { get; set; }
}

        #endregion
    }
}
