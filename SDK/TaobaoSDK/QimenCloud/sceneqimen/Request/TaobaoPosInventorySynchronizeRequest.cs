using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.inventory.synchronize
    /// </summary>
    public class TaobaoPosInventorySynchronizeRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosInventorySynchronizeResponse>
    {
        /// <summary>
        /// customerid
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// null
        /// </summary>
        public string Item { get; set; }

        public List<StructDomain> Item_ { set { this.Item = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 订单信息
        /// </summary>
        public string OrderInfo { get; set; }

        public OrderInfoDomain OrderInfo_ { set { this.OrderInfo = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.inventory.synchronize";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("item", this.Item);
            parameters.Add("orderInfo", this.OrderInfo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("item", this.Item, 20);
        }

	/// <summary>
/// OperatorDomain Data Structure.
/// </summary>
[Serializable]

public class OperatorDomain : TopObject
{
	        /// <summary>
	        /// 操作时间
	        /// </summary>
	        [XmlElement("operateTime")]
	        public string OperateTime { get; set; }
	
	        /// <summary>
	        /// 操作员编码
	        /// </summary>
	        [XmlElement("operatorCode")]
	        public string OperatorCode { get; set; }
	
	        /// <summary>
	        /// 操作员名称
	        /// </summary>
	        [XmlElement("operatorName")]
	        public string OperatorName { get; set; }
	
	        /// <summary>
	        /// 操作员类型
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
}

	/// <summary>
/// OrderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class OrderInfoDomain : TopObject
{
	        /// <summary>
	        /// 操作
	        /// </summary>
	        [XmlArray("operator")]
	        [XmlArrayItem("operator")]
	        public List<OperatorDomain> Operator { get; set; }
	
	        /// <summary>
	        /// 提出申请的门店编码
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
}

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 商品sku编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// POS的账面数量
	        /// </summary>
	        [XmlElement("paperQty")]
	        public Nullable<long> PaperQty { get; set; }
	
	        /// <summary>
	        /// 商品spu编码
	        /// </summary>
	        [XmlElement("productCode")]
	        public string ProductCode { get; set; }
}

        #endregion
    }
}
