using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.item.sync
    /// </summary>
    public class TaobaoErpItemSyncRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpItemSyncResponse>
    {
        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 每页条数(不超过100)
        /// </summary>
        public string PageSize { get; set; }

        /// <summary>
        /// 当前页从1开始
        /// </summary>
        public string PageNo { get; set; }

        /// <summary>
        /// 最后修改的起始时间
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// 仓储id
        /// </summary>
        public string WarehouseId { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.item.sync";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("endDate", this.EndDate);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("page_no", this.PageNo);
            parameters.Add("startDate", this.StartDate);
            parameters.Add("warehouse_id", this.WarehouseId);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("pageSize", this.PageSize);
            RequestValidator.ValidateRequired("page_no", this.PageNo);
        }

        #endregion
    }
}
