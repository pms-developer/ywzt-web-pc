using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.qianniu.cloudkefu.address.self.modify
    /// </summary>
    public class TaobaoQianniuCloudkefuAddressSelfModifyRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoQianniuCloudkefuAddressSelfModifyResponse>
    {
        /// <summary>
        /// 交易订单ID
        /// </summary>
        public string BizOrderId { get; set; }

        /// <summary>
        /// 买家账号名
        /// </summary>
        public string BuyerNick { get; set; }

        /// <summary>
        /// 要修改的地址信息
        /// </summary>
        public string ModifiedAddress { get; set; }

        public ModifiedAddressDomain ModifiedAddress_ { set { this.ModifiedAddress = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 订单原始收货地址信息
        /// </summary>
        public string OriginalAddress { get; set; }

        public OriginalAddressDomain OriginalAddress_ { set { this.OriginalAddress = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 店铺主账号
        /// </summary>
        public string SellerNick { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.qianniu.cloudkefu.address.self.modify";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("bizOrderId", this.BizOrderId);
            parameters.Add("buyerNick", this.BuyerNick);
            parameters.Add("modifiedAddress", this.ModifiedAddress);
            parameters.Add("originalAddress", this.OriginalAddress);
            parameters.Add("sellerNick", this.SellerNick);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("bizOrderId", this.BizOrderId);
            RequestValidator.ValidateRequired("buyerNick", this.BuyerNick);
            RequestValidator.ValidateRequired("modifiedAddress", this.ModifiedAddress);
            RequestValidator.ValidateRequired("sellerNick", this.SellerNick);
        }

	/// <summary>
/// ModifiedAddressDomain Data Structure.
/// </summary>
[Serializable]

public class ModifiedAddressDomain : TopObject
{
	        /// <summary>
	        /// 详细地址，不带town信息，非必须有可能为空
	        /// </summary>
	        [XmlElement("addressDetail")]
	        public string AddressDetail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("area")]
	        public string Area { get; set; }
	
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 可不填，不填的情况下默认中国
	        /// </summary>
	        [XmlElement("country")]
	        public string Country { get; set; }
	
	        /// <summary>
	        /// 收货人姓名（可不填，无表示没有修改）
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 收货人电话（可不填，无表示没有修改）
	        /// </summary>
	        [XmlElement("phone")]
	        public string Phone { get; set; }
	
	        /// <summary>
	        /// 邮编，非必须有可能为空
	        /// </summary>
	        [XmlElement("postCode")]
	        public string PostCode { get; set; }
	
	        /// <summary>
	        /// 省份、州等
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 乡、镇、街道信息
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

	/// <summary>
/// OriginalAddressDomain Data Structure.
/// </summary>
[Serializable]

public class OriginalAddressDomain : TopObject
{
	        /// <summary>
	        /// 详细地址，不带town信息，非必须有可能为空
	        /// </summary>
	        [XmlElement("addressDetail")]
	        public string AddressDetail { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("area")]
	        public string Area { get; set; }
	
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 可不填，不填的情况下默认中国
	        /// </summary>
	        [XmlElement("country")]
	        public string Country { get; set; }
	
	        /// <summary>
	        /// 原始收货人姓名
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 原始收货人电话
	        /// </summary>
	        [XmlElement("phone")]
	        public string Phone { get; set; }
	
	        /// <summary>
	        /// 邮编，非必须有可能为空
	        /// </summary>
	        [XmlElement("postCode")]
	        public string PostCode { get; set; }
	
	        /// <summary>
	        /// 省份、州等
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 乡、镇、街道信息
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
}

        #endregion
    }
}
