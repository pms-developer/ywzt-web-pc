using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.retailorder.get
    /// </summary>
    public class TaobaoErpRetailorderGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpRetailorderGetResponse>
    {
        /// <summary>
        /// null
        /// </summary>
        public string Order { get; set; }

        public List<StructDomain> Order_ { set { this.Order = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 路由参数
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 订单详情
        /// </summary>
        public string OrderLine { get; set; }

        public List<OrderLineDomain> OrderLine_ { set { this.OrderLine = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.retailorder.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("Order", this.Order);
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("orderLine", this.OrderLine);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("Order", this.Order, 20);
            RequestValidator.ValidateObjectMaxListSize("orderLine", this.OrderLine, 100);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("actualQty")]
	        public string ActualQty { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 品牌代码
	        /// </summary>
	        [XmlElement("brandID")]
	        public string BrandID { get; set; }
	
	        /// <summary>
	        /// 渠道代码
	        /// </summary>
	        [XmlElement("channelCode")]
	        public string ChannelCode { get; set; }
	
	        /// <summary>
	        /// 店铺代码
	        /// </summary>
	        [XmlElement("customerCode")]
	        public string CustomerCode { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 通知单编号
	        /// </summary>
	        [XmlElement("orderCode")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// 订单创建日期
	        /// </summary>
	        [XmlElement("orderCreateTime")]
	        public string OrderCreateTime { get; set; }
	
	        /// <summary>
	        /// 源单据号
	        /// </summary>
	        [XmlElement("orderId")]
	        public string OrderId { get; set; }
	
	        /// <summary>
	        /// 业务类型(LSCK=零售销货单; LSTH=零售退货单)
	        /// </summary>
	        [XmlElement("orderType")]
	        public string OrderType { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 发货仓库代码
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

	/// <summary>
/// OrderLineDomain Data Structure.
/// </summary>
[Serializable]

public class OrderLineDomain : TopObject
{
	        /// <summary>
	        /// 商品数量
	        /// </summary>
	        [XmlElement("actualQty")]
	        public Nullable<long> ActualQty { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 颜色编号
	        /// </summary>
	        [XmlElement("colorCode")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 颜色名称
	        /// </summary>
	        [XmlElement("colorName")]
	        public string ColorName { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 商品编码（上游系统）
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 订单编号（淘宝订单）
	        /// </summary>
	        [XmlElement("orderId")]
	        public string OrderId { get; set; }
	
	        /// <summary>
	        /// 采购价
	        /// </summary>
	        [XmlElement("purchasePrice")]
	        public string PurchasePrice { get; set; }
	
	        /// <summary>
	        /// 零售价
	        /// </summary>
	        [XmlElement("retailPrice")]
	        public string RetailPrice { get; set; }
	
	        /// <summary>
	        /// 尺寸编号
	        /// </summary>
	        [XmlElement("sizeCode")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 尺寸名称
	        /// </summary>
	        [XmlElement("sizeName")]
	        public string SizeName { get; set; }
	
	        /// <summary>
	        /// 商品属性
	        /// </summary>
	        [XmlElement("skuProperty")]
	        public string SkuProperty { get; set; }
	
	        /// <summary>
	        /// 标准价
	        /// </summary>
	        [XmlElement("stdprice")]
	        public string Stdprice { get; set; }
	
	        /// <summary>
	        /// 款编号
	        /// </summary>
	        [XmlElement("styleCode")]
	        public string StyleCode { get; set; }
	
	        /// <summary>
	        /// 款名
	        /// </summary>
	        [XmlElement("styleName")]
	        public string StyleName { get; set; }
}

        #endregion
    }
}
