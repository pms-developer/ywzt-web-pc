using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.pos.receipt.synchronize
    /// </summary>
    public class TaobaoPosReceiptSynchronizeRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoPosReceiptSynchronizeResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// null
        /// </summary>
        public string Receipt { get; set; }

        public List<StructDomain> Receipt_ { set { this.Receipt = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.pos.receipt.synchronize";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("receipt", this.Receipt);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerid", this.Customerid);
            RequestValidator.ValidateMaxLength("customerid", this.Customerid, 50);
            RequestValidator.ValidateObjectMaxListSize("receipt", this.Receipt, 20);
        }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// VIP的卡号编码
	        /// </summary>
	        [XmlElement("VIPCardNo")]
	        public string VIPCardNo { get; set; }
	
	        /// <summary>
	        /// VIP会员编码
	        /// </summary>
	        [XmlElement("VIPCode")]
	        public string VIPCode { get; set; }
	
	        /// <summary>
	        /// VIP名称
	        /// </summary>
	        [XmlElement("VIPName")]
	        public string VIPName { get; set; }
	
	        /// <summary>
	        /// 找零金额
	        /// </summary>
	        [XmlElement("change")]
	        public string Change { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 收银员编码
	        /// </summary>
	        [XmlElement("operatorCode")]
	        public string OperatorCode { get; set; }
	
	        /// <summary>
	        /// 收银员名称
	        /// </summary>
	        [XmlElement("operatorName")]
	        public string OperatorName { get; set; }
	
	        /// <summary>
	        /// 小票编码
	        /// </summary>
	        [XmlElement("orderCode")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// 单据创建时间
	        /// </summary>
	        [XmlElement("orderCreateTime")]
	        public string OrderCreateTime { get; set; }
	
	        /// <summary>
	        /// 开票日期
	        /// </summary>
	        [XmlElement("orderDate")]
	        public string OrderDate { get; set; }
	
	        /// <summary>
	        /// 组织编码
	        /// </summary>
	        [XmlElement("orgCode")]
	        public string OrgCode { get; set; }
	
	        /// <summary>
	        /// 收款金额
	        /// </summary>
	        [XmlElement("paidAmount")]
	        public string PaidAmount { get; set; }
	
	        /// <summary>
	        /// POS终端编码
	        /// </summary>
	        [XmlElement("posCode")]
	        public string PosCode { get; set; }
	
	        /// <summary>
	        /// 抹零金额
	        /// </summary>
	        [XmlElement("roundingAmount")]
	        public string RoundingAmount { get; set; }
	
	        /// <summary>
	        /// 销售日期
	        /// </summary>
	        [XmlElement("saleDate")]
	        public string SaleDate { get; set; }
	
	        /// <summary>
	        /// 标准金额
	        /// </summary>
	        [XmlElement("standardAmount")]
	        public string StandardAmount { get; set; }
	
	        /// <summary>
	        /// 门店编码
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 整单金额
	        /// </summary>
	        [XmlElement("totalAmount")]
	        public string TotalAmount { get; set; }
	
	        /// <summary>
	        /// 整单的商品总数
	        /// </summary>
	        [XmlElement("totalQty")]
	        public Nullable<long> TotalQty { get; set; }
}

        #endregion
    }
}
