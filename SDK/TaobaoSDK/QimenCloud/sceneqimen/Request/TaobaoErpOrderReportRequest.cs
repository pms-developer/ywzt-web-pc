using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.order.report
    /// </summary>
    public class TaobaoErpOrderReportRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpOrderReportResponse>
    {
        /// <summary>
        /// 指定路由参数
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 订单信息
        /// </summary>
        public string Orders { get; set; }

        public List<OrdersDomain> Orders_ { set { this.Orders = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.order.report";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("orders", this.Orders);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
            RequestValidator.ValidateMaxLength("customerId", this.CustomerId, 64);
        }

	/// <summary>
/// OrderDomain Data Structure.
/// </summary>
[Serializable]

public class OrderDomain : TopObject
{
	        /// <summary>
	        /// 快递公司编码
	        /// </summary>
	        [XmlElement("expressNumber")]
	        public string ExpressNumber { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 操作内容
	        /// </summary>
	        [XmlElement("operateInfo")]
	        public string OperateInfo { get; set; }
	
	        /// <summary>
	        /// 操作时间
	        /// </summary>
	        [XmlElement("operateTime")]
	        public string OperateTime { get; set; }
	
	        /// <summary>
	        /// 操作员名称
	        /// </summary>
	        [XmlElement("operatorName")]
	        public string OperatorName { get; set; }
	
	        /// <summary>
	        /// 订单状态
	        /// </summary>
	        [XmlElement("orderStatus")]
	        public string OrderStatus { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// OMS订单号
	        /// </summary>
	        [XmlElement("saleOrderNumber")]
	        public string SaleOrderNumber { get; set; }
	
	        /// <summary>
	        /// 发货时间
	        /// </summary>
	        [XmlElement("sendDate")]
	        public string SendDate { get; set; }
	
	        /// <summary>
	        /// ERP订单号
	        /// </summary>
	        [XmlElement("tradeNumber")]
	        public string TradeNumber { get; set; }
	
	        /// <summary>
	        /// 仓库编码
	        /// </summary>
	        [XmlElement("warehouseNumber")]
	        public string WarehouseNumber { get; set; }
	
	        /// <summary>
	        /// 运单号
	        /// </summary>
	        [XmlElement("waybillNumber")]
	        public string WaybillNumber { get; set; }
	
	        /// <summary>
	        /// 重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
}

	/// <summary>
/// OrderLineDomain Data Structure.
/// </summary>
[Serializable]

public class OrderLineDomain : TopObject
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amt")]
	        public string Amt { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 商品编码
	        /// </summary>
	        [XmlElement("goodNumber")]
	        public string GoodNumber { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("qty")]
	        public string Qty { get; set; }
	
	        /// <summary>
	        /// 单位
	        /// </summary>
	        [XmlElement("unit")]
	        public string Unit { get; set; }
}

	/// <summary>
/// OrdersDomain Data Structure.
/// </summary>
[Serializable]

public class OrdersDomain : TopObject
{
	        /// <summary>
	        /// 订单详情
	        /// </summary>
	        [XmlElement("order")]
	        public OrderDomain Order { get; set; }
	
	        /// <summary>
	        /// 详情
	        /// </summary>
	        [XmlArray("orderLine")]
	        [XmlArrayItem("order_line")]
	        public List<OrderLineDomain> OrderLine { get; set; }
}

        #endregion
    }
}
