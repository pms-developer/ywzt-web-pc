using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: alibaba.ascp.huawei.stock.in.confirm
    /// </summary>
    public class AlibabaAscpHuaweiStockInConfirmRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.AlibabaAscpHuaweiStockInConfirmResponse>
    {
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// 111
        /// </summary>
        public string Request { get; set; }

        public RequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "alibaba.ascp.huawei.stock.in.confirm";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerId", this.CustomerId);
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("customerId", this.CustomerId);
        }

	/// <summary>
/// SenderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class SenderInfoDomain : TopObject
{
	        /// <summary>
	        /// 西湖区
	        /// </summary>
	        [XmlElement("area")]
	        public string Area { get; set; }
	
	        /// <summary>
	        /// 杭州市
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 公司名称
	        /// </summary>
	        [XmlElement("company")]
	        public string Company { get; set; }
	
	        /// <summary>
	        /// 浙江省杭州市西湖区留下镇
	        /// </summary>
	        [XmlElement("detailAddress")]
	        public string DetailAddress { get; set; }
	
	        /// <summary>
	        /// xxx@163.com
	        /// </summary>
	        [XmlElement("email")]
	        public string Email { get; set; }
	
	        /// <summary>
	        /// 18767121111
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 张三
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 浙江省
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 07103100000
	        /// </summary>
	        [XmlElement("tel")]
	        public string Tel { get; set; }
	
	        /// <summary>
	        /// 留下镇
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
	
	        /// <summary>
	        /// 310000
	        /// </summary>
	        [XmlElement("zipCode")]
	        public string ZipCode { get; set; }
}

	/// <summary>
/// SnCodeListDomain Data Structure.
/// </summary>
[Serializable]

public class SnCodeListDomain : TopObject
{
	        /// <summary>
	        /// 111
	        /// </summary>
	        [XmlArray("snCode")]
	        [XmlArrayItem("string")]
	        public List<string> SnCode { get; set; }
}

	/// <summary>
/// AttributeDomain Data Structure.
/// </summary>
[Serializable]

public class AttributeDomain : TopObject
{
	        /// <summary>
	        /// 10
	        /// </summary>
	        [XmlElement("actualQty")]
	        public Nullable<long> ActualQty { get; set; }
	
	        /// <summary>
	        /// bpart
	        /// </summary>
	        [XmlElement("bpcode")]
	        public string Bpcode { get; set; }
	
	        /// <summary>
	        /// 111
	        /// </summary>
	        [XmlElement("snCodeList")]
	        public SnCodeListDomain SnCodeList { get; set; }
}

	/// <summary>
/// AttributesDomain Data Structure.
/// </summary>
[Serializable]

public class AttributesDomain : TopObject
{
	        /// <summary>
	        /// 111
	        /// </summary>
	        [XmlArray("attribute")]
	        [XmlArrayItem("attribute")]
	        public List<AttributeDomain> Attribute { get; set; }
}

	/// <summary>
/// OrderItemDomain Data Structure.
/// </summary>
[Serializable]

public class OrderItemDomain : TopObject
{
	        /// <summary>
	        /// 111
	        /// </summary>
	        [XmlElement("attributes")]
	        public AttributesDomain Attributes { get; set; }
	
	        /// <summary>
	        /// ZP
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 123123
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 123123
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 111111
	        /// </summary>
	        [XmlElement("ownUserId")]
	        public Nullable<long> OwnUserId { get; set; }
	
	        /// <summary>
	        /// 1111111
	        /// </summary>
	        [XmlElement("tradeId")]
	        public string TradeId { get; set; }
	
	        /// <summary>
	        /// 11111
	        /// </summary>
	        [XmlElement("tradeItemId")]
	        public string TradeItemId { get; set; }
}

	/// <summary>
/// OrderItemsDomain Data Structure.
/// </summary>
[Serializable]

public class OrderItemsDomain : TopObject
{
	        /// <summary>
	        /// 111
	        /// </summary>
	        [XmlArray("orderItem")]
	        [XmlArrayItem("order_item")]
	        public List<OrderItemDomain> OrderItem { get; set; }
}

	/// <summary>
/// RequestDomain Data Structure.
/// </summary>
[Serializable]

public class RequestDomain : TopObject
{
	        /// <summary>
	        /// SF
	        /// </summary>
	        [XmlElement("logisticsCode")]
	        public string LogisticsCode { get; set; }
	
	        /// <summary>
	        /// 顺丰
	        /// </summary>
	        [XmlElement("logisticsName")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// 2019-08-25 00:00:00
	        /// </summary>
	        [XmlElement("orderConfirmTime")]
	        public string OrderConfirmTime { get; set; }
	
	        /// <summary>
	        /// 111
	        /// </summary>
	        [XmlElement("orderItems")]
	        public OrderItemsDomain OrderItems { get; set; }
	
	        /// <summary>
	        /// THRK
	        /// </summary>
	        [XmlElement("orderType")]
	        public string OrderType { get; set; }
	
	        /// <summary>
	        /// 1111111
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 退货原因
	        /// </summary>
	        [XmlElement("returnReason")]
	        public string ReturnReason { get; set; }
	
	        /// <summary>
	        /// 111
	        /// </summary>
	        [XmlElement("senderInfo")]
	        public SenderInfoDomain SenderInfo { get; set; }
	
	        /// <summary>
	        /// lbx11111
	        /// </summary>
	        [XmlElement("storeOrderCode")]
	        public string StoreOrderCode { get; set; }
	
	        /// <summary>
	        /// wh_1
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

        #endregion
    }
}
