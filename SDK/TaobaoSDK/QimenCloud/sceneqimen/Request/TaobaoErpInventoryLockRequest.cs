using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.erp.inventory.lock
    /// </summary>
    public class TaobaoErpInventoryLockRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoErpInventoryLockResponse>
    {
        /// <summary>
        /// 金额
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 客户代码
        /// </summary>
        public string CustomerCode { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 订单创建日期
        /// </summary>
        public string OrderCreateTime { get; set; }

        /// <summary>
        /// 源单据号
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 业务类型（Lock=库存锁定）
        /// </summary>
        public string OrderType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 商品详情
        /// </summary>
        public string Sku { get; set; }

        public List<SkuListDomain> Sku_ { set { this.Sku = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 仓库代码（发货仓库）
        /// </summary>
        public string WarehouseCode { get; set; }

        /// <summary>
        /// 库区代码
        /// </summary>
        public string WhareaTypeCode { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.erp.inventory.lock";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("Amount", this.Amount);
            parameters.Add("Number", this.Number);
            parameters.Add("customerCode", this.CustomerCode);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("orderCreateTime", this.OrderCreateTime);
            parameters.Add("orderId", this.OrderId);
            parameters.Add("orderType", this.OrderType);
            parameters.Add("remark", this.Remark);
            parameters.Add("sku", this.Sku);
            parameters.Add("warehouseCode", this.WarehouseCode);
            parameters.Add("whareaTypeCode", this.WhareaTypeCode);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("sku", this.Sku, 5000);
        }

	/// <summary>
/// SkuListDomain Data Structure.
/// </summary>
[Serializable]

public class SkuListDomain : TopObject
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 颜色编号
	        /// </summary>
	        [XmlElement("colorCode")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 商品数量
	        /// </summary>
	        [XmlElement("items_number")]
	        public string ItemsNumber { get; set; }
	
	        /// <summary>
	        /// 标准价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 尺寸编号
	        /// </summary>
	        [XmlElement("sizeCode")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 款编号
	        /// </summary>
	        [XmlElement("styleCode")]
	        public string StyleCode { get; set; }
}

        #endregion
    }
}
