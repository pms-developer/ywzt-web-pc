using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.sceneqimen.Request
{
    /// <summary>
    /// TOP API: taobao.crm.goods.list.get
    /// </summary>
    public class TaobaoCrmGoodsListGetRequest : BaseQimenCloudRequest<QimenCloud.Api.sceneqimen.Response.TaobaoCrmGoodsListGetResponse>
    {
        /// <summary>
        /// 路由参数
        /// </summary>
        public string Customerid { get; set; }

        /// <summary>
        /// 查询结束时间
        /// </summary>
        public string EndModified { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public string ExtendProps { get; set; }

        /// <summary>
        /// 支持的传入字段 goodsSn,goodsName,sizeRangeCode,sizeRangeName,brandCode,brandName,catCode,catName,topCatCode,topCatName,seriesCode,seriesName,yearCode,yearName,seasonCode,seasonName,dwCode,dwName,goodsWeight,marketPrice,shopPrice,ckj,cbj,qdtjd,qdtjd_start_time,qdtjd_end_time,ghsCode,ghsName,goodsDesc,created,modified,is_delete,goods_id
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// 页码: 取值范围:大于零的整数
        /// </summary>
        public Nullable<long> PageNo { get; set; }

        /// <summary>
        /// 每页条数。取值范围:大于零的整数; 默认值:20;最大值:100
        /// </summary>
        public Nullable<long> PageSize { get; set; }

        /// <summary>
        /// 查询起始时间
        /// </summary>
        public string StartModified { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "taobao.crm.goods.list.get";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("customerid", this.Customerid);
            parameters.Add("endModified", this.EndModified);
            parameters.Add("extendProps", this.ExtendProps);
            parameters.Add("fields", this.Fields);
            parameters.Add("pageNo", this.PageNo);
            parameters.Add("pageSize", this.PageSize);
            parameters.Add("startModified", this.StartModified);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("endModified", this.EndModified);
            RequestValidator.ValidateMaxLength("endModified", this.EndModified, 64);
            RequestValidator.ValidateRequired("fields", this.Fields);
            RequestValidator.ValidateRequired("startModified", this.StartModified);
            RequestValidator.ValidateMaxLength("startModified", this.StartModified, 64);
        }

        #endregion
    }
}
