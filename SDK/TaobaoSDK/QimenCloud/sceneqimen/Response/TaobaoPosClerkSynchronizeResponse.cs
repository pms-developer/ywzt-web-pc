using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoPosClerkSynchronizeResponse.
    /// </summary>
    public class TaobaoPosClerkSynchronizeResponse : QimenCloudResponse
    {
        /// <summary>
        /// null
        /// </summary>
        [XmlArray("clerk")]
        [XmlArrayItem("struct")]
        public List<StructDomain> Clerk { get; set; }

        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// POS中的店员编码
	        /// </summary>
	        [XmlElement("clerkCode")]
	        public string ClerkCode { get; set; }
	
	        /// <summary>
	        /// 店员名称
	        /// </summary>
	        [XmlElement("clerkName")]
	        public string ClerkName { get; set; }
	
	        /// <summary>
	        /// 最后修改的结束时间
	        /// </summary>
	        [XmlElement("endTime")]
	        public string EndTime { get; set; }
	
	        /// <summary>
	        /// 最后修改的起始时间
	        /// </summary>
	        [XmlElement("startTime")]
	        public string StartTime { get; set; }
	
	        /// <summary>
	        /// 门店编码
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
}

    }
}
