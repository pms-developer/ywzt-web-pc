using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoCrmExtRefundGetResponse.
    /// </summary>
    public class TaobaoCrmExtRefundGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        [XmlElement("data")]
        public DataDomain Data { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// OrderListGetDomain Data Structure.
/// </summary>
[Serializable]

public class OrderListGetDomain : TopObject
{
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("bz")]
	        public string Bz { get; set; }
	
	        /// <summary>
	        /// 单据编号
	        /// </summary>
	        [XmlElement("djbh")]
	        public string Djbh { get; set; }
	
	        /// <summary>
	        /// 消费类型
	        /// </summary>
	        [XmlElement("djlx")]
	        public string Djlx { get; set; }
	
	        /// <summary>
	        /// 关联单据编号
	        /// </summary>
	        [XmlElement("dtdh")]
	        public string Dtdh { get; set; }
	
	        /// <summary>
	        /// 关联交易号
	        /// </summary>
	        [XmlElement("dtjyh")]
	        public string Dtjyh { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 单据类型(订单1，退单0)
	        /// </summary>
	        [XmlElement("is_order")]
	        public string IsOrder { get; set; }
	
	        /// <summary>
	        /// 支付方式编号
	        /// </summary>
	        [XmlElement("pay_id")]
	        public string PayId { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 渠道代码
	        /// </summary>
	        [XmlElement("qd_id")]
	        public string QdId { get; set; }
	
	        /// <summary>
	        /// 添加时间
	        /// </summary>
	        [XmlElement("rq")]
	        public string Rq { get; set; }
	
	        /// <summary>
	        /// 店铺代码
	        /// </summary>
	        [XmlElement("sd_id")]
	        public string SdId { get; set; }
}

	/// <summary>
/// PageDomain Data Structure.
/// </summary>
[Serializable]

public class PageDomain : TopObject
{
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageNo")]
	        public string PageNo { get; set; }
	
	        /// <summary>
	        /// 当前分页大小
	        /// </summary>
	        [XmlElement("pageSize")]
	        public string PageSize { get; set; }
	
	        /// <summary>
	        /// 总页数
	        /// </summary>
	        [XmlElement("pageTotal")]
	        public string PageTotal { get; set; }
	
	        /// <summary>
	        /// 总记录数
	        /// </summary>
	        [XmlElement("totalResult")]
	        public string TotalResult { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 列表详情
	        /// </summary>
	        [XmlArray("orderListGet")]
	        [XmlArrayItem("order_list_get")]
	        public List<OrderListGetDomain> OrderListGet { get; set; }
	
	        /// <summary>
	        /// 分页信息
	        /// </summary>
	        [XmlElement("page")]
	        public PageDomain Page { get; set; }
}

    }
}
