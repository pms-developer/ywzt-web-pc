using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoCrmSdGetResponse.
    /// </summary>
    public class TaobaoCrmSdGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        [XmlElement("data")]
        public DataDomain Data { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应结果:success|failure
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

	/// <summary>
/// PageDomain Data Structure.
/// </summary>
[Serializable]

public class PageDomain : TopObject
{
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageNo")]
	        public long PageNo { get; set; }
	
	        /// <summary>
	        /// 当前分页大小
	        /// </summary>
	        [XmlElement("pageSize")]
	        public long PageSize { get; set; }
	
	        /// <summary>
	        /// 总页数
	        /// </summary>
	        [XmlElement("pageTotal")]
	        public long PageTotal { get; set; }
	
	        /// <summary>
	        /// 总记录数
	        /// </summary>
	        [XmlElement("totalResult")]
	        public long TotalResult { get; set; }
}

	/// <summary>
/// SdListGetsDomain Data Structure.
/// </summary>
[Serializable]

public class SdListGetsDomain : TopObject
{
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("bz")]
	        public string Bz { get; set; }
	
	        /// <summary>
	        /// 保证金
	        /// </summary>
	        [XmlElement("bzj")]
	        public string Bzj { get; set; }
	
	        /// <summary>
	        /// 仓库代码
	        /// </summary>
	        [XmlElement("ckdm")]
	        public string Ckdm { get; set; }
	
	        /// <summary>
	        /// 电话
	        /// </summary>
	        [XmlElement("dh")]
	        public string Dh { get; set; }
	
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("dz")]
	        public string Dz { get; set; }
	
	        /// <summary>
	        /// 邮箱
	        /// </summary>
	        [XmlElement("email")]
	        public string Email { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 传真
	        /// </summary>
	        [XmlElement("fax")]
	        public string Fax { get; set; }
	
	        /// <summary>
	        /// 法人
	        /// </summary>
	        [XmlElement("frdb")]
	        public string Frdb { get; set; }
	
	        /// <summary>
	        /// 账户名称
	        /// </summary>
	        [XmlElement("hm")]
	        public string Hm { get; set; }
	
	        /// <summary>
	        /// 是否启用
	        /// </summary>
	        [XmlElement("is_qy")]
	        public string IsQy { get; set; }
	
	        /// <summary>
	        /// 商店代码
	        /// </summary>
	        [XmlElement("khdm")]
	        public string Khdm { get; set; }
	
	        /// <summary>
	        /// 开户行
	        /// </summary>
	        [XmlElement("khh")]
	        public string Khh { get; set; }
	
	        /// <summary>
	        /// 商店名称
	        /// </summary>
	        [XmlElement("khmc")]
	        public string Khmc { get; set; }
	
	        /// <summary>
	        /// 类别代码
	        /// </summary>
	        [XmlElement("lbdm")]
	        public string Lbdm { get; set; }
	
	        /// <summary>
	        /// 联系人
	        /// </summary>
	        [XmlElement("lxr")]
	        public string Lxr { get; set; }
	
	        /// <summary>
	        /// 来源名称
	        /// </summary>
	        [XmlElement("lylx_name")]
	        public string LylxName { get; set; }
	
	        /// <summary>
	        /// 渠道代码
	        /// </summary>
	        [XmlElement("qddm")]
	        public string Qddm { get; set; }
	
	        /// <summary>
	        /// 区域代码
	        /// </summary>
	        [XmlElement("qy_code")]
	        public string QyCode { get; set; }
	
	        /// <summary>
	        /// 员工代码
	        /// </summary>
	        [XmlElement("sf_no")]
	        public string SfNo { get; set; }
	
	        /// <summary>
	        /// 手机
	        /// </summary>
	        [XmlElement("sj")]
	        public string Sj { get; set; }
	
	        /// <summary>
	        /// 信用金额
	        /// </summary>
	        [XmlElement("xyje")]
	        public string Xyje { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("yb")]
	        public string Yb { get; set; }
	
	        /// <summary>
	        /// 帐号
	        /// </summary>
	        [XmlElement("yhzh")]
	        public string Yhzh { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("bz")]
	        public string Bz { get; set; }
	
	        /// <summary>
	        /// 法人
	        /// </summary>
	        [XmlElement("frdb")]
	        public string Frdb { get; set; }
	
	        /// <summary>
	        /// 分页信息
	        /// </summary>
	        [XmlElement("page")]
	        public PageDomain Page { get; set; }
	
	        /// <summary>
	        /// 商店列表
	        /// </summary>
	        [XmlArray("sdListGet")]
	        [XmlArrayItem("sdListGets")]
	        public List<SdListGetsDomain> SdListGet { get; set; }
	
	        /// <summary>
	        /// 帐号
	        /// </summary>
	        [XmlElement("yhzh")]
	        public string Yhzh { get; set; }
}

    }
}
