using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoIcpOrderStockinordermessagetoerpResponse.
    /// </summary>
    public class TaobaoIcpOrderStockinordermessagetoerpResponse : QimenCloudResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// 响应码:CD001
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 响应结果:failure
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
	
	        /// <summary>
	        /// 系统异常请重试
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
