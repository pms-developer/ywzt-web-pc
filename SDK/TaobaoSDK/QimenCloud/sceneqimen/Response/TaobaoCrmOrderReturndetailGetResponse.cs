using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoCrmOrderReturndetailGetResponse.
    /// </summary>
    public class TaobaoCrmOrderReturndetailGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        [XmlElement("data")]
        public DataDomain Data { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应结果:success|failure
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

	/// <summary>
/// PageDomain Data Structure.
/// </summary>
[Serializable]

public class PageDomain : TopObject
{
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageNo")]
	        public long PageNo { get; set; }
	
	        /// <summary>
	        /// 当前分页大小
	        /// </summary>
	        [XmlElement("pageSize")]
	        public long PageSize { get; set; }
	
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageTotal")]
	        public long PageTotal { get; set; }
	
	        /// <summary>
	        /// 总记录数
	        /// </summary>
	        [XmlElement("totalResult")]
	        public long TotalResult { get; set; }
}

	/// <summary>
/// OrderReturnDetailGetsDomain Data Structure.
/// </summary>
[Serializable]

public class OrderReturnDetailGetsDomain : TopObject
{
	        /// <summary>
	        /// 条形码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 商品品牌ID
	        /// </summary>
	        [XmlElement("brand_id")]
	        public string BrandId { get; set; }
	
	        /// <summary>
	        /// 品牌名称
	        /// </summary>
	        [XmlElement("brand_name")]
	        public string BrandName { get; set; }
	
	        /// <summary>
	        /// 参考价
	        /// </summary>
	        [XmlElement("ckj")]
	        public string Ckj { get; set; }
	
	        /// <summary>
	        /// 商品颜色编码
	        /// </summary>
	        [XmlElement("color_code")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 商品颜色
	        /// </summary>
	        [XmlElement("color_name")]
	        public string ColorName { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 商品编号
	        /// </summary>
	        [XmlElement("goods_number")]
	        public string GoodsNumber { get; set; }
	
	        /// <summary>
	        /// 商品价格
	        /// </summary>
	        [XmlElement("goods_price")]
	        public string GoodsPrice { get; set; }
	
	        /// <summary>
	        /// 商品sn号
	        /// </summary>
	        [XmlElement("goods_sn")]
	        public string GoodsSn { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("payment")]
	        public string Payment { get; set; }
	
	        /// <summary>
	        /// 退货原因
	        /// </summary>
	        [XmlElement("return_reason")]
	        public string ReturnReason { get; set; }
	
	        /// <summary>
	        /// 退货原因补充
	        /// </summary>
	        [XmlElement("return_reason_ex")]
	        public string ReturnReasonEx { get; set; }
	
	        /// <summary>
	        /// 实收金额
	        /// </summary>
	        [XmlElement("share_price")]
	        public string SharePrice { get; set; }
	
	        /// <summary>
	        /// 尺寸编码
	        /// </summary>
	        [XmlElement("size_code")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 尺寸名称
	        /// </summary>
	        [XmlElement("size_name")]
	        public string SizeName { get; set; }
}

	/// <summary>
/// OrderReturnResendDetailGetsDomain Data Structure.
/// </summary>
[Serializable]

public class OrderReturnResendDetailGetsDomain : TopObject
{
	        /// <summary>
	        /// 条形码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 商品品牌ID
	        /// </summary>
	        [XmlElement("brand_id")]
	        public string BrandId { get; set; }
	
	        /// <summary>
	        /// 品牌名称
	        /// </summary>
	        [XmlElement("brand_name")]
	        public string BrandName { get; set; }
	
	        /// <summary>
	        /// 参考价
	        /// </summary>
	        [XmlElement("ckj")]
	        public string Ckj { get; set; }
	
	        /// <summary>
	        /// 商品颜色编码
	        /// </summary>
	        [XmlElement("color_code")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 商品颜色
	        /// </summary>
	        [XmlElement("color_name")]
	        public string ColorName { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 商品编号
	        /// </summary>
	        [XmlElement("goods_number")]
	        public string GoodsNumber { get; set; }
	
	        /// <summary>
	        /// 商品价格
	        /// </summary>
	        [XmlElement("goods_price")]
	        public string GoodsPrice { get; set; }
	
	        /// <summary>
	        /// 商品sn号
	        /// </summary>
	        [XmlElement("goods_sn")]
	        public string GoodsSn { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("payment")]
	        public string Payment { get; set; }
	
	        /// <summary>
	        /// 实收金额
	        /// </summary>
	        [XmlElement("share_price")]
	        public string SharePrice { get; set; }
	
	        /// <summary>
	        /// 尺寸编码
	        /// </summary>
	        [XmlElement("size_code")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 尺寸名称
	        /// </summary>
	        [XmlElement("size_name")]
	        public string SizeName { get; set; }
}

	/// <summary>
/// OrderReturnListGetsDomain Data Structure.
/// </summary>
[Serializable]

public class OrderReturnListGetsDomain : TopObject
{
	        /// <summary>
	        /// 下单时间
	        /// </summary>
	        [XmlElement("add_time")]
	        public string AddTime { get; set; }
	
	        /// <summary>
	        /// 交易code
	        /// </summary>
	        [XmlElement("deal_code")]
	        public string DealCode { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 发货仓库
	        /// </summary>
	        [XmlElement("fhck")]
	        public string Fhck { get; set; }
	
	        /// <summary>
	        /// 最后更新时间
	        /// </summary>
	        [XmlElement("last_update")]
	        public string LastUpdate { get; set; }
	
	        /// <summary>
	        /// 来源类型
	        /// </summary>
	        [XmlElement("lylx")]
	        public string Lylx { get; set; }
	
	        /// <summary>
	        /// 来源名称
	        /// </summary>
	        [XmlElement("lylx_name")]
	        public string LylxName { get; set; }
	
	        /// <summary>
	        /// 退单详细信息列表
	        /// </summary>
	        [XmlArray("orderReturnDetailGets")]
	        [XmlArrayItem("OrderReturnDetailGets")]
	        public List<OrderReturnDetailGetsDomain> OrderReturnDetailGets { get; set; }
	
	        /// <summary>
	        /// 换货商品详情列表
	        /// </summary>
	        [XmlArray("orderReturnResendDetailGet")]
	        [XmlArrayItem("OrderReturnResendDetailGets")]
	        public List<OrderReturnResendDetailGetsDomain> OrderReturnResendDetailGet { get; set; }
	
	        /// <summary>
	        /// 交易状态
	        /// </summary>
	        [XmlElement("pay_status")]
	        public string PayStatus { get; set; }
	
	        /// <summary>
	        /// 渠道编码
	        /// </summary>
	        [XmlElement("qd_code")]
	        public string QdCode { get; set; }
	
	        /// <summary>
	        /// 渠道名称
	        /// </summary>
	        [XmlElement("qd_name")]
	        public string QdName { get; set; }
	
	        /// <summary>
	        /// 收件人地址
	        /// </summary>
	        [XmlElement("receiver_address")]
	        public string ReceiverAddress { get; set; }
	
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("receiver_city")]
	        public string ReceiverCity { get; set; }
	
	        /// <summary>
	        /// 国家
	        /// </summary>
	        [XmlElement("receiver_country")]
	        public string ReceiverCountry { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("receiver_district")]
	        public string ReceiverDistrict { get; set; }
	
	        /// <summary>
	        /// 收件人手机
	        /// </summary>
	        [XmlElement("receiver_mobile")]
	        public string ReceiverMobile { get; set; }
	
	        /// <summary>
	        /// 收件人姓名
	        /// </summary>
	        [XmlElement("receiver_name")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("receiver_province")]
	        public string ReceiverProvince { get; set; }
	
	        /// <summary>
	        /// 收件人电话
	        /// </summary>
	        [XmlElement("receiver_tel")]
	        public string ReceiverTel { get; set; }
	
	        /// <summary>
	        /// 退单sn号
	        /// </summary>
	        [XmlElement("relating_order_sn")]
	        public string RelatingOrderSn { get; set; }
	
	        /// <summary>
	        /// 实收金额
	        /// </summary>
	        [XmlElement("resend_payment")]
	        public string ResendPayment { get; set; }
	
	        /// <summary>
	        /// 平均折扣
	        /// </summary>
	        [XmlElement("return_discount_fee")]
	        public string ReturnDiscountFee { get; set; }
	
	        /// <summary>
	        /// 参考金额
	        /// </summary>
	        [XmlElement("return_market_goods_amount")]
	        public string ReturnMarketGoodsAmount { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("return_order_amount")]
	        public string ReturnOrderAmount { get; set; }
	
	        /// <summary>
	        /// 退单号
	        /// </summary>
	        [XmlElement("return_order_sn")]
	        public string ReturnOrderSn { get; set; }
	
	        /// <summary>
	        /// 退单快递公司代码
	        /// </summary>
	        [XmlElement("return_shipping_code")]
	        public string ReturnShippingCode { get; set; }
	
	        /// <summary>
	        /// 快递费
	        /// </summary>
	        [XmlElement("return_shipping_fee")]
	        public string ReturnShippingFee { get; set; }
	
	        /// <summary>
	        /// 快递方式
	        /// </summary>
	        [XmlElement("return_shipping_name")]
	        public string ReturnShippingName { get; set; }
	
	        /// <summary>
	        /// 退单物流单号
	        /// </summary>
	        [XmlElement("return_shipping_sn")]
	        public string ReturnShippingSn { get; set; }
	
	        /// <summary>
	        /// 订单商品的购买价格
	        /// </summary>
	        [XmlElement("return_shop_goods_amount")]
	        public string ReturnShopGoodsAmount { get; set; }
	
	        /// <summary>
	        /// 退单用户名
	        /// </summary>
	        [XmlElement("return_user_name")]
	        public string ReturnUserName { get; set; }
	
	        /// <summary>
	        /// 商店编码
	        /// </summary>
	        [XmlElement("sd_code")]
	        public string SdCode { get; set; }
	
	        /// <summary>
	        /// 淘宝分销ID
	        /// </summary>
	        [XmlElement("tbfx_id")]
	        public string TbfxId { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 退货列表
	        /// </summary>
	        [XmlArray("orderReturnListGet")]
	        [XmlArrayItem("OrderReturnListGets")]
	        public List<OrderReturnListGetsDomain> OrderReturnListGet { get; set; }
	
	        /// <summary>
	        /// 分页信息
	        /// </summary>
	        [XmlElement("page")]
	        public PageDomain Page { get; set; }
}

    }
}
