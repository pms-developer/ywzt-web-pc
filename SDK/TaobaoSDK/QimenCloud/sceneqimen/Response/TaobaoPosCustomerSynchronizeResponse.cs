using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoPosCustomerSynchronizeResponse.
    /// </summary>
    public class TaobaoPosCustomerSynchronizeResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// null
        /// </summary>
        [XmlArray("customer")]
        [XmlArrayItem("struct")]
        public List<StructDomain> Customer { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 顾客编码
	        /// </summary>
	        [XmlElement("customerCode")]
	        public string CustomerCode { get; set; }
	
	        /// <summary>
	        /// 顾客名称
	        /// </summary>
	        [XmlElement("customerName")]
	        public string CustomerName { get; set; }
}

    }
}
