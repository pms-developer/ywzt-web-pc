using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoAlphaxOpenJxtExpressResponse.
    /// </summary>
    public class TaobaoAlphaxOpenJxtExpressResponse : QimenCloudResponse
    {
        /// <summary>
        /// 返回值
        /// </summary>
        [XmlElement("result")]
        public ResultDODomain Result { get; set; }

	/// <summary>
/// ResultDODomain Data Structure.
/// </summary>
[Serializable]

public class ResultDODomain : TopObject
{
	        /// <summary>
	        /// 返回值
	        /// </summary>
	        [XmlElement("data")]
	        public string Data { get; set; }
	
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("errorCode")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误描述
	        /// </summary>
	        [XmlElement("errorMessage")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 是否正常
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
