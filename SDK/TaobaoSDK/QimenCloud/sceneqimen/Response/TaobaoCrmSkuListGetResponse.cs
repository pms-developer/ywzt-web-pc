using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoCrmSkuListGetResponse.
    /// </summary>
    public class TaobaoCrmSkuListGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        [XmlElement("data")]
        public DataDomain Data { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// PageDomain Data Structure.
/// </summary>
[Serializable]

public class PageDomain : TopObject
{
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageNo")]
	        public string PageNo { get; set; }
	
	        /// <summary>
	        /// 当前分页大小
	        /// </summary>
	        [XmlElement("pageSize")]
	        public string PageSize { get; set; }
	
	        /// <summary>
	        /// 总页数
	        /// </summary>
	        [XmlElement("pageTotal")]
	        public string PageTotal { get; set; }
	
	        /// <summary>
	        /// 总记录数
	        /// </summary>
	        [XmlElement("totalResult")]
	        public string TotalResult { get; set; }
}

	/// <summary>
/// SkuListGetDomain Data Structure.
/// </summary>
[Serializable]

public class SkuListGetDomain : TopObject
{
	        /// <summary>
	        /// 品牌代码
	        /// </summary>
	        [XmlElement("brandCode")]
	        public string BrandCode { get; set; }
	
	        /// <summary>
	        /// 品牌名称
	        /// </summary>
	        [XmlElement("brandName")]
	        public string BrandName { get; set; }
	
	        /// <summary>
	        /// 分类代码
	        /// </summary>
	        [XmlElement("catCode")]
	        public string CatCode { get; set; }
	
	        /// <summary>
	        /// 分类名称
	        /// </summary>
	        [XmlElement("catName")]
	        public string CatName { get; set; }
	
	        /// <summary>
	        /// 成本价
	        /// </summary>
	        [XmlElement("cbj")]
	        public string Cbj { get; set; }
	
	        /// <summary>
	        /// 参考价
	        /// </summary>
	        [XmlElement("ckj")]
	        public string Ckj { get; set; }
	
	        /// <summary>
	        /// 颜色代码
	        /// </summary>
	        [XmlElement("colorCode")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 颜色名称
	        /// </summary>
	        [XmlElement("colorName")]
	        public string ColorName { get; set; }
	
	        /// <summary>
	        /// 商品创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 商品条码
	        /// </summary>
	        [XmlElement("gbBarcode")]
	        public string GbBarcode { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("goodsName")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 商品编号
	        /// </summary>
	        [XmlElement("goodsSn")]
	        public string GoodsSn { get; set; }
	
	        /// <summary>
	        /// 商品简称
	        /// </summary>
	        [XmlElement("goodsSname")]
	        public string GoodsSname { get; set; }
	
	        /// <summary>
	        /// 商品重量
	        /// </summary>
	        [XmlElement("goodsWeight")]
	        public string GoodsWeight { get; set; }
	
	        /// <summary>
	        /// 最后更新时间
	        /// </summary>
	        [XmlElement("lastchanged")]
	        public string Lastchanged { get; set; }
	
	        /// <summary>
	        /// 市场价
	        /// </summary>
	        [XmlElement("marketPricePrice")]
	        public string MarketPricePrice { get; set; }
	
	        /// <summary>
	        /// 最后修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 调价单号
	        /// </summary>
	        [XmlElement("qdtjd")]
	        public string Qdtjd { get; set; }
	
	        /// <summary>
	        /// 渠道调价单结束时间
	        /// </summary>
	        [XmlElement("qdtjd_end_time")]
	        public string QdtjdEndTime { get; set; }
	
	        /// <summary>
	        /// 渠道调价单开始时间
	        /// </summary>
	        [XmlElement("qdtjd_start_time")]
	        public string QdtjdStartTime { get; set; }
	
	        /// <summary>
	        /// 季节代码
	        /// </summary>
	        [XmlElement("seasonCode")]
	        public string SeasonCode { get; set; }
	
	        /// <summary>
	        /// 季节名称
	        /// </summary>
	        [XmlElement("seasonName")]
	        public string SeasonName { get; set; }
	
	        /// <summary>
	        /// 系列代码
	        /// </summary>
	        [XmlElement("seriesCode")]
	        public string SeriesCode { get; set; }
	
	        /// <summary>
	        /// 系列名称
	        /// </summary>
	        [XmlElement("seriesName")]
	        public string SeriesName { get; set; }
	
	        /// <summary>
	        /// 售价
	        /// </summary>
	        [XmlElement("shopPrice")]
	        public string ShopPrice { get; set; }
	
	        /// <summary>
	        /// 69码
	        /// </summary>
	        [XmlElement("sixNineCode")]
	        public string SixNineCode { get; set; }
	
	        /// <summary>
	        /// 尺码代码
	        /// </summary>
	        [XmlElement("sizeCode")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 尺码名称
	        /// </summary>
	        [XmlElement("sizeName")]
	        public string SizeName { get; set; }
	
	        /// <summary>
	        /// 商品SKU
	        /// </summary>
	        [XmlElement("sku")]
	        public string Sku { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 分页信息
	        /// </summary>
	        [XmlElement("page")]
	        public PageDomain Page { get; set; }
	
	        /// <summary>
	        /// 商品sku列表
	        /// </summary>
	        [XmlArray("skuListGet")]
	        [XmlArrayItem("skuListGet")]
	        public List<SkuListGetDomain> SkuListGet { get; set; }
}

    }
}
