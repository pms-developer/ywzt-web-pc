using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoErpSupplierGetResponse.
    /// </summary>
    public class TaobaoErpSupplierGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 商品详情
        /// </summary>
        [XmlArray("item")]
        [XmlArrayItem("item")]
        public List<ItemDomain> Item { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 总计个数
        /// </summary>
        [XmlElement("total")]
        public long Total { get; set; }

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain : TopObject
{
	        /// <summary>
	        /// 创建日期
	        /// </summary>
	        [XmlElement("createDate")]
	        public string CreateDate { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("memo")]
	        public string Memo { get; set; }
	
	        /// <summary>
	        /// 手机
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 修改日期
	        /// </summary>
	        [XmlElement("modifyDate")]
	        public string ModifyDate { get; set; }
	
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("reveiveAddress")]
	        public string ReveiveAddress { get; set; }
	
	        /// <summary>
	        /// 联系人
	        /// </summary>
	        [XmlElement("reveiveName")]
	        public string ReveiveName { get; set; }
	
	        /// <summary>
	        /// 电话
	        /// </summary>
	        [XmlElement("tel")]
	        public string Tel { get; set; }
	
	        /// <summary>
	        /// 供应商编码
	        /// </summary>
	        [XmlElement("vendorCode")]
	        public string VendorCode { get; set; }
	
	        /// <summary>
	        /// 供应商ID
	        /// </summary>
	        [XmlElement("vendorID")]
	        public string VendorID { get; set; }
	
	        /// <summary>
	        /// 供应商名称
	        /// </summary>
	        [XmlElement("vendorName")]
	        public string VendorName { get; set; }
}

    }
}
