using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoCrmOrderDetailGetResponse.
    /// </summary>
    public class TaobaoCrmOrderDetailGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        [XmlElement("data")]
        public DataDomain Data { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应结果:success|failure
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

	/// <summary>
/// PageDomain Data Structure.
/// </summary>
[Serializable]

public class PageDomain : TopObject
{
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageNo")]
	        public long PageNo { get; set; }
	
	        /// <summary>
	        /// 当前分页大小
	        /// </summary>
	        [XmlElement("pageSize")]
	        public long PageSize { get; set; }
	
	        /// <summary>
	        /// 总页数
	        /// </summary>
	        [XmlElement("pageTotal")]
	        public long PageTotal { get; set; }
	
	        /// <summary>
	        /// 总记录数
	        /// </summary>
	        [XmlElement("totalResult")]
	        public long TotalResult { get; set; }
}

	/// <summary>
/// OrderDetailGetsDomain Data Structure.
/// </summary>
[Serializable]

public class OrderDetailGetsDomain : TopObject
{
	        /// <summary>
	        /// 商品条码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 品牌ID
	        /// </summary>
	        [XmlElement("brand_id")]
	        public long BrandId { get; set; }
	
	        /// <summary>
	        /// 品牌名称
	        /// </summary>
	        [XmlElement("brand_name")]
	        public string BrandName { get; set; }
	
	        /// <summary>
	        /// 参考价
	        /// </summary>
	        [XmlElement("ckj")]
	        public string Ckj { get; set; }
	
	        /// <summary>
	        /// 颜色代码
	        /// </summary>
	        [XmlElement("color_code")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 颜色名称
	        /// </summary>
	        [XmlElement("color_name")]
	        public string ColorName { get; set; }
	
	        /// <summary>
	        /// 交易号
	        /// </summary>
	        [XmlElement("deal_code")]
	        public string DealCode { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 商品数量
	        /// </summary>
	        [XmlElement("goods_number")]
	        public string GoodsNumber { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("goods_price")]
	        public string GoodsPrice { get; set; }
	
	        /// <summary>
	        /// 商品货号
	        /// </summary>
	        [XmlElement("goods_sn")]
	        public string GoodsSn { get; set; }
	
	        /// <summary>
	        /// 是否为赠品
	        /// </summary>
	        [XmlElement("is_largess")]
	        public string IsLargess { get; set; }
	
	        /// <summary>
	        /// 金额（money）
	        /// </summary>
	        [XmlElement("payment")]
	        public string Payment { get; set; }
	
	        /// <summary>
	        /// 实收价格
	        /// </summary>
	        [XmlElement("share_price")]
	        public string SharePrice { get; set; }
	
	        /// <summary>
	        /// 尺码代码
	        /// </summary>
	        [XmlElement("size_code")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 尺码名称
	        /// </summary>
	        [XmlElement("size_name")]
	        public string SizeName { get; set; }
}

	/// <summary>
/// OrderListGetsDomain Data Structure.
/// </summary>
[Serializable]

public class OrderListGetsDomain : TopObject
{
	        /// <summary>
	        /// 下单时间
	        /// </summary>
	        [XmlElement("add_time")]
	        public string AddTime { get; set; }
	
	        /// <summary>
	        /// 交易号
	        /// </summary>
	        [XmlElement("deal_code")]
	        public string DealCode { get; set; }
	
	        /// <summary>
	        /// 扩展字段
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 发货仓库
	        /// </summary>
	        [XmlElement("fhck")]
	        public string Fhck { get; set; }
	
	        /// <summary>
	        /// 是否换货单
	        /// </summary>
	        [XmlElement("is_hh")]
	        public string IsHh { get; set; }
	
	        /// <summary>
	        /// 最后更新时间
	        /// </summary>
	        [XmlElement("last_update")]
	        public string LastUpdate { get; set; }
	
	        /// <summary>
	        /// 来源类型
	        /// </summary>
	        [XmlElement("lylx")]
	        public long Lylx { get; set; }
	
	        /// <summary>
	        /// 来源名称
	        /// </summary>
	        [XmlElement("lylx_name")]
	        public string LylxName { get; set; }
	
	        /// <summary>
	        /// 参考金额：商品金额（市场价之和）
	        /// </summary>
	        [XmlElement("market_goods_amount")]
	        public string MarketGoodsAmount { get; set; }
	
	        /// <summary>
	        /// 消费数量
	        /// </summary>
	        [XmlElement("num")]
	        public string Num { get; set; }
	
	        /// <summary>
	        /// 订单商品
	        /// </summary>
	        [XmlArray("orderDetailGets")]
	        [XmlArrayItem("OrderDetailGets")]
	        public List<OrderDetailGetsDomain> OrderDetailGets { get; set; }
	
	        /// <summary>
	        /// 金额：订单应付金额（去除优惠券和积分抵现金额+快递费）
	        /// </summary>
	        [XmlElement("order_amount")]
	        public string OrderAmount { get; set; }
	
	        /// <summary>
	        /// 订单编号
	        /// </summary>
	        [XmlElement("order_sn")]
	        public string OrderSn { get; set; }
	
	        /// <summary>
	        /// 订单状态
	        /// </summary>
	        [XmlElement("order_status")]
	        public long OrderStatus { get; set; }
	
	        /// <summary>
	        /// 原交易号
	        /// </summary>
	        [XmlElement("ori_deal_code")]
	        public string OriDealCode { get; set; }
	
	        /// <summary>
	        /// 支付方式编号
	        /// </summary>
	        [XmlElement("pay_code")]
	        public string PayCode { get; set; }
	
	        /// <summary>
	        /// 支付方式名称
	        /// </summary>
	        [XmlElement("pay_name")]
	        public string PayName { get; set; }
	
	        /// <summary>
	        /// 付款状态
	        /// </summary>
	        [XmlElement("pay_status")]
	        public long PayStatus { get; set; }
	
	        /// <summary>
	        /// 支付时间
	        /// </summary>
	        [XmlElement("pay_time")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 渠道代码
	        /// </summary>
	        [XmlElement("qd_code")]
	        public string QdCode { get; set; }
	
	        /// <summary>
	        /// 渠道名称
	        /// </summary>
	        [XmlElement("qd_name")]
	        public string QdName { get; set; }
	
	        /// <summary>
	        /// 收货人地址
	        /// </summary>
	        [XmlElement("receiver_address")]
	        public string ReceiverAddress { get; set; }
	
	        /// <summary>
	        /// 市
	        /// </summary>
	        [XmlElement("receiver_city")]
	        public string ReceiverCity { get; set; }
	
	        /// <summary>
	        /// 国家
	        /// </summary>
	        [XmlElement("receiver_country")]
	        public string ReceiverCountry { get; set; }
	
	        /// <summary>
	        /// 区
	        /// </summary>
	        [XmlElement("receiver_district")]
	        public string ReceiverDistrict { get; set; }
	
	        /// <summary>
	        /// 收货人手机
	        /// </summary>
	        [XmlElement("receiver_mobile")]
	        public string ReceiverMobile { get; set; }
	
	        /// <summary>
	        /// 收货人姓名
	        /// </summary>
	        [XmlElement("receiver_name")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 省
	        /// </summary>
	        [XmlElement("receiver_province")]
	        public string ReceiverProvince { get; set; }
	
	        /// <summary>
	        /// 收货人电话
	        /// </summary>
	        [XmlElement("receiver_tel")]
	        public string ReceiverTel { get; set; }
	
	        /// <summary>
	        /// 商店代码
	        /// </summary>
	        [XmlElement("sd_code")]
	        public string SdCode { get; set; }
	
	        /// <summary>
	        /// 商家备注
	        /// </summary>
	        [XmlElement("seller_msg")]
	        public string SellerMsg { get; set; }
	
	        /// <summary>
	        /// 快递公司代码
	        /// </summary>
	        [XmlElement("shipping_code")]
	        public string ShippingCode { get; set; }
	
	        /// <summary>
	        /// 快递费
	        /// </summary>
	        [XmlElement("shipping_fee")]
	        public string ShippingFee { get; set; }
	
	        /// <summary>
	        /// 快递方式
	        /// </summary>
	        [XmlElement("shipping_name")]
	        public string ShippingName { get; set; }
	
	        /// <summary>
	        /// 物流单号
	        /// </summary>
	        [XmlElement("shipping_sn")]
	        public string ShippingSn { get; set; }
	
	        /// <summary>
	        /// 配送状态
	        /// </summary>
	        [XmlElement("shipping_status")]
	        public long ShippingStatus { get; set; }
	
	        /// <summary>
	        /// 发货时间
	        /// </summary>
	        [XmlElement("shipping_time_fh")]
	        public string ShippingTimeFh { get; set; }
	
	        /// <summary>
	        /// 实收金额
	        /// </summary>
	        [XmlElement("shop_goods_amount")]
	        public string ShopGoodsAmount { get; set; }
	
	        /// <summary>
	        /// 淘宝分销ID
	        /// </summary>
	        [XmlElement("tbfx_id")]
	        public string TbfxId { get; set; }
	
	        /// <summary>
	        /// 会员名
	        /// </summary>
	        [XmlElement("user_name")]
	        public string UserName { get; set; }
	
	        /// <summary>
	        /// 重量
	        /// </summary>
	        [XmlElement("weigh")]
	        public string Weigh { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 订单列表详情
	        /// </summary>
	        [XmlArray("orderListGet")]
	        [XmlArrayItem("OrderListGets")]
	        public List<OrderListGetsDomain> OrderListGet { get; set; }
	
	        /// <summary>
	        /// 分页信息
	        /// </summary>
	        [XmlElement("page")]
	        public PageDomain Page { get; set; }
}

    }
}
