using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoErpItemSyncResponse.
    /// </summary>
    public class TaobaoErpItemSyncResponse : QimenCloudResponse
    {
        /// <summary>
        /// 0成功(其他失败)
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 商品列表
        /// </summary>
        [XmlArray("item_list")]
        [XmlArrayItem("item_list")]
        public List<ItemListDomain> ItemList { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// ItemsDomain Data Structure.
/// </summary>
[Serializable]

public class ItemsDomain : TopObject
{
	        /// <summary>
	        /// 条形码+附加码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 条码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 附加码
	        /// </summary>
	        [XmlElement("fjcode")]
	        public string Fjcode { get; set; }
	
	        /// <summary>
	        /// 商品品牌
	        /// </summary>
	        [XmlElement("items_brand")]
	        public string ItemsBrand { get; set; }
	
	        /// <summary>
	        /// 商品类别
	        /// </summary>
	        [XmlElement("items_class")]
	        public string ItemsClass { get; set; }
	
	        /// <summary>
	        /// 商品类别ID
	        /// </summary>
	        [XmlElement("items_classid")]
	        public string ItemsClassid { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("items_code")]
	        public string ItemsCode { get; set; }
	
	        /// <summary>
	        /// 高
	        /// </summary>
	        [XmlElement("items_height")]
	        public string ItemsHeight { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("items_id")]
	        public string ItemsId { get; set; }
	
	        /// <summary>
	        /// 长
	        /// </summary>
	        [XmlElement("items_length")]
	        public string ItemsLength { get; set; }
	
	        /// <summary>
	        /// 商品名
	        /// </summary>
	        [XmlElement("items_name")]
	        public string ItemsName { get; set; }
	
	        /// <summary>
	        /// 产地
	        /// </summary>
	        [XmlElement("items_origin")]
	        public string ItemsOrigin { get; set; }
	
	        /// <summary>
	        /// 重量（克）
	        /// </summary>
	        [XmlElement("items_weight")]
	        public string ItemsWeight { get; set; }
	
	        /// <summary>
	        /// 宽
	        /// </summary>
	        [XmlElement("items_width")]
	        public string ItemsWidth { get; set; }
	
	        /// <summary>
	        /// 规格编号
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 单位
	        /// </summary>
	        [XmlElement("unit")]
	        public string Unit { get; set; }
	
	        /// <summary>
	        /// 仓库名
	        /// </summary>
	        [XmlElement("warehousename")]
	        public string Warehousename { get; set; }
}

	/// <summary>
/// ItemListDomain Data Structure.
/// </summary>
[Serializable]

public class ItemListDomain : TopObject
{
	        /// <summary>
	        /// 商品详情
	        /// </summary>
	        [XmlElement("items")]
	        public ItemsDomain Items { get; set; }
}

    }
}
