using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoQianniuCloudkefuAddressSelfModifyResponse.
    /// </summary>
    public class TaobaoQianniuCloudkefuAddressSelfModifyResponse : QimenCloudResponse
    {
        /// <summary>
        /// 修改地址返回结果
        /// </summary>
        [XmlElement("result")]
        public ResultDODomain Result { get; set; }

	/// <summary>
/// ResultDODomain Data Structure.
/// </summary>
[Serializable]

public class ResultDODomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("errorCode")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误详细描述信息
	        /// </summary>
	        [XmlElement("errorMsg")]
	        public string ErrorMsg { get; set; }
	
	        /// <summary>
	        /// 地址是否更新成功
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
