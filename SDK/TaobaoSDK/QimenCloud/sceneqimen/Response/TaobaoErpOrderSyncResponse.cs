using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoErpOrderSyncResponse.
    /// </summary>
    public class TaobaoErpOrderSyncResponse : QimenCloudResponse
    {
        /// <summary>
        /// 0成功(其他失败)
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("error_code")]
        public string ErrorCode { get; set; }

        /// <summary>
        /// 成功返回当前页数据条数；失败返回错误信息
        /// </summary>
        [XmlElement("error_info")]
        public string ErrorInfo { get; set; }

        /// <summary>
        /// 返回记录总条数
        /// </summary>
        [XmlElement("totalResults")]
        public long TotalResults { get; set; }

        /// <summary>
        /// 订单信息
        /// </summary>
        [XmlArray("trade_orders")]
        [XmlArrayItem("trade_orders")]
        public List<TradeOrdersDomain> TradeOrders { get; set; }

	/// <summary>
/// TradeOrderDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrderDomain : TopObject
{
	        /// <summary>
	        /// 支付宝交易号
	        /// </summary>
	        [XmlElement("alipay_no")]
	        public string AlipayNo { get; set; }
	
	        /// <summary>
	        /// 合计应收
	        /// </summary>
	        [XmlElement("all_total")]
	        public string AllTotal { get; set; }
	
	        /// <summary>
	        /// 是否开发票(0：不开发票;1：开发票)
	        /// </summary>
	        [XmlElement("binvoice")]
	        public string Binvoice { get; set; }
	
	        /// <summary>
	        /// 发票状态(0未开票1已开票2开票中3开票失败)
	        /// </summary>
	        [XmlElement("binvoicemade")]
	        public string Binvoicemade { get; set; }
	
	        /// <summary>
	        /// 买家支付宝账号
	        /// </summary>
	        [XmlElement("buyer_alipay_no")]
	        public string BuyerAlipayNo { get; set; }
	
	        /// <summary>
	        /// 买家留言
	        /// </summary>
	        [XmlElement("buyer_remark")]
	        public string BuyerRemark { get; set; }
	
	        /// <summary>
	        /// 取消原因
	        /// </summary>
	        [XmlElement("cancel_reason")]
	        public string CancelReason { get; set; }
	
	        /// <summary>
	        /// 审核时间
	        /// </summary>
	        [XmlElement("chk_time")]
	        public string ChkTime { get; set; }
	
	        /// <summary>
	        /// 交易佣金
	        /// </summary>
	        [XmlElement("commission_value")]
	        public string CommissionValue { get; set; }
	
	        /// <summary>
	        /// 国家
	        /// </summary>
	        [XmlElement("country")]
	        public string Country { get; set; }
	
	        /// <summary>
	        /// 抵扣金额
	        /// </summary>
	        [XmlElement("coupon_value")]
	        public string CouponValue { get; set; }
	
	        /// <summary>
	        /// 分销商名称
	        /// </summary>
	        [XmlElement("distributor_name")]
	        public string DistributorName { get; set; }
	
	        /// <summary>
	        /// 订单结束时间
	        /// </summary>
	        [XmlElement("end_time")]
	        public string EndTime { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 优惠金额
	        /// </summary>
	        [XmlElement("favourable_total")]
	        public string FavourableTotal { get; set; }
	
	        /// <summary>
	        /// 货品成本
	        /// </summary>
	        [XmlElement("goods_cost")]
	        public string GoodsCost { get; set; }
	
	        /// <summary>
	        /// 货款合计
	        /// </summary>
	        [XmlElement("goods_total")]
	        public string GoodsTotal { get; set; }
	
	        /// <summary>
	        /// 发票类型
	        /// </summary>
	        [XmlElement("invoice_kind")]
	        public string InvoiceKind { get; set; }
	
	        /// <summary>
	        /// 发票抬头
	        /// </summary>
	        [XmlElement("invoice_title")]
	        public string InvoiceTitle { get; set; }
	
	        /// <summary>
	        /// 发票号
	        /// </summary>
	        [XmlElement("invoiceno")]
	        public string Invoiceno { get; set; }
	
	        /// <summary>
	        /// 分销商实付金额
	        /// </summary>
	        [XmlElement("istributor_payment")]
	        public string IstributorPayment { get; set; }
	
	        /// <summary>
	        /// 物流公司ID
	        /// </summary>
	        [XmlElement("logistic_id")]
	        public string LogisticId { get; set; }
	
	        /// <summary>
	        /// 物流单号
	        /// </summary>
	        [XmlElement("logistic_no")]
	        public string LogisticNo { get; set; }
	
	        /// <summary>
	        /// 物流方式
	        /// </summary>
	        [XmlElement("logistname")]
	        public string Logistname { get; set; }
	
	        /// <summary>
	        /// 订单修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 其他成本
	        /// </summary>
	        [XmlElement("other_cost")]
	        public string OtherCost { get; set; }
	
	        /// <summary>
	        /// 包装成本
	        /// </summary>
	        [XmlElement("package_total")]
	        public string PackageTotal { get; set; }
	
	        /// <summary>
	        /// 付款时间
	        /// </summary>
	        [XmlElement("pay_time")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 支付方式
	        /// </summary>
	        [XmlElement("pay_type")]
	        public string PayType { get; set; }
	
	        /// <summary>
	        /// 实际邮资
	        /// </summary>
	        [XmlElement("postage")]
	        public string Postage { get; set; }
	
	        /// <summary>
	        /// 应收邮资
	        /// </summary>
	        [XmlElement("postage_total")]
	        public string PostageTotal { get; set; }
	
	        /// <summary>
	        /// 原始单号
	        /// </summary>
	        [XmlElement("raw_no")]
	        public string RawNo { get; set; }
	
	        /// <summary>
	        /// 实际结算
	        /// </summary>
	        [XmlElement("recv_total")]
	        public string RecvTotal { get; set; }
	
	        /// <summary>
	        /// 登记时间
	        /// </summary>
	        [XmlElement("reg_time")]
	        public string RegTime { get; set; }
	
	        /// <summary>
	        /// 业务员
	        /// </summary>
	        [XmlElement("seller")]
	        public string Seller { get; set; }
	
	        /// <summary>
	        /// 卖家追加备注
	        /// </summary>
	        [XmlElement("seller_append_remark")]
	        public string SellerAppendRemark { get; set; }
	
	        /// <summary>
	        /// 卖家备注
	        /// </summary>
	        [XmlElement("seller_flag")]
	        public string SellerFlag { get; set; }
	
	        /// <summary>
	        /// 客服备注
	        /// </summary>
	        [XmlElement("seller_remark")]
	        public string SellerRemark { get; set; }
	
	        /// <summary>
	        /// 店铺id
	        /// </summary>
	        [XmlElement("shop_id")]
	        public string ShopId { get; set; }
	
	        /// <summary>
	        /// 店铺名称
	        /// </summary>
	        [XmlElement("shop_name")]
	        public string ShopName { get; set; }
	
	        /// <summary>
	        /// 发货时间
	        /// </summary>
	        [XmlElement("sndtime")]
	        public string Sndtime { get; set; }
	
	        /// <summary>
	        /// 客付税额
	        /// </summary>
	        [XmlElement("tax_value")]
	        public string TaxValue { get; set; }
	
	        /// <summary>
	        /// 分销主订单号
	        /// </summary>
	        [XmlElement("tc_order_id")]
	        public string TcOrderId { get; set; }
	
	        /// <summary>
	        /// 订单利润
	        /// </summary>
	        [XmlElement("total_profit")]
	        public string TotalProfit { get; set; }
	
	        /// <summary>
	        /// 订单id
	        /// </summary>
	        [XmlElement("trade_id")]
	        public string TradeId { get; set; }
	
	        /// <summary>
	        /// 订单号
	        /// </summary>
	        [XmlElement("trade_no")]
	        public string TradeNo { get; set; }
	
	        /// <summary>
	        /// 订单状态（0被取消;1等待单;2待审核;3预订单;4待结算;5待发货;6生产等待;7发货在途;8代销发货;10委外发货;11已完成)
	        /// </summary>
	        [XmlElement("trade_status")]
	        public string TradeStatus { get; set; }
	
	        /// <summary>
	        /// 交易时间
	        /// </summary>
	        [XmlElement("trade_time")]
	        public string TradeTime { get; set; }
	
	        /// <summary>
	        /// 订单类型
	        /// </summary>
	        [XmlElement("trade_type")]
	        public string TradeType { get; set; }
	
	        /// <summary>
	        /// 发货仓库ID
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public string WarehouseId { get; set; }
	
	        /// <summary>
	        /// 仓库名称
	        /// </summary>
	        [XmlElement("warehouse_name")]
	        public string WarehouseName { get; set; }
}

	/// <summary>
/// OrderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class OrderInfoDomain : TopObject
{
	        /// <summary>
	        /// 条码+附加码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 是否组合装（True：是，其他否）
	        /// </summary>
	        [XmlElement("bfit")]
	        public string Bfit { get; set; }
	
	        /// <summary>
	        /// 条码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 分销商实付金额
	        /// </summary>
	        [XmlElement("distributor_payment")]
	        public string DistributorPayment { get; set; }
	
	        /// <summary>
	        /// 分摊实付金额
	        /// </summary>
	        [XmlElement("divide_order_fee")]
	        public string DivideOrderFee { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 是否赠品(0：表示非赠品;1：表示赠品)
	        /// </summary>
	        [XmlElement("gift")]
	        public string Gift { get; set; }
	
	        /// <summary>
	        /// 货品类别
	        /// </summary>
	        [XmlElement("goods_class")]
	        public string GoodsClass { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_code")]
	        public string GoodsCode { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 单位
	        /// </summary>
	        [XmlElement("goods_unit")]
	        public string GoodsUnit { get; set; }
	
	        /// <summary>
	        /// 子订单号
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
	
	        /// <summary>
	        /// 记录id
	        /// </summary>
	        [XmlElement("recid")]
	        public string Recid { get; set; }
	
	        /// <summary>
	        /// 退款编号
	        /// </summary>
	        [XmlElement("refund_id")]
	        public string RefundId { get; set; }
	
	        /// <summary>
	        /// 退款状态
	        /// </summary>
	        [XmlElement("refund_status")]
	        public string RefundStatus { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("sell_count")]
	        public string SellCount { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("sell_price")]
	        public string SellPrice { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("sell_total")]
	        public string SellTotal { get; set; }
	
	        /// <summary>
	        /// 规格编号
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 子订单状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 分销子订单号
	        /// </summary>
	        [XmlElement("tc_order_id")]
	        public string TcOrderId { get; set; }
}

	/// <summary>
/// PromotionDetailsDomain Data Structure.
/// </summary>
[Serializable]

public class PromotionDetailsDomain : TopObject
{
	        /// <summary>
	        /// 优惠金额
	        /// </summary>
	        [XmlElement("discount_fee")]
	        public string DiscountFee { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 赠送货号
	        /// </summary>
	        [XmlElement("gift_item_id")]
	        public string GiftItemId { get; set; }
	
	        /// <summary>
	        /// 赠送商品名称
	        /// </summary>
	        [XmlElement("gift_item_name")]
	        public string GiftItemName { get; set; }
	
	        /// <summary>
	        /// 赠送数量
	        /// </summary>
	        [XmlElement("gift_item_num")]
	        public string GiftItemNum { get; set; }
	
	        /// <summary>
	        /// 交易的主订单或子订单号
	        /// </summary>
	        [XmlElement("id")]
	        public string Id { get; set; }
	
	        /// <summary>
	        /// 优惠详情描述
	        /// </summary>
	        [XmlElement("promotion_desc")]
	        public string PromotionDesc { get; set; }
	
	        /// <summary>
	        /// 优惠信息的名称
	        /// </summary>
	        [XmlElement("promotion_name")]
	        public string PromotionName { get; set; }
}

	/// <summary>
/// TradeOrdersDomain Data Structure.
/// </summary>
[Serializable]

public class TradeOrdersDomain : TopObject
{
	        /// <summary>
	        /// 详情
	        /// </summary>
	        [XmlArray("order_info")]
	        [XmlArrayItem("order_info")]
	        public List<OrderInfoDomain> OrderInfo { get; set; }
	
	        /// <summary>
	        /// 促销详情
	        /// </summary>
	        [XmlArray("promotion_details")]
	        [XmlArrayItem("promotion_details")]
	        public List<PromotionDetailsDomain> PromotionDetails { get; set; }
	
	        /// <summary>
	        /// 订单详情
	        /// </summary>
	        [XmlElement("trade_order")]
	        public TradeOrderDomain TradeOrder { get; set; }
}

    }
}
