using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoBmsErptradeInterceptResponse.
    /// </summary>
    public class TaobaoBmsErptradeInterceptResponse : QimenCloudResponse
    {
        /// <summary>
        /// Response
        /// </summary>
        [XmlElement("response")]
        public ResponseDomain Response { get; set; }

	/// <summary>
/// ResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ResponseDomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 成功或失败
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
	
	        /// <summary>
	        /// 描述
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 同步标志
	        /// </summary>
	        [XmlElement("sync")]
	        public string Sync { get; set; }
}

    }
}
