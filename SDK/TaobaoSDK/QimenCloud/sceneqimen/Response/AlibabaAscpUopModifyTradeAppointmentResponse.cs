using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// AlibabaAscpUopModifyTradeAppointmentResponse.
    /// </summary>
    public class AlibabaAscpUopModifyTradeAppointmentResponse : QimenCloudResponse
    {
        /// <summary>
        /// 返回内容根节点
        /// </summary>
        [XmlElement("response")]
        public ResponseDomain Response { get; set; }

	/// <summary>
/// ResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ResponseDomain : TopObject
{
	        /// <summary>
	        /// 改约失败错误码 failure:改约失败，无法重试; exception:系统异常请重试
	        /// </summary>
	        [XmlElement("errorCode")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 改约失败，无法重试
	        /// </summary>
	        [XmlElement("errorMessage")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 改约结果 true:改约成功, false:改约失败
	        /// </summary>
	        [XmlElement("modifyResult")]
	        public bool ModifyResult { get; set; }
	
	        /// <summary>
	        /// 申请单号
	        /// </summary>
	        [XmlElement("requestOrderNo")]
	        public string RequestOrderNo { get; set; }
}

    }
}
