using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoIcpOrderStockoutordermessagetoerpResponse.
    /// </summary>
    public class TaobaoIcpOrderStockoutordermessagetoerpResponse : QimenCloudResponse
    {
        /// <summary>
        /// 返回结果
        /// </summary>
        [XmlElement("result")]
        public ResultDomain Result { get; set; }

	/// <summary>
/// ResultDomain Data Structure.
/// </summary>
[Serializable]

public class ResultDomain : TopObject
{
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("errorcode")]
	        public string Errorcode { get; set; }
	
	        /// <summary>
	        /// 错误提示
	        /// </summary>
	        [XmlElement("errormessage")]
	        public string Errormessage { get; set; }
	
	        /// <summary>
	        /// success|failure
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
}

    }
}
