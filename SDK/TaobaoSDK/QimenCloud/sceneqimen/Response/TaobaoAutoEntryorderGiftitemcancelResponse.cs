using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoAutoEntryorderGiftitemcancelResponse.
    /// </summary>
    public class TaobaoAutoEntryorderGiftitemcancelResponse : QimenCloudResponse
    {
        /// <summary>
        /// response
        /// </summary>
        [XmlElement("response")]
        public ResponseDomain Response { get; set; }

	/// <summary>
/// ResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ResponseDomain : TopObject
{
	        /// <summary>
	        /// 成功
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// success
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
	
	        /// <summary>
	        /// 成功
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
