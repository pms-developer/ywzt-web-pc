using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoQimenEntryorderCallbackResponse.
    /// </summary>
    public class TaobaoQimenEntryorderCallbackResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码:CD001
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 回传结果:success|failure
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

    }
}
