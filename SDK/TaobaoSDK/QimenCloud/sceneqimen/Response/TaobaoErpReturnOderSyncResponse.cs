using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoErpReturnOderSyncResponse.
    /// </summary>
    public class TaobaoErpReturnOderSyncResponse : QimenCloudResponse
    {
        /// <summary>
        /// 0成功(其他失败)
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 退换单列表
        /// </summary>
        [XmlArray("return_orders")]
        [XmlArrayItem("return_orders")]
        public List<ReturnOrdersDomain> ReturnOrders { get; set; }

        /// <summary>
        /// 返回记录总条数
        /// </summary>
        [XmlElement("totalResults")]
        public string TotalResults { get; set; }

	/// <summary>
/// ReturnOrderDomain Data Structure.
/// </summary>
[Serializable]

public class ReturnOrderDomain : TopObject
{
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("adr")]
	        public string Adr { get; set; }
	
	        /// <summary>
	        /// 退货原因
	        /// </summary>
	        [XmlElement("cause")]
	        public string Cause { get; set; }
	
	        /// <summary>
	        /// 结算时间
	        /// </summary>
	        [XmlElement("charge_time")]
	        public string ChargeTime { get; set; }
	
	        /// <summary>
	        /// 会员id
	        /// </summary>
	        [XmlElement("customerid")]
	        public string Customerid { get; set; }
	
	        /// <summary>
	        /// 会员名
	        /// </summary>
	        [XmlElement("customername")]
	        public string Customername { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 货品成本
	        /// </summary>
	        [XmlElement("goods_cost")]
	        public string GoodsCost { get; set; }
	
	        /// <summary>
	        /// 物流公司ID
	        /// </summary>
	        [XmlElement("logistic_id")]
	        public string LogisticId { get; set; }
	
	        /// <summary>
	        /// 物流单号
	        /// </summary>
	        [XmlElement("logistic_no")]
	        public string LogisticNo { get; set; }
	
	        /// <summary>
	        /// 换出货品订单ID
	        /// </summary>
	        [XmlElement("new_id")]
	        public string NewId { get; set; }
	
	        /// <summary>
	        /// 换货店铺名称
	        /// </summary>
	        [XmlElement("newshop_name")]
	        public string NewshopName { get; set; }
	
	        /// <summary>
	        /// 昵称
	        /// </summary>
	        [XmlElement("nickname")]
	        public string Nickname { get; set; }
	
	        /// <summary>
	        /// 原关联订单号
	        /// </summary>
	        [XmlElement("old_id")]
	        public string OldId { get; set; }
	
	        /// <summary>
	        /// 原店铺名称
	        /// </summary>
	        [XmlElement("oldshop_name")]
	        public string OldshopName { get; set; }
	
	        /// <summary>
	        /// 包装成本
	        /// </summary>
	        [XmlElement("package_total")]
	        public string PackageTotal { get; set; }
	
	        /// <summary>
	        /// 结算金额
	        /// </summary>
	        [XmlElement("pay_total")]
	        public string PayTotal { get; set; }
	
	        /// <summary>
	        /// 应收邮资
	        /// </summary>
	        [XmlElement("postage_total")]
	        public string PostageTotal { get; set; }
	
	        /// <summary>
	        /// 原始单号
	        /// </summary>
	        [XmlElement("raw_no")]
	        public string RawNo { get; set; }
	
	        /// <summary>
	        /// 收货时间
	        /// </summary>
	        [XmlElement("rcvtime")]
	        public string Rcvtime { get; set; }
	
	        /// <summary>
	        /// 登记时间
	        /// </summary>
	        [XmlElement("reg_time")]
	        public string RegTime { get; set; }
	
	        /// <summary>
	        /// 卖家备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 应退合计
	        /// </summary>
	        [XmlElement("return_total")]
	        public string ReturnTotal { get; set; }
	
	        /// <summary>
	        /// 业务员
	        /// </summary>
	        [XmlElement("seller")]
	        public string Seller { get; set; }
	
	        /// <summary>
	        /// 联系电话
	        /// </summary>
	        [XmlElement("tel")]
	        public string Tel { get; set; }
	
	        /// <summary>
	        /// 退货金额
	        /// </summary>
	        [XmlElement("total_pay")]
	        public string TotalPay { get; set; }
	
	        /// <summary>
	        /// 退换利润
	        /// </summary>
	        [XmlElement("total_profit")]
	        public string TotalProfit { get; set; }
	
	        /// <summary>
	        /// 换货金额
	        /// </summary>
	        [XmlElement("total_rcv")]
	        public string TotalRcv { get; set; }
	
	        /// <summary>
	        /// 退换ID
	        /// </summary>
	        [XmlElement("trade_id")]
	        public string TradeId { get; set; }
	
	        /// <summary>
	        /// 退换单号
	        /// </summary>
	        [XmlElement("trade_no")]
	        public string TradeNo { get; set; }
	
	        /// <summary>
	        /// 0待收货;1待结算;2被取消;3已完成;4待审核
	        /// </summary>
	        [XmlElement("trade_status")]
	        public string TradeStatus { get; set; }
	
	        /// <summary>
	        /// 原订单类型
	        /// </summary>
	        [XmlElement("tradetype")]
	        public string Tradetype { get; set; }
	
	        /// <summary>
	        /// 退货仓库ID
	        /// </summary>
	        [XmlElement("warehouse_id")]
	        public string WarehouseId { get; set; }
	
	        /// <summary>
	        /// 换货仓库ID
	        /// </summary>
	        [XmlElement("warehouse_id2")]
	        public string WarehouseId2 { get; set; }
}

	/// <summary>
/// OrderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class OrderInfoDomain : TopObject
{
	        /// <summary>
	        /// 条码+附加码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 条码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_code")]
	        public string GoodsCode { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 单位
	        /// </summary>
	        [XmlElement("goods_unit")]
	        public string GoodsUnit { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("sell_amount")]
	        public string SellAmount { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("sell_count")]
	        public string SellCount { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("sell_price")]
	        public string SellPrice { get; set; }
	
	        /// <summary>
	        /// 规格编号
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
}

	/// <summary>
/// ExchangeInfosDomain Data Structure.
/// </summary>
[Serializable]

public class ExchangeInfosDomain : TopObject
{
	        /// <summary>
	        /// 条码+附加码
	        /// </summary>
	        [XmlElement("barcode")]
	        public string Barcode { get; set; }
	
	        /// <summary>
	        /// 条码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_code")]
	        public string GoodsCode { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goods_name")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 单位
	        /// </summary>
	        [XmlElement("goods_unit")]
	        public string GoodsUnit { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 更换单金额
	        /// </summary>
	        [XmlElement("sell_amount")]
	        public string SellAmount { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("sell_count")]
	        public string SellCount { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("sell_price")]
	        public string SellPrice { get; set; }
	
	        /// <summary>
	        /// 规格编号
	        /// </summary>
	        [XmlElement("spec_code")]
	        public string SpecCode { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("spec_id")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("spec_name")]
	        public string SpecName { get; set; }
}

	/// <summary>
/// ReturnOrdersDomain Data Structure.
/// </summary>
[Serializable]

public class ReturnOrdersDomain : TopObject
{
	        /// <summary>
	        /// 更换单信息
	        /// </summary>
	        [XmlElement("exchange_infos")]
	        public ExchangeInfosDomain ExchangeInfos { get; set; }
	
	        /// <summary>
	        /// 订单详情
	        /// </summary>
	        [XmlArray("order_info")]
	        [XmlArrayItem("order_info")]
	        public List<OrderInfoDomain> OrderInfo { get; set; }
	
	        /// <summary>
	        /// 退换单详情
	        /// </summary>
	        [XmlElement("return_order")]
	        public ReturnOrderDomain ReturnOrder { get; set; }
}

    }
}
