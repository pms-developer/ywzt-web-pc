using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoCrmQdGetResponse.
    /// </summary>
    public class TaobaoCrmQdGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        [XmlElement("data")]
        public DataDomain Data { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应结果:success|failure
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

	/// <summary>
/// PageDomain Data Structure.
/// </summary>
[Serializable]

public class PageDomain : TopObject
{
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageNo")]
	        public long PageNo { get; set; }
	
	        /// <summary>
	        /// 当前分页大小
	        /// </summary>
	        [XmlElement("pageSize")]
	        public long PageSize { get; set; }
	
	        /// <summary>
	        /// 总页数
	        /// </summary>
	        [XmlElement("pageTotal")]
	        public long PageTotal { get; set; }
	
	        /// <summary>
	        /// 总记录数
	        /// </summary>
	        [XmlElement("totalResult")]
	        public long TotalResult { get; set; }
}

	/// <summary>
/// QdListGetsDomain Data Structure.
/// </summary>
[Serializable]

public class QdListGetsDomain : TopObject
{
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("bz")]
	        public string Bz { get; set; }
	
	        /// <summary>
	        /// 电话
	        /// </summary>
	        [XmlElement("dh")]
	        public string Dh { get; set; }
	
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("dz")]
	        public string Dz { get; set; }
	
	        /// <summary>
	        /// 邮箱
	        /// </summary>
	        [XmlElement("email")]
	        public string Email { get; set; }
	
	        /// <summary>
	        /// 传真
	        /// </summary>
	        [XmlElement("fax")]
	        public string Fax { get; set; }
	
	        /// <summary>
	        /// 是否启用
	        /// </summary>
	        [XmlElement("is_qy")]
	        public string IsQy { get; set; }
	
	        /// <summary>
	        /// 发货价格类型
	        /// </summary>
	        [XmlElement("jgsd2")]
	        public string Jgsd2 { get; set; }
	
	        /// <summary>
	        /// 接收价格类型
	        /// </summary>
	        [XmlElement("jqsd1")]
	        public string Jqsd1 { get; set; }
	
	        /// <summary>
	        /// 渠道代码
	        /// </summary>
	        [XmlElement("khdm")]
	        public string Khdm { get; set; }
	
	        /// <summary>
	        /// 渠道名称
	        /// </summary>
	        [XmlElement("khmc")]
	        public string Khmc { get; set; }
	
	        /// <summary>
	        /// 联系人
	        /// </summary>
	        [XmlElement("lxr")]
	        public string Lxr { get; set; }
	
	        /// <summary>
	        /// 手机
	        /// </summary>
	        [XmlElement("sj")]
	        public string Sj { get; set; }
	
	        /// <summary>
	        /// 上级渠道代码
	        /// </summary>
	        [XmlElement("sjkh")]
	        public string Sjkh { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("yb")]
	        public string Yb { get; set; }
	
	        /// <summary>
	        /// 业务员
	        /// </summary>
	        [XmlElement("ywy")]
	        public string Ywy { get; set; }
	
	        /// <summary>
	        /// 接收折扣
	        /// </summary>
	        [XmlElement("zk1")]
	        public string Zk1 { get; set; }
	
	        /// <summary>
	        /// 发货折扣
	        /// </summary>
	        [XmlElement("zk2")]
	        public string Zk2 { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 分页信息
	        /// </summary>
	        [XmlElement("page")]
	        public PageDomain Page { get; set; }
	
	        /// <summary>
	        /// 渠道列表
	        /// </summary>
	        [XmlArray("qdListGet")]
	        [XmlArrayItem("qd_list_gets")]
	        public List<QdListGetsDomain> QdListGet { get; set; }
}

    }
}
