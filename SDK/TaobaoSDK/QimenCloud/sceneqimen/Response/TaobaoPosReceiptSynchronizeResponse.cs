using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoPosReceiptSynchronizeResponse.
    /// </summary>
    public class TaobaoPosReceiptSynchronizeResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// null
        /// </summary>
        [XmlArray("receipt")]
        [XmlArrayItem("struct")]
        public List<StructDomain> Receipt { get; set; }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 小票编码
	        /// </summary>
	        [XmlElement("orderCode")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// POS终端编码
	        /// </summary>
	        [XmlElement("posCode")]
	        public string PosCode { get; set; }
	
	        /// <summary>
	        /// 门店编码
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
}

    }
}
