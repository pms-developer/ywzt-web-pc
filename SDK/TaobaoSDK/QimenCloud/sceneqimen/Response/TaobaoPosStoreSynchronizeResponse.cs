using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoPosStoreSynchronizeResponse.
    /// </summary>
    public class TaobaoPosStoreSynchronizeResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 门店信息
        /// </summary>
        [XmlArray("store")]
        [XmlArrayItem("struct")]
        public List<StructDomain> Store { get; set; }

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 门店(仓储)编码
	        /// </summary>
	        [XmlElement("storeCode")]
	        public string StoreCode { get; set; }
	
	        /// <summary>
	        /// 门店(仓储)名称
	        /// </summary>
	        [XmlElement("storeName")]
	        public string StoreName { get; set; }
}

    }
}
