using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoAlphaxOpenJxtInvoiceResponse.
    /// </summary>
    public class TaobaoAlphaxOpenJxtInvoiceResponse : QimenCloudResponse
    {
        /// <summary>
        /// 返回值
        /// </summary>
        [XmlElement("result")]
        public DataDomain Result { get; set; }

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 具体描述
	        /// </summary>
	        [XmlElement("data")]
	        public string Data_ { get; set; }
	
	        /// <summary>
	        /// 错误码
	        /// </summary>
	        [XmlElement("errorCode")]
	        public string ErrorCode { get; set; }
	
	        /// <summary>
	        /// 错误信息
	        /// </summary>
	        [XmlElement("errorMessage")]
	        public string ErrorMessage { get; set; }
	
	        /// <summary>
	        /// 是否正常
	        /// </summary>
	        [XmlElement("success")]
	        public bool Success { get; set; }
}

    }
}
