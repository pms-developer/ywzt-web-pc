using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoIcpOrderStockinordecanceltoerpResponse.
    /// </summary>
    public class TaobaoIcpOrderStockinordecanceltoerpResponse : QimenCloudResponse
    {
        /// <summary>
        /// null
        /// </summary>
        [XmlElement("response")]
        public ResponseDomain Response { get; set; }

	/// <summary>
/// ResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ResponseDomain : TopObject
{
	        /// <summary>
	        /// 响应码,,string (50),
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 响应结果,success|failure,string (10),必填
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
	
	        /// <summary>
	        /// 响应信息,,string (100),
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
