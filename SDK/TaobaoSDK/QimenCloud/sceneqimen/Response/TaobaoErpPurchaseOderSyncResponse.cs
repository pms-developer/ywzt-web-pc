using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoErpPurchaseOderSyncResponse.
    /// </summary>
    public class TaobaoErpPurchaseOderSyncResponse : QimenCloudResponse
    {
        /// <summary>
        /// 0成功(其他失败)
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 采购单
        /// </summary>
        [XmlArray("orders")]
        [XmlArrayItem("orders")]
        public List<OrdersDomain> Orders { get; set; }

	/// <summary>
/// OrderDomain Data Structure.
/// </summary>
[Serializable]

public class OrderDomain : TopObject
{
	        /// <summary>
	        /// 审核时间
	        /// </summary>
	        [XmlElement("ChkDate")]
	        public string ChkDate { get; set; }
	
	        /// <summary>
	        /// 汇率
	        /// </summary>
	        [XmlElement("CurrencyRate")]
	        public string CurrencyRate { get; set; }
	
	        /// <summary>
	        /// 货款合计
	        /// </summary>
	        [XmlElement("GoodsTotal")]
	        public string GoodsTotal { get; set; }
	
	        /// <summary>
	        /// 采购单ID
	        /// </summary>
	        [XmlElement("OrderID")]
	        public string OrderID { get; set; }
	
	        /// <summary>
	        /// 采购单编号
	        /// </summary>
	        [XmlElement("OrderNO")]
	        public string OrderNO { get; set; }
	
	        /// <summary>
	        /// 其它费用
	        /// </summary>
	        [XmlElement("OtherFee")]
	        public string OtherFee { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("PriceDis")]
	        public string PriceDis { get; set; }
	
	        /// <summary>
	        /// 供货商ID
	        /// </summary>
	        [XmlElement("ProviderID")]
	        public string ProviderID { get; set; }
	
	        /// <summary>
	        /// 供货商名称
	        /// </summary>
	        [XmlElement("ProviderName")]
	        public string ProviderName { get; set; }
	
	        /// <summary>
	        /// 供货商备注
	        /// </summary>
	        [XmlElement("ProviderRemark")]
	        public string ProviderRemark { get; set; }
	
	        /// <summary>
	        /// 预计到货时间
	        /// </summary>
	        [XmlElement("RcvDate")]
	        public string RcvDate { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("Remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 仓库ID
	        /// </summary>
	        [XmlElement("WareHouseCode")]
	        public string WareHouseCode { get; set; }
	
	        /// <summary>
	        /// 当前状态(0待审核;1执行中;2取消;3已完成;4被终止)
	        /// </summary>
	        [XmlElement("curStatus")]
	        public string CurStatus { get; set; }
	
	        /// <summary>
	        /// 开单日期
	        /// </summary>
	        [XmlElement("regDate")]
	        public string RegDate { get; set; }
	
	        /// <summary>
	        /// 经办人
	        /// </summary>
	        [XmlElement("regOperator")]
	        public string RegOperator { get; set; }
}

	/// <summary>
/// OrderInfoDomain Data Structure.
/// </summary>
[Serializable]

public class OrderInfoDomain : TopObject
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("Amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 条码+附加码
	        /// </summary>
	        [XmlElement("BarCode")]
	        public string BarCode { get; set; }
	
	        /// <summary>
	        /// 条码
	        /// </summary>
	        [XmlElement("Code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 订购量
	        /// </summary>
	        [XmlElement("Count1")]
	        public string Count1 { get; set; }
	
	        /// <summary>
	        /// 货品ID
	        /// </summary>
	        [XmlElement("GoodsID")]
	        public string GoodsID { get; set; }
	
	        /// <summary>
	        /// 商品名
	        /// </summary>
	        [XmlElement("GoodsName")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 商品编号
	        /// </summary>
	        [XmlElement("GoodsNo")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("Price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("Remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 规格ID
	        /// </summary>
	        [XmlElement("SpecID")]
	        public string SpecID { get; set; }
	
	        /// <summary>
	        /// 规格
	        /// </summary>
	        [XmlElement("SpecName")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 单位
	        /// </summary>
	        [XmlElement("Unit")]
	        public string Unit { get; set; }
}

	/// <summary>
/// OrdersDomain Data Structure.
/// </summary>
[Serializable]

public class OrdersDomain : TopObject
{
	        /// <summary>
	        /// 采购单详情
	        /// </summary>
	        [XmlElement("order")]
	        public OrderDomain Order { get; set; }
	
	        /// <summary>
	        /// 详情
	        /// </summary>
	        [XmlElement("order_info")]
	        public OrderInfoDomain OrderInfo { get; set; }
}

    }
}
