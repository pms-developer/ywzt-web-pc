using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoOmsItemInventoryGetResponse.
    /// </summary>
    public class TaobaoOmsItemInventoryGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 商品库存详情
        /// </summary>
        [XmlArray("data")]
        [XmlArrayItem("data")]
        public List<DataDomain> Data { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 颜色代码
	        /// </summary>
	        [XmlElement("ColorCode")]
	        public string ColorCode { get; set; }
	
	        /// <summary>
	        /// 尺码代码
	        /// </summary>
	        [XmlElement("SizeCode")]
	        public string SizeCode { get; set; }
	
	        /// <summary>
	        /// 商品编号
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("number")]
	        public long Number { get; set; }
	
	        /// <summary>
	        /// 仓库代码
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

    }
}
