using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoBmsTradeConsignResponse.
    /// </summary>
    public class TaobaoBmsTradeConsignResponse : QimenCloudResponse
    {
        /// <summary>
        /// null
        /// </summary>
        [XmlElement("response")]
        public ResponseDomain Response { get; set; }

	/// <summary>
/// ResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ResponseDomain : TopObject
{
	        /// <summary>
	        /// 响应码, （100:成功）
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// 响应结果 success|failure
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
	
	        /// <summary>
	        /// 响应结果描述
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
