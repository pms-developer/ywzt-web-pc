using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoCrmCatListGetResponse.
    /// </summary>
    public class TaobaoCrmCatListGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        [XmlElement("data")]
        public DataDomain Data { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应结果:success|failure
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

	/// <summary>
/// PageDomain Data Structure.
/// </summary>
[Serializable]

public class PageDomain : TopObject
{
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageNo")]
	        public long PageNo { get; set; }
	
	        /// <summary>
	        /// 当前分页大小
	        /// </summary>
	        [XmlElement("pageSize")]
	        public long PageSize { get; set; }
	
	        /// <summary>
	        /// 总页数
	        /// </summary>
	        [XmlElement("pageTotal")]
	        public long PageTotal { get; set; }
	
	        /// <summary>
	        /// 总记录数
	        /// </summary>
	        [XmlElement("totalResult")]
	        public long TotalResult { get; set; }
}

	/// <summary>
/// StructDomain Data Structure.
/// </summary>
[Serializable]

public class StructDomain : TopObject
{
	        /// <summary>
	        /// 代码
	        /// </summary>
	        [XmlElement("catCode")]
	        public string CatCode { get; set; }
	
	        /// <summary>
	        /// 名称
	        /// </summary>
	        [XmlElement("catName")]
	        public string CatName { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("catNote")]
	        public string CatNote { get; set; }
	
	        /// <summary>
	        /// 修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 上级代码
	        /// </summary>
	        [XmlElement("pcatCode")]
	        public string PcatCode { get; set; }
	
	        /// <summary>
	        /// 上级名称
	        /// </summary>
	        [XmlElement("pcatName")]
	        public string PcatName { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 分类列表
	        /// </summary>
	        [XmlArray("catListGet")]
	        [XmlArrayItem("catListGets")]
	        public List<StructDomain> CatListGet { get; set; }
	
	        /// <summary>
	        /// 分页信息
	        /// </summary>
	        [XmlElement("page")]
	        public PageDomain Page { get; set; }
}

    }
}
