using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoUopTmsupdatemessagetoerpResponse.
    /// </summary>
    public class TaobaoUopTmsupdatemessagetoerpResponse : QimenCloudResponse
    {
        /// <summary>
        /// response
        /// </summary>
        [XmlElement("response")]
        public ResponseDomain Response { get; set; }

	/// <summary>
/// ResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ResponseDomain : TopObject
{
	        /// <summary>
	        /// 失败响应码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// success|failure
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
	
	        /// <summary>
	        /// 响应信息
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
