using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoCrmGoodsListGetResponse.
    /// </summary>
    public class TaobaoCrmGoodsListGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        [XmlElement("data")]
        public DataDomain Data { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 响应结果:success|failure
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

	/// <summary>
/// GoodsListGetDomain Data Structure.
/// </summary>
[Serializable]

public class GoodsListGetDomain : TopObject
{
	        /// <summary>
	        /// 品牌代码
	        /// </summary>
	        [XmlElement("brandCode")]
	        public string BrandCode { get; set; }
	
	        /// <summary>
	        /// 品牌名称
	        /// </summary>
	        [XmlElement("brandName")]
	        public string BrandName { get; set; }
	
	        /// <summary>
	        /// 分类代码
	        /// </summary>
	        [XmlElement("catCode")]
	        public string CatCode { get; set; }
	
	        /// <summary>
	        /// 分类名称
	        /// </summary>
	        [XmlElement("catName")]
	        public string CatName { get; set; }
	
	        /// <summary>
	        /// 成本价
	        /// </summary>
	        [XmlElement("cbj")]
	        public string Cbj { get; set; }
	
	        /// <summary>
	        /// 参考价
	        /// </summary>
	        [XmlElement("ckj")]
	        public string Ckj { get; set; }
	
	        /// <summary>
	        /// 商品创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 单位代码
	        /// </summary>
	        [XmlElement("dwCode")]
	        public string DwCode { get; set; }
	
	        /// <summary>
	        /// 单位名称
	        /// </summary>
	        [XmlElement("dwName")]
	        public string DwName { get; set; }
	
	        /// <summary>
	        /// 附加属性代码
	        /// </summary>
	        [XmlElement("fjsx1Code")]
	        public string Fjsx1Code { get; set; }
	
	        /// <summary>
	        /// 附加属性名称
	        /// </summary>
	        [XmlElement("fjsx1Name")]
	        public string Fjsx1Name { get; set; }
	
	        /// <summary>
	        /// 供货商代码
	        /// </summary>
	        [XmlElement("ghsCode")]
	        public string GhsCode { get; set; }
	
	        /// <summary>
	        /// 供货商名称
	        /// </summary>
	        [XmlElement("ghsName")]
	        public string GhsName { get; set; }
	
	        /// <summary>
	        /// 商品描述
	        /// </summary>
	        [XmlElement("goodsDesc")]
	        public string GoodsDesc { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("goodsName")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 商品货号
	        /// </summary>
	        [XmlElement("goodsSn")]
	        public string GoodsSn { get; set; }
	
	        /// <summary>
	        /// 商品重量
	        /// </summary>
	        [XmlElement("goodsWeight")]
	        public string GoodsWeight { get; set; }
	
	        /// <summary>
	        /// 商品ID
	        /// </summary>
	        [XmlElement("goods_id")]
	        public long GoodsId { get; set; }
	
	        /// <summary>
	        /// 是否删除
	        /// </summary>
	        [XmlElement("is_delete")]
	        public long IsDelete { get; set; }
	
	        /// <summary>
	        /// 市场价
	        /// </summary>
	        [XmlElement("marketPrice")]
	        public string MarketPrice { get; set; }
	
	        /// <summary>
	        /// 最后修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 调价单号
	        /// </summary>
	        [XmlElement("qdtjd")]
	        public string Qdtjd { get; set; }
	
	        /// <summary>
	        /// 渠道调价单结束时间
	        /// </summary>
	        [XmlElement("qdtjd_end_time")]
	        public string QdtjdEndTime { get; set; }
	
	        /// <summary>
	        /// 渠道调价单开始时间
	        /// </summary>
	        [XmlElement("qdtjd_start_time")]
	        public string QdtjdStartTime { get; set; }
	
	        /// <summary>
	        /// 季节代码
	        /// </summary>
	        [XmlElement("seasonCode")]
	        public string SeasonCode { get; set; }
	
	        /// <summary>
	        /// 季节名称
	        /// </summary>
	        [XmlElement("seasonName")]
	        public string SeasonName { get; set; }
	
	        /// <summary>
	        /// 系列代码
	        /// </summary>
	        [XmlElement("seriesCode")]
	        public string SeriesCode { get; set; }
	
	        /// <summary>
	        /// 系列名称
	        /// </summary>
	        [XmlElement("seriesName")]
	        public string SeriesName { get; set; }
	
	        /// <summary>
	        /// 售价
	        /// </summary>
	        [XmlElement("shopPrice")]
	        public string ShopPrice { get; set; }
	
	        /// <summary>
	        /// 尺码范围代码
	        /// </summary>
	        [XmlElement("sizeRangeCode")]
	        public string SizeRangeCode { get; set; }
	
	        /// <summary>
	        /// 尺码范围名称
	        /// </summary>
	        [XmlElement("sizeRangeName")]
	        public string SizeRangeName { get; set; }
	
	        /// <summary>
	        /// 商品大类代码
	        /// </summary>
	        [XmlElement("topCatCode")]
	        public string TopCatCode { get; set; }
	
	        /// <summary>
	        /// 商品大类名称
	        /// </summary>
	        [XmlElement("topCatName")]
	        public string TopCatName { get; set; }
	
	        /// <summary>
	        /// 年度代码
	        /// </summary>
	        [XmlElement("yearCode")]
	        public string YearCode { get; set; }
	
	        /// <summary>
	        /// 年度名称
	        /// </summary>
	        [XmlElement("yearName")]
	        public string YearName { get; set; }
}

	/// <summary>
/// PageDomain Data Structure.
/// </summary>
[Serializable]

public class PageDomain : TopObject
{
	        /// <summary>
	        /// 当前页数
	        /// </summary>
	        [XmlElement("pageNo")]
	        public long PageNo { get; set; }
	
	        /// <summary>
	        /// 当前分页大小
	        /// </summary>
	        [XmlElement("pageSize")]
	        public long PageSize { get; set; }
	
	        /// <summary>
	        /// 总页数
	        /// </summary>
	        [XmlElement("pageTotal")]
	        public long PageTotal { get; set; }
	
	        /// <summary>
	        /// 总记录数
	        /// </summary>
	        [XmlElement("totalResult")]
	        public long TotalResult { get; set; }
}

	/// <summary>
/// DataDomain Data Structure.
/// </summary>
[Serializable]

public class DataDomain : TopObject
{
	        /// <summary>
	        /// 商品列表
	        /// </summary>
	        [XmlArray("goodsListGet")]
	        [XmlArrayItem("999")]
	        public List<GoodsListGetDomain> GoodsListGet { get; set; }
	
	        /// <summary>
	        /// 分页信息
	        /// </summary>
	        [XmlElement("page")]
	        public PageDomain Page { get; set; }
}

    }
}
