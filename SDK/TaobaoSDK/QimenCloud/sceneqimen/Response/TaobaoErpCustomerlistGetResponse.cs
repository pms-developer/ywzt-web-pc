using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.sceneqimen.Response
{
    /// <summary>
    /// TaobaoErpCustomerlistGetResponse.
    /// </summary>
    public class TaobaoErpCustomerlistGetResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应码
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 响应结果
        /// </summary>
        [XmlElement("flag")]
        public string Flag { get; set; }

        /// <summary>
        /// 信息详情
        /// </summary>
        [XmlArray("item")]
        [XmlArrayItem("item")]
        public List<ItemDomain> Item { get; set; }

        /// <summary>
        /// 响应信息
        /// </summary>
        [XmlElement("message")]
        public string Message { get; set; }

        /// <summary>
        /// 总计个数
        /// </summary>
        [XmlElement("total")]
        public long Total { get; set; }

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain : TopObject
{
	        /// <summary>
	        /// 客户编码
	        /// </summary>
	        [XmlElement("CustomerCode")]
	        public string CustomerCode { get; set; }
	
	        /// <summary>
	        /// 客户名称
	        /// </summary>
	        [XmlElement("CustomerName")]
	        public string CustomerName { get; set; }
	
	        /// <summary>
	        /// 客户类型
	        /// </summary>
	        [XmlElement("CustomerType")]
	        public string CustomerType { get; set; }
	
	        /// <summary>
	        /// 邮箱
	        /// </summary>
	        [XmlElement("EMAIL")]
	        public string EMAIL { get; set; }
	
	        /// <summary>
	        /// 地址
	        /// </summary>
	        [XmlElement("ReceiveAddress")]
	        public string ReceiveAddress { get; set; }
	
	        /// <summary>
	        /// 联系人
	        /// </summary>
	        [XmlElement("ReveiveName")]
	        public string ReveiveName { get; set; }
	
	        /// <summary>
	        /// 渠道代码
	        /// </summary>
	        [XmlElement("SJCustomerCode")]
	        public string SJCustomerCode { get; set; }
	
	        /// <summary>
	        /// 电话
	        /// </summary>
	        [XmlElement("Tel")]
	        public string Tel { get; set; }
	
	        /// <summary>
	        /// 邮编
	        /// </summary>
	        [XmlElement("ZipCode")]
	        public string ZipCode { get; set; }
	
	        /// <summary>
	        /// 扩展属性
	        /// </summary>
	        [XmlElement("extendProps")]
	        public string ExtendProps { get; set; }
}

    }
}
