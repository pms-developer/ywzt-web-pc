using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenecvzlge28n0.Request
{
    /// <summary>
    /// TOP API: sales.rawtrade.pushself
    /// </summary>
    public class SalesRawtradePushselfRequest : BaseQimenCloudRequest<QimenCloud.Api.scenecvzlge28n0.Response.SalesRawtradePushselfResponse>
    {
        /// <summary>
        /// 主单列表
        /// </summary>
        public string RawTradeList { get; set; }

        public List<RawTradeDomain> RawTradeList_ { set { this.RawTradeList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 子单列表
        /// </summary>
        public string RawTradeOrderList { get; set; }

        public List<RawTradeOrderDomain> RawTradeOrderList_ { set { this.RawTradeOrderList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 店铺编号
        /// </summary>
        public string ShopNo { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "sales.rawtrade.pushself";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("rawTradeList", this.RawTradeList);
            parameters.Add("rawTradeOrderList", this.RawTradeOrderList);
            parameters.Add("shopNo", this.ShopNo);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("rawTradeList", this.RawTradeList);
            RequestValidator.ValidateObjectMaxListSize("rawTradeList", this.RawTradeList, 999999);
            RequestValidator.ValidateRequired("rawTradeOrderList", this.RawTradeOrderList);
            RequestValidator.ValidateObjectMaxListSize("rawTradeOrderList", this.RawTradeOrderList, 999999);
            RequestValidator.ValidateRequired("shopNo", this.ShopNo);
        }

	/// <summary>
/// RawTradeDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("rawTrade")]
public class RawTradeDomain : TopObject
{
	        /// <summary>
	        /// 付款时间
	        /// </summary>
	        [XmlElement("payTime")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 邮费
	        /// </summary>
	        [XmlElement("postAmount")]
	        public Nullable<long> PostAmount { get; set; }
	
	        /// <summary>
	        /// 订单金额（不包含邮费及优惠）
	        /// </summary>
	        [XmlElement("receivable")]
	        public Nullable<long> Receivable { get; set; }
	
	        /// <summary>
	        /// 收件人地址(不包含省市区)
	        /// </summary>
	        [XmlElement("receiverAddress")]
	        public string ReceiverAddress { get; set; }
	
	        /// <summary>
	        /// 收件人省市区
	        /// </summary>
	        [XmlElement("receiverArea")]
	        public string ReceiverArea { get; set; }
	
	        /// <summary>
	        /// 收件人手机号
	        /// </summary>
	        [XmlElement("receiverMobile")]
	        public string ReceiverMobile { get; set; }
	
	        /// <summary>
	        /// 收件人
	        /// </summary>
	        [XmlElement("receiverName")]
	        public string ReceiverName { get; set; }
	
	        /// <summary>
	        /// 收件人固话
	        /// </summary>
	        [XmlElement("receiverTelno")]
	        public string ReceiverTelno { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 订单编号
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
	
	        /// <summary>
	        /// 下单时间
	        /// </summary>
	        [XmlElement("tradeTime")]
	        public string TradeTime { get; set; }
}

	/// <summary>
/// RawTradeOrderDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("rawTradeOrder")]
public class RawTradeOrderDomain : TopObject
{
	        /// <summary>
	        /// 平台上的货品ID
	        /// </summary>
	        [XmlElement("goodsId")]
	        public string GoodsId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("goodsName")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("num")]
	        public Nullable<long> Num { get; set; }
	
	        /// <summary>
	        /// 子单编号(当前货品行的唯一标识)
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public Nullable<long> Price { get; set; }
	
	        /// <summary>
	        /// 平台上的规格ID
	        /// </summary>
	        [XmlElement("specId")]
	        public string SpecId { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("specName")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("specNo")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 订单编号
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
	
	        /// <summary>
	        /// 总价
	        /// </summary>
	        [XmlElement("totalPrice")]
	        public Nullable<long> TotalPrice { get; set; }
}

        #endregion
    }
}
