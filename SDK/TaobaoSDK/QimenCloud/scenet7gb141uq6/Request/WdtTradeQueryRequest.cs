using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenet7gb141uq6.Request
{
    /// <summary>
    /// TOP API: wdt.trade.query
    /// </summary>
    public class WdtTradeQueryRequest : BaseQimenCloudRequest<QimenCloud.Api.scenet7gb141uq6.Response.WdtTradeQueryResponse>
    {
        /// <summary>
        /// 授权
        /// </summary>
        public string Appkey { get; set; }

        /// <summary>
        /// 更新时间结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 货品详情
        /// </summary>
        public string GoodsList { get; set; }

        public List<GoodsListDomain> GoodsList_ { set { this.GoodsList = TopUtils.ObjectToJson(value); } } 

        /// <summary>
        /// 卖家账号
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// 更新时间开始时间
        /// </summary>
        public DateTime StartTime { get; set; }

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "wdt.trade.query";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("appkey", this.Appkey);
            parameters.Add("end_time", this.EndTime);
            parameters.Add("goods_list", this.GoodsList);
            parameters.Add("sid", this.Sid);
            parameters.Add("start_time", this.StartTime);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateRequired("appkey", this.Appkey);
            RequestValidator.ValidateRequired("end_time", this.EndTime);
            RequestValidator.ValidateObjectMaxListSize("goods_list", this.GoodsList, 999999);
            RequestValidator.ValidateRequired("sid", this.Sid);
            RequestValidator.ValidateRequired("start_time", this.StartTime);
        }

	/// <summary>
/// GoodsListDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("goodsList")]
public class GoodsListDomain : TopObject
{
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goods_no")]
	        public string GoodsNo { get; set; }
}

        #endregion
    }
}
