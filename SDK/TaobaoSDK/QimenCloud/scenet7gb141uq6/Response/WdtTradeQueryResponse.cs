using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenet7gb141uq6.Response
{
    /// <summary>
    /// WdtTradeQueryResponse.
    /// </summary>
    public class WdtTradeQueryResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        [XmlElement("status")]
        public long Status { get; set; }

        /// <summary>
        /// 信息
        /// </summary>
        [XmlElement("sub_msg")]
        public string SubMsg { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [XmlElement("sub_status")]
        public string SubStatus { get; set; }

    }
}
