using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenetaobao.Request
{
    /// <summary>
    /// TOP API: qimen.warehouseinfo.synchronize
    /// </summary>
    public class QimenWarehouseinfoSynchronizeRequest : BaseQimenCloudRequest<QimenCloud.Api.scenetaobao.Response.QimenWarehouseinfoSynchronizeResponse>
    {
        /// <summary>
        /// 请求报文
        /// </summary>
        public string Request { get; set; }

        public RequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "qimen.warehouseinfo.synchronize";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// WarehouseInfosDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("warehouseInfo")]
public class WarehouseInfosDomain : TopObject
{
	        /// <summary>
	        /// 地区，string（15）
	        /// </summary>
	        [XmlElement("area")]
	        public string Area { get; set; }
	
	        /// <summary>
	        /// 城市，string（15）
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址，string（50）
	        /// </summary>
	        [XmlElement("detailAddress")]
	        public string DetailAddress { get; set; }
	
	        /// <summary>
	        /// 负责人手机，string（20）
	        /// </summary>
	        [XmlElement("mobile")]
	        public string Mobile { get; set; }
	
	        /// <summary>
	        /// 仓库名称，string（50）
	        /// </summary>
	        [XmlElement("name")]
	        public string Name { get; set; }
	
	        /// <summary>
	        /// 省份，string（15）
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 仓库状态，string（20）
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 仓库电话，string（20）
	        /// </summary>
	        [XmlElement("tel")]
	        public string Tel { get; set; }
	
	        /// <summary>
	        /// 乡镇，string（15）
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
	
	        /// <summary>
	        /// 仓库编码，string（50）
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
	
	        /// <summary>
	        /// 仓库名称，string（50）
	        /// </summary>
	        [XmlElement("warehouseName")]
	        public string WarehouseName { get; set; }
}

	/// <summary>
/// RequestDomain Data Structure.
/// </summary>
[Serializable]

public class RequestDomain : TopObject
{
	        /// <summary>
	        /// 货主编码，string（50）
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 货主名称，string（50）
	        /// </summary>
	        [XmlElement("ownerName")]
	        public string OwnerName { get; set; }
	
	        /// <summary>
	        /// 仓库信息
	        /// </summary>
	        [XmlArray("warehouseInfos")]
	        [XmlArrayItem("warehouseInfo")]
	        public List<WarehouseInfosDomain> WarehouseInfos { get; set; }
}

        #endregion
    }
}
