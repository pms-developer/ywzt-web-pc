using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenetaobao.Request
{
    /// <summary>
    /// TOP API: qimen.shop.synchronize
    /// </summary>
    public class QimenShopSynchronizeRequest : BaseQimenCloudRequest<QimenCloud.Api.scenetaobao.Response.QimenShopSynchronizeResponse>
    {
        /// <summary>
        /// 请求
        /// </summary>
        public string Request { get; set; }

        public RequestDomain Request_ { set { this.Request = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "qimen.shop.synchronize";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("request", this.Request);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
        }

	/// <summary>
/// ShopAddressDomain Data Structure.
/// </summary>
[Serializable]

public class ShopAddressDomain : TopObject
{
	        /// <summary>
	        /// 区域, string (50)
	        /// </summary>
	        [XmlElement("area")]
	        public string Area { get; set; }
	
	        /// <summary>
	        /// 城市, string (50)
	        /// </summary>
	        [XmlElement("city")]
	        public string City { get; set; }
	
	        /// <summary>
	        /// 详细地址, string (200)
	        /// </summary>
	        [XmlElement("detailAddress")]
	        public string DetailAddress { get; set; }
	
	        /// <summary>
	        /// 省份, string (50)
	        /// </summary>
	        [XmlElement("province")]
	        public string Province { get; set; }
	
	        /// <summary>
	        /// 村镇, string (50)
	        /// </summary>
	        [XmlElement("town")]
	        public string Town { get; set; }
	
	        /// <summary>
	        /// 邮编, string (50)
	        /// </summary>
	        [XmlElement("zipCode")]
	        public string ZipCode { get; set; }
}

	/// <summary>
/// ShopDomain Data Structure.
/// </summary>
[Serializable]

public class ShopDomain : TopObject
{
	        /// <summary>
	        /// 货主编码, string (50) , 必填
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 平台店铺Id（如淘宝店铺Id）,  string (50)
	        /// </summary>
	        [XmlElement("platformShopCode")]
	        public string PlatformShopCode { get; set; }
	
	        /// <summary>
	        /// 平台店铺名称（如淘宝店铺名称）,  string (50)
	        /// </summary>
	        [XmlElement("platformShopName")]
	        public string PlatformShopName { get; set; }
	
	        /// <summary>
	        /// 店铺地址
	        /// </summary>
	        [XmlElement("shopAddress")]
	        public ShopAddressDomain ShopAddress { get; set; }
	
	        /// <summary>
	        /// ERP店铺编码,  string (50) ,  必填
	        /// </summary>
	        [XmlElement("shopCode")]
	        public string ShopCode { get; set; }
	
	        /// <summary>
	        /// ERP店铺名称，string (50)
	        /// </summary>
	        [XmlElement("shopName")]
	        public string ShopName { get; set; }
	
	        /// <summary>
	        /// 来源平台编码, string (50) , 必填,TB= 淘宝 、TM=天猫 、JD=京东、DD=当当、PP=拍拍、YX=易讯、EBAY=ebay、QQ=QQ网购、AMAZON=亚马逊、SN=苏宁、GM=国美、WPH=唯品会、JM=聚美、LF=乐蜂、MGJ=蘑菇街、JS=聚尚、PX=拍鞋、YT=银泰、YHD=1号店、VANCL=凡客、YL=邮乐、YG=优购、1688=阿里巴巴、POS=POS门店、MIA=蜜芽、GW=商家官网、CT=村淘、YJWD=云集微店、OTHERS=其他,  (只传英文编码)
	        /// </summary>
	        [XmlElement("sourcePlatformCode")]
	        public string SourcePlatformCode { get; set; }
	
	        /// <summary>
	        /// 仓库编码, string (50)，必填
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

	/// <summary>
/// RequestDomain Data Structure.
/// </summary>
[Serializable]

public class RequestDomain : TopObject
{
	        /// <summary>
	        /// add,update, 必填
	        /// </summary>
	        [XmlElement("actionType")]
	        public string ActionType { get; set; }
	
	        /// <summary>
	        /// 店铺
	        /// </summary>
	        [XmlElement("shop")]
	        public ShopDomain Shop { get; set; }
}

        #endregion
    }
}
