using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenetaobao.Response
{
    /// <summary>
    /// QimenWmsReturnapplyReportResponse.
    /// </summary>
    public class QimenWmsReturnapplyReportResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应对象
        /// </summary>
        [XmlElement("response")]
        public ResponseDomain Response { get; set; }

	/// <summary>
/// ResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ResponseDomain : TopObject
{
	        /// <summary>
	        /// 响应码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// success|failure
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
	
	        /// <summary>
	        /// 响应信息
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
}

    }
}
