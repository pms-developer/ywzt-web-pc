using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenetaobao.Response
{
    /// <summary>
    /// QimenWarehouseinfoSynchronizeResponse.
    /// </summary>
    public class QimenWarehouseinfoSynchronizeResponse : QimenCloudResponse
    {
        /// <summary>
        /// 响应报文
        /// </summary>
        [XmlElement("response")]
        public ResponseDomain Response { get; set; }

	/// <summary>
/// WarehouseInfoDomain Data Structure.
/// </summary>
[Serializable]

public class WarehouseInfoDomain : TopObject
{
	        /// <summary>
	        /// 仓库编码，string（50）
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

	/// <summary>
/// ResponseDomain Data Structure.
/// </summary>
[Serializable]

public class ResponseDomain : TopObject
{
	        /// <summary>
	        /// 响应码
	        /// </summary>
	        [XmlElement("code")]
	        public string Code { get; set; }
	
	        /// <summary>
	        /// success|failure
	        /// </summary>
	        [XmlElement("flag")]
	        public string Flag { get; set; }
	
	        /// <summary>
	        /// 响应信息
	        /// </summary>
	        [XmlElement("message")]
	        public string Message { get; set; }
	
	        /// <summary>
	        /// 仓库信息
	        /// </summary>
	        [XmlArray("warehouseInfos")]
	        [XmlArrayItem("warehouse_info")]
	        public List<WarehouseInfoDomain> WarehouseInfos { get; set; }
}

    }
}
