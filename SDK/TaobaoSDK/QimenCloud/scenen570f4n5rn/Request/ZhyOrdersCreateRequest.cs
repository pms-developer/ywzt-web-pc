using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenen570f4n5rn.Request
{
    /// <summary>
    /// TOP API: zhy.orders.create
    /// </summary>
    public class ZhyOrdersCreateRequest : BaseQimenCloudRequest<QimenCloud.Api.scenen570f4n5rn.Response.ZhyOrdersCreateResponse>
    {
        /// <summary>
        /// 子订单列表
        /// </summary>
        public string Orders { get; set; }

        public List<WDTOrderDataDomain> Orders_ { set { this.Orders = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "zhy.orders.create";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("orders", this.Orders);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("orders", this.Orders, 999999);
        }

	/// <summary>
/// WDTOrderEntryDataDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("wDTOrderEntryData")]
public class WDTOrderEntryDataDomain : TopObject
{
	        /// <summary>
	        /// 实发数量
	        /// </summary>
	        [XmlElement("actualNum")]
	        public Nullable<long> ActualNum { get; set; }
	
	        /// <summary>
	        /// 手工调整价
	        /// </summary>
	        [XmlElement("adjust")]
	        public string Adjust { get; set; }
	
	        /// <summary>
	        /// 创建时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 总折扣金额
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 货品id
	        /// </summary>
	        [XmlElement("goodsId")]
	        public Nullable<long> GoodsId { get; set; }
	
	        /// <summary>
	        /// 货品名称
	        /// </summary>
	        [XmlElement("goodsName")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编号
	        /// </summary>
	        [XmlElement("goodsNo")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 货品类别
	        /// </summary>
	        [XmlElement("goodsType")]
	        public string GoodsType { get; set; }
	
	        /// <summary>
	        /// 是否是赠品
	        /// </summary>
	        [XmlElement("isGift")]
	        public Nullable<bool> IsGift { get; set; }
	
	        /// <summary>
	        /// 最后修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 下单数量
	        /// </summary>
	        [XmlElement("num")]
	        public Nullable<long> Num { get; set; }
	
	        /// <summary>
	        /// 成交价
	        /// </summary>
	        [XmlElement("orderPrice")]
	        public string OrderPrice { get; set; }
	
	        /// <summary>
	        /// 已付
	        /// </summary>
	        [XmlElement("paid")]
	        public string Paid { get; set; }
	
	        /// <summary>
	        /// 平台货品ID
	        /// </summary>
	        [XmlElement("platformGoodsId")]
	        public Nullable<long> PlatformGoodsId { get; set; }
	
	        /// <summary>
	        /// 平台商品ID
	        /// </summary>
	        [XmlElement("platformSpecId")]
	        public Nullable<long> PlatformSpecId { get; set; }
	
	        /// <summary>
	        /// 标价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 主键
	        /// </summary>
	        [XmlElement("recId")]
	        public string RecId { get; set; }
	
	        /// <summary>
	        /// 退款状态
	        /// </summary>
	        [XmlElement("refundStatus")]
	        public string RefundStatus { get; set; }
	
	        /// <summary>
	        /// 分摊后总价
	        /// </summary>
	        [XmlElement("shareAmount")]
	        public string ShareAmount { get; set; }
	
	        /// <summary>
	        /// 分摊邮费
	        /// </summary>
	        [XmlElement("sharePost")]
	        public string SharePost { get; set; }
	
	        /// <summary>
	        /// 分摊后价格
	        /// </summary>
	        [XmlElement("sharePrice")]
	        public string SharePrice { get; set; }
	
	        /// <summary>
	        /// 规格名
	        /// </summary>
	        [XmlElement("specName")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 商家编码
	        /// </summary>
	        [XmlElement("specNo")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 子订单编号
	        /// </summary>
	        [XmlElement("srcOid")]
	        public string SrcOid { get; set; }
	
	        /// <summary>
	        /// 原始订单编号
	        /// </summary>
	        [XmlElement("srcTid")]
	        public string SrcTid { get; set; }
	
	        /// <summary>
	        /// 组合装分摊后总价
	        /// </summary>
	        [XmlElement("suiteAmount")]
	        public string SuiteAmount { get; set; }
	
	        /// <summary>
	        /// 组合装优惠
	        /// </summary>
	        [XmlElement("suiteDiscount")]
	        public string SuiteDiscount { get; set; }
	
	        /// <summary>
	        /// 组合装ID
	        /// </summary>
	        [XmlElement("suiteId")]
	        public string SuiteId { get; set; }
	
	        /// <summary>
	        /// 拆自组合装
	        /// </summary>
	        [XmlElement("suiteName")]
	        public string SuiteName { get; set; }
	
	        /// <summary>
	        /// 组合装编码
	        /// </summary>
	        [XmlElement("suiteNo")]
	        public string SuiteNo { get; set; }
	
	        /// <summary>
	        /// 组合装数量
	        /// </summary>
	        [XmlElement("suiteNum")]
	        public Nullable<long> SuiteNum { get; set; }
}

	/// <summary>
/// WDTOrderPromotionDataDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("wDTOrderPromotionData")]
public class WDTOrderPromotionDataDomain : TopObject
{
	        /// <summary>
	        /// 优惠金额
	        /// </summary>
	        [XmlElement("discountAmount")]
	        public string DiscountAmount { get; set; }
	
	        /// <summary>
	        /// 优惠描述
	        /// </summary>
	        [XmlElement("discountDescription")]
	        public string DiscountDescription { get; set; }
	
	        /// <summary>
	        /// 优惠名称
	        /// </summary>
	        [XmlElement("discountName")]
	        public string DiscountName { get; set; }
}

	/// <summary>
/// WDTOrderDataDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("wDTOrderData")]
public class WDTOrderDataDomain : TopObject
{
	        /// <summary>
	        /// 实际重量
	        /// </summary>
	        [XmlElement("actualWeight")]
	        public string ActualWeight { get; set; }
	
	        /// <summary>
	        /// 买家留言
	        /// </summary>
	        [XmlElement("buyerMessage")]
	        public string BuyerMessage { get; set; }
	
	        /// <summary>
	        /// 审核人
	        /// </summary>
	        [XmlElement("checkerId")]
	        public Nullable<long> CheckerId { get; set; }
	
	        /// <summary>
	        /// 审核人
	        /// </summary>
	        [XmlElement("checkerName")]
	        public string CheckerName { get; set; }
	
	        /// <summary>
	        /// 发货时间
	        /// </summary>
	        [XmlElement("consignTime")]
	        public string ConsignTime { get; set; }
	
	        /// <summary>
	        /// 订单生成时间
	        /// </summary>
	        [XmlElement("created")]
	        public string Created { get; set; }
	
	        /// <summary>
	        /// 客服备注
	        /// </summary>
	        [XmlElement("csRemark")]
	        public string CsRemark { get; set; }
	
	        /// <summary>
	        /// 币种
	        /// </summary>
	        [XmlElement("currency")]
	        public string Currency { get; set; }
	
	        /// <summary>
	        /// 折扣
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 子订单行列表
	        /// </summary>
	        [XmlArray("entries")]
	        [XmlArrayItem("wDTOrderEntryData")]
	        public List<WDTOrderEntryDataDomain> Entries { get; set; }
	
	        /// <summary>
	        /// 财审人
	        /// </summary>
	        [XmlElement("fcheckerId")]
	        public Nullable<long> FcheckerId { get; set; }
	
	        /// <summary>
	        /// 货品总额
	        /// </summary>
	        [XmlElement("goodsAmount")]
	        public string GoodsAmount { get; set; }
	
	        /// <summary>
	        /// 货品预估成本
	        /// </summary>
	        [XmlElement("goodsCost")]
	        public string GoodsCost { get; set; }
	
	        /// <summary>
	        /// 发票内容
	        /// </summary>
	        [XmlElement("invoiceContent")]
	        public string InvoiceContent { get; set; }
	
	        /// <summary>
	        /// 发票抬头
	        /// </summary>
	        [XmlElement("invoiceTitle")]
	        public string InvoiceTitle { get; set; }
	
	        /// <summary>
	        /// 发票类型
	        /// </summary>
	        [XmlElement("invoiceType")]
	        public string InvoiceType { get; set; }
	
	        /// <summary>
	        /// 是否开具发票
	        /// </summary>
	        [XmlElement("isInvoice")]
	        public Nullable<bool> IsInvoice { get; set; }
	
	        /// <summary>
	        /// 物流公司
	        /// </summary>
	        [XmlElement("logisticsName")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// 物流单号
	        /// </summary>
	        [XmlElement("logisticsNo")]
	        public string LogisticsNo { get; set; }
	
	        /// <summary>
	        /// 物流方式
	        /// </summary>
	        [XmlElement("logisticsType")]
	        public string LogisticsType { get; set; }
	
	        /// <summary>
	        /// 最后修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 其它费用
	        /// </summary>
	        [XmlElement("otherAmount")]
	        public string OtherAmount { get; set; }
	
	        /// <summary>
	        /// 已付金额
	        /// </summary>
	        [XmlElement("paid")]
	        public string Paid { get; set; }
	
	        /// <summary>
	        /// 交易流水单号
	        /// </summary>
	        [XmlElement("payId")]
	        public string PayId { get; set; }
	
	        /// <summary>
	        /// 付款时间
	        /// </summary>
	        [XmlElement("payTime")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 平台类型
	        /// </summary>
	        [XmlElement("platform")]
	        public string Platform { get; set; }
	
	        /// <summary>
	        /// 邮资
	        /// </summary>
	        [XmlElement("postAmount")]
	        public string PostAmount { get; set; }
	
	        /// <summary>
	        /// 打印备注
	        /// </summary>
	        [XmlElement("printRemark")]
	        public string PrintRemark { get; set; }
	
	        /// <summary>
	        /// 预估毛利
	        /// </summary>
	        [XmlElement("profit")]
	        public string Profit { get; set; }
	
	        /// <summary>
	        /// 优惠列表
	        /// </summary>
	        [XmlArray("promotions")]
	        [XmlArrayItem("wDTOrderPromotionData")]
	        public List<WDTOrderPromotionDataDomain> Promotions { get; set; }
	
	        /// <summary>
	        /// 退款状态
	        /// </summary>
	        [XmlElement("refundStatus")]
	        public string RefundStatus { get; set; }
	
	        /// <summary>
	        /// 业务员
	        /// </summary>
	        [XmlElement("salesmanId")]
	        public Nullable<long> SalesmanId { get; set; }
	
	        /// <summary>
	        /// 业务员
	        /// </summary>
	        [XmlElement("salesmanName")]
	        public string SalesmanName { get; set; }
	
	        /// <summary>
	        /// 店铺
	        /// </summary>
	        [XmlElement("shopName")]
	        public string ShopName { get; set; }
	
	        /// <summary>
	        /// 原始单号
	        /// </summary>
	        [XmlElement("srcTids")]
	        public string SrcTids { get; set; }
	
	        /// <summary>
	        /// 订单来源
	        /// </summary>
	        [XmlElement("tradeFrom")]
	        public string TradeFrom { get; set; }
	
	        /// <summary>
	        /// 旺店通订单编号
	        /// </summary>
	        [XmlElement("tradeNo")]
	        public string TradeNo { get; set; }
	
	        /// <summary>
	        /// 订单状态
	        /// </summary>
	        [XmlElement("tradeStatus")]
	        public string TradeStatus { get; set; }
	
	        /// <summary>
	        /// 下单时间
	        /// </summary>
	        [XmlElement("tradeTime")]
	        public string TradeTime { get; set; }
	
	        /// <summary>
	        /// 订单类型
	        /// </summary>
	        [XmlElement("tradeType")]
	        public string TradeType { get; set; }
	
	        /// <summary>
	        /// 仓库编号
	        /// </summary>
	        [XmlElement("warehouseNo")]
	        public string WarehouseNo { get; set; }
	
	        /// <summary>
	        /// 预估重量
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
}

        #endregion
    }
}
