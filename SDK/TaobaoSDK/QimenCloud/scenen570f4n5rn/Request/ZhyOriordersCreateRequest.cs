using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenen570f4n5rn.Request
{
    /// <summary>
    /// TOP API: zhy.oriorders.create
    /// </summary>
    public class ZhyOriordersCreateRequest : BaseQimenCloudRequest<QimenCloud.Api.scenen570f4n5rn.Response.ZhyOriordersCreateResponse>
    {
        /// <summary>
        /// 原始订单列表
        /// </summary>
        public string OriOrders { get; set; }

        public List<WDTOriOrderDataDomain> OriOrders_ { set { this.OriOrders = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "zhy.oriorders.create";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("oriOrders", this.OriOrders);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("oriOrders", this.OriOrders, 999999);
        }

	/// <summary>
/// WDTOriOrderEntryDataDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("wDTOriOrderEntryData")]
public class WDTOriOrderEntryDataDomain : TopObject
{
	        /// <summary>
	        /// 调整
	        /// </summary>
	        [XmlElement("adjustAmount")]
	        public string AdjustAmount { get; set; }
	
	        /// <summary>
	        /// 优惠
	        /// </summary>
	        [XmlElement("discount")]
	        public string Discount { get; set; }
	
	        /// <summary>
	        /// 货品名
	        /// </summary>
	        [XmlElement("goodsName")]
	        public string GoodsName { get; set; }
	
	        /// <summary>
	        /// 货品编码
	        /// </summary>
	        [XmlElement("goodsNo")]
	        public string GoodsNo { get; set; }
	
	        /// <summary>
	        /// 数量
	        /// </summary>
	        [XmlElement("num")]
	        public Nullable<long> Num { get; set; }
	
	        /// <summary>
	        /// 子订单编号
	        /// </summary>
	        [XmlElement("oid")]
	        public string Oid { get; set; }
	
	        /// <summary>
	        /// 实付金额
	        /// </summary>
	        [XmlElement("paid")]
	        public string Paid { get; set; }
	
	        /// <summary>
	        /// 单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 主键
	        /// </summary>
	        [XmlElement("recId")]
	        public string RecId { get; set; }
	
	        /// <summary>
	        /// 退款状态
	        /// </summary>
	        [XmlElement("refundStatus")]
	        public string RefundStatus { get; set; }
	
	        /// <summary>
	        /// 分摊优惠
	        /// </summary>
	        [XmlElement("shareDiscount")]
	        public string ShareDiscount { get; set; }
	
	        /// <summary>
	        /// 规格名
	        /// </summary>
	        [XmlElement("specName")]
	        public string SpecName { get; set; }
	
	        /// <summary>
	        /// 规格编码
	        /// </summary>
	        [XmlElement("specNo")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
}

	/// <summary>
/// WDTOriOrderDataDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("wDTOriOrderData")]
public class WDTOriOrderDataDomain : TopObject
{
	        /// <summary>
	        /// 买家备注
	        /// </summary>
	        [XmlElement("buyerMessage")]
	        public string BuyerMessage { get; set; }
	
	        /// <summary>
	        /// 订单完结时间
	        /// </summary>
	        [XmlElement("endTime")]
	        public string EndTime { get; set; }
	
	        /// <summary>
	        /// 原始订单行
	        /// </summary>
	        [XmlArray("entries")]
	        [XmlArrayItem("wDTOriOrderEntryData")]
	        public List<WDTOriOrderEntryDataDomain> Entries { get; set; }
	
	        /// <summary>
	        /// 发票内容
	        /// </summary>
	        [XmlElement("invoiceContent")]
	        public string InvoiceContent { get; set; }
	
	        /// <summary>
	        /// 发票抬头
	        /// </summary>
	        [XmlElement("invoiceTitle")]
	        public string InvoiceTitle { get; set; }
	
	        /// <summary>
	        /// 发票类别
	        /// </summary>
	        [XmlElement("invoiceType")]
	        public string InvoiceType { get; set; }
	
	        /// <summary>
	        /// 是否开具发票
	        /// </summary>
	        [XmlElement("isInvoice")]
	        public Nullable<bool> IsInvoice { get; set; }
	
	        /// <summary>
	        /// 物流方式
	        /// </summary>
	        [XmlElement("logisticsType")]
	        public string LogisticsType { get; set; }
	
	        /// <summary>
	        /// 其他收费
	        /// </summary>
	        [XmlElement("otherAmount")]
	        public string OtherAmount { get; set; }
	
	        /// <summary>
	        /// 已付
	        /// </summary>
	        [XmlElement("paid")]
	        public string Paid { get; set; }
	
	        /// <summary>
	        /// 支付流水号
	        /// </summary>
	        [XmlElement("payId")]
	        public string PayId { get; set; }
	
	        /// <summary>
	        /// 支付时间
	        /// </summary>
	        [XmlElement("payTime")]
	        public string PayTime { get; set; }
	
	        /// <summary>
	        /// 平台类型
	        /// </summary>
	        [XmlElement("platform")]
	        public string Platform { get; set; }
	
	        /// <summary>
	        /// 邮费
	        /// </summary>
	        [XmlElement("postAmount")]
	        public string PostAmount { get; set; }
	
	        /// <summary>
	        /// 退款金额
	        /// </summary>
	        [XmlElement("refundTotal")]
	        public string RefundTotal { get; set; }
	
	        /// <summary>
	        /// 客服备注
	        /// </summary>
	        [XmlElement("sellerMemo")]
	        public string SellerMemo { get; set; }
	
	        /// <summary>
	        /// 店铺
	        /// </summary>
	        [XmlElement("shopName")]
	        public string ShopName { get; set; }
	
	        /// <summary>
	        /// 原始订单号
	        /// </summary>
	        [XmlElement("tid")]
	        public string Tid { get; set; }
	
	        /// <summary>
	        /// 平台状态
	        /// </summary>
	        [XmlElement("tradeStatus")]
	        public string TradeStatus { get; set; }
	
	        /// <summary>
	        /// 下单时间
	        /// </summary>
	        [XmlElement("tradeTime")]
	        public string TradeTime { get; set; }
}

        #endregion
    }
}
