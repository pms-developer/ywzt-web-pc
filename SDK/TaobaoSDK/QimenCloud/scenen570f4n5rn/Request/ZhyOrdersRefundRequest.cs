using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace QimenCloud.Api.scenen570f4n5rn.Request
{
    /// <summary>
    /// TOP API: zhy.orders.refund
    /// </summary>
    public class ZhyOrdersRefundRequest : BaseQimenCloudRequest<QimenCloud.Api.scenen570f4n5rn.Response.ZhyOrdersRefundResponse>
    {
        /// <summary>
        /// 退款单列表
        /// </summary>
        public string RefundOrders { get; set; }

        public List<RefundOrderDataDomain> RefundOrders_ { set { this.RefundOrders = TopUtils.ObjectToJson(value); } } 

        #region IQimenCloudRequest Members

        public override string GetApiName()
        {
            return "zhy.orders.refund";
        }
        
        public override IDictionary<string, string> GetParameters()
        {
            TopDictionary parameters = new TopDictionary();
            parameters.Add("refundOrders", this.RefundOrders);
            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }
            return parameters;
        }

        public override void Validate()
        {
            RequestValidator.ValidateObjectMaxListSize("refundOrders", this.RefundOrders, 999999);
        }

	/// <summary>
/// RefundOrderEntryDataDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("refundOrderEntryData")]
public class RefundOrderEntryDataDomain : TopObject
{
	        /// <summary>
	        /// 退款单行编号
	        /// </summary>
	        [XmlElement("lineNum")]
	        public Nullable<long> LineNum { get; set; }
	
	        /// <summary>
	        /// 退款数量
	        /// </summary>
	        [XmlElement("refundNum")]
	        public Nullable<long> RefundNum { get; set; }
	
	        /// <summary>
	        /// 规格名称
	        /// </summary>
	        [XmlElement("specNo")]
	        public string SpecNo { get; set; }
	
	        /// <summary>
	        /// 组合装名称
	        /// </summary>
	        [XmlElement("suiteName")]
	        public string SuiteName { get; set; }
	
	        /// <summary>
	        /// 组合装编号
	        /// </summary>
	        [XmlElement("suiteNo")]
	        public string SuiteNo { get; set; }
	
	        /// <summary>
	        /// 组合装数量
	        /// </summary>
	        [XmlElement("suiteNum")]
	        public Nullable<long> SuiteNum { get; set; }
}

	/// <summary>
/// RefundOrderDataDomain Data Structure.
/// </summary>
[Serializable]
[Top.Api.Util.ApiListType("refundOrderData")]
public class RefundOrderDataDomain : TopObject
{
	        /// <summary>
	        /// 实际退款
	        /// </summary>
	        [XmlElement("actualRefundAmount")]
	        public string ActualRefundAmount { get; set; }
	
	        /// <summary>
	        /// 线下退款金额
	        /// </summary>
	        [XmlElement("directRefundAmount")]
	        public string DirectRefundAmount { get; set; }
	
	        /// <summary>
	        /// 退款单行列表
	        /// </summary>
	        [XmlArray("entries")]
	        [XmlArrayItem("refundOrderEntryData")]
	        public List<RefundOrderEntryDataDomain> Entries { get; set; }
	
	        /// <summary>
	        /// 平台退款金额
	        /// </summary>
	        [XmlElement("guaranteRefund")]
	        public string GuaranteRefund { get; set; }
	
	        /// <summary>
	        /// 修改时间
	        /// </summary>
	        [XmlElement("modified")]
	        public string Modified { get; set; }
	
	        /// <summary>
	        /// 操作人
	        /// </summary>
	        [XmlElement("operator")]
	        public string Operator { get; set; }
	
	        /// <summary>
	        /// 买家支付账号
	        /// </summary>
	        [XmlElement("payAccount")]
	        public string PayAccount { get; set; }
	
	        /// <summary>
	        /// 退款金额
	        /// </summary>
	        [XmlElement("refundAmount")]
	        public string RefundAmount { get; set; }
	
	        /// <summary>
	        /// erp退换单号
	        /// </summary>
	        [XmlElement("refundNo")]
	        public string RefundNo { get; set; }
	
	        /// <summary>
	        /// 退款原因
	        /// </summary>
	        [XmlElement("refundReason")]
	        public string RefundReason { get; set; }
	
	        /// <summary>
	        /// 退款结算时间
	        /// </summary>
	        [XmlElement("refundTime")]
	        public string RefundTime { get; set; }
	
	        /// <summary>
	        /// 备注
	        /// </summary>
	        [XmlElement("remark")]
	        public string Remark { get; set; }
	
	        /// <summary>
	        /// 旺店通系统订单号
	        /// </summary>
	        [XmlElement("salesTid")]
	        public string SalesTid { get; set; }
	
	        /// <summary>
	        /// 平台退款单编号
	        /// </summary>
	        [XmlElement("srcNo")]
	        public string SrcNo { get; set; }
	
	        /// <summary>
	        /// 类型
	        /// </summary>
	        [XmlElement("type")]
	        public string Type { get; set; }
}

        #endregion
    }
}
