using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Top.Api;


namespace QimenCloud.Api.scenen570f4n5rn.Response
{
    /// <summary>
    /// ZhyOrdersRefundResponse.
    /// </summary>
    public class ZhyOrdersRefundResponse : QimenCloudResponse
    {
        /// <summary>
        /// 错误码
        /// </summary>
        [XmlElement("errorCode")]
        public string ErrorCode { get; set; }

        /// <summary>
        /// 标记
        /// </summary>
        [XmlElement("errorFlag")]
        public string ErrorFlag { get; set; }

        /// <summary>
        /// 错误消息
        /// </summary>
        [XmlElement("errorMessage")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// 状态码
        /// </summary>
        [XmlElement("returnCode")]
        public long ReturnCode { get; set; }

        /// <summary>
        /// 状态信息
        /// </summary>
        [XmlElement("returnMessage")]
        public string ReturnMessage { get; set; }

    }
}
