using System;
using System.Xml.Serialization;

namespace Top.Api.Domain
{
    /// <summary>
    /// LogisticsInfo Data Structure.
    /// </summary>
    [Serializable]
    public class LogisticsInfo : TopObject
    {
        /// <summary>
        /// 组合商品编码code
        /// </summary>
        [XmlElement("combine_item_code")]
        public string CombineItemCode { get; set; }

        /// <summary>
        /// 组合商品id
        /// </summary>
        [XmlElement("combine_item_id")]
        public string CombineItemId { get; set; }

        /// <summary>
        /// 发货类型CN=菜鸟发货，SC的商家仓发货
        /// </summary>
        [XmlElement("consign_type")]
        public string ConsignType { get; set; }

        /// <summary>
        /// 货品仓储code
        /// </summary>
        [XmlElement("item_code")]
        public string ItemCode { get; set; }

        /// <summary>
        /// 货品仓储ID
        /// </summary>
        [XmlElement("item_id")]
        public string ItemId { get; set; }

        /// <summary>
        /// 商品比例
        /// </summary>
        [XmlElement("item_ratio")]
        public long ItemRatio { get; set; }

        /// <summary>
        /// 应发数量
        /// </summary>
        [XmlElement("need_consign_num")]
        public long NeedConsignNum { get; set; }

        /// <summary>
        /// 商品数字编号
        /// </summary>
        [XmlElement("num_iid")]
        public long NumIid { get; set; }

        /// <summary>
        /// 商品的最小库存单位Sku的id
        /// </summary>
        [XmlElement("sku_id")]
        public string SkuId { get; set; }

        /// <summary>
        /// 如是菜鸟仓，则将菜鸟仓的区域仓code进行填充，如是商家仓发货则填充商家仓code
        /// </summary>
        [XmlElement("store_code")]
        public string StoreCode { get; set; }

        /// <summary>
        /// 子交易号
        /// </summary>
        [XmlElement("sub_trade_id")]
        public long SubTradeId { get; set; }

        /// <summary>
        /// 交易号
        /// </summary>
        [XmlElement("trade_id")]
        public long TradeId { get; set; }

        /// <summary>
        /// 子订单类型:标示该子交易单来源交易，还是BMS增加的，枚举值(00=交易，10=BMS绑定)
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
    }
}
