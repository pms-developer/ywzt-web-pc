using System;
using System.Xml.Serialization;

namespace Top.Api.Domain
{
    /// <summary>
    /// ConfirmConsignGoodsDto Data Structure.
    /// </summary>
    [Serializable]
    public class ConfirmConsignGoodsDto : TopObject
    {
        /// <summary>
        /// 待发货商品的前端宝贝id
        /// </summary>
        [XmlElement("item_id")]
        public string ItemId { get; set; }

        /// <summary>
        /// 待发货商品的数量
        /// </summary>
        [XmlElement("quantity")]
        public Nullable<long> Quantity { get; set; }

        /// <summary>
        /// 待发货商品的子交易号
        /// </summary>
        [XmlElement("tc_sub_trade_id")]
        public Nullable<long> TcSubTradeId { get; set; }
    }
}
