using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace Qimen.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.returnpackage.report
    /// </summary>
    public class ReturnpackageReportRequest : QimenRequest<Qimen.Api.Response.ReturnpackageReportResponse>
    {
        public OrderDomain Order { get; set; }

        public PackagesDomain Packages { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.returnpackage.report";
        }


	/// <summary>
/// OrderDomain Data Structure.
/// </summary>
[Serializable]

public class OrderDomain
{
	        /// <summary>
	        /// 退货包裹交接单编码, string (50) , 必填
	        /// </summary>
	        [XmlElement("orderCode")]
	        public string OrderCode { get; set; }
	
	        /// <summary>
	        /// 单据类型, string（50）,THPL=退货包裹交接单
	        /// </summary>
	        [XmlElement("orderType")]
	        public string OrderType { get; set; }
	
	        /// <summary>
	        /// 外部业务编码, 消息ID, 用于去重, ISV对于同一请求，分配一个唯一性的编码。用来保证因为网络等原因导致重复传输，请求不会被重复处理。必填
	        /// </summary>
	        [XmlElement("outBizCode")]
	        public string OutBizCode { get; set; }
	
	        /// <summary>
	        /// 货主编码, string (50) , 必填
	        /// </summary>
	        [XmlElement("ownerCode")]
	        public string OwnerCode { get; set; }
	
	        /// <summary>
	        /// 仓库编码, string (50)，必填
	        /// </summary>
	        [XmlElement("warehouseCode")]
	        public string WarehouseCode { get; set; }
}

	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain
{
	        /// <summary>
	        /// 金额
	        /// </summary>
	        [XmlElement("amount")]
	        public string Amount { get; set; }
	
	        /// <summary>
	        /// 商品编码
	        /// </summary>
	        [XmlElement("itemCode")]
	        public string ItemCode { get; set; }
	
	        /// <summary>
	        /// 商品仓储系统编码
	        /// </summary>
	        [XmlElement("itemId")]
	        public string ItemId { get; set; }
	
	        /// <summary>
	        /// 商品名称
	        /// </summary>
	        [XmlElement("itemName")]
	        public string ItemName { get; set; }
	
	        /// <summary>
	        /// 商品单价
	        /// </summary>
	        [XmlElement("price")]
	        public string Price { get; set; }
	
	        /// <summary>
	        /// 包裹内该商品的数量,
	        /// </summary>
	        [XmlElement("quantity")]
	        public string Quantity { get; set; }
	
	        /// <summary>
	        /// 签收状态
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 商品单位
	        /// </summary>
	        [XmlElement("unit")]
	        public string Unit { get; set; }
}

	/// <summary>
/// ItemsDomain Data Structure.
/// </summary>
[Serializable]

public class ItemsDomain
{
	        /// <summary>
	        /// 商品详情
	        /// </summary>
	        [XmlElement("item")]
	        public ItemDomain Item { get; set; }
}

	/// <summary>
/// PackageDomain Data Structure.
/// </summary>
[Serializable]

public class PackageDomain
{
	        /// <summary>
	        /// 运单号, string (50) , 不必填
	        /// </summary>
	        [XmlElement("expressCode")]
	        public string ExpressCode { get; set; }
	
	        /// <summary>
	        /// 商品列表
	        /// </summary>
	        [XmlElement("items")]
	        public ItemsDomain Items { get; set; }
	
	        /// <summary>
	        /// 承运商编码string (50) , SF=顺丰、EMS=标准快递、EYB=经济快件、ZJS=宅急送、YTO=圆通 、ZTO=中通 (ZTO) 、HTKY=百世汇通、BSKY=百世快运、UC=优速、STO=申通、TTKDEX=天天快递 、QFKD=全峰、FAST=快捷、POSTB=邮政小包 、GTO=国通、YUNDA=韵达、JD=京东配送、DD=当当宅配、AMAZON=亚马逊物流、DBWL=德邦物流、DBKD=德邦快递、DBKY=德邦快运、RRS=日日顺、OTHER=其他，必填, (只传英文编码)
	        /// </summary>
	        [XmlElement("logisticsCode")]
	        public string LogisticsCode { get; set; }
	
	        /// <summary>
	        /// 物流公司名称, string (200)
	        /// </summary>
	        [XmlElement("logisticsName")]
	        public string LogisticsName { get; set; }
	
	        /// <summary>
	        /// 备注, string (500) ,
	        /// </summary>
	        [XmlElement("remarks")]
	        public string Remarks { get; set; }
	
	        /// <summary>
	        /// 当前状态操作时间, string (19) , YYYY-MM-DD HH:MM:SS
	        /// </summary>
	        [XmlElement("signTime")]
	        public string SignTime { get; set; }
	
	        /// <summary>
	        /// 签收人姓名, string (50) ，必填
	        /// </summary>
	        [XmlElement("signUserName")]
	        public string SignUserName { get; set; }
	
	        /// <summary>
	        /// 状态, sign-已签收string (50)
	        /// </summary>
	        [XmlElement("status")]
	        public string Status { get; set; }
	
	        /// <summary>
	        /// 包裹重量 (千克) , double (18, 3)
	        /// </summary>
	        [XmlElement("weight")]
	        public string Weight { get; set; }
}

	/// <summary>
/// PackagesDomain Data Structure.
/// </summary>
[Serializable]

public class PackagesDomain
{
	        /// <summary>
	        /// 包裹详情
	        /// </summary>
	        [XmlElement("packageValue")]
	        public PackageDomain PackageValue { get; set; }
}

        #endregion
    }
}
