using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Top.Api.Util;
using Top.Api;

namespace Qimen.Api.Request
{
    /// <summary>
    /// TOP API: taobao.qimen.transferorder.report
    /// </summary>
    public class TransferorderReportRequest : QimenRequest<Qimen.Api.Response.TransferorderReportResponse>
    {
        /// <summary>
        /// 确认入库时间
        /// </summary>
        public string ConfirmInTime { get; set; }

        /// <summary>
        /// 确认出库时间
        /// </summary>
        public string ConfirmOutTime { get; set; }

        /// <summary>
        /// 调拨单创建时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// erpOrderCode
        /// </summary>
        public string ErpOrderCode { get; set; }

        /// <summary>
        /// 调拨出库仓编码
        /// </summary>
        public string FromWarehouseCode { get; set; }

        public ItemsDomain Items { get; set; }

        /// <summary>
        /// orderStatus
        /// </summary>
        public string OrderStatus { get; set; }

        /// <summary>
        /// 111
        /// </summary>
        public string OwnerCode { get; set; }

        /// <summary>
        /// 调拨入库仓编码
        /// </summary>
        public string ToWarehouseCode { get; set; }

        /// <summary>
        /// 调拨入库单号
        /// </summary>
        public string TransferInOrderCode { get; set; }

        /// <summary>
        /// 调拨单号,0,string(50),必填,
        /// </summary>
        public string TransferOrderCode { get; set; }

        /// <summary>
        /// 调拨出库单号
        /// </summary>
        public string TransferOutOrderCode { get; set; }

        #region ITopRequest Members

        public override string GetApiName()
        {
            return "taobao.qimen.transferorder.report";
        }


	/// <summary>
/// ItemDomain Data Structure.
/// </summary>
[Serializable]

public class ItemDomain
{
	        /// <summary>
	        /// 实际入库数量,Item1234,string(50),,
	        /// </summary>
	        [XmlElement("inCount")]
	        public string InCount { get; set; }
	
	        /// <summary>
	        /// 库存类型(1:可销售库存.101:残次),HZ1234,string(500),,
	        /// </summary>
	        [XmlElement("inventoryType")]
	        public string InventoryType { get; set; }
	
	        /// <summary>
	        /// 实际出库数量,Item1234,string(50),,
	        /// </summary>
	        [XmlElement("outCount")]
	        public string OutCount { get; set; }
	
	        /// <summary>
	        /// 计划调拨数量
	        /// </summary>
	        [XmlElement("planCount")]
	        public string PlanCount { get; set; }
	
	        /// <summary>
	        /// 货品编码,HZ1234,string(50),,
	        /// </summary>
	        [XmlElement("scItemCode")]
	        public string ScItemCode { get; set; }
}

	/// <summary>
/// ItemsDomain Data Structure.
/// </summary>
[Serializable]

public class ItemsDomain
{
	        /// <summary>
	        /// 项目
	        /// </summary>
	        [XmlElement("item")]
	        public ItemDomain Item { get; set; }
}

        #endregion
    }
}
