﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Reflection;
using Aspose.Cells;
using XiZi.Excel.Attributes;
using XiZi.Excel.Extensions;

namespace XiZi.Excel
{
    public class BaseExcelExporter<T> : BaseExcelWorker<T>
    {
        private readonly List<string> _ignoreProperties = new List<string>();

        public BaseExcelExporter(IEnumerable<T> models)
        {
            Models = models;
            Workbook = new Workbook();
            Workbook.Settings.MemorySetting = MemorySetting.MemoryPreference;
            Workbook.Settings.CheckExcelRestriction = false;
            Worksheet = Workbook.Worksheets[0];
            Cells = Worksheet.Cells;
            StartRow = 0;
            Row = StartRow;
        }

        public BaseExcelExporter(IEnumerable<T> models, IDictionary<string, string> userExportCols) : this(models)
        {
            UserExportCols = userExportCols;
        }

        public Workbook Execute()
        {
            ProcessPreHeader();
            ExtractHeaders();
            ProcessPostHeader();

            ProcessPreModel();
            ExtractModels();
            ProcessPostModel();

            ProcessExcelStyle();

            return Workbook;
        }

        protected virtual void ProcessPreHeader()
        {
            // do nothing.
        }

        protected virtual void ExtractHeaders()
        {
            var column = 0;
            foreach (var propertyInfo in PropertyInfos)
            {
                var cellAttribute = propertyInfo.GetCustomAttribute<CellAttribute>();
                if (cellAttribute != null && cellAttribute.Ignored)
                {
                    _ignoreProperties.Add(propertyInfo.Name);
                    continue;
                }

                if (cellAttribute != null && cellAttribute.IgnoredByComputation)
                {
                    T model = Models.FirstOrDefault();
                    if (model is ICellAttributeIgnoredByComputation cellAttributeIgnoredByComputation && cellAttributeIgnoredByComputation.IgnoredByComputation(propertyInfo.Name))
                    {
                        _ignoreProperties.Add(propertyInfo.Name);
                        continue;
                    }
                }

                if (UserExportCols != null && UserExportCols.Any())
                {
                    if (UserExportCols.ContainsKey(propertyInfo.Name))
                    {
                        Cells[Row, column].PutValue(UserExportCols[propertyInfo.Name]);
                    }
                    else
                    {
                        _ignoreProperties.Add(propertyInfo.Name);
                        continue;
                    }
                }
                else
                {
                    var displayAttribute = propertyInfo.GetCustomAttribute<DisplayAttribute>();
                    var name = GetMetadataProvider(displayAttribute?.GetName() ?? propertyInfo.Name);

                    Cells[Row, column].PutValue(name);
                }
                ++column;
            }
        }

        protected virtual void ProcessPostHeader()
        {
            var cellsFactory = new CellsFactory();

            var headerStyle = cellsFactory.CreateStyle();
            headerStyle.Font.IsBold = true;
            headerStyle.Font.Name = "Calibri";
            headerStyle.ForegroundColor = Color.FromArgb(238, 236, 225);
            headerStyle.Pattern = BackgroundType.Solid;
            headerStyle.VerticalAlignment = TextAlignmentType.Center;
            headerStyle.SetBorder(BorderType.LeftBorder, CellBorderType.Thin, Color.Black);
            headerStyle.SetBorder(BorderType.RightBorder, CellBorderType.Thin, Color.Black);
            headerStyle.SetBorder(BorderType.TopBorder, CellBorderType.Thin, Color.Black);
            headerStyle.SetBorder(BorderType.BottomBorder, CellBorderType.Thin, Color.Black);
            var range = Cells.CreateRange(0, 0, 1, Cells.MaxColumn + 1);
            range.SetStyle(headerStyle);
            range.RowHeight = 23;
        }

        protected virtual void ProcessPreModel()
        {
            if (!Models.Any())
            {
                return;
            }

            var cellsFactory = new CellsFactory();

            var bodyStyle = cellsFactory.CreateStyle();
            bodyStyle.SetBorder(BorderType.LeftBorder, CellBorderType.Thin, Color.Black);
            bodyStyle.SetBorder(BorderType.RightBorder, CellBorderType.Thin, Color.Black);
            bodyStyle.SetBorder(BorderType.TopBorder, CellBorderType.Thin, Color.Black);
            bodyStyle.SetBorder(BorderType.BottomBorder, CellBorderType.Thin, Color.Black);
            bodyStyle.HorizontalAlignment = TextAlignmentType.Left;
            bodyStyle.VerticalAlignment = TextAlignmentType.Center;
            var bodyRange = Cells.CreateRange(1, 0, Models.Count(), Cells.MaxColumn + 1);
            bodyRange.SetStyle(bodyStyle);
            bodyRange.RowHeight = 20;
        }

        private void ExtractModels()
        {
            foreach (var model in Models)
            {
                ++Row;
                ExtractOneRow(model);
            }
        }

        protected virtual void ProcessPostModel()
        {
            _ignoreProperties.Clear();
        }

        protected virtual void ProcessExcelStyle()
        {
            Worksheet.AutoFitColumns();
        }

        protected virtual void ExtractOneRow(T model)
        {
            var column = 0;
            foreach (var property in PropertyInfos.Where(p => !_ignoreProperties.Contains(p.Name)))
            {
                var cellAttribute = property.GetCustomAttribute<CellAttribute>();

                var cell = Cells[Row, column];
                Cells = ProcessCellsStyle(Cells, column, cellAttribute);
                cell = ProcessCellStyle(cell, cellAttribute);
                cell = ProcessCellValue(cell, cellAttribute, model, property);

                ++column;
            }
        }

        protected Cells ProcessCellsStyle(Cells cells, int column, CellAttribute attribute)
        {
            return attribute?.Style == CellStyle.Width ? cells.SetWidth(column) : cells;
        }

        protected Cell ProcessCellStyle(Cell cell, CellAttribute attribute)
        {
            if (attribute == null)
            {
                return cell;
            }

            switch (attribute.Style)
            {
                case CellStyle.Text:
                    break;

                case CellStyle.Currency:
                    cell.SetMoneyStyle();
                    break;

                case CellStyle.Date:
                    cell.SetDateStyle();
                    break;

                case CellStyle.DateTime:
                    cell.SetDateTimeStyle();
                    break;

                case CellStyle.Number:
                    cell.SetNumberStyle();
                    break;

                case CellStyle.Percentage:
                    cell.SetPercentageStyle();
                    break;

                case CellStyle.Month:
                    cell.SetMonthStyle();
                    break;

                case CellStyle.Html:
                    break;

                case CellStyle.Width:
                    break;

                case CellStyle.TextWrapped:
                    cell.SetTextWrapped();
                    break;

                case CellStyle.PhoneNumber:
                    break;

                default:
                    break;
            }

            return cell;
        }

        protected Cell ProcessCellValue(Cell cell, CellAttribute attribute, T model, PropertyInfo property)
        {
            var displayValue = GetDisplayValue(model, property);

            if (attribute == null)
            {
                cell.PutValue(displayValue);
                return cell;
            }

            switch (attribute.Style)
            {
                case CellStyle.Html:
                    cell.HtmlString = SetHtml(displayValue);
                    break;

                case CellStyle.PhoneNumber:
                    cell.HtmlString = SetPhoneNumber(displayValue);
                    break;

                default:
                    cell.PutValue(displayValue);
                    break;
            }

            return cell;
        }

        protected object GetDisplayValue(T model, PropertyInfo property)
        {
            object displayValue = null;
            var modelProperty = model.GetType().GetProperty(property.Name);
            var propertyValue = modelProperty.GetValue(model);
            if (propertyValue != null)
            {
                displayValue = DisplayValueIntercepter(propertyValue, model, property);

                var displayFormatAttribute = property.GetCustomAttribute<DisplayFormatAttribute>();
                if (displayFormatAttribute != null && displayFormatAttribute.DataFormatString != null)
                {
                    displayValue = string.Format(displayFormatAttribute.DataFormatString, displayValue);
                }
                else
                {
                    var cellAttribute = property.GetCustomAttribute<CellAttribute>();
                    displayValue = cellAttribute != null ? displayValue : displayValue.ToString();
                }
            }

            return displayValue;
        }

        protected string SetHtml(object finalValue)
        {
            return finalValue?.ToString() ?? "";
        }

        protected string SetPhoneNumber(object finalValue)
        {
            if (finalValue != null && !string.IsNullOrWhiteSpace(finalValue.ToString()))
            {
                return StringExtension.ToPhoneText(finalValue.ToString());
            }

            return "";
        }

        protected virtual object DisplayValueIntercepter(object propertyValue, T model, PropertyInfo property)
        {
            return propertyValue;
        }

        public IEnumerable<T> Models { get; set; }

        public IDictionary<string, string> UserExportCols { get; set; }

        public int Row { get; set; }

        public int StartRow { get; set; }
    }
}
