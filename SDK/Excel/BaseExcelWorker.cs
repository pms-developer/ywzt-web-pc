﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using Aspose.Cells;
using XiZi.Excel.Attributes;

namespace XiZi.Excel
{
    public class BaseExcelWorker<T>
    {
        public BaseExcelWorker()
        {
            Type type = typeof(T);
            PropertyInfos = type.GetProperties()
                .Where(x => x.GetCustomAttribute<RenderModeAttribute>() == null)
                .OrderBy(x => x.GetCustomAttribute<DisplayAttribute>()?.GetOrder() ?? int.MaxValue)
                .ToArray();
        }

        public string GetMetadataProvider(string val)
        {
            return val;
        }

        public PropertyInfo[] PropertyInfos { get; set; }

        public Workbook Workbook { get; set; }

        public Worksheet Worksheet { get; set; }

        public Cells Cells { get; set; }
    }
}
