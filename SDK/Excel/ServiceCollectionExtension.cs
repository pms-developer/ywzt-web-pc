﻿using Autofac;


namespace XiZi.Excel
{

    public static partial class ServiceCollectionExtension
    {
        public static void ConfigureExcel(this ContainerBuilder builder)
        {
            builder.RegisterType<ExcelService>().As<IExcelService>().OwnedByLifetimeScope(); 
        }
    }
}
