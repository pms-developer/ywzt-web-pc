﻿using System;
using System.Collections.Generic;
using System.IO;
using Aspose.Cells;

namespace XiZi.Excel
{
    public interface IExcelService
    {
        Workbook ExportToWorkbook<TOutput>(IEnumerable<TOutput> models, IDictionary<string, string> userExportCols = default);

        Stream ExportToStream<TOutput>(IEnumerable<TOutput> models, IDictionary<string, string> userExportCols = default);

        Workbook GetWorkbookFromExcel<TOutput>(byte[] fileBytes);

        List<TOutput> GetListFromExcel<TOutput>(Workbook workbook, Func<Worksheet, int, TOutput> processExcelRow);

        List<TOutput> GetListFromExcel<TOutput, TExtra>(Workbook workbook, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra);

        List<TOutput> GetListFromExcel<TOutput>(Stream stream, Func<Worksheet, int, TOutput> processExcelRow);

        List<TOutput> GetListFromExcel<TOutput, TExtra>(Stream stream, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra);

        List<TOutput> GetListFromExcel<TOutput>(byte[] fileBytes, Func<Worksheet, int, TOutput> processExcelRow);

        List<TOutput> GetListFromExcel<TOutput, TExtra>(byte[] fileBytes, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra);
    }
}
