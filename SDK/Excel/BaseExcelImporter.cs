﻿using System;
using System.Collections.Generic;
using System.IO;
using Aspose.Cells;

namespace XiZi.Excel
{
    public class BaseExcelImporter<TOutput>
    {
        public Workbook GetWorkbook(byte[] fileBytes)
        {
            using (var stream = new MemoryStream(fileBytes))
            {
                var workbook = new Workbook(stream);
                return workbook;
            }
        }

        public List<TOutput> ProcessExcelFile(Workbook workbook, Func<Worksheet, int, TOutput> processExcelRow)
        {
            return ProcessExcelFileCore(workbook, worksheet => ProcessWorksheet(worksheet, processExcelRow));
        }

        public List<TOutput> ProcessExcelFile<TExtra>(Workbook workbook, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra)
        {
            return ProcessExcelFileCore(workbook, worksheet => ProcessWorksheet(worksheet, processExcelRow, extra));
        }

        public List<TOutput> ProcessExcelFile(Stream stream, Func<Worksheet, int, TOutput> processExcelRow)
        {
            return ProcessExcelFileCore(stream, workSheet => ProcessWorksheet(workSheet, processExcelRow));
        }

        public List<TOutput> ProcessExcelFile<TExtra>(Stream stream, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra)
        {
            return ProcessExcelFileCore(stream, workSheet => ProcessWorksheet(workSheet, processExcelRow, extra));
        }

        public List<TOutput> ProcessExcelFile(byte[] fileBytes, Func<Worksheet, int, TOutput> processExcelRow)
        {
            return ProcessExcelFileCore(fileBytes, workSheet => ProcessWorksheet(workSheet, processExcelRow));
        }

        public List<TOutput> ProcessExcelFile<TExtra>(byte[] fileBytes, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra)
        {
            return ProcessExcelFileCore(fileBytes, workSheet => ProcessWorksheet(workSheet, processExcelRow, extra));
        }

        private List<TOutput> ProcessExcelFileCore(Workbook workbook, Func<Worksheet, List<TOutput>> processWorksheetCore)
        {
            var entities = new List<TOutput>();

            for (var i = 0; i < workbook.Worksheets.Count; i++)
            {
                var entitiesInWorksheet = processWorksheetCore(workbook.Worksheets[i]);
                entities.AddRange(entitiesInWorksheet);
            }

            return entities;
        }

        private List<TOutput> ProcessExcelFileCore(Stream stream, Func<Worksheet, List<TOutput>> processWorksheetCore)
        {
            var entities = new List<TOutput>();

            var workbook = new Workbook(stream);
            for (var i = 0; i < workbook.Worksheets.Count; i++)
            {
                var entitiesInWorksheet = processWorksheetCore(workbook.Worksheets[i]);
                entities.AddRange(entitiesInWorksheet);
            }

            return entities;
        }

        private List<TOutput> ProcessExcelFileCore(byte[] fileBytes, Func<Worksheet, List<TOutput>> processWorksheetCore)
        {
            var entities = new List<TOutput>();

            using (var stream = new MemoryStream(fileBytes))
            {
                var workbook = new Workbook(stream);
                for (var i = 0; i < workbook.Worksheets.Count; i++)
                {
                    var entitiesInWorksheet = processWorksheetCore(workbook.Worksheets[i]);
                    entities.AddRange(entitiesInWorksheet);
                }
            }

            return entities;
        }

        private List<TOutput> ProcessWorksheet(Worksheet worksheet, Func<Worksheet, int, TOutput> processExcelRow)
        {
            return ProcessWorksheetCore(worksheet, row => processExcelRow(worksheet, row));
        }

        private List<TOutput> ProcessWorksheet<TExtra>(Worksheet worksheet, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra)
        {
            return ProcessWorksheetCore(worksheet, row => processExcelRow(worksheet, row, extra));
        }

        private List<TOutput> ProcessWorksheetCore(Worksheet worksheet, Func<int, TOutput> processExcelRow)
        {
            var entities = new List<TOutput>();
            var cells = worksheet.Cells;

            for (int row = 0; row <= cells.MaxRow; row++)
            {
                if (row == 0)
                {
                    //Skip header
                    continue;
                }

                var entity = processExcelRow(row);
                if (entity != null)
                {
                    entities.Add(entity);
                }
            }

            return entities;
        }
    }
}
