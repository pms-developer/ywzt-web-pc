﻿using System;
using System.Collections.Generic;
using System.IO;
using Aspose.Cells;

namespace XiZi.Excel
{
    public class ExcelService : IExcelService
    {
        public Workbook ExportToWorkbook<TOutput>(IEnumerable<TOutput> models, IDictionary<string, string> userExportCols = default)
        {
            var exporter = new BaseExcelExporter<TOutput>(models, userExportCols);
            return exporter.Execute();
        }

        public Stream ExportToStream<TOutput>(IEnumerable<TOutput> models, IDictionary<string, string> userExportCols = default)
        {
            var stream = new MemoryStream();
            var exporter = new BaseExcelExporter<TOutput>(models, userExportCols);
            var workbook = exporter.Execute();
            workbook.Save(stream, SaveFormat.Xlsx);
            stream.Position = 0;
            return stream;
        }

        public Workbook GetWorkbookFromExcel<TOutput>(byte[] fileBytes)
        {
            var importer = new BaseExcelImporter<TOutput>();
            return importer.GetWorkbook(fileBytes);
        }

        public List<TOutput> GetListFromExcel<TOutput>(Workbook workbook, Func<Worksheet, int, TOutput> processExcelRow)
        {
            var importer = new BaseExcelImporter<TOutput>();
            return importer.ProcessExcelFile(workbook, processExcelRow);
        }

        public List<TOutput> GetListFromExcel<TOutput>(Stream stream, Func<Worksheet, int, TOutput> processExcelRow)
        {
            var importer = new BaseExcelImporter<TOutput>();
            return importer.ProcessExcelFile(stream, processExcelRow);
        }

        public List<TOutput> GetListFromExcel<TOutput>(byte[] fileBytes, Func<Worksheet, int, TOutput> processExcelRow)
        {
            var importer = new BaseExcelImporter<TOutput>();
            return importer.ProcessExcelFile(fileBytes, processExcelRow);
        }

        public List<TOutput> GetListFromExcel<TOutput, TExtra>(Workbook workbook, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra)
        {
            var importer = new BaseExcelImporter<TOutput>();
            return importer.ProcessExcelFile(workbook, processExcelRow, extra);
        }

        public List<TOutput> GetListFromExcel<TOutput, TExtra>(Stream stream, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra)
        {
            var importer = new BaseExcelImporter<TOutput>();
            return importer.ProcessExcelFile(stream, processExcelRow, extra);
        }

        public List<TOutput> GetListFromExcel<TOutput, TExtra>(byte[] fileBytes, Func<Worksheet, int, TExtra, TOutput> processExcelRow, TExtra extra)
        {
            var importer = new BaseExcelImporter<TOutput>();
            return importer.ProcessExcelFile(fileBytes, processExcelRow, extra);
        }
    }
}
