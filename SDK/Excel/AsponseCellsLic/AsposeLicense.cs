﻿using System.IO;
using System.Reflection;
using Aspose.Cells;

namespace XiZi.Excel
{
    public class AsposeLicense
    {
        public static void SetCellsLicense()
        {
            var stream = GetAsponseCellsLicFromAssembly("XiZi.Excel.AsponseCellsLic.Aspose.Total.lic");
            var license = new License();
            license.SetLicense(stream);
        }

        private static Stream GetAsponseCellsLicFromAssembly(string path)
        {
            var resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(path);
            if (resourceStream == null) throw new IOException("Template " + path + " not found!");
            var fileBytes = new byte[resourceStream.Length];
            resourceStream.Read(fileBytes, 0, (int)resourceStream.Length);
            resourceStream.Close();
            var stream = new MemoryStream(fileBytes);
            return stream;
        }
    }
}
