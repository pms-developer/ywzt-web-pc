﻿using System.Text.RegularExpressions;

namespace XiZi.Excel.Extensions
{
    internal static class StringExtension
    {
        public static bool IsNullOrWhiteSpace(this string me)
        {
            return string.IsNullOrWhiteSpace(me);
        }

        public static string ToPhoneText(this string str)
        {
            if (str.IsNullOrWhiteSpace()) return str;
            if (str.IsMatch(@"^[\d|-]{10,20}$"))
            {
                str = str.Replace("-", "");
            }
            if (str.IsMatch(@"^\d{10}$"))
            {
                return str.RegexReplace(@"^(\d{3})(\d{3})(\d{4})$", @"($1) $2-$3");
            }
            if (str.IsMatch(@"^\d{11,20}$"))
            {
                return str.RegexReplace(@"^(\d{3})(\d{3})(\d{4})(\d{1,10})$", "($1) $2-$3-$4");
            }
            return str;
        }

        public static string RegexReplace(this string str, string pattern, string replacement)
        {
            return Regex.Replace(str, pattern, replacement);
        }

        public static bool IsMatch(this string str, string pattern)
        {
            return Regex.IsMatch(str, pattern);
        }
    }
}
