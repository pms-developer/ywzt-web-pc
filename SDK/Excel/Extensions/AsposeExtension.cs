﻿using System;
using System.Drawing;
using Aspose.Cells;

namespace XiZi.Excel.Extensions
{
    public static class AsposeExtension
    {
        public static Cell SetAlignCenterStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.HorizontalAlignment = TextAlignmentType.Center;
            style.VerticalAlignment = TextAlignmentType.Center;
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetBorderStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.SetBorder(BorderType.LeftBorder, CellBorderType.Thin, Color.Black);
            style.SetBorder(BorderType.RightBorder, CellBorderType.Thin, Color.Black);
            style.SetBorder(BorderType.TopBorder, CellBorderType.Thin, Color.Black);
            style.SetBorder(BorderType.BottomBorder, CellBorderType.Thin, Color.Black);
            cell.SetStyle(style);
            return cell;
        }

        public static Cells SetWidth(this Cells cells, int column)
        {
            cells.SetColumnWidth(column, 50);
            return cells;
        }

        public static Cell SetMoneyStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.Number = 8;// $#,##0.00;[Red]$-#,##0.00
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetMoneyStyleRoundTwo(this Cell cell, object value)
        {
            var style = cell.GetStyle();
            if (value != null)
            {
                if (decimal.TryParse(value.ToString(), out decimal result))
                {
                    cell.Value = Math.Round(result, 2, MidpointRounding.AwayFromZero);
                }
            }
            style.Number = 8;// $#,##0.00;[Red]$-#,##0.00
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetMoneyStyleRoundZero(this Cell cell)
        {
            var style = cell.GetStyle();
            style.Number = 6;// $#,##0;[Red]$-#,##0
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetDateStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.Number = 14;// The format is m/d/yy for date.
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetMonthStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.Number = 17;// The format is mmm-yy for date.
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetTextWrapped(this Cell cell)
        {
            var style = cell.GetStyle();
            style.IsTextWrapped = true;
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetDateTimeStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.Number = 22;// The format is  m/d/yy h:mm for date time.
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetNumberStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.Number = 0;
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetPercentageStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.Number = 10;// The format is 0.00%
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetAlignRightStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.HorizontalAlignment = TextAlignmentType.Right;
            style.VerticalAlignment = TextAlignmentType.Center;
            cell.SetStyle(style);
            return cell;
        }

        public static Cell SetStrikeoutStyle(this Cell cell)
        {
            var style = cell.GetStyle();
            style.Font.IsStrikeout = true;
            cell.SetStyle(style);
            return cell;
        }
    }
}
