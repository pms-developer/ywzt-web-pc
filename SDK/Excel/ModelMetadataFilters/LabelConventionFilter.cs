﻿using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;

namespace XiZi.Excel.ModelMetadataFilters
{
    public class LabelConventionFilter : IModelMetadataFilter
    {
        public string TransformMetadata(string val, PropertyInfo metadata)
        {
            if (!string.IsNullOrEmpty(metadata.Name) &&
                metadata.GetCustomAttribute<DisplayNameAttribute>() == null)
            {
                val = GetStringWithSpaces(metadata.Name);
            }

            return val;
        }

        private string GetStringWithSpaces(string input)
        {
            var camelCase = input.Replace("_", " ");
            var withEmpty = Regex.Replace(
                camelCase,
                "(?<!^)" +
                "(" +
                "  [A-Z][a-z] |" +
                "  (?<=[a-z])[A-Z] |" +
                "  (?<![A-Z])[A-Z]$" +
                ")",
                " $1",
                RegexOptions.IgnorePatternWhitespace);
            var info = new CultureInfo(1033).TextInfo;
            var titleCase = info.ToTitleCase(withEmpty);
            titleCase = titleCase.Replace(" Num", " #");
            return titleCase;
        }
    }
}
