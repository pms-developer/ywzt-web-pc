﻿using System.Reflection;

namespace XiZi.Excel.ModelMetadataFilters
{
    public interface IModelMetadataFilter
    {
        string TransformMetadata(string val, PropertyInfo metadata);
    }
}
