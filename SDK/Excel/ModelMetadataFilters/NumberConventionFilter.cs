﻿using System.ComponentModel;
using System.Reflection;

namespace XiZi.Excel.ModelMetadataFilters
{
    public class NumberConventionFilter : IModelMetadataFilter
    {
        public string TransformMetadata(string val, PropertyInfo metadata)
        {
            if (!string.IsNullOrEmpty(metadata.Name) &&
                metadata.GetCustomAttribute<DisplayNameAttribute>() == null &&
                metadata.Name.EndsWith("Num"))
            {
                var idx = metadata.Name.LastIndexOf("Num", System.StringComparison.Ordinal);
                var name = metadata.Name.Substring(0, idx);
                val = name + " #";
            }

            return val;
        }
    }
}
