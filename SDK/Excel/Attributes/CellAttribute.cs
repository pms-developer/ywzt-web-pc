﻿using System;

namespace XiZi.Excel.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CellAttribute : Attribute
    {
        public CellStyle Style { get; set; }

        public bool Ignored { get; set; }

        public bool DisplayUtcTime { get; set; }

        /// <summary>
        /// Ingore when specific condition is true
        /// Must implement ICellAttributeIgnoredByComputation
        /// </summary>
        public bool IgnoredByComputation { get; set; }

        public decimal Width { get; set; }
    }

    public enum CellStyle
    {
        Text,
        Currency,
        Date,
        DateTime,
        Number,
        Percentage,
        Month,
        Html,
        Width,
        TextWrapped,
        PhoneNumber
    }

    public interface ICellAttributeIgnoredByComputation
    {
        bool IgnoredByComputation(string name);
    }
}
